-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2014 at 11:51 AM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yearsley`
--

-- --------------------------------------------------------

--
-- Table structure for table `exp_accessories`
--

CREATE TABLE IF NOT EXISTS `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_accessories`
--

INSERT INTO `exp_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`) VALUES
(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'),
(2, 'Structure_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|sites|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '3.3.4');

-- --------------------------------------------------------

--
-- Table structure for table `exp_actions`
--

CREATE TABLE IF NOT EXISTS `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `exp_actions`
--

INSERT INTO `exp_actions` (`action_id`, `class`, `method`) VALUES
(1, 'Comment', 'insert_new_comment'),
(2, 'Comment_mcp', 'delete_comment_notification'),
(3, 'Comment', 'comment_subscribe'),
(4, 'Comment', 'edit_comment'),
(5, 'Email', 'send_email'),
(6, 'Safecracker', 'submit_entry'),
(7, 'Safecracker', 'combo_loader'),
(8, 'Search', 'do_search'),
(9, 'Channel', 'insert_new_entry'),
(10, 'Channel', 'filemanager_endpoint'),
(11, 'Channel', 'smiley_pop'),
(12, 'Member', 'registration_form'),
(13, 'Member', 'register_member'),
(14, 'Member', 'activate_member'),
(15, 'Member', 'member_login'),
(16, 'Member', 'member_logout'),
(17, 'Member', 'retrieve_password'),
(18, 'Member', 'reset_password'),
(19, 'Member', 'send_member_email'),
(20, 'Member', 'update_un_pw'),
(21, 'Member', 'member_search'),
(22, 'Member', 'member_delete'),
(23, 'Rte', 'get_js'),
(24, 'Structure', 'ajax_move_set_data'),
(25, 'Libraree', 'save_to_file'),
(26, 'Assets_mcp', 'get_subfolders'),
(27, 'Assets_mcp', 'upload_file'),
(28, 'Assets_mcp', 'get_files_view_by_folders'),
(29, 'Assets_mcp', 'get_props'),
(30, 'Assets_mcp', 'save_props'),
(31, 'Assets_mcp', 'get_ordered_files_view'),
(32, 'Assets_mcp', 'move_folder'),
(33, 'Assets_mcp', 'create_folder'),
(34, 'Assets_mcp', 'delete_folder'),
(35, 'Assets_mcp', 'view_file'),
(36, 'Assets_mcp', 'move_file'),
(37, 'Assets_mcp', 'delete_file'),
(38, 'Assets_mcp', 'build_sheet'),
(39, 'Assets_mcp', 'get_selected_files'),
(45, 'Freeform', 'delete_freeform_notification'),
(44, 'Freeform', 'retrieve_entries'),
(43, 'Freeform', 'insert_new_entry'),
(46, 'Updater', 'ACT_general_router'),
(47, 'Assets_mcp', 'rename_folder'),
(48, 'Assets_mcp', 'view_thumbnail'),
(49, 'Assets_mcp', 'get_session_id'),
(50, 'Assets_mcp', 'start_index'),
(51, 'Assets_mcp', 'perform_index'),
(52, 'Assets_mcp', 'finish_index'),
(53, 'Assets_mcp', 'get_s3_buckets');

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_files`
--

CREATE TABLE IF NOT EXISTS `exp_assets_files` (
  `file_id` int(10) NOT NULL AUTO_INCREMENT,
  `folder_id` int(10) NOT NULL,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `source_id` int(10) unsigned DEFAULT NULL,
  `filedir_id` int(4) unsigned DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` int(10) unsigned DEFAULT NULL,
  `alt_text` tinytext,
  `caption` tinytext,
  `author` tinytext,
  `desc` text,
  `location` tinytext,
  `keywords` text,
  `date_modified` int(10) unsigned DEFAULT NULL,
  `kind` varchar(5) DEFAULT NULL,
  `width` int(2) DEFAULT NULL,
  `height` int(2) DEFAULT NULL,
  `size` int(3) DEFAULT NULL,
  `search_keywords` text,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `unq_folder_id__file_name` (`folder_id`,`file_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=130 ;

--
-- Dumping data for table `exp_assets_files`
--

INSERT INTO `exp_assets_files` (`file_id`, `folder_id`, `source_type`, `source_id`, `filedir_id`, `file_name`, `title`, `date`, `alt_text`, `caption`, `author`, `desc`, `location`, `keywords`, `date_modified`, `kind`, `width`, `height`, `size`, `search_keywords`) VALUES
(1, 1, 'ee', NULL, 1, 'dir_hy.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360161619, 'image', 148, 147, 8383, 'dir_hy.jpg'),
(2, 1, 'ee', NULL, 1, 'dir_jb.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360161619, 'image', 98, 96, 3916, 'dir_jb.jpg'),
(3, 1, 'ee', NULL, 1, 'dir_pw.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360161619, 'image', 98, 96, 4382, 'dir_pw.jpg'),
(4, 1, 'ee', NULL, 1, 'dir_ik.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360161619, 'image', 98, 96, 4371, 'dir_ik.jpg'),
(5, 1, 'ee', NULL, 1, 'dir_tm.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360161619, 'image', 98, 96, 3790, 'dir_tm.jpg'),
(6, 1, 'ee', NULL, 1, 'dir_mh.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360161619, 'image', 98, 96, 3771, 'dir_mh.jpg'),
(10, 2, 'ee', NULL, 2, 'home_logistics_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 635, 327, 54273, 'home_logistics_banner.jpg'),
(79, 2, 'ee', NULL, 2, 'home_food_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 635, 327, 50483, 'home_food_banner.jpg'),
(11, 2, 'ee', NULL, 2, 'img_placeholder.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061463, 'image', 128, 129, 1522, 'img_placeholder.jpg'),
(12, 2, 'ee', NULL, 2, 'img_placeholder2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061464, 'image', 128, 129, 1539, 'img_placeholder2.jpg'),
(13, 2, 'ee', NULL, 2, 'about_img.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 212, 151, 8846, 'about_img.jpg'),
(14, 2, 'ee', NULL, 2, 'belfield1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065297, 'image', 128, 129, 9960, 'belfield1.jpg'),
(15, 2, 'ee', NULL, 2, 'accreditations.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061462, 'image', 152, 169, 19371, 'accreditations.png'),
(16, 2, 'ee', NULL, 2, 'belfield-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061462, 'image', 1280, 326, 155110, 'belfield-page.jpg'),
(17, 2, 'ee', NULL, 2, 'belfield3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065298, 'image', 128, 129, 8877, 'belfield3.jpg'),
(18, 2, 'ee', NULL, 2, 'bs_logistics.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061462, 'image', 176, 97, 7789, 'bs_logistics.jpg'),
(19, 2, 'ee', NULL, 2, 'career-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061463, 'image', 1280, 326, 48841, 'career-page.jpg'),
(20, 2, 'ee', NULL, 2, 'bs_cold_storage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061462, 'image', 176, 97, 8357, 'bs_cold_storage.jpg'),
(21, 2, 'ee', NULL, 2, 'contact-us-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061463, 'image', 1280, 326, 38831, 'contact-us-page.jpg'),
(22, 2, 'ee', NULL, 2, 'cser-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 1280, 326, 59249, 'cser-page.jpg'),
(23, 2, 'ee', NULL, 2, 'directors-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061463, 'image', 1280, 326, 60713, 'directors-page.jpg'),
(24, 2, 'ee', NULL, 2, 'food-sales1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065298, 'image', 128, 129, 7383, 'food-sales1.jpg'),
(25, 2, 'ee', NULL, 2, 'food-sales2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065298, 'image', 128, 129, 10121, 'food-sales2.jpg'),
(26, 2, 'ee', NULL, 2, 'generic-about-us-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360061463, 'image', 1280, 326, 132558, 'generic-about-us-page.jpg'),
(27, 2, 'ee', NULL, 2, 'icePak1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065297, 'image', 128, 129, 8781, 'icePak1.jpg'),
(28, 2, 'ee', NULL, 2, 'icePak2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065298, 'image', 128, 129, 10544, 'icePak2.jpg'),
(29, 2, 'ee', NULL, 2, 'icepak-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 1280, 326, 197504, 'icepak-page.jpg'),
(30, 2, 'ee', NULL, 2, 'belfield2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065298, 'image', 128, 129, 7868, 'belfield2.jpg'),
(31, 2, 'ee', NULL, 2, 'logistics1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 128, 129, 8125, 'logistics1.jpg'),
(32, 2, 'ee', NULL, 2, 'lucky1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065298, 'image', 128, 129, 8412, 'lucky1.jpg'),
(33, 2, 'ee', NULL, 2, 'logistics2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360065298, 'image', 128, 129, 10644, 'logistics2.jpg'),
(34, 2, 'ee', NULL, 2, 'logistics-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 1280, 326, 86114, 'logistics-page.jpg'),
(35, 2, 'ee', NULL, 2, '559178_10151122573650921_1221548660_n1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360077544, 'image', 238, 200, 64827, '559178_10151122573650921_1221548660_n1.jpg'),
(36, 2, 'ee', NULL, 2, 'Childrens_Hospital_Logo_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360076744, 'image', 456, 333, 58445, 'Childrens_Hospital_Logo_4.jpg'),
(37, 2, 'ee', NULL, 2, 'Pg_8_photo_(5)1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360076745, 'image', 248, 333, 60409, 'Pg_8_photo_(5)1.jpg'),
(38, 2, 'ee', NULL, 2, 'photo4.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360077544, 'image', 150, 200, 54583, 'photo4.JPG'),
(39, 2, 'ee', NULL, 2, 'green_bear_at_hosp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360076745, 'image', 376, 333, 75198, 'green_bear_at_hosp.jpg'),
(40, 2, 'ee', NULL, 2, 'Final_Movember_mug_shots_003.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360076745, 'image', 446, 333, 81855, 'Final_Movember_mug_shots_003.jpg'),
(41, 2, 'ee', NULL, 2, 'Childrens Hospital Logo 4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360077542, 'image', 274, 200, 42281, 'Childrens Hospital Logo 4.jpg'),
(42, 2, 'ee', NULL, 2, 'Final Movember mug shots 003.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360077544, 'image', 268, 200, 49640, 'Final Movember mug shots 003.jpg'),
(43, 2, 'ee', NULL, 2, 'Pg 8 photo (5)1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360077540, 'image', 149, 200, 46142, 'Pg 8 photo (5)1.jpg'),
(44, 2, 'ee', NULL, 2, 'cser1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360078245, 'image', 177, 178, 15495, 'cser1.jpg'),
(45, 2, 'ee', NULL, 2, 'cser2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360078260, 'image', 177, 178, 7520, 'cser2.jpg'),
(46, 2, 'ee', NULL, 2, 'cser3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360078261, 'image', 177, 178, 8372, 'cser3.jpg'),
(47, 2, 'ee', NULL, 2, 'cser4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360078245, 'image', 177, 178, 12370, 'cser4.jpg'),
(48, 2, 'ee', NULL, 2, 'cser5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360078246, 'image', 178, 177, 13727, 'cser5.jpg'),
(49, 2, 'ee', NULL, 2, 'cser6.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360078245, 'image', 177, 178, 14762, 'cser6.jpg'),
(50, 2, 'ee', NULL, 2, 'green bear at hosp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360077544, 'image', 226, 200, 56291, 'green bear at hosp.jpg'),
(51, 2, 'ee', NULL, 2, 'structure-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 1280, 326, 100248, 'structure-page.jpg'),
(52, 2, 'ee', NULL, 2, 'cser10.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 128, 128, 32829, 'cser10.jpg'),
(53, 2, 'ee', NULL, 2, 'cser7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360079444, 'image', 177, 177, 15843, 'cser7.jpg'),
(54, 2, 'ee', NULL, 2, 'cser8.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 128, 128, 36008, 'cser8.jpg'),
(55, 2, 'ee', NULL, 2, 'cser9.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 128, 128, 29902, 'cser9.jpg'),
(56, 1, 'ee', NULL, 1, 'Harry-and-Mayoress.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360168377, 'image', 230, 260, 17531, 'Harry-and-Mayoress.jpg'),
(57, 2, 'ee', NULL, 2, 'Isoqar_logo.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 145, 125, 5025, 'Isoqar_logo.gif'),
(58, 2, 'ee', NULL, 2, 'brc_logo.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 145, 125, 4300, 'brc_logo.gif'),
(59, 2, 'ee', NULL, 2, 'cser11.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 128, 128, 21781, 'cser11.jpg'),
(60, 2, 'ee', NULL, 2, 'lucky-red-thumb.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 128, 129, 11192, 'lucky-red-thumb.jpg'),
(61, 2, 'ee', NULL, 2, 'news-temporary.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 1280, 326, 6049, 'news-temporary.jpg'),
(62, 2, 'ee', NULL, 2, 'rha_logo.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 145, 125, 3298, 'rha_logo.gif'),
(63, 2, 'ee', NULL, 2, 'royal-manchester-childrens-hospital.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 288, 199, 46652, 'royal-manchester-childrens-hospital.jpg'),
(64, 2, 'ee', NULL, 2, 'sts_logo.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360334349, 'image', 145, 125, 5293, 'sts_logo.gif'),
(65, 2, 'ee', NULL, 2, 'food-sales-page-banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 1280, 326, 99473, 'food-sales-page-banner.jpg'),
(66, 2, 'ee', NULL, 2, 'food-sales3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 128, 129, 9209, 'food-sales3.jpg'),
(67, 2, 'ee', NULL, 2, 'belfield4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 128, 129, 9004, 'belfield4.jpg'),
(68, 2, 'ee', NULL, 2, 'belfield5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 128, 129, 8067, 'belfield5.jpg'),
(69, 2, 'ee', NULL, 2, 'locations-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 1280, 326, 183700, 'locations-page.jpg'),
(70, 2, 'ee', NULL, 2, 'cser12.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 128, 129, 9699, 'cser12.jpg'),
(71, 2, 'ee', NULL, 2, 'acc-bfff-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 107, 100, 9549, 'acc-bfff-logo.jpg'),
(72, 2, 'ee', NULL, 2, 'acc-biff-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 107, 100, 9549, 'acc-biff-logo.jpg'),
(73, 2, 'ee', NULL, 2, 'acc-brc-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 78, 100, 6124, 'acc-brc-logo.jpg'),
(74, 2, 'ee', NULL, 2, 'acc-fsdf-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 107, 87, 5270, 'acc-fsdf-logo.jpg'),
(75, 2, 'ee', NULL, 2, 'acc-isoqar-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 105, 100, 6704, 'acc-isoqar-logo.jpg'),
(76, 2, 'ee', NULL, 2, 'acc-rha-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 78, 87, 3857, 'acc-rha-logo.jpg'),
(77, 2, 'ee', NULL, 2, 'acc-sts-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361357955, 'image', 105, 87, 6529, 'acc-sts-logo.jpg'),
(78, 2, 'ee', NULL, 2, 'rha-logoacc-.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360851656, 'image', 78, 87, 4686, 'rha-logoacc-.jpg'),
(80, 1, 'ee', NULL, 1, 'Time_Capsulet_Cover_Sheet.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360161620, 'pdf', NULL, NULL, 176676, 'Time_Capsulet_Cover_Sheet.pdf'),
(81, 2, 'ee', NULL, 2, 'home_pie_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360921935, 'image', 635, 327, 81372, 'home_pie_banner.jpg'),
(82, 2, 'ee', NULL, 2, 'home_cranes_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360921935, 'image', 635, 327, 128776, 'home_cranes_banner.jpg'),
(83, 2, 'ee', NULL, 2, 'home_crates_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360921935, 'image', 635, 327, 107432, 'home_crates_banner.jpg'),
(84, 2, 'ee', NULL, 2, 'home_prawn_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360921935, 'image', 635, 327, 70984, 'home_prawn_banner.jpg'),
(85, 2, 'ee', NULL, 2, 'home_prof_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360921935, 'image', 635, 327, 87780, 'home_prof_banner.jpg'),
(86, 2, 'ee', NULL, 2, 'home_cake_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360921935, 'image', 635, 327, 66066, 'home_cake_banner.jpg'),
(87, 2, 'ee', NULL, 2, 'home_storage_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1360921936, 'image', 635, 327, 100485, 'home_storage_banner.jpg'),
(88, 2, 'ee', NULL, 2, 'Commitment_Thumbnail.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361186483, 'image', 700, 1000, 567506, 'Commitment_Thumbnail.jpg'),
(89, 2, 'ee', NULL, 2, 'Lucky_Red_Thumbnail.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361186721, 'image', 1000, 704, 928406, 'Lucky_Red_Thumbnail.jpg'),
(90, 2, 'ee', NULL, 2, 'Lucky-Red-Thumbnail.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361186829, 'image', 128, 128, 17490, 'Lucky-Red-Thumbnail.jpg'),
(91, 2, 'ee', NULL, 2, 'Commitment-Thumbnail.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361186881, 'image', 128, 128, 11077, 'Commitment-Thumbnail.jpg'),
(92, 2, 'ee', NULL, 2, 'Invite-School-Children.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361196453, 'image', 618, 824, 174579, 'Invite-School-Children.jpg'),
(93, 2, 'ee', NULL, 2, 'Invite-School-Children_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361196685, 'image', 618, 824, 174579, 'Invite-School-Children_1.jpg'),
(94, 2, 'ee', NULL, 2, 'Invite-School-Children_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361196781, 'image', 618, 618, 411512, 'Invite-School-Children_2.jpg'),
(95, 2, 'ee', NULL, 2, 'Invite-School-Children_3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361196850, 'image', 618, 618, 411512, 'Invite-School-Children_3.jpg'),
(96, 2, 'ee', NULL, 2, 'Yearsley-Logistics-wins-CSM-contract.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361197008, 'image', 618, 423, 78610, 'Yearsley-Logistics-wins-CSM-contract.jpg'),
(97, 2, 'ee', NULL, 2, 'new-identity-news.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361281289, 'image', 618, 430, 58817, 'new-identity-news.jpg'),
(98, 2, 'ee', NULL, 2, 'Icepak_packaging_2.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361293816, 'image', 400, 532, 57275, 'Icepak_packaging_2.jpeg'),
(99, 2, 'ee', NULL, 2, 'Icepak-packaging-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361353023, 'image', 128, 129, 10532, 'Icepak-packaging-2.jpg'),
(100, 2, 'ee', NULL, 2, 'Heywood-Solar-Installation.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361297285, 'image', 128, 128, 8642, 'Heywood-Solar-Installation.jpg'),
(101, 2, 'ee', NULL, 2, 'prawn-stirfry.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361352907, 'image', 128, 129, 7173, 'prawn-stirfry.jpg'),
(103, 2, 'ee', NULL, 2, '6789949956_8261de0731_b1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361448411, 'image', 630, 483, 74056, '6789949956_8261de0731_b1.jpg'),
(104, 2, 'ee', NULL, 2, 'Hams-Hall-(2).jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361448657, 'image', 630, 443, 85975, 'Hams-Hall-(2).jpg'),
(105, 2, 'ee', NULL, 2, 'Harry-Yearsley-plus-HGV.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361448808, 'image', 640, 370, 61521, 'Harry-Yearsley-plus-HGV.jpg'),
(106, 2, 'ee', NULL, 2, 'Pg-front--Solar-Installation.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361448898, 'image', 630, 421, 84254, 'Pg-front--Solar-Installation.jpg'),
(107, 2, 'ee', NULL, 2, 'prawn-stirfry_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361448942, 'image', 630, 434, 42862, 'prawn-stirfry_1.jpg'),
(108, 2, 'ee', NULL, 2, 'Yearsley-(31).jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361449069, 'image', 630, 630, 103097, 'Yearsley-(31).jpg'),
(109, 2, 'ee', NULL, 2, 'Yearsley-07.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361450019, 'image', 630, 434, 93880, 'Yearsley-07.jpg'),
(110, 2, 'ee', NULL, 2, '2012.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361461783, 'image', 630, 461, 90050, '2012.jpg'),
(112, 2, 'ee', NULL, 2, 'cser-page-alt.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361180488, 'image', 1280, 326, 192059, 'cser-page-alt.jpg'),
(113, 2, 'ee', NULL, 2, 'home_bean_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361536236, 'image', 635, 327, 89136, 'home_bean_banner.jpg'),
(114, 2, 'ee', NULL, 2, 'head-office-new-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361359869, 'image', 1280, 326, 217631, 'head-office-new-logo.jpg'),
(115, 2, 'ee', NULL, 2, 'logolink-foodsales.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361197519, 'image', 278, 139, 18998, 'logolink-foodsales.jpg'),
(116, 2, 'ee', NULL, 2, 'logolink-belfield.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361197373, 'image', 278, 139, 14731, 'logolink-belfield.jpg'),
(117, 2, 'ee', NULL, 2, 'logolink-luckyred.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361205469, 'image', 278, 139, 16866, 'logolink-luckyred.jpg'),
(118, 2, 'ee', NULL, 2, 'logolink-logistics.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361197509, 'image', 278, 139, 18185, 'logolink-logistics.jpg'),
(119, 2, 'ee', NULL, 2, 'logolink-icepak.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361205560, 'image', 278, 139, 17422, 'logolink-icepak.jpg'),
(120, 2, 'ee', NULL, 2, 'lucky-red-comp.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361359862, 'image', 1280, 326, 150141, 'lucky-red-comp.jpeg'),
(121, 2, 'ee', NULL, 2, 'netstock-banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361380136, 'image', 1280, 326, 283814, 'netstock-banner.jpg'),
(122, 2, 'ee', NULL, 2, 'rochdale-expansion-banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361361490, 'image', 1280, 326, 271557, 'rochdale-expansion-banner.jpg'),
(123, 2, 'ee', NULL, 2, 'home_truckblue_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361543913, 'image', 635, 327, 79566, 'home_truckblue_banner.jpg'),
(125, 2, 'ee', NULL, 2, 'home_truckblue_banner_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361536916, 'image', 635, 327, 79566, 'home_truckblue_banner_1.jpg'),
(126, 2, 'ee', NULL, 2, 'home_icecream_banner.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361537172, 'image', 635, 327, 100437, 'home_icecream_banner.jpg'),
(129, 2, 'ee', NULL, 2, 'Shot-2-right-hand-side.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1361544868, 'image', 635, 327, 57024, 'Shot-2-right-hand-side.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_folders`
--

CREATE TABLE IF NOT EXISTS `exp_assets_folders` (
  `folder_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `folder_name` varchar(255) NOT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  `filedir_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`folder_id`),
  UNIQUE KEY `unq_source_type__source_id__parent_id__folder_name` (`source_type`,`source_id`,`parent_id`,`folder_name`),
  UNIQUE KEY `unq_source_type__source_id__full_path` (`source_type`,`source_id`,`full_path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_assets_folders`
--

INSERT INTO `exp_assets_folders` (`folder_id`, `source_type`, `folder_name`, `full_path`, `parent_id`, `source_id`, `filedir_id`) VALUES
(1, 'ee', 'uploads', '', NULL, NULL, 1),
(2, 'ee', 'Generic Images', '', NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_index_data`
--

CREATE TABLE IF NOT EXISTS `exp_assets_index_data` (
  `session_id` char(36) DEFAULT NULL,
  `source_type` varchar(2) DEFAULT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  `offset` int(10) unsigned DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `filesize` int(10) unsigned DEFAULT NULL,
  `type` enum('file','folder') DEFAULT NULL,
  `record_id` int(10) unsigned DEFAULT NULL,
  UNIQUE KEY `unq__session_id__source_type__source_id__offset` (`session_id`,`source_type`,`source_id`,`offset`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_selections`
--

CREATE TABLE IF NOT EXISTS `exp_assets_selections` (
  `file_id` int(10) DEFAULT NULL,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `col_id` int(6) unsigned DEFAULT NULL,
  `row_id` int(10) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `sort_order` int(4) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  KEY `asset_id` (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `col_id` (`col_id`),
  KEY `row_id` (`row_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_assets_selections`
--

INSERT INTO `exp_assets_selections` (`file_id`, `entry_id`, `field_id`, `col_id`, `row_id`, `var_id`, `sort_order`, `is_draft`) VALUES
(6, 67, 9, 5, 7, NULL, 0, 0),
(5, 67, 9, 5, 6, NULL, 0, 0),
(4, 67, 9, 5, 5, NULL, 0, 0),
(3, 67, 9, 5, 4, NULL, 0, 0),
(2, 67, 9, 5, 3, NULL, 0, 0),
(56, 67, 18, 7, 2, NULL, 0, 0),
(33, 25, 15, NULL, NULL, NULL, 1, 0),
(126, 32, 13, 1, 1, NULL, 2, 0),
(113, 32, 13, 1, 1, NULL, 1, 0),
(31, 25, 15, NULL, NULL, NULL, 0, 0),
(66, 26, 15, NULL, NULL, NULL, 1, 0),
(24, 26, 15, NULL, NULL, NULL, 0, 0),
(68, 27, 15, NULL, NULL, NULL, 1, 0),
(17, 27, 15, NULL, NULL, NULL, 0, 0),
(99, 28, 15, NULL, NULL, NULL, 1, 0),
(27, 28, 15, NULL, NULL, NULL, 0, 0),
(101, 29, 15, NULL, NULL, NULL, 1, 0),
(90, 29, 15, NULL, NULL, NULL, 0, 0),
(49, 23, 17, NULL, NULL, NULL, 5, 0),
(48, 23, 17, NULL, NULL, NULL, 4, 0),
(47, 23, 17, NULL, NULL, NULL, 3, 0),
(46, 23, 17, NULL, NULL, NULL, 2, 0),
(45, 23, 17, NULL, NULL, NULL, 1, 0),
(44, 23, 17, NULL, NULL, NULL, 0, 0),
(91, 24, 17, NULL, NULL, NULL, 3, 0),
(70, 24, 17, NULL, NULL, NULL, 2, 0),
(55, 24, 17, NULL, NULL, NULL, 1, 0),
(100, 3, 17, NULL, NULL, NULL, 3, 0),
(49, 3, 17, NULL, NULL, NULL, 2, 0),
(47, 3, 17, NULL, NULL, NULL, 1, 0),
(44, 3, 17, NULL, NULL, NULL, 0, 0),
(20, 71, 19, 12, 8, NULL, 0, 0),
(79, 32, 13, 1, 1, NULL, 0, 0),
(123, 32, 13, 3, 1, NULL, 2, 0),
(129, 32, 13, 3, 1, NULL, 1, 0),
(10, 32, 13, 3, 1, NULL, 0, 0),
(54, 24, 17, NULL, NULL, NULL, 0, 0),
(95, 66, 21, NULL, NULL, NULL, 0, 0),
(96, 65, 21, NULL, NULL, NULL, 0, 0),
(97, 74, 21, NULL, NULL, NULL, 0, 0),
(103, 57, 21, NULL, NULL, NULL, 0, 0),
(110, 64, 21, NULL, NULL, NULL, 0, 0),
(109, 61, 21, NULL, NULL, NULL, 0, 0),
(106, 63, 21, NULL, NULL, NULL, 0, 0),
(107, 59, 21, NULL, NULL, NULL, 0, 0),
(108, 55, 21, NULL, NULL, NULL, 0, 0),
(105, 58, 21, NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_sources`
--

CREATE TABLE IF NOT EXISTS `exp_assets_sources` (
  `source_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(2) NOT NULL DEFAULT 's3',
  `name` varchar(255) DEFAULT NULL,
  `settings` text NOT NULL,
  PRIMARY KEY (`source_id`),
  UNIQUE KEY `unq_source_type__source_id` (`source_type`,`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_captcha`
--

CREATE TABLE IF NOT EXISTS `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=154 ;

--
-- Dumping data for table `exp_captcha`
--

INSERT INTO `exp_captcha` (`captcha_id`, `date`, `ip_address`, `word`) VALUES
(152, 1363189197, '109.111.197.225', 'again92'),
(151, 1363189175, '109.111.197.225', 'decided71');

-- --------------------------------------------------------

--
-- Table structure for table `exp_categories`
--

CREATE TABLE IF NOT EXISTS `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_fields`
--

CREATE TABLE IF NOT EXISTS `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_field_data`
--

CREATE TABLE IF NOT EXISTS `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_groups`
--

CREATE TABLE IF NOT EXISTS `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_posts`
--

CREATE TABLE IF NOT EXISTS `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channels`
--

CREATE TABLE IF NOT EXISTS `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `exp_channels`
--

INSERT INTO `exp_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`) VALUES
(1, 1, 'generic', 'Generic', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 16, 0, 1361788491, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(2, 1, 'news', 'News', 'http://yearsley.stmpreview.co.uk/index.php', '', 'en', 25, 0, 1361280753, 0, '', 1, 'open', 2, 2, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', 'http://www.yearsleygroup.net/rss', 'n', 10, '', '', 0),
(3, 1, 'careers', 'Careers', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 0, 0, 0, 0, '', 1, 'open', 3, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(4, 1, 'directors', 'Directors', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 7, 0, 1360164798, 0, '', 1, 'open', 4, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(5, 1, 'homepage', 'Homepage', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 1, 0, 1359972943, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(6, 1, 'business_structure', 'Business Structure', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 0, 0, 0, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(7, 1, 'our_businesses', 'Our Businesses', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 6, 0, 1359450890, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(8, 1, 'cser', 'CSER', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 3, 0, 1359440404, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(9, 1, 'careers_intro', 'Careers Intro', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 1, 0, 1358530222, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(10, 1, 'timeline', 'Timeline', 'http://yearsley.stmpreview.co.uk/index.php', NULL, 'en', 1, 0, 1360688695, 0, '', 1, 'open', 5, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(11, 1, 'news_newsletters', 'News Newsletters', 'http://yearsley.stmpreview.co.uk/index.php', '', 'en', 2, 0, 1361276031, 0, '', 1, 'open', 6, 24, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_data`
--

CREATE TABLE IF NOT EXISTS `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` text,
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_21` text,
  `field_ft_21` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_26` text,
  `field_ft_26` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_data`
--

INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_2`, `field_ft_2`, `field_id_3`, `field_ft_3`, `field_id_4`, `field_ft_4`, `field_id_5`, `field_ft_5`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_11`, `field_ft_11`, `field_id_12`, `field_ft_12`, `field_id_13`, `field_ft_13`, `field_id_14`, `field_ft_14`, `field_id_15`, `field_ft_15`, `field_id_16`, `field_ft_16`, `field_id_17`, `field_ft_17`, `field_id_18`, `field_ft_18`, `field_id_19`, `field_ft_19`, `field_id_20`, `field_ft_20`, `field_id_21`, `field_ft_21`, `field_id_22`, `field_ft_22`, `field_id_23`, `field_ft_23`, `field_id_24`, `field_ft_24`, `field_id_25`, `field_ft_25`, `field_id_26`, `field_ft_26`) VALUES
(1, 1, 1, '<h2>\n	The Yearsley Group</h2>\n<p>\n	<img alt="" class="content-image" src="{filedir_2}about_img.jpg" style="width: 212px; height: 151px;" />Yearsley Group is leading the way in frozen foods via its two divisions: logistics and food sales.</p>\n<p>\n	Yearsley Logistics is the UK&rsquo;s largest logistics service provider in the frozen food sector, as well as offering ambient, chilled and freight forwarding solutions.<br />\n	It has 13 sites nationally, a total capacity of 320,000 pallets and a fleet of over 300 temperature controlled vehicles delivering nationwide.</p>\n<p>\n	Yearsley Food markets and supplies a full range of innovative frozen products to customers in all sectors of the marketplace including export. It also offers exclusive imported frozen foods under its Belfield brand and IcePak, the Group&rsquo;s specialist frozen seafood supplier serves the wholesale and restaurant market.</p>\n<p>\n	An industry recognised organisation, Yearsley Group has BRC and ISO 14001 accreditations and plays an active part in the British Frozen Food Federation and the RHA.</p>\n<p>\n	The future of the company is based on delivering a sustainable business model, focused on innovation, agility and customer service excellence. A commitment to re-investment has recently seen the development of our first &lsquo;Super Hub&rsquo; consolidation centre at Heywood, which will be completed May 2013, as well as the purchase of Hams Hall in the Midlands, bringing our Cold Store network to 13 sites.</p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '<h2>\n	About Us</h2>\n<p>\n	AAn industry recognised organisation, Yearsley Group has BRC and ISO 14001 accreditations and plays an active part in the British Frozen Food Federation and the RHA.</p>\n<p>\n	<img alt="" src="{filedir_2}accreditations.png" /></p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(2, 1, 7, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(3, 1, 8, '<p>\n	As a family company, we run our business for the long term, in a transparent and responsible way. Because of this, we are naturally concerned about the environmental footprint we leave for future generations.</p>\n<p>\n	Our business has many impacts on the environment, local communities and other groups including our employees, suppliers and customers. To do this we have developed a CSER strategy, objectives and targets. This is successfully helping us to identify and develop ways to manage our business sustainably and responsibly.</p>\n<p>\n	One key tool in helping us manage CSER in our business is our Social and Environmental Management System. We worked hard to make the system robust and effective, which has been recognised with ISO14001 accreditation.</p>\n<p>\n	CSER is a broad topic so we are focussing on our biggest impacts, these include:</p>\n<ul>\n	<li>\n		Reducing travel miles &ndash; and food miles for our customers</li>\n	<li>\n		Reducing our carbon footprint on electricity and fuel by adopting many initiatives around operational efficiency as well as developing renewable technologies.</li>\n	<li>\n		Protecting our employees&#39; health and safety</li>\n	<li>\n		Looking at how we can provide more sustainable products, in particular fish</li>\n</ul>\n<p>\n	For more information about our progress so far and targets for the future can be found in our <a href="{filedir_1}cser.pdf">CSER report</a>.</p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '<h2>\n	More Info</h2>\n<p>\n	Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.</p>', 'none', '', 'none', '', 'none', 'cser1.jpg\ncser4.jpg\ncser6.jpg\nHeywood-Solar-Installation.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'For more information about our progress so far and targets for the future, view our most up to date <span>CSER report</span>', 'none', '{filedir_1}cser.pdf', 'none', '', 'none', '', 'none', '', 'none'),
(4, 1, 1, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(5, 1, 9, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '<h2>\n	Working with us</h2>\n<p>\n	Want to work for a Nationwide company with a proven track record of success?</p>\n<p>\n	Want a job that offers both security and progression?</p>\n<p>\n	Then apply now&hellip;.</p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '<p>\n	No vacancies at present. Please check back soon or alternatively send your CV for future consideration to <a href="mailto:hr@yearsley.co.uk?subject=CV%20submission%20from%20Yearsley%20Group%20Website">hr@yearsley.co.uk</a></p>', 'none'),
(6, 1, 1, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(7, 1, 1, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '<h2>\n	About Us</h2>\n<p>\n	An industry recognised organisation, Yearsley Group has BRC and ISO 14001 accreditations and plays an active part in the British Frozen Food Federation and the RHA.</p>\n<p>\n	<img alt="" src="{filedir_2}accreditations.png" /></p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(8, 1, 1, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '<h2>\n	About Us</h2>\n<p>\n	An industry recognised organisation, Yearsley Group has BRC and ISO 14001 accreditations and plays an active part in the British Frozen Food Federation and the RHA.</p>\n<p>\n	<img alt="" src="{filedir_2}accreditations.png" /></p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(9, 1, 1, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(10, 1, 4, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'dir_hy.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(11, 1, 4, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'dir_jb.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(12, 1, 4, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'dir_pw.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(13, 1, 4, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'dir_ik.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(14, 1, 4, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'dir_tm.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(15, 1, 4, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'dir_mh.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(16, 1, 1, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(22, 1, 3, '', NULL, '', NULL, '', NULL, 'Permanent - Full Time', 'none', '£23,000', 'none', 'Manchester Office', 'none', '<p>\n	Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>\n<p>\n	Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>', 'none', 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(18, 1, 3, '', NULL, '', NULL, '', NULL, 'Permanent - Full Time', 'none', '£45,000 - £55,000 (negotiable)', 'none', 'Southern Based', 'none', '<p>\n	Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>\n<p>\n	Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue.</p>', 'none', 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.\n', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(19, 1, 1, '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(23, 1, 8, '<ul>\n	<li>\n		Employee nominated charity &ndash; Royal Manchester Children&rsquo;s Hospital has benefitted from various fundraising activities</li>\n	<li>\n		Donations sent to depot nominated local charities instead of corporate Christmas cards sent to clients.</li>\n	<li>\n		Links with local schools developed to support school projects &amp; show employment opportunities.</li>\n	<li>\n		Local sports teams and community initiatives supported with time and or money</li>\n	<li>\n		Encouraging employees to live a healthier life, reduced gym memberships offered, in-house events arranged</li>\n</ul>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '<h2>\n	More Info</h2>\n<p>\n	Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.</p>', 'none', '', 'none', '', 'none', 'cser1.jpg\ncser2.jpg\ncser3.jpg\ncser4.jpg\ncser5.jpg\ncser6.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'For more information about our progress so far and targets for the future, view our most up to date <span>CSER report</span>', 'none', '{filedir_1}cser.pdf', 'none', '', 'none', '', 'none', '', 'none'),
(24, 1, 8, '<ul>\n	<li>\n		Reduction of Electricity use by 8% by 2015</li>\n	<li>\n		Installation of &pound;3.5m worth of solar panels at 5 depots</li>\n	<li>\n		Reduced fuel usage by increasing pallets per vehicle (+5%yoy), decreasing empty running (-3%yoy) and improving driver performance (+3%yoy)</li>\n	<li>\n		Reduce CO2 emissions by 10% on both trailers &amp; units</li>\n	<li>\n		Reduce packaging used and office waste sent to landfill</li>\n	<li>\n		Source more fish from sustainable sources</li>\n	<li>\n		Implement an anti corruption policy</li>\n</ul>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '<h2>\n	More Info</h2>\n<p>\n	Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.</p>', 'none', '', 'none', '', 'none', 'cser8.jpg\ncser9.jpg\ncser12.jpg\nCommitment-Thumbnail.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'For more information about our progress so far and targets for the future, view our latest <span>Environmental Policy</span>', 'none', '{filedir_1}02._Environmental_Policy_SEMS13.pdf', 'none', '', 'none', '', 'none', '', 'none'),
(25, 1, 7, '<p>\n	As the largest frozen food logistics service provider in the UK, we can offer customers large and small, day one for day two, 7 days a week into major retailers, as well as an un-rivalled foodservice distribution network.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', 'logistics1.jpg\nlogistics2.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(26, 1, 7, '<p>\n	Supplying a comprehensive range of frozen foods nationwide to retail, foodservice and the cost sector, we have a reputation for outstanding customer service.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', 'food-sales1.jpg\nfood-sales3.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(27, 1, 7, '<p>\n	Supplying major retail, foodservice and industrial clients with a bespoke range of quality frozen products imported exclusively from our overseas manufacturing partners.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', 'belfield3.jpg\nbelfield5.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(28, 1, 7, '<p>\n	Specialising in the global sourcing and supply of frozen seafood and fish, we provide an extensive range whilst maintaining price stability and quality.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', 'icePak1.jpg\nIcepak-packaging-2.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(29, 1, 7, '<p>\n	Wholesaler and distributor of frozen, chilled and ambient products to the Chinese market with a strong focus on seafood and poultry.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', 'Lucky-Red-Thumbnail.jpg\nprawn-stirfry.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(30, 1, 1, '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(31, 1, 1, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt, nisl vel feugiat aliquet, odio lacus congue risus, at lobortis sapien massa in orci. Cras in leo mauris, a interdum mauris. Vivamus egestas, augue at tempus laoreet, ante nibh porta mauris, ut blandit eros ligula id ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras quis metus in diam venenatis dapibus. Nulla facilisi. Sed sollicitudin nulla quam, luctus tincidunt nisi. Sed ultricies, odio non tristique sollicitudin, risus justo cursus odio, sit amet rhoncus erat nulla sit amet mi.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(32, 1, 5, '<p>\n	Yearsley Group is leading the way in frozen foods via its two divisions: logistics and food.</p>\n<p>\n	<a href="http://yearsley.co.uk">Yearsley Logistics</a> is the UK&rsquo;s largest logistics service provider in the frozen food sector, as well as offering ambient, chilled and freight forwarding solutions.</p>\n<p>\n	<a href="http://yearsleyfood.co.uk">Yearsley Food</a> markets and supplies a full range of innovative frozen products to customers in all sectors of the marketplace including export.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(43, 1, 2, '', NULL, '<p>\n	Hot Cross Buns, Hot Cross Buns........ Yearsley Group was singing the traditional rhyme this Easter with more than 15 million buns stored in their cold stores.</p>\n<p>\n	Tens of thousands of pallets of Britain&#39;s finest treats were stored at two of Yearsley Group&#39;s depots in the Midlands. The Coleshill and Chesterfield cold stores were close to the manufacturing sites of these festive breads.</p>\n<p>\n	Tim Moran, Yearsley Group Sales Director said, "It is quite common for us to store seasonal lines, with stocks building up to high levels prior to the event and then delivered within a short period of time. Obvious examples are turkeys and mince pies at Christmas and ice cream in the summer. Our experience of this type of sale means we are very accomplished at it. Major benefits that we offer our clients include the fact that we can deliver on our own fleet of vehicles which means we have full control. Also NetStock our on-line information system is vital for both us and our clients at such times. Clients can view all the information they need about stocks and deliveries and inform us of any extra deliveries on-line and at real time. This obviously cuts down on time taken to contact an individual to get this information required."</p>\n<p>\n	15 Million Hot Cross buns seems like a mountain of bread but the systems Yearsley Group have in place ensured all our daughters and sons got theirs this Easter!!</p>', 'none', 'Hot Cross Buns, Hot Cross Buns........ Yearsley Group was singing the traditional rhyme this Easter with more than 15 million buns stored in their cold stores.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(44, 1, 2, '', NULL, '<p>\n	Profiteroles, Gateaux, Roulades, Eclairs, Sponges, Slices .... Whatever dessert you need Mr King&#39;s has the answer.</p>\n<p>\n	Belfield launched the Mr Kings range in 2009 with just 1 product, the cream slice. The product was so successful obtaining many listings and consumer sales that more products were immediately added to the range. Subsequent launches now take the total range to 14 with more exciting products in development.</p>\n<p>\n	The relationship that Belfield has with its supplying partners means that if a client requests a bespoke product or a variation on a theme these can be developed, tested and added to the range very quickly.</p>\n<p>\n	So, what is the secret to the success? Firstly the range is of good quality but with a price point that won&#39;t stretch the pocket. The recommended retail prices are from &pound;1.00 to &pound;2.99. Secondly Mr King&#39;s range is filling a gap in the market with a product that people want. The products are the ideal dessert to be eaten at any time with a price point that means they don&#39;t have to be saved for the weekend or special occasions.</p>\n<p>\n	For further information on these and Belfield&#39;s other product ranges, Tel 01706 694610 , email enquiries@belfield.co.uk.or visit www.belfield.co.uk</p>', 'none', 'Profiteroles, Gateaux, Roulades, Eclairs, Sponges, Slices .... Whatever dessert you need Mr King''s has the answer.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(45, 1, 2, '', NULL, '<p>\n	Yearsley Group, the UK&#39;s largest cold storage and distribution provider, has appointed Trafford-based Russells Construction to carry out a refurbishment at its Grimsby depot. The &pound;500,000 contract will see the installation of a new 63,000 sq ft superflat, insulated concrete floor.</p>\n<p>\n	With a &pound;130m turnover Yearsley Group has 12 sites across the UK and has instructed Russells Construction to complete a number of contracts around the country over recent years, including a new 100,000 sq/ft cold store in the north east town of Seaham and works to their Birmingham and Gillingham stores. This latest 10-week project is set to start immediately.</p>\n<p>\n	Jonathan Baker Commercial Director at Yearsley Group said &#39;Our policy is to have efficient facilities and this investment is ensuring we maintain a sustainable supply chain.&#39;</p>\n<p>\n	Gareth Russell, construction director at Russells Construction, said: &#39;The Yearsley Group is a longstanding client of ours and we understand their business well. This new insulated floor will enable the Grimsby store to control storage conditions with even greater accuracy and improve energy efficiency.&#39;</p>', 'none', 'Yearsley Group, the UK''s largest cold storage and distribution provider, has appointed Trafford-based Russells Construction to carry out a refurbishment at its Grimsby depot. The £500,000 contract will see the installation of a new 63,000 sq ft superflat, insulated concrete floor.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(46, 1, 2, '', NULL, '<p>\n	Yearsley Group is set to submit a planning application for a new 160,000sq ft cold storage and distribution facility close to its UK headquarters in Heywood. The frozen food specialist hopes to acquire a neighbouring 13.6 acre plot to allow for the expansion of its North West operation, in a move that could create around 150 new jobs for the area.</p>\n<p>\n	The site is part of the Hareshill Distribution Park and already has outline planning consent for development for employment use. This latest application, due to be registered with Rochdale Borough Council next week, covers details such as layout, design and landscaping.</p>\n<p>\n	If approved, the plan would see investment of more than &pound;20m in the acquisition of the land and construction of a new cold store, which would be built to high environmental rating standards. The expansion forms part of the Yearsley Group&#39;s mid- to long-term growth strategy, complementing the existing Heywood operation which employs around 300 people.</p>\n<p>\n	Harry Yearsley, managing director of Yearsley Group, said the proposal demonstrated the firm&#39;s commitment to the area. He said: "As our business continues to grow, we need to expand our current storage facility and distribution capacity to meet demand from customers and improve the efficiency of our operations.</p>\n<p>\n	"We invested heavily in making Heywood the centre of our UK operation in 1999 and it is the obvious location for our future expansion. The site is ideal for logistics distribution with good links to the M66, and would support our existing facility. We have almost 300 employees in Heywood, with the vast majority living within a 10-mile radius, and we are committed to supporting the economy of the Heywood and wider Rochdale area through the recruitment of local staff."</p>\n<p>\n	John Hudson, chief executive of the Rochdale Development Agency, said: "We have had a long association with the Yearsley Group and it is pleasing to see this important local company making plans for another major investment in the borough. The opportunity to create perhaps 150 new jobs is clearly welcome and helps consolidate the presence of this growing company in Heywood".</p>\n<p>\n	Councillor Peter Williams, Rochdale&#39;s Cabinet member for Corporate Management and Economic Regeneration, added: "We&#39;ll need to examine the planning aspects of the proposed development, but we welcome Yearsley Group&#39;s commitment to investing in the borough and creating new jobs. It also shows once again that Heywood is well placed for accommodating successful and expanding businesses."</p>\n<p>\n	Yearsley Group operates in two sectors: frozen food sales and cold storage and distribution. The new facility would expand the capacity of the storage and logistics division. The proposed building would be similar in style to the neighbouring facility, providing a floor space of around 160,000 sq ft. The height of the proposed building would be within the parameters set by the outline planning permission, and would be built to the environmental rating - BREEAM &#39;Very Good&#39; - standard. The plan includes provision for 111 car parking spaces. HGV parking and 20 employee cycle spaces will also be provided, with access via Hareshill Road and Hill Top Road. All current and new employees would be encouraged to respect the company&#39;s green travel plan, promoting car sharing, cycling and walking, and the use of public transport. Additional HGV traffic would be directed, as at present, along the distribution park&#39;s dedicated logistics route to the M66, avoiding Heywood&#39;s main residential areas.</p>', 'none', 'Yearsley Group is set to submit a planning application for a new 160,000sq ft cold storage and distribution facility close to its UK headquarters in Heywood. ', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(47, 1, 2, '', NULL, '<p>\n	Leading frozen food company Yearsley Group today announced the acquisition of Leeds-based Ice Pak Seafood Specialists Ltd for an undisclosed sum.&nbsp;Based in Heywood, Greater Manchester, Yearsley Group operates in two sectors; frozen food sales and cold storage and logistics.</p>\n<p>\n	The Yearsley Food Sales division wholesales a full range of frozen food products, supplying retailers, caterers and institutions in all sectors of the UK marketplace. The division operates a purpose-built repack facility at its Heywood cold store, and supplies overseas customers with exports of traditional UK products.</p>\n<p>\n	Yearsley Food Sales also incorporates the group&#39;s exclusive import operation, under the Belfield brand, while the cold storage and logistics division is the UK&#39;s largest provider of temperature controlled stores and distribution services, operating from 12 depots nationwide with a fleet of more than 300 vehicles.</p>\n<p>\n	Following a transitional period, Ice Pak will be incorporated into the Yearsley Food Sales division, complementing the current operation by providing additional seafood-based product lines to existing customers. The move will also enable the Yearsley Food Sales to enter new market areas, thereby further increasing its share of the frozen foods supply sector, in line with its current growth strategy.</p>\n<p>\n	Harry Yearsley, managing director of Yearsley Group, said: "We are delighted to announce the acquisition of Ice Pak. It strengthens our share of the frozen food sales market and, as there are very few crossovers in terms of customer base and product range, the opportunities to grow the business are huge."</p>\n<p>\n	Founded in 1983 by Alan and Tony Cantrill, Ice Pak grew to a &pound;32m turnover, supplying frozen food to wholesalers and restaurants across the UK from its cold stores in Leeds and Middleton. Ice Pak will continue to trade independently before being fully incorporated into Yearsley Food Sales, with Tony and Alan Cantrill working closely with Yearsley Group sales director Ian King over the next few months to help ensure a smooth transition for existing customers.</p>\n<p>\n	Commenting on the deal, Tony Cantrill said: "We have spent almost 30 years building up this business and are pleased to see it being taken forward as part of a well respected and growing national frozen food supplier."</p>', 'none', 'Leading frozen food company Yearsley Group today announced the acquisition of Leeds-based Ice Pak Seafood Specialists Ltd for an undisclosed sum.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(48, 1, 2, '', NULL, '<p>\n	The UK&#39;s largest cold storage and distribution provider has appointed a new head of logistics. Based at Yearsley Group&#39;s Heywood headquarters in Greater Manchester, Mark Haslam will oversee the operation which takes in a national network of 12 cold stores and a fleet of more than 300 temperature controlled vehicles.</p>\n<p>\n	The 40-year-old from Bury has more than 20 years&#39; experience in the sector, with Wincanton, TDG and, more recently, as managing director for the UK transport operations of Suttons Group.</p>\n<p>\n	Mark joins Yearsley Group as part of the firm&#39;s strategic commitment to supply chain optimisation and cost reduction. Managing director Harry Yearsley welcomed Mark to the group. He said: "With rising fuel costs and increasing sustainability demands, pressure is on distributors to maximise efficiency without compromising service delivery.</p>\n<p>\n	"Yearsley Group is responding to the needs of both manufacturing clients and retail customers by implementing a strategy that increases efficiency by improving consolidation and cutting waste delivery miles. Mark has a strong track record in the sector and we have brought him on board to spearhead this programme. We are confident that both Yearsley Group and our customers will soon start to realise the benefits and are pleased to welcome him to the group."</p>\n<p>\n	One of Marks&#39; first tasks will be to implement a new fleet management system which will provide additional control over the response time and flexibility of collections and deliveries. This will enable managers to better map journeys, ensuring more outbound deliveries link with inbound collections, thereby reducing both fuel usage and carbon emissions.</p>\n<p>\n	Mark said: "I&#39;m delighted to have the chance to build upon the great work Yearsley Group is doing in terms of consolidation and efficiency. The industry is fast moving towards this model as manufacturers are keen to cut food miles by delivering to one hub, and retailers prefer to reduce the number of deliveries into their stores with mixed loads.</p>\n<p>\n	"This is ideal for Yearsley Group which already consolidates stock from hundreds of manufacturers at its national network of 12 cold stores, from where we can provide bespoke deliveries. This benefits our manufacturing customers who can entrust their collection and delivery to our fleet of vehicles, any time of the day or night. We can also satisfy retailers who want more flexibility in their orders and the 24-hour, seven day delivery capability, which allows them to reduce their own storage requirements.</p>\n<p>\n	"I will be leading an experienced team of logistics professionals to identify further opportunities for consolidation and optimise the efficiency of our delivery management, providing savings across the supply chain."</p>\n<p>\n	Within its broad client base, Yearsley Group can take on the logistics requirements of small scale manufacturers while also providing off-site frozen storage solution for larger producers. It has relationships and agreements to deliver into all of the country&#39;s largest retail multiples and food service companies. Customers include Heinz, Youngs and Brakes.</p>', 'none', 'The UK''s largest cold storage and distribution provider has appointed a new head of logistics. ', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(49, 1, 2, '', NULL, '<p>\n	Yearsley Group, the frozen food storage, distribution and wholesale specialist, has once again thrown its support behind the British Frozen Food Federation and its members. For the third year running, Yearsley Group will sponsor the prestigious BFFF Awards, Retail Section, which aims to seek out and recognise the very best new frozen food products brought to market over the last year.</p>\n<p>\n	Yearsley Group&#39;s sponsorship demonstrates its long term commitment to supporting product development and innovation, which is championed by the BFFF. Harry Yearsley, managing director of Yearsley Group, said:<br />\n	"The BFFF is the authority in the UK&#39;s frozen food industry and these awards are an annual showcase for the sector. The event provides the ideal chance for both manufacturers and retailers to demonstrate the very best of what they have to offer consumers. It is also a great opportunity to recognise the impressive innovation in frozen food product development by the sector as a whole which is why we are pleased to be sponsoring the BFFF Awards once again."</p>\n<p>\n	Based in Heywood, Greater Manchester, Yearsley Group operates in two sectors; frozen food sales, and cold storage and logistics. The Yearsley Food Sales division wholesales a full range of frozen food products, supplying retailers, caterers and institutions in all sectors of the UK marketplace. The division, which recently acquired seafood specialist Ice Pak, also incorporates the group&#39;s exclusive import operation, under the Belfield brand. The cold storage and logistics division is the UK&#39;s largest provider of temperature controlled stores and distribution services, operating from 12 depots nationwide with a fleet of more than 300 vehicles.</p>\n<p>\n	Brian Young, director general for the BFFF, said the support of sponsors was very welcome:<br />\n	"With these awards we seek to recognise excellence in frozen food manufacturing and are always keen to partner with companies that support that ethos. Yearsley Group has long been a friend to the BFFF and its members and we thank them for their support once again this year."</p>', 'none', 'Yearsley Group, the frozen food storage, distribution and wholesale specialist, has once again thrown its support behind the British Frozen Food Federation and its members. ', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(50, 1, 2, '', NULL, '<p>\n	Frozen food wholesaler Yearsley Food Sales has been appointed to a two-year framework with the university catering organisation TUCO Purchasing. The Yearsley&#39;s team is already making the most of the opportunity, securing 16 new contracts with universities and colleges across the UK within just the first month.</p>\n<p>\n	Following a tender process, Yearsley Food Sales was appointed to the framework which supports catering procurement for more than 320 higher and further educations institutions nationwide. The opening up of this new market boosts Yearsley&#39;s already successful education portfolio.</p>\n<p>\n	Mark Fletcher from Yearsley Food Sales, said: "We were delighted to have been appointed to the framework agreement and are pleased with the initial success of our sales teams in securing these early contracts. Already supplier to a number of independent schools, local authorities and school catering customers, this represents our first major break into the higher and further education sector.</p>\n<p>\n	"At a time when the sector is rigorously reviewing its costs and seeking to improve its own services, we are pleased to be winning new customers. Early feedback from the catering managers shows many have been attracted to our competitive pricing structure and nationwide delivery capability."</p>\n<p>\n	Yearsley Food Sales offers more than 1,500 individual frozen food lines. Products proving popular so far with TUCO Purchasing clients include handheld snack foods &ndash; including chicken burgers, pies and pastries. In addition, specialist ethnic products from Yearsley&#39;s large Indian, Chinese, Halal and Kosher ranges have also been in high demand with many establishments now providing a wide choice of on-campus restaurants for staff, students and visitors.</p>\n<p>\n	Within the education sector, Yearsley Food Sales also supplies customers through relationships with the organisations such as the Catering Academy, Alliance in Partnership, G4S Group, Aramark, CMC and a number of independent schools.</p>\n<p>\n	Yearsley Food Sales wholesales a full range of frozen food products, supplying retailers, caterers and institutions in all sectors of the UK marketplace. The Food Sales division is part of Yearsley Group, which operates a purpose-built repack facility at its Heywood cold store, and has recently acquired seafood specialist IcePak, offering 500+ additional product lines. Food Sales customers benefit from the Group&#39;s distribution capability as the UK&#39;s largest provider of temperature controlled stores, with 12 depots nationwide with a fleet of more than 300 vehicles.</p>', 'none', 'Frozen food wholesaler Yearsley Food Sales has been appointed to a two-year framework with the university catering organisation TUCO Purchasing. ', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(51, 1, 2, '', NULL, '<p>\n	The UK&#39;s largest cold storage and distribution provider has invested more than &pound;500,000 in the creation of a state-of-the-art ambient storage chamber at its Grimsby depot. The 65,000 sq ft refurbished store provides more than 7,500 pallet spaces within Yearsley Group&#39;s existing warehouse facility.</p>\n<p>\n	The creation of an ambient offering comes in response to demand from local manufacturers and importers using Humber Bank Port, to use a single supplier for both their frozen and dry goods. The newly refurbished dry goods store complements the group&#39;s traditional frozen storage offering, which extends to 40,000 pallets at Grimsby. It is expected that existing customers will want to take space in both sections, for both raw materials and packaging, as well as finished products. It is also aimed at new customers for whom frozen storage is not required, and could even be used to store non-food items such as homewares and electricals.</p>\n<p>\n	Harry Yearsley, managing director of Yearsley Group, said: "We store and distribute a lot of frozen stock on behalf of local manufacturers and importers using the Humber ports, and we realised many of them also have a requirement for dry goods storage. Rather than split their deliveries - taking on additional contracts, management time and road miles - we took a decision to invest in our facility in order to offer that additional service to our customers.</p>\n<p>\n	"Bringing the same level of service, quality of customer relations and best-in-class warehouse management system that we offer for cold storage to the ambient market, will also open up new opportunities for us in the area, enabling us to extend our service provision to a whole new range of customers."</p>\n<p>\n	The ambient store maximises space with the use of narrow aisles, accessed with a fleet of &#39;articulated&#39; forklift trucks which have the capability to retrieve pallets as well as individual cases. These hi-tech articulated trucks are supplied by Aisle-Master and driven by fully trained, dedicated staff.</p>\n<p>\n	A host of ancillary services are also available, including individual order picking, shrink wrapping and of course onward dispatch. It is anticipated the new storage facility will also require the creation of new jobs, as customers start to come online.</p>\n<p>\n	The new storage space provides additional support for the Yearsley Group Vendor Management service offering &ndash; whereby suppliers to local manufacturers store all their raw materials in one place, reducing the amount of storage required by each individual manufacturer and enabling them to place specific orders as required. Storage of the frozen and non frozen raw materials in the same place reduces management time for manufacturers and enables better consolidation of loads.</p>\n<p>\n	All customers will also have access to Yearsley Group&#39;s bespoke warehouse management system Net Stock, enabling real time review of all stock stored within the Yearsley Group network. Yearsley Group consolidates stock from hundreds of manufacturers in its national network of 12 cold stores, enabling customers to reduce their own storage requirements. And with a fleet of more than 300 temperature controlled vehicles, Yearsley Group is also able to offer both collection of products and distribution into the UK&#39;s largest retailers and food service providers any time of the day or night, providing a 24-hour, seven day delivery capability.</p>\n<p>\n	To contact the Grimsby cold and ambient store please call 01472 252 100.</p>', 'none', 'The UK''s largest cold storage and distribution provider has invested more than £500,000 in the creation of a state-of-the-art ambient storage chamber at its Grimsby depot. ', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(52, 1, 2, '', NULL, '<p>\n	Huge investment in roof-mounted solar panel installations.</p>\n<br />\n<p>\n	Yearsley Group, the UK&#39;s largest cold storage and distribution provider, has installed solar panels at two of its cold stores in England. The vast roofs at both the Heywood cold store in Greater Manchester, and Holmewood near Chesterfield in Derbyshire, have been fitted with a total of 5,000 sq metres of photovoltaic panels.</p>\n<p>\n	These installations &ndash; each of around 2,500 sq m &ndash; are among the largest roof mounted solar panel systems in the UK. Each system produces 310,000 kilowatt hours per annum, thereby saving upwards of 170 tons of CO2 per warehouse, and will reduce the Group&#39;s reliance on the national grid. It is estimated they will provide each of the cold stores with up to 10 per cent of its annual electricity.<br />\n	The &pound;2.2m investment is part of an ongoing strategy by the company to increase the energy it obtains from renewable sources by 2015, in line with Yearsley Group&#39;s commitment to carbon reduction and sustainable sourcing. With the UK&#39;s largest national network of cold stores, Yearsley Group has plans to install renewable solutions (either solar or wind power) at other depots around the UK.</p>\n<p>\n	Harry Yearsley, managing director of Yearsley Group, said:<br />\n	"Sustainability is high on our agenda and the reduction of carbon emissions is key in terms of our warehouse operation and in regards to transportation. We have already implemented new management systems for reducing waste road miles and updated many of the cold stores with state-of-the-art insulation, but we knew we could do more and began to investigate renewable."</p>\n<p>\n	"Cold storage by its nature requires a large energy input and, with fuel prices increasing on an almost daily basis, any measures we could employ to create our own electricity would be welcome. Solar PV panels were the natural solution for the group as it makes sense to use the large exposed surface area of the warehouses to generate sustainable electricity. Hopefully these will be the first of many renewable projects that will help us to meet our carbon reduction commitments. It also coincides with our implementation of ISO 14001, the environmental management standard and ISO 26000, the international standard guidelines for social responsibility."</p>\n<p>\n	The panels have been installed on the southern elevation of each of the two storage units - as this is the most effective for catching the sun&#39;s rays &ndash; by Bury-based Solar Choice. Ajay Hargreaves, sales director for Solar Choice, added:</p>\n<p>\n	"Renewables, in particular solar power, are becoming more popular in response to increasing fuel costs and a rising public concern about energy reduction and carbon emissions. Large companies are taking their responsibilities to sustainability very seriously and seeking to reduce their reliance on the national grid. Yearsley Group has made a major investment and cleverly made use of its existing assets to create green energy to help power its own operations. These are by far the largest roof mounted installations we have ever done, and will be one of the biggest in the UK, producing an enormous amount of electricity in a clean and sustainable way."</p>', 'none', 'Huge investment in roof-mounted solar panel installations.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_2`, `field_ft_2`, `field_id_3`, `field_ft_3`, `field_id_4`, `field_ft_4`, `field_id_5`, `field_ft_5`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_11`, `field_ft_11`, `field_id_12`, `field_ft_12`, `field_id_13`, `field_ft_13`, `field_id_14`, `field_ft_14`, `field_id_15`, `field_ft_15`, `field_id_16`, `field_ft_16`, `field_id_17`, `field_ft_17`, `field_id_18`, `field_ft_18`, `field_id_19`, `field_ft_19`, `field_id_20`, `field_ft_20`, `field_id_21`, `field_ft_21`, `field_id_22`, `field_ft_22`, `field_id_23`, `field_ft_23`, `field_id_24`, `field_ft_24`, `field_id_25`, `field_ft_25`, `field_id_26`, `field_ft_26`) VALUES
(53, 1, 2, '', NULL, '<p>\n	Huge investment in roof-mounted solar panel installations.</p>\n<br />\n<p>\n	Yearsley Group, the UK&rsquo;s largest cold storage and distribution provider, has taken delivery of a &pound;1.4m fleet of 20 trucks that will help it further reduce the carbon emissions from its nationwide operation.</p>\n<p>\n	The Heywood-based company, which is committed to sustainability, recently announced a &pound;2.2 million investment in solar panels at two of its cold stores as it looks to increase the amount of energy it obtains from renewable sources.</p>\n<p>\n	Now 20 new MAN TGX 26.442BLS 6x2 tractor units have been delivered and will be based at five of Yearsley&rsquo;s 12 depots across the UK. In trials, the MAN units posted lower fuel consumption and maintenance costs that the alternatives Yearsley Group was considering.</p>\n<p>\n	&ldquo;This represents significant investment and is part of our on-going strategy of replacing vehicles to ensure they remain as environmentally friendly and cost efficient as possible,&rdquo; said Yearsley Group&rsquo;s head of logistics, Mark Haslam.</p>\n<p>\n	&ldquo;These vehicles are the first we have ordered from MAN and we believe that they are ideally suited to our operation.&rdquo;</p>\n<p>\n	Other recent green moves from Yearsley Group include the implementation of management systems for reducing waste road miles and the company has updated many of its cold stores with state-of-the-art insulation.</p>', 'none', 'Huge investment in roof-mounted solar panel installations.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(54, 1, 2, '', NULL, '<p>\n	An initiative between Yearsley Group and Tesco is helping save more than 140,000 empty running road miles per year.</p>\n<br />\n<p>\n	Yearsley Group customers in the South West are taking advantage of a sustainable delivery programme into one of Tesco&rsquo;s largest national distribution centres, after an agreement between the two companies. Manufacturers delivering into Tesco&rsquo;s Daventry frozen NDC, who store at Yearsley Group&rsquo;s Bristol depot, are contributing towards carbon saving initiatives.</p>\n<p>\n	The agreement sees Tesco trucks deployed from Daventry to retail outlets in the South West, return via Yearsley Group&rsquo;s Bristol depot to collect products destined for the Daventry NDC. This backhaul by Tesco vehicles saves up to 110 waste road miles per journey, and reduces the number of dedicated deliveries from Yearsley Group&rsquo;s Bristol store to Daventry, an estimated 1,350 times per year.</p>\n<p>\n	It also enables suppliers in the South East to reduce their own road miles, by delivering into the closer Yearsley depot rather than travelling up to Daventry in Northamptonshire.</p>\n<p>\n	Harry Yearsley, managing director of Yearsley Group, said: &ldquo;The scheme offers savings to manufacturers by reducing their own road miles. It also provides Tesco and Yearsley Group with significant benefits in terms of reducing waste miles, enabling more efficient use of vehicles and reducing the frequency of other vehicle deliveries into its own frozen NDC. As a logistics specialist, Yearsley Group is in the ideal position to facilitate consolidation such as this and it is this sort of partnership working which is allowing us to achieve the twin aims of improving service for our customers and reducing our own carbon emissions.&rdquo;</p>\n<p>\n	Yearsley Group is a Tesco Primary Approved provider for consolidation throughout its national network, and is working with the retailer to explore which other routes offer similar potential.</p>\n<p>\n	Yearsley Group consolidates stock from hundreds of manufacturers in its national network of 12 cold stores, enabling customers to reduce their own storage requirements. And with a fleet of more than 300 temperature controlled vehicles, Yearsley Group is also able to offer both collection of products and distribution into the UK&rsquo;s largest retailers and food service providers any time of the day or night, providing a 24-hour, seven day delivery capability.</p>\n<p>\n	All customers have access to Yearsley Group&rsquo;s bespoke warehouse management system NetStock, enabling real time review of all stock stored within the Yearsley Group network. Yearsley Group also offers a Vendor Management service giving swift access to manufacturers storing their raw materials in their local cold store and place specific orders as required.</p>\n<p>\n	The Group&rsquo;s environmental commitment includes reduction in empty running and waste road miles, increased consolidation of loads, use of energy saving measures and the installation renewable power sources at its cold stores.</p>\n<p>\n	To contact the Bristol cold store please call 01275 374 971 or go to <a href="http://www.yearsley.co.uk">www.yearsley.co.uk</a></p>', 'none', 'An initiative between Yearsley Group and Tesco is helping save more than 140,000 empty running road miles per year.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(55, 1, 2, '', NULL, '<p>\n	Global delivery with Yearsley Group&#39;s new freight forwarding division.</p>\n<br />\n<p>\n	Yearsley Group is &lsquo;going global&rsquo; with the launch of a new global logistics and freight forwarding division. Providing a single source solution to UK food manufacturing customers, the service will see Yearsley Group take control of the logistics and import operations for products from overseas suppliers, throughout their journey to Yearsley Group&rsquo;s wide logistics network.</p>\n<p>\n	The new offering extends the group&rsquo;s current service (of domestic port-side collection and deliveries into UK manufacturers) to include overseas road haulage, international freight transport, by ship or by air, and customs requirements. Development of partnerships with overseas logistics operators will allow customers and suppliers to benefit from Yearsley Group&rsquo;s experienced management and systems, from door-to-door. It will also require investment in a fleet of &lsquo;skeleton&rsquo; trailer units to collect the range of 20ft and 40ft containers (known as &lsquo;reefers&rsquo;) as they are received by ports in the UK and distributed to Yearsley Group&rsquo;s national network of cold stores.</p>\n<p>\n	It is anticipated that the move will help UK manufacturers to reduce the number of operators and intermediaries they encounter throughout the product&rsquo;s journey, and thereby time and cost inefficiencies. Accessing the Yearsley Group specialist frozen logistics management operation from source, also gives customers peace of mind as their container is handled by one, experienced team, throughout the journey.</p>\n<p>\n	Tim Moran, logistics sales director, said: &ldquo;Global logistics and freight forwarding is a complex business and can involved half-a-dozen different organisations from source to factory. This not only allows for cost inefficiencies but can also create a lot of red tape and stresses between the different suppliers. This single source solution aims to eliminate all of that with the provision of a more streamlined, transparent service. As with all aspects of our business, Yearsley Group will bring its experienced, well-managed systems and substantial buying power to this operation, to provide the very best quality and value to our current and future customers.&rdquo;</p>\n<p>\n	UK manufacturers will be able to request a contract covering any aspect of the product journey. This could include collection from the overseas supplier, plus international transfer, or simply collection from the UK port and road delivery into the Yearsley cold store. Yearsley&rsquo;s will also provide emergency air freight collection for customers with immediate and unforeseen requirements.</p>\n<p>\n	The global offering is expected to be of interest to many of Yearsley Group&rsquo;s existing manufacturing customers who source raw materials from overseas, and it is anticipated the move will allow the group to grow its share of the UK road haulage market.</p>\n<p>\n	Yearsley Group consolidates stock from hundreds of manufacturers in its national network of 12 cold stores, enabling customers to reduce their own storage requirements for raw materials and finished products. With a fleet of more than 300 temperature controlled vehicles, Yearsley Group is also able to offer both collection of products and distribution into the UK&rsquo;s largest retailers and food service providers any time of the day or night, providing a 24-hour, seven day delivery capability.</p>\n<p>\n	All customers have access to Yearsley Group&rsquo;s bespoke warehouse management system NetStock, enabling real time review of all stock stored within the Yearsley Group network. Yearsley Group also offers a vendor management service giving swift access to manufacturers storing their raw materials in their local cold store and enabling them to place specific orders as required.</p>\n<p>\n	To contact Yearsley Group please go to www.yearsley.co.uk</p>', 'none', 'Global delivery with Yearsley Group''s new freight forwarding division.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', 'Yearsley-(31).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(56, 1, 2, '', NULL, '<p>\n	Recently-launched &lsquo;ambient store&rsquo; secures major contract</p>\n<br />\n<p>\n	Young&rsquo;s Seafood Limited, the leading provider of chilled and frozen fish and seafood, has become the first major manufacturer to take advantage of Yearsley Group&rsquo;s new ambient storage facility, after signing a deal to take space at the Grimsby depot.</p>\n<p>\n	The UK seafood specialist, which has three manufacturing facilities based in Grimsby, has stored frozen raw materials and finished products with Yearsleys for more than 10 years. This new contract extends the amount of pallet space Young&rsquo;s Seafood Limited has with Yearsleys, and includes non-frozen ingredients and packaging.</p>\n<p>\n	It is the biggest contract of its type since the launch of Grimsby&rsquo;s ambient storage facility in summer 2011, following the &pound;500,000 refurbishment of an existing &lsquo;chamber&rsquo;. Yearsley Group also provides Young&rsquo;s Seafood Limited with a dedicated shuttle service, feeding materials into its three Grimsby manufacturing facilities and collecting finished frozen products for storage and distribution to retailers.</p>\n<p>\n	Scott Barker, Distribution Manager at Young&rsquo;s Seafood Limited, said: &ldquo;We have stored frozen materials and products with Yearsley Group for over a decade and made use of their vendor management service, NetStock on-line information system, and dedicated deliveries. They provide a very efficient and responsive service and, now they have capacity for non-frozen storage and distribution, it makes sense for us to consolidate all of our stock with one provider.&rdquo;</p>\n<p>\n	Having identified there is significant potential for growth within this sector, Yearsley Group is now analysing which other manufacturing customers could benefit from this additional service, and which of its other 11 cold stores across the UK could incorporate a similar ambient storage facility for dry goods.</p>\n<p>\n	Tim Moran, Yearsley Group&rsquo;s logistics sales director, said: &ldquo;In the current market, manufacturers are looking for logistics providers who can provide a straightforward service which reduces management time and increases efficiency. The one-stop-shop solution which Yearsley Group has created, allows manufacturers to deal with a single supplier for all of their storage and distribution requirements. We can develop a bespoke service which ensures real time updates about stock levels, dedicated collection and delivery, and access to the wider Yearsley Group network of cold stores and customers which includes the UK&rsquo;s biggest retailers and catering providers.</p>\n<p>\n	&ldquo;This extension of service provision has been welcomed by several existing and new customers who are now storing their non-frozen raw materials and packaging with us. We are extremely pleased to be able to include Young&rsquo;s Seafood Ltd among them and to be providing this additional solution for one of the UK&rsquo;s largest food manufacturers. Our next step is to identify where else and for whom this solution might be able to provide significant customer benefits.&rdquo;</p>', 'none', 'Recently-launched ‘ambient store’ secures major contract', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(57, 1, 2, '', NULL, '<p>\n	Arctic conditions for Blue Peter presenters&rsquo; challenge.</p>\n<br />\n<p>\n	It was just another day at the office for Blue Peter&rsquo;s young adventurer Helen Skelton when she joined presenting partner Barney Harwood for a frozen filming session in Manchester this week. The pair donned thermal suits for the 30-second segment, filmed at Yearsley Group&rsquo;s 165,000 sq ft cold store in Heywood, which forms part of a three-part challenge to test a traditional hard back book against a state-of-the-art e-reader.</p>\n<p>\n	Both items were placed in the giant cold store over night, where temperatures dropped to minus 22oc, and were retrieved the following day by the presenters who then attempted to read excerpts from the children&rsquo;s classic Alice in Wonderland. On hand to help set up the experiment was Yearsley Heywood&rsquo;s cold store manager Ian Wantling.</p>\n<p>\n	Ian said: &ldquo;The challenge was between the presenters, Barney who thinks e-readers are great and Helen, who prefers a good old fashioned book. Ours was one of three challenges, the others involved who was the fastest to buy a copy and which would survive being run over by a tank. It was pretty clear to which one won our challenge as it was Helen leaving the cold store with the biggest smile on her face, but we don&rsquo;t know how the other two challenges got on so we&rsquo;ll have to watch the show ourselves to find out.&rdquo;</p>\n<p>\n	Helen Skelton is no stranger to filming in extreme conditions, having recently completed the BBC Sport relief challenge to reach the South Pole by ski, kite-ski and snow-bike, covering an incredible 500 miles in just 18 days.</p>\n<p>\n	Ian added: &ldquo;We made sure everyone was well wrapped up and put the presenters in our special cold store thermals, including salopettes, padded jackets, gloves and hats. I think Barney was pretty chilly but Helen&rsquo;s just back from the Antarctic where it was minus 40oc so everyone was joking that it was almost warm for her at only minus 22oc. But she was as glad as anyone when we got back to the office for a warm cup of tea. They were both really friendly and even stopped to chat to the staff who were on shift and sign autographs for some of the children who had come in to see them, as they&rsquo;re all big Blue Peter fans.&rdquo;</p>\n<p>\n	This is the second time the BBC has called upon the cooling powers of Yearsley Group&rsquo;s cold stores. Last year the One Show demonstrated the formation of frost crystals with slow motion photography at Yearsley&rsquo;s Bristol warehouse.</p>', 'none', 'Arctic conditions for Blue Peter presenters’ challenge.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '6789949956_8261de0731_b1.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(58, 1, 2, '', NULL, '<p>\n	Logistics specialist to create new &lsquo;super hub&rsquo; in North West.</p>\n<br />\n<p>\n	Yearsley Group is to start work on the expansion of its Heywood headquarters to meet demand for new contracts. Construction is set to begin in June in order to provide storage facilities for new customers coming on line later in the year.</p>\n<p>\n	The 13.6 acre site will accommodate a 160,000 sq ft unit which will be built and fitted out in three phases. Trafford-based Russells Construction has been appointed to carry out the works which will see the first two phases built this year (comprising 100,000 sq ft, or 27,000 pallet spaces) with the remaining floor space and fit out completed in line with future demand.</p>\n<p>\n	Planning was secured in 2011 for the new unit at the Hareshill Distribution Park, immediately adjacent to the Group&rsquo;s national headquarters which currently has a 40,000 pallet capacity. The new cold store will ultimately provide a further 40,000 pallet spaces, doubling Heywood&rsquo;s capacity and enabling the company to create a northern &lsquo;superhub&rsquo; where stock will be consolidated for more efficient delivery into the UK&#39;s major retailers and food service organisations.</p>\n<p>\n	The new Heywood expansion will amount to a total investment of around &pound;20million, creating upto 150 new jobs and securing employment for the existing 300 members of staff at Heywood, the majority of which come from within a 10 mile radius.</p>\n<p>\n	Harry Yearsley, managing director of Yearsley Group, said: "The Heywood cold store is currently near to full capacity so the expansion of our operation is in direct response to the demands of new and existing customers for whom this North West location is ideal. It will become the first of two national &#39;super hubs&#39; for Yearsley Group, enabling us to consolidate more stock in single, strategically located stores, thereby increasing efficiency and reducing carbon output."</p>\n<p>\n	"The cold storage and distribution market has developed over recent years and customers at both ends of the supply chain are demanding faster, more efficient, environmentally conscious services from their logistics partners. The extension of Heywood will ultimately provide us with a frozen storage capacity of more than 80,000 pallets in one North West location, helping us to better meet the storage needs of manufacturing customers and the delivery requirements of retail and food service customers across the country."</p>\n<p>\n	Yearsley Group invested heavily in making Heywood the centre of its UK operation in 1999 as it was not only ideal for logistics distribution with good links to the M66 and M62, but had room for expansion. The new steel frame building will be similar in style to the neighbouring facility, providing a floor space of around 160,000 sq ft and built to the environmental rating BREEAM &#39;Very Good&#39;. The building will be superinsluated, to help reduce energy use, and will include rainwater harvesting system to feed the cooling systems and save on fresh water usage. The plan includes 111 car parking spaces plus HGV parking. Planning was secured on behalf of the Yearsley Group by advisors Indigo Planning Ltd.</p>\n<p>\n	The second &#39;superhub&#39; will be strategically located to complement the North West operation and other &#39;regional&#39; depots, and sites in the East and Southern regions are currently being considered. This consolidation strategy forms part of Yearsley Group&rsquo;s forward plan which has seen the recent launch of the Global Logistics platform (including freight forwarding) and provision of an ambient storage and distribution offering from its Grimsby depot, providing additional services for existing and new customers.</p>', 'none', 'Logistics specialist to create new ‘super hub’ in North West', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', 'Harry-Yearsley-plus-HGV.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(59, 1, 2, '', NULL, '<p>\n	Yearsley Group&rsquo;s acquisition adding value for customers.</p>\n<br />\n<p>\n	IcePak, the seafood sales division of the Yearsley Group, has launched a trio of new product lines, offering customers a guaranteed supply of three of the most popular products for wholesalers. The 777-branded block cooked and peeled prawns, a range of prepared baby squid and a variety of King Prawns have all been sourced direct from their suppliers in Indonesia and India, following an international product search by IcePak buyers.</p>\n<p>\n	Sourcing direct from suppliers overseas will enable IcePak to guarantee not only the availability of stock, but also the quality of product, and that of the logistics and supply chain which utilises Yearsley&rsquo;s long-established cold storage operation. IcePak is also going direct to source for a number of other products which are already part of the portfolio, providing similar guarantees for tuna, tilapia, pangasius, swordfish and cuttlefish.</p>\n<p>\n	Ian King, IcePak Director, explains: &ldquo;Quality of product and reliability of stock are the two most important elements in our business and we are always looking for ways to make improvements. In the past we were reliant on 3rd parties which caused variations in supply and price and could be frustrating from a customer&rsquo;s point of view and for us.</p>\n<p>\n	&ldquo;By going direct for the most popular ranges, we can be sure of the provenance of the product and check the suppliers&rsquo; own credentials are up to the exacting standards IcePak expects. We can improve the lines of communication to ensure supply and, ultimately, cut out any uncertainty over the delivery of stock by using our own transport and logistics operation. This also allows us to provide customers with price stability and product traceability.&rdquo;</p>\n<p>\n	Since its acquisition by Yearsley&rsquo;s in January 2011, frozen seafood specialist IcePak has gone from strength to strength. Now fully integrated with Yearsley Group&rsquo;s IT and management operations, IcePak is also able to take advantage of Yearsley Group&rsquo;s huge transport and storage network, and has expanded its customer base by offering more than 300 additional product lines to Yearsley Food Sales&rsquo; existing customer base.</p>\n<p>\n	Ian King added: &ldquo;One year on from the acquisition, IcePak continues to grow. Not only is IcePak building on its own customer base, it is also offering additional frozen seafood products to Yearsley Food Sales existing clients, providing them with all their frozen food needs.&rdquo;</p>\n<p>\n	777 is available in 100/200 to 300/500 prawns per pound<br />\n	King Prawns available from 21/25 to 61/70 prawns per pound<br />\n	Prepared baby squid is available in 10/20 to 21/40 per pound</p>', 'none', 'Yearsley Group’s acquisition adding value for customers.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', 'prawn-stirfry_1.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(60, 1, 2, '', NULL, '<p>\n	The man at the helm of cold storage and food sales giant the Yearsley Group tells all on how a successful family business can be run.</p>\n<p>\n	To read full article <a href="http://www.insidermedia.com/productsandservices/archive/nwbi/june-2012/interview-harry-yearsley/index.html" target="_blank">click here</a>.</p>', 'none', 'The man at the helm of cold storage and food sales giant the Yearsley Group tells all on how a successful family business can be run.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(61, 1, 2, '', NULL, '<p>\n	Ground breaking ceremony as work starts on new Heywood "Super Hub"</p>\n<p>\n	Yearsley Group managing director Harry Yearsley has welcomed Rochdale&#39;s Mayor and Mayoress, Councillor James Gartside and Councillor Jane Gartside, to help celebrate the official &#39;ground breaking&#39; as work begins on the group&#39;s &pound;20million expansion in Heywood.</p>\n<p>\n	The 13.6 acre site, adjacent to the group&#39;s headquarters at the Hareshill Distribution Park, will accommodate a 160,000sq ft unit to satisfy the needs of a growing customer base and increased demand from existing customers. Up to 150 new jobs will be created within the local community.</p>\n<p>\n	Harry Yearsley, managing director of Yearsley Group said: "This ground breaking ceremony marks an exciting time for Yearsley, as we start work on the new cold store. It&#39;s paramount that we react to the demands of new and existing customers; hence the expansion plans within our operation. On completion, Heywood will become the first of two national &#39;Super Hubs&#39; for Yearsley Group, enabling us to consolidate more stock in single, strategically located stores, thereby increasing efficiency and reducing carbon output."</p>\n<p>\n	The project will be built and fitted out in three phases by Trafford-based Russells Construction; with an anticipated deadline of early 2013 for the first two phases. The new cold store will provide a further 40,000 pallet capacity, on top of the current 40,000 capacity - doubling Heywood&#39;s capacity and enabling the company to create its northern &#39;Super Hub&#39; where stock will be consolidated for more efficient delivery into the UK&#39;s major retailers and food service organisations.</p>\n<p>\n	The new steel frame building will be similar in style to the neighbouring headquarters facility and will be super insulated, to help reduce energy use. The plans also include rainwater harvesting system to feed the cooling systems and save on fresh water usage.</p>\n<p>\n	John Hudson, Chief Executive of Rochdale Development Agency, said: "It is great to see another major investment underway in the borough, this time at Hareshill Business Park in Heywood. This important expansion for the Yearsley Group not only creates up to 150 new jobs but secures the employment of the existing 300 members of staff, the majority of which come from within a 10 mile radius."</p>', 'none', 'Ground breaking ceremony as work starts on new Heywood "Super Hub"', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', 'Yearsley-07.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(62, 1, 2, '', NULL, '<p>\n	Centrally located cold store takes pallet capacity to 320,000.</p>\n<p>\n	Yearsley Group has added a 200,000 sq ft cold storage facility in the Midlands to its portfolio as the company seeks to improve its service offering to retailers. Agent Barton Kendal, acting on behalf of Yearsley Group, has agreed the purchase of the automated cold store at the Hams Hall Distribution Park, Coleshill, close to many of the UK&#39;s biggest retail National Distribution Centres (NDCs).</p>\n<p>\n	Housing more than 42,000 pallet spaces, the state-of-the-art cold store is ideally located for the M6, M6 toll, M5, M42 and M40. It will not only provide ample opportunities for new customers, but will also support Yearsley Group&#39;s existing Midlands depots including Coleshill - which is at full capacity - Holmewood and Belle Eau Park, which is dedicated to servicing the Brakes Brothers catering contract.</p>\n<p>\n	Harry Yearsley, managing director, said: "We are actively looking to grow our business and we have been looking for an opportunity in the Midlands which would give us greater storage capacity and support the strategic aims of growing our food sales division and servicing a larger retail customer base."</p>\n<p>\n	"The location of this cold store could not be better for consolidating stock from around the UK and abroad, and delivering into many of the leading retailers NDCs. Consolidation of stock from manufacturers not only makes for more efficient delivery into major retail and food service organisations, it also contributes to reducing carbon output."</p>\n<p>\n	With the addition of Hams Hall, Yearsley&#39;s national pallet capacity will exceed 320,000. The acquisition comes just weeks after Yearsley Group began work on the expansion of its Heywood headquarters to create a northern &#39;superhub&#39; to meet demand for new contracts, which will increase capacity by a further 40,000 to 80,000 pallets by next year.</p>', 'none', 'Centrally located cold store takes pallet capacity to 320,000.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(63, 1, 2, '', NULL, '<p>\n	Further investment in roof-mounted solar panel installations.</p>\n<p>\n	Yearsley Group, the UK&rsquo;s largest cold storage and distribution provider, have installed solar panels at a further five of its cold stores in England. These installations bring the group&rsquo;s total use to 8,997 panels across 7 of its 13 depots, generating 1,753,000 kilowatt hours per annum with a total investment of &pound;3.5 million.</p>\n<p>\n	This will help to reduce the Group&rsquo;s reliance on the national grid and forms part of an on-going strategy by the company to increase the energy it obtains from renewable sources by 2015, in line with Yearsley Group&rsquo;s commitment to carbon reduction and sustainability. With a national network of cold stores, Yearsley Group also has plans to install other renewable solutions like wind power at depots around the UK.</p>\n<p>\n	Harry Yearsley, managing director of Yearsley Group, said: &ldquo;Cold storage by its nature requires a large energy input, so it makes sense to use the large exposed surface area of the warehouses to generate electricity. This also helps us to meet our carbon reduction commitments as part of our ISO 14001 environmental accreditation. Sustainability is very important within our business and carbon reduction is vital in both our warehouse and logistics operations. We have already implemented new management systems for reducing food miles and updated many of the cold stores with state-of-the-art insulation, but we know we can more and are heavily investing in moving this forward across our entire network of depots.&rdquo;</p>', 'none', 'Further investment in roof-mounted solar panel installations.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', 'Pg-front--Solar-Installation.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(64, 1, 2, '', NULL, '<p>\n	Yearsley Group has now formally purchased the business and assets of Partner Logistics Hams Hall Ltd following the operation of the business on licence from July. The Group has also acquired the freehold of the Hams Hall warehouse from Ropemaker Properties Ltd. This now secures Yearsley Group&rsquo;s commitment to the future of Hams Hall and the clients that utilise the depot.</p>\n<p>\n	Yearsley Group added the 200,000 sq ft automated cold storage facility at Coleshill in the Midlands to its portfolio to continue improving its service offering to retailers. Having run the business for 3 months, existing clients have seen a seamless transition to the Yearsley Group ownership. New clients have been attracted to the 42,000 pallet, state of the art cold store due to its location close to 5 major motorways and the service offering from Yearsley Group.</p>\n<p>\n	Harry Yearsley, managing director, said: &ldquo;The location of this cold store could not be better for consolidating stock from around the UK and abroad, and delivering into many of the leading retailers NDCs. Consolidation of stock from manufacturers not only makes for more efficient delivery into major retail and food service organisations, it also contributes to reducing carbon output. We are forecasting that Hams Hall will be 60% full by March 2013&rdquo;</p>\n<p>\n	With the addition of Hams Hall, Yearsley Group now has 13 regional depots, a national pallet capacity exceeding 320,000 and a fleet of more than 300 temperature-controlled vehicles.</p>\n<p>\n	The acquisition comes 3 months after Yearsley Group began work on the expansion of its Heywood headquarters to create a northern &lsquo;superhub&rsquo; to meet demand for new contracts, which will increase capacity by a further 40,000 to 80,000 pallets by next year.</p>', 'none', 'Yearsley Group has now formally purchased the business and assets of Partner Logistics Hams Hall Ltd following the operation of the business on licence from July. ', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '2012.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(65, 1, 2, '', NULL, '<p>\n	Yearsley Group has been announced as the preferred UK provider of frozen logistics for CSM, the world&rsquo;s largest bakery supplier. Following a competitive tender, the 10,000 pallet contract will see CSM store more than 300 finished frozen products with Yearsley Group, including a variety of brands of cookies, muffins, brownies and doughnuts from November 2012.</p>\n<p>\n	All collections will be from CSM&rsquo;s factory at Bromborough on the Wirral and stored at Yearsley Group&rsquo;s Heywood cold store before being distributed to retailers nationwide.</p>\n<p>\n	Tim Moran, Cold Storage &amp; Distribution Sales Director at Yearsley Group said: &ldquo;We&rsquo;re delighted to have secured this contact to provide CSM with all of its UK frozen storage requirements. As with all of our customers, we will assign a dedicated customer service team and develop a bespoke collection and delivery programme that fits in with both CSM and its customers&rsquo; needs. Although we will consolidate CSM stock at Heywood, our national network of cold stores will allow us distribute it quickly and effectively anywhere in the UK.</p>\n<p>\n	We were already working closely with CSM, having won the contract for their business with Tesco in Ireland and Cookie Man business from our Coleshill depot. This allowed us to demonstrate our service and expertise and opened the door to bringing on board their entire storage operation.&rdquo; Consolidating stock from CSM&rsquo;s Bromborough site at the Heywood store also provides CSM access to Yearsley Group&rsquo;s picking and repacking facilities. A section of the cold store will be dedicated to order picking for CSM customers, while the repack department will offer new packing formats to be considered.</p>\n<p>\n	CSM, formerly known as BakeMark in the UK, is a global supplier of bakery products and employs more than 9,700 people around the world. It operates six sites in the UK, and supplies finished products to most of the UK&rsquo;s major multiples and catering organisations. Danny Martin, European Planning &amp; Logistics Manager from CSM added: &ldquo;As the world&rsquo;s largest bakery products supplier, our logistics partner plays an enormously important role in our operations, so it was critical for us to find the right team. Yearsley Group are a proven quality service provider who satisfied our requirement for providing deliveries into our customers 24/7, meeting extremely short lead times, and providing additional services such as repacking and order picking which will benefit our customers greatly.&rdquo;</p>\n<p>\n	The deal is one of several new contracts for Yearsley Group, creating demand for new space within the Group&rsquo;s storage operation. Work is well underway on the expansion of its Heywood headquarters, with the first 100,000 sq ft of a 160,000 sq ft unit being constructed within the next year, boosting pallet capacity by 27,000. Ultimately the new cold store will double Heywood&rsquo;s existing 40,000 pallet capacity and enabling the company to create its northern &lsquo;superhub&rsquo; where stock will be consolidated from a wide range of different suppliers, for more efficient delivery. This consolidation strategy forms part of Yearsley Group&rsquo;s forward plan which has seen the recent launch of the Global Logistics platform (including freight forwarding) and provision of an ambient storage and distribution offering from its Grimsby depot, providing additional services for existing and new customers.</p>', 'none', 'Yearsley Group has been announced as the preferred UK provider of frozen logistics for CSM, the world’s largest bakery supplier. ', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', 'Yearsley-Logistics-wins-CSM-contract.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(66, 1, 2, '', NULL, '<p>\n	QU: What do twenty 10 year olds, a time capsule and a frozen fish have in common? ANS: They were all involved in a School&rsquo;s visit to Yearsley Group&rsquo;s head office.</p>\n<p>\n	Harwood Park School, in Heywood, contacted frozen food sales and logistic company, Yearsley Group, to see if they could look round the cold store as part of their class project on hot and cold. Yearsley Group obliged, inviting teacher Helen Law and 20 Year 5 children to their boardroom for a presentation about the company. This was followed by a visit onto the loading bay where the children looked into a refrigerated vehicle and then watched the contents being unloading, temperature checked and moved into the cold store. The children followed the pallet&rsquo;s journey, being greeted with temperatures of -25 degrees once in the cold store: which at first they didn&rsquo;t think was that cold but after a few minutes they decided it definitely was!!</p>\n<p>\n	The children also looked at and handled some of the products sold by Yearsley Group&rsquo;s food sales division. They particularly liking pulling the legs off the squid and picking the icing off donuts &ndash; KIDS!!</p>\n<p>\n	The visit was completed with the burial of a Time Capsule in the grounds of Yearsley Groups &pound;20m extension. The extension, which is being built by Russells Construction, is adjacent to the head office and will create the company&rsquo;s first &ldquo;Superhub&rdquo;. It will have a total capacity of 40,000 pallet sites upon completion with 13,000 pallets sites available from May 2013 and the remainder completed and fitted out in line with future demand. The Time Capsule burial was the finale of an eventful morning with the contents having been suggested by Yearsley Group employees and Harwood Park children. Almost 50 items were placed in the capsule including the day&rsquo;s newspaper, a supermarket till receipt, a photo of the school and school badge, some Yearsley literature, toys of the day and some &ldquo;broken&rdquo; phones and gaming equipment. The morning culminated in one of the children interrupting Managing Director, Harry Yearsley half way through his speech to ask &ldquo;can we bury it yet&rdquo;.</p>\n<p>\n	The children were asked to do one last job as they left and that was to return in December 2062 to celebrate the opening of the capsule!</p>\n<p>\n	Afterwards, Harry Yearsley commented &ldquo;Working with schools is just one way of building relationships with local people and is part of our commitment to keeping everyone informed about what&rsquo;s happening on their doorstep. While the children are probably too young to be discussing their future career choices, this visit has perhaps given them an early taster of the types of opportunities available to them in the future.&rdquo;</p>\n<p>\n	<br />\n	To view the cover letter and contents list for the Time Capsule <a href="{filedir_1}Time_Capsulet_Cover_Sheet1.pdf" target="_blank">click here</a></p>', 'none', 'QU: What do twenty 10 year olds, a time capsule and a frozen fish have in common? ANS: They were all involved in a School’s visit to Yearsley Group’s head office.', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', 'Invite-School-Children_3.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(67, 1, 4, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(68, 1, 1, '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(69, 1, 1, '<p>\n	Reprehenderit nisi cursus! Nonummy maecenas molestiae. Lorem, hymenaeos asperiores, eaque molestie mattis. Cupiditate facilisis tempora eveniet congue laborum, maiores porro? Dui! Commodi placeat, facere, aliqua! Facilisi nibh, optio, cupidatat adipiscing.</p>\n<p>\n	Arcu voluptatum nisl adipiscing mi mollit auctor itaque tempore? Dolore, pulvinar! Mollit pariatur, etiam nam urna. Omnis voluptatum porta atque, maxime ullamco, tortor! Cum! Voluptatibus tempus potenti reprehenderit. Gravida deleniti.</p>\n<p>\n	Mi pulvinar voluptatibus aliquip fames, fugit lectus sollicitudin, ante do class, per! Ut phasellus laoreet mus, molestias, harum eius etiam doloribus culpa, fuga accusamus. Possimus aperiam. Quia alias sequi. Ipsum.</p>\n<p>\n	Minima sunt tempore beatae dolor massa beatae cillum erat porta arcu! Labore, rutrum magna fusce? Rhoncus facilisi aliqua cras risus dolores ullamcorper sapien vero, congue massa fames. Dolor officia in.</p>\n<p>\n	Volutpat adipisicing felis risus dictumst iusto ullamcorper? Ab, quod lobortis hac sollicitudin, ipsum occaecat sollicitudin vehicula voluptate curabitur enim venenatis, quaerat! Aliquam ab tempor a ex pulvinar maxime dignissimos occaecati.</p>\n<p>\n	Rhoncus venenatis rutrum ab arcu, class, semper. Hendrerit facilisi libero accusamus, natus, ullamco convallis augue do, expedita integer pede ratione quidem, semper vulputate odit cras commodi molestiae platea proident molestiae.</p>\n<p>\n	Similique integer placerat malesuada, nunc asperiores egestas? Voluptates illo minim tenetur minus, dictum, gravida. Odio ipsa quidem euismod, dolorem saepe. Aute non recusandae dis, doloremque! Hymenaeos tempus numquam! Turpis aspernatur.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(70, 1, 1, '<p>\n	Please find below details on cookies used on this website:</p>\n<p>\n	&nbsp;</p>\n<table align="left" border="1" cellpadding="5" cellspacing="1" dir="ltr" id="directory" style="width: 680px;">\n	<thead>\n		<tr align="left">\n			<th scope="col" width="180">\n				Cookie name</th>\n			<th bgcolor="#eeeeee" scope="col" width="250">\n				Owner</th>\n			<th scope="col" width="200">\n				Description</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td width="180">\n				__utma</td>\n			<td bgcolor="#eeeeee" width="250">\n				Google Analytics</td>\n			<td width="200">\n				Lets Google Analytics know when you return to a website so we can see how many users come back to our site.</td>\n		</tr>\n		<tr>\n			<td width="180">\n				__utmc</td>\n			<td bgcolor="#eeeeee" width="250">\n				Google Analytics</td>\n			<td width="200">\n				Used in conjunction with the utma cookie in some older versions of the Google Analytics tracking code.</td>\n		</tr>\n		<tr>\n			<td width="180">\n				__utmb</td>\n			<td bgcolor="#eeeeee" width="250">\n				Google Analytics</td>\n			<td width="200">\n				Used during a single visit to a website so we can see which pages individual users looked at in that visit. It expires after 30 minutes and is completely anonymous.</td>\n		</tr>\n		<tr>\n			<td width="180">\n				__utmz</td>\n			<td bgcolor="#eeeeee" width="250">\n				Google Analytics</td>\n			<td width="200">\n				Used to identify which site you came from so, for example, we can measure the usefulness of the links we place on certain other sites.</td>\n		</tr>\n		<tr>\n			<td width="180">\n				exp_last_activity</td>\n			<td bgcolor="#eeeeee" width="250">\n				&nbsp;</td>\n			<td width="200">\n				Every time the page is reloaded the last activity is set to the current date and time. Used to determine expiry. This is a requirement for logged in users, but not for general visitors. It is set for both.</td>\n		</tr>\n		<tr>\n			<td width="180">\n				exp_tracker</td>\n			<td bgcolor="#eeeeee" width="250">\n				&nbsp;</td>\n			<td width="200">\n				Tracks the last 5 pages viewed by the user, and is used primarily for redirection after logging in etc. This is a requirement for logged in users, but not for general visitors. It is set for both.</td>\n		</tr>\n		<tr>\n			<td width="180">\n				&nbsp;</td>\n			<td bgcolor="#eeeeee" width="250">\n				&nbsp;</td>\n			<td width="200">\n				&nbsp;</td>\n		</tr>\n	</tbody>\n</table>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(71, 1, 10, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(72, 1, 11, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '{filedir_1}Nov2012.pdf', 'none', 'Newsletter November 2012 Edition', 'none', '', 'none'),
(73, 1, 11, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '{filedir_1}Dec2012.pdf', 'none', 'Newsletter December 2012 Edition', 'none', '', 'none');
INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_2`, `field_ft_2`, `field_id_3`, `field_ft_3`, `field_id_4`, `field_ft_4`, `field_id_5`, `field_ft_5`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_11`, `field_ft_11`, `field_id_12`, `field_ft_12`, `field_id_13`, `field_ft_13`, `field_id_14`, `field_ft_14`, `field_id_15`, `field_ft_15`, `field_id_16`, `field_ft_16`, `field_id_17`, `field_ft_17`, `field_id_18`, `field_ft_18`, `field_id_19`, `field_ft_19`, `field_id_20`, `field_ft_20`, `field_id_21`, `field_ft_21`, `field_id_22`, `field_ft_22`, `field_id_23`, `field_ft_23`, `field_id_24`, `field_ft_24`, `field_id_25`, `field_ft_25`, `field_id_26`, `field_ft_26`) VALUES
(74, 1, 2, '', NULL, '<p>\n	Yearsley Group has unveiled a complete re branding for all its business divisions, including new logos and vehicle livery. New websites including a new corporate site will be rolled out throughout the first half of 2013 to support the rebrand.</p>\n<p>\n	The brand re-launch was undertaken for several reasons; To modernise the logo and corporate image of the group as we move forward, without totally moving away from our heritage; retaining the strong impact our vehicles currently have on the road. One brand image across our vehicles, literature and electronic media, which could be adopted across both trading divisions &ndash; Food and Logistics.</p>\n<p>\n	Yearsley Logistics now incorporates all aspects of the supply chain including frozen, ambient and freight forwarding and this is what will now be displayed on our vehicles and stores. Yearsley Food incorporates Belfield, Yearsley Food Sales, IcePak and Lucky Red companies, with the logos being changed in line with the group theme.</p>\n<p>\n	Commenting on the re-brand, Managing Director, Harry Yearsley said; &ldquo;It has been another exciting period as the company continues to grow and expand, despite the current financial climate. The last 12 months has seen the acquisition of Hams Hall, the building of the 40,000 pallet expansion at Heywood as well as the recent additions to our fleet of 300+ vehicles. This re-branding activity is another way in which we are taking the company into the future, re-affirming our position as one of the UK&rsquo;s leading frozen food distributors and logistics providers&rdquo;.</p>', 'none', 'Yearsley Group has unveiled a complete re branding for all its business divisions, including new logos and vehicle livery. ', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'new-identity-news.jpg', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(76, 1, 1, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt, nisl vel feugiat aliquet, odio lacus congue risus, at lobortis sapien massa in orci. Cras in leo mauris, a interdum mauris. Vivamus egestas, augue at tempus laoreet, ante nibh porta mauris, ut blandit eros ligula id ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras quis metus in diam venenatis dapibus. Nulla facilisi. Sed sollicitudin nulla quam, luctus tincidunt nisi. Sed ultricies, odio non tristique sollicitudin, risus justo cursus odio, sit amet rhoncus erat nulla sit amet mi.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', NULL, '', NULL, '', 'none'),
(77, 1, 1, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt, nisl vel feugiat aliquet, odio lacus congue risus, at lobortis sapien massa in orci. Cras in leo mauris, a interdum mauris. Vivamus egestas, augue at tempus laoreet, ante nibh porta mauris, ut blandit eros ligula id ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras quis metus in diam venenatis dapibus. Nulla facilisi. Sed sollicitudin nulla quam, luctus tincidunt nisi. Sed ultricies, odio non tristique sollicitudin, risus justo cursus odio, sit amet rhoncus erat nulla sit amet mi.</p>', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '<p>\n	Working with us</p>\n<p>\n	Want to work for a Nationwide company with a proven track record of success?</p>\n<p>\n	Want a job that offers both security and progression?</p>\n<p>\n	Then apply now&hellip;.</p>', 'none', '', 'none', '', 'none', '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', NULL, '', NULL, '', 'none'),
(78, 1, 1, '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', NULL, '', NULL, '', 'none'),
(79, 1, 1, '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', NULL, '', NULL, '', 'none'),
(80, 1, 1, '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', NULL, '', NULL, '', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_entries_autosave`
--

CREATE TABLE IF NOT EXISTS `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_fields`
--

CREATE TABLE IF NOT EXISTS `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `exp_channel_fields`
--

INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`) VALUES
(1, 1, 1, 'page_copy', 'Page Copy', '', 'wygwam', '', '0', 0, 0, 'channel', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(2, 1, 2, 'news_story', 'News Story', '', 'wygwam', '', '0', 0, 0, 'channel', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(3, 1, 2, 'news_snippet', 'News Snippet', '', 'textarea', '', '0', 0, 0, 'channel', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(4, 1, 3, 'work_type', 'Work Type', '', 'text', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(5, 1, 3, 'salary', 'Salary', '', 'text', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(6, 1, 3, 'job_location', 'Job Location', '', 'text', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(7, 1, 3, 'careers_copy', 'Careers Copy', '', 'wygwam', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(8, 1, 3, 'intro_copy', 'Intro Copy', '', 'textarea', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(9, 1, 4, 'directors', 'Directors', '', 'matrix', '', '0', 0, 0, 'channel', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO3M6MjoiMTAiO2k6MTtzOjE6IjUiO2k6MjtzOjE6IjYiO319'),
(18, 1, 4, 'md_details', 'MD Details', '', 'matrix', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtzOjE6IjkiO2k6MTtzOjE6IjciO2k6MjtzOjE6IjgiO319'),
(11, 1, 1, 'generic_image', 'Generic Image', '', 'assets', '', '0', 0, 0, 'channel', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YToxMDp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czoxOiIxIjt9czo1OiJtdWx0aSI7czoxOiJ5IjtzOjQ6InZpZXciO3M6NjoidGh1bWJzIjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO31zOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(12, 1, 1, 'website_link', 'Website Link', '', 'text', '', '0', 0, 0, 'channel', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(13, 1, 1, 'homepage_section_images', 'Homepage Section Images', '', 'matrix', '', '0', 0, 0, 'channel', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjQ6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO2k6MjtzOjE6IjMiO2k6MztzOjE6IjQiO319'),
(14, 1, 1, 'side_panel', 'Side Panel', '', 'wygwam', '', '0', 0, 0, 'channel', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(15, 1, 1, 'side_images', 'Side Images', '', 'assets', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czo0OiJlZToyIjt9czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToieSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(16, 1, 1, 'careers_side_text', 'Careers Side Text', '', 'wygwam', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(17, 1, 1, 'cser_images', 'CSER Images', '', 'assets', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czo0OiJlZToyIjt9czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToieSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(19, 1, 5, 'date_range', 'date-range', '', 'matrix', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtpOjExO2k6MTtpOjEyO2k6MjtpOjEzO319'),
(20, 1, 3, 'upload', 'File Upload', '', 'file', '', '0', 0, 0, 'channel', 0, '0', '0', 0, 0, 0, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjE6IjEiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(21, 1, 2, 'news_image', 'News Image', '', 'assets', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6Im4iO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(22, 1, 1, 'pdf_text', 'PDF Text', 'Text for side PDF panel', 'text', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 9, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(23, 1, 1, 'pdf_upload', 'PDF Upload', 'Allows you to upload a PDF to support the PDF side panel', 'file', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjE6IjEiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(24, 1, 6, 'newsletter_pdf_upload', 'Newsletter PDF upload', 'PDF upload for News Newsletters', 'file', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(25, 1, 6, 'newsletter_pdf_text', 'Newsletter PDF Text', 'Text for Newsletter PDF upload', 'text', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(26, 1, 1, 'careers_vacancy_text', 'Careers Vacancy Text', 'Text that sits above the main careers listings. Can be used to explain that no jobs are available for example.', 'wygwam', '', '0', 0, 0, 'channel', 6, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 11, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_member_groups`
--

INSERT INTO `exp_channel_member_groups` (`group_id`, `channel_id`) VALUES
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_titles`
--

CREATE TABLE IF NOT EXISTS `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `exp_channel_titles`
--

INSERT INTO `exp_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `pentry_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `dst_enabled`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`) VALUES
(1, 1, 1, 1, 0, NULL, '127.0.0.1', 'About Us', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358500758, 'n', '2013', '01', '18', 0, 0, 20130213142919, 0, 0),
(2, 1, 7, 1, 0, NULL, '127.0.0.1', 'Our Business', 'our-business', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358511795, 'n', '2013', '01', '18', 0, 0, 20130225124416, 0, 0),
(3, 1, 8, 1, 0, NULL, '192.168.90.119', 'Corporate, Social and Environmental Responsibility at Yearsley', 'cser-pledge', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358529970, 'n', '2013', '01', '18', 0, 0, 20130219180811, 0, 0),
(4, 1, 1, 1, 0, NULL, '127.0.0.1', 'Latest News', 'news', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358530269, 'n', '2013', '01', '18', 0, 0, 20130219132010, 0, 0),
(5, 1, 9, 1, 0, NULL, '192.168.90.119', 'Careers', 'careers', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358530222, 'n', '2013', '01', '18', 0, 0, 20130219141323, 0, 0),
(6, 1, 1, 1, 0, NULL, '127.0.0.1', 'Contact Us', 'contact-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358530457, 'n', '2013', '01', '18', 0, 0, 20130121144018, 0, 0),
(7, 1, 1, 1, 0, NULL, '127.0.0.1', 'Business Structure', 'business-structure', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358937048, 'n', '2013', '01', '23', 0, 0, 20130206171849, 0, 0),
(8, 1, 1, 1, 0, NULL, '127.0.0.1', 'Directors', 'directors', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1358939221, 'n', '2013', '01', '23', 0, 0, 20130225122002, 0, 0),
(9, 1, 1, 1, 0, NULL, '127.0.0.1', 'Locations', 'locations', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358939395, 'n', '2013', '01', '23', 0, 0, 20130207110356, 0, 0),
(10, 1, 4, 1, 0, NULL, '127.0.0.1', 'Harry Yearsley', 'harry-yearsley', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358939820, 'n', '2013', '01', '23', 0, 0, 20130204154901, 0, 0),
(11, 1, 4, 1, 0, NULL, '127.0.0.1', 'Jonathan Baker', 'jonathan-baker', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358940620, 'n', '2013', '01', '23', 0, 0, 20130204154921, 0, 0),
(12, 1, 4, 1, 0, NULL, '127.0.0.1', 'Phil Whitworth', 'phil-whitworth', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358940697, 'n', '2013', '01', '23', 0, 0, 20130204154938, 0, 0),
(13, 1, 4, 1, 0, NULL, '127.0.0.1', 'Ian King', 'ian-king', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358940721, 'n', '2013', '01', '23', 0, 0, 20130204155002, 0, 0),
(14, 1, 4, 1, 0, NULL, '127.0.0.1', 'Tim Moran', 'tim-moran', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358940741, 'n', '2013', '01', '23', 0, 0, 20130204155022, 0, 0),
(15, 1, 4, 1, 0, NULL, '127.0.0.1', 'Mark Haslam', 'mark-haslam', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358940759, 'n', '2013', '01', '23', 0, 0, 20130204155040, 0, 0),
(16, 1, 1, 1, 0, NULL, '109.111.197.225', 'History', 'history', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358940916, 'n', '2013', '01', '23', 0, 0, 20130212170717, 0, 0),
(18, 1, 3, 1, 0, NULL, '192.168.90.119', 'Account Manager', 'account-manager', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1358943953, 'n', '2013', '01', '23', 0, 0, 20130219141354, 0, 0),
(19, 1, 1, 1, 0, NULL, '127.0.0.1', 'Yearsley Group', 'yearsley-group', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1359040699, 'n', '2013', '01', '24', 0, 0, 20130124161819, 0, 0),
(22, 1, 3, 1, 0, NULL, '192.168.90.119', 'Project Manager', 'project-manager', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1359128822, 'n', '2013', '01', '25', 0, 0, 20130222123903, 0, 0),
(23, 1, 8, 1, 0, NULL, '192.168.90.119', 'Causes we Support', 'causes-we-support', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359416142, 'n', '2013', '01', '29', 0, 0, 20130218154143, 0, 0),
(24, 1, 8, 1, 0, NULL, '109.111.197.225', 'Our Commitment to the Environment', 'our-commitment-to-the-environment', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359440404, 'n', '2013', '01', '29', 0, 0, 20130306151505, 0, 0),
(25, 1, 7, 1, 0, NULL, '127.0.0.1', 'Logistics', 'logistics', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359443138, 'n', '2013', '01', '29', 0, 0, 20130218110439, 0, 0),
(26, 1, 7, 1, 0, NULL, '127.0.0.1', 'Food Sales', 'food-sales', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359450825, 'n', '2013', '01', '29', 0, 0, 20130218110446, 0, 0),
(27, 1, 7, 1, 0, NULL, '127.0.0.1', 'Belfield', 'belfield', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359450890, 'n', '2013', '01', '29', 0, 0, 20130213152951, 0, 0),
(28, 1, 7, 1, 0, NULL, '192.168.90.119', 'IcePak', 'icepak', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359450795, 'n', '2013', '01', '29', 0, 0, 20130220093716, 0, 0),
(29, 1, 7, 1, 0, NULL, '192.168.90.119', 'Lucky Red', 'lucky-red', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359450851, 'n', '2013', '01', '29', 0, 0, 20130220093512, 0, 0),
(30, 1, 1, 1, 0, NULL, '127.0.0.1', 'Archive', 'archive', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359652258, 'n', '2013', '01', '31', 0, 0, 20130131171359, 0, 0),
(31, 1, 1, 1, 0, NULL, '127.0.0.1', 'Thanks for contacting us!', 'thank-you', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359676486, 'n', '2013', '02', '01', 0, 0, 20130219151947, 0, 0),
(32, 1, 5, 1, 0, NULL, '127.0.0.1', 'Home', 'the-yearsley-group-of-companies', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1359972943, 'n', '2013', '02', '04', 0, 0, 20130225124344, 0, 0),
(68, 1, 1, 1, 0, NULL, '127.0.0.1', 'Newsletters', 'newsletters', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1360318046, 'n', '2013', '02', '08', 0, 0, 20130208110726, 0, 0),
(43, 1, 2, 1, 0, NULL, '109.111.197.225', '15 Million Hot Cross Buns were in Yearsley Group''s Stores This Easter', '15-million-hot-cross-buns-were-in-yearsley-groups-stores-this-easter', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1270901236, 'n', '2010', '04', '10', 0, 0, 20100410130716, 0, 0),
(44, 1, 2, 1, 0, NULL, '109.111.197.225', 'Belfields New Mr Kings Range Is Ruling The Dessert Market', 'belfields-new-mr-kings-range-is-ruling-the-dessert-market', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1271765411, 'n', '2010', '04', '20', 0, 0, 20100420131011, 0, 0),
(45, 1, 2, 1, 0, NULL, '109.111.197.225', 'Yearsley Group Invest £0.5m at Grimsby Depot', 'yearsley-group-invest-0.5m-at-grimsby-depot', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1293970330, 'n', '2011', '01', '02', 0, 0, 20110102131210, 0, 0),
(46, 1, 2, 1, 0, NULL, '109.111.197.225', '£20m expansion plan for Yearsley Group at Heywood', '20m-expansion-plan-for-yearsley-group-at-heywood', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1294402426, 'n', '2011', '01', '07', 0, 0, 20130205134847, 0, 0),
(47, 1, 2, 1, 0, NULL, '109.111.197.225', 'Yearsley Group acquires seafood specialist Ice Pak', 'yearsley-group-acquires-seafood-specialist-ice-pak', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1298463239, 'n', '2011', '02', '23', 0, 0, 20110223131359, 0, 0),
(48, 1, 2, 1, 0, NULL, '109.111.197.225', 'New logistics specialist for Yearsley Group', 'new-logistics-specialist-for-yearsley-group', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1305461855, 'n', '2011', '05', '15', 0, 0, 20110515131735, 0, 0),
(49, 1, 2, 1, 0, NULL, '109.111.197.225', 'Yearsley Group supports manufacturers with BFFF awards sponsorship', 'yearsley-group-supports-manufacturers-with-bfff-awards-sponsorship', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1307103594, 'n', '2011', '06', '03', 0, 0, 20110603131954, 0, 0),
(50, 1, 2, 1, 0, NULL, '109.111.197.225', 'Yearsley Group wins new education supply contract', 'yearsley-group-wins-new-education-supply-contract', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1308745298, 'n', '2011', '06', '22', 0, 0, 20110622132138, 0, 0),
(51, 1, 2, 1, 0, NULL, '109.111.197.225', 'New ambient storage offering from frozen food specialist', 'new-ambient-storage-offering-from-frozen-food-specialist', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1310646295, 'n', '2011', '07', '14', 0, 0, 20110714132455, 0, 0),
(52, 1, 2, 1, 0, NULL, '109.111.197.225', 'Yearsley Group switches on renewable energy', 'yearsley-group-switches-on-renewable-energy', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1297941953, 'n', '2011', '02', '17', 0, 0, 20110217122553, 0, 0),
(53, 1, 2, 1, 0, NULL, '109.111.197.225', 'New trucks to help Yearsley Group reduce emissions', 'new-trucks-to-help-yearsley-group-reduce-emissions', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1325161744, 'n', '2011', '12', '29', 0, 0, 20111229132904, 0, 0),
(54, 1, 2, 1, 0, NULL, '109.111.197.225', 'Agreement with Tesco gives Yearsley customers ''a little help''', 'agreement-with-tesco-gives-yearsley-customers-a-little-help', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1326198617, 'n', '2012', '01', '10', 0, 0, 20120110133017, 0, 0),
(55, 1, 2, 1, 0, NULL, '192.168.90.119', 'Yearsley Group''s going global', 'yearsley-groups-going-global', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1330000372, 'n', '2012', '02', '23', 0, 0, 20130221121753, 0, 0),
(56, 1, 2, 1, 0, NULL, '109.111.197.225', 'Young’s to store dry goods with Yearsley Group', 'youngs-to-store-dry-goods-with-yearsley-group', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1331642027, 'n', '2012', '03', '13', 0, 0, 20120313133347, 0, 0),
(57, 1, 2, 1, 0, NULL, '192.168.90.119', 'Freeze frame with Yearsley Group', 'freeze-frame-with-yearsley-group', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1331383254, 'n', '2012', '03', '10', 0, 0, 20130221120655, 0, 0),
(58, 1, 2, 1, 0, NULL, '192.168.90.119', 'Yearsley Group to start £20m expansion at Heywood', 'yearsley-group-to-start-20m-expansion-at-heywood', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1335790912, 'n', '2012', '04', '30', 0, 0, 20130221123453, 0, 0),
(59, 1, 2, 1, 0, NULL, '192.168.90.119', 'New products and direct sourcing from IcePak', 'new-products-and-direct-sourcing-from-icepak', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1337432752, 'n', '2012', '05', '19', 0, 0, 20130221121553, 0, 0),
(60, 1, 2, 1, 0, NULL, '109.111.197.225', 'Harry Yearsley talks with Insider Magazine', 'harry-yearsley-talks-with-insider-magazine', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1338555987, 'n', '2012', '06', '01', 0, 0, 20120601140627, 0, 0),
(61, 1, 2, 1, 0, NULL, '192.168.90.119', 'Yearsley Group starts £20m expansion at Heywood', 'yearsley-group-starts-20m-expansion-at-heywood', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1342012181, 'n', '2012', '07', '11', 0, 0, 20130221123342, 0, 0),
(62, 1, 2, 1, 0, NULL, '109.111.197.225', 'Yearsley Group takes on new Midlands depot', 'yearsley-group-takes-on-new-midlands-depot', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1342271450, 'n', '2012', '07', '14', 0, 0, 20120714141050, 0, 0),
(63, 1, 2, 1, 0, NULL, '192.168.90.119', 'Yearsley Group £3.5 million investment in renewable energy', 'yearsley-group-3.5-million-investment-in-renewable-energy', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346850720, 'n', '2012', '09', '05', 0, 0, 20130221121501, 0, 0),
(64, 1, 2, 1, 0, NULL, '192.168.90.119', 'Yearsley Group acquire business, assets and building at Hams Hall.', 'yearsley-group-acquire-business-assets-and-building-at-hams-hall', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349183567, 'n', '2012', '10', '02', 0, 0, 20130221154948, 0, 0),
(65, 1, 2, 1, 0, NULL, '192.168.90.119', 'Yearsley Logistics wins CSM contract', 'yearsley-logistics-wins-csm-contract', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1352121292, 'n', '2012', '11', '05', 0, 0, 20130218141653, 0, 0),
(66, 1, 2, 1, 0, NULL, '192.168.90.119', 'Yearsley Group Invite School Children to Time Capsule Burial at Frozen “Superhub”', 'yearsley-group-invite-school-children-to-time-capsule-burial-at-frozen-supe', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356009582, 'n', '2012', '12', '20', 0, 0, 20130218144943, 0, 0),
(67, 1, 4, 1, 0, NULL, '127.0.0.1', 'Directors', 'directors', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1360164798, 'n', '2013', '02', '06', 0, 0, 20130207123519, 0, 0),
(69, 1, 1, 1, 0, NULL, '192.168.90.119', 'Terms and conditions', 'terms-and-conditions', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1360598722, 'n', '2013', '02', '11', 0, 0, 20130225115723, 0, 0),
(70, 1, 1, 1, 0, NULL, '192.168.90.119', 'Cookie Usage', 'cookie-usage', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1360599126, 'n', '2013', '02', '11', 0, 0, 20130225115507, 0, 0),
(71, 1, 10, 1, 0, NULL, '109.111.197.225', '1952-1986', '1952-1986', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360688695, 'n', '2013', '02', '12', 0, 0, 20130212180455, 0, 0),
(72, 1, 11, 1, 0, NULL, '192.168.90.119', 'November 2012', 'newsletter-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1361275927, 'n', '2013', '02', '19', 0, 0, 20130219134808, 0, 0),
(73, 1, 11, 1, 0, NULL, '192.168.90.119', 'December 2012', 'newsletter-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1361276031, 'n', '2013', '02', '19', 0, 0, 20130219134752, 0, 0),
(74, 1, 2, 1, 0, NULL, '192.168.90.119', 'New brand identity for UK’s leading frozen food sales and logistics operator', 'new-brand-identity-for-uks-leading-frozen-food-sales-and-logistics-operator', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1361280753, 'n', '2013', '02', '19', 0, 0, 20130219134134, 0, 0),
(76, 1, 1, 1, 0, NULL, '127.0.0.1', 'Thanks for signing up!', 'thank-you-newsletter', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1361286323, 'n', '2013', '02', '19', 0, 0, 20130225165024, 0, 0),
(77, 1, 1, 1, 0, NULL, '127.0.0.1', 'Thanks for applying!', 'careers-thankyou', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1361288451, 'n', '2013', '02', '19', 0, 0, 20130225122852, 0, 0),
(78, 1, 1, 1, 0, NULL, '127.0.0.1', 'Netstock Login', 'netstock-login', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1361378918, 'n', '2013', '02', '20', 0, 0, 20130220165339, 0, 0),
(79, 1, 1, 1, 0, NULL, '127.0.0.1', 'Employee Login', 'employee-login', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1361534733, 'n', '2013', '02', '22', 0, 0, 20130222120834, 0, 0),
(80, 1, 1, 1, 0, NULL, '127.0.0.1', 'Sitemap', 'sitemap', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1361788491, 'n', '2013', '02', '25', 0, 0, 20130225113451, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_comments`
--

CREATE TABLE IF NOT EXISTS `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_comment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_log`
--

CREATE TABLE IF NOT EXISTS `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=145 ;

--
-- Dumping data for table `exp_cp_log`
--

INSERT INTO `exp_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`) VALUES
(1, 1, 1, 'admin', '127.0.0.1', 1355938962, 'Logged in'),
(2, 1, 1, 'admin', '127.0.0.1', 1355996087, 'Logged in'),
(3, 1, 1, 'admin', '127.0.0.1', 1355997186, 'Site Updated&nbsp;&nbsp;Yearsley Group'),
(4, 1, 1, 'admin', '127.0.0.1', 1357740876, 'Logged in'),
(5, 1, 1, 'admin', '127.0.0.1', 1358268613, 'Logged in'),
(6, 1, 1, 'admin', '127.0.0.1', 1358269295, 'Site Created&nbsp;&nbsp;Yearsley Logistics'),
(7, 2, 1, 'admin', '127.0.0.1', 1358269887, 'Logged in'),
(8, 1, 1, 'admin', '127.0.0.1', 1358332063, 'Logged in'),
(9, 1, 1, 'admin', '127.0.0.1', 1358332146, 'Site Created&nbsp;&nbsp;Yearsley Food Sales'),
(10, 1, 1, 'admin', '127.0.0.1', 1358344517, 'Logged in'),
(11, 2, 1, 'admin', '127.0.0.1', 1358351933, 'Logged in'),
(12, 1, 1, 'admin', '127.0.0.1', 1358358065, 'Logged in'),
(13, 1, 1, 'admin', '127.0.0.1', 1358499509, 'Logged in'),
(14, 1, 1, 'admin', '127.0.0.1', 1358500319, 'Channel Created:&nbsp;&nbsp;Generic'),
(15, 1, 1, 'admin', '127.0.0.1', 1358511704, 'Logged in'),
(16, 1, 1, 'admin', '127.0.0.1', 1358517353, 'Logged in'),
(17, 1, 1, 'admin', '127.0.0.1', 1358529637, 'Logged in'),
(18, 1, 1, 'admin', '127.0.0.1', 1358530412, 'Channel Created:&nbsp;&nbsp;News'),
(19, 1, 1, 'admin', '127.0.0.1', 1358530457, 'Channel Created:&nbsp;&nbsp;Careers'),
(20, 1, 1, 'admin', '127.0.0.1', 1358775296, 'Logged in'),
(21, 1, 1, 'admin', '127.0.0.1', 1358778920, 'Logged in'),
(22, 1, 1, 'admin', '127.0.0.1', 1358788122, 'Logged in'),
(23, 1, 1, 'admin', '127.0.0.1', 1358788242, 'Logged out'),
(24, 1, 1, 'admin', '127.0.0.1', 1358788335, 'Logged in'),
(25, 1, 1, 'admin', '127.0.0.1', 1358854157, 'Logged in'),
(26, 1, 1, 'admin', '127.0.0.1', 1358854342, 'Field Group Created:&nbsp;Fields'),
(27, 1, 1, 'admin', '127.0.0.1', 1358866336, 'Logged in'),
(28, 1, 1, 'admin', '127.0.0.1', 1358873734, 'Logged out'),
(29, 1, 1, 'admin', '127.0.0.1', 1358933960, 'Logged in'),
(30, 1, 1, 'admin', '127.0.0.1', 1358939540, 'Channel Created:&nbsp;&nbsp;Directors'),
(31, 1, 1, 'admin', '127.0.0.1', 1358942179, 'Field Group Created:&nbsp;News Fields'),
(32, 1, 1, 'admin', '127.0.0.1', 1358943392, 'Field Group Created:&nbsp;Careers Fields'),
(33, 1, 1, 'admin', '127.0.0.1', 1358956988, 'Logged out'),
(34, 1, 1, 'admin', '127.0.0.1', 1359040438, 'Logged in'),
(35, 1, 1, 'admin', '127.0.0.1', 1359040567, 'Field Group Created:&nbsp;Director Fields'),
(36, 1, 1, 'admin', '127.0.0.1', 1359047871, 'Logged out'),
(37, 1, 1, 'admin', '127.0.0.1', 1359106029, 'Logged in'),
(38, 1, 1, 'admin', '127.0.0.1', 1359113020, 'Logged in'),
(39, 1, 1, 'admin', '127.0.0.1', 1359118059, 'Logged in'),
(40, 1, 1, 'admin', '127.0.0.1', 1359120777, 'Logged out'),
(41, 1, 1, 'admin', '127.0.0.1', 1359124297, 'Logged in'),
(42, 1, 1, 'admin', '127.0.0.1', 1359403968, 'Logged in'),
(43, 1, 1, 'admin', '127.0.0.1', 1359414575, 'Logged in'),
(44, 1, 1, 'admin', '127.0.0.1', 1359439156, 'Logged in'),
(45, 1, 1, 'admin', '127.0.0.1', 1359449884, 'Logged in'),
(46, 1, 1, 'admin', '127.0.0.1', 1359458483, 'Logged out'),
(47, 1, 1, 'admin', '127.0.0.1', 1359531650, 'Logged in'),
(48, 1, 1, 'admin', '127.0.0.1', 1359538769, 'Logged out'),
(49, 1, 1, 'admin', '127.0.0.1', 1359652132, 'Logged in'),
(50, 1, 1, 'admin', '127.0.0.1', 1359658415, 'Logged in'),
(51, 1, 1, 'admin', '127.0.0.1', 1359672626, 'Logged in'),
(52, 1, 1, 'admin', '127.0.0.1', 1359676001, 'Logged out'),
(53, 1, 1, 'admin', '127.0.0.1', 1359676171, 'Logged in'),
(54, 1, 1, 'admin', '127.0.0.1', 1359708134, 'Logged in'),
(55, 1, 1, 'admin', '87.114.97.89', 1359708270, 'Logged in'),
(56, 1, 1, 'admin', '127.0.0.1', 1359971468, 'Logged in'),
(57, 1, 1, 'admin', '127.0.0.1', 1359971476, 'Logged in'),
(58, 1, 1, 'admin', '127.0.0.1', 1359971610, 'Logged in'),
(59, 1, 1, 'admin', '127.0.0.1', 1359971622, 'Logged in'),
(60, 1, 1, 'admin', '127.0.0.1', 1359971757, 'Logged in'),
(61, 1, 1, 'admin', '127.0.0.1', 1359972576, 'Channel Created:&nbsp;&nbsp;Homepage'),
(62, 1, 1, 'admin', '127.0.0.1', 1359973092, 'Member Group Created:&nbsp;&nbsp;Editors'),
(63, 1, 1, 'admin', '127.0.0.1', 1359981671, 'Channel Created:&nbsp;&nbsp;Business Structure'),
(64, 1, 1, 'admin', '127.0.0.1', 1359982961, 'Channel Created:&nbsp;&nbsp;Our Businesses'),
(65, 1, 1, 'admin', '127.0.0.1', 1359989727, 'Channel Created:&nbsp;&nbsp;CSER'),
(66, 1, 1, 'admin', '127.0.0.1', 1359991304, 'Channel Created:&nbsp;&nbsp;Careers Intro'),
(67, 1, 1, 'admin', '109.111.197.225', 1359998853, 'Logged in'),
(68, 1, 1, 'admin', '109.111.197.225', 1360064389, 'Logged in'),
(69, 1, 1, 'admin', '109.111.197.225', 1360065401, 'Logged in'),
(70, 1, 1, 'admin', '109.111.197.225', 1360083934, 'Logged in'),
(71, 1, 1, 'admin', '109.111.197.225', 1360084251, 'Logged out'),
(72, 1, 1, 'admin', '109.111.197.225', 1360156397, 'Logged in'),
(73, 1, 1, 'admin', '127.0.0.1', 1360162948, 'Logged in'),
(74, 1, 1, 'admin', '127.0.0.1', 1360164853, 'Custom Field Deleted:&nbsp;Director Copy'),
(75, 1, 1, 'admin', '127.0.0.1', 1360235006, 'Logged in'),
(76, 1, 1, 'admin', '127.0.0.1', 1360254236, 'Logged out'),
(77, 1, 1, 'admin', '127.0.0.1', 1360255611, 'Logged in'),
(78, 1, 1, 'admin', '127.0.0.1', 1360317996, 'Logged in'),
(79, 1, 1, 'admin', '127.0.0.1', 1360325201, 'Logged out'),
(80, 1, 1, 'admin', '127.0.0.1', 1360335092, 'Logged in'),
(81, 1, 1, 'admin', '127.0.0.1', 1360342388, 'Logged out'),
(82, 1, 1, 'admin', '127.0.0.1', 1360586253, 'Logged in'),
(83, 1, 1, 'admin', '192.168.90.119', 1360586339, 'Logged in'),
(84, 1, 1, 'admin', '127.0.0.1', 1360599113, 'Logged out'),
(85, 1, 1, 'admin', '127.0.0.1', 1360599137, 'Logged in'),
(86, 1, 1, 'admin', '109.111.197.225', 1360602628, 'Logged in'),
(87, 1, 1, 'admin', '109.111.197.225', 1360663402, 'Logged in'),
(88, 1, 1, 'admin', '88.98.36.214', 1360676566, 'Logged in'),
(89, 1, 1, 'admin', '88.98.36.214', 1360683742, 'Logged out'),
(90, 1, 1, 'admin', '109.111.197.225', 1360688420, 'Logged in'),
(91, 1, 1, 'admin', '109.111.197.225', 1360688441, 'Channel Created:&nbsp;&nbsp;Timeline'),
(92, 1, 1, 'admin', '109.111.197.225', 1360688457, 'Field Group Created:&nbsp;Timeline'),
(93, 1, 1, 'admin', '127.0.0.1', 1360755216, 'Logged in'),
(94, 1, 1, 'admin', '127.0.0.1', 1360837761, 'Logged in'),
(95, 1, 1, 'admin', '127.0.0.1', 1360862202, 'Logged in'),
(96, 1, 1, 'admin', '127.0.0.1', 1360920794, 'Logged in'),
(97, 1, 1, 'admin', '127.0.0.1', 1361180518, 'Logged in'),
(98, 1, 1, 'admin', '192.168.90.119', 1361195693, 'Logged in'),
(99, 1, 1, 'admin', '127.0.0.1', 1361206648, 'Logged out'),
(100, 1, 1, 'admin', '127.0.0.1', 1361275411, 'Logged in'),
(101, 1, 1, 'admin', '127.0.0.1', 1361275503, 'Channel Created:&nbsp;&nbsp;News Page'),
(102, 1, 1, 'admin', '127.0.0.1', 1361275565, 'Field Group Created:&nbsp;News Page Fields'),
(103, 1, 1, 'admin', '192.168.90.119', 1361280687, 'Logged in'),
(104, 1, 1, 'admin', '192.168.90.119', 1361288333, 'Logged in'),
(105, 1, 1, 'admin', '192.168.90.119', 1361293488, 'Logged in'),
(106, 1, 1, 'admin', '192.168.90.119', 1361352878, 'Logged in'),
(107, 1, 1, 'admin', '127.0.0.1', 1361359960, 'Logged in'),
(108, 1, 1, 'admin', '192.168.90.119', 1361360147, 'Logged out'),
(109, 1, 1, 'admin', '127.0.0.1', 1361378219, 'Logged in'),
(110, 1, 1, 'admin', '192.168.90.119', 1361441512, 'Logged in'),
(111, 1, 1, 'admin', '192.168.90.119', 1361448126, 'Logged in'),
(112, 1, 1, 'admin', '192.168.90.119', 1361457204, 'Logged out'),
(113, 1, 1, 'admin', '192.168.90.119', 1361461685, 'Logged in'),
(114, 1, 1, 'admin', '127.0.0.1', 1361530372, 'Logged in'),
(115, 1, 1, 'admin', '192.168.90.119', 1361536733, 'Logged in'),
(116, 1, 1, 'admin', '192.168.90.119', 1361536748, 'Logged out'),
(117, 1, 1, 'admin', '192.168.90.119', 1361542648, 'Logged in'),
(118, 1, 1, 'admin', '127.0.0.1', 1361543997, 'Logged in'),
(119, 1, 1, 'admin', '127.0.0.1', 1361788138, 'Logged in'),
(120, 1, 1, 'admin', '192.168.90.119', 1361793069, 'Logged in'),
(121, 1, 1, 'admin', '192.168.90.109', 1361802525, 'Logged in'),
(122, 1, 1, 'admin', '127.0.0.1', 1361803499, 'Logged out'),
(123, 1, 1, 'admin', '127.0.0.1', 1361805931, 'Logged in'),
(124, 1, 1, 'admin', '127.0.0.1', 1361810849, 'Logged in'),
(125, 1, 1, 'admin', '192.168.90.119', 1361811528, 'Logged in'),
(126, 1, 1, 'admin', '127.0.0.1', 1361880085, 'Logged in'),
(127, 1, 1, 'admin', '127.0.0.1', 1362055063, 'Logged in'),
(128, 1, 1, 'admin', '109.111.197.225', 1362058513, 'Logged in'),
(129, 1, 1, 'admin', '109.111.197.225', 1362475737, 'Logged in'),
(130, 1, 1, 'admin', '109.111.197.225', 1362504175, 'Logged in'),
(131, 1, 1, 'admin', '109.111.197.225', 1362560285, 'Logged in'),
(132, 1, 1, 'admin', '109.111.197.225', 1362582871, 'Logged in'),
(133, 1, 1, 'admin', '109.111.197.225', 1362590018, 'Logged out'),
(134, 1, 1, 'admin', '109.111.197.225', 1362654521, 'Logged in'),
(135, 1, 1, 'admin', '109.111.197.225', 1362761704, 'Logged in'),
(136, 1, 1, 'admin', '109.111.197.225', 1362995945, 'Logged in'),
(137, 1, 1, 'admin', '109.111.197.225', 1363081001, 'Logged in'),
(138, 1, 1, 'admin', '109.111.197.225', 1363092218, 'Logged in'),
(139, 1, 1, 'admin', '109.111.197.225', 1363186436, 'Logged in'),
(140, 1, 1, 'admin', '109.111.197.225', 1363193603, 'Logged out'),
(141, 1, 1, 'admin', '109.111.197.225', 1363253971, 'Logged in'),
(142, 1, 1, 'admin', '109.111.197.225', 1363255238, 'Logged in'),
(143, 1, 1, 'admin', '109.111.197.225', 1363265789, 'Logged in'),
(144, 1, 1, 'admin', '109.111.197.225', 1363269924, 'Logged in');

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_search_index`
--

CREATE TABLE IF NOT EXISTS `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_developer_log`
--

CREATE TABLE IF NOT EXISTS `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_mg`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_ml`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_console_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_ping_status`
--

CREATE TABLE IF NOT EXISTS `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_versioning`
--

CREATE TABLE IF NOT EXISTS `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_extensions`
--

CREATE TABLE IF NOT EXISTS `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `exp_extensions`
--

INSERT INTO `exp_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`) VALUES
(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y'),
(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y'),
(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y'),
(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y'),
(5, 'Structure_ext', 'entry_submission_redirect', 'entry_submission_redirect', '', 10, '3.3.8', 'y'),
(6, 'Structure_ext', 'cp_member_login', 'cp_member_login', '', 10, '3.3.8', 'y'),
(7, 'Structure_ext', 'sessions_start', 'sessions_start', '', 10, '3.3.8', 'y'),
(8, 'Structure_ext', 'channel_module_create_pagination', 'channel_module_create_pagination', '', 9, '3.3.8', 'y'),
(9, 'Structure_ext', 'wygwam_config', 'wygwam_config', '', 10, '3.3.8', 'y'),
(10, 'Structure_ext', 'core_template_route', 'core_template_route', '', 10, '3.3.8', 'y'),
(11, 'Structure_ext', 'entry_submission_end', 'entry_submission_end', '', 10, '3.3.8', 'y'),
(12, 'Structure_ext', 'safecracker_submit_entry_end', 'safecracker_submit_entry_end', '', 10, '3.3.8', 'y'),
(13, 'Structure_ext', 'template_post_parse', 'template_post_parse', '', 10, '3.3.8', 'y'),
(14, 'Libraree_ext', 'process', 'sessions_start', 'a:4:{s:11:"license_key";s:36:"3fd07cb9-e5fa-499c-8697-a20b1cc4fa5f";s:4:"path";s:62:"/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/templates";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 1, '1.0.5', 'y'),
(15, 'Libraree_ext', 'process_check', 'cp_js_end', 'a:4:{s:11:"license_key";s:36:"3fd07cb9-e5fa-499c-8697-a20b1cc4fa5f";s:4:"path";s:62:"/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/templates";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 2, '1.0.5', 'y'),
(16, 'Libraree_ext', 'low_variables_delete', 'low_variables_delete', 'a:4:{s:11:"license_key";s:36:"3fd07cb9-e5fa-499c-8697-a20b1cc4fa5f";s:4:"path";s:62:"/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/templates";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 2, '1.0.5', 'y'),
(17, 'Assets_ext', 'channel_entries_query_result', 'channel_entries_query_result', '', 10, '2.0.4', 'y'),
(18, 'Freebie_ext', 'Freebie_ext', 'sessions_start', 'a:7:{s:9:"to_ignore";s:0:"";s:13:"ignore_beyond";s:7:"archive";s:14:"break_category";s:2:"no";s:14:"remove_numbers";s:2:"no";s:23:"always_parse_pagination";s:2:"no";s:12:"always_parse";s:0:"";s:13:"cache_cleared";s:3:"yes";}', 10, '0.2', 'y'),
(19, 'Updater_ext', 'cp_menu_array', 'cp_menu_array', 'a:0:{}', 100, '3.1.6', 'y'),
(20, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.5.3', 'y'),
(21, 'Field_editor_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0.3', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `exp_fieldtypes`
--

INSERT INTO `exp_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`) VALUES
(1, 'select', '1.0', 'YTowOnt9', 'n'),
(2, 'text', '1.0', 'YTowOnt9', 'n'),
(3, 'textarea', '1.0', 'YTowOnt9', 'n'),
(4, 'date', '1.0', 'YTowOnt9', 'n'),
(5, 'file', '1.0', 'YTowOnt9', 'n'),
(6, 'multi_select', '1.0', 'YTowOnt9', 'n'),
(7, 'checkboxes', '1.0', 'YTowOnt9', 'n'),
(8, 'radio', '1.0', 'YTowOnt9', 'n'),
(9, 'rel', '1.0', 'YTowOnt9', 'n'),
(10, 'rte', '1.0', 'YTowOnt9', 'n'),
(11, 'structure', '3.3.4', 'YTowOnt9', 'n'),
(12, 'wygwam', '2.7', 'YTowOnt9', 'y'),
(13, 'assets', '2.0.4', 'YTowOnt9', 'y'),
(14, 'matrix', '2.5.3', 'YTowOnt9', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_formatting`
--

CREATE TABLE IF NOT EXISTS `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `exp_field_formatting`
--

INSERT INTO `exp_field_formatting` (`formatting_id`, `field_id`, `field_fmt`) VALUES
(1, 1, 'none'),
(2, 1, 'br'),
(3, 1, 'xhtml'),
(4, 2, 'none'),
(5, 2, 'br'),
(6, 2, 'xhtml'),
(7, 3, 'none'),
(8, 3, 'br'),
(9, 3, 'xhtml'),
(10, 4, 'none'),
(11, 4, 'br'),
(12, 4, 'xhtml'),
(13, 5, 'none'),
(14, 5, 'br'),
(15, 5, 'xhtml'),
(16, 6, 'none'),
(17, 6, 'br'),
(18, 6, 'xhtml'),
(19, 7, 'none'),
(20, 7, 'br'),
(21, 7, 'xhtml'),
(22, 8, 'none'),
(23, 8, 'br'),
(24, 8, 'xhtml'),
(25, 9, 'none'),
(26, 9, 'br'),
(27, 9, 'xhtml'),
(54, 18, 'xhtml'),
(53, 18, 'br'),
(52, 18, 'none'),
(31, 11, 'none'),
(32, 11, 'br'),
(33, 11, 'xhtml'),
(34, 12, 'none'),
(35, 12, 'br'),
(36, 12, 'xhtml'),
(37, 13, 'none'),
(38, 13, 'br'),
(39, 13, 'xhtml'),
(40, 14, 'none'),
(41, 14, 'br'),
(42, 14, 'xhtml'),
(43, 15, 'none'),
(44, 15, 'br'),
(45, 15, 'xhtml'),
(46, 16, 'none'),
(47, 16, 'br'),
(48, 16, 'xhtml'),
(49, 17, 'none'),
(50, 17, 'br'),
(51, 17, 'xhtml'),
(55, 19, 'none'),
(56, 19, 'br'),
(57, 19, 'xhtml'),
(58, 20, 'none'),
(59, 20, 'br'),
(60, 20, 'xhtml'),
(61, 21, 'none'),
(62, 21, 'br'),
(63, 21, 'xhtml'),
(64, 22, 'none'),
(65, 22, 'br'),
(66, 22, 'xhtml'),
(67, 23, 'none'),
(68, 23, 'br'),
(69, 23, 'xhtml'),
(70, 24, 'none'),
(71, 24, 'br'),
(72, 24, 'xhtml'),
(73, 25, 'none'),
(74, 25, 'br'),
(75, 25, 'xhtml'),
(76, 26, 'none'),
(77, 26, 'br'),
(78, 26, 'xhtml');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_groups`
--

CREATE TABLE IF NOT EXISTS `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_field_groups`
--

INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Channel Fields'),
(2, 1, 'News Fields'),
(3, 1, 'Careers Fields'),
(4, 1, 'Director Fields'),
(5, 1, 'Timeline'),
(6, 1, 'News Newsletter Fields');

-- --------------------------------------------------------

--
-- Table structure for table `exp_files`
--

CREATE TABLE IF NOT EXISTS `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=134 ;

--
-- Dumping data for table `exp_files`
--

INSERT INTO `exp_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `credit`, `location`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`) VALUES
(1, 1, 'dir_hy.jpg', 1, '/Users/jonnyfrodsham/work/shoot_the_moon/yearsley/YE121101-Websites/www/yearsley/uploads/dir_hy.jpg', 'image/jpeg', 'dir_hy.jpg', 8383, NULL, NULL, NULL, 1, 1359405702, 1, 1359405702, '147 148'),
(2, 1, 'dir_jb.jpg', 1, '/Users/jonnyfrodsham/work/shoot_the_moon/yearsley/YE121101-Websites/www/yearsley/uploads/dir_jb.jpg', 'image/jpeg', 'dir_jb.jpg', 3916, NULL, NULL, NULL, 1, 1359407456, 1, 1359407456, '96 98'),
(3, 1, 'dir_pw.jpg', 1, '/Users/jonnyfrodsham/work/shoot_the_moon/yearsley/YE121101-Websites/www/yearsley/uploads/dir_pw.jpg', 'image/jpeg', 'dir_pw.jpg', 4382, NULL, NULL, NULL, 1, 1359407484, 1, 1359407484, '96 98'),
(4, 1, 'dir_ik.jpg', 1, '/Users/jonnyfrodsham/work/shoot_the_moon/yearsley/YE121101-Websites/www/yearsley/uploads/dir_ik.jpg', 'image/jpeg', 'dir_ik.jpg', 4371, NULL, NULL, NULL, 1, 1359407506, 1, 1359407506, '96 98'),
(5, 1, 'dir_tm.jpg', 1, '/Users/jonnyfrodsham/work/shoot_the_moon/yearsley/YE121101-Websites/www/yearsley/uploads/dir_tm.jpg', 'image/jpeg', 'dir_tm.jpg', 3790, NULL, NULL, NULL, 1, 1359407534, 1, 1359407534, '96 98'),
(6, 1, 'dir_mh.jpg', 1, '/Users/jonnyfrodsham/work/shoot_the_moon/yearsley/YE121101-Websites/www/yearsley/uploads/dir_mh.jpg', 'image/jpeg', 'dir_mh.jpg', 3771, NULL, NULL, NULL, 1, 1359407556, 1, 1359407556, '96 98'),
(7, 1, 'img_placeholder.jpg', 1, '/Users/jonnyfrodsham/work/shoot_the_moon/yearsley/YE121101-Websites/www/yearsley/uploads/img_placeholder.jpg', 'image/jpeg', 'img_placeholder.jpg', 1522, NULL, NULL, NULL, 1, 1359661344, 1, 1359661344, '129 128'),
(8, 1, 'content_img.jpg', 1, '/Users/jonnyfrodsham/work/shoot_the_moon/yearsley/YE121101-Websites/www/yearsley/uploads/content_img.jpg', 'image/jpeg', 'content_img.jpg', 1782, NULL, NULL, NULL, 1, 1359661491, 1, 1359661491, '194 211'),
(9, 1, 'home_food_banner.jpg', 2, '/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/images/content/Generic_Content_images/home_food_banner.jpg', 'image/jpeg', 'home_food_banner.jpg', 50483, NULL, NULL, NULL, 1, 1359976549, 1, 1361544304, '327 635'),
(10, 1, 'home_logistics_banner.jpg', 2, '/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/images/content/Generic_Content_images/home_logistics_banner.jpg', 'image/jpeg', 'home_logistics_banner.jpg', 54273, NULL, NULL, NULL, 1, 1359976572, 1, 1361544304, '327 635'),
(11, 1, 'about_img.jpg', 2, '/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/images/content/Generic_Content_images/about_img.jpg', 'image/jpeg', 'about_img.jpg', 8846, NULL, NULL, NULL, 1, 1359979390, 1, 1361544302, '151 212'),
(12, 1, 'accreditations.png', 2, '/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/images/content/Generic_Content_images/accreditations.png', 'image/png', 'accreditations.png', 19371, NULL, NULL, NULL, 1, 1359981583, 1, 1361544302, '169 152'),
(13, 1, 'img_placeholder.jpg', 2, '/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/images/content/Generic_Content_images/img_placeholder.jpg', 'image/jpeg', 'img_placeholder.jpg', 1522, NULL, NULL, NULL, 1, 1359983135, 1, 1361544304, '129 128'),
(14, 1, 'img_placeholder2.jpg', 2, '/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/images/content/Generic_Content_images/img_placeholder2.jpg', 'image/jpeg', 'img_placeholder2.jpg', 1539, NULL, NULL, NULL, 1, 1359985821, 1, 1361544304, '129 128'),
(15, 1, '559178_10151122573650921_1221548660_n1.jpg', 2, '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/images/content/Generic_Content_images/559178_10151122573650921_1221548660_n1.jpg', 'image/jpeg', '559178_10151122573650921_1221548660_n1.jpg', 64827, NULL, NULL, NULL, 1, 1360076735, 1, 1361544301, '200 238'),
(16, 1, 'Childrens_Hospital_Logo_4.jpg', 2, '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/images/content/Generic_Content_images/Childrens_Hospital_Logo_4.jpg', 'image/jpeg', 'Childrens_Hospital_Logo_4.jpg', 58445, NULL, NULL, NULL, 1, 1360076744, 1, 1361544301, '333 456'),
(17, 1, 'Pg_8_photo_(5)1.jpg', 2, '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/images/content/Generic_Content_images/Pg_8_photo_(5)1.jpg', 'image/jpeg', 'Pg_8_photo_(5)1.jpg', 60409, NULL, NULL, NULL, 1, 1360076745, 1, 1361544301, '333 248'),
(18, 1, 'photo4.JPG', 2, '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/images/content/Generic_Content_images/photo4.JPG', 'image/jpeg', 'photo4.JPG', 54583, NULL, NULL, NULL, 1, 1360076745, 1, 1361544305, '200 150'),
(19, 1, 'green_bear_at_hosp.jpg', 2, '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/images/content/Generic_Content_images/green_bear_at_hosp.jpg', 'image/jpeg', 'green_bear_at_hosp.jpg', 75198, NULL, NULL, NULL, 1, 1360076745, 1, 1361544303, '333 376'),
(20, 1, 'Final_Movember_mug_shots_003.jpg', 2, '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/images/content/Generic_Content_images/Final_Movember_mug_shots_003.jpg', 'image/jpeg', 'Final_Movember_mug_shots_003.jpg', 81855, NULL, NULL, NULL, 1, 1360076745, 1, 1361544301, '333 446'),
(21, 1, 'Time_Capsulet_Cover_Sheet.pdf', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/uploads/Time_Capsulet_Cover_Sheet.pdf', 'application/pdf', 'Time_Capsulet_Cover_Sheet.pdf', 176681, NULL, NULL, NULL, 1, 1360084075, 1, 1360084075, ' '),
(22, 1, 'Harry-and-Mayoress.jpg', 1, '/Users/martinsmith/Sites/yearsley-group/ye121101-websites/www/yearsley/uploads/Harry-and-Mayoress.jpg', 'image/jpeg', 'Harry-and-Mayoress.jpg', 20968, NULL, NULL, NULL, 1, 1360167328, 1, 1360167328, '299 250'),
(23, 1, 'home_pie_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_pie_banner.jpg', 'image/jpeg', 'home_pie_banner.jpg', 81372, NULL, NULL, NULL, 1, 1360921935, 1, 1361544304, '327 635'),
(24, 1, 'home_cranes_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_cranes_banner.jpg', 'image/jpeg', 'home_cranes_banner.jpg', 128776, NULL, NULL, NULL, 1, 1360921935, 1, 1361544303, '327 635'),
(25, 1, 'home_crates_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_crates_banner.jpg', 'image/jpeg', 'home_crates_banner.jpg', 107432, NULL, NULL, NULL, 1, 1360921935, 1, 1361544303, '327 635'),
(26, 1, 'home_prawn_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_prawn_banner.jpg', 'image/jpeg', 'home_prawn_banner.jpg', 70984, NULL, NULL, NULL, 1, 1360921935, 1, 1361544304, '327 635'),
(27, 1, 'home_prof_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_prof_banner.jpg', 'image/jpeg', 'home_prof_banner.jpg', 87780, NULL, NULL, NULL, 1, 1360921935, 1, 1361544304, '327 635'),
(28, 1, 'home_cake_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_cake_banner.jpg', 'image/jpeg', 'home_cake_banner.jpg', 66066, NULL, NULL, NULL, 1, 1360921935, 1, 1361544303, '327 635'),
(29, 1, 'home_storage_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_storage_banner.jpg', 'image/jpeg', 'home_storage_banner.jpg', 100485, NULL, NULL, NULL, 1, 1360921936, 1, 1361544304, '327 635'),
(131, 1, 'structure-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/structure-page.jpg', 'image/jpeg', 'structure-page.jpg', 100248, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '326 1280'),
(31, 1, 'logolink-logistics.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/logolink-logistics.jpg', 'image/jpeg', 'logolink-logistics.jpg', 18185, NULL, NULL, NULL, 1, 1361184375, 1, 1361544304, '139 278'),
(32, 1, 'logolink-foodsales.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/logolink-foodsales.jpg', 'image/jpeg', 'logolink-foodsales.jpg', 18998, NULL, NULL, NULL, 1, 1361185004, 1, 1361544304, '139 278'),
(33, 1, 'Commitment_Thumbnail.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Commitment_Thumbnail.jpg', 'image/jpeg', 'Commitment_Thumbnail.jpg', 567506, NULL, NULL, NULL, 1, 1361186482, 1, 1361544301, '1000 700'),
(34, 1, 'Lucky_Red_Thumbnail.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Lucky_Red_Thumbnail.jpg', 'image/jpeg', 'Lucky_Red_Thumbnail.jpg', 928406, NULL, NULL, NULL, 1, 1361186720, 1, 1361544301, '704 1000'),
(35, 1, 'Lucky-Red-Thumbnail.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Lucky-Red-Thumbnail.jpg', 'image/jpeg', 'Lucky-Red-Thumbnail.jpg', 17490, NULL, NULL, NULL, 1, 1361186829, 1, 1361544301, '128 128'),
(36, 1, 'Commitment-Thumbnail.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Commitment-Thumbnail.jpg', 'image/jpeg', 'Commitment-Thumbnail.jpg', 11077, NULL, NULL, NULL, 1, 1361186881, 1, 1361544301, '128 128'),
(40, 1, 'Invite-School-Children_3.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Invite-School-Children_3.jpg', 'image/jpeg', 'Invite-School-Children_3.jpg', 411512, NULL, NULL, NULL, 1, 1361196850, 1, 1361544301, '618 618'),
(41, 1, 'Yearsley-Logistics-wins-CSM-contract.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Yearsley-Logistics-wins-CSM-contract.jpg', 'image/jpeg', 'Yearsley-Logistics-wins-CSM-contract.jpg', 78610, NULL, NULL, NULL, 1, 1361197008, 1, 1361544302, '423 618'),
(42, 1, 'Time_Capsulet_Cover_Sheet1.pdf', 1, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/uploads/Time_Capsulet_Cover_Sheet1.pdf', 'application/pdf', 'Time_Capsulet_Cover_Sheet1.pdf', 176681, NULL, NULL, NULL, 1, 1361198442, 1, 1361198442, ' '),
(43, 1, 'cser.pdf', 1, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/uploads/cser.pdf', 'application/pdf', 'cser.pdf', 2841559, NULL, NULL, NULL, 1, 1361200574, 1, 1361200574, ' '),
(44, 1, '02._Environmental_Policy_SEMS13.pdf', 1, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/uploads/02._Environmental_Policy_SEMS13.pdf', 'application/pdf', '02._Environmental_Policy_SEMS13.pdf', 70646, NULL, NULL, NULL, 1, 1361201952, 1, 1361201952, ' '),
(45, 1, 'new-identity-news.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/new-identity-news.jpg', 'image/jpeg', 'new-identity-news.jpg', 58817, NULL, NULL, NULL, 1, 1361281289, 1, 1361544305, '430 618'),
(46, 1, 'Nov2012.pdf', 1, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/uploads/Nov2012.pdf', 'application/pdf', 'Nov2012.pdf', 889846, NULL, NULL, NULL, 1, 1361281498, 1, 1361281498, ' '),
(47, 1, 'Dec2012.pdf', 1, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/uploads/Dec2012.pdf', 'application/pdf', 'Dec2012.pdf', 949801, NULL, NULL, NULL, 1, 1361281571, 1, 1361281571, ' '),
(53, 1, '6789949956_8261de0731_b1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/6789949956_8261de0731_b1.jpg', 'image/jpeg', '6789949956_8261de0731_b1.jpg', 74056, NULL, NULL, NULL, 1, 1361448411, 1, 1361544301, '483 630'),
(50, 1, 'Heywood-Solar-Installation.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Heywood-Solar-Installation.jpg', 'image/jpeg', 'Heywood-Solar-Installation.jpg', 8642, NULL, NULL, NULL, 1, 1361297285, 1, 1361544301, '128 128'),
(51, 1, 'prawn-stirfry.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/prawn-stirfry.jpg', 'image/jpeg', 'prawn-stirfry.jpg', 7173, NULL, NULL, NULL, 1, 1361352906, 1, 1361544305, '129 128'),
(52, 1, 'Icepak-packaging-2_1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Icepak-packaging-2.jpg', 'image/jpeg', 'Icepak-packaging-2.jpg', 10532, NULL, NULL, NULL, 1, 1361353023, 1, 1361544301, '129 128'),
(54, 1, 'Hams-Hall-(2).jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Hams-Hall-(2).jpg', 'image/jpeg', 'Hams-Hall-(2).jpg', 85975, NULL, NULL, NULL, 1, 1361448657, 1, 1361544301, '443 630'),
(55, 1, 'Harry-Yearsley-plus-HGV.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Harry-Yearsley-plus-HGV.jpg', 'image/jpeg', 'Harry-Yearsley-plus-HGV.jpg', 61521, NULL, NULL, NULL, 1, 1361448808, 1, 1361544301, '370 640'),
(56, 1, 'Pg-front--Solar-Installation.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Pg-front--Solar-Installation.jpg', 'image/jpeg', 'Pg-front--Solar-Installation.jpg', 84254, NULL, NULL, NULL, 1, 1361448898, 1, 1361544301, '421 630'),
(57, 1, 'prawn-stirfry_1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/prawn-stirfry_1.jpg', 'image/jpeg', 'prawn-stirfry_1.jpg', 42862, NULL, NULL, NULL, 1, 1361448942, 1, 1361544305, '434 630'),
(58, 1, 'Yearsley-(31).jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Yearsley-(31).jpg', 'image/jpeg', 'Yearsley-(31).jpg', 103097, NULL, NULL, NULL, 1, 1361449069, 1, 1361544302, '630 630'),
(59, 1, 'Yearsley-07.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Yearsley-07.jpg', 'image/jpeg', 'Yearsley-07.jpg', 93880, NULL, NULL, NULL, 1, 1361450019, 1, 1361544302, '434 630'),
(61, 1, '2012_1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/2012.jpg', 'image/jpeg', '2012.jpg', 90050, NULL, NULL, NULL, 1, 1361461783, 1, 1361544301, '461 630'),
(66, 1, 'home_truckblue_banner_2.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_truckblue_banner.jpg', 'image/jpeg', 'home_truckblue_banner.jpg', 79566, NULL, NULL, NULL, 1, 1361543913, 1, 1361544304, '327 635'),
(63, 1, 'home_truckblue_banner_1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_truckblue_banner_1.jpg', 'image/jpeg', 'home_truckblue_banner_1.jpg', 79566, NULL, NULL, NULL, 1, 1361536916, 1, 1361544304, '327 635'),
(64, 1, 'home_icecream_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_icecream_banner.jpg', 'image/jpeg', 'home_icecream_banner.jpg', 100437, NULL, NULL, NULL, 1, 1361537171, 1, 1361544304, '327 635'),
(67, 1, 'Childrens Hospital Logo 4.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Childrens Hospital Logo 4.jpg', 'image/jpeg', 'Childrens Hospital Logo 4.jpg', 42281, NULL, NULL, NULL, 1, 1360077542, 1, 1360077542, '200 274'),
(68, 1, 'Final Movember mug shots 003.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Final Movember mug shots 003.jpg', 'image/jpeg', 'Final Movember mug shots 003.jpg', 49640, NULL, NULL, NULL, 1, 1360077544, 1, 1360077544, '200 268'),
(69, 1, 'Isoqar_logo.gif', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Isoqar_logo.gif', 'image/gif', 'Isoqar_logo.gif', 5025, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '125 145'),
(70, 1, 'Pg 8 photo (5)1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Pg 8 photo (5)1.jpg', 'image/jpeg', 'Pg 8 photo (5)1.jpg', 46142, NULL, NULL, NULL, 1, 1360077540, 1, 1360077540, '200 149'),
(71, 1, 'acc-bfff-logo.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/acc-bfff-logo.jpg', 'image/jpeg', 'acc-bfff-logo.jpg', 9549, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '100 107'),
(72, 1, 'acc-biff-logo.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/acc-biff-logo.jpg', 'image/jpeg', 'acc-biff-logo.jpg', 9549, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '100 107'),
(73, 1, 'acc-brc-logo.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/acc-brc-logo.jpg', 'image/jpeg', 'acc-brc-logo.jpg', 6124, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '100 78'),
(74, 1, 'acc-fsdf-logo.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/acc-fsdf-logo.jpg', 'image/jpeg', 'acc-fsdf-logo.jpg', 5270, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '87 107'),
(75, 1, 'acc-isoqar-logo.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/acc-isoqar-logo.jpg', 'image/jpeg', 'acc-isoqar-logo.jpg', 6704, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '100 105'),
(76, 1, 'acc-rha-logo.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/acc-rha-logo.jpg', 'image/jpeg', 'acc-rha-logo.jpg', 3857, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '87 78'),
(77, 1, 'acc-sts-logo.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/acc-sts-logo.jpg', 'image/jpeg', 'acc-sts-logo.jpg', 6529, NULL, NULL, NULL, 1, 1361357955, 1, 1361357955, '87 105'),
(78, 1, 'belfield3.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/belfield3.jpg', 'image/jpeg', 'belfield3.jpg', 8877, NULL, NULL, NULL, 1, 1360065298, 1, 1360065298, '129 128'),
(79, 1, 'belfield-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/belfield-page.jpg', 'image/jpeg', 'belfield-page.jpg', 155110, NULL, NULL, NULL, 1, 1360061462, 1, 1360061462, '326 1280'),
(80, 1, 'belfield4.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/belfield4.jpg', 'image/jpeg', 'belfield4.jpg', 9004, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '129 128'),
(81, 1, 'belfield1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/belfield1.jpg', 'image/jpeg', 'belfield1.jpg', 9960, NULL, NULL, NULL, 1, 1360065297, 1, 1360065297, '129 128'),
(82, 1, 'belfield5.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/belfield5.jpg', 'image/jpeg', 'belfield5.jpg', 8067, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '129 128'),
(83, 1, 'belfield2.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/belfield2.jpg', 'image/jpeg', 'belfield2.jpg', 7868, NULL, NULL, NULL, 1, 1360065298, 1, 1360065298, '129 128'),
(84, 1, 'brc_logo.gif', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/brc_logo.gif', 'image/gif', 'brc_logo.gif', 4300, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '125 145'),
(85, 1, 'bs_cold_storage.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/bs_cold_storage.jpg', 'image/jpeg', 'bs_cold_storage.jpg', 8357, NULL, NULL, NULL, 1, 1360061462, 1, 1360061462, '97 176'),
(86, 1, 'bs_logistics.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/bs_logistics.jpg', 'image/jpeg', 'bs_logistics.jpg', 7789, NULL, NULL, NULL, 1, 1360061462, 1, 1360061462, '97 176'),
(87, 1, 'cser1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser1.jpg', 'image/jpeg', 'cser1.jpg', 15495, NULL, NULL, NULL, 1, 1360078245, 1, 1360078245, '178 177'),
(88, 1, 'cser10.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser10.jpg', 'image/jpeg', 'cser10.jpg', 32829, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '128 128'),
(89, 1, 'cser11.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser11.jpg', 'image/jpeg', 'cser11.jpg', 21781, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '128 128'),
(90, 1, 'cser12.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser12.jpg', 'image/jpeg', 'cser12.jpg', 9699, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '129 128'),
(91, 1, 'career-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/career-page.jpg', 'image/jpeg', 'career-page.jpg', 48841, NULL, NULL, NULL, 1, 1360061463, 1, 1360061463, '326 1280'),
(92, 1, 'cser2.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser2.jpg', 'image/jpeg', 'cser2.jpg', 7520, NULL, NULL, NULL, 1, 1360078260, 1, 1360078260, '178 177'),
(93, 1, 'contact-us-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/contact-us-page.jpg', 'image/jpeg', 'contact-us-page.jpg', 38831, NULL, NULL, NULL, 1, 1360061463, 1, 1360061463, '326 1280'),
(94, 1, 'cser-page-alt.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser-page-alt.jpg', 'image/jpeg', 'cser-page-alt.jpg', 192059, NULL, NULL, NULL, 1, 1361180488, 1, 1361180488, '326 1280'),
(95, 1, 'cser-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser-page.jpg', 'image/jpeg', 'cser-page.jpg', 59249, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '326 1280'),
(96, 1, 'cser3.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser3.jpg', 'image/jpeg', 'cser3.jpg', 8372, NULL, NULL, NULL, 1, 1360078261, 1, 1360078261, '178 177'),
(97, 1, 'cser4.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser4.jpg', 'image/jpeg', 'cser4.jpg', 12370, NULL, NULL, NULL, 1, 1360078245, 1, 1360078245, '178 177'),
(98, 1, 'cser5.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser5.jpg', 'image/jpeg', 'cser5.jpg', 13727, NULL, NULL, NULL, 1, 1360078246, 1, 1360078246, '177 178'),
(99, 1, 'cser6.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser6.jpg', 'image/jpeg', 'cser6.jpg', 14762, NULL, NULL, NULL, 1, 1360078245, 1, 1360078245, '178 177'),
(100, 1, 'cser7.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser7.jpg', 'image/jpeg', 'cser7.jpg', 15843, NULL, NULL, NULL, 1, 1360079444, 1, 1360079444, '177 177'),
(101, 1, 'cser8.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser8.jpg', 'image/jpeg', 'cser8.jpg', 36008, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '128 128'),
(102, 1, 'cser9.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/cser9.jpg', 'image/jpeg', 'cser9.jpg', 29902, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '128 128'),
(103, 1, 'directors-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/directors-page.jpg', 'image/jpeg', 'directors-page.jpg', 60713, NULL, NULL, NULL, 1, 1360061463, 1, 1360061463, '326 1280'),
(104, 1, 'food-sales2.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/food-sales2.jpg', 'image/jpeg', 'food-sales2.jpg', 10121, NULL, NULL, NULL, 1, 1360065298, 1, 1360065298, '129 128'),
(105, 1, 'food-sales3.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/food-sales3.jpg', 'image/jpeg', 'food-sales3.jpg', 9209, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '129 128'),
(106, 1, 'food-sales-page-banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/food-sales-page-banner.jpg', 'image/jpeg', 'food-sales-page-banner.jpg', 99473, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '326 1280'),
(107, 1, 'food-sales1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/food-sales1.jpg', 'image/jpeg', 'food-sales1.jpg', 7383, NULL, NULL, NULL, 1, 1360065298, 1, 1360065298, '129 128'),
(108, 1, 'generic-about-us-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/generic-about-us-page.jpg', 'image/jpeg', 'generic-about-us-page.jpg', 132558, NULL, NULL, NULL, 1, 1360061463, 1, 1360061463, '326 1280'),
(109, 1, 'green bear at hosp.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/green bear at hosp.jpg', 'image/jpeg', 'green bear at hosp.jpg', 56291, NULL, NULL, NULL, 1, 1360077544, 1, 1360077544, '200 226'),
(110, 1, 'head-office-new-logo.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/head-office-new-logo.jpg', 'image/jpeg', 'head-office-new-logo.jpg', 217631, NULL, NULL, NULL, 1, 1361359869, 1, 1361359869, '326 1280'),
(111, 1, 'home_bean_banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/home_bean_banner.jpg', 'image/jpeg', 'home_bean_banner.jpg', 89136, NULL, NULL, NULL, 1, 1361536236, 1, 1361536236, '327 635'),
(112, 1, 'icePak2.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/icePak2.jpg', 'image/jpeg', 'icePak2.jpg', 10544, NULL, NULL, NULL, 1, 1360065298, 1, 1360065298, '129 128'),
(113, 1, 'icepak-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/icepak-page.jpg', 'image/jpeg', 'icepak-page.jpg', 197504, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '326 1280'),
(114, 1, 'icePak1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/icePak1.jpg', 'image/jpeg', 'icePak1.jpg', 8781, NULL, NULL, NULL, 1, 1360065297, 1, 1360065297, '129 128'),
(115, 1, 'locations-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/locations-page.jpg', 'image/jpeg', 'locations-page.jpg', 183700, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '326 1280'),
(116, 1, 'logistics-page.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/logistics-page.jpg', 'image/jpeg', 'logistics-page.jpg', 86114, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '326 1280'),
(117, 1, 'logistics1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/logistics1.jpg', 'image/jpeg', 'logistics1.jpg', 8125, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '129 128'),
(118, 1, 'logolink-icepak.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/logolink-icepak.jpg', 'image/jpeg', 'logolink-icepak.jpg', 17422, NULL, NULL, NULL, 1, 1361205560, 1, 1361205560, '139 278'),
(119, 1, 'logistics2.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/logistics2.jpg', 'image/jpeg', 'logistics2.jpg', 10644, NULL, NULL, NULL, 1, 1360065298, 1, 1360065298, '129 128'),
(120, 1, 'logolink-belfield.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/logolink-belfield.jpg', 'image/jpeg', 'logolink-belfield.jpg', 14731, NULL, NULL, NULL, 1, 1361197373, 1, 1361197373, '139 278'),
(121, 1, 'logolink-luckyred.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/logolink-luckyred.jpg', 'image/jpeg', 'logolink-luckyred.jpg', 16866, NULL, NULL, NULL, 1, 1361205469, 1, 1361205469, '139 278'),
(122, 1, 'lucky-red-comp.jpeg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/lucky-red-comp.jpeg', 'image/jpeg', 'lucky-red-comp.jpeg', 150141, NULL, NULL, NULL, 1, 1361359862, 1, 1361359862, '326 1280'),
(123, 1, 'lucky-red-thumb.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/lucky-red-thumb.jpg', 'image/jpeg', 'lucky-red-thumb.jpg', 11192, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '129 128'),
(124, 1, 'lucky1.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/lucky1.jpg', 'image/jpeg', 'lucky1.jpg', 8412, NULL, NULL, NULL, 1, 1360065298, 1, 1360065298, '129 128'),
(125, 1, 'netstock-banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/netstock-banner.jpg', 'image/jpeg', 'netstock-banner.jpg', 283814, NULL, NULL, NULL, 1, 1361380136, 1, 1361380136, '326 1280'),
(126, 1, 'rha-logoacc-.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/rha-logoacc-.jpg', 'image/jpeg', 'rha-logoacc-.jpg', 4686, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '87 78'),
(127, 1, 'rha_logo.gif', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/rha_logo.gif', 'image/gif', 'rha_logo.gif', 3298, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '125 145'),
(128, 1, 'news-temporary.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/news-temporary.jpg', 'image/jpeg', 'news-temporary.jpg', 6049, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '326 1280'),
(129, 1, 'rochdale-expansion-banner.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/rochdale-expansion-banner.jpg', 'image/jpeg', 'rochdale-expansion-banner.jpg', 271557, NULL, NULL, NULL, 1, 1361361490, 1, 1361361490, '326 1280'),
(130, 1, 'royal-manchester-childrens-hospital.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/royal-manchester-childrens-hospital.jpg', 'image/jpeg', 'royal-manchester-childrens-hospital.jpg', 46652, NULL, NULL, NULL, 1, 1360851656, 1, 1360851656, '199 288'),
(132, 1, 'sts_logo.gif', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/sts_logo.gif', 'image/gif', 'sts_logo.gif', 5293, NULL, NULL, NULL, 1, 1360334349, 1, 1360334349, '125 145'),
(133, 1, 'Shot-2-right-hand-side.jpg', 2, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/images/content/Generic_Content_images/Shot-2-right-hand-side.jpg', 'image/jpeg', 'Shot-2-right-hand-side.jpg', 57024, NULL, NULL, NULL, 1, 1361544868, 1, 1361544868, '327 635');

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_categories`
--

CREATE TABLE IF NOT EXISTS `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_dimensions`
--

CREATE TABLE IF NOT EXISTS `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_watermarks`
--

CREATE TABLE IF NOT EXISTS `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pref_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `server_path` varchar(150) NOT NULL DEFAULT '',
  `filename` varchar(150) NOT NULL DEFAULT '',
  `extension` varchar(7) NOT NULL DEFAULT '',
  `filesize` int(10) NOT NULL DEFAULT '1',
  `emailed` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`attachment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `pref_id` (`pref_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_freeform_attachments`
--

INSERT INTO `exp_freeform_attachments` (`attachment_id`, `entry_id`, `pref_id`, `entry_date`, `server_path`, `filename`, `extension`, `filesize`, `emailed`) VALUES
(1, 20, 3, 1361290677, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/careersuploads/', 'viewTaxReturnPdf', '.pdf', 724, 'y'),
(2, 21, 3, 1361291620, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/careersuploads/', 'viewTaxReturnPdf1', '.pdf', 724, 'y'),
(3, 22, 3, 1361291802, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/careersuploads/', 'viewTaxReturnPdf2', '.pdf', 724, 'y'),
(4, 26, 3, 1361297687, '/Users/Jamie/Sites/yearsleygroup/ye121101-websites/www/yearsley/careersuploads/', 'viewTaxReturnPdf3', '.pdf', 724, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_entries`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_entries` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `weblog_id` int(4) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `form_name` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(150) NOT NULL DEFAULT '',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `status` char(10) NOT NULL DEFAULT 'open',
  `email` varchar(150) NOT NULL DEFAULT '',
  `subject` varchar(150) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `newsletter` varchar(150) NOT NULL DEFAULT '',
  `firstname` varchar(150) NOT NULL DEFAULT '',
  `lastname` varchar(150) NOT NULL DEFAULT '',
  `file1` varchar(150) NOT NULL DEFAULT '',
  `phone` varchar(150) NOT NULL DEFAULT '',
  `captcha` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`entry_id`),
  KEY `author_id` (`author_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `exp_freeform_entries`
--

INSERT INTO `exp_freeform_entries` (`entry_id`, `group_id`, `weblog_id`, `author_id`, `ip_address`, `form_name`, `template`, `entry_date`, `edit_date`, `status`, `email`, `subject`, `message`, `newsletter`, `firstname`, `lastname`, `file1`, `phone`, `captcha`) VALUES
(1, 1, 0, 1, '127.0.0.1', 'Contact Form', '', 1359676811, 1359676811, 'open', 'jonny@hicontrast.co.uk', '890890890', '8908908098', '', '', '', '', '', ''),
(2, 3, 0, 0, '192.168.90.119', 'Job Application', '', 1360341011, 1360341011, 'open', 'kathryn@shoot-the-moon.co.uk', '', 'kjsgfajfgfglulfGSDLF', '', '', '', '', '', ''),
(3, 3, 0, 0, '192.168.90.119', 'Contact Form', '', 1360341306, 1360341306, 'open', 'kathryn@shoot-the-moon.co.uk', 'test', '2469174514754971541', '', '', '', '', '', ''),
(4, 3, 0, 0, '192.168.90.119', 'Job Application', '', 1360584564, 1360584564, 'open', 'kathryn@shoot-the-moon.co.uk', '', 'test', '', '', '', '', '', ''),
(5, 3, 0, 0, '192.168.90.119', 'Contact Form', '', 1360584592, 1360584592, 'open', 'kathryn@shoot-the-moon.co.uk', 'test', 'test', '', '', '', '', '', ''),
(6, 1, 0, 1, '192.168.90.119', 'Contact Form', '', 1360586992, 1360586992, 'open', 'kathryn@shoot-the-moon.co.uk', 'test', 'test', '', '', '', '', '', ''),
(7, 1, 0, 1, '192.168.90.119', 'Contact Form', '', 1360587203, 1360587203, 'open', 'martin@shoot-the-moon.co.uk', 'test', 'test', '', '', '', '', '', ''),
(8, 1, 0, 1, '127.0.0.1', 'Job Application', 'default_template', 1360771983, 1360771983, 'open', 'martagnan@gmail.com', '', 'Test', '', '', '', '', '', ''),
(9, 1, 0, 1, '127.0.0.1', 'Job Application', 'default_template', 1360772050, 1360772050, 'open', 'fsdfsdfsf@sdfsdfsdfsdfsf.net', '', 'Testing', '', '', '', '', '', ''),
(10, 1, 0, 1, '127.0.0.1', 'Job Application', 'default_template', 1360772367, 1360772367, 'open', 'ppepepepww@derewewewe.net', '', 'Hello', '', '', '', '', '', ''),
(11, 1, 0, 1, '192.168.90.119', 'Contact Form', '', 1361209442, 1361209442, 'open', 'kathryn@shoot-the-moon.co.uk', 'test', 'ajfakhdfadafdkh', '', '', '', '', '', ''),
(12, 1, 0, 1, '127.0.0.1', 'Contact Form', '', 1361283977, 1361283977, 'open', 'home@jamiewallace.co.uk', 'This is a test subject', 'skcjnkdnckcnkcnskjcnjscndjkscdjkscndjksc', '', '', '', '', '', ''),
(13, 1, 0, 1, '127.0.0.1', 'Contact Form', '', 1361284100, 1361284100, 'open', 'hello@jamiewallace.co.uk', 'This is a test subject 2', 'dfljkcnvkdjnvvkdvnkf', '', '', '', '', '', ''),
(14, 1, 0, 1, '127.0.0.1', 'Contact Form', 'contact_form', 1361284936, 1361284936, 'open', 'kathryn@shoot-the-moon.co.uk', 'skdcnskcn', 'dkfnvdkfjnvkdjnv', 'Yes', '', '', '', '', ''),
(15, 1, 0, 1, '127.0.0.1', 'Newsletter', '', 1361285868, 1361285868, 'open', 'newsletters@jamiewallace.co.uk', '', '', '', '', '', '', '', ''),
(16, 1, 0, 1, '127.0.0.1', 'Newsletter', '', 1361285914, 1361285914, 'open', 'news@jamiewallace.co.uk', '', '', '', '', '', '', '', ''),
(17, 1, 0, 1, '127.0.0.1', 'Contact Form', 'contact_form', 1361287143, 1361287143, 'open', 'yearsley@jamiewallace.co.uk', 'acbxabcjsbc', 'jsdfhbvjdbdbfhjvbhdfjbvdj', 'Yes', '', '', '', '', ''),
(18, 1, 0, 1, '127.0.0.1', 'Newsletter', '', 1361287917, 1361287917, 'open', 'smell@jamiewallace.co.uk', '', '', '', '', '', '', '', ''),
(19, 1, 0, 1, '127.0.0.1', 'Contact Form', 'contact_form', 1361287964, 1361287964, 'open', 'smellyou@jamiewallace.co.uk', 'eilvbdfbvdkbvdfjbhv,bhv', 'skfjdnvk.dfjnvkdfjvdfknv', 'Yes', 'Jamie', 'Wallace', '', '', ''),
(20, 1, 0, 1, '127.0.0.1', 'Job Application', 'default_template', 1361290677, 1361290677, 'open', 'jobs@jamiewallace.co.uk', '', 'fvdvdfvdfvdfvfdvdfvfdvfdvdfvdfvdfv', '', 'Jamie', 'Wallace', '', '', ''),
(21, 1, 0, 1, '127.0.0.1', 'Contact Form', 'careers_form', 1361291620, 1361291620, 'open', 'jobs@jamiewallace.co.uk', '', 'scdscdscdscdscdscdscdscdscdscdscdsc', '', 'Jamie', 'Wallace', '', '', ''),
(22, 1, 0, 1, '127.0.0.1', 'Job Application', 'careers_form', 1361291802, 1361291802, 'open', 'jobs@jamiewallace.co.uk', '', 'sdcscsdscdscdscdscdscdscdscdscdscscsdc', '', 'Jamie', 'Wallace', '', '', ''),
(23, 1, 0, 1, '192.168.90.119', 'Newsletter', '', 1361294309, 1361294309, 'open', 'test19@test.co.uk', '', '', '', '', '', '', '', ''),
(24, 1, 0, 1, '127.0.0.1', 'Contact Form', 'contact_form', 1361296002, 1361296002, 'open', 'jamie@shoot-the-moon.co.uk', 'This is a test subject', 'scdfvfdvfdvfdvdvdvdvdfv', 'Yes', 'Jamie', 'Wallace', '', '', ''),
(25, 1, 0, 1, '127.0.0.1', 'Contact Form', 'contact_form', 1361296130, 1361296130, 'open', 'testing@jamiewallace.co.uk', 'This is a test subject', 'slfnvkdfnvfdlskdfdflmeilcilsfc', 'Yes', 'Jamie', 'Wallace', '', '', ''),
(26, 1, 0, 1, '127.0.0.1', 'Job Application', 'careers_form', 1361297687, 1361297687, 'open', 'testuser@jamiewallace.co.uk', '', 'fvfgbfvffbfbfgbfbfb', '', 'Jamie', 'Wallace', '', '07921135819', ''),
(27, 1, 0, 1, '127.0.0.1', 'Newsletter', '', 1361811107, 1361811107, 'open', 'hello@jamiewallace.co.uk', '', '', '', '', '', '', '', ''),
(28, 3, 0, 0, '192.168.90.119', 'Newsletter', '', 1361811424, 1361811424, 'open', 'kathryn@shoot-the-moon.co.uk', '', '', '', '', '', '', '', ''),
(29, 3, 0, 0, '192.168.90.119', 'Newsletter', '', 1361811461, 1361811461, 'open', 'kathryn@shoot-the-moon.co.uk', '', '', '', '', '', '', '', ''),
(30, 1, 0, 1, '192.168.90.119', 'Newsletter', '', 1361811837, 1361811837, 'open', 'kathryn@shoot-the-moon.co.uk', '', '', '', '', '', '', '', ''),
(31, 1, 0, 1, '192.168.90.119', 'Newsletter', '', 1361812895, 1361812895, 'open', 'test@test.co.uk', '', '', '', '', '', '', '', ''),
(32, 3, 0, 0, '109.111.197.225', 'Contact Form', 'contact_form', 1362559862, 1362559862, 'open', 'testuser@jamiewallace.co.uk', 'This is a test message', 'Expedita beatae, sodales, rerum, condimentum corrupti? Harum quidem curae? Quae dui semper sequi, euismod, convallis aspernatur, quas vulputate voluptate vivamus. Fames aliquet! Curabitur ad? Hendrerit? Omnis reiciendis, magna faucibus repudiandae.\n\nDolores, magni voluptatum eget metus habitasse pariatur fusce! Ultrices, laboriosam itaque quis, anim bibendum! Fringilla class maecenas dui turpis, habitasse exercitation tempor metus sodales? Quis quo enim, odit ultricies maiores.', 'Yes', 'Jamie', 'Wallace', '', '', 'mother96'),
(33, 3, 0, 0, '109.111.197.225', 'Contact Form', 'contact_form', 1362560059, 1362560059, 'open', 'jamie@shoot-the-moon.co.uk', 'This is a test message from the contact form', 'kjenckExpedita beatae, sodales, rerum, condimentum corrupti? Harum quidem curae? Quae dui semper sequi, euismod, convallis aspernatur, quas vulputate voluptate vivamus. Fames aliquet! Curabitur ad? Hendrerit? Omnis reiciendis, magna faucibus repudiandae.\n\nDolores, magni voluptatum eget metus habitasse pariatur fusce! Ultrices, laboriosam itaque quis, anim bibendum! Fringilla class maecenas dui turpis, habitasse exercitation tempor metus sodales? Quis quo enim, odit ultricies maiores.', 'Yes', 'Jamie', 'Wallace', '', '', 'change82'),
(34, 3, 0, 0, '109.111.197.225', 'Contact Form', 'contact_form', 1362560408, 1362560408, 'open', 'thisisbrill@jamiewallace.co.uk', 'I would like to find out more', 'Expedita beatae, sodales, rerum, condimentum corrupti? Harum quidem curae? Quae dui semper sequi, euismod, convallis aspernatur, quas vulputate voluptate vivamus. Fames aliquet! Curabitur ad? Hendrerit? Omnis reiciendis, magna faucibus repudiandae.\n\nDolores, magni voluptatum eget metus habitasse pariatur fusce! Ultrices, laboriosam itaque quis, anim bibendum! Fringilla class maecenas dui turpis, habitasse exercitation tempor metus sodales? Quis quo enim, odit ultricies maiores.', 'Yes', 'Jamie', 'Wallace', '', '', 'usually66'),
(35, 1, 0, 1, '109.111.197.225', 'Contact Form', 'contact_form', 1362560504, 1362560504, 'open', 'martin@shoot-the-moon.co.uk', 'test', 'test again', 'Yes', 'Martin', 'Smith', '', '', 'set77'),
(36, 1, 0, 1, '109.111.197.225', 'Newsletter', '', 1363189234, 1363189234, 'open', 'jamie@shoot-the-moon.co.uk', '', '', '', '', '', '', '', ''),
(37, 1, 0, 1, '109.111.197.225', 'Contact Form', 'contact_form', 1363190713, 1363190713, 'open', 'jamie@shoot-the-moon.co.uk', 'Test', 'test', 'Yes', 'Jamie', 'Wallace', '', '', 'out97'),
(38, 3, 0, 0, '109.111.197.225', 'Newsletter', '', 1363262899, 1363262899, 'open', 'test80@test.co.uk', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fields`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_order` int(10) NOT NULL DEFAULT '0',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_length` int(3) NOT NULL DEFAULT '150',
  `form_name` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `name_old` varchar(50) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `weblog_id` int(4) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `editable` char(1) NOT NULL DEFAULT 'y',
  `status` char(10) NOT NULL DEFAULT 'open',
  PRIMARY KEY (`field_id`),
  KEY `name` (`name`),
  KEY `author_id` (`author_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `exp_freeform_fields`
--

INSERT INTO `exp_freeform_fields` (`field_id`, `field_order`, `field_type`, `field_length`, `form_name`, `name`, `name_old`, `label`, `weblog_id`, `author_id`, `entry_date`, `edit_date`, `editable`, `status`) VALUES
(19, 7, 'text', 150, '', 'file1', '', 'CV Upload', 0, 0, 0, 0, 'y', 'open'),
(2, 3, 'text', 150, '', 'email', '', 'Email', 0, 0, 0, 0, 'n', 'open'),
(15, 4, 'textarea', 150, '', 'message', '', 'Message', 0, 0, 0, 0, 'y', 'open'),
(14, 5, 'text', 150, '', 'subject', '', 'Subject', 0, 0, 0, 0, 'y', 'open'),
(16, 6, 'text', 150, '', 'newsletter', '', 'Newsletter', 0, 0, 0, 0, 'y', 'open'),
(17, 1, 'text', 150, '', 'firstname', '', 'First Name', 0, 0, 0, 0, 'y', 'open'),
(18, 2, 'text', 150, '', 'lastname', '', 'Last Name', 0, 0, 0, 0, 'y', 'open'),
(20, 8, 'text', 150, '', 'phone', '', 'Phone Number', 0, 0, 0, 0, 'y', 'open'),
(21, 9, 'text', 150, '', 'captcha', '', 'Captcha', 0, 0, 0, 0, 'y', 'open');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_params`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`params_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2470 ;

--
-- Dumping data for table `exp_freeform_params`
--

INSERT INTO `exp_freeform_params` (`params_id`, `entry_date`, `data`) VALUES
(2467, 1363269863, 'a:25:{s:15:"require_captcha";s:2:"no";s:9:"form_name";s:10:"Newsletter";s:10:"require_ip";s:0:"";s:11:"ee_required";s:5:"email";s:9:"ee_notify";s:0:"";s:18:"allowed_file_types";s:0:"";s:8:"reply_to";b:0;s:20:"reply_to_email_field";s:0:"";s:19:"reply_to_name_field";s:0:"";s:11:"output_json";s:1:"n";s:12:"ajax_request";s:1:"y";s:10:"recipients";s:1:"n";s:15:"recipient_limit";s:2:"10";s:17:"static_recipients";b:0;s:22:"static_recipients_list";a:0:{}s:18:"recipient_template";s:16:"default_template";s:13:"discard_field";s:0:"";s:15:"send_attachment";s:0:"";s:15:"send_user_email";s:3:"yes";s:20:"send_user_attachment";s:0:"";s:18:"attachment_profile";s:0:"";s:19:"user_email_template";s:16:"newslettersignup";s:8:"template";s:16:"default_template";s:20:"prevent_duplicate_on";s:5:"email";s:11:"file_upload";s:0:"";}'),
(2468, 1363269867, 'a:25:{s:15:"require_captcha";s:2:"no";s:9:"form_name";s:10:"Newsletter";s:10:"require_ip";s:0:"";s:11:"ee_required";s:5:"email";s:9:"ee_notify";s:0:"";s:18:"allowed_file_types";s:0:"";s:8:"reply_to";b:0;s:20:"reply_to_email_field";s:0:"";s:19:"reply_to_name_field";s:0:"";s:11:"output_json";s:1:"n";s:12:"ajax_request";s:1:"y";s:10:"recipients";s:1:"n";s:15:"recipient_limit";s:2:"10";s:17:"static_recipients";b:0;s:22:"static_recipients_list";a:0:{}s:18:"recipient_template";s:16:"default_template";s:13:"discard_field";s:0:"";s:15:"send_attachment";s:0:"";s:15:"send_user_email";s:3:"yes";s:20:"send_user_attachment";s:0:"";s:18:"attachment_profile";s:0:"";s:19:"user_email_template";s:16:"newslettersignup";s:8:"template";s:16:"default_template";s:20:"prevent_duplicate_on";s:5:"email";s:11:"file_upload";s:0:"";}'),
(2469, 1363271671, 'a:25:{s:15:"require_captcha";s:2:"no";s:9:"form_name";s:10:"Newsletter";s:10:"require_ip";s:0:"";s:11:"ee_required";s:5:"email";s:9:"ee_notify";s:0:"";s:18:"allowed_file_types";s:0:"";s:8:"reply_to";b:0;s:20:"reply_to_email_field";s:0:"";s:19:"reply_to_name_field";s:0:"";s:11:"output_json";s:1:"n";s:12:"ajax_request";s:1:"y";s:10:"recipients";s:1:"n";s:15:"recipient_limit";s:2:"10";s:17:"static_recipients";b:0;s:22:"static_recipients_list";a:0:{}s:18:"recipient_template";s:16:"default_template";s:13:"discard_field";s:0:"";s:15:"send_attachment";s:0:"";s:15:"send_user_email";s:3:"yes";s:20:"send_user_attachment";s:0:"";s:18:"attachment_profile";s:0:"";s:19:"user_email_template";s:16:"newslettersignup";s:8:"template";s:16:"default_template";s:20:"prevent_duplicate_on";s:5:"email";s:11:"file_upload";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_preferences`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) NOT NULL DEFAULT '',
  `preference_value` text NOT NULL,
  PRIMARY KEY (`preference_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `html` char(1) NOT NULL DEFAULT 'n',
  `template_name` varchar(150) NOT NULL DEFAULT '',
  `template_label` varchar(150) NOT NULL DEFAULT '',
  `data_from_name` varchar(150) NOT NULL DEFAULT '',
  `data_from_email` varchar(200) NOT NULL DEFAULT '',
  `data_title` varchar(80) NOT NULL DEFAULT '',
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_freeform_templates`
--

INSERT INTO `exp_freeform_templates` (`template_id`, `enable_template`, `wordwrap`, `html`, `template_name`, `template_label`, `data_from_name`, `data_from_email`, `data_title`, `template_data`) VALUES
(1, 'y', 'y', 'n', 'default_template', 'Default Template', '', '', 'Someone has posted to Freeform', 'Someone has posted to Freeform. Here are the details:\n\nEntry Date: {entry_date}\n{all_custom_fields}'),
(2, 'y', 'y', 'y', 'contact_form', 'Contact Form', 'Contact Form', 'logistics@yearsley.co.uk', 'Yearsley Group Web Contact Form Submission', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n<html>\n<head>\n<title>Email Template</title>\n<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">\n</head>\n<body bgcolor="#d7d7d7" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="border: 0;margin: 0;padding: 0;">\n\n<!--100% body table-->\n<table width="100%" border="0" cellspacing="0" cellpadding="0">\n    <tr>\n        <td bgcolor="#d7d7d7" style="background-color: #d7d7d7;">\n\n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="35" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="35" bgcolor="#d7d7d7"></td>\n                    <td width="30" height="35" bgcolor="#333333"></td>\n                    <td width="270" height="35" bgcolor="#333333"><p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0; padding: 0; line-height: 1.2; text-align: left; font-size: 11px; font-style:italic; color: #c5c5c5;">Visit <a href="http://www.yearsleygroup.co.uk" style="font-family:Georgia, ''Times New Roman'', Times, serif; color: #c5c5c5; text-decoration: underline; color: #c5c5c5;">yearsley.co.uk</a></p></td>\n                    <td width="270" height="35" bgcolor="#333333"><p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0; padding: 0; line-height: 1.2; text-align: right; font-size: 11px; font-style:italic;"><a href="http://www.yearsleygroup.co.uk/admin.php" style="font-family:Georgia, ''Times New Roman'', Times, serif; color: #c5c5c5; text-decoration: underline;">Admin login</a></p></td>\n                    <td width="30" height="35" bgcolor="#333333"></td>\n                    <td width="8" height="35" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="158" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="158" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="158" bgcolor="#fcfbff" valign="top"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/yearsley-header.jpg" width="600" height="158" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="8" height="158" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="540" bgcolor="#fcfbff" valign="top" style="padding: 0;"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/zebra_line.jpg" width="540" height="5" alt="" style="display: block;background: #fcfbff;">\n                    </td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="540" bgcolor="#fcfbff" valign="top" style="padding: 20px 0;">\n                    <h3 style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2; text-align: left; font-size: 20px; font-style:italic;">On {entry_date}, {firstname} {lastname} posted to the Yearsley Group website contact form.</h3>\n                    </td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="540" bgcolor="#fcfbff" valign="top" style="padding: 0;"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/zebra_line.jpg" width="540" height="5" alt="" style="display: block;background: #fcfbff;">\n                    </td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="540" bgcolor="#fcfbff" valign="top" style="padding: 10px 0;">\n                    <p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2;text-align: left; font-size: 15px; font-style:italic;">Here are their details:</p>\n                    </td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0" >\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="540" bgcolor="#fcfbff" valign="top" style="margin:0; padding:0;">\n                        <ul style="font-family:Georgia, ''Times New Roman'', Times, serif; line-height: 1.2;text-align: left; font-size: 15px; margin: 0; padding: 0 0 0 20px; font-style:italic; list-style:square;">\n                        	<li style="padding:10px 0;">First name: <!--<strong>{firstname}</strong>--><strong>Jamie</strong></li>\n                            <li style="padding:10px 0;">Last name: <!--<strong>{lastname}</strong>--><strong>Wallace</strong></li>\n                            <li style="padding:10px 0;">Email: <!--<strong>{email}</strong>--><strong>jamie@shoot-the-moon.co.uk</strong></li>\n                            <li style="padding:10px 0;">Subject: <!--<strong>{subject}</strong>--><strong>Hi there!</strong></li>\n                            <li style="padding:10px 0;">Message: <!--<strong>{message}</strong>-->Excepturi nam pretium quod animi irure, orci nonummy nulla ea. Laboris arcu quam. Tempore gravida hendrerit dictum lobortis debitis ut nec aliquid quod autem, reprehenderit tempus rutrum morbi tempore egestas.\n\nCillum faucibus ultrices consequatur, dui sem! Taciti quis vestibulum cras leo ad rerum quis. Auctor! Condimentum officia auctor dolorem accusantium, blandit consequuntur, eius laoreet vitae repudiandae. Non, iste ullamcorper pellentesque.\n\nEtiam, iaculis quas ligula, montes justo dapibus iste litora natoque netus? Integer quasi odio, natoque adipisci nulla ligula corrupti mollit numquam aute cillum erat montes eros dignissim nulla iusto laboriosam.</li>\n                            <li style="padding:10px 0;">Newsletter Opt In: <!--<strong>{newsletter}</strong>--><strong>Yes</strong></li>\n                        </ul>\n					</td>\n                    \n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="30" height="20" bgcolor="#fcfbff"></td>\n                    <td width="540" height="20" bgcolor="#fcfbff" valign="top" style="padding: 10px 0 0;"></td>\n                    <td width="30" height="20" bgcolor="#fcfbff"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="15" height="20" bgcolor="#fcfbff"></td>\n                    <td width="570" height="20"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/body-lift.jpg" width="570" height="20" border="0" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="15" height="20" bgcolor="#fcfbff"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="6" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="6" bgcolor="#d7d7d7"></td>\n                    <td width="15" height="6" bgcolor="#fcfbff"></td>\n                    <td width="570" height="6" bgcolor="#fcfbff"></td>\n                    <td width="15" height="6" bgcolor="#fcfbff"></td>\n                    <td width="8" height="6" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            \n            \n            <table width="616" height="8" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="8" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="8" bgcolor="#d7d7d7"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/shadow-bottom.jpg" width="600" height="8" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="8" height="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="10" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="10" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="10" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="10" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="15" bgcolor="#d7d7d7"></td>\n                    <td width="570" valign="top" align="center" bgcolor="#d7d7d7"><p style="font-family:Georgia, ''Times New Roman'', Times, serif;margin: 0;padding: 0;line-height: 1.2;text-align: center;font-size: 12px;color: #888; font-style:italic;"><strong>&copy; 2013. Harry Yearsley Ltd.</strong><br><br>\nHareshill Road Heywood, Greater Manchester, OL10 2TP<br>\nRegistered Number: 556895. <a href="http://www.yearsleygroup.co.uk" style="font-family:Georgia, ''Times New Roman'', Times, serif;color: #888;text-decoration: underline;font-weight: 400;">www.yearsleygroup.co.uk</a>.<br><br>\nPlease do not reply to this email.  This is an automated email from the CMS.</p></td>\n                    <td width="15" bgcolor="#d7d7d7"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n        </td>\n    </tr>\n</table>\n\n</body>\n</html>'),
(3, 'y', 'y', 'y', 'newslettersignup', 'Newsletter User Notification Email', 'Yearsley Group', 'logistics@yearsley.co.uk', 'Yearsley Group Newsletter Subscription Confirmation', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n<html>\n<head>\n<title>Email Template</title>\n<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">\n</head>\n<body bgcolor="#d7d7d7" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="border: 0;margin: 0;padding: 0;">\n\n<!--100% body table-->\n<table width="100%" border="0" cellspacing="0" cellpadding="0">\n    <tr>\n        <td bgcolor="#d7d7d7" style="background-color: #d7d7d7;">\n\n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="35" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="35" bgcolor="#d7d7d7"></td>\n                    <td width="30" height="35" bgcolor="#333333"></td>\n                    <td width="270" height="35" bgcolor="#333333"><p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0; padding: 0; line-height: 1.2; text-align: left; font-size: 11px; font-style:italic; color: #c5c5c5;">Visit <a href="http://www.yearsleygroup.co.uk" style="font-family:Georgia, ''Times New Roman'', Times, serif; color: #c5c5c5; text-decoration: underline; color: #c5c5c5;">yearsley.co.uk</a></p></td>\n                    <td width="270" height="35" bgcolor="#333333"></td>\n                    <td width="30" height="35" bgcolor="#333333"></td>\n                    <td width="8" height="35" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="158" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="158" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="158" bgcolor="#fcfbff" valign="top"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/yearsley-header.jpg" width="600" height="158" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="8" height="158" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="540" bgcolor="#fcfbff" valign="top" style="padding: 20px 0 0;">\n                    <h3 style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2; text-align: left; font-size: 20px; padding-bottom: 20px; font-style:italic;">Thanks for subscribing!</h3>\n		<p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2;text-align: left; font-size: 15px; padding-bottom: 10px; font-style:italic;">This is an automated email to confirm you have subscribed to the Yearsley Group email newsletter.</p>\n                    </td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="30" height="20" bgcolor="#fcfbff"></td>\n                    <td width="540" height="20" bgcolor="#fcfbff" valign="top" style="padding: 20px 0 0;"></td>\n                    <td width="30" height="20" bgcolor="#fcfbff"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="15" height="20" bgcolor="#fcfbff"></td>\n                    <td width="570" height="20"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/body-lift.jpg" width="570" height="20" border="0" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="15" height="20" bgcolor="#fcfbff"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="6" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="6" bgcolor="#d7d7d7"></td>\n                    <td width="15" height="6" bgcolor="#fcfbff"></td>\n                    <td width="570" height="6" bgcolor="#fcfbff"></td>\n                    <td width="15" height="6" bgcolor="#fcfbff"></td>\n                    <td width="8" height="6" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            \n            \n            <table width="616" height="8" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="8" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="8" bgcolor="#d7d7d7"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/shadow-bottom.jpg" width="600" height="8" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="8" height="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="10" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="10" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="10" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="10" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="15" bgcolor="#d7d7d7"></td>\n                    <td width="570" valign="top" align="center" bgcolor="#d7d7d7"><p style="font-family:Georgia, ''Times New Roman'', Times, serif;margin: 0;padding: 0;line-height: 1.2;text-align: center;font-size: 12px;color: #888; font-style:italic;"><strong>&copy; 2013. Harry Yearsley Ltd.</strong><br><br>\nHareshill Road Heywood, Greater Manchester, OL10 2TP<br>\nRegistered Number: 556895. <a href="http://www.yearsleygroup.co.uk" style="font-family:Georgia, ''Times New Roman'', Times, serif;color: #888;text-decoration: underline;font-weight: 400;">www.yearsleygroup.co.uk</a>.<br><br>\nThis is an automated email, please do not reply to this email.</p></td>\n                    <td width="15" bgcolor="#d7d7d7"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    \n                    \n                    \n                 \n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n        </td>\n    </tr>\n</table>\n\n</body>\n</html>'),
(4, 'y', 'y', 'y', 'careers_form', 'Careers Form', 'Yearsley Careers', 'careers@yearsley.co.uk', 'Yearsley Group Careers Job Application', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n<html>\n<head>\n<title>Email Template</title>\n<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">\n</head>\n<body bgcolor="#d7d7d7" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="border: 0;margin: 0;padding: 0;">\n\n<!--100% body table-->\n<table width="100%" border="0" cellspacing="0" cellpadding="0">\n    <tr>\n        <td bgcolor="#d7d7d7" style="background-color: #d7d7d7;">\n\n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="35" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="35" bgcolor="#d7d7d7"></td>\n                    <td width="30" height="35" bgcolor="#333333"></td>\n                    <td width="270" height="35" bgcolor="#333333"><p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0; padding: 0; line-height: 1.2; text-align: left; font-size: 11px; font-style:italic; color: #c5c5c5;">Visit <a href="http://www.yearsleygroup.co.uk" style="font-family:Georgia, ''Times New Roman'', Times, serif; color: #c5c5c5; text-decoration: underline; color: #c5c5c5;">yearsley.co.uk</a></p></td>\n                    <td width="270" height="35" bgcolor="#333333"><p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0; padding: 0; line-height: 1.2; text-align: right; font-size: 11px; font-style:italic;"><a href="http://www.yearsleygroup.co.uk/admin.php" style="font-family:Georgia, ''Times New Roman'', Times, serif; color: #c5c5c5; text-decoration: underline;">Admin login</a></p></td>\n                    <td width="30" height="35" bgcolor="#333333"></td>\n                    <td width="8" height="35" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="158" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="158" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="158" bgcolor="#fcfbff" valign="top"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/yearsley-header.jpg" width="600" height="158" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="8" height="158" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="540" bgcolor="#fcfbff" valign="top" style="padding: 20px 0 0;">\n                    <h3 style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2; text-align: left; font-size: 20px; padding-bottom: 20px; font-style:italic;">On {entry_date}, {firstname} {lastname} applied for the job {title} from the Yearsley Group web careers page.</h3>\n		<p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2;text-align: left; font-size: 15px; padding-bottom: 10px; font-style:italic;">Here are their details:</p>\n                    </td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0" >\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="180" bgcolor="#fcfbff" valign="top" style="margin:0; padding:0;">\n                        <ul style="font-family:Georgia, ''Times New Roman'', Times, serif; line-height: 1.2;text-align: left; font-size: 15px; margin: 0; padding: 0 0 0 20px; font-style:italic; list-style:square;">\n                        	<li>First Name:</li>\n                            <li>Last Name:</li>\n                            <li>Email:</li>\n                            <li>Phone No:</li>\n                        </ul>\n					</td>\n                    <td width="360" bgcolor="#fcfbff" valign="top" style="margin:0; padding:0;">\n                        <ul style="font-family:Georgia, ''Times New Roman'', Times, serif; line-height: 1.2; text-align: left; font-size: 15px; margin: 0; padding: 0; font-style:italic; list-style:none;">\n                            <li><strong>{firstname}</strong></li>\n                            <li><strong>{lastname}</strong></li>\n                            <li><strong>{email}</strong></li>\n                            <li><strong>{phone}</strong></li>\n                        </ul>\n                    </td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="30" height="20" bgcolor="#fcfbff"></td>\n                    <td width="540" height="20" bgcolor="#fcfbff" valign="top" style="padding: 20px 0 0;"><p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2;text-align: left; font-size: 15px; padding-bottom: 10px; font-style:italic;">Covering note: <strong>{message}</strong></p>\n                    \n                    <p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2;text-align: left; font-size: 15px; padding-bottom: 10px; font-style:italic;">The applicants CV is attached.</p></td>\n                    <td width="30" height="20" bgcolor="#fcfbff"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="15" height="20" bgcolor="#fcfbff"></td>\n                    <td width="570" height="20"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/body-lift.jpg" width="570" height="20" border="0" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="15" height="20" bgcolor="#fcfbff"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="6" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="6" bgcolor="#d7d7d7"></td>\n                    <td width="15" height="6" bgcolor="#fcfbff"></td>\n                    <td width="570" height="6" bgcolor="#fcfbff"></td>\n                    <td width="15" height="6" bgcolor="#fcfbff"></td>\n                    <td width="8" height="6" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            \n            \n            <table width="616" height="8" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="8" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="8" bgcolor="#d7d7d7"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/shadow-bottom.jpg" width="600" height="8" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="8" height="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="10" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="10" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="10" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="10" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="15" bgcolor="#d7d7d7"></td>\n                    <td width="570" valign="top" align="center" bgcolor="#d7d7d7"><p style="font-family:Georgia, ''Times New Roman'', Times, serif;margin: 0;padding: 0;line-height: 1.2;text-align: center;font-size: 12px;color: #888; font-style:italic;"><strong>&copy; 2013. Harry Yearsley Ltd.</strong><br><br>\nHareshill Road Heywood, Greater Manchester, OL10 2TP<br>\nRegistered Number: 556895. <a href="http://www.yearsleygroup.co.uk" style="font-family:Georgia, ''Times New Roman'', Times, serif;color: #888;text-decoration: underline;font-weight: 400;">www.yearsleygroup.co.uk</a>.<br><br>\nPlease do not reply to this email. This is an automated email from the CMS.</p></td>\n                    <td width="15" bgcolor="#d7d7d7"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    \n                    \n                    \n                 \n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n        </td>\n    </tr>\n</table>\n\n</body>\n</html>'),
(5, 'y', 'y', 'y', 'careers_notify', 'Careers User Notification Email', 'Yearsley Careers', 'careers@yearsley.co.uk', 'Thanks for applying for a Yearsley Group Job Vacancy', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n<html>\n<head>\n<title>Email Template</title>\n<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">\n</head>\n<body bgcolor="#d7d7d7" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="border: 0;margin: 0;padding: 0;">\n\n<!--100% body table-->\n<table width="100%" border="0" cellspacing="0" cellpadding="0">\n    <tr>\n        <td bgcolor="#d7d7d7" style="background-color: #d7d7d7;">\n\n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="35" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="35" bgcolor="#d7d7d7"></td>\n                    <td width="30" height="35" bgcolor="#333333"></td>\n                    <td width="270" height="35" bgcolor="#333333"><p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0; padding: 0; line-height: 1.2; text-align: left; font-size: 11px; font-style:italic; color: #c5c5c5;">Visit <a href="http://www.yearsleygroup.co.uk" style="font-family:Georgia, ''Times New Roman'', Times, serif; color: #c5c5c5; text-decoration: underline; color: #c5c5c5;">yearsley.co.uk</a></p></td>\n                    <td width="270" height="35" bgcolor="#333333"></td>\n                    <td width="30" height="35" bgcolor="#333333"></td>\n                    <td width="8" height="35" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="158" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="158" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="158" bgcolor="#fcfbff" valign="top"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/yearsley-header.jpg" width="600" height="158" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="8" height="158" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="540" bgcolor="#fcfbff" valign="top" style="padding: 20px 0 0;">\n                    <h3 style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2; text-align: left; font-size: 20px; padding-bottom: 20px; font-style:italic;">Careers Application Successful!</h3>\n		<p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2;text-align: left; font-size: 15px; padding-bottom: 10px; font-style:italic;">This is an automated email to confirm you have successfully applied for a Yearsley Group Job Application.</p>\n        <p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2;text-align: left; font-size: 15px; padding-bottom: 10px; font-style:italic;">We will take the time to consider your application and get back to you soon.</p>\n        <p style="font-family:Georgia, ''Times New Roman'', Times, serif; margin: 0;padding: 0;line-height: 1.2;text-align: left; font-size: 15px; padding-bottom: 10px; font-style:italic;">Regards, The Yearsley Group.</p>\n                    </td>\n                    <td width="30" bgcolor="#fcfbff"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="30" height="20" bgcolor="#fcfbff"></td>\n                    <td width="540" height="20" bgcolor="#fcfbff" valign="top" style="padding: 20px 0 0;"></td>\n                    <td width="30" height="20" bgcolor="#fcfbff"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="15" height="20" bgcolor="#fcfbff"></td>\n                    <td width="570" height="20"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/body-lift.jpg" width="570" height="20" border="0" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="15" height="20" bgcolor="#fcfbff"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="6" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="6" bgcolor="#d7d7d7"></td>\n                    <td width="15" height="6" bgcolor="#fcfbff"></td>\n                    <td width="570" height="6" bgcolor="#fcfbff"></td>\n                    <td width="15" height="6" bgcolor="#fcfbff"></td>\n                    <td width="8" height="6" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            \n            \n            <table width="616" height="8" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="8" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="8" bgcolor="#d7d7d7"><img src="http://yearsley.stmpreview.co.uk/img/emailtemplates/shadow-bottom.jpg" width="600" height="8" alt="" style="display: block;background: #fcfbff;"></td>\n                    <td width="8" height="8" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" height="10" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="10" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="10" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="10" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n            <table width="616" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    <td width="15" bgcolor="#d7d7d7"></td>\n                    <td width="570" valign="top" align="center" bgcolor="#d7d7d7"><p style="font-family:Georgia, ''Times New Roman'', Times, serif;margin: 0;padding: 0;line-height: 1.2;text-align: center;font-size: 12px;color: #888; font-style:italic;"><strong>&copy; 2013. Harry Yearsley Ltd.</strong><br><br>\nHareshill Road Heywood, Greater Manchester, OL10 2TP<br>\nRegistered Number: 556895. <a href="http://www.yearsleygroup.co.uk" style="font-family:Georgia, ''Times New Roman'', Times, serif;color: #888;text-decoration: underline;font-weight: 400;">www.yearsleygroup.co.uk</a>.<br><br>\nThis is an automated email, please do not reply to this email.</p></td>\n                    <td width="15" bgcolor="#d7d7d7"></td>\n                    <td width="8" bgcolor="#d7d7d7"></td>\n                    \n                    \n                    \n                 \n                </tr>\n            </table>\n            \n            <table width="616" height="20" border="0" align="center" cellpadding="0" cellspacing="0">\n            	<tr>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="600" height="20" bgcolor="#d7d7d7"></td>\n                    <td width="8" height="20" bgcolor="#d7d7d7"></td>\n                </tr>\n            </table>\n            \n        </td>\n    </tr>\n</table>\n\n</body>\n</html>');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_user_email`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `exp_freeform_user_email`
--

INSERT INTO `exp_freeform_user_email` (`email_id`, `entry_id`, `email_count`) VALUES
(1, 3, 1),
(2, 5, 1),
(3, 6, 1),
(4, 7, 1),
(5, 11, 1),
(6, 12, 1),
(7, 13, 1),
(8, 14, 1),
(9, 17, 1),
(10, 19, 1),
(11, 21, 1),
(12, 22, 1),
(13, 24, 1),
(14, 25, 1),
(15, 26, 1),
(16, 32, 1),
(17, 33, 1),
(18, 34, 1),
(19, 35, 1),
(20, 37, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_global_variables`
--

CREATE TABLE IF NOT EXISTS `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_html_buttons`
--

CREATE TABLE IF NOT EXISTS `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `exp_html_buttons`
--

INSERT INTO `exp_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`) VALUES
(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(4, 1, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(5, 1, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img'),
(6, 2, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(7, 2, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(8, 2, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(9, 2, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(10, 2, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img'),
(11, 3, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(12, 3, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(13, 3, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(14, 3, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(15, 3, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img');

-- --------------------------------------------------------

--
-- Table structure for table `exp_layout_publish`
--

CREATE TABLE IF NOT EXISTS `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `exp_layout_publish`
--

INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(10, 1, 1, 6, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(11, 1, 6, 6, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(30, 1, 1, 5, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(31, 1, 6, 5, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(36, 1, 1, 1, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(37, 1, 6, 1, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(32, 1, 1, 7, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(33, 1, 6, 7, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(28, 1, 1, 9, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(29, 1, 6, 9, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(38, 1, 1, 8, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(39, 1, 6, 8, 'a:5:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(40, 1, 1, 4, 'a:5:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(41, 1, 6, 4, 'a:5:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(42, 1, 1, 10, 'a:5:{s:7:"publish";a:4:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:19;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(43, 1, 6, 10, 'a:5:{s:7:"publish";a:4:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:19;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(44, 1, 1, 11, 'a:5:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:24;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:25;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');
INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(45, 1, 6, 11, 'a:5:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:24;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:25;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_cols`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `exp_matrix_cols`
--

INSERT INTO `exp_matrix_cols` (`col_id`, `site_id`, `field_id`, `var_id`, `col_name`, `col_label`, `col_instructions`, `col_type`, `col_required`, `col_search`, `col_order`, `col_width`, `col_settings`) VALUES
(1, 1, 13, NULL, 'food_image', 'Food Image', '', 'assets', 'n', 'n', 0, '15%', 'YTo2OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjQ6ImVlOjIiO31zOjQ6InZpZXciO3M6NjoidGh1bWJzIjtzOjEwOiJ0aHVtYl9zaXplIjtzOjU6InNtYWxsIjtzOjE0OiJzaG93X2ZpbGVuYW1lcyI7czoxOiJuIjtzOjk6InNob3dfY29scyI7YToxOntpOjA7czo0OiJuYW1lIjt9czo1OiJtdWx0aSI7czoxOiJ5Ijt9'),
(2, 1, 13, NULL, 'food_text', 'Food Text', '', 'text', 'n', 'n', 1, '35%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(3, 1, 13, NULL, 'logistics_image', 'Logistics Image', '', 'assets', 'n', 'n', 2, '15%', 'YTo2OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjQ6ImVlOjIiO31zOjQ6InZpZXciO3M6NjoidGh1bWJzIjtzOjEwOiJ0aHVtYl9zaXplIjtzOjU6InNtYWxsIjtzOjE0OiJzaG93X2ZpbGVuYW1lcyI7czoxOiJuIjtzOjk6InNob3dfY29scyI7YToxOntpOjA7czo0OiJuYW1lIjt9czo1OiJtdWx0aSI7czoxOiJ5Ijt9'),
(4, 1, 13, NULL, 'logistics_text', 'Logistics Text', '', 'text', 'n', 'n', 3, '35%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(5, 1, 9, NULL, 'director_image', 'Director Image', '', 'assets', 'n', 'n', 1, '15%', 'YTo2OntzOjg6ImZpbGVkaXJzIjtzOjM6ImFsbCI7czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6NDp7aTowO3M6NDoibmFtZSI7aToxO3M6NjoiZm9sZGVyIjtpOjI7czo0OiJkYXRlIjtpOjM7czo0OiJzaXplIjt9czo1OiJtdWx0aSI7czoxOiJuIjt9'),
(6, 1, 9, NULL, 'director_bio', 'Director Bio', '', 'text', 'n', 'n', 2, '70%', 'YTo0OntzOjQ6Im1heGwiO3M6NDoiMTAwMCI7czo5OiJtdWx0aWxpbmUiO3M6MToieSI7czozOiJmbXQiO3M6NDoibm9uZSI7czozOiJkaXIiO3M6MzoibHRyIjt9'),
(7, 1, 18, NULL, 'md_image', 'Md Image', '', 'assets', 'n', 'n', 1, '15%', 'YTo2OntzOjg6ImZpbGVkaXJzIjtzOjM6ImFsbCI7czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToibiI7fQ=='),
(8, 1, 18, NULL, 'md_bio', 'MD Bio', '', 'text', 'n', 'n', 2, '70%', 'YTo0OntzOjQ6Im1heGwiO3M6NDoiMTAwMCI7czo5OiJtdWx0aWxpbmUiO3M6MToieSI7czozOiJmbXQiO3M6NDoibm9uZSI7czozOiJkaXIiO3M6MzoibHRyIjt9'),
(9, 1, 18, NULL, 'md_name', 'MD Name', '', 'text', 'n', 'n', 0, '15%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(10, 1, 9, NULL, 'director_name', 'Name', '', 'text', 'n', 'n', 0, '15%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(11, 1, 19, NULL, 'timeline_date', 'Timeline Date', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(12, 1, 19, NULL, 'timeline_image', 'Timeline Image', '', 'assets', 'n', 'n', 1, '', 'YTo2OntzOjg6ImZpbGVkaXJzIjtzOjM6ImFsbCI7czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToieSI7fQ=='),
(13, 1, 19, NULL, 'timeline_text', 'Timeline Text', '', 'wygwam', 'n', 'n', 2, '', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_data`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_3` text,
  `col_id_4` text,
  `col_id_5` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` text,
  `col_id_13` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `exp_matrix_data`
--

INSERT INTO `exp_matrix_data` (`row_id`, `site_id`, `entry_id`, `field_id`, `var_id`, `is_draft`, `row_order`, `col_id_1`, `col_id_2`, `col_id_3`, `col_id_4`, `col_id_5`, `col_id_6`, `col_id_7`, `col_id_8`, `col_id_9`, `col_id_10`, `col_id_11`, `col_id_12`, `col_id_13`) VALUES
(1, 1, 32, 13, NULL, 0, 1, 'home_food_banner.jpg\nhome_bean_banner.jpg\nhome_icecream_banner.jpg', 'Food', 'home_logistics_banner.jpg\nhome_truckblue_banner.jpg\nShot-2-right-hand-side.jpg', 'Logistics', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 67, 18, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'Harry-and-Mayoress.jpg', 'Harry joined the business in 1982 and was made Managing Director in 1998 at the age of 32. Harry has experience in all aspects of the supply chain, and has been instrumental in the considerable growth and development of the business in the last 15 years.', 'Harry Yearsley', NULL, NULL, NULL, NULL),
(3, 1, 67, 9, NULL, 0, 1, NULL, NULL, NULL, NULL, 'dir_jb.jpg', 'Jonathan has 30 years’ experience in frozen food sales and the supply chain. Following a long and successful period in the food business, Jonathan is now responsible for all company sales, marketing and procurement, as well as the CSER strategy.', NULL, NULL, NULL, 'Jonathan Baker', NULL, NULL, NULL),
(4, 1, 67, 9, NULL, 0, 2, NULL, NULL, NULL, NULL, 'dir_pw.jpg', 'Phil has been a Chartered Accountant for over 30 years, and has held various FD roles for the last 20 years. Phil has been with the Yearsley Group for over 12 years and as well as finance, his role also covers IT, legal and property.', NULL, NULL, NULL, 'Phil Whitworth', NULL, NULL, NULL),
(5, 1, 67, 9, NULL, 0, 3, NULL, NULL, NULL, NULL, 'dir_ik.jpg', 'Ian has been on the board for 10 years now and is highly experienced in food procurement, sales and marketing, especially in developing new markets and product innovation. After developing Belfield into a significant business from scratch, Ian is now responsible for all sales and purchasing for the food division.', NULL, NULL, NULL, 'Ian King', NULL, NULL, NULL),
(6, 1, 67, 9, NULL, 0, 4, NULL, NULL, NULL, NULL, 'dir_tm.jpg', 'Tim has more than 20 years’ experience in frozen logistics, encompassing warehouse and transportation operations. He has over 13 years’ experience in customer facing roles and extensive knowledge of supply chains.  Tim joined the board in 2007 and is responsible for all commercial activity within the Logistics division.', NULL, NULL, NULL, 'Tim Moran', NULL, NULL, NULL),
(7, 1, 67, 9, NULL, 0, 5, NULL, NULL, NULL, NULL, 'dir_mh.jpg', 'With 20 years industry experience at blue chip companies within the logistics sector, Mark joined the business in 2011 and has a degree in Business Administration and a Master’s in Finance. Mark oversees Yearsley operations, which take in a national network of 13 sites, our large fleet and maintenance.', NULL, NULL, NULL, 'Mark Haslam', NULL, NULL, NULL),
(8, 1, 71, 19, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1952', 'bs_cold_storage.jpg', '<p>\n	gfsgsgsfgsgsdgs</p>');

-- --------------------------------------------------------

--
-- Table structure for table `exp_members`
--

CREATE TABLE IF NOT EXISTS `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_members`
--

INSERT INTO `exp_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `daylight_savings`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`, `rte_enabled`, `rte_toolset_id`) VALUES
(1, 1, 'admin', 'Admin', '92fb9557a27a1ff8f90f1006e62bdfa6447179e2c0700f05eba47b780d3ecb636bffb891048cb62036ace82ab772d871df361f5454b529a82cf3fd0819defb4f', 'd~f4p`Y_F@bgeaZ%|KNi0=TiBqrC}9AMl?>?1&uRFFeo5HkNOc[_IG$+|*vwJS7rCTjZ''OS{(wc=ybc}W"RD8Pz7M%<ICCMhE=MH&mdYUS|d`lE-MF,~@ff;iED''k^80', '3a900f5de32170e62259eb083a32cd71299fc18c', '1460c886b77ea96c5f0998e34dca5928a37da9f6', NULL, 'jonny@hicontrast.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '127.0.0.1', 1355938901, 1363265789, 1363269925, 66, 0, 0, 0, 1361788492, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UTC', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', '', 'Structure|C=addons_modules&M=show_module_cp&module=structure|1\nField Editor|C=addons_modules&M=show_module_cp&module=field_editor|2', 'n', 0, 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_bulletin_board`
--

CREATE TABLE IF NOT EXISTS `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_data`
--

CREATE TABLE IF NOT EXISTS `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_data`
--

INSERT INTO `exp_member_data` (`member_id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_fields`
--

CREATE TABLE IF NOT EXISTS `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_groups`
--

INSERT INTO `exp_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_admin_accessories`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`) VALUES
(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(1, 2, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 2, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 2, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 2, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 2, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(1, 3, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 3, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 3, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 3, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 3, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(6, 1, 'Editors', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(6, 2, 'Editors', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(6, 3, 'Editors', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_homepage`
--

CREATE TABLE IF NOT EXISTS `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_homepage`
--

INSERT INTO `exp_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`) VALUES
(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_search`
--

CREATE TABLE IF NOT EXISTS `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_copies`
--

CREATE TABLE IF NOT EXISTS `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_data`
--

CREATE TABLE IF NOT EXISTS `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_folders`
--

CREATE TABLE IF NOT EXISTS `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_message_folders`
--

INSERT INTO `exp_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`) VALUES
(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_listed`
--

CREATE TABLE IF NOT EXISTS `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_modules`
--

CREATE TABLE IF NOT EXISTS `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  `settings` text,
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `exp_modules`
--

INSERT INTO `exp_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`, `settings`) VALUES
(1, 'Comment', '2.3', 'y', 'n', NULL),
(2, 'Email', '2.0', 'n', 'n', NULL),
(3, 'Emoticon', '2.0', 'n', 'n', NULL),
(4, 'Jquery', '1.0', 'n', 'n', NULL),
(5, 'Rss', '2.0', 'n', 'n', NULL),
(6, 'Safecracker', '2.1', 'y', 'n', NULL),
(7, 'Search', '2.2', 'n', 'n', NULL),
(8, 'Channel', '2.0.1', 'n', 'n', NULL),
(9, 'Member', '2.1', 'n', 'n', NULL),
(10, 'Stats', '2.0', 'n', 'n', NULL),
(11, 'Rte', '1.0', 'y', 'n', NULL),
(12, 'Structure', '3.3.8', 'y', 'y', NULL),
(13, 'Libraree', '1.0.5', 'y', 'n', NULL),
(14, 'Wygwam', '2.7', 'y', 'n', NULL),
(15, 'Assets', '2.0.4', 'y', 'n', NULL),
(17, 'Freeform', '3.1.5', 'y', 'n', NULL),
(18, 'Deeploy_helper', '2.0.3', 'y', 'n', NULL),
(19, 'Updater', '3.1.6', 'y', 'n', 'a:4:{s:20:"file_transfer_method";s:5:"local";s:3:"ftp";a:6:{s:8:"hostname";s:0:"";s:4:"port";s:2:"21";s:8:"username";s:0:"";s:8:"password";s:0:"";s:7:"passive";s:3:"yes";s:3:"ssl";s:2:"no";}s:4:"sftp";a:4:{s:8:"hostname";s:0:"";s:4:"port";s:2:"22";s:8:"username";s:0:"";s:8:"password";s:0:"";}s:8:"path_map";a:6:{s:4:"root";s:65:"/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/";s:6:"backup";s:81:"/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/updater_backups/";s:6:"system";s:81:"/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/system_y34r5l3y/";s:18:"system_third_party";s:110:"/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/system_y34r5l3y/expressionengine/third_party/";s:6:"themes";s:72:"/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/themes/";s:18:"themes_third_party";s:84:"/Users/martinsmith/Sites/yearsley/YE121101-Websites/www/yearsley/themes/third_party/";}}'),
(20, 'Field_editor', '1.0.3', 'y', 'n', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_module_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_module_member_groups`
--

INSERT INTO `exp_module_member_groups` (`group_id`, `module_id`) VALUES
(6, 1),
(6, 6),
(6, 11),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(6, 17),
(6, 18);

-- --------------------------------------------------------

--
-- Table structure for table `exp_online_users`
--

CREATE TABLE IF NOT EXISTS `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=473 ;

--
-- Dumping data for table `exp_online_users`
--

INSERT INTO `exp_online_users` (`online_id`, `site_id`, `member_id`, `in_forum`, `name`, `ip_address`, `date`, `anon`) VALUES
(19, 3, 0, 'n', '', '127.0.0.1', 1358332389, ''),
(35, 2, 0, 'n', '', '127.0.0.1', 1358495665, ''),
(34, 2, 0, 'n', '', '127.0.0.1', 1358466413, ''),
(33, 2, 0, 'n', '', '127.0.0.1', 1358459501, ''),
(32, 2, 0, 'n', '', '127.0.0.1', 1358449396, ''),
(472, 1, 0, 'n', '', '109.111.197.225', 1363271671, ''),
(457, 1, 0, 'n', '', '88.98.36.214', 1363190895, ''),
(471, 1, 0, 'n', '', '109.111.197.225', 1363269867, ''),
(470, 1, 0, 'n', '', '109.111.197.225', 1363269867, ''),
(460, 1, 0, 'n', '', '88.98.36.214', 1363190895, ''),
(461, 1, 0, 'n', '', '88.98.36.214', 1363190895, ''),
(462, 1, 0, 'n', '', '88.98.36.214', 1363190895, ''),
(463, 1, 0, 'n', '', '109.111.197.225', 1363269867, ''),
(453, 1, 0, 'n', '', '88.98.36.214', 1363190895, ''),
(469, 1, 1, 'n', 'Admin', '109.111.197.225', 1363257089, ''),
(455, 1, 0, 'n', '', '88.98.36.214', 1363190895, ''),
(454, 1, 0, 'n', '', '109.111.197.225', 1363269867, ''),
(465, 1, 0, 'n', '', '88.98.36.214', 1363190895, ''),
(468, 1, 0, 'n', '', '109.111.197.225', 1363269867, ''),
(467, 1, 0, 'n', '', '88.98.36.214', 1363190895, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_password_lockout`
--

CREATE TABLE IF NOT EXISTS `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `exp_password_lockout`
--

INSERT INTO `exp_password_lockout` (`lockout_id`, `login_date`, `ip_address`, `user_agent`, `username`) VALUES
(1, 1358332056, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(2, 1358788323, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(3, 1358933936, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(4, 1359971498, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(5, 1359971531, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'Admin'),
(6, 1359971548, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(7, 1359971744, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(8, 1360586250, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'y34r5l3y'),
(9, 1360676550, '88.98.36.214', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; GTB7.4; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.3', 'admin'),
(10, 1362504148, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(11, 1363269912, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.155 Safari/537.22', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `exp_ping_servers`
--

CREATE TABLE IF NOT EXISTS `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_remember_me`
--

CREATE TABLE IF NOT EXISTS `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_reset_password`
--

CREATE TABLE IF NOT EXISTS `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_revision_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_tools`
--

CREATE TABLE IF NOT EXISTS `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_rte_tools`
--

INSERT INTO `exp_rte_tools` (`tool_id`, `name`, `class`, `enabled`) VALUES
(1, 'Blockquote', 'Blockquote_rte', 'y'),
(2, 'Bold', 'Bold_rte', 'y'),
(3, 'Headings', 'Headings_rte', 'y'),
(4, 'Image', 'Image_rte', 'y'),
(5, 'Italic', 'Italic_rte', 'y'),
(6, 'Link', 'Link_rte', 'y'),
(7, 'Ordered List', 'Ordered_list_rte', 'y'),
(8, 'Underline', 'Underline_rte', 'y'),
(9, 'Unordered List', 'Unordered_list_rte', 'y'),
(10, 'View Source', 'View_source_rte', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_toolsets`
--

CREATE TABLE IF NOT EXISTS `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_rte_toolsets`
--

INSERT INTO `exp_rte_toolsets` (`toolset_id`, `member_id`, `name`, `tools`, `enabled`) VALUES
(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_search`
--

CREATE TABLE IF NOT EXISTS `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_search_log`
--

CREATE TABLE IF NOT EXISTS `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_security_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8993 ;

--
-- Dumping data for table `exp_security_hashes`
--

INSERT INTO `exp_security_hashes` (`hash_id`, `date`, `session_id`, `hash`) VALUES
(8992, 1363269978, 'e66bbed445956898b20017df3358320c571309a0', '09ab5fd3d1cde7380a4982de6d64f3b58ec552e8'),
(8991, 1363269973, 'e66bbed445956898b20017df3358320c571309a0', 'f86230b21e8631ec09066eeda44b1e706813fa18'),
(8990, 1363269934, 'e66bbed445956898b20017df3358320c571309a0', 'fdffb730c37477fff89831bcf36b790491388e95'),
(8989, 1363269931, 'e66bbed445956898b20017df3358320c571309a0', '4195e623285e4c0163e5d78e06c7414dd03216c1'),
(8987, 1363269924, '0', '476bef783eecc041bffe5996a4a9d11fd1773d14'),
(8988, 1363269925, 'e66bbed445956898b20017df3358320c571309a0', '551cbd8cf02cf86b7ddae3b4e1e39e9e9bf29955'),
(8985, 1363269912, '0', 'c0dbcd5e2a36e40a81bb5b664a8dcbcff1e2ee18'),
(8983, 1363266053, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '71499099e18e66ab73cead6400249c4a5110811b'),
(8982, 1363266050, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '46f973f20dcb3abc85ae42eee535311f31ae2393'),
(8981, 1363266049, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'ee5ab48955e0f1a5be08eb429cd004efc76b88b8'),
(8980, 1363266048, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '5d1d82362b26f084bc7741f5cf5e23ce210b2042'),
(8979, 1363266048, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '57471abd80a863bc0c8e5203d1ccd215a16bc693'),
(8978, 1363266047, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '1127cba04e2135cabf082bb227f52d1cb95d8e21'),
(8977, 1363266038, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'c68604c4ce16275c3ba761864bc7dbe372761e99'),
(8976, 1363266037, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'fd196f5a5e296c750ef221d9942cd38e399c3974'),
(8975, 1363266037, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'f37b599e7088a489de7438f9ac5c8f16635276c0'),
(8974, 1363266037, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'e3775291afc1837df9d524dd18bde717db1c3ecf'),
(8973, 1363266036, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '01a45d1e0dad9417630ec84a13f5111cb17e8b7e'),
(8972, 1363266035, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '513615e0c21e07d9df189016f344fd088033f00d'),
(8971, 1363266034, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'f5a80091f61d247a1f520337086d412d59a0f289'),
(8970, 1363266032, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '49f2790381fbc314daa935661f7ae4ee0e95f951'),
(8957, 1363265933, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'fdcbebac299e2e498cb3794a49f7dc925dba5f1c'),
(8956, 1363265924, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '11ea039cc180267c92abd380050335b1e5a40f0e'),
(8941, 1363265865, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '3572186fab04cf23e8b6efd3ac3c0e9e317ec430'),
(8942, 1363265876, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '470fbb4a7e43903b6f80261f7d41512cbe7abca4'),
(8943, 1363265884, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '5db140aa9c205372489a9ba422700f67477c2e56'),
(8944, 1363265897, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'ec028e913b4e13c47a70389c37ba5044998e69f8'),
(8945, 1363265898, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '14abf231c7fcd2dc11a88fe364e9be5d20b6ca4a'),
(8946, 1363265900, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '554276c90f86a4377f18942a979018942a2992c2'),
(8947, 1363265900, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '566abe6d7c36ce31fbe3e5c4eb4b97e5bcd9c1fb'),
(8948, 1363265901, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '2a2e5cb940f09d76bbb9a179d5f68319a7e3c247'),
(8949, 1363265901, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'ef6ae8d562e470af293385ffc8527ec4ddb0784c'),
(8950, 1363265905, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '35a4831e0a40c43a99bf8fc4bcd0058f28da06ae'),
(8951, 1363265906, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '0c377a9ad679a4cc9841bfe7fb86d29a801aafff'),
(8952, 1363265907, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '80c78224c2d042868ff00392bcad329d2caf4fbf'),
(8953, 1363265908, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'b8aec47684daef7f8881df273830ddbf2b6198a7'),
(8954, 1363265910, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '1bd323d3361392780cb737418f21fcbd77b4f5e4'),
(8955, 1363265923, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '10d201e515de87c28b4430203aaf78c92c56fc60'),
(8940, 1363265865, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'dab978ad5c5b73c17931d915b13c99c7b6715833'),
(8939, 1363265861, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'eb1485b654738a6bf6aa49b9c83db17b313bbb4b'),
(8938, 1363265860, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '9b7ece7a81052bd890c3158f7d3c8d6f44c9e013'),
(8937, 1363265849, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'af19c54f62149fbbb9481c9fb76afefe61508a64'),
(8936, 1363265849, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'ec8695e3b1b2abad898f2a2201cd73dae61eea32'),
(8935, 1363265849, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'd6d6846e971be0f359a0796ff1a60eaeb702c1e0'),
(8934, 1363265848, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '469f010a7d11c67865e6c8b85390badbcafd25c6'),
(8933, 1363265840, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'bae43ae99e98f61355f40a311740b36741aed392'),
(8932, 1363265840, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '2bf6dae7e6252e3b2b2812601c9abd8574b4c44b'),
(8931, 1363265839, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '0ac20b068c1059fca662c61560ac73a0f7bd3839'),
(8930, 1363265839, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '4d13a83edf44cba3697dcbc19158c4cb99004863'),
(8929, 1363265838, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'b604e49ff090ba5c472ae922730095fe75b016a3'),
(8928, 1363265836, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '66a2b948b20aba0c64a08b43bba6332f7382cd33'),
(8919, 1363265789, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '0b3f34f43de915a5abc917a1e2538e53f35f2f72'),
(8920, 1363265793, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'a431c363a58eae46e500ecd63b70534690bc493c'),
(8921, 1363265795, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '118256dcb0946df186a66f9b8ce5a2b23102cbac'),
(8922, 1363265795, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '5bc88bab16c8ba6bf98231fea46956fdccd458f4'),
(8923, 1363265795, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '490a7d112edcb3c6a5b4a71b8eaa3415e41a8a87'),
(8924, 1363265828, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'd1024039d86e2a1c92fa45ace96cb7cb5f76f1a1'),
(8925, 1363265834, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'f2c8d8180713097fa9178a00086b3a53b297bac8'),
(8926, 1363265835, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'b766d65bbcf56458a731896a71d9badca8baa485'),
(8927, 1363265836, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '18c8c750dc8aa802b79d5ac799f798f7c0462cf9'),
(8969, 1363266011, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'ff537621a6355c8d8b827af5bb18117073b4f525'),
(8968, 1363266007, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'd7c53fcbbb8ea3f9b4ba593a748c3507b40dcd6a'),
(8967, 1363266006, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '0e5fe51fb49bf67730c3e76a5d821d88ee74ed3c'),
(8966, 1363266005, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '4a3ec76045a182970788425890384dc568f246ec'),
(8965, 1363266005, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'bca8e90ff72a8f5a7896e12056089c5733ec76b2'),
(8964, 1363266003, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '6055a8576325e5396225b3f5afa8a322a92c75d5'),
(8963, 1363265940, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'ac2f16bf3d64291fa8cec1f194d3728b0cba547d'),
(8962, 1363265939, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'a19cb2f3815981ddfe52fbcbc8e81cfad7723b62'),
(8961, 1363265939, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '8244ce114638794965b27e38988120c824748f2e'),
(8960, 1363265939, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '73befa5d83d77b6133f0f9fb961c15cb6b5d769a'),
(8959, 1363265937, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', '59a5e33c6e360514976f50c2d2e39d20c909c5bf'),
(8958, 1363265935, 'a108766e23f8cd8aae367733834d0c8e3e0b15cf', 'dfbd0ce6b2caadc7450847970f15791f54e04b23'),
(8918, 1363265789, '0', 'dcefd7f5f0173c032404a68844175cf7bd4ff754'),
(8916, 1363263137, '0', 'b153a4d2a07f5479bd4d9cf18d1df9d5f2bdd787'),
(8915, 1363263136, '0', 'c4eea1dca83528283ae1b8ee79cec5001b9776a9');

-- --------------------------------------------------------

--
-- Table structure for table `exp_sessions`
--

CREATE TABLE IF NOT EXISTS `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_sessions`
--

INSERT INTO `exp_sessions` (`session_id`, `member_id`, `admin_sess`, `ip_address`, `user_agent`, `last_activity`) VALUES
('e66bbed445956898b20017df3358320c571309a0', 1, 1, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.155 Safari/537.22', 1363269979),
('a108766e23f8cd8aae367733834d0c8e3e0b15cf', 1, 1, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.155 Safari/537.22', 1363266053);

-- --------------------------------------------------------

--
-- Table structure for table `exp_sites`
--

CREATE TABLE IF NOT EXISTS `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` longtext,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_sites`
--

INSERT INTO `exp_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`, `site_pages`) VALUES
(1, 'Yearsley Group', 'default_site', 'Yearsley Main Group Site', 'YTo5Mzp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MzM6Imh0dHA6Ly95ZWFyc2xleS5zdG1wcmV2aWV3LmNvLnVrLyI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czo0MDoiaHR0cDovL3llYXJzbGV5LnN0bXByZXZpZXcuY28udWsvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjE3OiJockB5ZWFyc2xleS5jby51ayI7czoxNDoid2VibWFzdGVyX25hbWUiO3M6MDoiIjtzOjIwOiJjaGFubmVsX25vbWVuY2xhdHVyZSI7czo3OiJjaGFubmVsIjtzOjEwOiJtYXhfY2FjaGVzIjtzOjM6IjE1MCI7czoxMToiY2FwdGNoYV91cmwiO3M6NDk6Imh0dHA6Ly95ZWFyc2xleS5zdG1wcmV2aWV3LmNvLnVrL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfcGF0aCI7czo2OToiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy95ZWFyc2xleS9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX2ZvbnQiO3M6MToieSI7czoxMjoiY2FwdGNoYV9yYW5kIjtzOjE6InkiO3M6MjM6ImNhcHRjaGFfcmVxdWlyZV9tZW1iZXJzIjtzOjE6InkiO3M6MTc6ImVuYWJsZV9kYl9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImVuYWJsZV9zcWxfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJmb3JjZV9xdWVyeV9zdHJpbmciO3M6MToibiI7czoxMzoic2hvd19wcm9maWxlciI7czoxOiJuIjtzOjE4OiJ0ZW1wbGF0ZV9kZWJ1Z2dpbmciO3M6MToibiI7czoxNToiaW5jbHVkZV9zZWNvbmRzIjtzOjE6Im4iO3M6MTM6ImNvb2tpZV9kb21haW4iO3M6MDoiIjtzOjExOiJjb29raWVfcGF0aCI7czowOiIiO3M6MTc6InVzZXJfc2Vzc2lvbl90eXBlIjtzOjE6ImMiO3M6MTg6ImFkbWluX3Nlc3Npb25fdHlwZSI7czoyOiJjcyI7czoyMToiYWxsb3dfdXNlcm5hbWVfY2hhbmdlIjtzOjE6InkiO3M6MTg6ImFsbG93X211bHRpX2xvZ2lucyI7czoxOiJ5IjtzOjE2OiJwYXNzd29yZF9sb2Nrb3V0IjtzOjE6InkiO3M6MjU6InBhc3N3b3JkX2xvY2tvdXRfaW50ZXJ2YWwiO3M6MToiMSI7czoyMDoicmVxdWlyZV9pcF9mb3JfbG9naW4iO3M6MToieSI7czoyMjoicmVxdWlyZV9pcF9mb3JfcG9zdGluZyI7czoxOiJ5IjtzOjI0OiJyZXF1aXJlX3NlY3VyZV9wYXNzd29yZHMiO3M6MToibiI7czoxOToiYWxsb3dfZGljdGlvbmFyeV9wdyI7czoxOiJ5IjtzOjIzOiJuYW1lX29mX2RpY3Rpb25hcnlfZmlsZSI7czowOiIiO3M6MTc6Inhzc19jbGVhbl91cGxvYWRzIjtzOjE6InkiO3M6MTU6InJlZGlyZWN0X21ldGhvZCI7czo4OiJyZWRpcmVjdCI7czo5OiJkZWZ0X2xhbmciO3M6NzoiZW5nbGlzaCI7czo4OiJ4bWxfbGFuZyI7czoyOiJlbiI7czoxMjoic2VuZF9oZWFkZXJzIjtzOjE6InkiO3M6MTE6Imd6aXBfb3V0cHV0IjtzOjE6Im4iO3M6MTM6ImxvZ19yZWZlcnJlcnMiO3M6MToibiI7czoxMzoibWF4X3JlZmVycmVycyI7czozOiI1MDAiO3M6MTE6InRpbWVfZm9ybWF0IjtzOjI6InVzIjtzOjE1OiJzZXJ2ZXJfdGltZXpvbmUiO3M6MzoiVVRDIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJ5IjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6MzoiVVRDIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjE6InkiO3M6MTU6Imhvbm9yX2VudHJ5X2RzdCI7czoxOiJ5IjtzOjEzOiJtYWlsX3Byb3RvY29sIjtzOjg6InNlbmRtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6InkiO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czozMToiaHR0cDovL3llYXJzbGV5L2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo2MDoiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy95ZWFyc2xleS90aGVtZXMvIjtzOjEwOiJpc19zaXRlX29uIjtzOjE6InkiO3M6MTE6InJ0ZV9lbmFibGVkIjtzOjE6InkiO3M6MjI6InJ0ZV9kZWZhdWx0X3Rvb2xzZXRfaWQiO3M6MToiMSI7czo2OiJjcF91cmwiO3M6MjU6Imh0dHA6Ly95ZWFyc2xleS9hZG1pbi5waHAiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6NDg6Imh0dHA6Ly95ZWFyc2xleS5zdG1wcmV2aWV3LmNvLnVrL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6Njg6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMveWVhcnNsZXkvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjU0OiJodHRwOi8veWVhcnNsZXkuc3RtcHJldmlldy5jby51ay9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NzQ6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMveWVhcnNsZXkvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NjI6Imh0dHA6Ly95ZWFyc2xleS5zdG1wcmV2aWV3LmNvLnVrL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6ODI6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMveWVhcnNsZXkvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo3NToiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy95ZWFyc2xleS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJ5IjtzOjg6InNpdGVfNDA0IjtzOjEyOiJ5ZWFyc2xleS80MDQiO3M6MTk6InNhdmVfdG1wbF9yZXZpc2lvbnMiO3M6MToibiI7czoxODoibWF4X3RtcGxfcmV2aXNpb25zIjtzOjE6IjUiO3M6MTU6InNhdmVfdG1wbF9maWxlcyI7czoxOiJ5IjtzOjE4OiJ0bXBsX2ZpbGVfYmFzZXBhdGgiO3M6NjI6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMveWVhcnNsZXkvdGVtcGxhdGVzIjt9', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NjI6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMveWVhcnNsZXkvaW5kZXgucGhwIjtzOjMyOiIwZmVkOTZmM2IyYzc3ZDZiMzhjNzM4YWQ0OTE0NDU0ZSI7fQ==', 'YToxOntpOjE7YTozOntzOjM6InVybCI7czozMzoiaHR0cDovL3llYXJzbGV5LnN0bXByZXZpZXcuY28udWsvIjtzOjQ6InVyaXMiO2E6NTg6e2k6MzI7czozMjoiL3RoZS15ZWFyc2xleS1ncm91cC1vZi1jb21wYW5pZXMiO2k6MTtzOjk6Ii9hYm91dC11cyI7aTo3O3M6Mjg6Ii9hYm91dC11cy9idXNpbmVzcy1zdHJ1Y3R1cmUiO2k6Njc7czoxOToiL2Fib3V0LXVzL2RpcmVjdG9ycyI7aTo5O3M6MTk6Ii9hYm91dC11cy9sb2NhdGlvbnMiO2k6MTY7czoxNzoiL2Fib3V0LXVzL2hpc3RvcnkiO2k6MjtzOjEzOiIvb3VyLWJ1c2luZXNzIjtpOjI1O3M6MjM6Ii9vdXItYnVzaW5lc3MvbG9naXN0aWNzIjtpOjI2O3M6MjQ6Ii9vdXItYnVzaW5lc3MvZm9vZC1zYWxlcyI7aToyNztzOjIyOiIvb3VyLWJ1c2luZXNzL2JlbGZpZWxkIjtpOjI4O3M6MjA6Ii9vdXItYnVzaW5lc3MvaWNlcGFrIjtpOjI5O3M6MjM6Ii9vdXItYnVzaW5lc3MvbHVja3ktcmVkIjtpOjM7czoxMjoiL2NzZXItcGxlZGdlIjtpOjIzO3M6MzA6Ii9jc2VyLXBsZWRnZS9jYXVzZXMtd2Utc3VwcG9ydCI7aToyNDtzOjQ2OiIvY3Nlci1wbGVkZ2Uvb3VyLWNvbW1pdG1lbnQtdG8tdGhlLWVudmlyb25tZW50IjtpOjQ7czo1OiIvbmV3cyI7aTozMDtzOjEzOiIvbmV3cy9hcmNoaXZlIjtpOjU7czo4OiIvY2FyZWVycyI7aTo2O3M6MTE6Ii9jb250YWN0LXVzIjtpOjMxO3M6MTA6Ii90aGFuay15b3UiO2k6NDM7czo3NDoiL25ld3MvMTUtbWlsbGlvbi1ob3QtY3Jvc3MtYnVucy13ZXJlLWluLXllYXJzbGV5LWdyb3Vwcy1zdG9yZXMtdGhpcy1lYXN0ZXIiO2k6NDQ7czo2MzoiL25ld3MvYmVsZmllbGRzLW5ldy1tci1raW5ncy1yYW5nZS1pcy1ydWxpbmctdGhlLWRlc3NlcnQtbWFya2V0IjtpOjQ1O3M6NDg6Ii9uZXdzL3llYXJzbGV5LWdyb3VwLWludmVzdC0wNW0tYXQtZ3JpbXNieS1kZXBvdCI7aTo0NjtzOjU0OiIvbmV3cy8yMG0tZXhwYW5zaW9uLXBsYW4tZm9yLXllYXJzbGV5LWdyb3VwLWF0LWhleXdvb2QiO2k6NDc7czo1NjoiL25ld3MveWVhcnNsZXktZ3JvdXAtYWNxdWlyZXMtc2VhZm9vZC1zcGVjaWFsaXN0LWljZS1wYWsiO2k6NDg7czo0OToiL25ld3MvbmV3LWxvZ2lzdGljcy1zcGVjaWFsaXN0LWZvci15ZWFyc2xleS1ncm91cCI7aTo0OTtzOjcyOiIvbmV3cy95ZWFyc2xleS1ncm91cC1zdXBwb3J0cy1tYW51ZmFjdHVyZXJzLXdpdGgtYmZmZi1hd2FyZHMtc3BvbnNvcnNoaXAiO2k6NTA7czo1NToiL25ld3MveWVhcnNsZXktZ3JvdXAtd2lucy1uZXctZWR1Y2F0aW9uLXN1cHBseS1jb250cmFjdCI7aTo1MTtzOjYyOiIvbmV3cy9uZXctYW1iaWVudC1zdG9yYWdlLW9mZmVyaW5nLWZyb20tZnJvemVuLWZvb2Qtc3BlY2lhbGlzdCI7aTo1MjtzOjQ5OiIvbmV3cy95ZWFyc2xleS1ncm91cC1zd2l0Y2hlcy1vbi1yZW5ld2FibGUtZW5lcmd5IjtpOjUzO3M6NTY6Ii9uZXdzL25ldy10cnVja3MtdG8taGVscC15ZWFyc2xleS1ncm91cC1yZWR1Y2UtZW1pc3Npb25zIjtpOjU0O3M6NjU6Ii9uZXdzL2FncmVlbWVudC13aXRoLXRlc2NvLWdpdmVzLXllYXJzbGV5LWN1c3RvbWVycy1hLWxpdHRsZS1oZWxwIjtpOjU1O3M6MzQ6Ii9uZXdzL3llYXJzbGV5LWdyb3Vwcy1nb2luZy1nbG9iYWwiO2k6NTY7czo1MToiL25ld3MveW91bmdzLXRvLXN0b3JlLWRyeS1nb29kcy13aXRoLXllYXJzbGV5LWdyb3VwIjtpOjU3O3M6Mzg6Ii9uZXdzL2ZyZWV6ZS1mcmFtZS13aXRoLXllYXJzbGV5LWdyb3VwIjtpOjU4O3M6NTQ6Ii9uZXdzL3llYXJzbGV5LWdyb3VwLXRvLXN0YXJ0LTIwbS1leHBhbnNpb24tYXQtaGV5d29vZCI7aTo1OTtzOjUwOiIvbmV3cy9uZXctcHJvZHVjdHMtYW5kLWRpcmVjdC1zb3VyY2luZy1mcm9tLWljZXBhayI7aTo2MDtzOjQ4OiIvbmV3cy9oYXJyeS15ZWFyc2xleS10YWxrcy13aXRoLWluc2lkZXItbWFnYXppbmUiO2k6NjE7czo1MjoiL25ld3MveWVhcnNsZXktZ3JvdXAtc3RhcnRzLTIwbS1leHBhbnNpb24tYXQtaGV5d29vZCI7aTo2MjtzOjQ4OiIvbmV3cy95ZWFyc2xleS1ncm91cC10YWtlcy1vbi1uZXctbWlkbGFuZHMtZGVwb3QiO2k6NjM7czo2MjoiL25ld3MveWVhcnNsZXktZ3JvdXAtMzUtbWlsbGlvbi1pbnZlc3RtZW50LWluLXJlbmV3YWJsZS1lbmVyZ3kiO2k6NjQ7czo3MDoiL25ld3MveWVhcnNsZXktZ3JvdXAtYWNxdWlyZS1idXNpbmVzcy1hc3NldHMtYW5kLWJ1aWxkaW5nLWF0LWhhbXMtaGFsbCI7aTo2NTtzOjQyOiIvbmV3cy95ZWFyc2xleS1sb2dpc3RpY3Mtd2lucy1jc20tY29udHJhY3QiO2k6NjY7czo4MToiL25ld3MveWVhcnNsZXktZ3JvdXAtaW52aXRlLXNjaG9vbC1jaGlsZHJlbi10by10aW1lLWNhcHN1bGUtYnVyaWFsLWF0LWZyb3plbi1zdXBlIjtpOjE4O3M6MjQ6Ii9jYXJlZXJzL2FjY291bnQtbWFuYWdlciI7aToyMjtzOjI0OiIvY2FyZWVycy9wcm9qZWN0LW1hbmFnZXIiO2k6Njg7czoxNzoiL25ld3MvbmV3c2xldHRlcnMiO2k6Njk7czoyMToiL3Rlcm1zLWFuZC1jb25kaXRpb25zIjtpOjcwO3M6MTM6Ii9jb29raWUtdXNhZ2UiO2k6NzQ7czo4MToiL25ld3MvbmV3LWJyYW5kLWlkZW50aXR5LWZvci11a3MtbGVhZGluZy1mcm96ZW4tZm9vZC1zYWxlcy1hbmQtbG9naXN0aWNzLW9wZXJhdG9yIjtpOjczO3M6MTM6Ii9uZXdzbGV0dGVyLTIiO2k6NzI7czoxMzoiL25ld3NsZXR0ZXItMSI7aTo3NjtzOjIxOiIvdGhhbmsteW91LW5ld3NsZXR0ZXIiO2k6Nzc7czoxNzoiL2NhcmVlcnMtdGhhbmt5b3UiO2k6Nzg7czoxNToiL25ldHN0b2NrLWxvZ2luIjtpOjc5O3M6MTU6Ii9lbXBsb3llZS1sb2dpbiI7aTo4MDtzOjg6Ii9zaXRlbWFwIjtpOjg7czoxMDoiL2RpcmVjdG9ycyI7fXM6OToidGVtcGxhdGVzIjthOjU4OntpOjE7czoxOiI0IjtpOjI7czoyOiIyNSI7aTozO3M6MjoiMTQiO2k6NDtzOjE6IjgiO2k6NTtzOjE6IjciO2k6NjtzOjE6IjYiO2k6NztzOjI6IjE4IjtpOjk7czoyOiIyMCI7aToxNjtzOjI6IjIyIjtpOjE4O3M6MjoiMTMiO2k6MjI7czoyOiIxMyI7aToyMztzOjI6IjE0IjtpOjI0O3M6MjoiMTQiO2k6MjU7czoyOiIxMCI7aToyNjtzOjI6IjEwIjtpOjI3O3M6MjoiMTAiO2k6Mjg7czoyOiIxMCI7aToyOTtzOjI6IjEwIjtpOjMwO3M6MjoiMTUiO2k6MzE7czoyOiIyNiI7aTozMjtzOjE6IjEiO2k6NDM7czoyOiIxMSI7aTo0NDtzOjI6IjExIjtpOjQ1O3M6MjoiMTEiO2k6NDY7czoyOiIxMSI7aTo0NztzOjI6IjExIjtpOjQ4O3M6MjoiMTEiO2k6NDk7czoyOiIxMSI7aTo1MDtzOjI6IjExIjtpOjUxO3M6MjoiMTEiO2k6NTI7czoyOiIxMSI7aTo1MztzOjI6IjExIjtpOjU0O3M6MjoiMTEiO2k6NTU7czoyOiIxMSI7aTo1NjtzOjI6IjExIjtpOjU3O3M6MjoiMTEiO2k6NTg7czoyOiIxMSI7aTo1OTtzOjI6IjExIjtpOjYwO3M6MjoiMTEiO2k6NjE7czoyOiIxMSI7aTo2MjtzOjI6IjExIjtpOjYzO3M6MjoiMTEiO2k6NjQ7czoyOiIxMSI7aTo2NTtzOjI6IjExIjtpOjY2O3M6MjoiMTEiO2k6Njc7czoyOiIxNiI7aTo2ODtzOjE6IjUiO2k6Njk7czoyOiIyMyI7aTo3MDtzOjI6IjI0IjtpOjc0O3M6MjoiMTEiO2k6NzM7czoxOiI4IjtpOjcyO3M6MToiOCI7aTo3NjtzOjI6IjMyIjtpOjc3O3M6MjoiMjciO2k6Nzg7czoyOiIyOCI7aTo3OTtzOjI6IjMwIjtpOjgwO3M6MjoiMzEiO2k6ODtzOjE6IjUiO319fQ=='),
(2, 'Yearsley Logistics', 'logistics', 'Yearsley Logistics', 'YTo5Mjp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjk6ImluZGV4LnBocCI7czo4OiJzaXRlX3VybCI7czoyNjoiaHR0cDovL3llYXJzbGV5LWxvZ2lzdGljcy8iO3M6NjoiY3BfdXJsIjtzOjI1OiJodHRwOi8veWVhcnNsZXkvYWRtaW4ucGhwIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjIzOiJodHRwOi8veWVhcnNsZXkvdGhlbWVzLyI7czoxNzoidGhlbWVfZm9sZGVyX3BhdGgiO3M6NzU6Ii9Vc2Vycy9qb25ueWZyb2RzaGFtL3dvcmsvc2hvb3RfdGhlX21vb24veWVhcnNsZXlfbXNtL3llYXJzbGV5L3NpdGUvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjIyOiJqb25ueUBoaWNvbnRyYXN0LmNvLnVrIjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czozMjoiaHR0cDovL3llYXJzbGV5L2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfcGF0aCI7czo3MToiL1VzZXJzL2pvbm55ZnJvZHNoYW0vd29yay9zaG9vdF90aGVfbW9vbi95ZWFyc2xleS9zaXRlL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czozOiJVVEMiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6InkiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czozOiJVVEMiO3M6MTY6ImRlZmF1bHRfc2l0ZV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czozMToiaHR0cDovL3llYXJzbGV5L2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzE6Imh0dHA6Ly95ZWFyc2xleS9pbWFnZXMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjcwOiIvVXNlcnMvam9ubnlmcm9kc2hhbS93b3JrL3Nob290X3RoZV9tb29uL3llYXJzbGV5L3NpdGUvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjM3OiJodHRwOi8veWVhcnNsZXkvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjEwOiJwaG90b19wYXRoIjtzOjc2OiIvVXNlcnMvam9ubnlmcm9kc2hhbS93b3JrL3Nob290X3RoZV9tb29uL3llYXJzbGV5L3NpdGUvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NDU6Imh0dHA6Ly95ZWFyc2xleS9pbWFnZXMvc2lnbmF0dXJlX2F0dGFjaG1lbnRzLyI7czoxMjoic2lnX2ltZ19wYXRoIjtzOjg0OiIvVXNlcnMvam9ubnlmcm9kc2hhbS93b3JrL3Nob290X3RoZV9tb29uL3llYXJzbGV5L3NpdGUvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo3NzoiL1VzZXJzL2pvbm55ZnJvZHNoYW0vd29yay9zaG9vdF90aGVfbW9vbi95ZWFyc2xleS9zaXRlL2ltYWdlcy9wbV9hdHRhY2htZW50cy8iO3M6MjM6InBydl9tc2dfbWF4X2F0dGFjaG1lbnRzIjtzOjE6IjMiO3M6MjI6InBydl9tc2dfYXR0YWNoX21heHNpemUiO3M6MzoiMjUwIjtzOjIwOiJwcnZfbXNnX2F0dGFjaF90b3RhbCI7czozOiIxMDAiO3M6MTk6InBydl9tc2dfaHRtbF9mb3JtYXQiO3M6NDoic2FmZSI7czoxODoicHJ2X21zZ19hdXRvX2xpbmtzIjtzOjE6InkiO3M6MTc6InBydl9tc2dfbWF4X2NoYXJzIjtzOjQ6IjYwMDAiO3M6MTk6Im1lbWJlcmxpc3Rfb3JkZXJfYnkiO3M6MTE6InRvdGFsX3Bvc3RzIjtzOjIxOiJtZW1iZXJsaXN0X3NvcnRfb3JkZXIiO3M6NDoiZGVzYyI7czoyMDoibWVtYmVybGlzdF9yb3dfbGltaXQiO3M6MjoiMjAiO30=', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToieSI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjkwOiIvVXNlcnMvam9ubnlmcm9kc2hhbS93b3JrL3Nob290X3RoZV9tb29uL3llYXJzbGV5L1lFMTIxMTAxLVdlYnNpdGVzL3d3dy95ZWFyc2xleS1sb2dpc3RpY3MiO3M6ODoic2l0ZV80MDQiO3M6MDoiIjtzOjE5OiJzYXZlX3RtcGxfcmV2aXNpb25zIjtzOjE6Im4iO3M6MTg6Im1heF90bXBsX3JldmlzaW9ucyI7czoxOiI1IjtzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjt9', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToxOntzOjc6ImVtYWlsZWQiO2E6MDp7fX0=', 'YToxOntpOjI7YToxOntzOjM6InVybCI7czozNjoiaHR0cDovL3llYXJzbGV5LWxvZ2lzdGljcy9pbmRleC5waHAvIjt9fQ=='),
(3, 'Yearsley Food Sales', 'food_sales', 'Yearsley Food Sales', 'YTo5Mjp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjk6ImluZGV4LnBocCI7czo4OiJzaXRlX3VybCI7czoxNjoiaHR0cDovL3llYXJzbGV5LyI7czo2OiJjcF91cmwiO3M6MjU6Imh0dHA6Ly95ZWFyc2xleS9hZG1pbi5waHAiO3M6MTY6InRoZW1lX2ZvbGRlcl91cmwiO3M6MjM6Imh0dHA6Ly95ZWFyc2xleS90aGVtZXMvIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo2MjoiL1VzZXJzL2pvbm55ZnJvZHNoYW0vd29yay9zaG9vdF90aGVfbW9vbi95ZWFyc2xleS9zaXRlL3RoZW1lcy8iO3M6MTU6IndlYm1hc3Rlcl9lbWFpbCI7czoyMjoiam9ubnlAaGljb250cmFzdC5jby51ayI7czoxNDoid2VibWFzdGVyX25hbWUiO3M6MDoiIjtzOjIwOiJjaGFubmVsX25vbWVuY2xhdHVyZSI7czo3OiJjaGFubmVsIjtzOjEwOiJtYXhfY2FjaGVzIjtzOjM6IjE1MCI7czoxMToiY2FwdGNoYV91cmwiO3M6MzI6Imh0dHA6Ly95ZWFyc2xleS9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NzE6Ii9Vc2Vycy9qb25ueWZyb2RzaGFtL3dvcmsvc2hvb3RfdGhlX21vb24veWVhcnNsZXkvc2l0ZS9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX2ZvbnQiO3M6MToieSI7czoxMjoiY2FwdGNoYV9yYW5kIjtzOjE6InkiO3M6MjM6ImNhcHRjaGFfcmVxdWlyZV9tZW1iZXJzIjtzOjE6Im4iO3M6MTc6ImVuYWJsZV9kYl9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImVuYWJsZV9zcWxfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJmb3JjZV9xdWVyeV9zdHJpbmciO3M6MToibiI7czoxMzoic2hvd19wcm9maWxlciI7czoxOiJuIjtzOjE4OiJ0ZW1wbGF0ZV9kZWJ1Z2dpbmciO3M6MToibiI7czoxNToiaW5jbHVkZV9zZWNvbmRzIjtzOjE6Im4iO3M6MTM6ImNvb2tpZV9kb21haW4iO3M6MDoiIjtzOjExOiJjb29raWVfcGF0aCI7czowOiIiO3M6MTc6InVzZXJfc2Vzc2lvbl90eXBlIjtzOjE6ImMiO3M6MTg6ImFkbWluX3Nlc3Npb25fdHlwZSI7czoyOiJjcyI7czoyMToiYWxsb3dfdXNlcm5hbWVfY2hhbmdlIjtzOjE6InkiO3M6MTg6ImFsbG93X211bHRpX2xvZ2lucyI7czoxOiJ5IjtzOjE2OiJwYXNzd29yZF9sb2Nrb3V0IjtzOjE6InkiO3M6MjU6InBhc3N3b3JkX2xvY2tvdXRfaW50ZXJ2YWwiO3M6MToiMSI7czoyMDoicmVxdWlyZV9pcF9mb3JfbG9naW4iO3M6MToieSI7czoyMjoicmVxdWlyZV9pcF9mb3JfcG9zdGluZyI7czoxOiJ5IjtzOjI0OiJyZXF1aXJlX3NlY3VyZV9wYXNzd29yZHMiO3M6MToibiI7czoxOToiYWxsb3dfZGljdGlvbmFyeV9wdyI7czoxOiJ5IjtzOjIzOiJuYW1lX29mX2RpY3Rpb25hcnlfZmlsZSI7czowOiIiO3M6MTc6Inhzc19jbGVhbl91cGxvYWRzIjtzOjE6InkiO3M6MTU6InJlZGlyZWN0X21ldGhvZCI7czo4OiJyZWRpcmVjdCI7czo5OiJkZWZ0X2xhbmciO3M6NzoiZW5nbGlzaCI7czo4OiJ4bWxfbGFuZyI7czoyOiJlbiI7czoxMjoic2VuZF9oZWFkZXJzIjtzOjE6InkiO3M6MTE6Imd6aXBfb3V0cHV0IjtzOjE6Im4iO3M6MTM6ImxvZ19yZWZlcnJlcnMiO3M6MToibiI7czoxMzoibWF4X3JlZmVycmVycyI7czozOiI1MDAiO3M6MTE6InRpbWVfZm9ybWF0IjtzOjI6InVzIjtzOjE1OiJzZXJ2ZXJfdGltZXpvbmUiO3M6MzoiVVRDIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJ5IjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6MzoiVVRDIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6MzE6Imh0dHA6Ly95ZWFyc2xleS9pbWFnZXMvc21pbGV5cy8iO3M6MTk6InJlY291bnRfYmF0Y2hfdG90YWwiO3M6NDoiMTAwMCI7czoxNzoibmV3X3ZlcnNpb25fY2hlY2siO3M6MToieSI7czoxNzoiZW5hYmxlX3Rocm90dGxpbmciO3M6MToibiI7czoxNzoiYmFuaXNoX21hc2tlZF9pcHMiO3M6MToieSI7czoxNDoibWF4X3BhZ2VfbG9hZHMiO3M6MjoiMTAiO3M6MTM6InRpbWVfaW50ZXJ2YWwiO3M6MToiOCI7czoxMjoibG9ja291dF90aW1lIjtzOjI6IjMwIjtzOjE1OiJiYW5pc2htZW50X3R5cGUiO3M6NzoibWVzc2FnZSI7czoxNDoiYmFuaXNobWVudF91cmwiO3M6MDoiIjtzOjE4OiJiYW5pc2htZW50X21lc3NhZ2UiO3M6NTA6IllvdSBoYXZlIGV4Y2VlZGVkIHRoZSBhbGxvd2VkIHBhZ2UgbG9hZCBmcmVxdWVuY3kuIjtzOjE3OiJlbmFibGVfc2VhcmNoX2xvZyI7czoxOiJ5IjtzOjE5OiJtYXhfbG9nZ2VkX3NlYXJjaGVzIjtzOjM6IjUwMCI7czoxMToicnRlX2VuYWJsZWQiO3M6MToieSI7czoyMjoicnRlX2RlZmF1bHRfdG9vbHNldF9pZCI7czoxOiIxIjt9', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzE6Imh0dHA6Ly95ZWFyc2xleS9pbWFnZXMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjcwOiIvVXNlcnMvam9ubnlmcm9kc2hhbS93b3JrL3Nob290X3RoZV9tb29uL3llYXJzbGV5L3NpdGUvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjM3OiJodHRwOi8veWVhcnNsZXkvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjEwOiJwaG90b19wYXRoIjtzOjc2OiIvVXNlcnMvam9ubnlmcm9kc2hhbS93b3JrL3Nob290X3RoZV9tb29uL3llYXJzbGV5L3NpdGUvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NDU6Imh0dHA6Ly95ZWFyc2xleS9pbWFnZXMvc2lnbmF0dXJlX2F0dGFjaG1lbnRzLyI7czoxMjoic2lnX2ltZ19wYXRoIjtzOjg0OiIvVXNlcnMvam9ubnlmcm9kc2hhbS93b3JrL3Nob290X3RoZV9tb29uL3llYXJzbGV5L3NpdGUvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo3NzoiL1VzZXJzL2pvbm55ZnJvZHNoYW0vd29yay9zaG9vdF90aGVfbW9vbi95ZWFyc2xleS9zaXRlL2ltYWdlcy9wbV9hdHRhY2htZW50cy8iO3M6MjM6InBydl9tc2dfbWF4X2F0dGFjaG1lbnRzIjtzOjE6IjMiO3M6MjI6InBydl9tc2dfYXR0YWNoX21heHNpemUiO3M6MzoiMjUwIjtzOjIwOiJwcnZfbXNnX2F0dGFjaF90b3RhbCI7czozOiIxMDAiO3M6MTk6InBydl9tc2dfaHRtbF9mb3JtYXQiO3M6NDoic2FmZSI7czoxODoicHJ2X21zZ19hdXRvX2xpbmtzIjtzOjE6InkiO3M6MTc6InBydl9tc2dfbWF4X2NoYXJzIjtzOjQ6IjYwMDAiO3M6MTk6Im1lbWJlcmxpc3Rfb3JkZXJfYnkiO3M6MTE6InRvdGFsX3Bvc3RzIjtzOjIxOiJtZW1iZXJsaXN0X3NvcnRfb3JkZXIiO3M6NDoiZGVzYyI7czoyMDoibWVtYmVybGlzdF9yb3dfbGltaXQiO3M6MjoiMjAiO30=', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToibiI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjA6IiI7czo4OiJzaXRlXzQwNCI7czowOiIiO3M6MTk6InNhdmVfdG1wbF9yZXZpc2lvbnMiO3M6MToibiI7czoxODoibWF4X3RtcGxfcmV2aXNpb25zIjtzOjE6IjUiO3M6MTE6InN0cmljdF91cmxzIjtzOjE6Im4iO30=', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToxOntzOjc6ImVtYWlsZWQiO2E6MDp7fX0=', 'YToxOntpOjM7YToxOntzOjM6InVybCI7czoyNjoiaHR0cDovL3llYXJzbGV5L2luZGV4LnBocC8iO319');

-- --------------------------------------------------------

--
-- Table structure for table `exp_snippets`
--

CREATE TABLE IF NOT EXISTS `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `exp_snippets`
--

INSERT INTO `exp_snippets` (`snippet_id`, `site_id`, `snippet_name`, `snippet_contents`, `sync_time`) VALUES
(2, 1, 'header', '<!DOCTYPE html>\n<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->\n<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->\n<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">\n		<link rel="alternate" type="application/rss+xml" title="RSS Feed for Yearsley Group" href="/rss">\n        <title></title>\n        <meta name="description" content="">\n        <meta name="viewport" content="width=device-width">\n        <link rel="stylesheet" href="/css/normalize.css">\n        <link rel="stylesheet" href="/css/main.css">\n		<!--[if lt IE 10]>\n            <link rel="stylesheet" href="/css/ie.css">\n        <![endif]-->\n        {if segment_2 =="locations"}\n        <link rel="stylesheet" href="/css/locations.css">\n        {/if}\n		{if segment_2 =="history"}\n		<link href="/css/timeline.css" rel="stylesheet" type="text/css" />\n		{/if}\n		<script src="/js/vendor/modernizr-2.6.2.min.js"></script>\n	</head>\n    <body>\n        <!--[if lt IE 7]>\n            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>\n        <![endif]-->\n\n			<header class="clearfix">\n				\n				<div class="breadcrumbs">\n					<div class="nine60">\n						<p>You are here: {if segment_1 == ""}Home{/if}{if segment_1 AND segment_2 == ""}<a href="/">Home</a> > {title}{/if}{if segment_2}<a href="/">Home</a> > <a href="/{segment_1}">{structure:parent:title}</a> > {title}{/if}</p>\n					</div>\n				</div>\n				\n				<div class="company">\n					<div class="nine60">\n						<p class="logo"><a href="/"><img src="/stat/yearsley-group-logo.png" alt="Yearsley Group" /></a></p>\n						<div id="right">\n							<div id="login">\n								<div class="login-button"><a href="/netstock-login"><img src="/stat/login-netstock.jpg" alt="Netstock Login" />Netstock Login</a></div>\n							</div>\n							<br />\n							<!--<p class="contact clearfix">Customer Services: 01706 694600</p>-->\n						</div>\n					</div>\n				</div>\n				<nav class="clearfix">\n					<div class="nine60" class="clearfix">\n						<ul>\n							<li><a href="/" {if segment_1 == ''''}class="selected"{/if}>Home</a></li>\n							<li><a href="/about-us" {if segment_1 == ''about-us''}class="selected"{/if}>About Us</a></li>\n							<li><a href="/our-business" {if segment_1 == ''our-business''}class="selected"{/if}>Our Business</a>\n								<ul>\n									<li><a href="/">Test</a></li>\n								</ul>\n							</li>\n						</ul>\n						<ul>\n							<li><a href="/cser-pledge" {if segment_1 == ''cser-pledge''}class="selected"{/if}>CSER Pledge</a></li>\n							<li><a href="/news" {if segment_1 == ''news''}class="selected"{/if}>News and Media</a></li>\n							<li><a href="/careers" {if segment_1 == ''careers''}class="selected"{/if}>Careers</a></li>\n							<li><a href="/contact-us" {if segment_1 == ''contact-us''}class="selected"{/if}>Contact Us</a></li>\n						</ul>\n					</div>\n				</nav>\n			</header>', '2013-03-05 15:16:18'),
(3, 1, 'footer', '			<div id="truckbg"></div>\n			<footer>\n				\n				{if segment_1 =="news" OR segment_1 =="contact-us"}\n\n				<div id="newsletter">\n					{exp:freeform:form collection="Newsletter" form:class="newsletter fl" required="email" return="/thank-you-newsletter" send_user_email="yes" user_email_template="newslettersignup" prevent_duplicate_on="email"}\n						<label for="newsletter">Want news updates in your inbox from the Yearsley Group?  Enter your email address...</label>\n						<input id="email" name="email" type="email" placeholder="example@domain.com" required>\n						<button type="submit"><img src="/stat/newsletter-submit.jpg" /></button>\n					{/exp:freeform:form}\n				</div>\n				{/if}				\n				\n				<div class="info">\n					<div class="nine60">\n						<ul>\n							<li><a href="/news">News and Media</a></li>\n							<!--<li><a href="/rss">RSS Feed</a></li>-->\n							<!--<li><a href="/terms-and-conditions">Terms and Conditions</a></li>-->\n							{if segment_1 =="about-us"}\n								<li><a href="/employee-login">Employee Login</a></li>\n							{/if}\n							<li><a href="/cookie-usage">Cookie Usage</a></li>\n							<li><a href="/sitemap">Sitemap</a></li>\n						</ul>\n						<p class="stm">Website design &amp; build by: <a href="http://www.shoot-the-moon.co.uk/" target="_blank">Shoot the Moon Digital</a></p>\n					</div>\n				</div>\n				<div class="base">\n					<div class="nine60">\n						<p class="vcard">\n							&copy <span class="org">Harry Yearsley Ltd</span>\n							<span class="adr">\n								<span class="street-address">Hareshill Road</span>\n								<span class="locality">Heywood</span>, \n								<span class="region">Greater Manchester</span>, \n								<span class="postal-code">OL10 2TP</span>\n							</span>\n						</p>\n						<p><a href="#">Back to top</a></p>\n					</div>\n				</div>\n				<div class="social">\n					<div class="nine60">\n						<div class="social-button"><a href="https://www.facebook.com/YearsleyGroup" target="_blank"><img src="/stat/social-facebook.jpg" alt="Netstock Login" />Like us on Facebook</a></div>\n						<div class="social-button"><a href="https://twitter.com/Yearsley_Group" target="_blank"><img src="/stat/social-twitter.jpg" alt="Netstock Login" />Follow us on Twitter</a></div>\n						<div class="social-button"><a href="http://www.linkedin.com/company/yearsley-group" target="_blank"><img src="/stat/social-linkedin.jpg" alt="Netstock Login" />Find us on LinkedIn</a></div>\n						<!--<div class="social-button" style="margin: 0 20px 0 10px; width: 1px;"><img src="/stat/social-break.jpg" alt="" /></div>-->\n					</div>\n				</div>\n			</footer>\n\n        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>\n        <script>window.jQuery || document.write(''<script src="/js/vendor/jquery-1.8.3.min.js"><\\/script>'')</script>\n        <script src="/js/plugins.js"></script>\n        <script src="/js/main.js"></script>\n		\n		{if segment_2 =="history"}\n		<script type="text/javascript" src="/js/jquery_History.js"></script>\n		<script type="text/javascript" src="/js/timeline.js"></script>\n		\n		<script type="text/javascript">\n			$(''#timeline dl.slidedeck'').slidedeck({\n				hideSpines: true,\n				transition: ''linear'',\n				index: false\n			}).vertical();\n		</script>\n		{/if}\n		\n		{if segment_1 =="netstock-login"}\n		<script type="text/javascript">\n		//<![CDATA[\n			function lnetstock() {\n				var url = ''http://extranet.yearsley.co.uk/netstock/netstock_launcher.html'';\n						var wdth = screen.availWidth-3;\n						var hght = screen.availHeight-42;\n						 var        options2  = ''toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1'';\n									options2 += '',width='' + wdth + '',height='' + hght;    \n									options2 += ''top=0,left=0'';  \n	\n						var win = window.open(url,''_blank'', options2);    \n						win.focus();    \n						win.moveTo(0, -4);\n			}\n		//]]>\n		</script>\n		\n		<script type="text/javascript" src="js/nslogin-page.js"></script>\n		<script type="text/javascript" src="js/nslogin-main.js"></script>\n		<script type="text/javascript" src="js/nscookie.js"></script>\n		{/if}\n		\n		{if segment_1 ==""}\n		<!-- include Cycle plugin -->\n		<script type="text/javascript" src="/js/jquery.cycle.lite.js"></script>\n		<script type="text/javascript">\n				$(''.imgfade'').cycle({\n					fx: ''fade'' // choose your transition type, ex: fade, scrollUp, shuffle, etc...\n				});\n		</script>\n		{/if}\n		\n        <script>\n            var _gaq=[[''_setAccount'',''UA-38848310-1''],[''_trackPageview'']];\n            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n            g.src=(''https:''==location.protocol?''//ssl'':''//www'')+''.google-analytics.com/ga.js'';\n            s.parentNode.insertBefore(g,s)}(document,''script''));\n        </script>\n    </body>\n</html>', '2013-03-05 16:33:10'),
(4, 2, 'header_logisitics', 'I''m a header', '2013-02-28 12:40:57'),
(5, 1, 'about_side', '					<aside class="aside-left">\n						<h2>About us</h2>\n						<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>\n						<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>\n						<h2>Accreditations</h2>\n						<img src="/img/accreditations.png" width="181" height="68" alt="Accreditations">\n					</aside>', '2013-02-28 12:40:57'),
(6, 1, 'news_side', '					<aside class="aside-left">\n						<h2>Archive</h2>\n						{exp:structure_monthly_archives:show parent_entry_id="4" url_path="/news/archive/" listing="true"}\n					</aside>', '2013-02-28 12:40:57'),
(7, 1, 'careers_side', '					<aside class="aside-left">\n						<h2>Working with us</h2>\n						<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>\n						<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>\n					</aside>', '2013-02-28 12:40:57'),
(8, 1, 'cser_side', '<aside class="aside-left">\n	<h2>More Info</h2>\n	<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>\n</aside>', '2013-02-28 12:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `exp_specialty_templates`
--

CREATE TABLE IF NOT EXISTS `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `exp_specialty_templates`
--

INSERT INTO `exp_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`, `sync_time`) VALUES
(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}', '0000-00-00 00:00:00'),
(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n', '0000-00-00 00:00:00'),
(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}', '0000-00-00 00:00:00'),
(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}', '0000-00-00 00:00:00'),
(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}', '0000-00-00 00:00:00'),
(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}', '0000-00-00 00:00:00'),
(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}', '0000-00-00 00:00:00'),
(17, 2, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(18, 2, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(19, 2, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}', '0000-00-00 00:00:00'),
(20, 2, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n', '0000-00-00 00:00:00'),
(21, 2, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}', '0000-00-00 00:00:00'),
(22, 2, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}', '0000-00-00 00:00:00'),
(23, 2, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}', '0000-00-00 00:00:00'),
(24, 2, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(25, 2, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(26, 2, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(27, 2, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(28, 2, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}', '0000-00-00 00:00:00'),
(29, 2, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(30, 2, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(31, 2, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(32, 2, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}', '0000-00-00 00:00:00'),
(33, 3, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(34, 3, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(35, 3, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}', '0000-00-00 00:00:00'),
(36, 3, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n', '0000-00-00 00:00:00'),
(37, 3, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}', '0000-00-00 00:00:00'),
(38, 3, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}', '0000-00-00 00:00:00'),
(39, 3, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}', '0000-00-00 00:00:00'),
(40, 3, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(41, 3, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(42, 3, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(43, 3, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(44, 3, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}', '0000-00-00 00:00:00'),
(45, 3, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(46, 3, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(47, 3, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(48, 3, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `exp_stats`
--

CREATE TABLE IF NOT EXISTS `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_stats`
--

INSERT INTO `exp_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`) VALUES
(1, 1, 1, 1, 'Admin', 61, 0, 0, 0, 1361788491, 0, 0, 1363271671, 21, 1359652200, 1363874663),
(2, 2, 1, 1, 'Admin', 0, 0, 0, 0, 0, 0, 0, 1358495665, 6, 1358269109, 1358874654),
(3, 3, 1, 1, 'Admin', 0, 0, 0, 0, 0, 0, 0, 1358332389, 6, 1358269109, 1358937189);

-- --------------------------------------------------------

--
-- Table structure for table `exp_statuses`
--

CREATE TABLE IF NOT EXISTS `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_statuses`
--

INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`) VALUES
(1, 1, 1, 'open', 1, '009933'),
(2, 1, 1, 'closed', 2, '990000'),
(3, 2, 2, 'open', 1, '009933'),
(4, 2, 2, 'closed', 2, '990000'),
(5, 3, 3, 'open', 1, '009933'),
(6, 3, 3, 'closed', 2, '990000');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_groups`
--

CREATE TABLE IF NOT EXISTS `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_status_groups`
--

INSERT INTO `exp_status_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Statuses'),
(2, 2, 'Statuses'),
(3, 3, 'Statuses');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure`
--

CREATE TABLE IF NOT EXISTS `exp_structure` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `listing_cid` int(6) unsigned NOT NULL DEFAULT '0',
  `lft` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rgt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dead` varchar(100) NOT NULL,
  `hidden` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure`
--

INSERT INTO `exp_structure` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `listing_cid`, `lft`, `rgt`, `dead`, `hidden`) VALUES
(0, 0, 0, 0, 0, 1, 66, 'root', 'n'),
(1, 1, 0, 1, 0, 4, 13, '', 'n'),
(1, 2, 0, 7, 0, 14, 25, '', 'n'),
(1, 3, 0, 8, 0, 26, 31, '', 'n'),
(1, 4, 0, 1, 2, 32, 37, '', 'n'),
(1, 5, 0, 9, 3, 38, 39, '', 'n'),
(1, 6, 0, 1, 0, 40, 41, '', 'n'),
(1, 7, 1, 1, 0, 5, 6, '', 'n'),
(1, 67, 1, 4, 0, 7, 8, '', 'n'),
(1, 9, 1, 1, 0, 9, 10, '', 'n'),
(1, 16, 1, 1, 10, 11, 12, '', 'n'),
(1, 23, 3, 8, 0, 27, 28, '', 'n'),
(1, 24, 3, 8, 0, 29, 30, '', 'n'),
(1, 25, 2, 7, 0, 15, 16, '', 'n'),
(1, 26, 2, 7, 0, 17, 18, '', 'n'),
(1, 27, 2, 7, 0, 19, 20, '', 'n'),
(1, 28, 2, 7, 0, 21, 22, '', 'n'),
(1, 29, 2, 7, 0, 23, 24, '', 'n'),
(1, 30, 4, 1, 0, 33, 34, '', 'n'),
(1, 31, 0, 1, 0, 42, 43, '', 'n'),
(1, 32, 0, 5, 0, 2, 3, '', 'n'),
(1, 68, 4, 1, 0, 35, 36, '', 'n'),
(1, 69, 0, 1, 0, 44, 45, '', 'n'),
(1, 70, 0, 1, 0, 46, 47, '', 'n'),
(1, 80, 0, 1, 0, 62, 63, '', 'y'),
(1, 73, 0, 11, 0, 48, 49, '', 'n'),
(1, 72, 0, 11, 0, 50, 51, '', 'n'),
(1, 76, 0, 1, 0, 52, 53, '', 'n'),
(1, 77, 0, 1, 0, 54, 55, '', 'y'),
(1, 78, 0, 1, 0, 56, 57, '', 'n'),
(1, 79, 0, 1, 0, 58, 59, '', 'n'),
(1, 8, 0, 1, 0, 64, 65, '', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_channels`
--

CREATE TABLE IF NOT EXISTS `exp_structure_channels` (
  `site_id` smallint(5) unsigned NOT NULL,
  `channel_id` mediumint(8) unsigned NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `type` enum('page','listing','asset','unmanaged') NOT NULL DEFAULT 'unmanaged',
  `split_assets` enum('y','n') NOT NULL DEFAULT 'n',
  `show_in_page_selector` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`site_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_channels`
--

INSERT INTO `exp_structure_channels` (`site_id`, `channel_id`, `template_id`, `type`, `split_assets`, `show_in_page_selector`) VALUES
(1, 1, 5, 'page', 'n', 'y'),
(1, 3, 13, 'listing', 'n', 'y'),
(1, 4, 16, 'page', 'n', 'y'),
(1, 2, 11, 'listing', 'n', 'y'),
(1, 5, 1, 'page', 'n', 'y'),
(1, 6, 4, 'page', 'n', 'y'),
(1, 7, 10, 'page', 'n', 'y'),
(1, 8, 14, 'page', 'n', 'y'),
(1, 9, 7, 'page', 'n', 'y'),
(1, 10, 0, 'listing', 'n', 'y'),
(1, 11, 8, 'page', 'n', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_listings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_listings` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `template_id` int(6) unsigned NOT NULL DEFAULT '0',
  `uri` varchar(75) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_listings`
--

INSERT INTO `exp_structure_listings` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `template_id`, `uri`) VALUES
(1, 18, 5, 3, 13, 'account-manager'),
(1, 22, 5, 3, 13, 'project-manager'),
(1, 43, 4, 2, 11, '15-million-hot-cross-buns-were-in-yearsley-groups-stores-this-easter'),
(1, 44, 4, 2, 11, 'belfields-new-mr-kings-range-is-ruling-the-dessert-market'),
(1, 45, 4, 2, 11, 'yearsley-group-invest-0.5m-at-grimsby-depot'),
(1, 46, 4, 2, 11, '20m-expansion-plan-for-yearsley-group-at-heywood'),
(1, 47, 4, 2, 11, 'yearsley-group-acquires-seafood-specialist-ice-pak'),
(1, 48, 4, 2, 11, 'new-logistics-specialist-for-yearsley-group'),
(1, 49, 4, 2, 11, 'yearsley-group-supports-manufacturers-with-bfff-awards-sponsorship'),
(1, 50, 4, 2, 11, 'yearsley-group-wins-new-education-supply-contract'),
(1, 51, 4, 2, 11, 'new-ambient-storage-offering-from-frozen-food-specialist'),
(1, 52, 4, 2, 11, 'yearsley-group-switches-on-renewable-energy'),
(1, 53, 4, 2, 11, 'new-trucks-to-help-yearsley-group-reduce-emissions'),
(1, 54, 4, 2, 11, 'agreement-with-tesco-gives-yearsley-customers-a-little-help'),
(1, 55, 4, 2, 11, 'yearsley-groups-going-global'),
(1, 56, 4, 2, 11, 'youngs-to-store-dry-goods-with-yearsley-group'),
(1, 57, 4, 2, 11, 'freeze-frame-with-yearsley-group'),
(1, 58, 4, 2, 11, 'yearsley-group-to-start-20m-expansion-at-heywood'),
(1, 59, 4, 2, 11, 'new-products-and-direct-sourcing-from-icepak'),
(1, 60, 4, 2, 11, 'harry-yearsley-talks-with-insider-magazine'),
(1, 61, 4, 2, 11, 'yearsley-group-starts-20m-expansion-at-heywood'),
(1, 62, 4, 2, 11, 'yearsley-group-takes-on-new-midlands-depot'),
(1, 63, 4, 2, 11, 'yearsley-group-35-million-investment-in-renewable-energy'),
(1, 64, 4, 2, 11, 'yearsley-group-acquire-business-assets-and-building-at-hams-hall'),
(1, 65, 4, 2, 11, 'yearsley-logistics-wins-csm-contract'),
(1, 66, 4, 2, 11, 'yearsley-group-invite-school-children-to-time-capsule-burial-at-frozen-supe'),
(1, 74, 4, 2, 11, 'new-brand-identity-for-uks-leading-frozen-food-sales-and-logistics-operator');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_members`
--

CREATE TABLE IF NOT EXISTS `exp_structure_members` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `nav_state` text,
  PRIMARY KEY (`site_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_members`
--

INSERT INTO `exp_structure_members` (`member_id`, `site_id`, `nav_state`) VALUES
(1, 1, 'false');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_settings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `var` varchar(60) NOT NULL,
  `var_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `exp_structure_settings`
--

INSERT INTO `exp_structure_settings` (`id`, `site_id`, `var`, `var_value`) VALUES
(1, 0, 'action_ajax_move', '24'),
(2, 0, 'module_id', '12'),
(15, 1, 'perm_reorder_6', 'all'),
(14, 1, 'perm_delete_6', 'all'),
(13, 1, 'add_trailing_slash', 'y'),
(12, 1, 'hide_hidden_templates', 'y'),
(11, 1, 'redirect_on_publish', 'structure_only'),
(10, 1, 'redirect_on_login', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_templates`
--

CREATE TABLE IF NOT EXISTS `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `exp_templates`
--

INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(1, 1, 1, 'index', 'y', 'webpage', '{header}\n{exp:channel:entries channel="homepage" dynamic="off"}\n			\n			<div id="sections" class="clearfix">\n				{homepage_section_images}\n				<p><span class="big-text home-right">{logistics_text}</span><img src="{logistics_image}"></p>\n				<p><span class="big-text home-left">{food_text}</span><img src="{food_image}"></p>\n				{/homepage_section_images}\n			</div>\n			\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					<div id="content">\n						<h1>{title}</h1>\n						{page_copy}\n					</div>\n					<aside>\n						<h2>Latest news and updates</h2>\n						<ul class="news-side">\n							<li>\n								<img src="/stat/test-image.jpg" alt="" />\n								<p class="colophon">11 SEPTEMBER | 2012 | 12.33pm</p>\n								<p>Magnam provident libero excepturi nullam reprehenderit aut turpis sint iste, esse...</p>\n								<p><a href="">Read more</a></p>\n							</li>\n							<li>\n								<img src="/stat/test-image.jpg" alt="" />\n								<p class="colophon">11 SEPTEMBER | 2012 | 12.33pm</p>\n								<p>Magnam provident libero excepturi nullam reprehenderit aut turpis sint iste, esse...</p>\n								<p><a href="">Read more</a></p>\n							</li>\n						</ul>\n						<p><a href="">More news</a></p>\n					</aside>\n				</div>\n{/exp:channel:entries}\n			</div>\n						\n{footer}\n', '', 1360164085, 1, 'n', 0, '', 'n', 'y', 'o', 927),
(22, 1, 1, 'history', 'y', 'webpage', '{exp:channel:entries}\n\n{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/generic-about-us-page.jpg">\n				</p>\n					\n				<div class="subnav-container">\n					<div class="nine60 clearfix">\n						<span class="big-text">{title}</span>\n						<ul class="subnav">\n							<li><a href="/about-us/" class="home-icon{if segment_2 == ""} selected{/if}">About Us</a></li>\n							<li><a href="/about-us/business-structure" {if segment_2 == "business-structure"}class="selected"{/if}>Business Structure</a></li>\n							<li><a href="/about-us/directors" {if segment_2 == "directors"}class="selected"{/if}>Directors</a></li>\n							<li><a href="/about-us/locations" {if segment_2 == "locations"}class="selected"{/if}>Locations</a></li>\n							<li><a href="/about-us/history" {if segment_2 == "history"}class="selected"{/if}>History</a></li>\n						</ul>\n					</div>\n				</div>\n				\n			</div>\n		\n			<div class="fullWidth">\n			\n				<div class="nine60 clearfix">\n					<aside class="aside-left">\n						<h2>Accreditations</h2>\n						<ul class="accreditations">\n							<li><a href=""><img src="/images/content/Generic_Content_images/Isoqar_logo.gif" alt="" /></a></li>\n							<li><a href=""><img src="/images/content/Generic_Content_images/sts_logo.gif" alt="" /></a></li>\n							<li><a href=""><img src="/images/content/Generic_Content_images/brc_logo.gif" alt="" /></a></li>\n							<li><a href=""><img src="/images/content/Generic_Content_images/rha_logo.gif" alt="" /></a></li>\n						</ul>\n					</aside>\n					<div id="content" class="content-right">\n						<div class="content-wrap">\n							{page_copy}\n							<a href="/about-us/business-structure" class="more-btn btn-r">Next: Business Structure<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n						</div>\n					</div>\n				</div>\n		\n{footer}\n\n{/exp:channel:entries}', NULL, 1360240635, 1, 'n', 0, '', 'n', 'n', 'o', 241),
(2, 2, 2, 'index', 'n', 'webpage', 'YEARSLEY LOGISTICS\n\n{header_logisitics}', '', 1358347205, 1, 'n', 0, '', 'n', 'n', 'o', 13),
(3, 3, 3, 'index', 'n', 'webpage', 'YEARSLEY FOOD SALES', '', 1358332378, 1, 'n', 0, '', 'n', 'n', 'o', 1),
(4, 1, 1, 'about', 'y', 'webpage', '{exp:channel:entries}\n\n{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/generic-about-us-page.jpg">\n				</p>\n					\n				<div class="subnav-container">\n					<div class="nine60 clearfix">\n						<span class="big-text">{title}</span>\n						<ul class="subnav">\n							<li><a href="/about-us/" class="home-icon{if segment_2 == ""} selected{/if}">About Us</a></li>\n							<li><a href="/about-us/business-structure" {if segment_2 == "business-structure"}class="selected"{/if}>Business Structure</a></li>\n							<li><a href="/about-us/directors" {if segment_2 == "directors"}class="selected"{/if}>Directors</a></li>\n							<li><a href="/about-us/locations" {if segment_2 == "locations"}class="selected"{/if}>Locations</a></li>\n							<li><a href="/about-us/history" {if segment_2 == "history"}class="selected"{/if}>History</a></li>\n						</ul>\n					</div>\n				</div>\n				\n			</div>\n		\n			<div class="fullWidth">\n			\n				<div class="nine60 clearfix">\n					<aside class="aside-left">\n						<h2>Accreditations</h2>\n						<ul class="accreditations">\n							<li><a href=""><img src="/images/content/Generic_Content_images/Isoqar_logo.gif" alt="" /></a></li>\n							<li><a href=""><img src="/images/content/Generic_Content_images/sts_logo.gif" alt="" /></a></li>\n							<li><a href=""><img src="/images/content/Generic_Content_images/brc_logo.gif" alt="" /></a></li>\n							<li><a href=""><img src="/images/content/Generic_Content_images/rha_logo.gif" alt="" /></a></li>\n						</ul>\n					</aside>\n					<div id="content" class="content-right">\n						<div class="content-wrap">\n							{page_copy}\n							<a href="/about-us/business-structure" class="more-btn btn-r">Next: Business Structure<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n						</div>\n					</div>\n				</div>\n		\n{footer}\n\n{/exp:channel:entries}', '', 1360240501, 1, 'n', 0, '', 'n', 'n', 'o', 1947),
(5, 1, 1, 'simple', 'y', 'webpage', '{exp:channel:entries}\n{header}\n\n			{title}\n		\n{footer}\n{/exp:channel:entries}', '', 1360164085, 1, 'n', 0, '', 'n', 'n', 'o', 94),
(6, 1, 1, 'contact', 'y', 'webpage', '{exp:channel:entries}\n{header}\n		\n		<div id="sections" class="clearfix">\n			<p class="full-width">\n				<img src="/images/content/Generic_Content_images/contact-us-page.jpg">\n			</p>\n			\n			<div class="big-container">\n				<div class="nine60" class="clearfix">\n					<span class="big-text b-r">Contact</span>\n				</div>\n			</div>\n		</div>\n	\n		<div class="fullWidth">\n			<div class="nine60 clearfix">\n				\n				<aside class="aside-left contact">\n					<h2>Contact Info</h2>\n					<p class="address no-bold"><span>Head Office:</span><br>\n					Hareshill Road, Heywood,<br>\n					Lancashire, OL10 2TP</p>\n					<p class="numbers">\n					<span>Coldstore:</span><br>\n					Tel: 01706 694620<br>\n					Fax: 01706 694625</p>\n					<p class="numbers">\n					<span>Accounts:</span><br>\n					Tel: 01706 694640<br>\n					Fax: 01706 694645</p>\n					<p class="numbers">\n					<span>Transport:</span><br>\n					Tel: 01706 694630<br>\n					Fax: 01706 694635</p>\n					<p class="numbers">\n					<span>Customer Services:</span><br>\n					Tel: 01706 694680<br>\n					Fax: 01706 694666</p>\n					<p class="numbers">\n					<span>Food Sales:</span><br>\n					Tel: 01706 694600<br>\n					Fax: 01706 694605</p>\n					<p class="numbers">\n					<span>Belfield:</span><br>\n					Tel: 01706 694610<br>\n					Fax: 01706 694605</p>\n					<p class="phone">Or Call:<br>\n					<strong>01706 694680</strong></p>\n				</aside>\n				\n				<div id="content" class="content-right">\n					<h1>Drop us a line</h1>\n					<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>\n					<p>Consectetuer voluptates? Laborum congue est consectetur debitis ornare harum. Conubia iure corrupti wisi?</p>\n					\n					{exp:freeform:form collection="Contact Form" form:class="contact-form" required="name|email|subject|message" return="/thank-you" recipients="yes" recipient_limit="1" recipient_template="contact_us"}\n					<input type="hidden" name="recipient_email" value="info@scorpioworldwide.com">\n					<div class="w_180"><label>Your name*:</label></div>\n					<div class="w_450 pr"><input type="text" name="name" value="" class="required"></div>\n					<div class="w_180"><label>Your email address*:</label></div>\n					<div class="w_450 pr"><input type="text" name="email" value="" class="required"></div>\n					<div class="w_180"><label>Subject*:</label></div>\n					<div class="w_450"><input type="text" name="subject" value=""></div>\n					<div class="w_180"><label>Your message*:</label></div>\n					<div class="w_450 pr"><textarea name="message" class="required"></textarea></div>\n					<div class="w_450 al">\n						<input type="checkbox" name="international_shopper" value="Yes"> <label>Please un-tick the box if you WOULD NOT like to register for the Yearsley Group e-newsletter and other relevant email communications from us and our associated companies.</label>\n					</div>\n					<input type="submit" name="submit" value="Send" class="submit">\n					{/exp:freeform:form}\n					\n				</div>\n				\n			</div>\n		</div>\n		\n{footer}\n{/exp:channel:entries}', '', 1360164085, 1, 'n', 0, '', 'n', 'n', 'o', 342),
(7, 1, 1, 'careers', 'y', 'webpage', '{exp:channel:entries}\n{header}\n			\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/career-page.jpg">\n				</p>\n				\n				<div class="big-container">\n					<div class="nine60" class="clearfix">\n						<span class="big-text b-r">Careers</span>\n					</div>\n				</div>\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					\n					<aside class="aside-left">\n						{careers_side_text}\n					</aside>\n					\n					<div id="content" class="content-right">\n						<h1>{title}: Current Opportunities</h1>\n						{/exp:channel:entries}\n						{exp:channel:entries channel="careers" dynamic="off"}\n						<div class="news-item careers">\n							<span class="date">{entry_date format="%d.%m.%Y"}</span>\n							<h2><a href="{page_url}">{title}</a></h2>\n							<a href="{page_url}" class="more-btn btn-r">Full Details<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n							<p>\n								<b>Work Type:</b> {work_type}<br>\n								<b>Salary:</b> {job_location}<br>\n								<b>Location:</b> {salary}<br>\n							</p>\n							<p>{intro_copy}</p>\n							\n						</div>\n						{/exp:channel:entries}\n					</div>\n					\n				</div>\n			</div>\n		\n{footer}', '', 1360164085, 1, 'n', 0, '', 'n', 'n', 'o', 241),
(8, 1, 1, 'news', 'y', 'webpage', '{exp:channel:entries}\n\n{header}\n\n{/exp:channel:entries}\n\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/news-temporary.jpg">\n				</p>\n				\n				<div class="big-container">\n					<div class="nine60 clearfix">\n						<span class="big-text b-r">News</span>\n					</div>\n				</div>\n				\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					\n					{news_side}\n					\n					<div id="content" class="content-right">\n						<h1>Latest News</h1>\n						{exp:channel:entries channel="news" dynamic="off"}\n						<div class="news-item">\n							<span class="date">{entry_date format="%d.%m.%Y"}</span>\n							<h2><a href="{page_url}">{title}</a></h2>\n							<p>{news_snippet}</p>\n							<a href="{page_url}" class="more-btn btn-r">Read more<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					\n				</div>\n			</div>\n		\n{footer}', '', 1360164085, 1, 'n', 0, '', 'n', 'n', 'o', 782),
(10, 1, 1, 'our-businesses', 'y', 'webpage', '{exp:channel:entries}\n\n{header}\n		\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					{if segment_2 == "logistics"}\n					<img src="/images/content/Generic_Content_images/logistics-page.jpg">\n					{if:elseif segment_2 == "food-sales"}\n					<img src="/img/about_banner.jpg">\n					{if:elseif segment_2 == "belfield"}\n					<img src="/images/content/Generic_Content_images/belfield-page.jpg">\n					{if:elseif segment_2 == "icepak"}\n					<img src="/images/content/Generic_Content_images/icepak-page.jpg">\n					{if:else}\n					<img src="/img/about_banner.jpg">\n					{/if}\n				\n					\n					\n					\n					\n					\n					\n				</p>\n				\n				<div class="subnav-container">\n					<div class="nine60 clearfix">\n						\n						<span class="big-text">Our Businesses</span>\n						\n						<ul class="subnav">\n							<li><a href="/our-businesses" class="home-icon{if segment_2 == ""} selected{/if}">Our Businesses</a></li>\n							<li><a href="/our-businesses/logistics" {if segment_2 == "logistics"}class="selected"{/if}>Logistics</a></li>\n							<li><a href="/our-businesses/food-sales" {if segment_2 == "food-sales"}class="selected"{/if}>Food Sales</a></li>\n							<li><a href="/our-businesses/belfield" {if segment_2 == "belfield"}class="selected"{/if}>Belfield</a></li>\n							<li><a href="/our-businesses/icepak" {if segment_2 == "icepak"}class="selected"{/if}>IcePak</a></li>\n							<li><a href="/our-businesses/lucky-red" {if segment_2 == "lucky-red"}class="selected"{/if}>Lucky Red</a></li>\n						</ul>\n					</div>\n				</div>\n				\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					<aside class="aside-left business">\n					{side_images}\n						<img src="{url}" width="128" height="129" alt="Img Placeholder" class="content-img img-1">\n					{/side_images}\n						{if segment_2}\n						<p class="siteLink"><a href="{website_link}" class="more-btn btn-l siteLink aside-btn">Visit the {title} Website<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a></p>\n						{/if}\n					</aside>\n					\n					\n					<div id="content" class="content-right">\n						<h1>{title}</h1>\n						<div class="content-wrap">\n							{if generic_image}\n							<img src="{generic_image}" class="content-img">\n							{/if}\n							{page_copy}\n							{if segment_2 == ""}\n							<a href="/our-businesses/logistics" class="more-btn btn-r">Next: Logistics<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n							{if:else}\n							\n							{if segment_2 == "logistics"}\n							<a href="/our-businesses/food-sales" class="more-btn btn-r">Next: Food Sales<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n							{if:elseif segment_2 == "food-sales"}\n							<a href="/our-businesses/belfield" class="more-btn btn-r">Next: Belfield<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n							{if:elseif segment_2 == "belfield"}\n							<a href="/our-businesses/icepak" class="more-btn btn-r">Next: IcePak<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n							{if:elseif segment_2 == "icepak"}\n							<a href="/our-businesses/lucky-red" class="more-btn btn-r">Next: Lucky Red<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n							{/if}\n							\n							{/if}\n						</div>\n					</div>\n					\n				</div>\n			</div>\n		\n		\n{footer}\n\n{/exp:channel:entries}', '', 1360164085, 1, 'n', 0, '', 'n', 'n', 'o', 1238),
(11, 1, 1, 'news-item', 'y', 'webpage', '{exp:channel:entries}\n\n{header}\n		\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/news-temporary.jpg">\n				</p>\n				\n				<div class="big-container">\n					<div class="nine60 clearfix">\n						<span class="big-text b-r">News</span>\n					</div>\n				</div>\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					\n					{news_side}\n					\n					<div id="content" class="content-right">\n						<div class="news-item">\n							\n							<span class="date">{entry_date format="%d.%m.%Y"}</span>\n							<h1>{title}</h1>\n							{news_story}\n							<a href="/news" class="more-btn btn-l btn-b">Back<span class="more-arrow"><img src="/img/more_arrow_left.png" width="13" height="20" alt="More Arrow" class="arr-l"></span></a>\n						</div>\n					</div>\n					\n				</div>\n			</div>\n		\n		\n{footer}\n{/exp:channel:entries}\n', '', 1360164085, 1, 'n', 0, '', 'n', 'n', 'o', 253),
(12, 1, 1, '404', 'y', 'webpage', '{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width"><img src="/stat/404.jpg"></p>\n				<div class="subnav-container">\n					<div class="nine60 clearfix">\n						\n					</div>\n				</div>\n			</div>\n			\n			\n			\n\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					<aside class="aside-left">\n                    </aside>\n					\n					<div id="content" class="content-right">\n						<h2 style="margin: 0 0 30px 0; padding-bottom: 0;">Sorry, this page cannot be found.</h2>\n                        <div class="content-wrap">\n						\n                           \n							<div class="clearfix"></div>\n								<a href="/" class="more-btn btn-l btn-b">Back<span class="more-arrow"><img src="/img/more_arrow_left.png" width="13" height="20" alt="More Arrow" class="arr-l"></span></a>\n						</div>\n					</div>\n\n				</div>\n			</div>\n{footer}\n', '', 1361545014, 1, 'n', 0, '', 'n', 'n', 'o', 1278),
(13, 1, 1, 'careers-item', 'y', 'webpage', '{exp:channel:entries}\n{header}\n			\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/career-page.jpg">\n				</p>\n				\n				<div class="big-container">\n					<div class="nine60" class="clearfix">\n						<span class="big-text b-r">Careers</span>\n					</div>\n				</div>\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					\n					{careers_side}\n					\n					<div id="content" class="content-right">\n						<div class="news-item">\n							<span class="date">{entry_date format="%d.%m.%Y"}</span>\n							<h1>{title}</h1>\n							<p>\n								<b>Work Type:</b> {work_type}<br>\n								<b>Salary:</b> {job_location}<br>\n								<b>Location:</b> {salary}<br>\n							</p>\n							<p>{careers_copy}</p>\n							<a href="/careers" class="more-btn btn-l btn-b">Back<span class="more-arrow"><img src="/img/more_arrow_left.png" width="13" height="20" alt="More Arrow" class="arr-l"></span></a>\n						</div>\n						\n						<div >\n							<h2>Apply Now</h2>\n							<p class="no-bold">Fill in your details below and we will contact you regarding your application.</p>\n							{exp:freeform:form collection="Job Application" form:class="contact-form" required="name|email|message" return="/thank-you"}\n							<!--input type="hidden" name="recipient_email" value="info@scorpioworldwide.com"-->\n							<div class="w_180"><label>Your name*:</label></div>\n							<div class="w_450 pr"><input type="text" name="name" value="" class="required"></div>\n							<div class="w_180"><label>Your email address*:</label></div>\n							<div class="w_450 pr"><input type="text" name="email" value="" class="required"></div>\n							<div class="w_180"><label>Your phone number:</label></div>\n							<div class="w_450"><input type="text" name="phone" value=""></div>\n							\n							<div class="w_180"><label>Covering note*:</label></div>\n							<div class="w_450 pr"><textarea name="message" class="required" rows="6"></textarea></div>\n							<input type="submit" name="submit" value="Send" class="submit">\n							{/exp:freeform:form}\n							\n						</div>\n					</div>\n					\n				</div>\n			</div>\n		\n{footer}\n{/exp:channel:entries}', '', 1360164085, 1, 'n', 0, '', 'n', 'n', 'o', 169),
(14, 1, 1, 'cser-pledge', 'y', 'webpage', '{exp:channel:entries}\n{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/cser-page.jpg">\n				</p>\n					\n				<div class="subnav-container">\n					<div class="nine60 clearfix">\n						\n						<span class="big-text">CSER Pledge</span>\n						\n						<ul class="subnav">\n							<li><a href="/cser-pledge" class="home-icon{if segment_2 == ""} selected{/if}">CSER Pledge</a></li>\n							<li><a href="/cser-pledge/causes-we-support" {if segment_2 == "causes-we-support"}class="selected"{/if}>Causes we support</a></li>\n							<li><a href="/cser-pledge/our-commitment-to-the-environment" {if segment_2 == "our-commitment-to-the-environment"}class="selected"{/if}>Our Commitment to the Environment</a></li>\n						</ul>\n					</div>\n				</div>\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix cser">\n					\n					<aside class="aside-left">\n						{if segment_2 =="causes-we-support"}\n						<img src="/images/content/Generic_Content_images/royal-manchester-childrens-hospital.jpg" alt="" />\n						{if:else}\n							<ul class="images">\n							{cser_images}\n								<li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" max="128" crop="yes"}</li>\n							{/cser_images}\n							</ul>\n						{/if}\n						<p class="cser-pdf"><a href="/uploads/cser.pdf">For more information about our progress so far and targets for the future can be found in our <span>Yearsley CSER report</span></a></p>\n					</aside>\n					\n					\n					<div id="content" class="content-right cser">\n						<h1>{title}</h1>\n						<div class="content-wrap">\n							{page_copy}\n							{if segment_2 == ""}\n							<a href="/cser-pledge/causes-we-support" class="more-btn btn-r">Next: Causes we Support<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n							{if:else}\n							{if segment_2 == "causes-we-support"}\n							<a href="/cser-pledge/our-commitment-to-the-environment" class="more-btn btn-r">Next: Our Commitment to the Environment<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n							{/if}\n							{/if}\n						</div>\n					</div>\n					\n				</div>\n			</div>\n		\n		\n{footer}\n\n{/exp:channel:entries}', '', 1360667355, 1, 'n', 0, '', 'n', 'n', 'o', 725),
(15, 1, 1, 'news-archive', 'y', 'webpage', '{exp:channel:entries}\n{header}\n{/exp:channel:entries}\n\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/img/news_banner.jpg">\n				</p>\n				\n				<div class="big-container">\n					<div class="nine60 clearfix">\n						<span class="big-text b-r">News</span>\n					</div>\n				</div>\n				\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					\n					{news_side}\n					\n					<div id="content" class="content-right">\n						<h1>News Archive</h1>\n						{exp:channel:entries channel="news" dynamic="off" year="{freebie_3}" month="{freebie_4}"}\n						{if no_results}\n						<p>There are no results to display.</p>\n						{/if}\n						<div class="news-item">\n							<span class="date">{entry_date format="%d.%m.%Y"}</span>\n							<h2><a href="{page_url}">{title}</a></h2>\n							<p>{news_snippet}</p>\n							<a href="{page_url}" class="more-btn btn-r">Read more<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					\n				</div>\n			</div>\n		\n{footer}', '', 1360164085, 1, 'n', 0, '', 'n', 'n', 'o', 221),
(16, 1, 1, 'directors', 'y', 'webpage', '{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width"><img src="/images/content/Generic_Content_images/directors-page.jpg"></p>\n				<div class="subnav-container">\n					<div class="nine60 clearfix">\n						<span class="big-text">About us</span>\n						<ul class="subnav">\n							<li><a href="/about-us/" class="home-icon{if segment_2 == ""} selected{/if}">About Us</a></li>\n							<li><a href="/about-us/business-structure" {if segment_2 == "business-structure"}class="selected"{/if}>Business Structure</a></li>\n							<li><a href="/about-us/directors" {if segment_2 == "directors"}class="selected"{/if}>Directors</a></li>\n							<li><a href="/about-us/locations" {if segment_2 == "locations"}class="selected"{/if}>Locations</a></li>\n							<li><a href="/about-us/history" {if segment_2 == "history"}class="selected"{/if}>History</a></li>\n						</ul>\n					</div>\n				</div>\n			</div>\n			\n			\n			\n{exp:channel:entries channel="directors" dynamic="on" orderby="entry_id" sort="asc"}\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					<aside class="aside-left director">\n						{md_details}\n							<img src="{md_image}" class="content-img">\n							<h3>{md_name}</h3>\n							<p>{md_bio}</p>\n						{/md_details}\n					</aside>\n					\n					<div id="content" class="content-right">\n						<div class="content-wrap">\n							{directors}\n							<div class="director director-hw">\n								<img src="{director_image}" class="content-img">\n								<h3>{director_name}</h3>\n								<p>{director_bio}</p>\n							</div>\n							{/directors}\n							<div class="clearfix"></div>\n								<a href="/about-us/locations" class="more-btn btn-r">Next: Locations<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n						</div>\n					</div>\n{/exp:channel:entries}\n				</div>\n			</div>\n{footer}\n', NULL, 1360171118, 1, 'n', 0, '', 'n', 'n', 'o', 209),
(18, 1, 1, 'structure', 'y', 'webpage', '{exp:channel:entries}\n\n{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/structure-page.jpg">\n				</p>\n					\n				<div class="subnav-container">\n					<div class="nine60 clearfix">\n							<ul class="subnav">\n							<li><a href="/about-us/" class="home-icon{if segment_2 == ""} selected{/if}">About Us</a></li>\n							<li><a href="/about-us/business-structure" {if segment_2 == "business-structure"}class="selected"{/if}>Business Structure</a></li>\n							<li><a href="/about-us/directors" {if segment_2 == "directors"}class="selected"{/if}>Directors</a></li>\n							<li><a href="/about-us/locations" {if segment_2 == "locations"}class="selected"{/if}>Locations</a></li>\n							<li><a href="/about-us/history" {if segment_2 == "history"}class="selected"{/if}>History</a></li>\n						</ul>\n					</div>\n				</div>\n			</div>\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					<div id="content" class="content-right">\n						<div class="content-wrap">\n							<div class="business">\n								<img src="/img/bs_yearsley.png" width="358" height="127" alt="Bs Yearsley" class="content-img">\n								<div class="biz-nav">\n									<div class="arrow">\n										<img src="/img/arrowhead.png" width="30" height="8" alt="Arrowhead">\n									</div>\n									<nav>\n										<ul id="business-selector">\n											<li><a href="#" id="biz-foodsales" class="selected">Food Sales</a></li>\n											<li><a href="#" id="biz-logistics">Logistics</a></li>\n										</ul>\n									</nav>\n								</div>\n								\n								<div class="businesses">\n									<div class="biz-foodsales">\n										<a href="" class="lucky-red"><img src="/img/bs_lucky_red.png" width="176" height="97" alt="Lucky Red" class="content-img pop"></a>\n										<span></span>\n										<a href="" class="icepak"><img src="/img/bs_icepak.png" width="176" height="97" alt="IcePak" class="content-img pad-img pop"></a>\n										<span></span>\n										<a href="" class="belfield"><img src="/img/bs_belfield.png" width="176" height="97" alt="Belfield" class="content-img pop"></a>\n									</div>\n									<div class="biz-logistics">\n										<a href="" class="storage"><img src="/img/bs_cold_storage.jpg" width="176" height="97" alt="Cold Storage" class="content-img pop"></a>\n										<span></span>\n										<a href="" class="logistics"><img src="/img/bs_logistics.jpg" width="176" height="97" alt="Logistics" class="content-img pop"></a>\n									</div>\n									\n									<div class="popup">\n										<div class="pop-wrap">\n											<img src="/img/pop_arrow.png" width="30" height="16" class="pop-arrow">\n											<div id="lucky-red" class="pop-info">\n												<h3>Lucky Red Frozen Foods</h3>\n												<p>Chinese food wholesalers.</p>\n												<a href="#" class="web-link">Click to visit the Lucky Red Website</a>\n											</div>\n											<div id="icepak" class="pop-info">\n												<h3>IcePak</h3>\n												<p>Frozen seafood and poultry importers</p>\n												<a href="#" class="web-link">Click to visit the IcePak Website</a>\n											</div>\n											<div id="belfield" class="pop-info">\n												<h3>Belfield</h3>\n												<p>Frozen food sales partner.</p>\n												<a href="#" class="web-link">Click to visit the Belfield Website</a>\n											</div>\n											<div id="storage" class="pop-info">\n												<h3>Cold Storage</h3>\n												<p>Temperature controlled storage and temperature controlled distribution</p>\n												<a href="#" class="web-link">Click to visit the Website</a>\n											</div>\n											<div id="logistics" class="pop-info">\n												<h3>Logistics</h3>\n												<p>Urna inceptos turpis cursus nonummy eum consequat volutpat animi semper proin. Praesent laoreet sollicitudin.</p>\n												<a href="#" class="web-link">Click to visit the Logistics Website</a>\n											</div>\n										</div>\n									</div>\n								</div>\n							</div>\n							<div class="clearfix"></div>\n							<a href="/about-us/directors" class="more-btn btn-r">Next: Directors<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n						</div>\n					</div>\n				</div>\n			</div>\n		\n		\n{footer}\n\n{/exp:channel:entries}', NULL, 1360235024, 1, 'n', 0, '', 'n', 'n', 'o', 221),
(20, 1, 1, 'locations', 'y', 'webpage', '{exp:channel:entries}\n\n{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/img/locations_banner.jpg">\n				</p>\n					\n				<div class="subnav-container">\n					<div class="nine60 clearfix">\n						<span class="big-text">About us</span>\n						<ul class="subnav">\n							<li><a href="/about-us/" class="home-icon{if segment_2 == ""} selected{/if}">About Us</a></li>\n							<li><a href="/about-us/business-structure" {if segment_2 == "business-structure"}class="selected"{/if}>Business Structure</a></li>\n							<li><a href="/about-us/directors" {if segment_2 == "directors"}class="selected"{/if}>Directors</a></li>\n							<li><a href="/about-us/locations" {if segment_2 == "locations"}class="selected"{/if}>Locations</a></li>\n							<li><a href="/about-us/history" {if segment_2 == "history"}class="selected"{/if}>History</a></li>\n						</ul>\n					</div>\n				</div>\n				\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					\n					<aside class="aside-left">\n						<h2>How to use</h2>\n						<p class="no-bold">Click on a site below to focus in the main map. Click the arrow to expand location.</p>\n						<h3 class="location-hdr">13 Locations</h3>\n						<ul class="location">\n							<li><a href="" id="heywood"><span>1</span> Heywood (Head Office)</a><br>\n								<span class="location-info"><strong>Address:</strong><br>\n								Hareshill Road, Heywood, Lancashire, OL10 2TP</br></br>\n								<strong>Tel:</strong> 01706 694 600\n								</span>\n							</li>\n							<li><a href="" id="belle-eau-park"><span>2</span> Belle Eau Park</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Belle Eau Park, Bilsthorpe, Newark, Notts, NG22 8TX<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="bellshill"><span>3</span> Bellshill</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Belgowan Street, North Industrial Estate, Bellshill, Lanarkshire, ML4 3LB<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="bristol"><span>4</span> Bristol</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Garanor Way, Portbury, Bristol, BS20 7XT<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="coleshill"><span>5</span> Coleshill</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Gorsey Lane, Coleshill, Birmingham, B46 1JU<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="gillingham"><span>6</span> Gillingham</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Valentine Close, Gillingham Business Park, Gillingham, Kent, ME8 0QR<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="grimsby"><span>7</span> Grimsby</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Marsden Road, Grimsby, N.E. Lincs, DN31 3FS<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="hams-hall"><span>8</span> Hams Hall</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Unit 7, Faraday Avenue, Hams Hall Distribution Park, Coleshill, Birmingham, B46 1AL<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="holmewood"><span>9</span> Holmewood</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Tupton Way, Holmewood Industrial Estate, Chesterfield, Derbyshire, S42 5BX<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="rochdale"><span>10</span> Rochdale</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Albert Royds St, Rochdale, Lancashire, OL16 5AA<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="scunthorpe"><span>11</span> Scunthorpe</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Normanby Road, Cupola Way, Scunthorpe, DN15 9YJ<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="seaham"><span>12</span> Seaham</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Fox Cover industrial Est. Admiralty Way, Dawdon, Seaham, SR7 7DN<br>\n								\n								</span>\n							</li>\n							<li><a href="" id="wisbech"><span>13</span> Wisbech</a>\n								<span class="location-info"><strong>Address:</strong><br>\n								Weasenham Lane, Wisbech, Cambridgeshire, PE13 2RN<br>\n								\n								</span>\n							</li>\n						</ul>\n					</aside>\n					\n					<div id="content" class="content-right">\n						<h1>{title}</h1>\n						<div class="content-wrap">\n							<div class="location-map">\n								\n								<img src="/img/uk_map.png" width="579" height="713" alt="Uk Map">\n								<div class="icon heywood"><a href="" id="heywood">1</a></div>\n								<div class="icon belle-eau-park"><a href="" id="belle-eau-park">2</a></div>\n								<div class="icon bellshill"><a href="" id="bellshill">3</a></div>\n								<div class="icon bristol"><a href="" id="bristol">4</a></div>\n								<div class="icon coleshill"><a href="" id="coleshill">5</a></div>\n								<div class="icon gillingham"><a href="" id="gillingham">6</a></div>\n								<div class="icon grimsby"><a href="" id="grimsby">7</a></div>\n								<div class="icon hams-hall"><a href="" id="hams-hall">8</a></div>\n								<div class="icon holmewood"><a href="" id="holmewood">9</a></div>\n								<div class="icon rochdale"><a href="" id="rochdale">10</a></div>\n								<div class="icon scunthorpe"><a href="" id="scunthorpe">11</a></div>\n								<div class="icon seaham"><a href="" id="seaham">12</a></div>\n								<div class="icon wisbech"><a href="" id="wisbech">13</a></div>\n								\n								<div class="popup">\n									<div class="pop-wrap">\n										<img src="/img/pop_arrow_down.png" width="30" height="16" class="pop-arrow">\n										<div class="heywood info">\n											<h3>Heywood (Head Office)</h3>\n											<p>Hareshill Road, Heywood, Lancashire, OL10 2TP</p>\n										</div>\n										<div class="belle-eau-park info">\n											<h3>Belle Eau Park</h3>\n											<p>Belle Eau Park, Bilsthorpe, Newark, Notts, NG22 8TX</p>\n										</div>\n										<div class="bellshill info">\n											<h3>Bellshill</h3>\n											<p>Belgowan Street, North Industrial Estate, Bellshill, Lanarkshire, ML4 3LB</p>\n										</div>\n										<div class="bristol info">\n											<h3>Bristol</h3>\n											<p>Garanor Way, Portbury, Bristol, BS20 7XT</p>\n										</div>\n										<div class="coleshill info">\n											<h3>Coleshill</h3>\n											<p>Gorsey Lane, Coleshill, Birmingham, B46 1JU</p>\n										</div>\n										<div class="gillingham info">\n											<h3>Gillingham</h3>\n											<p>Valentine Close, Gillingham Business Park, Gillingham, Kent, ME8 0QR</p>\n										</div>\n										<div class="grimsby info">\n											<h3>Grimsby</h3>\n											<p>Marsden Road, Grimsby, N.E. Lincs, DN31 3FS</p>\n										</div>\n										<div class="hams-hall info">\n											<h3>Hams Hall</h3>\n											<p>Unit 7, Faraday Avenue, Hams Hall Distribution Park, Coleshill, Birmingham, B46 1AL</p>\n										</div>\n										<div class="holmewood info">\n											<h3>Holmewood</h3>\n											<p>Tupton Way, Holmewood Industrial Estate, Chesterfield, Derbyshire, S42 5BX</p>\n										</div>\n										<div class="rochdale info">\n											<h3>Rochdale</h3>\n											<p>Albert Royds St, Rochdale, Lancashire, OL16 5AA</p>\n										</div>\n										<div class="scunthorpe info">\n											<h3>Scunthorpe</h3>\n											<p>Normanby Road, Cupola Way, Scunthorpe, DN15 9YJ</p>\n										</div>\n										<div class="seaham info">\n											<h3>Seaham</h3>\n											<p>Fox Cover industrial Est. Admiralty Way, Dawdon, Seaham, SR7 7DN</p>\n										</div>\n										<div class="wisbech info">\n											<h3>Wisbech</h3>\n											<p>Weasenham Lane, Wisbech, Cambridgeshire, PE13 2RN</p>\n										</div>\n									</div>\n								</div>\n								\n							</div>\n							<a href="/about-us/history" class="more-btn btn-r">Next: History<span class="more-arrow"><img src="/img/more_arrow.png" width="13" height="20" alt="More Arrow" class="arr-r"></span></a>\n						</div>\n					</div>\n				</div>\n			</div>\n		\n		\n{footer}\n\n{/exp:channel:entries}', '', 1362504216, 1, 'n', 0, '', 'n', 'n', 'o', 250),
(23, 1, 1, 'terms', 'y', 'webpage', '{exp:channel:entries}\n{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/news-temporary.jpg">\n				</p>\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix cser">\n					\n					<aside class="aside-left">\n						<p></p>\n					</aside>\n					\n					\n					<div id="content" class="content-right cser">\n						<h1>{title}</h1>\n						<div class="content-wrap">\n							{page_copy}\n						</div>\n					</div>\n					\n				</div>\n			</div>\n		\n		\n{footer}\n\n{/exp:channel:entries}', NULL, 1360598726, 1, 'n', 0, '', 'n', 'n', 'o', 14),
(24, 1, 1, 'cookies', 'y', 'webpage', '{exp:channel:entries}\n{header}\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/news-temporary.jpg">\n				</p>\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix cser">\n					\n					<aside class="aside-left">\n						<p></p>\n					</aside>\n					\n					\n					<div id="content" class="content-right cser">\n						<h1>{title}</h1>\n						<div class="content-wrap">\n							{page_copy}\n						</div>\n					</div>\n					\n				</div>\n			</div>\n		\n		\n{footer}\n\n{/exp:channel:entries}', NULL, 1360599087, 1, 'n', 0, '', 'n', 'n', 'o', 24),
(25, 1, 1, 're-direct-to-first-child', 'y', 'webpage', '{exp:structure:first_child_redirect}', '', 1360755977, 1, 'n', 0, '', 'n', 'n', 'o', 141),
(26, 1, 1, 'contact-thankyou', 'y', 'webpage', '{exp:channel:entries}\n{header}\n\n    	<div id="sections" class="clearfix">\n            <p class="full-width"><img src="/images/content/Generic_Content_images/contact-us-page.jpg">></p>\n            <div class="big-container">\n				<div class="nine60" class="clearfix">\n					<span class="big-text b-r">Contact</span>\n				</div>\n			</div>\n        </div>\n        \n        <div class="fullWidth">\n			<div class="nine60 clearfix">\n				\n				<aside class="aside-left contact">\n					<h2>Contact Info</h2>\n					<p class="address no-bold"><span>Head Office:</span><br>\n					Hareshill Road, Heywood,<br>\n					Lancashire, OL10 2TP</p>\n					<p class="numbers">\n					<span>Coldstore:</span><br>\n					Tel: 01706 694620<br>\n					Fax: 01706 694625</p>\n					<p class="numbers">\n					<span>Accounts:</span><br>\n					Tel: 01706 694640<br>\n					Fax: 01706 694645</p>\n					<p class="numbers">\n					<span>Transport:</span><br>\n					Tel: 01706 694630<br>\n					Fax: 01706 694635</p>\n					<p class="numbers">\n					<span>Customer Services:</span><br>\n					Tel: 01706 694680<br>\n					Fax: 01706 694666</p>\n					<p class="numbers">\n					<span>Food Sales:</span><br>\n					Tel: 01706 694600<br>\n					Fax: 01706 694605</p>\n					<p class="numbers">\n					<span>Belfield:</span><br>\n					Tel: 01706 694610<br>\n					Fax: 01706 694605</p>\n					<p class="phone">Or Call:<br>\n					<strong>01706 694680</strong></p>\n				</aside>\n                \n                <div id="content" class="content-right">\n					<h1>{title}</h1>\n					<p>{page_copy}</p>\n                    \n                    <div class="clearfix"></div>\n                    <a href="/" class="more-btn btn-l btn-b">Back<span class="more-arrow"><img src="/img/more_arrow_left.png" width="13" height="20" alt="More Arrow" class="arr-l"></span></a>\n                </div>\n\n            </div>\n        </div>\n		\n{footer}\n{/exp:channel:entries}', NULL, 1361287042, 1, 'n', 0, '', 'n', 'n', 'o', 17),
(27, 1, 1, 'careers-thankyou', 'y', 'webpage', '{exp:channel:entries}\n{header}\n			\n			<div id="sections" class="clearfix">\n				<p class="full-width">\n					<img src="/images/content/Generic_Content_images/career-page.jpg">\n				</p>\n				\n				<div class="big-container">\n					<div class="nine60" class="clearfix">\n						<span class="big-text b-r">Careers</span>\n					</div>\n				</div>\n			</div>\n		\n			<div class="fullWidth">\n				<div class="nine60 clearfix">\n					\n					<aside class="aside-left">\n						{careers_side_text}\n					</aside>\n					\n					<div id="content" class="content-right">\n                        <h1>{title}</h1>\n                        <p>{page_copy}</p>\n                        \n                        <div class="clearfix"></div>\n                        <a href="/careers" class="more-btn btn-l btn-b">Back<span class="more-arrow"><img src="/img/more_arrow_left.png" width="13" height="20" alt="Back" class="arr-l"></span></a>\n                    </div>\n					\n				</div>\n			</div>\n		\n{footer}', NULL, 1361288442, 1, 'n', 0, '', 'n', 'n', 'o', 12),
(28, 1, 1, 'netstock', 'y', 'webpage', '{exp:channel:entries}\n{header}\n		\n		<div id="sections" class="clearfix">\n			<p class="full-width">\n				<img src="/images/content/Generic_Content_images/contact-us-page.jpg">\n			</p>\n			\n			<div class="big-container">\n				<div class="nine60" class="clearfix">\n					<span class="big-text b-r">Contact</span>\n				</div>\n			</div>\n		</div>\n	\n		<div class="fullWidth">\n			<div class="nine60 clearfix">\n				\n				<aside class="aside-left contact">\n					<h2>Contact Info</h2>\n					<p class="address no-bold"><span>Head Office:</span><br>\n					Hareshill Road, Heywood,<br>\n					Lancashire, OL10 2TP</p>\n					<p class="numbers">\n					<span>Coldstore:</span><br>\n					Tel: 01706 694620<br>\n					Fax: 01706 694625</p>\n					<p class="numbers">\n					<span>Accounts:</span><br>\n					Tel: 01706 694640<br>\n					Fax: 01706 694645</p>\n					<p class="numbers">\n					<span>Transport:</span><br>\n					Tel: 01706 694630<br>\n					Fax: 01706 694635</p>\n					<p class="numbers">\n					<span>Customer Services:</span><br>\n					Tel: 01706 694680<br>\n					Fax: 01706 694666</p>\n					<p class="numbers">\n					<span>Food Sales:</span><br>\n					Tel: 01706 694600<br>\n					Fax: 01706 694605</p>\n					<p class="numbers">\n					<span>Belfield:</span><br>\n					Tel: 01706 694610<br>\n					Fax: 01706 694605</p>\n					<p class="phone">Or Call:<br>\n					<strong>01706 694680</strong></p>\n				</aside>\n				\n				<div id="content" class="content-right">\n					<h1>Drop us a line</h1>\n					<p> If you have any queries, please complete the form below or contact us on the phone numbers provided.</p>\n					\n					{exp:freeform:form collection="Contact Form" form:class="contact-form" required="firstname|lastname|email|subject|message|captcha" return="/thank-you" recipients="yes" recipient_limit="1" recipient_template="contact_form"}\n					<input type="hidden" name="recipient_email" value="jamie@shoot-the-moon.co.uk">\n					<div class="w_180"><label>Your first name*:</label></div>\n					<div class="w_450 pr"><input type="text" name="firstname" value="" class="required"></div>\n                    <div class="w_180"><label>Your last name*:</label></div>\n					<div class="w_450 pr"><input type="text" name="lastname" value="" class="required"></div>\n					<div class="w_180"><label>Your email address*:</label></div>\n					<div class="w_450 pr"><input type="text" name="email" value="" class="required"></div>\n					<div class="w_180"><label>Subject*:</label></div>\n					<div class="w_450"><input type="text" name="subject" value=""></div>\n					<div class="w_180"><label>Your message*:</label></div>\n					<div class="w_450 pr"><textarea name="message" class="required"></textarea></div>\n					<div class="w_450 al">\n						<input type="checkbox" name="newsletter" value="Yes" checked> <label>Please un-tick the box if you WOULD NOT like to register for the Yearsley Group e-newsletter and other relevant email communications from us and our associated companies.</label>\n					</div>\n                    \n                    {if captcha}\n                    <div class="w_450 al"><label>Please enter in the word you see:</label></div>\n            		<div class="w_450 al"><span style="top: -2px; position: relative; display: block; height: 30px; width: 140px; margin: 0;">{captcha}</span></div>\n                    <div class="w_450 al"><input type="text" name="captcha" size="20" class="required captcha" style="padding: 15px; width: 140px;" /></div>\n                    {/if}\n                    \n					<input type="submit" name="submit" value="Send" class="submit">\n					{/exp:freeform:form}\n					\n				</div>\n				\n			</div>\n		</div>\n		\n{footer}\n{/exp:channel:entries}', NULL, 1361379195, 1, 'n', 0, '', 'n', 'n', 'o', 52),
(29, 1, 1, 'rss', 'y', 'feed', '{preload_replace:master_channel_name="default_site"}\n{exp:rss:feed channel="news"}\n\n<?xml version="1.0" encoding="{encoding}"?>\n<rss version="2.0"\n    xmlns:dc="http://purl.org/dc/elements/1.1/"\n    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"\n    xmlns:admin="http://webns.net/mvcb/"\n    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"\n    xmlns:content="http://purl.org/rss/1.0/modules/content/">\n\n    <channel>\n    \n    <title><![CDATA[{channel_name}]]></title>\n    <link>{channel_url}</link>\n    <description>{channel_description}</description>\n    <dc:language>{channel_language}</dc:language>\n    <dc:creator>{email}</dc:creator>\n    <dc:rights>Copyright {gmt_date format="%Y"}</dc:rights>\n    <dc:date>{gmt_date format="%Y-%m-%dT%H:%i:%s%Q"}</dc:date>\n    <admin:generatorAgent rdf:resource="http://expressionengine.com/" />\n    \n{exp:channel:entries channel="news" limit="10" dynamic_start="on" disable="member_data|pagination"}\n    <item>\n      <title><![CDATA[{title}]]></title>\n      <link>{title_permalink=''site/index''}</link>\n      <guid>{title_permalink=''site/index''}#When:{gmt_entry_date format="%H:%i:%sZ"}</guid>\n      <description><![CDATA[{summary}{body}]]></description>\n      <dc:subject><![CDATA[{categories backspace="1"}{category_name}, {/categories}]]></dc:subject>\n      <dc:date>{gmt_entry_date format="%Y-%m-%dT%H:%i:%s%Q"}</dc:date>\n    </item>\n{/exp:channel:entries}\n    \n    </channel>\n</rss>\n\n{/exp:rss:feed}', '', 1361533695, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(30, 1, 1, 'employee', 'y', 'webpage', '{exp:channel:entries}\n{header}\n		\n		<div id="sections" class="clearfix">\n			<p class="full-width">\n				<img src="/images/content/Generic_Content_images/netstock-banner.jpg">\n			</p>\n			\n			<div class="big-container">\n				<div class="nine60" class="clearfix">\n					<span class="big-text b-r">Employee Login</span>\n				</div>\n			</div>\n		</div>\n	\n		<div class="fullWidth">\n			<div class="nine60 clearfix">\n				\n				<aside class="aside-left contact">\n					\n				</aside>\n				\n				<div id="content" class="content-right">\n					<h1>Netstock Login</h1>\n					<p style="margin-bottom: 40px;">NetStock is a bespoke on-line system that ensures the smooth transfer of information and product. The process allows Yearsley Group and the client to work as one with the related synergies associated with this.</p>\n					\n					<form class="contact-form" action="#" method="post">\n                        <div class="w_180"><label for="username">Username:</label></div>\n                        <div class="w_450 pr"><input name="username" type="text" id="username" size="23" /></div>\n                        <div class="w_180"><label for="company1">Company:</label></div>\n                        <div class="w_450 pr"><input name="company1" type="text" id="company1" size="23" /></div>\n                        <div class="w_180"><label for="password">Password:</label></div>\n                        <div class="w_450 pr"><input name="password" type="password" id="password" size="23" /></div>\n                        <div class="w_450 pr"><a href="http://extranet.yearsley.co.uk/cgibin/netrtvpwd.PGM">forgotten your password?</a></div>\n                        \n                        <div class="w_450 pr"><a class="submit" href="#" onclick="submitForm()">LOGIN</a></div>\n                        \n                        <div id="incorrect" style="position:relative; font-size:7pt; float:left; color:red; display:none;">Username / Password Incorrect</div>	\n                        <div id="accdisabled" style="position:relative; font-size:7pt; float:left; color:red; display:none; ">Account Disabled</div>\n                    </form>\n                    \n                </div>\n				\n			</div>\n		</div>\n		\n{footer}\n{/exp:channel:entries}', NULL, 1361534870, 1, 'n', 0, '', 'n', 'n', 'o', 21),
(31, 1, 1, 'sitemap', 'y', 'webpage', '{header}\n  	<div id="sections" class="clearfix">\n        <p class="full-width"><img src="/images/content/Generic_Content_images/news-temporary.jpg"></p>\n        <div class="subnav-container">\n            <div class="nine60 clearfix">\n                \n            </div>\n        </div>\n    </div>\n    <div class="fullWidth">\n        <div class="nine60 clearfix">\n            <aside class="aside-left">\n            </aside>\n            \n            <div id="content" class="content-right">\n                <h2 style="margin: 0 0 30px 0; padding-bottom: 0;">Sitemap</h2>\n                <div class="content-wrap">\n                    <ul>\n                        <li><a href="{homepage}">Home</a>\n                            <ul>\n                                {exp:channel:entries channel="pages" limit="50" dynamic="off"}                           \n                                    <li><a href="{page_url}">{title}</a></li> \n                                {/exp:channel:entries}\n                            </ul>\n                        </li> \n    				</ul> \n                </div>\n\n            </div>\n        </div>\n    </div>\n{footer}', '', 1361793514, 1, 'n', 0, '', 'n', 'n', 'o', 77),
(32, 1, 1, 'newsletter-thankyou', 'y', 'webpage', '{exp:channel:entries}\n{header}\n\n    	<div id="sections" class="clearfix">\n            <p class="full-width"><img src="/images/content/Generic_Content_images/contact-us-page.jpg">></p>\n            <div class="big-container">\n				<div class="nine60 clearfix">\n					<span class="big-text b-r">Contact</span>\n				</div>\n			</div>\n        </div>\n        \n        <div class="fullWidth">\n			<div class="nine60 clearfix">\n				\n				<aside class="aside-left contact">\n					<h2>Contact Info</h2>\n					<p class="address no-bold"><span>Head Office:</span><br>\n					Hareshill Road, Heywood,<br>\n					Lancashire, OL10 2TP</p>\n					<p class="no-bold"><span>If you know the department you would like to reach, please call:</span></p>\n					<p class="numbers">\n					<span>Logistics Division:</span><br>\n					Tel: 01706 694680</p>\n					<p class="numbers">\n					<span>Food Sales Division:</span><br>\n					Tel: 01706 694600</p>\n					<p class="numbers">\n					<span>Belfield:</span><br>\n					Tel: 01706 694610</p>\n					<p class="numbers">\n					<span>IcePak:</span><br>\n					Tel: 01706 694655</p>\n					<p class="numbers">\n					<span>Accounts:</span><br>\n					Tel: 01706 694640</p>\n					<p class="numbers">\n					<span>I.T:</span><br>\n					Tel: 01706 694497</p>\n					<p class="numbers">\n					<span>Human Resources:</span><br>\n					Tel: 01706 694277</p>\n					<p class="numbers">\n					<span>Marketing:</span><br>\n					Tel: 01706 694648</p>\n					<p class="phone">For General Enquiries please call:<br>\n					<strong>01706 694600</strong></p>\n				</aside>\n                \n                <div id="content" class="content-right">\n					<h1>{title}</h1>\n					<p>{page_copy}</p>\n                    \n                    <div class="clearfix"></div>\n                    <a href="/contact-us" class="more-btn btn-l btn-b">Back<span class="more-arrow"><img src="/img/more_arrow_left.png" width="13" height="20" alt="More Arrow" class="arr-l"></span></a>\n                </div>\n\n            </div>\n        </div>\n		\n{footer}\n{/exp:channel:entries}', NULL, 1361810940, 1, 'n', 0, '', 'n', 'n', 'o', 8);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_template_groups`
--

INSERT INTO `exp_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`) VALUES
(1, 1, 'yearsley', 1, 'y'),
(2, 2, 'logistics', 2, 'y'),
(3, 3, 'food_sales', 3, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_template_member_groups`
--

INSERT INTO `exp_template_member_groups` (`group_id`, `template_group_id`) VALUES
(6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_throttle`
--

CREATE TABLE IF NOT EXISTS `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_upload_no_access`
--

INSERT INTO `exp_upload_no_access` (`upload_id`, `upload_loc`, `member_group`) VALUES
(1, 'cp', 6),
(3, 'cp', 6);

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_prefs`
--

CREATE TABLE IF NOT EXISTS `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_upload_prefs`
--

INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`) VALUES
(1, 1, 'uploads', '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/uploads/', 'http://yearsley.stmpreview.co.uk/uploads/', 'all', '', '', '', '', '', '', '', '', '', '', NULL),
(2, 1, 'Generic Images', '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/images/content/Generic_Content_images/', 'http://yearsley.stmpreview.co.uk/images/content/Generic_Content_images/', 'all', '', '', '', '', '', '', '', '', '', '', NULL),
(3, 1, 'careersuploads', '/var/www/vhosts/stmpreview.co.uk/subdomains/yearsley/careersuploads/', 'http://yearsley.stmpreview.co.uk/careersuploads/', 'all', '', '', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_wygwam_configs`
--

CREATE TABLE IF NOT EXISTS `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_wygwam_configs`
--

INSERT INTO `exp_wygwam_configs` (`config_id`, `config_name`, `settings`) VALUES
(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTI6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo5OiJTZWxlY3RBbGwiO2k6MTA7czoxMjoiUmVtb3ZlRm9ybWF0IjtpOjExO3M6NjoiU291cmNlIjt9czo2OiJoZWlnaHQiO3M6MzoiMjAwIjtzOjE0OiJyZXNpemVfZW5hYmxlZCI7czoxOiJ5IjtzOjExOiJjb250ZW50c0NzcyI7YTowOnt9czoxMDoidXBsb2FkX2RpciI7czowOiIiO30='),
(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ=='),
(3, 'Page Text', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTQ6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTI6Ik51bWJlcmVkTGlzdCI7aTo2O3M6MTI6IkJ1bGxldGVkTGlzdCI7aTo3O3M6NToiSW1hZ2UiO2k6ODtzOjQ6IkxpbmsiO2k6OTtzOjY6IlVubGluayI7aToxMDtzOjY6IkFuY2hvciI7aToxMTtzOjk6IlNlbGVjdEFsbCI7aToxMjtzOjEyOiJSZW1vdmVGb3JtYXQiO2k6MTM7czo2OiJTb3VyY2UiO31zOjY6ImhlaWdodCI7czozOiIxNTAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjE6IjIiO30='),
(4, 'Side Text', 'YTo1OntzOjc6InRvb2xiYXIiO2E6OTp7aTowO3M6NjoiRm9ybWF0IjtpOjE7czo0OiJCb2xkIjtpOjI7czo2OiJJdGFsaWMiO2k6MztzOjk6IlVuZGVybGluZSI7aTo0O3M6NjoiU3RyaWtlIjtpOjU7czoxMjoiTnVtYmVyZWRMaXN0IjtpOjY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjc7czo1OiJJbWFnZSI7aTo4O3M6NjoiU291cmNlIjt9czo2OiJoZWlnaHQiO3M6MjoiOTAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjE6IjIiO30=');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
