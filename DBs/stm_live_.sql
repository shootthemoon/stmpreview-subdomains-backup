-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2014 at 11:50 AM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stm_live_`
--

-- --------------------------------------------------------

--
-- Table structure for table `exp_accessories`
--

CREATE TABLE IF NOT EXISTS `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_accessories`
--

INSERT INTO `exp_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`) VALUES
(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'),
(3, 'Ep_better_workflow_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.4'),
(4, 'Structure_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '3.3.6');

-- --------------------------------------------------------

--
-- Table structure for table `exp_actions`
--

CREATE TABLE IF NOT EXISTS `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `exp_actions`
--

INSERT INTO `exp_actions` (`action_id`, `class`, `method`) VALUES
(1, 'Comment', 'insert_new_comment'),
(2, 'Comment_mcp', 'delete_comment_notification'),
(3, 'Comment', 'comment_subscribe'),
(4, 'Comment', 'edit_comment'),
(5, 'Email', 'send_email'),
(6, 'Safecracker', 'submit_entry'),
(7, 'Safecracker', 'combo_loader'),
(8, 'Search', 'do_search'),
(9, 'Channel', 'insert_new_entry'),
(10, 'Channel', 'filemanager_endpoint'),
(11, 'Channel', 'smiley_pop'),
(12, 'Member', 'registration_form'),
(13, 'Member', 'register_member'),
(14, 'Member', 'activate_member'),
(15, 'Member', 'member_login'),
(16, 'Member', 'member_logout'),
(17, 'Member', 'retrieve_password'),
(18, 'Member', 'reset_password'),
(19, 'Member', 'send_member_email'),
(20, 'Member', 'update_un_pw'),
(21, 'Member', 'member_search'),
(22, 'Member', 'member_delete'),
(23, 'Rte', 'get_js'),
(25, 'Assets_mcp', 'get_subfolders'),
(26, 'Assets_mcp', 'upload_file'),
(27, 'Assets_mcp', 'get_files_view_by_folders'),
(28, 'Assets_mcp', 'get_props'),
(29, 'Assets_mcp', 'save_props'),
(30, 'Assets_mcp', 'get_ordered_files_view'),
(31, 'Assets_mcp', 'move_folder'),
(32, 'Assets_mcp', 'create_folder'),
(33, 'Assets_mcp', 'delete_folder'),
(34, 'Assets_mcp', 'view_file'),
(35, 'Assets_mcp', 'move_file'),
(36, 'Assets_mcp', 'delete_file'),
(37, 'Assets_mcp', 'build_sheet'),
(38, 'Assets_mcp', 'get_selected_files'),
(39, 'Libraree', 'save_to_file'),
(40, 'Playa_mcp', 'filter_entries'),
(41, 'Updater', 'ACT_general_router'),
(42, 'Zoo_flexible_admin', 'ajax_preview'),
(43, 'Zoo_flexible_admin', 'ajax_load_tree'),
(44, 'Zoo_flexible_admin', 'ajax_load_settings'),
(45, 'Zoo_flexible_admin', 'ajax_save_tree'),
(46, 'Zoo_flexible_admin', 'ajax_remove_tree'),
(47, 'Zoo_flexible_admin', 'ajax_copy_tree'),
(48, 'Structure', 'ajax_move_set_data'),
(49, 'Freeform', 'save_form'),
(50, 'Backup_proish', 'cron');

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets`
--

CREATE TABLE IF NOT EXISTS `exp_assets` (
  `asset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` int(10) unsigned DEFAULT NULL,
  `alt_text` tinytext,
  `caption` tinytext,
  `author` tinytext,
  `desc` text,
  `location` tinytext,
  `keywords` text,
  PRIMARY KEY (`asset_id`),
  UNIQUE KEY `unq_file_path` (`file_path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=429 ;

--
-- Dumping data for table `exp_assets`
--

INSERT INTO `exp_assets` (`asset_id`, `file_path`, `title`, `date`, `alt_text`, `caption`, `author`, `desc`, `location`, `keywords`) VALUES
(1, '{filedir_7}photography/ss_sample_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '{filedir_7}photography/ss_sample_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '{filedir_7}photography/ss_sample_3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '{filedir_1}food/sample4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '{filedir_1}food/sample1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '{filedir_1}product/sample2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '{filedir_1}location/sample9.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '{filedir_1}food/sample5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '{filedir_1}product/sample3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '{filedir_1}location/sample7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '{filedir_1}food/sample6.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '{filedir_1}food/sample8.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '{filedir_7}1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '{filedir_7}2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '{filedir_7}3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '{filedir_2}stm-brand-portfolio-2012-10.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '{filedir_2}stm-brand-portfolio-2012-11.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '{filedir_2}stm-brand-portfolio-2012-20.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '{filedir_2}stm-brand-portfolio-2012-16.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '{filedir_2}stm-brand-portfolio-2012-25.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '{filedir_2}stm-brand-portfolio-2012-4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '{filedir_2}stm-brand-portfolio-2012-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '{filedir_2}stm-brand-portfolio-2012-28.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '{filedir_7}creative/stm-brand-portfolio-2012-26.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '{filedir_7}creative/stm-brand-portfolio-2012-27.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '{filedir_7}creative/stm-brand-portfolio-2012-28.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, '{filedir_2}stm-brand-portfolio-2012-30.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, '{filedir_2}stm-brand-portfolio-2012-33.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, '{filedir_2}stm-brand-portfolio-2012-32.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '{filedir_8}DSC_0222.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '{filedir_8}DSC_0226.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '{filedir_8}DSC_0283.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, '{filedir_4}Original_Squash_«_Jucee.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, '{filedir_3}Alpha---Special-Offers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, '{filedir_7}digital/Untitled-1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, '{filedir_7}digital/Untitled-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, '{filedir_7}digital/Untitled-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '{filedir_7}digital/Untitled-1_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '{filedir_7}digital/ident.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, '{filedir_3}devilsishly.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '{filedir_3}Macphie_Development.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, '{filedir_7}photography/ph1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, '{filedir_7}photography/ph10.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, '{filedir_7}photography/ph11.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, '{filedir_7}photography/ph12.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, '{filedir_7}photography/ph13.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, '{filedir_7}photography/ph14.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '{filedir_7}photography/ph15.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, '{filedir_7}photography/ph16.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, '{filedir_7}photography/ph17.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, '{filedir_7}photography/ph18.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, '{filedir_7}photography/ph2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, '{filedir_7}photography/ph3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, '{filedir_7}photography/ph4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, '{filedir_7}photography/ph5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, '{filedir_7}photography/ph6.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, '{filedir_7}photography/ph7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, '{filedir_7}photography/ph8.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, '{filedir_1}food/ph2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, '{filedir_1}food/ph12.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, '{filedir_1}food/ph4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, '{filedir_1}product/ph13.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, '{filedir_1}product/ph15.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, '{filedir_1}product/ph16.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, '{filedir_1}product/ph14.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, '{filedir_7}studio/olly.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, '{filedir_7}studio/darren.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, '{filedir_1}food/40-coconut-seafood-soup.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, '{filedir_1}food/50-thai-green-curry-1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, '{filedir_1}food/52-hot-thick-red-curry.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '{filedir_1}food/60-stir-fry-garlic-and-pepper.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, '{filedir_1}food/67-three-flavour-sauce-with-duck.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, '{filedir_1}food/85-red-curry-lobster-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, '{filedir_1}food/_15-steamed-mussels.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, '{filedir_1}food/carrs-pastie.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, '{filedir_1}food/chicken-teryaki-best.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, '{filedir_1}food/chickpea,aubergine&butternut-squash-pasta.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, '{filedir_1}food/ColdWaterPrawnsF1307.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, '{filedir_1}food/corned-beef-pie-3-RTsteam.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, '{filedir_1}food/DSC_5065.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, '{filedir_1}food/fish-shop-best.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, '{filedir_1}food/fruit-tart-on-white.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, '{filedir_1}food/herby-chicken.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, '{filedir_1}food/IMG_6015.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, '{filedir_1}food/Napolina-pasta-recipe.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, '{filedir_1}food/nic-test-10.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, '{filedir_1}food/nic-test-14.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, '{filedir_1}food/nic-test-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, '{filedir_1}food/nic-test-4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, '{filedir_1}food/nic-test-7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, '{filedir_1}food/nic-test-one.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, '{filedir_1}product/black-face-watch.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, '{filedir_1}product/Dita-and-Gaga-4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, '{filedir_1}product/DKNY-golden-RT.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, '{filedir_1}product/IlluminateBowlStack.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, '{filedir_1}product/Illuminated3Bowls.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, '{filedir_1}product/IlluminatePlaceSetting.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, '{filedir_1}product/Job_0047.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, '{filedir_1}product/Marc-Jacobs-Background.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, '{filedir_1}product/milk-chocolate.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, '{filedir_1}product/Pages-20-21-Background.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, '{filedir_1}product/Pierre-Cardin.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, '{filedir_1}product/Pierre-fasion.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, '{filedir_1}product/Spicebomb-Page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, '{filedir_1}product/ted-Baker-Window.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, '{filedir_1}product/YSL-Shocking.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, '{filedir_1}people/_K3D3129.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, '{filedir_1}people/_K3D3151.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, '{filedir_1}people/_K3D3156.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, '{filedir_1}people/_K3D3472.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, '{filedir_1}people/_K3D3587.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, '{filedir_1}people/Cardin-ad.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, '{filedir_1}people/Pad-Thai-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, '{filedir_1}people/PCX0510L05---MODEL-C-(Cynthia)2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, '{filedir_1}people/PXX0026W---MODEL-B-(Bella)2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, '{filedir_1}people/Sweet-Mandarin4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, '{filedir_1}location/_K3D1712.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, '{filedir_1}location/chef-cooking-front-cover.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, '{filedir_1}location/DSC_0324.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, '{filedir_1}location/DSC_0407.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, '{filedir_1}location/Page1Movement.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, '{filedir_1}location/Shot2Restaurant3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, '{filedir_1}location/Shot3ProductBenefits.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, '{filedir_1}location/Shot4Equation3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, '{filedir_1}location/Shot6Med3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, '{filedir_1}location/Shot7Pattern2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, '{filedir_1}location/various-17a.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, '{filedir_8}studio_3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, '{filedir_8}DSC_0220.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, '{filedir_8}DSC_0224.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, '{filedir_8}DSC_0228.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, '{filedir_8}DSC_0230.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, '{filedir_8}DSC_0279.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, '{filedir_8}DSC_6820.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, '{filedir_8}photography-studio.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, '{filedir_8}studio_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, '{filedir_8}studio_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, '{filedir_2}Brand/country-style-1-tezzer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, '{filedir_2}Brand/Harbour-Smokehouse.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, '{filedir_7}creative/country-style-1-tezzer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, '{filedir_7}creative/Country-style-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, '{filedir_7}creative/Harbour-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, '{filedir_7}creative/Highbury-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, '{filedir_7}creative/Montana-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, '{filedir_7}creative/Trophy-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, '{filedir_2}Brand/Hewbys.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, '{filedir_2}Brand/Montana.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, '{filedir_2}Brand/Nuba.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, '{filedir_2}Brand/Scorpio-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, '{filedir_2}Brand/Swizzles-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, '{filedir_2}Brand/Trophy-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, '{filedir_2}dm/3-peaks-tezzer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, '{filedir_2}dm/3-peaks.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, '{filedir_2}dm/Aggreko-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, '{filedir_2}dm/aggreko.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, '{filedir_2}dm/Dissoto.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, '{filedir_2}dm/josef-meirs.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, '{filedir_2}dm/Lakeland-1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, '{filedir_2}dm/Thompson-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, '{filedir_2}dm/Thomson-1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(164, '{filedir_7}creative/3-peaks-tezzer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(165, '{filedir_7}creative/Aggreko-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(166, '{filedir_7}creative/Josef-Meirs-Teezer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, '{filedir_7}creative/Lakeland-Teezer1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, '{filedir_7}creative/Lakeland-Teezer2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, '{filedir_7}creative/Goodness-me-Tezzer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(170, '{filedir_7}creative/Humdingers-teezer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(171, '{filedir_7}creative/Montana-teezer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(172, '{filedir_7}creative/Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(173, '{filedir_7}creative/Vimto-tezzer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(174, '{filedir_2}packaging/Advanced-nut-dog.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(175, '{filedir_2}packaging/count-down.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(176, '{filedir_2}packaging/Fish-mongers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(177, '{filedir_2}packaging/Humdingers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(178, '{filedir_2}packaging/John-west.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(179, '{filedir_2}packaging/jucee.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(180, '{filedir_2}packaging/koko-noir.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(181, '{filedir_2}packaging/Kuli.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(182, '{filedir_2}packaging/laziz.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, '{filedir_2}packaging/meadowvale.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(184, '{filedir_2}packaging/Molpol.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(185, '{filedir_2}packaging/Montana-chun.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, '{filedir_2}packaging/Montana.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(187, '{filedir_2}packaging/Morrisons.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(188, '{filedir_2}packaging/Vimto.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(189, '{filedir_2}store-comms/Advanced-nutrition.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, '{filedir_2}store-comms/Bird.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(191, '{filedir_2}store-comms/Dickies.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(192, '{filedir_2}store-comms/Distribution.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(193, '{filedir_2}store-comms/Pet-club.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(194, '{filedir_2}store-comms/Ryman-Furniture-Range-Brochure.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(195, '{filedir_2}store-comms/Ryman-Furniture-Range-POS.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, '{filedir_2}store-comms/Ryman-Store-Graphics.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, '{filedir_2}store-comms/store-open.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(198, '{filedir_7}studio/Camera-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(199, '{filedir_7}studio/DSC_0216.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(200, '{filedir_7}studio/DSC_0220.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(201, '{filedir_7}studio/DSC_0224.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(202, '{filedir_7}studio/DSC_0228.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(203, '{filedir_7}studio/DSC_0230.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(204, '{filedir_7}studio/DSC_0279.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(205, '{filedir_7}studio/DSC_6820.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(206, '{filedir_7}studio/photography-studio.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(207, '{filedir_7}studio/studio_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(208, '{filedir_7}studio/studio_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(209, '{filedir_7}studio/studio_3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(210, '{filedir_7}studio/DSC_0283.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(211, '{filedir_1}hot-thick-red-curry.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(212, '{filedir_1}orchid-banquet.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(213, '{filedir_1}oysters.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(214, '{filedir_1}pannacotta.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(215, '{filedir_1}pea-soup.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(216, '{filedir_1}reduced-fat-custard.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(217, '{filedir_1}rice-wraps.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(218, '{filedir_1}runny-cheese.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(219, '{filedir_1}salad-red.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(220, '{filedir_1}three-flavour-sauce-with-duck.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(221, '{filedir_1}shippams-RS.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(222, '{filedir_1}steak-and-pomigranate.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(223, '{filedir_1}Spicebomb-Page-NEW.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(224, '{filedir_1}white-aviator-watch.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(225, '{filedir_1}Winter-2012-Ed-2-Front-Cover-1-v2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(226, '{filedir_1}YSL-Shocking.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(227, '{filedir_1}hoover.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(228, '{filedir_1}red-kettel.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(229, '{filedir_1}K3D3256.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(230, '{filedir_1}MG0936..jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(231, '{filedir_1}IMG_6086-RT.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(232, '{filedir_1}IMG_6189-RT.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(234, '{filedir_1}personal--49.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(235, '{filedir_1}personal-copy.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(238, '{filedir_1}_Almond-Extract.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(239, '{filedir_1}authentic-cocktail-on-the-beach-.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(240, '{filedir_1}IMG_6012-RT.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(241, '{filedir_2}store-comms/Pick-n-Mix-POS.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(242, '{filedir_2}packaging/Marriages.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, '{filedir_2}packaging/HarbourSmokehouse-HighlandPark.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(244, '{filedir_2}packaging/Devilishly_Delicious_300ml.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, '{filedir_2}packaging/Disotto_NY_Deli_Tubs.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, '{filedir_2}packaging/pinkpanther.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, '{filedir_2}dm/Lakeland_Win12_Option_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, '{filedir_2}dm/Lakeland_Win12_Option_2v1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(249, '{filedir_2}dm/Lakeland_Win12_Option_2v2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(250, '{filedir_2}dm/Thomson_Win12_Option_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, '{filedir_2}dm/Thomson_Win12_Option_2v1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, '{filedir_2}dm/Thomson_Win12_Option_2v2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, '{filedir_2}Brand/Kids_Holiday_Pet_Club_Feb.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(254, '{filedir_2}Brand/Kids_Holiday_Pet_Club_Summer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(255, '{filedir_2}Brand/Vero_Gelato.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(256, '{filedir_2}Brand/Poundbakery.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(257, '{filedir_2}Brand/Sayers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(258, '{filedir_2}Brand/Goodness_Me.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(259, '{filedir_1}product/spain-shirt.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(260, '{filedir_3}active-procurement.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(261, '{filedir_3}caterforce.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(262, '{filedir_3}chefs-selections.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(263, '{filedir_3}devilish-home.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(264, '{filedir_3}devilish-facebook.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(265, '{filedir_3}macphie-homepage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(266, '{filedir_3}marriages.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(267, '{filedir_3}meadowvale.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(268, '{filedir_3}jucee-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(269, '{filedir_3}sayers-screen.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(270, '{filedir_3}scorpio-screen.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(271, '{filedir_3}Yearsley-Screen.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, '{filedir_7}digital/active-procurement.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, '{filedir_7}digital/chefs-selections.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(274, '{filedir_7}digital/delicious.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(275, '{filedir_7}digital/devilish-facebook.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(276, '{filedir_7}digital/jucee-eshot.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(277, '{filedir_7}digital/macphie.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(278, '{filedir_7}digital/marriages.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(279, '{filedir_7}digital/meadowvale-ecrm.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(280, '{filedir_7}digital/meadowvale-inspired.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(281, '{filedir_7}digital/meadowvale.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(282, '{filedir_7}digital/pet-club.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(283, '{filedir_7}digital/scorpio.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(284, '{filedir_7}digital/theo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(285, '{filedir_3}bat-and-ball.m4v', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(286, '{filedir_3}bat-and-ball.webm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(287, '{filedir_3}bat-and-ball.ogv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(288, '{filedir_3}bat-and-ball.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, '{filedir_3}nets.m4v', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(290, '{filedir_3}nets.webm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, '{filedir_3}nets.ogv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, '{filedir_3}nets.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(293, '{filedir_3}Jucee_Umbrella.m4v', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(294, '{filedir_3}Jucee_Umbrella.webm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(295, '{filedir_3}Jucee_Umbrella.ogv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(296, '{filedir_3}Jucee_Umbrella.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, '{filedir_3}two_meadowvale.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(298, '{filedir_3}tcx-combined.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, '{filedir_3}pets.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, '{filedir_3}jucee.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, '{filedir_3}pah-banners.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(302, '{filedir_3}ryman.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, '{filedir_3}pound-bakery.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(304, '{filedir_3}mixed-banners.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(305, '{filedir_2}dm/store-open.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, '{filedir_4}rob_winstanley.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(307, '{filedir_4}Jucee.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(308, '{filedir_4}Ryman.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, '{filedir_4}Lakeland_German.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, '{filedir_4}Lakeland.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, '{filedir_4}Eatery_Menu_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(312, '{filedir_4}Inflight_Guide_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(313, '{filedir_3}pah-banners-updated.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(314, '{filedir_3}pah-ecrm.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(315, '{filedir_3}jucee-ecrm.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, '{filedir_3}ride-away-ecrm.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(317, '{filedir_3}tcx-apps.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(318, '{filedir_3}aunt-bessies-campaign.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, '{filedir_3}ryman-escalator-creative.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(320, '{filedir_3}yearsley.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(321, '{filedir_3}rob-winstanley.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(322, '{filedir_2}packaging/koko_noir_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(323, '{filedir_2}dm/Thomson-Inflight01.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(324, '{filedir_4}josefmeiers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(325, '{filedir_1}location/HapeToys.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(326, '{filedir_4}frankie-and-benny-leeds.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(327, '{filedir_4}vimto-pr.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(328, '{filedir_4}Photography_location_europe.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(329, '{filedir_4}Chiquito.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(330, '{filedir_4}TryThai.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(331, '{filedir_4}Photography_location_manchester.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(332, '{filedir_4}Photography_location_1137a.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(333, '{filedir_4}Hour-Glass.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(334, '{filedir_4}Thomson-Airways-beauty.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(335, '{filedir_4}Thomson-Airways-gifts.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(336, '{filedir_4}JDWilliams.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(337, '{filedir_4}Thomson_Photography_product_spirits.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(338, '{filedir_4}Photography_product_pets_home_fashion.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(339, '{filedir_4}east.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(340, '{filedir_4}food-pr.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(341, '{filedir_4}food-pack-fronts.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(342, '{filedir_4}dessert.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(343, '{filedir_4}Trove-jar-shot.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(344, '{filedir_4}food_for_packaging.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(345, '{filedir_4}test_custard.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(346, '{filedir_4}chicken.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(347, '{filedir_4}breakfast.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(348, '{filedir_4}Napolina-pasta-recipe.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(349, '{filedir_4}the_restaurant_group.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(350, '{filedir_4}wahaca.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(351, '{filedir_4}test_produce.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(352, '{filedir_4}lifestyle-food.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(353, '{filedir_4}test_food-cheese-orange.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(354, '{filedir_4}jaffa_recipes.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(355, '{filedir_4}test_food-cheese-orange_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(356, '{filedir_4}test_beef.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(357, '{filedir_4}starbucks.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(358, '{filedir_4}authentic-cocktail-comp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(359, '{filedir_4}KHPC.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(360, '{filedir_4}kokonoir-flat.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(361, '{filedir_4}rideaway.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(362, '{filedir_4}Jucee001.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(363, '{filedir_4}Lakeland2013.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(364, '{filedir_4}Advanced_Nutrition.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(365, '{filedir_4}Aunt-Bessies-blk.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(366, '{filedir_4}Ryman_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(367, '{filedir_4}RobWinstanleyBrandPod.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(368, '{filedir_4}ride-away1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(369, '{filedir_4}Thomson-Crew-Comms.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(370, '{filedir_2}packaging/Wainwrights_cat02_blk.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(371, '{filedir_4}bn_wainrights_catsFull.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(372, '{filedir_7}creative/jucee002.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(373, '{filedir_7}creative/ryman001.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(374, '{filedir_7}creative/wainwr001.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(375, '{filedir_7}creative/abessi.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(376, '{filedir_7}creative/jucee001.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(377, '{filedir_7}creative/lak001.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(378, '{filedir_7}creative/morpol.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(379, '{filedir_7}creative/ride001.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(380, '{filedir_7}creative/thoms001.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(381, '{filedir_2}packaging/jucee-2013.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(382, '{filedir_2}packaging/Wainwrights_Grain-Free-Range.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(383, '{filedir_4}Wainwrights_Puppy_blk.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(384, '{filedir_7}studio/team03.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(385, '{filedir_7}studio/studio_n26b.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(386, '{filedir_7}studio/team01.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(387, '{filedir_7}studio/studio_n26d.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(388, '{filedir_7}studio/team05.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(389, '{filedir_7}studio/studio_n26e.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(390, '{filedir_7}studio/team02.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(391, '{filedir_7}studio/team04.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(392, '{filedir_7}studio/studio_lightbox.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(393, '{filedir_7}studio/studio_n26a.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(394, '{filedir_7}studio/studio_n26c.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(395, '{filedir_7}studio/team06.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(396, '{filedir_7}studio/team07.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(397, '{filedir_7}studio/team09.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(398, '{filedir_7}studio/team08.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(399, '{filedir_7}studio/team05_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(400, '{filedir_7}studio/studio_lightbox_1.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(401, '{filedir_7}studio/studio_n26a_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(402, '{filedir_7}studio/studio_n26b_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(403, '{filedir_7}studio/studio_n26c_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(404, '{filedir_7}studio/studio_n26d_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, '{filedir_7}studio/studio_n26e_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(406, '{filedir_7}studio/studio_n26z_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(407, '{filedir_7}studio/team01_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(408, '{filedir_7}studio/team02_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(409, '{filedir_7}studio/team03_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(410, '{filedir_7}studio/team04_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(411, '{filedir_7}studio/team06_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(412, '{filedir_7}studio/team07_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(413, '{filedir_7}studio/team08_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(414, '{filedir_7}studio/team09b.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(415, '{filedir_7}studio/Camera-2_470_320_s_c1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(416, '{filedir_7}studio/darren_470_320_s_c1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(417, '{filedir_7}studio/studio_3_470_320_s_c1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(418, '{filedir_7}studio/team02_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(419, '{filedir_7}studio/team08_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(420, '{filedir_7}studio/DSC_0220_470_320_s_c1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(421, '{filedir_7}studio/DSC_0283_470_320_s_c1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(422, '{filedir_7}studio/DSC_6820_470_320_s_c1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(423, '{filedir_7}studio/team09b_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(424, '{filedir_7}team07.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(425, '{filedir_1}product/rideaway-01.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(426, '{filedir_4}rideaway-01B.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(427, '{filedir_3}jucee-website.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(428, '{filedir_3}alpha-apps.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_entries`
--

CREATE TABLE IF NOT EXISTS `exp_assets_entries` (
  `asset_id` int(10) unsigned DEFAULT NULL,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `col_id` int(6) unsigned DEFAULT NULL,
  `row_id` int(10) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `asset_order` int(4) unsigned DEFAULT NULL,
  KEY `asset_id` (`asset_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `col_id` (`col_id`),
  KEY `row_id` (`row_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_assets_entries`
--

INSERT INTO `exp_assets_entries` (`asset_id`, `entry_id`, `field_id`, `col_id`, `row_id`, `var_id`, `asset_order`) VALUES
(20, 37, 2, 1, 101, NULL, 0),
(32, 46, 6, 10, 117, NULL, 0),
(131, 45, 6, 10, 116, NULL, 0),
(132, 44, 6, 10, 115, NULL, 0),
(133, 105, 6, 10, 241, NULL, 0),
(134, 106, 6, 10, 242, NULL, 0),
(135, 107, 6, 10, 243, NULL, 0),
(136, 108, 6, 10, 244, NULL, 0),
(137, 109, 6, 10, 245, NULL, 0),
(138, 110, 6, 10, 246, NULL, 0),
(139, 111, 6, 10, 247, NULL, 0),
(140, 112, 6, 10, 248, NULL, 0),
(131, 113, 6, 10, 249, NULL, 0),
(191, 135, 2, 1, 292, NULL, 0),
(190, 42, 2, 1, 111, NULL, 0),
(242, 170, 2, 1, 362, NULL, 0),
(161, 175, 2, 1, 372, NULL, 0),
(249, 178, 2, 1, 378, NULL, 0),
(259, 191, 2, 1, 403, NULL, 0),
(13, 5, 1, NULL, NULL, NULL, 0),
(261, 192, 2, 1, 413, NULL, 0),
(269, 199, 2, 1, 427, NULL, 0),
(285, 204, 9, 16, 435, NULL, 0),
(286, 204, 9, 17, 435, NULL, 0),
(287, 204, 9, 18, 435, NULL, 0),
(288, 204, 9, 19, 435, NULL, 0),
(289, 205, 9, 16, 436, NULL, 0),
(290, 205, 9, 17, 436, NULL, 0),
(291, 205, 9, 18, 436, NULL, 0),
(292, 205, 9, 19, 436, NULL, 0),
(293, 206, 9, 16, 437, NULL, 0),
(294, 206, 9, 17, 437, NULL, 0),
(295, 206, 9, 18, 437, NULL, 0),
(296, 206, 9, 19, 437, NULL, 0),
(297, 207, 2, 1, 438, NULL, 0),
(272, 3, 1, NULL, NULL, NULL, 0),
(273, 3, 1, NULL, NULL, NULL, 1),
(274, 3, 1, NULL, NULL, NULL, 2),
(275, 3, 1, NULL, NULL, NULL, 3),
(276, 3, 1, NULL, NULL, NULL, 4),
(277, 3, 1, NULL, NULL, NULL, 5),
(278, 3, 1, NULL, NULL, NULL, 6),
(279, 3, 1, NULL, NULL, NULL, 7),
(281, 3, 1, NULL, NULL, NULL, 8),
(282, 3, 1, NULL, NULL, NULL, 9),
(283, 3, 1, NULL, NULL, NULL, 10),
(198, 218, 1, NULL, NULL, NULL, 0),
(70, 218, 1, NULL, NULL, NULL, 1),
(199, 218, 1, NULL, NULL, NULL, 2),
(200, 218, 1, NULL, NULL, NULL, 3),
(238, 220, 2, 1, 459, NULL, 0),
(306, 221, 2, 1, 461, NULL, 0),
(307, 222, 2, 1, 463, NULL, 0),
(152, 116, 2, 1, 254, NULL, 0),
(179, 125, 2, 1, 272, NULL, 0),
(319, 233, 2, 1, 485, NULL, 0),
(318, 232, 2, 1, 483, NULL, 0),
(313, 212, 2, 1, 447, NULL, 0),
(304, 215, 2, 1, 453, NULL, 0),
(142, 39, 2, 1, 105, NULL, 0),
(149, 38, 2, 1, 103, NULL, 0),
(257, 189, 2, 1, 399, NULL, 0),
(255, 186, 2, 1, 394, NULL, 0),
(258, 190, 2, 1, 401, NULL, 0),
(246, 174, 2, 1, 370, NULL, 0),
(245, 173, 2, 1, 368, NULL, 0),
(186, 132, 2, 1, 286, NULL, 0),
(176, 33, 2, 1, 93, NULL, 0),
(322, 126, 2, 1, 274, NULL, 0),
(178, 124, 2, 1, 270, NULL, 0),
(187, 133, 2, 1, 288, NULL, 0),
(185, 131, 2, 1, 284, NULL, 0),
(177, 34, 2, 1, 95, NULL, 0),
(181, 127, 2, 1, 276, NULL, 0),
(244, 172, 2, 1, 366, NULL, 0),
(183, 129, 2, 1, 280, NULL, 0),
(243, 171, 2, 1, 364, NULL, 0),
(184, 130, 2, 1, 282, NULL, 0),
(156, 41, 2, 1, 109, NULL, 0),
(174, 36, 2, 1, 99, NULL, 0),
(182, 128, 2, 1, 278, NULL, 0),
(188, 134, 2, 1, 290, NULL, 0),
(175, 35, 2, 1, 97, NULL, 0),
(151, 115, 2, 1, 252, NULL, 0),
(323, 180, 2, 1, 382, NULL, 0),
(324, 120, 2, 1, 262, NULL, 0),
(325, 238, 2, 1, 493, NULL, 0),
(326, 239, 2, 1, 495, NULL, 0),
(327, 240, 2, 1, 497, NULL, 0),
(328, 241, 2, 1, 499, NULL, 0),
(330, 243, 2, 1, 503, NULL, 0),
(331, 244, 2, 1, 505, NULL, 0),
(332, 245, 2, 1, 507, NULL, 0),
(333, 246, 2, 1, 509, NULL, 0),
(334, 156, 2, 1, 334, NULL, 0),
(228, 159, 2, 1, 340, NULL, 0),
(238, 168, 2, 1, 358, NULL, 0),
(335, 155, 2, 1, 332, NULL, 0),
(227, 158, 2, 1, 338, NULL, 0),
(336, 247, 2, 1, 511, NULL, 0),
(337, 248, 2, 1, 513, NULL, 0),
(329, 242, 2, 1, 501, NULL, 0),
(339, 142, 2, 1, 306, NULL, 0),
(213, 144, 2, 1, 310, NULL, 0),
(340, 153, 2, 1, 328, NULL, 0),
(341, 150, 2, 1, 322, NULL, 0),
(342, 147, 2, 1, 316, NULL, 0),
(345, 149, 2, 1, 320, NULL, 0),
(346, 250, 2, 1, 517, NULL, 0),
(343, 151, 2, 1, 324, NULL, 0),
(344, 148, 2, 1, 318, NULL, 0),
(347, 251, 2, 1, 519, NULL, 0),
(348, 252, 2, 1, 521, NULL, 0),
(349, 253, 2, 1, 523, NULL, 0),
(350, 254, 2, 1, 525, NULL, 0),
(351, 255, 2, 1, 527, NULL, 0),
(352, 256, 2, 1, 529, NULL, 0),
(354, 258, 2, 1, 533, NULL, 0),
(355, 259, 2, 1, 535, NULL, 0),
(356, 260, 2, 1, 537, NULL, 0),
(357, 261, 2, 1, 539, NULL, 0),
(358, 167, 2, 1, 356, NULL, 0),
(338, 249, 2, 1, 515, NULL, 0),
(359, 185, 2, 1, 392, NULL, 0),
(256, 187, 2, 1, 396, NULL, 0),
(150, 114, 2, 1, 250, NULL, 0),
(154, 118, 2, 1, 258, NULL, 0),
(361, 263, 2, 1, 543, NULL, 0),
(362, 264, 2, 1, 545, NULL, 0),
(360, 262, 2, 1, 541, NULL, 0),
(159, 40, 2, 1, 107, NULL, 0),
(305, 141, 2, 1, 304, NULL, 0),
(194, 138, 2, 1, 298, NULL, 0),
(250, 181, 2, 1, 384, NULL, 0),
(363, 225, 2, 1, 547, NULL, 0),
(364, 43, 2, 1, 113, NULL, 0),
(196, 140, 2, 1, 302, NULL, 0),
(241, 169, 2, 1, 360, NULL, 0),
(195, 139, 2, 1, 300, NULL, 0),
(193, 137, 2, 1, 297, NULL, 0),
(366, 266, 2, 1, 550, NULL, 0),
(365, 268, 2, 1, 554, NULL, 0),
(367, 267, 2, 1, 552, NULL, 0),
(368, 269, 2, 1, 556, NULL, 0),
(265, 195, 2, 1, 419, NULL, 0),
(270, 200, 2, 1, 429, NULL, 0),
(263, 49, 2, 1, 129, NULL, 0),
(260, 50, 2, 1, 131, NULL, 0),
(321, 235, 2, 1, 489, NULL, 0),
(264, 194, 2, 1, 417, NULL, 0),
(267, 197, 2, 1, 423, NULL, 0),
(266, 196, 2, 1, 421, NULL, 0),
(262, 193, 2, 1, 415, NULL, 0),
(315, 228, 2, 1, 476, NULL, 0),
(316, 229, 2, 1, 478, NULL, 0),
(314, 209, 2, 1, 475, NULL, 0),
(299, 209, 2, 1, 442, NULL, 0),
(298, 208, 2, 1, 440, NULL, 0),
(302, 213, 2, 1, 449, NULL, 0),
(303, 214, 2, 1, 451, NULL, 0),
(369, 231, 2, 1, 481, NULL, 0),
(370, 270, 2, 1, 558, NULL, 0),
(371, 270, 2, 1, 560, NULL, 0),
(381, 271, 2, 1, 561, NULL, 0),
(382, 272, 2, 1, 563, NULL, 0),
(383, 273, 2, 1, 565, NULL, 0),
(13, 4, 1, NULL, NULL, NULL, 0),
(14, 4, 1, NULL, NULL, NULL, 1),
(2, 4, 1, NULL, NULL, NULL, 2),
(1, 4, 1, NULL, NULL, NULL, 3),
(3, 4, 1, NULL, NULL, NULL, 4),
(45, 4, 1, NULL, NULL, NULL, 5),
(46, 4, 1, NULL, NULL, NULL, 6),
(47, 4, 1, NULL, NULL, NULL, 7),
(48, 4, 1, NULL, NULL, NULL, 8),
(49, 4, 1, NULL, NULL, NULL, 9),
(50, 4, 1, NULL, NULL, NULL, 10),
(51, 4, 1, NULL, NULL, NULL, 11),
(52, 4, 1, NULL, NULL, NULL, 12),
(53, 4, 1, NULL, NULL, NULL, 13),
(54, 4, 1, NULL, NULL, NULL, 14),
(55, 4, 1, NULL, NULL, NULL, 15),
(56, 4, 1, NULL, NULL, NULL, 16),
(57, 4, 1, NULL, NULL, NULL, 17),
(58, 4, 1, NULL, NULL, NULL, 18),
(59, 4, 1, NULL, NULL, NULL, 19),
(60, 4, 1, NULL, NULL, NULL, 20),
(61, 4, 1, NULL, NULL, NULL, 21),
(426, 274, 2, 1, 567, NULL, 0),
(399, 1, 1, NULL, NULL, NULL, 0),
(400, 1, 1, NULL, NULL, NULL, 1),
(408, 1, 1, NULL, NULL, NULL, 2),
(403, 1, 1, NULL, NULL, NULL, 3),
(404, 1, 1, NULL, NULL, NULL, 4),
(405, 1, 1, NULL, NULL, NULL, 5),
(406, 1, 1, NULL, NULL, NULL, 6),
(401, 1, 1, NULL, NULL, NULL, 7),
(407, 1, 1, NULL, NULL, NULL, 8),
(411, 1, 1, NULL, NULL, NULL, 9),
(410, 1, 1, NULL, NULL, NULL, 10),
(424, 1, 1, NULL, NULL, NULL, 11),
(413, 1, 1, NULL, NULL, NULL, 12),
(414, 1, 1, NULL, NULL, NULL, 13),
(415, 1, 1, NULL, NULL, NULL, 14),
(416, 1, 1, NULL, NULL, NULL, 15),
(417, 1, 1, NULL, NULL, NULL, 16),
(409, 1, 1, NULL, NULL, NULL, 17),
(421, 1, 1, NULL, NULL, NULL, 18),
(402, 1, 1, NULL, NULL, NULL, 19),
(420, 1, 1, NULL, NULL, NULL, 20),
(423, 1, 1, NULL, NULL, NULL, 21),
(422, 1, 1, NULL, NULL, NULL, 22),
(372, 2, 1, NULL, NULL, NULL, 0),
(373, 2, 1, NULL, NULL, NULL, 1),
(374, 2, 1, NULL, NULL, NULL, 2),
(375, 2, 1, NULL, NULL, NULL, 3),
(25, 2, 1, NULL, NULL, NULL, 4),
(26, 2, 1, NULL, NULL, NULL, 5),
(164, 2, 1, NULL, NULL, NULL, 6),
(166, 2, 1, NULL, NULL, NULL, 7),
(170, 2, 1, NULL, NULL, NULL, 8),
(376, 2, 1, NULL, NULL, NULL, 9),
(377, 2, 1, NULL, NULL, NULL, 10),
(378, 2, 1, NULL, NULL, NULL, 11),
(379, 2, 1, NULL, NULL, NULL, 12),
(380, 2, 1, NULL, NULL, NULL, 13),
(427, 275, 2, 1, 569, NULL, 0),
(428, 276, 2, 1, 571, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_backup_proish_settings`
--

CREATE TABLE IF NOT EXISTS `exp_backup_proish_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(30) NOT NULL DEFAULT '',
  `setting_value` text NOT NULL,
  `serialized` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_backup_proish_settings`
--

INSERT INTO `exp_backup_proish_settings` (`id`, `setting_key`, `setting_value`, `serialized`) VALUES
(1, 'backup_file_location', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/', 0),
(2, 'backup_store_location', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/backups/', 0),
(3, 'auto_threshold', '0', 0),
(4, 'exclude_paths', 'a:1:{i:0;s:0:"";}', 1),
(5, 'cron_notify_emails', 'a:0:{}', 1),
(6, 'cron_attach_backups', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_captcha`
--

CREATE TABLE IF NOT EXISTS `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_categories`
--

CREATE TABLE IF NOT EXISTS `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_fields`
--

CREATE TABLE IF NOT EXISTS `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_field_data`
--

CREATE TABLE IF NOT EXISTS `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_groups`
--

CREATE TABLE IF NOT EXISTS `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_posts`
--

CREATE TABLE IF NOT EXISTS `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channels`
--

CREATE TABLE IF NOT EXISTS `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `exp_channels`
--

INSERT INTO `exp_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`) VALUES
(1, 1, 'landing_page', 'Landing Page', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 16, 0, 1383039479, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(2, 1, 'creative', 'Creative', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 0, 0, 0, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(3, 1, 'digital', 'Digital', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 2, 0, 1351250232, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(4, 1, 'photography', 'Photography', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 1, 0, 1360677707, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(5, 1, 'the_studio', 'The Studio', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 0, 0, 0, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(6, 1, 'homepage', 'Homepage', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 2, 0, 1360753874, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(7, 1, 'food', 'food', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 20, 0, 1384266739, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(8, 1, 'product', 'Product', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 13, 0, 1386326201, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(9, 1, 'location', 'Location', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 9, 0, 1384259517, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(10, 1, 'personal', 'Personal', 'http://www.shoot-the-moon.co.uk/index.php', '', 'en', 1, 0, 1383039365, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(11, 1, 'testimonials', 'Testimonials', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 1, 0, 1351521484, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(12, 1, 'packaging', 'packaging', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 24, 0, 1385480169, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(14, 1, 'store-comms', 'Store Comms', 'http://www.shoot-the-moon.co.uk/index.php', '', 'en', 10, 0, 1384275109, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(15, 1, 'branding', 'Branding', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 14, 0, 1384272979, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(16, 1, 'advertising-comms', 'Advertising & Comms', 'http://www.shoot-the-moon.co.uk/index.php', '', 'en', 11, 0, 1370351553, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(17, 1, 'studio', 'Studio', 'http://www.shoot-the-moon.co.uk/', '', 'en', 12, 0, 1356277204, 0, '', 2, 'open', 1, NULL, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(18, 1, 'mobile', 'Mobile', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 0, 0, 0, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(19, 1, 'ecrm', 'eCRM', 'http://www.shoot-the-moon.co.uk/index.php', '', 'en', 6, 0, 1382966689, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(20, 1, 'websites', 'Websites', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 13, 0, 1393347808, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(21, 1, 'banners', 'Banners', 'http://www.shoot-the-moon.co.uk/index.php', '', 'en', 4, 0, 1382967167, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(22, 1, 'motion', 'Motion', 'http://www.shoot-the-moon.co.uk/index.php', NULL, 'en', 4, 0, 1357916352, 0, '', 2, 'open', 2, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(23, 1, 'facilities', 'Facilities', 'http://www.shoot-the-moon.co.uk/index.php', '', 'en', 0, 0, 0, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(24, 1, 'mobile-apps', 'Mobile Apps', 'http://www.shoot-the-moon.co.uk/index.php', '', 'en', 2, 0, 1393348920, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_data`
--

CREATE TABLE IF NOT EXISTS `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_data`
--

INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_2`, `field_ft_2`, `field_id_3`, `field_ft_3`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_10`, `field_ft_10`, `field_id_11`, `field_ft_11`, `field_id_12`, `field_ft_12`, `field_id_13`, `field_ft_13`, `field_id_14`, `field_ft_14`) VALUES
(1, 1, 6, '{filedir_7}studio/Camera-2_470_320_s_c1.jpg\n{filedir_7}studio/darren_470_320_s_c1.jpg\n{filedir_7}studio/DSC_0220_470_320_s_c1.jpg\n{filedir_7}studio/DSC_0283_470_320_s_c1.jpg\n{filedir_7}studio/DSC_6820_470_320_s_c1.jpg\n{filedir_7}studio/studio_3_470_320_s_c1.jpg\n{filedir_7}studio/studio_lightbox_1.JPG\n{filedir_7}studio/studio_n26a_1.jpg\n{filedir_7}studio/studio_n26b_1.jpg\n{filedir_7}studio/studio_n26c_1.jpg\n{filedir_7}studio/studio_n26d_1.jpg\n{filedir_7}studio/studio_n26e_1.jpg\n{filedir_7}studio/studio_n26z_1.jpg\n{filedir_7}studio/team01_1.jpg\n{filedir_7}studio/team02_1.jpg\n{filedir_7}studio/team03_1.jpg\n{filedir_7}studio/team04_1.jpg\n{filedir_7}studio/team05_1.jpg\n{filedir_7}studio/team06_1.jpg\n{filedir_7}studio/team08_1.jpg\n{filedir_7}studio/team09b.jpg\n{filedir_7}studio/team09b_1.jpg\n{filedir_7}team07.jpg', 'none', '', 'none', '<p>\n	Shoot the Moon is a collective of experienced creatives, illustrators, developers and photographers with a large chunk of retail, marketing and technical experience.</p>\n<p>\n	We work with a whole host of clients from small fledgling businesses right up to some of the bigger household brands.</p>\n<p>\n	Jobs vary from packaging and branding to product photography, POS &amp; literature, both online and offline. Have a look through the portfolios and Show Reels; we&#39;ve included a range of recent work. If you would like to meet up or drop in for a coffee, give us a call or send an e-mail and we&#39;ll be in touch.</p>', 'none', '1', 'none', '', 'none', '<p>\n	Flexible, straight talking, and offering heaps of effective ideas &ndash; placing understanding, quality, and consistency above all else.</p>', 'none', '', 'none', '', 'none', 'Digital Design Agency Manchester - Shoot The Moon', 'none', 'Shoot the Moon are a friendly, straight talking design agency based in Manchester specialising in photography, creative and digital.', 'none', 'Monthly', 'none', '1', 'none'),
(2, 1, 1, '{filedir_7}creative/stm-brand-portfolio-2012-27.jpg\n{filedir_7}creative/stm-brand-portfolio-2012-28.jpg\n{filedir_7}creative/3-peaks-tezzer.jpg\n{filedir_7}creative/Josef-Meirs-Teezer.jpg\n{filedir_7}creative/Humdingers-teezer.jpg\n{filedir_7}creative/jucee002.jpg\n{filedir_7}creative/ryman001.jpg\n{filedir_7}creative/wainwr001.jpg\n{filedir_7}creative/abessi.jpg\n{filedir_7}creative/jucee001.jpg\n{filedir_7}creative/lak001.jpg\n{filedir_7}creative/morpol.jpg\n{filedir_7}creative/ride001.jpg\n{filedir_7}creative/thoms001.jpg', 'none', '', 'none', '<p>\n	Whether you&#39;re looking for fresh new creative, inspiring images or you&#39;re simply curious about what a new perspective on your existing creative could bring, we&#39;re sure we can help you find what you&#39;re looking for.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Branding, Packaging, Direct Mail & Creatives From Shoot The Moon - Manchester', 'none', 'Imaginative thinking, creative flair & exacting technique come together to create outstanding creative at Manchester based Shoot the Moon ', 'none', 'Weekly', 'none', '1', 'none'),
(3, 1, 1, '{filedir_7}digital/active-procurement.jpg\n{filedir_7}digital/chefs-selections.jpg\n{filedir_7}digital/delicious.jpg\n{filedir_7}digital/devilish-facebook.jpg\n{filedir_7}digital/jucee-eshot.jpg\n{filedir_7}digital/macphie.jpg\n{filedir_7}digital/marriages.jpg\n{filedir_7}digital/meadowvale-ecrm.jpg\n{filedir_7}digital/meadowvale.jpg\n{filedir_7}digital/pet-club.jpg\n{filedir_7}digital/scorpio.jpg', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(4, 1, 1, '{filedir_7}1.jpg\n{filedir_7}2.jpg\n{filedir_7}photography/ph1.jpg\n{filedir_7}photography/ph10.jpg\n{filedir_7}photography/ph11.jpg\n{filedir_7}photography/ph12.jpg\n{filedir_7}photography/ph13.jpg\n{filedir_7}photography/ph14.jpg\n{filedir_7}photography/ph15.jpg\n{filedir_7}photography/ph16.jpg\n{filedir_7}photography/ph17.jpg\n{filedir_7}photography/ph18.jpg\n{filedir_7}photography/ph2.jpg\n{filedir_7}photography/ph3.jpg\n{filedir_7}photography/ph4.jpg\n{filedir_7}photography/ph5.jpg\n{filedir_7}photography/ph6.jpg\n{filedir_7}photography/ph7.jpg\n{filedir_7}photography/ph8.jpg\n{filedir_7}photography/ss_sample_1.jpg\n{filedir_7}photography/ss_sample_2.jpg\n{filedir_7}photography/ss_sample_3.jpg', 'none', '', 'none', '<p>\n	Still life, lifestyle, location or set photography; Shoot the Moon has a wealth of experience in creating stunning images for design, advertising, packaging and editorial.</p>\n<p>\n	If you want to talk megapixels we&#39;d be happy to oblige &ndash; but take it as read that we have invested heavily in &#39;kit&#39; to give our team the best tools to work with.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(5, 1, 1, '{filedir_7}1.jpg', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(10, 1, 1, '', 'none', '', 'none', '<p>\n	With over 60 years combined experience in packaging development, we offer an end to end service from concept development through to repro. We can also manage the print process on your behalf and with the advantage of having photography in-house, the service is seamless.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(11, 1, 1, '', 'none', '', 'none', '<p>\n	From a full revamp to individual pieces of POS, we have experience in both retail and wholesale, B2B and B2C &ndash; creating clear, concise communication that works, drives footfall and clearly reflects your category priorities.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(12, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(13, 1, 1, '', 'none', '', 'none', '<p>\n	New brands or a polish, or repositioning, of an existing brand &ndash; working with our clients from conception to implementation and ongoing brand integrity across all media.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Branding & Rebranding in Manchester From Shoot The Moon', 'none', 'Whether you want a new brand, rebrand or just a refresh we’ve got the experience & design knowledge to deliver the results you need.', 'none', 'Weekly', 'none', '1', 'none'),
(14, 1, 3, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(15, 1, 3, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(16, 1, 3, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(17, 1, 3, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(18, 1, 1, '', 'none', '1', 'none', '<p>\n	Creating perfect food is artistry in itself. We have a roster of acclaimed stylists, each with their own niche. Leave it to us or in conjunction with your own development team we&#39;ll come up with a range of tempting ideas and stunning images.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(19, 1, 1, '', 'none', '1', 'none', '<p>\n	Shoot the Moon has a wealth of experience in creating stunning images for design, advertising, packaging and editorial.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(20, 1, 1, '', 'none', '1', 'none', '<p>\n	Be it a few shots at your office or a full project managed shoot &ndash; we are experienced in developing style boards, and selecting locations and models, to ensure the end result is a true reflection of your brand.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(21, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(31, 1, 11, '', 'none', '', 'none', '', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(32, 1, 1, '', 'none', '', 'none', '<p>\n	Corporate literature, sales brochures, catalogues, price lists, full sales presenters, newsletters, magazines... the combination of good creative, stunning photography, technical expertise and support from illustrators &amp; copywriters, all makes for a good team effort and happy customers.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(33, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(34, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(35, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(36, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(38, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(39, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(40, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(41, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(42, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(43, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(44, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(45, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(46, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(49, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(50, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(105, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(106, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(107, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(108, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(109, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(110, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(111, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(112, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(113, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(114, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(115, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(116, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(118, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(120, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(124, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(125, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(126, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(127, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(128, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(129, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(130, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(131, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(132, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(133, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(134, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(135, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(137, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(138, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(139, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(140, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(141, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(142, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(143, 1, 7, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(144, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(147, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(148, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(149, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(150, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(151, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(153, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(154, 1, 8, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(155, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(156, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(157, 1, 8, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(158, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(159, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(167, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(168, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(169, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(170, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(171, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(172, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(173, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(174, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(175, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(178, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(179, 1, 16, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(180, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(181, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(185, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(186, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(187, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(189, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(190, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(191, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(192, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(193, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(194, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(195, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(196, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(197, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(199, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(200, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(202, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(203, 1, 22, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(204, 1, 22, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(205, 1, 22, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(206, 1, 22, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(207, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(208, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(209, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(211, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(212, 1, 21, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(213, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(214, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(215, 1, 21, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(216, 1, 4, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu molestie leo. Integer aliquet nibh et leo facilisis varius. Praesent eu nulla mi, at iaculis neque. Suspendisse tincidunt elementum rutrum. Nullam tortor enim, pharetra non pharetra quis, dictum in est. Vestibulum porttitor vehicula tellus, molestie ultrices elit rhoncus venenatis. Vivamus vel tellus velit. Proin vitae est neque, vel tristique velit. In hac habitasse platea dictumst. Integer egestas, purus non aliquam vehicula, turpis erat lacinia magna, eu suscipit turpis elit a orci. Maecenas tempus mi ac quam malesuada eget feugiat nulla tempus. Aenean orci elit, hendrerit at venenatis quis, pretium non enim.</p>', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(217, 1, 1, '', 'none', '', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu molestie leo. Integer aliquet nibh et leo facilisis varius. Praesent eu nulla mi, at iaculis neque. Suspendisse tincidunt elementum rutrum. Nullam tortor enim, pharetra non pharetra quis, dictum in est. Vestibulum porttitor vehicula tellus, molestie ultrices elit rhoncus venenatis. Vivamus vel tellus velit. Proin vitae est neque, vel tristique velit. In hac habitasse platea dictumst. Integer egestas, purus non aliquam vehicula, turpis erat lacinia magna, eu suscipit turpis elit a orci. Maecenas tempus mi ac quam malesuada eget feugiat nulla tempus. Aenean orci elit, hendrerit at venenatis quis, pretium non enim.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(218, 1, 6, '{filedir_7}studio/darren.jpg\n{filedir_7}studio/Camera-2.jpg\n{filedir_7}studio/DSC_0216.jpg\n{filedir_7}studio/DSC_0220.jpg', 'none', '', 'none', '<p>\n	...and starts with a thorough understanding of the desired result.</p>\n<p>\n	Creative should not be open to interpretation or simply emotive to look at; it needs a desired outcome and insight into what will underpin delivery.</p>\n<p>\n	Our broad creative experience, wide marketing knowledge, ability to digest insight and a thirst for really getting underneath a project, means that we usually get involved way before there is a creative brief in sight.</p>\n<p>\n	Indeed, we offer the best value when simply faced with a client&#39;s dilemma - we&#39;ll construct the brief once we&#39;ve helped to define the strategy...</p>', 'none', '1', 'none', '', 'none', '<p>\n	It&#39;s all in<br />\n	the thinking...</p>', 'none', '', NULL, '<h2 class="rightheads-title">\n	Getting the right heads around the table</h2>\n<p>\n	Outside the agency, we have a number of specialist resources to support thinking, from primary research and localistion expertise to scenario planning for NPD.</p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(219, 1, 1, '', 'none', '', 'none', '<p>\n	Dapibus porro nulla sem exercitationem est massa quisquam incidunt porta corporis animi doloribus tortor magnis accusantium nascetur hic convallis nec exercitationem ullamcorper vestibulum natus torquent ullamco phasellus, iaculis. Rerum class.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(220, 1, 23, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(221, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(222, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(223, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(224, 1, 16, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(225, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(226, 1, 14, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(227, 1, 14, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(228, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(229, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(230, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(231, 1, 24, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(232, 1, 21, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(233, 1, 21, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(235, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(236, 1, 10, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(237, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(238, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(239, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(240, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(241, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(242, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(243, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(244, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(245, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(246, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(247, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(248, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(249, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(250, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(251, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(252, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(253, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(254, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(255, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(256, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(258, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(259, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(260, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(261, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(262, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(263, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(264, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(266, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(267, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(268, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(269, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(270, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(271, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(272, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(273, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(274, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(275, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', 'Never', 'none', '0', 'none'),
(276, 1, 24, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', 'Never', 'none', '0', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_entries_autosave`
--

CREATE TABLE IF NOT EXISTS `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_fields`
--

CREATE TABLE IF NOT EXISTS `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `exp_channel_fields`
--

INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`) VALUES
(1, 1, 1, 'cf_rotating_image_blocks', 'Rotating Image Blocks', 'These are the images fading at the top of each department page', 'assets', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 0, 0, 'n', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YToxMDp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czoxOiI3Ijt9czo1OiJtdWx0aSI7czoxOiJ5IjtzOjQ6InZpZXciO3M6NjoidGh1bWJzIjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO31zOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(2, 1, 1, 'cf_portfolio_images', 'Portfolio Images', 'These are the main folio shots - Don''t worry about uploading the correct size as the CMS will scale for you.', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 0, 0, 'n', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MToiMSI7aToxO3M6MToiMiI7fX0='),
(3, 1, 1, 'cf_introduction_text', 'Introduction text', '', 'wygwam', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(6, 1, 1, 'cf_studio_shots', 'Studio Shots', 'These are the company shots - Don''t worry about uploading the correct size as the CMS will scale for you.', 'matrix', '', '0', 0, 0, 'channel', 15, 'title', 'desc', 0, 0, 0, 'n', 'ltr', 'y', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO3M6MjoiMTAiO2k6MTtzOjI6IjExIjtpOjI7czoyOiIxMiI7fX0='),
(7, 1, 1, 'testimonials', 'Testimonials', '', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO2k6MTM7aToxO2k6MTQ7aToyO2k6MTU7fX0='),
(8, 1, 1, 'homepage_pull_quote', 'Homepage Pull Quote', '', 'wygwam', '', '0', 0, 0, 'channel', 15, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(9, 1, 2, 'video_upload', 'Video upload', 'You will first need to use a conversion tool such as http://easyhtml5video.com/ This will help you convert your video into the formats required below.', 'matrix', '', '0', 0, 0, 'channel', 15, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjQ6e2k6MDtzOjI6IjE2IjtpOjE7czoyOiIxNyI7aToyO3M6MjoiMTgiO2k6MztzOjI6IjE5Ijt9fQ=='),
(10, 1, 1, 'cf_rightheads_text', 'Thinkingpage Right heads Text', '', 'wygwam', '', '0', 0, 0, 'channel', 21, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(11, 1, 1, 'browser_title', 'Browser Title', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(12, 1, 1, 'meta_description', 'Meta Description', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 165, 'n', 'ltr', 'n', 'n', 'none', 'n', 9, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(13, 1, 1, 'change_frequency', 'Change Frequency', '', 'select', 'Never\nDaily\nWeekly\nMonthly\nAlways', 'n', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(14, 1, 1, 'priority', 'Priority', '', 'select', '0\n0.1\n0.2\n0.3\n0.4\n0.5\n0.6\n0.7\n0.8\n0.9\n1', 'n', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 11, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_titles`
--

CREATE TABLE IF NOT EXISTS `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=277 ;

--
-- Dumping data for table `exp_channel_titles`
--

INSERT INTO `exp_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `pentry_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `dst_enabled`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`) VALUES
(1, 1, 6, 1, 0, NULL, '109.111.197.225', 'The Studio', 'the-studio', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351165163, 'n', '2012', '10', '25', 0, 0, 20131210153524, 0, 0),
(2, 1, 1, 1, 0, NULL, '109.111.197.225', 'Creative', 'creative', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351165250, 'n', '2012', '10', '25', 0, 0, 20131210153851, 0, 0),
(3, 1, 1, 1, 0, NULL, '109.111.197.225', 'Digital', 'digital', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351165232, 'n', '2012', '10', '25', 0, 0, 20130116105133, 0, 0),
(4, 1, 1, 1, 0, NULL, '109.111.197.225', 'Photography', 'photography', 'open', 'y', 0, 0, 0, 0, 'n', 'y', 1351165248, 'n', '2012', '10', '25', 0, 0, 20131206151549, 0, 0),
(5, 1, 1, 1, 0, NULL, '109.111.197.225', 'Get in Touch', 'get-in-touch', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351165264, 'n', '2012', '10', '25', 0, 0, 20130111124905, 0, 0),
(10, 1, 1, 1, 0, NULL, '109.111.197.225', 'Packaging', 'packaging', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250044, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(11, 1, 1, 1, 0, NULL, '109.111.197.225', 'Store Comms', 'store-comms', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250164, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(12, 1, 1, 1, 0, NULL, '127.0.0.1', 'Advertising', 'advertising', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1351250164, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(13, 1, 1, 1, 0, NULL, '109.111.197.225', 'Branding', 'branding', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250206, 'n', '2012', '10', '26', 0, 0, 20131210153947, 0, 0),
(14, 1, 3, 1, 0, NULL, '109.111.197.225', 'Websites', 'websites', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250224, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(15, 1, 3, 1, 0, NULL, '109.111.197.225', 'eCRM', 'ecrm', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250232, 'n', '2012', '10', '26', 0, 0, 20130111155613, 0, 0),
(16, 1, 3, 1, 0, NULL, '127.0.0.1', 'Strategy', 'marketing', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1351250224, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(17, 1, 3, 1, 0, NULL, '109.111.197.225', 'Mobile', 'mobile', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1351250284, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(18, 1, 1, 1, 0, NULL, '109.111.197.225', 'Food', 'food', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250224, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(19, 1, 1, 1, 0, NULL, '109.111.197.225', 'Product', 'product', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1335183207, 'n', '2012', '04', '23', 0, 0, 20131206151728, 0, 0),
(20, 1, 1, 1, 0, NULL, '109.111.197.225', 'Location', 'location', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250224, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(21, 1, 1, 1, 0, NULL, '109.111.197.225', 'Personal', 'personal', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250364, 'n', '2012', '10', '26', 0, 0, 20131108144825, 0, 0),
(31, 1, 11, 1, 0, NULL, '109.111.197.225', 'Testimonials', 'testimonials', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351521484, 'n', '2012', '10', '29', 0, 0, 20130111124905, 0, 0),
(32, 1, 1, 1, 0, NULL, '109.111.197.225', 'Advertising & Comms', 'advertising-comms', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349784807, 'n', '2012', '10', '09', 0, 0, 20131108143828, 0, 0),
(33, 1, 12, 1, 0, NULL, '109.111.197.225', 'Fish Mongers', 'packaging-one', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345810398, 'n', '2012', '08', '24', 0, 0, 20131108170619, 0, 0),
(34, 1, 12, 1, 0, NULL, '109.111.197.225', 'Humdingers - Concept Development', 'packaging-two', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1342613633, 'n', '2012', '07', '18', 0, 0, 20131111090954, 0, 0),
(35, 1, 12, 1, 0, NULL, '109.111.197.225', 'Countdown - Rivington Biscuits', 'packaging-three', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1321877628, 'n', '2011', '11', '21', 0, 0, 20131111103249, 0, 0),
(36, 1, 12, 1, 0, NULL, '109.111.197.225', 'Advanced Nutrition - Pets at Home', 'packaging-four', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327925626, 'n', '2012', '01', '30', 0, 0, 20131111100447, 0, 0),
(38, 1, 15, 1, 0, NULL, '109.111.197.225', 'Hewby''s Bakehouse', 'branding-one', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327666420, 'n', '2012', '01', '27', 0, 0, 20131108164241, 0, 0),
(39, 1, 15, 1, 0, NULL, '109.111.197.225', 'Harbour Smokehouse', 'branding-two', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339416798, 'n', '2012', '06', '11', 0, 0, 20131108164119, 0, 0),
(40, 1, 16, 1, 0, NULL, '109.111.197.225', 'Disotto Foods', 'brochure-one', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1337688798, 'n', '2012', '05', '22', 0, 0, 20131112162419, 0, 0),
(41, 1, 12, 1, 0, NULL, '109.111.197.225', 'Literature Item One', 'literature-item-one', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327925609, 'n', '2012', '01', '30', 0, 0, 20131111095330, 0, 0),
(42, 1, 14, 1, 0, NULL, '109.111.197.225', 'Pets at Home - Wild Bird', 'point-of-sale-one', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1337602384, 'n', '2012', '05', '21', 0, 0, 20130111124905, 0, 0),
(43, 1, 14, 1, 0, NULL, '109.111.197.225', 'Advanced Nutrition - Pets at Home', 'point-of-sale-two', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351532188, 'n', '2012', '10', '29', 0, 0, 20131112163529, 0, 0),
(44, 1, 17, 1, 0, NULL, '109.157.36.243', 'People', 'people', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1352137144, 'n', '2012', '11', '05', 0, 0, 20130111124905, 0, 0),
(45, 1, 17, 1, 0, NULL, '109.157.36.243', 'Thinking', 'thinking', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1352137204, 'n', '2012', '11', '05', 0, 0, 20130111124905, 0, 0),
(46, 1, 17, 1, 0, NULL, '109.157.36.243', 'Jamie', 'jamie', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1352137204, 'n', '2012', '11', '05', 0, 0, 20130111124905, 0, 0),
(49, 1, 20, 1, 0, NULL, '109.111.197.225', 'Devilishly Delicious', 'devilishly-delicious', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353082401, 'n', '2012', '11', '16', 0, 0, 20131112170822, 0, 0),
(50, 1, 20, 1, 0, NULL, '109.111.197.225', 'Active Procurement', 'macphie-of-glenbervie', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353082972, 'n', '2012', '11', '16', 0, 0, 20131112170853, 0, 0),
(105, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio3', 'studio3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277084, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(106, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio4', 'studio4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277084, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(107, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio5', 'studio5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277084, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(108, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio6', 'studio6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277144, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(109, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio7', 'studio7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277144, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(110, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio8', 'studio8', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277144, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(111, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio9', 'studio9', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277204, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(112, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio10', 'studio10', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277204, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(113, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio11', 'studio11', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277204, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(114, 1, 15, 1, 0, NULL, '109.111.197.225', 'Montana', 'brand3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1331554397, 'n', '2012', '03', '12', 0, 0, 20131112160918, 0, 0),
(115, 1, 15, 1, 0, NULL, '109.111.197.225', 'Nuba Cocktails', 'brand4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339762405, 'n', '2012', '06', '15', 0, 0, 20131111132426, 0, 0),
(116, 1, 15, 1, 0, NULL, '109.111.197.225', 'Scorpio Worldwide', 'brand5', 'closed', 'y', 0, 0, 0, 0, 'y', 'y', 1343909616, 'n', '2012', '08', '02', 0, 0, 20131108141137, 0, 0),
(118, 1, 15, 1, 0, NULL, '109.111.197.225', 'Trophy - Rivington Biscuits', 'brand7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1329048834, 'n', '2012', '02', '12', 0, 0, 20131112161055, 0, 0),
(120, 1, 16, 1, 0, NULL, '109.111.197.225', 'Josef Meirs', 'dm4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350821598, 'n', '2012', '10', '21', 0, 0, 20131111150519, 0, 0),
(124, 1, 12, 1, 0, NULL, '109.111.197.225', 'Morpol', 'packaging6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345551220, 'n', '2012', '08', '21', 0, 0, 20131108173341, 0, 0),
(125, 1, 12, 1, 0, NULL, '109.111.197.225', 'Jucee - Princes Food & Drink Group', 'packaging7', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1354277636, 'n', '2012', '11', '30', 0, 0, 20131108142857, 0, 0),
(126, 1, 12, 1, 0, NULL, '109.111.197.225', 'Kokonoir', 'packaging8', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1340108011, 'n', '2012', '06', '19', 0, 0, 20131108172832, 0, 0),
(127, 1, 12, 1, 0, NULL, '109.111.197.225', 'Kuli', 'packaging9', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1336997599, 'n', '2012', '05', '14', 0, 0, 20131111091920, 0, 0),
(128, 1, 12, 1, 0, NULL, '109.111.197.225', 'Laziz', 'packaging10', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1323087227, 'n', '2011', '12', '05', 0, 0, 20131111100848, 0, 0),
(129, 1, 12, 1, 0, NULL, '109.111.197.225', 'Meadow Vale Foods', 'packaging11', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1334751224, 'n', '2012', '04', '18', 0, 0, 20131111092445, 0, 0),
(130, 1, 12, 1, 0, NULL, '109.111.197.225', 'Harbour Smokehouse', 'packaging12', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356342263, 'n', '2012', '12', '24', 0, 0, 20131111094924, 0, 0),
(131, 1, 12, 1, 0, NULL, '109.111.197.225', 'Montana - Rivington Biscuits', 'packaging13', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353327218, 'n', '2012', '11', '19', 0, 0, 20131108173639, 0, 0),
(132, 1, 12, 1, 0, NULL, '109.111.197.225', 'Fairfield Farm Crisps', 'packaging14', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1342527222, 'n', '2012', '07', '17', 0, 0, 20131108170343, 0, 0),
(133, 1, 12, 1, 0, NULL, '109.111.197.225', 'CP Foods Concepts', 'packaging15', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327061617, 'n', '2012', '01', '20', 0, 0, 20131108173438, 0, 0),
(134, 1, 12, 1, 0, NULL, '109.111.197.225', 'Vimto', 'packaging16', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1321877603, 'n', '2011', '11', '21', 0, 0, 20131111102124, 0, 0),
(135, 1, 14, 1, 0, NULL, '109.111.197.225', 'Dickies - Concept Work', 'store-comms-3', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1356342844, 'n', '2012', '12', '24', 0, 0, 20130111124905, 0, 0),
(137, 1, 14, 1, 0, NULL, '109.111.197.225', 'Kids'' Holiday Pet Club', 'store-comms-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1329221594, 'n', '2012', '02', '14', 0, 0, 20131112164215, 0, 0),
(138, 1, 16, 1, 0, NULL, '109.111.197.225', 'Ryman Furniture Showroom', 'store-comms-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1337170412, 'n', '2012', '05', '16', 0, 0, 20131112162833, 0, 0),
(139, 1, 14, 1, 0, NULL, '109.111.197.225', 'Ryman Furniture Showroom', 'store-comms-7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327320836, 'n', '2012', '01', '23', 0, 0, 20131112163957, 0, 0),
(140, 1, 14, 1, 0, NULL, '109.111.197.225', 'Ryman the Stationers', 'store-comms-8', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1332072801, 'n', '2012', '03', '18', 0, 0, 20131112163722, 0, 0),
(141, 1, 16, 1, 0, NULL, '109.111.197.225', 'Pets at Home - Store Openings', 'store-comms-9', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1337688812, 'n', '2012', '05', '22', 0, 0, 20131112162633, 0, 0),
(142, 1, 7, 1, 0, NULL, '109.111.197.225', 'Eastern dishes', 'food1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353413579, 'n', '2012', '11', '20', 0, 0, 20131112134200, 0, 0),
(143, 1, 7, 1, 0, NULL, '109.111.197.225', 'Orchid Banquet', 'food2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350994418, 'n', '2012', '10', '23', 0, 0, 20131112134539, 0, 0),
(144, 1, 7, 1, 0, NULL, '109.111.197.225', 'Oysters', 'food3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353240819, 'n', '2012', '11', '18', 0, 0, 20131112134240, 0, 0),
(147, 1, 7, 1, 0, NULL, '109.111.197.225', 'Desserts', 'food6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346847192, 'n', '2012', '09', '05', 0, 0, 20131112134913, 0, 0),
(148, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food Packaging', 'food7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345032780, 'n', '2012', '08', '15', 0, 0, 20131112141101, 0, 0),
(149, 1, 7, 1, 0, NULL, '109.111.197.225', 'Custard', 'food8', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1343996023, 'n', '2012', '08', '03', 0, 0, 20131112140144, 0, 0),
(150, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food pack fronts', 'food9', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349698383, 'n', '2012', '10', '08', 0, 0, 20131112134704, 0, 0),
(151, 1, 7, 1, 0, NULL, '109.111.197.225', 'Trove', 'food10', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346069612, 'n', '2012', '08', '27', 0, 0, 20131112140933, 0, 0),
(153, 1, 7, 1, 0, NULL, '109.111.197.225', 'PR Food shots', 'food12', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350994387, 'n', '2012', '10', '23', 0, 0, 20131112134508, 0, 0),
(154, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product1', 'product1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339416828, 'n', '2012', '06', '11', 0, 0, 20131112130149, 0, 0),
(155, 1, 8, 1, 0, NULL, '109.111.197.225', 'Thomson Airways gifts', 'product2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1330085609, 'n', '2012', '02', '24', 0, 0, 20131112130730, 0, 0),
(156, 1, 8, 1, 0, NULL, '109.111.197.225', 'Thomson Airways Beauty and Fragrance', 'product3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1348747996, 'n', '2012', '09', '27', 0, 0, 20131112130117, 0, 0),
(157, 1, 8, 1, 0, NULL, '109.111.197.225', 'YSL', 'product4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1332763989, 'n', '2012', '03', '26', 0, 0, 20131112130610, 0, 0),
(158, 1, 8, 1, 0, NULL, '109.111.197.225', 'Vacuum Cleaner', 'product5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1326197617, 'n', '2012', '01', '10', 0, 0, 20131112132038, 0, 0),
(159, 1, 8, 1, 0, NULL, '109.111.197.225', 'Red Kettle', 'product6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1336911190, 'n', '2012', '05', '13', 0, 0, 20131112130511, 0, 0),
(167, 1, 8, 1, 0, NULL, '109.111.197.225', 'Authentic Cocktails', 'product61', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339330437, 'n', '2012', '06', '10', 0, 0, 20131112143758, 0, 0),
(168, 1, 8, 1, 0, NULL, '109.111.197.225', 'Almond Extract', 'product7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1334664822, 'n', '2012', '04', '17', 0, 0, 20131112130543, 0, 0),
(169, 1, 14, 1, 0, NULL, '109.111.197.225', 'Pick and Mix', 'pick-and-mix', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1324383181, 'n', '2011', '12', '20', 0, 0, 20131112163902, 0, 0),
(170, 1, 12, 1, 0, NULL, '109.111.197.225', 'Marriages', 'marriages', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1357314415, 'n', '2013', '01', '04', 0, 0, 20130123163956, 0, 0),
(171, 1, 12, 1, 0, NULL, '109.111.197.225', 'Harbour Smoke House', 'harbour-smoke-house', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357314437, 'n', '2013', '01', '04', 0, 0, 20131111094818, 0, 0),
(172, 1, 12, 1, 0, NULL, '109.111.197.225', 'Devilishly Delicious', 'devilishly-delicious', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1338207199, 'n', '2012', '05', '28', 0, 0, 20131111092320, 0, 0),
(173, 1, 12, 1, 0, NULL, '109.111.197.225', 'Disotto', 'disotto', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1333368798, 'n', '2012', '04', '02', 0, 0, 20131108170119, 0, 0),
(174, 1, 12, 1, 0, NULL, '109.111.197.225', 'Pink Panther', 'pink-panther', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1348229604, 'n', '2012', '09', '21', 0, 0, 20131108170025, 0, 0),
(175, 1, 16, 1, 0, NULL, '109.111.197.225', 'Lakeland', 'lakeland', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349352784, 'n', '2012', '10', '04', 0, 0, 20130111124905, 0, 0),
(178, 1, 16, 1, 0, NULL, '109.111.197.225', 'Lakeland', 'lakeland3', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1341231206, 'n', '2012', '07', '02', 0, 0, 20130604131128, 0, 0),
(179, 1, 16, 1, 0, NULL, '109.111.197.225', 'Thomson', 'thomson', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1341663238, 'n', '2012', '07', '07', 0, 0, 20131112163159, 0, 0),
(180, 1, 16, 1, 0, NULL, '109.111.197.225', 'Thomson', 'thomson1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350908027, 'n', '2012', '10', '22', 0, 0, 20131111133948, 0, 0),
(181, 1, 16, 1, 0, NULL, '109.111.197.225', 'Thomson Airways Beauty and Fragrance', 'thomson2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1326629594, 'n', '2012', '01', '15', 0, 0, 20131112163015, 0, 0),
(185, 1, 15, 1, 0, NULL, '109.111.197.225', 'Kids Holiday Pet Club Summer', 'kids-holiday-pet-club-summer', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346760814, 'n', '2012', '09', '04', 0, 0, 20131112160435, 0, 0),
(186, 1, 15, 1, 0, NULL, '109.111.197.225', 'Vero Gelato', 'vero-gelato', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345983209, 'n', '2012', '08', '26', 0, 0, 20131108164830, 0, 0),
(187, 1, 15, 1, 0, NULL, '109.111.197.225', 'Pound Bakery', 'pound-bakery', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1335010430, 'n', '2012', '04', '21', 0, 0, 20131112160751, 0, 0),
(189, 1, 15, 1, 0, NULL, '109.111.197.225', 'Sayers', 'sayers', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1316520816, 'n', '2011', '09', '20', 0, 0, 20131108164437, 0, 0),
(190, 1, 15, 1, 0, NULL, '109.111.197.225', 'Goodness Me', 'goodness-me', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339935202, 'n', '2012', '06', '17', 0, 0, 20131108165023, 0, 0),
(191, 1, 8, 1, 0, NULL, '109.111.197.225', 'Spain Shirt', 'spain-shirt', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345464784, 'n', '2012', '08', '20', 0, 0, 20130111124905, 0, 0),
(192, 1, 20, 1, 0, NULL, '109.111.197.225', 'Caterforce Website', 'caterforce-website', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346242384, 'n', '2012', '08', '29', 0, 0, 20130111124905, 0, 0),
(193, 1, 20, 1, 0, NULL, '109.111.197.225', 'Chefs Selections', 'chefs-selections', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349957626, 'n', '2012', '10', '11', 0, 0, 20131112171447, 0, 0),
(194, 1, 20, 1, 0, NULL, '109.111.197.225', 'Devilishly Delicious Facebook Creative', 'devilishly-delicious-facebook-creative', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353082357, 'n', '2012', '11', '16', 0, 0, 20131112171038, 0, 0),
(195, 1, 20, 1, 0, NULL, '109.111.197.225', 'Macphie of Glenbervie', 'macphie-of-glenbervie1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358242859, 'n', '2013', '01', '15', 0, 0, 20131112170500, 0, 0),
(196, 1, 20, 1, 0, NULL, '109.111.197.225', 'Marriages', 'marriages', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350389597, 'n', '2012', '10', '16', 0, 0, 20131112171418, 0, 0),
(197, 1, 20, 1, 0, NULL, '109.111.197.225', 'Meadow Vale Foods', 'meadowvale', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351787973, 'n', '2012', '11', '01', 0, 0, 20131112171334, 0, 0),
(199, 1, 20, 1, 0, NULL, '109.111.197.225', 'Sayers the Bakery', 'sayers-the-bakery', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1340971984, 'n', '2012', '06', '29', 0, 0, 20130111124905, 0, 0),
(200, 1, 20, 1, 0, NULL, '109.111.197.225', 'Scorpio Worldwide', 'scorpio-worldwide', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357836692, 'n', '2013', '01', '10', 0, 0, 20131112170733, 0, 0),
(202, 1, 1, 1, 0, NULL, '109.111.197.225', 'Motion', 'motion', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357909323, 'n', '2013', '01', '11', 0, 0, 20130111131604, 0, 0),
(203, 1, 22, 1, 0, NULL, '109.111.197.225', 'Jucee Idents', 'jucee-idents', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357910095, 'n', '2013', '01', '11', 0, 0, 20130111131455, 0, 0),
(204, 1, 22, 1, 0, NULL, '109.111.197.225', 'Jucee Bat and Ball', 'jucee-bat-and-ball', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357915358, 'n', '2013', '01', '11', 0, 0, 20130111144238, 0, 0),
(205, 1, 22, 1, 0, NULL, '109.111.197.225', 'Jucee Nets', 'jucee-nets', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357915992, 'n', '2013', '01', '11', 0, 0, 20130111145312, 0, 0),
(206, 1, 22, 1, 0, NULL, '109.111.197.225', 'Jucee Umbrella', 'jucee-umbrella', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357916352, 'n', '2013', '01', '11', 0, 0, 20130111145912, 0, 0),
(207, 1, 19, 1, 0, NULL, '109.111.197.225', 'Meadowvale', 'meadowvale', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1357920897, 'n', '2013', '01', '11', 0, 0, 20130114102558, 0, 0),
(208, 1, 19, 1, 0, NULL, '109.111.197.225', 'Fly Thomas Cook', 'fly-thomas-cook', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357834371, 'n', '2013', '01', '10', 0, 0, 20131112171952, 0, 0),
(209, 1, 19, 1, 0, NULL, '109.111.197.225', 'Pets at Home', 'pets-at-home', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357920438, 'n', '2013', '01', '11', 0, 0, 20131112171919, 0, 0),
(211, 1, 1, 1, 0, NULL, '109.111.197.225', 'Banners', 'banners', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357921680, 'n', '2013', '01', '11', 0, 0, 20130111163001, 0, 0),
(212, 1, 21, 1, 0, NULL, '109.111.197.225', 'Pets at Home', 'pets-at-home', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357921749, 'n', '2013', '01', '11', 0, 0, 20131108151210, 0, 0),
(213, 1, 19, 1, 0, NULL, '109.111.197.225', 'Ryman', 'ryman', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357814416, 'n', '2013', '01', '10', 0, 0, 20131112172017, 0, 0),
(214, 1, 19, 1, 0, NULL, '109.111.197.225', 'Pound Bakery', 'pound-bakery', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357727908, 'n', '2013', '01', '09', 0, 0, 20131112172129, 0, 0),
(215, 1, 21, 1, 0, NULL, '109.111.197.225', 'Mixed Banners', 'mixed-banners', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357814885, 'n', '2013', '01', '10', 0, 0, 20131108153006, 0, 0),
(216, 1, 4, 1, 0, NULL, '109.111.197.225', 'Facilities', 'facilities', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360677707, 'n', '2013', '02', '12', 0, 0, 20130212140147, 0, 0),
(217, 1, 1, 1, 0, NULL, '109.111.197.225', 'Facilities', 'facilities', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360678026, 'n', '2013', '02', '12', 0, 0, 20130212140807, 0, 0),
(218, 1, 6, 1, 0, NULL, '127.0.0.1', 'Our Thinking', 'our-thinking', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1360753874, 'n', '2013', '02', '13', 0, 0, 20130305171715, 0, 0),
(219, 1, 1, 1, 0, NULL, '192.168.90.119', 'Facilities', 'facilities1', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360754684, 'n', '2013', '02', '13', 0, 0, 20130307120145, 0, 0),
(220, 1, 23, 1, 0, NULL, '192.168.90.119', 'This is a test image', 'this-is-a-test-image', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360755583, 'n', '2013', '02', '13', 0, 0, 20130307114944, 0, 0),
(221, 1, 15, 1, 0, NULL, '109.111.197.225', 'Rob Winstanley Cycles', 'rob-winstanley-cycles', 'draft', 'y', 0, 0, 0, 0, 'y', 'n', 1370351113, 'n', '2013', '06', '04', 0, 0, 20131108142415, 0, 0),
(222, 1, 12, 1, 0, NULL, '109.111.197.225', 'Jucee Drinks', 'jucee-drinks', 'draft', 'y', 0, 0, 0, 0, 'y', 'n', 1370351233, 'n', '2013', '06', '04', 0, 0, 20131108142415, 0, 0),
(223, 1, 16, 1, 0, NULL, '109.111.197.225', 'Ryman Catalogue', 'ryman-catalogue', 'draft', 'y', 0, 0, 0, 0, 'y', 'n', 1370351307, 'n', '2013', '06', '04', 0, 0, 20131112162728, 0, 0),
(224, 1, 16, 1, 0, NULL, '109.111.197.225', 'Lakeland (German)', 'lakeland-german', 'draft', 'y', 0, 0, 0, 0, 'y', 'n', 1370351489, 'n', '2013', '06', '04', 0, 0, 20131108145030, 0, 0),
(225, 1, 16, 1, 0, NULL, '109.111.197.225', 'Lakeland', 'lakeland-outdoors', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1370351553, 'n', '2013', '06', '04', 0, 0, 20131112163434, 0, 0),
(226, 1, 14, 1, 0, NULL, '109.111.197.225', 'Thomson Airways Eatery Menu', 'thomson-airways-eatery-menu', 'draft', 'y', 0, 0, 0, 0, 'y', 'n', 1370351623, 'n', '2013', '06', '04', 0, 0, 20131108142244, 0, 0),
(227, 1, 14, 1, 0, NULL, '109.111.197.225', 'Thomson Airways Inflight Guide', 'thomson-airways-inflight-guide', 'draft', 'y', 0, 0, 0, 0, 'y', 'n', 1370351719, 'n', '2013', '06', '04', 0, 0, 20131108142220, 0, 0),
(228, 1, 19, 1, 0, NULL, '109.111.197.225', 'Jucee eCRM', 'jucee-internal', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1382966521, 'n', '2013', '10', '28', 0, 0, 20131112171702, 0, 0),
(229, 1, 19, 1, 0, NULL, '109.111.197.225', 'Ride-Away', 'ride-away', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1382966689, 'n', '2013', '10', '28', 0, 0, 20131112171750, 0, 0),
(230, 1, 1, 1, 0, NULL, '109.111.197.225', 'Mobile Apps', 'mobile-apps', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1382966930, 'n', '2013', '10', '28', 0, 0, 20131028132951, 0, 0),
(231, 1, 24, 1, 0, NULL, '109.111.197.225', 'Thomson Airways Crew', 'thomas-cook', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1382967014, 'n', '2013', '10', '28', 0, 0, 20131120120315, 0, 0),
(232, 1, 21, 1, 0, NULL, '109.111.197.225', 'Aunt Bessie''s Nisa Campaign', 'aunt-bessies-nisa-campaign', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1382967060, 'n', '2013', '10', '28', 0, 0, 20131108151001, 0, 0),
(233, 1, 21, 1, 0, NULL, '109.111.197.225', 'Ryman Escalator Panels', 'ryman-escalator-panels', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1382967167, 'n', '2013', '10', '28', 0, 0, 20131108150348, 0, 0),
(235, 1, 20, 1, 0, NULL, '109.111.197.225', 'Rob Winstanley Cycles', 'rob-winstanley-cycles', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1382967273, 'n', '2013', '10', '28', 0, 0, 20131112170934, 0, 0),
(236, 1, 10, 1, 0, NULL, '109.111.197.225', 'contact-thankyou', 'contact-thankyou', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1383039365, 'n', '2013', '10', '29', 0, 0, 20131029093605, 0, 0),
(237, 1, 1, 1, 0, NULL, '109.111.197.225', 'Thank you', 'get-in-touch-thankyou', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1383039479, 'n', '2013', '10', '29', 0, 0, 20131029094500, 0, 0),
(238, 1, 9, 1, 0, NULL, '109.111.197.225', 'Hape Toys', 'hape-toys', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384257721, 'n', '2013', '11', '12', 0, 0, 20131112120201, 0, 0),
(239, 1, 9, 1, 0, NULL, '109.111.197.225', 'Frankie & Bennys', 'frankie-bennys', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384257923, 'n', '2013', '11', '12', 0, 0, 20131112120523, 0, 0),
(240, 1, 9, 1, 0, NULL, '109.111.197.225', 'Vimto', 'vimto', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384258100, 'n', '2013', '11', '12', 0, 0, 20131112120820, 0, 0),
(241, 1, 9, 1, 0, NULL, '109.111.197.225', 'Europe', 'europe', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384258149, 'n', '2013', '11', '12', 0, 0, 20131112120909, 0, 0),
(242, 1, 9, 1, 0, NULL, '109.111.197.225', 'Chiquito', 'chiquito', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384258354, 'n', '2013', '11', '12', 0, 0, 20131112133935, 0, 0),
(243, 1, 9, 1, 0, NULL, '109.111.197.225', 'Try Thai', 'try-thai', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384258622, 'n', '2013', '11', '12', 0, 0, 20131112121702, 0, 0),
(244, 1, 9, 1, 0, NULL, '109.111.197.225', 'Around Manchester', 'around-manchester', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384259150, 'n', '2013', '11', '12', 0, 0, 20131112122550, 0, 0),
(245, 1, 9, 1, 0, NULL, '109.111.197.225', 'Other Locations', 'other-locations', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384259378, 'n', '2013', '11', '12', 0, 0, 20131112122938, 0, 0),
(246, 1, 9, 1, 0, NULL, '109.111.197.225', 'Hour Glass', 'hour-glass', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384259517, 'n', '2013', '11', '12', 0, 0, 20131112123157, 0, 0),
(247, 1, 8, 1, 0, NULL, '109.111.197.225', 'JD Williams', 'jd-williams', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384262414, 'n', '2013', '11', '12', 0, 0, 20131112132014, 0, 0),
(248, 1, 8, 1, 0, NULL, '109.111.197.225', 'Thomson Airways drinks', 'thomson-airways-drinks', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384262613, 'n', '2013', '11', '12', 0, 0, 20131112132333, 0, 0),
(249, 1, 8, 1, 0, NULL, '109.111.197.225', 'Pets at Home Dog Fashion', 'pets-at-home-dog-fashion', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384262872, 'n', '2013', '11', '12', 0, 0, 20131112143953, 0, 0),
(250, 1, 7, 1, 0, NULL, '109.111.197.225', 'Chicken', 'chicken', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384264972, 'n', '2013', '11', '12', 0, 0, 20131112140252, 0, 0),
(251, 1, 7, 1, 0, NULL, '109.111.197.225', 'Breakfast', 'breakfast', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384265481, 'n', '2013', '11', '12', 0, 0, 20131112141121, 0, 0),
(252, 1, 7, 1, 0, NULL, '109.111.197.225', 'Napolina', 'napolina', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384265661, 'n', '2013', '11', '12', 0, 0, 20131112141421, 0, 0),
(253, 1, 7, 1, 0, NULL, '109.111.197.225', 'The Restaurant Group', 'the-restaurant-group', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384265730, 'n', '2013', '11', '12', 0, 0, 20131112141530, 0, 0),
(254, 1, 7, 1, 0, NULL, '109.111.197.225', 'Wahaca Mexican Market Eating', 'wahaca-mexican-market-eating', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384265769, 'n', '2013', '11', '12', 0, 0, 20131112141609, 0, 0),
(255, 1, 7, 1, 0, NULL, '109.111.197.225', 'Test Produce - overhead', 'test-produce-overhead', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384265995, 'n', '2013', '11', '12', 0, 0, 20131112141955, 0, 0),
(256, 1, 7, 1, 0, NULL, '109.111.197.225', 'Lifestyle food', 'lifestyle-food', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384266285, 'n', '2013', '11', '12', 0, 0, 20131112142445, 0, 0),
(258, 1, 7, 1, 0, NULL, '109.111.197.225', 'Jaffa recipes', 'jaffa-recipes', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384266506, 'n', '2013', '11', '12', 0, 0, 20131112142826, 0, 0),
(259, 1, 7, 1, 0, NULL, '109.111.197.225', 'Test Food Cheese Orange', 'test-food-cheese-orange', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384266586, 'n', '2013', '11', '12', 0, 0, 20131112142946, 0, 0),
(260, 1, 7, 1, 0, NULL, '109.111.197.225', 'Beef test', 'beef-test', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384266656, 'n', '2013', '11', '12', 0, 0, 20131112143056, 0, 0),
(261, 1, 7, 1, 0, NULL, '109.111.197.225', 'Starbucks', 'starbucks', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384266739, 'n', '2013', '11', '12', 0, 0, 20131112143219, 0, 0),
(262, 1, 15, 1, 0, NULL, '109.111.197.225', 'Koko Noir', 'koko-noir', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384272660, 'n', '2013', '11', '12', 0, 0, 20131112162001, 0, 0),
(263, 1, 15, 1, 0, NULL, '109.111.197.225', 'Ride-away', 'ride-away', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384272822, 'n', '2013', '11', '12', 0, 0, 20131112161342, 0, 0),
(264, 1, 15, 1, 0, NULL, '109.111.197.225', 'Jucee', 'jucee', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384272979, 'n', '2013', '11', '12', 0, 0, 20131112161619, 0, 0),
(266, 1, 14, 1, 0, NULL, '109.111.197.225', 'Ryman the Stationers', 'ryman-the-stationers', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384274730, 'n', '2013', '11', '12', 0, 0, 20131112164530, 0, 0),
(267, 1, 14, 1, 0, NULL, '109.111.197.225', 'Rob Winstanley Cycles', 'rob-winstanley-cycles', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384275004, 'n', '2013', '11', '12', 0, 0, 20131112165305, 0, 0),
(268, 1, 14, 1, 0, NULL, '109.111.197.225', 'Aunt Bessie''s', 'aunt-bessies', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384275109, 'n', '2013', '11', '12', 0, 0, 20131112165149, 0, 0),
(269, 1, 20, 1, 0, NULL, '109.111.197.225', 'Ride-away', 'ride-away', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1384275199, 'n', '2013', '11', '12', 0, 0, 20131112165319, 0, 0),
(270, 1, 12, 1, 0, NULL, '109.111.197.225', 'Wainwright''s Cat', 'wainwrights-cat', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1385478030, 'n', '2013', '11', '26', 0, 0, 20131126150631, 0, 0),
(271, 1, 12, 1, 0, NULL, '109.111.197.225', 'Jucee Packaging', 'jucee-packaging', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1385479777, 'n', '2013', '11', '26', 0, 0, 20131126152937, 0, 0),
(272, 1, 12, 1, 0, NULL, '109.111.197.225', 'Wainwrights Grain Free', 'wainwrights-grain-free', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1385480042, 'n', '2013', '11', '26', 0, 0, 20131126153402, 0, 0),
(273, 1, 12, 1, 0, NULL, '109.111.197.225', 'Wainwright''s Puppy', 'wainwrights-puppy', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1385480169, 'n', '2013', '11', '26', 0, 0, 20131126153609, 0, 0),
(274, 1, 8, 1, 0, NULL, '109.111.197.225', 'Ride-Away Country Clothing', 'ride-away-country-clothing', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1386326201, 'n', '2013', '12', '06', 0, 0, 20131206172942, 0, 0),
(275, 1, 20, 1, 0, NULL, '109.111.197.225', 'Jucee Drinks', 'jucee-drinks', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1393347808, 'n', '2014', '02', '25', 0, 0, 20140225170629, 0, 0),
(276, 1, 24, 1, 0, NULL, '109.111.197.225', 'Thomas Cook In-Flight Retail App', 'thomas-cook-in-flight-retail-app', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1393348920, 'n', '2014', '02', '25', 0, 0, 20140225172200, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_comments`
--

CREATE TABLE IF NOT EXISTS `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_comment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_log`
--

CREATE TABLE IF NOT EXISTS `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `exp_cp_log`
--

INSERT INTO `exp_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`) VALUES
(1, 1, 1, 'Admin', '127.0.0.1', 1351164716, 'Logged in'),
(2, 1, 1, 'Admin', '127.0.0.1', 1351164989, 'Member Group Created:&nbsp;&nbsp;Editors'),
(3, 1, 1, 'Admin', '127.0.0.1', 1351165033, 'Member profile created:&nbsp;&nbsp;Editor'),
(4, 1, 1, 'Admin', '127.0.0.1', 1351165051, 'Field Group Created:&nbsp;Generic'),
(5, 1, 1, 'Admin', '127.0.0.1', 1351165073, 'Channel Created:&nbsp;&nbsp;Landing Page'),
(6, 1, 1, 'Admin', '127.0.0.1', 1351165091, 'Channel Created:&nbsp;&nbsp;Creative'),
(7, 1, 1, 'Admin', '127.0.0.1', 1351165100, 'Channel Created:&nbsp;&nbsp;Digital'),
(8, 1, 1, 'Admin', '127.0.0.1', 1351165111, 'Channel Created:&nbsp;&nbsp;Photography'),
(9, 1, 1, 'Admin', '127.0.0.1', 1351165184, 'Channel Created:&nbsp;&nbsp;The Studio'),
(10, 1, 1, 'Admin', '127.0.0.1', 1351243234, 'Channel Created:&nbsp;&nbsp;Homepage'),
(11, 1, 1, 'Admin', '127.0.0.1', 1351516758, 'Channel Created:&nbsp;&nbsp;food'),
(12, 1, 1, 'Admin', '127.0.0.1', 1351516769, 'Channel Created:&nbsp;&nbsp;Product'),
(13, 1, 1, 'Admin', '127.0.0.1', 1351516795, 'Channel Created:&nbsp;&nbsp;Location'),
(14, 1, 1, 'Admin', '127.0.0.1', 1351516806, 'Channel Created:&nbsp;&nbsp;Set'),
(15, 1, 1, 'Admin', '127.0.0.1', 1351521273, 'Channel Created:&nbsp;&nbsp;Testimonials'),
(16, 1, 1, 'Admin', '127.0.0.1', 1351527474, 'Channel Created:&nbsp;&nbsp;packaging'),
(17, 1, 1, 'Admin', '127.0.0.1', 1351527486, 'Channel Created:&nbsp;&nbsp;Advertising'),
(18, 1, 1, 'Admin', '127.0.0.1', 1351527502, 'Channel Created:&nbsp;&nbsp;Point of Sale'),
(19, 1, 1, 'Admin', '127.0.0.1', 1351527514, 'Channel Created:&nbsp;&nbsp;Branding'),
(20, 1, 1, 'Admin', '127.0.0.1', 1351527527, 'Channel Created:&nbsp;&nbsp;brochure'),
(21, 1, 1, 'Admin', '127.0.0.1', 1351531033, 'Channel Deleted:&nbsp;&nbsp;Advertising'),
(22, 1, 1, 'Admin', '127.0.0.1', 1352136247, 'Channel Created:&nbsp;&nbsp;studio'),
(23, 1, 1, 'Admin', '109.111.197.225', 1352453140, 'Channel Created:&nbsp;&nbsp;Mobile'),
(24, 1, 1, 'Admin', '109.111.197.225', 1352453153, 'Channel Created:&nbsp;&nbsp;eCRM'),
(25, 1, 1, 'Admin', '109.111.197.225', 1352453166, 'Channel Created:&nbsp;&nbsp;Websites'),
(26, 1, 1, 'Admin', '109.111.197.225', 1352453189, 'Channel Created:&nbsp;&nbsp;Strategy'),
(27, 1, 1, 'Admin', '127.0.0.1', 1353941306, 'Logged out'),
(28, 1, 1, 'Admin', '127.0.0.1', 1354707937, 'Logged out'),
(29, 1, 1, 'Admin', '109.111.197.225', 1354723890, 'Logged out'),
(30, 1, 1, 'Admin', '109.111.197.225', 1356344726, 'Logged out'),
(31, 1, 1, 'Admin', '109.111.197.225', 1357315976, 'Logged out'),
(32, 1, 1, 'Admin', '109.111.197.225', 1357643633, 'Logged out'),
(33, 1, 1, 'Admin', '109.111.197.225', 1357660139, 'Logged out'),
(34, 1, 1, 'Admin', '109.111.197.225', 1357743361, 'Logged out'),
(35, 1, 1, 'Admin', '109.111.197.225', 1357743674, 'Logged out'),
(36, 1, 1, 'Admin', '109.111.197.225', 1357743756, 'Logged out'),
(37, 1, 1, 'Admin', '109.111.197.225', 1357743789, 'Logged out'),
(38, 1, 1, 'Admin', '109.111.197.225', 1357743921, 'Logged out'),
(39, 1, 1, 'Admin', '109.111.197.225', 1357838293, 'Logged out'),
(40, 1, 1, 'Admin', '109.111.197.225', 1357909507, 'Channel Created:&nbsp;&nbsp;Motion'),
(41, 1, 1, 'Admin', '109.111.197.225', 1357914809, 'Field Group Created:&nbsp;Video'),
(42, 1, 1, 'Admin', '109.111.197.225', 1357918299, 'Logged out'),
(43, 1, 1, 'Admin', '109.111.197.225', 1357922705, 'Logged out'),
(44, 1, 1, 'Admin', '109.111.197.225', 1358173045, 'Logged out'),
(45, 1, 1, 'Admin', '109.111.197.225', 1358244522, 'Logged out'),
(46, 1, 1, 'Admin', '109.111.197.225', 1358959258, 'Logged out'),
(47, 1, 1, 'Admin', '109.111.197.225', 1360170814, 'Logged out'),
(48, 1, 1, 'Admin', '127.0.0.1', 1360754723, 'Channel Created:&nbsp;&nbsp;Facilities'),
(49, 1, 1, 'Admin', '109.111.197.225', 1382966802, 'Channel Created:&nbsp;&nbsp;Apps'),
(50, 1, 1, 'Admin', '109.111.197.225', 1383041517, 'Logged out'),
(51, 1, 1, 'Admin', '109.111.197.225', 1383932406, 'Logged out'),
(52, 1, 1, 'Admin', '109.111.197.225', 1385487373, 'Logged out'),
(53, 1, 1, 'Admin', '109.111.197.225', 1386350191, 'Logged out');

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_search_index`
--

CREATE TABLE IF NOT EXISTS `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_developer_log`
--

CREATE TABLE IF NOT EXISTS `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_mg`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_ml`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_console_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_ping_status`
--

CREATE TABLE IF NOT EXISTS `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_versioning`
--

CREATE TABLE IF NOT EXISTS `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts` (
  `ep_entry_drafts_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) NOT NULL,
  `author_id` int(10) NOT NULL,
  `channel_id` int(10) NOT NULL,
  `site_id` int(10) NOT NULL,
  `status` varchar(255) NOT NULL,
  `url_title` varchar(255) NOT NULL,
  `draft_data` text,
  `expiration_date` int(10) NOT NULL,
  `edit_date` int(10) NOT NULL,
  `entry_date` int(10) NOT NULL,
  PRIMARY KEY (`ep_entry_drafts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts_auth`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts_auth` (
  `ep_auth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `timestamp` int(10) NOT NULL,
  PRIMARY KEY (`ep_auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts_thirdparty`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts_thirdparty` (
  `entry_id` int(10) NOT NULL,
  `field_id` int(10) NOT NULL,
  `type` varchar(255) NOT NULL,
  `row_id` varchar(25) NOT NULL,
  `row_order` int(10) NOT NULL,
  `col_id` int(10) NOT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_roles`
--

CREATE TABLE IF NOT EXISTS `exp_ep_roles` (
  `site_id` int(10) NOT NULL,
  `role` varchar(255) NOT NULL,
  `states` text,
  PRIMARY KEY (`site_id`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_ep_roles`
--

INSERT INTO `exp_ep_roles` (`site_id`, `role`, `states`) VALUES
(1, 'editor', 'a:7:{s:9:"null|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"draft|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:14:"submitted|null";a:4:{s:5:"label";s:26:"bwf_status_label_submitted";s:8:"can_edit";b:0;s:11:"can_preview";b:0;s:7:"buttons";a:1:{i:0;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:9:"open|null";a:2:{s:5:"label";s:21:"bwf_status_label_live";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"open|draft";a:2:{s:5:"label";s:27:"bwf_status_label_draft_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}i:2;a:5:{i:0;N;i:1;s:10:"epBwfDraft";i:2;s:6:"delete";i:3;s:21:"bwf_btn_discard_draft";i:4;b:0;}}}s:14:"open|submitted";a:4:{s:5:"label";s:31:"bwf_status_label_submitted_live";s:8:"can_edit";b:0;s:11:"can_preview";b:0;s:7:"buttons";a:1:{i:0;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:11:"closed|null";a:2:{s:5:"label";s:25:"bwf_status_label_archived";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}}'),
(1, 'publisher', 'a:7:{s:9:"null|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"draft|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:14:"submitted|null";a:2:{s:5:"label";s:26:"bwf_status_label_submitted";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:9:"open|null";a:2:{s:5:"label";s:21:"bwf_status_label_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:6:"closed";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_archive";i:4;b:0;}i:1;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:2;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"open|draft";a:2:{s:5:"label";s:27:"bwf_status_label_draft_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfDraft";i:2;s:7:"replace";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}i:2;a:5:{i:0;N;i:1;s:10:"epBwfDraft";i:2;s:6:"delete";i:3;s:21:"bwf_btn_discard_draft";i:4;b:0;}}}s:14:"open|submitted";a:2:{s:5:"label";s:31:"bwf_status_label_submitted_live";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfDraft";i:2;s:7:"replace";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:11:"closed|null";a:2:{s:5:"label";s:23:"bwf_status_label_closed";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_settings`
--

CREATE TABLE IF NOT EXISTS `exp_ep_settings` (
  `site_id` int(10) NOT NULL,
  `class` varchar(255) NOT NULL,
  `settings` text,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_extensions`
--

CREATE TABLE IF NOT EXISTS `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `exp_extensions`
--

INSERT INTO `exp_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`) VALUES
(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y'),
(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y'),
(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y'),
(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y'),
(14, 'Assets_ext', 'channel_entries_query_result', 'channel_entries_query_result', '', 10, '1.2.2', 'y'),
(15, 'Field_editor_ext', 'cp_menu_array', 'cp_menu_array', 'a:2:{s:13:"global_prefix";s:0:"";s:14:"group_prefixes";a:1:{i:1;s:3:"cf_";}}', 10, '1.0.3', 'y'),
(16, 'Libraree_ext', 'process', 'sessions_start', 'a:4:{s:11:"license_key";s:36:"eb01f217-108c-4de0-915b-6c4a358b09d5";s:4:"path";s:56:"/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/templates/";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 1, '1.0.5', 'y'),
(17, 'Libraree_ext', 'process_check', 'cp_js_end', 'a:4:{s:11:"license_key";s:36:"eb01f217-108c-4de0-915b-6c4a358b09d5";s:4:"path";s:56:"/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/templates/";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 2, '1.0.5', 'y'),
(18, 'Libraree_ext', 'low_variables_delete', 'low_variables_delete', 'a:4:{s:11:"license_key";s:36:"eb01f217-108c-4de0-915b-6c4a358b09d5";s:4:"path";s:56:"/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/templates/";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 2, '1.0.5', 'y'),
(19, 'Low_reorder_ext', 'entry_submission_end', 'entry_submission_end', 'a:0:{}', 5, '2.0.4', 'y'),
(20, 'Low_reorder_ext', 'channel_entries_query_result', 'channel_entries_query_result', 'a:0:{}', 5, '2.0.4', 'y'),
(21, 'Low_variables_ext', 'sessions_end', 'sessions_end', 'a:7:{s:11:"license_key";s:36:"58007bbf-79cf-45b0-b225-5b3d344aef5b";s:10:"can_manage";a:2:{i:0;s:1:"6";i:1;s:1:"1";}s:16:"register_globals";s:1:"y";s:20:"register_member_data";s:1:"n";s:13:"save_as_files";s:1:"y";s:9:"file_path";s:78:"/Users/martinsmith/Desktop/www/system/expressionengine/templates/low_variables";s:13:"enabled_types";a:1:{i:0;s:12:"low_textarea";}}', 2, '2.3.2', 'y'),
(22, 'Low_variables_ext', 'template_fetch_template', 'template_fetch_template', 'a:7:{s:11:"license_key";s:36:"58007bbf-79cf-45b0-b225-5b3d344aef5b";s:10:"can_manage";a:2:{i:0;s:1:"6";i:1;s:1:"1";}s:16:"register_globals";s:1:"y";s:20:"register_member_data";s:1:"n";s:13:"save_as_files";s:1:"y";s:9:"file_path";s:78:"/Users/martinsmith/Desktop/www/system/expressionengine/templates/low_variables";s:13:"enabled_types";a:1:{i:0;s:12:"low_textarea";}}', 2, '2.3.2', 'y'),
(23, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y'),
(24, 'Updater_ext', 'cp_menu_array', 'cp_menu_array', 'a:0:{}', 100, '3.1.5', 'y'),
(25, 'Zoo_flexible_admin_ext', 'cp_css_end', 'cp_css_end', '', 1, '1.6', 'y'),
(26, 'Zoo_flexible_admin_ext', 'cp_js_end', 'cp_js_end', '', 1, '1.6', 'y'),
(27, 'Zoo_flexible_admin_ext', 'sessions_end', 'sessions_end', '', 1, '1.6', 'y'),
(28, 'Zoo_triggers_ext', 'hook_sessions_start', 'sessions_start', 'a:2:{s:8:"triggers";a:4:{s:3:"tag";a:1:{i:0;s:3:"tag";}s:8:"category";a:1:{i:0;s:8:"category";}s:7:"archive";a:1:{i:0;s:7:"archive";}s:6:"author";a:1:{i:0;s:6:"author";}}s:8:"settings";a:19:{s:16:"entries_operator";s:1:"|";s:31:"entries_title_categories_prefix";s:15:" in categories ";s:32:"entries_title_categories_postfix";s:0:"";s:34:"entries_title_categories_separator";s:2:", ";s:39:"entries_title_categories_separator_last";s:5:" and ";s:28:"entries_title_archives_first";s:5:"month";s:29:"entries_title_archives_prefix";s:4:" in ";s:30:"entries_title_archives_postfix";s:1:".";s:32:"entries_title_archives_separator";s:1:" ";s:27:"entries_title_archives_year";s:1:"Y";s:28:"entries_title_archives_month";s:1:"F";s:24:"entries_title_tag_prefix";s:9:" in tags ";s:25:"entries_title_tag_postfix";s:0:"";s:27:"entries_title_tag_separator";s:2:", ";s:32:"entries_title_tag_separator_last";s:5:" and ";s:27:"entries_title_author_prefix";s:12:" written by ";s:28:"entries_title_author_postfix";s:0:"";s:30:"entries_title_author_separator";s:2:", ";s:35:"entries_title_author_separator_last";s:5:" and ";}}', 9, '1.1.10', 'y'),
(29, 'Zoo_triggers_ext', 'hook_channel_module_create_pagination', 'channel_module_create_pagination', '', 10, '1.1.10', 'y'),
(30, 'Zoo_triggers_ext', 'hook_cp_css_end', 'cp_css_end', '', 1, '1.1.10', 'y'),
(31, 'Zoo_triggers_ext', 'hook_cp_js_end', 'cp_js_end', '', 1, '1.1.10', 'y'),
(32, 'Ep_better_workflow_ext', 'on_sessions_start', 'sessions_start', 'a:0:{}', 8, '1.4', 'y'),
(33, 'Ep_better_workflow_ext', 'on_entry_submission_start', 'entry_submission_start', 'a:0:{}', 10, '1.4', 'y'),
(34, 'Ep_better_workflow_ext', 'on_entry_submission_ready', 'entry_submission_ready', 'a:0:{}', 10, '1.4', 'y'),
(35, 'Ep_better_workflow_ext', 'on_entry_submission_end', 'entry_submission_end', 'a:0:{}', 10, '1.4', 'y'),
(36, 'Ep_better_workflow_ext', 'on_publish_form_entry_data', 'publish_form_entry_data', 'a:0:{}', 10, '1.4', 'y'),
(37, 'Ep_better_workflow_ext', 'on_publish_form_channel_preferences', 'publish_form_channel_preferences', 'a:0:{}', 10, '1.4', 'y'),
(38, 'Ep_better_workflow_ext', 'on_channel_entries_row', 'channel_entries_row', 'a:0:{}', 10, '1.4', 'y'),
(39, 'Ep_better_workflow_ext', 'on_channel_entries_query_result', 'channel_entries_query_result', 'a:0:{}', 8, '1.4', 'y'),
(40, 'Ep_better_workflow_ext', 'on_cp_js_end', 'cp_js_end', 'a:0:{}', 10, '1.4', 'y'),
(41, 'Ep_better_workflow_ext', 'on_template_post_parse', 'template_post_parse', 'a:0:{}', 100, '1.4', 'y'),
(42, 'Ep_better_workflow_ext', 'on_matrix_data_query', 'matrix_data_query', 'a:0:{}', 10, '1.4', 'y'),
(43, 'Ep_better_workflow_ext', 'on_playa_data_query', 'playa_data_query', 'a:0:{}', 10, '1.4', 'y'),
(44, 'Ep_better_workflow_ext', 'on_playa_fetch_rels_query', 'playa_fetch_rels_query', 'a:0:{}', 10, '1.4', 'y'),
(45, 'Ep_better_workflow_ext', 'on_zenbu_filter_by_status', 'zenbu_filter_by_status', 'a:0:{}', 100, '1.4', 'y'),
(46, 'Ep_better_workflow_ext', 'on_zenbu_modify_status_display', 'zenbu_modify_status_display', 'a:0:{}', 100, '1.4', 'y'),
(47, 'Ep_better_workflow_ext', 'on_zenbu_modify_title_display', 'zenbu_modify_title_display', 'a:0:{}', 100, '1.4', 'y'),
(48, 'Ce_img_aws_ext', 'pre_parse', 'ce_img_pre_parse', 'a:0:{}', 9, '1.0', 'y'),
(49, 'Ce_img_aws_ext', 'update_valid_params', 'ce_img_start', 'a:0:{}', 9, '1.0', 'y'),
(50, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.4.3', 'y'),
(51, 'Structure_ext', 'entry_submission_redirect', 'entry_submission_redirect', '', 10, '3.3.6', 'y'),
(52, 'Structure_ext', 'cp_member_login', 'cp_member_login', '', 10, '3.3.6', 'y'),
(53, 'Structure_ext', 'sessions_start', 'sessions_start', '', 10, '3.3.6', 'y'),
(54, 'Structure_ext', 'channel_module_create_pagination', 'channel_module_create_pagination', '', 9, '3.3.6', 'y'),
(55, 'Structure_ext', 'wygwam_config', 'wygwam_config', '', 10, '3.3.6', 'y'),
(56, 'Structure_ext', 'core_template_route', 'core_template_route', '', 10, '3.3.6', 'y'),
(57, 'Structure_ext', 'entry_submission_end', 'entry_submission_end', '', 10, '3.3.6', 'y'),
(58, 'Structure_ext', 'safecracker_submit_entry_end', 'safecracker_submit_entry_end', '', 10, '3.3.6', 'y'),
(59, 'Structure_ext', 'template_post_parse', 'template_post_parse', '', 10, '3.3.6', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `exp_fieldtypes`
--

INSERT INTO `exp_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`) VALUES
(1, 'select', '1.0', 'YTowOnt9', 'n'),
(2, 'text', '1.0', 'YTowOnt9', 'n'),
(3, 'textarea', '1.0', 'YTowOnt9', 'n'),
(4, 'date', '1.0', 'YTowOnt9', 'n'),
(5, 'file', '1.0', 'YTowOnt9', 'n'),
(6, 'multi_select', '1.0', 'YTowOnt9', 'n'),
(7, 'checkboxes', '1.0', 'YTowOnt9', 'n'),
(8, 'radio', '1.0', 'YTowOnt9', 'n'),
(9, 'rel', '1.0', 'YTowOnt9', 'n'),
(10, 'rte', '1.0', 'YTowOnt9', 'n'),
(12, 'assets', '1.2.2', 'YTowOnt9', 'y'),
(13, 'low_variables', '2.3.2', 'YTowOnt9', 'n'),
(14, 'playa', '4.3.3', 'YTowOnt9', 'y'),
(15, 'wygwam', '2.6.3', 'YToyOntzOjExOiJsaWNlbnNlX2tleSI7czowOiIiO3M6MTI6ImZpbGVfYnJvd3NlciI7czo2OiJhc3NldHMiO30=', 'y'),
(16, 'matrix', '2.4.3', 'YTowOnt9', 'y'),
(17, 'pt_checkboxes', '1.0.3', 'YTowOnt9', 'n'),
(18, 'pt_dropdown', '1.0.3', 'YTowOnt9', 'n'),
(19, 'pt_multiselect', '1.0.3', 'YTowOnt9', 'n'),
(20, 'pt_radio_buttons', '1.0.3', 'YTowOnt9', 'n'),
(21, 'structure', '3.3.6', 'YTowOnt9', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_formatting`
--

CREATE TABLE IF NOT EXISTS `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `exp_field_formatting`
--

INSERT INTO `exp_field_formatting` (`formatting_id`, `field_id`, `field_fmt`) VALUES
(1, 1, 'none'),
(2, 1, 'br'),
(3, 1, 'xhtml'),
(4, 2, 'none'),
(5, 2, 'br'),
(6, 2, 'xhtml'),
(7, 3, 'none'),
(8, 3, 'br'),
(9, 3, 'xhtml'),
(16, 6, 'none'),
(17, 6, 'br'),
(18, 6, 'xhtml'),
(19, 7, 'none'),
(20, 7, 'br'),
(21, 7, 'xhtml'),
(22, 8, 'none'),
(23, 8, 'br'),
(24, 8, 'xhtml'),
(25, 9, 'none'),
(26, 9, 'br'),
(27, 9, 'xhtml'),
(28, 10, 'none'),
(29, 10, 'br'),
(30, 10, 'xhtml'),
(31, 11, 'none'),
(32, 11, 'br'),
(33, 11, 'xhtml'),
(34, 12, 'none'),
(35, 12, 'br'),
(36, 12, 'xhtml'),
(37, 13, 'none'),
(38, 13, 'br'),
(39, 13, 'xhtml'),
(40, 14, 'none'),
(41, 14, 'br'),
(42, 14, 'xhtml');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_groups`
--

CREATE TABLE IF NOT EXISTS `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_field_groups`
--

INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Generic'),
(2, 1, 'Video');

-- --------------------------------------------------------

--
-- Table structure for table `exp_files`
--

CREATE TABLE IF NOT EXISTS `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=195 ;

--
-- Dumping data for table `exp_files`
--

INSERT INTO `exp_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `credit`, `location`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`) VALUES
(1, 1, '1.jpg', 7, '/Users/martinsmith/Desktop/www/images/rotating_image_panels/1.jpg', 'image/jpeg', '1.jpg', 57831, NULL, NULL, NULL, 1, 1351526635, 1, 1351526635, '466 464'),
(2, 1, '3.jpg', 7, '/Users/martinsmith/Desktop/www/images/rotating_image_panels/3.jpg', 'image/jpeg', '3.jpg', 77316, NULL, NULL, NULL, 1, 1351526635, 1, 1351526635, '717 596'),
(3, 1, '2.jpg', 7, '/Users/martinsmith/Desktop/www/images/rotating_image_panels/2.jpg', 'image/jpeg', '2.jpg', 76815, NULL, NULL, NULL, 1, 1351526635, 1, 1351526635, '722 604'),
(4, 1, 'stm-brand-portfolio-2012-3.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-3.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-3.jpg', 194112, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1096 1549'),
(5, 1, 'stm-brand-portfolio-2012-4.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-4.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-4.jpg', 150997, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1108 1549'),
(6, 1, 'stm-brand-portfolio-2012-6.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-6.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-6.jpg', 164988, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1108 1549'),
(7, 1, 'stm-brand-portfolio-2012-7.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-7.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-7.jpg', 251958, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1108 1549'),
(8, 1, 'stm-brand-portfolio-2012-8.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-8.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-8.jpg', 191932, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1108 1549'),
(9, 1, 'stm-brand-portfolio-2012-9.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-9.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-9.jpg', 189878, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1109 1549'),
(10, 1, 'stm-brand-portfolio-2012-10.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-10.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-10.jpg', 132467, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1108 1549'),
(11, 1, 'stm-brand-portfolio-2012-11.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-11.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-11.jpg', 167100, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1109 1549'),
(12, 1, 'stm-brand-portfolio-2012-12.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-12.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-12.jpg', 229840, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1109 1549'),
(13, 1, 'stm-brand-portfolio-2012-13.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-13.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-13.jpg', 132202, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1108 1549'),
(14, 1, 'stm-brand-portfolio-2012-14.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-14.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-14.jpg', 146279, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1109 1549'),
(15, 1, 'stm-brand-portfolio-2012-15.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-15.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-15.jpg', 201585, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1108 1549'),
(16, 1, 'stm-brand-portfolio-2012-16.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-16.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-16.jpg', 211481, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1109 1549'),
(17, 1, 'stm-brand-portfolio-2012-17.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-17.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-17.jpg', 156160, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1108 1549'),
(18, 1, 'stm-brand-portfolio-2012-18.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-18.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-18.jpg', 153833, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1106 1549'),
(19, 1, 'stm-brand-portfolio-2012-20.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-20.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-20.jpg', 224514, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1108 1549'),
(20, 1, 'stm-brand-portfolio-2012-19.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-19.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-19.jpg', 180734, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1101 1549'),
(21, 1, 'stm-brand-portfolio-2012-21.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-21.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-21.jpg', 263011, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1108 1549'),
(22, 1, 'stm-brand-portfolio-2012-22.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-22.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-22.jpg', 222677, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1108 1549'),
(23, 1, 'stm-brand-portfolio-2012-23.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-23.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-23.jpg', 188589, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1109 1549'),
(24, 1, 'stm-brand-portfolio-2012-24.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-24.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-24.jpg', 186677, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1108 1549'),
(25, 1, 'stm-brand-portfolio-2012-26.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-26.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-26.jpg', 233105, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1109 1549'),
(26, 1, 'stm-brand-portfolio-2012-25.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-25.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-25.jpg', 371597, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1281 1777'),
(27, 1, 'stm-brand-portfolio-2012-27.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-27.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-27.jpg', 275051, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1109 1548'),
(28, 1, 'stm-brand-portfolio-2012-28.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-28.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-28.jpg', 217450, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1108 1549'),
(29, 1, 'stm-brand-portfolio-2012-29.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-29.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-29.jpg', 216274, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1108 1549'),
(30, 1, 'stm-brand-portfolio-2012-30.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-30.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-30.jpg', 237888, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1104 1549'),
(31, 1, 'stm-brand-portfolio-2012-31.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-31.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-31.jpg', 220081, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1104 1549'),
(32, 1, 'stm-brand-portfolio-2012-32.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-32.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-32.jpg', 243955, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1109 1548'),
(33, 1, 'stm-brand-portfolio-2012-33.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-33.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-33.jpg', 208068, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1109 1548'),
(34, 1, 'stm-brand-portfolio-2012-34.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-34.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-34.jpg', 253880, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1109 1549'),
(35, 1, 'stm-brand-portfolio-2012-35.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-35.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-35.jpg', 194088, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1108 1549'),
(36, 1, 'stm-brand-portfolio-2012-36.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-36.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-36.jpg', 309436, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1105 1549'),
(37, 1, 'stm-brand-portfolio-2012-37.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-37.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-37.jpg', 267919, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1109 1549'),
(38, 1, 'stm-brand-portfolio-2012-38.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-38.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-38.jpg', 362128, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1108 1549'),
(39, 1, 'stm-brand-portfolio-2012-39.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-39.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-39.jpg', 375876, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1109 1549'),
(40, 1, 'stm-brand-portfolio-2012-40.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-40.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-40.jpg', 243773, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1108 1549'),
(41, 1, 'stm-brand-portfolio-2012-41.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-41.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-41.jpg', 278535, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1108 1549'),
(42, 1, 'stm-brand-portfolio-2012-42.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-42.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-42.jpg', 233821, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1109 1549'),
(43, 1, 'stm-brand-portfolio-2012-43.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-43.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-43.jpg', 311881, NULL, NULL, NULL, 1, 1351527786, 1, 1351527786, '1109 1549'),
(44, 1, 'stm-brand-portfolio-2012-44.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-44.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-44.jpg', 278643, NULL, NULL, NULL, 1, 1351527786, 1, 1351527786, '1109 1549'),
(45, 1, 'stm-brand-portfolio-2012-45.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-45.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-45.jpg', 169638, NULL, NULL, NULL, 1, 1351527786, 1, 1351527786, '1109 1549'),
(48, 1, 'Alpha---Special-Offers.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Alpha---Special-Offers.jpg', 'image/jpeg', 'Alpha---Special-Offers.jpg', 608989, NULL, NULL, NULL, 1, 1352454534, 1, 1353082484, '1416 727'),
(49, 1, 'devilsishly.jpg', 3, '/Users/martinsmith/Desktop/shoot-the-moon/XXXXXXXXX-website/www/images/digital/devilsishly.jpg', 'image/jpeg', 'devilsishly.jpg', 423516, NULL, NULL, NULL, 1, 1353082423, 1, 1353082484, '771 1059'),
(50, 1, 'Original_Squash_ÃÂ«_Jucee.jpeg', 4, '/Users/martinsmith/Desktop/shoot-the-moon/XXXXXXXXX-website/www/images/airside/Original_Squash_ÃÂ«_Jucee.jpeg', 'image/jpeg', 'Original_Squash_ÃÂ«_Jucee.jpeg', 916953, NULL, NULL, NULL, 1, 1352886843, 1, 1352886843, '1070 1030'),
(51, 1, 'Macphie_Development.jpeg', 3, '/Users/martinsmith/Desktop/shoot-the-moon/XXXXXXXXX-website/www/images/digital/Macphie_Development.jpeg', 'image/jpeg', 'Macphie_Development.jpeg', 914112, NULL, NULL, NULL, 1, 1353082981, 1, 1353082981, '1660 1006'),
(52, 1, 'hot-thick-red-curry.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/hot-thick-red-curry.jpg', 'image/jpeg', 'hot-thick-red-curry.jpg', 341257, NULL, NULL, NULL, 1, 1357307068, 1, 1357307068, '855 640'),
(54, 1, 'IMG_6086-RT.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/IMG_6086-RT.jpg', 'image/jpeg', 'IMG_6086-RT.jpg', 180778, NULL, NULL, NULL, 1, 1357307069, 1, 1357307069, '480 640'),
(55, 1, 'hoover.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/hoover.jpg', 'image/jpeg', 'hoover.jpg', 131046, NULL, NULL, NULL, 1, 1357307070, 1, 1357307070, '487 640'),
(56, 1, 'K3D3256.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/K3D3256.jpg', 'image/jpeg', 'K3D3256.jpg', 113382, NULL, NULL, NULL, 1, 1357307071, 1, 1357307071, '489 640'),
(57, 1, 'IMG_6189-RT.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/IMG_6189-RT.jpg', 'image/jpeg', 'IMG_6189-RT.jpg', 425731, NULL, NULL, NULL, 1, 1357307071, 1, 1357307071, '960 640'),
(58, 1, 'MG0936..jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/MG0936..jpg', 'image/jpeg', 'MG0936..jpg', 695778, NULL, NULL, NULL, 1, 1357307072, 1, 1357307072, '960 640'),
(59, 1, 'oysters.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/oysters.jpg', 'image/jpeg', 'oysters.jpg', 354291, NULL, NULL, NULL, 1, 1357307072, 1, 1357307072, '683 640'),
(60, 1, 'pannacotta.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/pannacotta.jpg', 'image/jpeg', 'pannacotta.jpg', 94151, NULL, NULL, NULL, 1, 1357307072, 1, 1357307072, '350 640'),
(61, 1, 'red-kettel.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/red-kettel.jpg', 'image/jpeg', 'red-kettel.jpg', 109775, NULL, NULL, NULL, 1, 1357307073, 1, 1357307073, '640 640'),
(62, 1, 'pea-soup.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/pea-soup.jpg', 'image/jpeg', 'pea-soup.jpg', 473889, NULL, NULL, NULL, 1, 1357307073, 1, 1357307073, '1060 640'),
(63, 1, 'orchid-banquet.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/orchid-banquet.jpg', 'image/jpeg', 'orchid-banquet.jpg', 345511, NULL, NULL, NULL, 1, 1357307073, 1, 1357307073, '431 640'),
(64, 1, 'reduced-fat-custard.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/reduced-fat-custard.jpg', 'image/jpeg', 'reduced-fat-custard.jpg', 132085, NULL, NULL, NULL, 1, 1357307073, 1, 1357307073, '587 640'),
(65, 1, 'runny-cheese.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/runny-cheese.jpg', 'image/jpeg', 'runny-cheese.jpg', 197027, NULL, NULL, NULL, 1, 1357307074, 1, 1357307074, '724 640'),
(66, 1, 'rice-wraps.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/rice-wraps.jpg', 'image/jpeg', 'rice-wraps.jpg', 258433, NULL, NULL, NULL, 1, 1357307074, 1, 1357307074, '640 640'),
(67, 1, 'shippams-RS.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/shippams-RS.jpg', 'image/jpeg', 'shippams-RS.jpg', 309843, NULL, NULL, NULL, 1, 1357307076, 1, 1357307076, '753 640'),
(68, 1, 'salad-red.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/salad-red.jpg', 'image/jpeg', 'salad-red.jpg', 670572, NULL, NULL, NULL, 1, 1357307076, 1, 1357307076, '956 640'),
(69, 1, 'Spicebomb-Page-NEW.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/Spicebomb-Page-NEW.jpg', 'image/jpeg', 'Spicebomb-Page-NEW.jpg', 131852, NULL, NULL, NULL, 1, 1357307077, 1, 1357307077, '427 640'),
(70, 1, 'salmon-sizzler.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/salmon-sizzler.jpg', 'image/jpeg', 'salmon-sizzler.jpg', 376602, NULL, NULL, NULL, 1, 1357307077, 1, 1357307077, '780 640'),
(71, 1, 'small-eggs.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/small-eggs.jpg', 'image/jpeg', 'small-eggs.jpg', 225266, NULL, NULL, NULL, 1, 1357307077, 1, 1357307077, '720 640'),
(72, 1, 'steak-and-pomigranate.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/steak-and-pomigranate.jpg', 'image/jpeg', 'steak-and-pomigranate.jpg', 203284, NULL, NULL, NULL, 1, 1357307077, 1, 1357307077, '640 640'),
(73, 1, 'white-aviator-watch.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/white-aviator-watch.jpg', 'image/jpeg', 'white-aviator-watch.jpg', 203779, NULL, NULL, NULL, 1, 1357307078, 1, 1357307078, '741 640'),
(74, 1, 'Trove-jar-shot.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/Trove-jar-shot.jpg', 'image/jpeg', 'Trove-jar-shot.jpg', 386066, NULL, NULL, NULL, 1, 1357307079, 1, 1357307079, '909 640'),
(75, 1, 'three-flavour-sauce-with-duck.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/three-flavour-sauce-with-duck.jpg', 'image/jpeg', 'three-flavour-sauce-with-duck.jpg', 331446, NULL, NULL, NULL, 1, 1357307079, 1, 1357307079, '638 640'),
(76, 1, 'Winter-2012-Ed-2-Front-Cover-1-v2.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/Winter-2012-Ed-2-Front-Cover-1-v2.jpg', 'image/jpeg', 'Winter-2012-Ed-2-Front-Cover-1-v2.jpg', 265413, NULL, NULL, NULL, 1, 1357307079, 1, 1357307079, '810 640'),
(77, 1, 'YSL-Shocking.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/YSL-Shocking.jpg', 'image/jpeg', 'YSL-Shocking.jpg', 351022, NULL, NULL, NULL, 1, 1357307079, 1, 1357307079, '816 640'),
(84, 1, 'personal--49.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/personal--49.jpg', 'image/jpeg', 'personal--49.jpg', 480465, NULL, NULL, NULL, 1, 1357312052, 1, 1357312052, '956 640'),
(85, 1, 'personal-copy.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/personal-copy.jpg', 'image/jpeg', 'personal-copy.jpg', 145922, NULL, NULL, NULL, 1, 1357312082, 1, 1357312082, '427 640'),
(88, 1, 'Screen_Shot_2013-01-04_at_14.59.20.png', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/Screen_Shot_2013-01-04_at_14.59.20.png', 'image/png', 'Screen_Shot_2013-01-04_at_14.59.20.png', 454181, NULL, NULL, NULL, 1, 1357312214, 1, 1357312214, '640 457'),
(89, 1, '_Almond-Extract.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/_Almond-Extract.jpg', 'image/jpeg', '_Almond-Extract.jpg', 210441, NULL, NULL, NULL, 1, 1357312398, 1, 1357312398, '594 640'),
(90, 1, 'hot-thick-red-curry_1.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/hot-thick-red-curry_1.jpg', 'image/jpeg', 'hot-thick-red-curry_1.jpg', 341257, NULL, NULL, NULL, 1, 1357312398, 1, 1357312398, '855 640'),
(91, 1, 'authentic-cocktail-on-the-beach-.jpeg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/authentic-cocktail-on-the-beach-.jpeg', 'image/jpeg', 'authentic-cocktail-on-the-beach-.jpeg', 126577, NULL, NULL, NULL, 1, 1357312398, 1, 1357312398, '636 640'),
(92, 1, 'IMG_6012-RT.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/IMG_6012-RT.jpg', 'image/jpeg', 'IMG_6012-RT.jpg', 595574, NULL, NULL, NULL, 1, 1357312399, 1, 1357312399, '960 640'),
(93, 1, 'active-procurement.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/active-procurement.jpg', 'image/jpeg', 'active-procurement.jpg', 73143, NULL, NULL, NULL, 1, 1357834834, 1, 1357834834, '594 640'),
(94, 1, 'Untitled-1.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Untitled-1.jpg', 'image/jpeg', 'Untitled-1.jpg', 93306, NULL, NULL, NULL, 1, 1357834982, 1, 1357834982, '594 640'),
(95, 1, 'caterforce.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/caterforce.jpg', 'image/jpeg', 'caterforce.jpg', 93306, NULL, NULL, NULL, 1, 1357835005, 1, 1357835005, '594 640'),
(96, 1, 'chefs-selections.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/chefs-selections.jpg', 'image/jpeg', 'chefs-selections.jpg', 68893, NULL, NULL, NULL, 1, 1357835130, 1, 1357835130, '410 640'),
(97, 1, 'devilish-home.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/devilish-home.jpg', 'image/jpeg', 'devilish-home.jpg', 49609, NULL, NULL, NULL, 1, 1357835267, 1, 1357835267, '382 640'),
(98, 1, 'devilish-facebook.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/devilish-facebook.jpg', 'image/jpeg', 'devilish-facebook.jpg', 73086, NULL, NULL, NULL, 1, 1357835430, 1, 1357835430, '382 640'),
(99, 1, 'macphie-homepage.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/macphie-homepage.jpg', 'image/jpeg', 'macphie-homepage.jpg', 113981, NULL, NULL, NULL, 1, 1357835718, 1, 1357835718, '594 640'),
(100, 1, 'marriages.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/marriages.jpg', 'image/jpeg', 'marriages.jpg', 85445, NULL, NULL, NULL, 1, 1357835867, 1, 1357835867, '594 640'),
(101, 1, 'meadowvale.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/meadowvale.jpg', 'image/jpeg', 'meadowvale.jpg', 120958, NULL, NULL, NULL, 1, 1357835988, 1, 1357835988, '594 640'),
(102, 1, 'jucee-page.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/jucee-page.jpg', 'image/jpeg', 'jucee-page.jpg', 117021, NULL, NULL, NULL, 1, 1357836291, 1, 1357836291, '594 640'),
(103, 1, 'sayers-screen.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/sayers-screen.jpg', 'image/jpeg', 'sayers-screen.jpg', 95218, NULL, NULL, NULL, 1, 1357836511, 1, 1357836511, '448 640'),
(104, 1, 'scorpio-screen.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/scorpio-screen.jpg', 'image/jpeg', 'scorpio-screen.jpg', 116624, NULL, NULL, NULL, 1, 1357836735, 1, 1357836735, '1000 640'),
(105, 1, 'Yearsley-Screen.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Yearsley-Screen.jpg', 'image/jpeg', 'Yearsley-Screen.jpg', 86051, NULL, NULL, NULL, 1, 1357836900, 1, 1357836900, '506 640'),
(106, 1, 'bat-and-ball.m4v', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/bat-and-ball.m4v', 'video/m4v', 'bat-and-ball.m4v', 1062429, NULL, NULL, NULL, 1, 1357915377, 1, 1357915377, ''),
(107, 1, 'bat-and-ball.webm', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/bat-and-ball.webm', 'video/webm', 'bat-and-ball.webm', 1103137, NULL, NULL, NULL, 1, 1357915392, 1, 1357915392, ''),
(108, 1, 'bat-and-ball.ogv', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/bat-and-ball.ogv', 'video/ogg', 'bat-and-ball.ogv', 1533453, NULL, NULL, NULL, 1, 1357915404, 1, 1357915404, ''),
(109, 1, 'bat-and-ball.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/bat-and-ball.jpg', 'image/jpeg', 'bat-and-ball.jpg', 32797, NULL, NULL, NULL, 1, 1357915414, 1, 1357915414, '360 640'),
(110, 1, 'nets.m4v', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/nets.m4v', 'video/m4v', 'nets.m4v', 1375891, NULL, NULL, NULL, 1, 1357916067, 1, 1357916067, ''),
(111, 1, 'nets.webm', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/nets.webm', 'video/webm', 'nets.webm', 1019725, NULL, NULL, NULL, 1, 1357916081, 1, 1357916081, ''),
(112, 1, 'nets.ogv', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/nets.ogv', 'video/ogg', 'nets.ogv', 1935004, NULL, NULL, NULL, 1, 1357916097, 1, 1357916097, ''),
(113, 1, 'nets.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/nets.jpg', 'image/jpeg', 'nets.jpg', 35855, NULL, NULL, NULL, 1, 1357916108, 1, 1357916108, '360 640'),
(114, 1, 'Jucee_Umbrella.m4v', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Jucee_Umbrella.m4v', 'video/m4v', 'Jucee_Umbrella.m4v', 1594112, NULL, NULL, NULL, 1, 1357916379, 1, 1357916379, ''),
(115, 1, 'Jucee_Umbrella.webm', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Jucee_Umbrella.webm', 'video/webm', 'Jucee_Umbrella.webm', 1018600, NULL, NULL, NULL, 1, 1357916388, 1, 1357916388, ''),
(116, 1, 'Jucee_Umbrella.ogv', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Jucee_Umbrella.ogv', 'video/ogg', 'Jucee_Umbrella.ogv', 2112926, NULL, NULL, NULL, 1, 1357916399, 1, 1357916399, ''),
(117, 1, 'Jucee_Umbrella.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Jucee_Umbrella.jpg', 'image/jpeg', 'Jucee_Umbrella.jpg', 32654, NULL, NULL, NULL, 1, 1357916409, 1, 1357916409, '360 640'),
(118, 1, 'two_meadowvale.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/two_meadowvale.jpg', 'image/jpeg', 'two_meadowvale.jpg', 99451, NULL, NULL, NULL, 1, 1357919243, 1, 1357919243, '571 640'),
(119, 1, 'tcx-combined.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/tcx-combined.jpg', 'image/jpeg', 'tcx-combined.jpg', 228420, NULL, NULL, NULL, 1, 1357920314, 1, 1357920314, '1402 640'),
(120, 1, 'pets.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/pets.jpg', 'image/jpeg', 'pets.jpg', 175553, NULL, NULL, NULL, 1, 1357920470, 1, 1357920470, '1226 640'),
(121, 1, 'jucee.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/jucee.jpg', 'image/jpeg', 'jucee.jpg', 242879, NULL, NULL, NULL, 1, 1357920705, 1, 1357920705, '1406 640'),
(122, 1, 'pah-banners.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/pah-banners.jpg', 'image/jpeg', 'pah-banners.jpg', 165735, NULL, NULL, NULL, 1, 1357921769, 1, 1357921769, '894 640'),
(123, 1, 'ryman.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/ryman.jpg', 'image/jpeg', 'ryman.jpg', 223510, NULL, NULL, NULL, 1, 1358159660, 1, 1358159660, '1472 640'),
(124, 1, 'pound-bakery.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/pound-bakery.jpg', 'image/jpeg', 'pound-bakery.jpg', 255415, NULL, NULL, NULL, 1, 1358159990, 1, 1358159990, '1500 640'),
(125, 1, 'mixed-banners.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/mixed-banners.jpg', 'image/jpeg', 'mixed-banners.jpg', 121485, NULL, NULL, NULL, 1, 1358160461, 1, 1358160461, '788 640'),
(126, 1, 'rob_winstanley.jpeg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/rob_winstanley.jpeg', 'image/jpeg', 'rob_winstanley.jpeg', 161427, NULL, NULL, NULL, 1, 1370351168, 1, 1370351168, '1091 640'),
(127, 1, 'Jucee.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Jucee.jpg', 'image/jpeg', 'Jucee.jpg', 229594, NULL, NULL, NULL, 1, 1370351270, 1, 1370351270, '418 640'),
(128, 1, 'Ryman.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Ryman.jpg', 'image/jpeg', 'Ryman.jpg', 474268, NULL, NULL, NULL, 1, 1370351318, 1, 1370351318, '1157 640'),
(129, 1, 'Lakeland_German.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Lakeland_German.jpg', 'image/jpeg', 'Lakeland_German.jpg', 483917, NULL, NULL, NULL, 1, 1370351510, 1, 1370351510, '1197 640'),
(130, 1, 'Lakeland.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Lakeland.jpg', 'image/jpeg', 'Lakeland.jpg', 365562, NULL, NULL, NULL, 1, 1370351590, 1, 1370351590, '880 640'),
(131, 1, 'Eatery_Menu_2.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Eatery_Menu_2.jpg', 'image/jpeg', 'Eatery_Menu_2.jpg', 296730, NULL, NULL, NULL, 1, 1370351716, 1, 1370351716, '615 640'),
(132, 1, 'Inflight_Guide_2.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Inflight_Guide_2.jpg', 'image/jpeg', 'Inflight_Guide_2.jpg', 316211, NULL, NULL, NULL, 1, 1370351819, 1, 1370351819, '615 640'),
(133, 1, 'pah-banners-updated.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/pah-banners-updated.jpg', 'image/jpeg', 'pah-banners-updated.jpg', 446579, NULL, NULL, NULL, 1, 1382966429, 1, 1382966429, '1400 640'),
(134, 1, 'pah-ecrm.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/pah-ecrm.jpg', 'image/jpeg', 'pah-ecrm.jpg', 658562, NULL, NULL, NULL, 1, 1382966508, 1, 1382966508, '1888 640'),
(135, 1, 'jucee-ecrm.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/jucee-ecrm.jpg', 'image/jpeg', 'jucee-ecrm.jpg', 556712, NULL, NULL, NULL, 1, 1382966611, 1, 1382966611, '1052 640'),
(136, 1, 'ride-away-ecrm.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/ride-away-ecrm.jpg', 'image/jpeg', 'ride-away-ecrm.jpg', 917410, NULL, NULL, NULL, 1, 1382966686, 1, 1382966686, '2034 640'),
(137, 1, 'tcx-apps.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/tcx-apps.jpg', 'image/jpeg', 'tcx-apps.jpg', 226784, NULL, NULL, NULL, 1, 1382967035, 1, 1382967035, '572 640'),
(138, 1, 'aunt-bessies-campaign.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/aunt-bessies-campaign.jpg', 'image/jpeg', 'aunt-bessies-campaign.jpg', 498098, NULL, NULL, NULL, 1, 1382967126, 1, 1382967126, '894 640'),
(139, 1, 'ryman-escalator-creative.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/ryman-escalator-creative.jpg', 'image/jpeg', 'ryman-escalator-creative.jpg', 216859, NULL, NULL, NULL, 1, 1382967183, 1, 1382967183, '555 640'),
(140, 1, 'yearsley.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/yearsley.jpg', 'image/jpeg', 'yearsley.jpg', 158987, NULL, NULL, NULL, 1, 1382967230, 1, 1382967230, '470 640'),
(141, 1, 'rob-winstanley.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/rob-winstanley.jpg', 'image/jpeg', 'rob-winstanley.jpg', 267183, NULL, NULL, NULL, 1, 1382967279, 1, 1382967279, '641 640'),
(142, 1, 'josefmeiers.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/josefmeiers.jpg', 'image/jpeg', 'josefmeiers.jpg', 544125, NULL, NULL, NULL, 1, 1384182276, 1, 1384182276, '2253 640'),
(143, 1, 'frankie-and-benny-leeds.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/frankie-and-benny-leeds.jpg', 'image/jpeg', 'frankie-and-benny-leeds.jpg', 668182, NULL, NULL, NULL, 1, 1384257970, 1, 1384257970, '4160 640'),
(144, 1, 'vimto-pr.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/vimto-pr.jpg', 'image/jpeg', 'vimto-pr.jpg', 353358, NULL, NULL, NULL, 1, 1384258123, 1, 1384258123, '915 640'),
(145, 1, 'Photography_location_europe.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Photography_location_europe.jpg', 'image/jpeg', 'Photography_location_europe.jpg', 513971, NULL, NULL, NULL, 1, 1384258219, 1, 1384258219, '2835 640'),
(146, 1, 'Chiquito.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Chiquito.jpg', 'image/jpeg', 'Chiquito.jpg', 333141, NULL, NULL, NULL, 1, 1384258349, 1, 1384258349, '2314 640'),
(147, 1, 'TryThai.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/TryThai.jpg', 'image/jpeg', 'TryThai.jpg', 502458, NULL, NULL, NULL, 1, 1384258881, 1, 1384258881, '1929 640'),
(148, 1, 'Photography_location_manchester.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Photography_location_manchester.jpg', 'image/jpeg', 'Photography_location_manchester.jpg', 398081, NULL, NULL, NULL, 1, 1384259166, 1, 1384259166, '2271 640'),
(149, 1, 'Photography_location_1137a.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Photography_location_1137a.jpg', 'image/jpeg', 'Photography_location_1137a.jpg', 130575, NULL, NULL, NULL, 1, 1384259371, 1, 1384259371, '1008 640'),
(150, 1, 'Hour-Glass.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Hour-Glass.jpg', 'image/jpeg', 'Hour-Glass.jpg', 225370, NULL, NULL, NULL, 1, 1384259538, 1, 1384259538, '972 640'),
(151, 1, 'Thomson-Airways-beauty.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Thomson-Airways-beauty.jpg', 'image/jpeg', 'Thomson-Airways-beauty.jpg', 834504, NULL, NULL, NULL, 1, 1384259969, 1, 1384259969, '4111 640'),
(152, 1, 'Thomson-Airways-gifts.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Thomson-Airways-gifts.jpg', 'image/jpeg', 'Thomson-Airways-gifts.jpg', 143711, NULL, NULL, NULL, 1, 1384261598, 1, 1384261598, '983 640'),
(153, 1, 'JDWilliams.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/JDWilliams.jpg', 'image/jpeg', 'JDWilliams.jpg', 718198, NULL, NULL, NULL, 1, 1384262506, 1, 1384262506, '4459 640'),
(154, 1, 'Thomson_Photography_product_spirits.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Thomson_Photography_product_spirits.jpg', 'image/jpeg', 'Thomson_Photography_product_spirits.jpg', 114345, NULL, NULL, NULL, 1, 1384262637, 1, 1384262637, '855 640'),
(155, 1, 'Photography_product_pets_home_fashion.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Photography_product_pets_home_fashion.jpg', 'image/jpeg', 'Photography_product_pets_home_fashion.jpg', 173112, NULL, NULL, NULL, 1, 1384262856, 1, 1384262856, '1649 640'),
(156, 1, 'east.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/east.jpg', 'image/jpeg', 'east.jpg', 384814, NULL, NULL, NULL, 1, 1384263656, 1, 1384263656, '2100 640'),
(157, 1, 'food-pr.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/food-pr.jpg', 'image/jpeg', 'food-pr.jpg', 485272, NULL, NULL, NULL, 1, 1384263810, 1, 1384263810, '3799 640'),
(158, 1, 'food-pack-fronts.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/food-pack-fronts.jpg', 'image/jpeg', 'food-pack-fronts.jpg', 223208, NULL, NULL, NULL, 1, 1384263975, 1, 1384263975, '2005 640'),
(159, 1, 'dessert.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/dessert.jpg', 'image/jpeg', 'dessert.jpg', 271431, NULL, NULL, NULL, 1, 1384264074, 1, 1384264074, '2865 640'),
(160, 1, 'Trove-jar-shot.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Trove-jar-shot.jpg', 'image/jpeg', 'Trove-jar-shot.jpg', 104889, NULL, NULL, NULL, 1, 1384264240, 1, 1384264240, '909 640'),
(161, 1, 'food_for_packaging.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/food_for_packaging.jpg', 'image/jpeg', 'food_for_packaging.jpg', 350224, NULL, NULL, NULL, 1, 1384264491, 1, 1384264491, '2688 640'),
(162, 1, 'test_custard.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/test_custard.jpg', 'image/jpeg', 'test_custard.jpg', 102449, NULL, NULL, NULL, 1, 1384264626, 1, 1384264626, '726 640'),
(163, 1, 'chicken.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/chicken.jpg', 'image/jpeg', 'chicken.jpg', 764741, NULL, NULL, NULL, 1, 1384264957, 1, 1384264957, '1633 640'),
(164, 1, 'breakfast.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/breakfast.jpg', 'image/jpeg', 'breakfast.jpg', 143484, NULL, NULL, NULL, 1, 1384265493, 1, 1384265493, '1257 640'),
(165, 1, 'Napolina-pasta-recipe.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Napolina-pasta-recipe.jpg', 'image/jpeg', 'Napolina-pasta-recipe.jpg', 71004, NULL, NULL, NULL, 1, 1384265688, 1, 1384265688, '464 640'),
(166, 1, 'the_restaurant_group.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/the_restaurant_group.jpg', 'image/jpeg', 'the_restaurant_group.jpg', 487795, NULL, NULL, NULL, 1, 1384265747, 1, 1384265747, '4248 640'),
(167, 1, 'wahaca.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/wahaca.jpg', 'image/jpeg', 'wahaca.jpg', 113193, NULL, NULL, NULL, 1, 1384265848, 1, 1384265848, '829 640'),
(168, 1, 'test_produce.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/test_produce.jpg', 'image/jpeg', 'test_produce.jpg', 547937, NULL, NULL, NULL, 1, 1384266008, 1, 1384266008, '2888 640'),
(169, 1, 'lifestyle-food.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/lifestyle-food.jpg', 'image/jpeg', 'lifestyle-food.jpg', 360881, NULL, NULL, NULL, 1, 1384266276, 1, 1384266276, '2729 640'),
(170, 1, 'test_food-cheese-orange.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/test_food-cheese-orange.jpg', 'image/jpeg', 'test_food-cheese-orange.jpg', 123441, NULL, NULL, NULL, 1, 1384266475, 1, 1384266475, '1444 640'),
(171, 1, 'jaffa_recipes.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/jaffa_recipes.jpg', 'image/jpeg', 'jaffa_recipes.jpg', 154635, NULL, NULL, NULL, 1, 1384266517, 1, 1384266517, '1783 640'),
(172, 1, 'test_food-cheese-orange_1.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/test_food-cheese-orange_1.jpg', 'image/jpeg', 'test_food-cheese-orange_1.jpg', 123441, NULL, NULL, NULL, 1, 1384266625, 1, 1384266625, '1444 640'),
(173, 1, 'test_beef.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/test_beef.jpg', 'image/jpeg', 'test_beef.jpg', 216508, NULL, NULL, NULL, 1, 1384266671, 1, 1384266671, '1320 640'),
(174, 1, 'starbucks.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/starbucks.jpg', 'image/jpeg', 'starbucks.jpg', 108594, NULL, NULL, NULL, 1, 1384266756, 1, 1384266756, '1094 640'),
(175, 1, 'authentic-cocktail-comp.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/authentic-cocktail-comp.jpg', 'image/jpeg', 'authentic-cocktail-comp.jpg', 52874, NULL, NULL, NULL, 1, 1384267062, 1, 1384267062, '236 640'),
(176, 1, 'KHPC.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/KHPC.jpg', 'image/jpeg', 'KHPC.jpg', 590869, NULL, NULL, NULL, 1, 1384272125, 1, 1384272125, '1754 640'),
(177, 1, 'kokonoir-flat.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/kokonoir-flat.jpg', 'image/jpeg', 'kokonoir-flat.jpg', 305913, NULL, NULL, NULL, 1, 1384272719, 1, 1384272719, '1820 640'),
(178, 1, 'rideaway.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/rideaway.jpg', 'image/jpeg', 'rideaway.jpg', 760455, NULL, NULL, NULL, 1, 1384272812, 1, 1384272812, '1366 640'),
(179, 1, 'Jucee001.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Jucee001.jpg', 'image/jpeg', 'Jucee001.jpg', 334351, NULL, NULL, NULL, 1, 1384273032, 1, 1384273032, '501 640'),
(180, 1, 'Lakeland2013.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Lakeland2013.jpg', 'image/jpeg', 'Lakeland2013.jpg', 177056, NULL, NULL, NULL, 1, 1384273309, 1, 1384273309, '478 640'),
(181, 1, 'Advanced_Nutrition.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Advanced_Nutrition.jpg', 'image/jpeg', 'Advanced_Nutrition.jpg', 273699, NULL, NULL, NULL, 1, 1384274120, 1, 1384274120, '1152 640'),
(182, 1, 'Aunt-Bessies-blk.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Aunt-Bessies-blk.jpg', 'image/jpeg', 'Aunt-Bessies-blk.jpg', 164774, NULL, NULL, NULL, 1, 1384274613, 1, 1384274613, '945 640'),
(183, 1, 'Ryman_1.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Ryman_1.jpg', 'image/jpeg', 'Ryman_1.jpg', 643845, NULL, NULL, NULL, 1, 1384274769, 1, 1384274769, '1701 640'),
(184, 1, 'RobWinstanleyBrandPod.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/RobWinstanleyBrandPod.jpg', 'image/jpeg', 'RobWinstanleyBrandPod.jpg', 218720, NULL, NULL, NULL, 1, 1384275068, 1, 1384275068, '860 640'),
(185, 1, 'ride-away1.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/ride-away1.jpg', 'image/jpeg', 'ride-away1.jpg', 492780, NULL, NULL, NULL, 1, 1384275705, 1, 1384275705, '2277 1009'),
(186, 1, 'Thomson-Crew-Comms.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Thomson-Crew-Comms.jpg', 'image/jpeg', 'Thomson-Crew-Comms.jpg', 88290, NULL, NULL, NULL, 1, 1384946404, 1, 1384946404, '418 640'),
(187, 1, 'Thomson-Crew-Comms2.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Thomson-Crew-Comms2.jpg', 'image/jpeg', 'Thomson-Crew-Comms2.jpg', 75083, NULL, NULL, NULL, 1, 1384946404, 1, 1384946404, '418 640'),
(188, 1, 'bn_wainrights_catsFull.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/bn_wainrights_catsFull.jpg', 'image/jpeg', 'bn_wainrights_catsFull.jpg', 227563, NULL, NULL, NULL, 1, 1385478358, 1, 1385478358, '510 640'),
(189, 1, 'Wainwrights_Puppy_blk.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/Wainwrights_Puppy_blk.jpg', 'image/jpeg', 'Wainwrights_Puppy_blk.jpg', 492109, NULL, NULL, NULL, 1, 1385480215, 1, 1385480215, '2177 2480'),
(190, 1, 'team07.jpg', 7, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/rotating_image_panels/team07.jpg', 'image/jpeg', 'team07.jpg', 119183, NULL, NULL, NULL, 1, 1385556463, 1, 1385556463, '320 470'),
(191, 1, 'rideaway-01B.jpg', 4, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/rideaway-01B.jpg', 'image/jpeg', 'rideaway-01B.jpg', 311824, NULL, NULL, NULL, 1, 1386350883, 1, 1386350883, '1701 640'),
(192, 1, 'jucee-website.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/jucee-website.jpg', 'image/jpeg', 'jucee-website.jpg', 127356, NULL, NULL, NULL, 1, 1393347885, 1, 1393347885, '507 640'),
(193, 1, 'TCX-iPad-04-pages-nav-drop-v2.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/TCX-iPad-04-pages-nav-drop-v2.jpg', 'image/jpeg', 'TCX-iPad-04-pages-nav-drop-v2.jpg', 460344, NULL, NULL, NULL, 1, 1393348990, 1, 1393348990, '1270 1344'),
(194, 1, 'alpha-apps.jpg', 3, '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/alpha-apps.jpg', 'image/jpeg', 'alpha-apps.jpg', 101859, NULL, NULL, NULL, 1, 1393349013, 1, 1393349013, '466 640');

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_categories`
--

CREATE TABLE IF NOT EXISTS `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_dimensions`
--

CREATE TABLE IF NOT EXISTS `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_watermarks`
--

CREATE TABLE IF NOT EXISTS `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_composer_layouts`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_composer_layouts` (
  `composer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composer_data` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `preview` char(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`composer_id`),
  KEY `preview` (`preview`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_composer_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_composer_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `template_name` varchar(150) NOT NULL DEFAULT 'default',
  `template_label` varchar(150) NOT NULL DEFAULT 'default',
  `template_description` text,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_data` text,
  `param_data` text,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fields`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `field_name` varchar(150) NOT NULL DEFAULT 'default',
  `field_label` varchar(150) NOT NULL DEFAULT 'default',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `settings` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'n',
  `submissions_page` char(1) NOT NULL DEFAULT 'y',
  `moderation_page` char(1) NOT NULL DEFAULT 'y',
  `composer_use` char(1) NOT NULL DEFAULT 'y',
  `field_description` text,
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_freeform_fields`
--

INSERT INTO `exp_freeform_fields` (`field_id`, `site_id`, `field_name`, `field_label`, `field_type`, `settings`, `author_id`, `entry_date`, `edit_date`, `required`, `submissions_page`, `moderation_page`, `composer_use`, `field_description`) VALUES
(1, 1, 'first_name', 'First Name', 'text', '{"field_length":150,"field_content_type":"any"}', 1, 1352298407, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s first name.'),
(2, 1, 'last_name', 'Last Name', 'text', '{"field_length":150,"field_content_type":"any"}', 1, 1352298407, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s last name.'),
(3, 1, 'email', 'Email', 'text', '{"field_length":150,"field_content_type":"email"}', 1, 1352298407, 0, 'n', 'y', 'y', 'y', 'A basic email field for collecting stuff like an email address.'),
(4, 1, 'user_message', 'Message', 'textarea', '{"field_ta_rows":6}', 1, 1352298407, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s message.');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fieldtypes` (
  `fieldtype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldtype_name` varchar(250) DEFAULT NULL,
  `settings` text,
  `default_field` char(1) NOT NULL DEFAULT 'n',
  `version` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`fieldtype_id`),
  KEY `fieldtype_name` (`fieldtype_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_freeform_fieldtypes`
--

INSERT INTO `exp_freeform_fieldtypes` (`fieldtype_id`, `fieldtype_name`, `settings`, `default_field`, `version`) VALUES
(1, 'file_upload', '[]', 'n', '4.0.7'),
(2, 'mailinglist', '[]', 'n', '4.0.7'),
(3, 'text', '[]', 'n', '4.0.7'),
(4, 'textarea', '[]', 'n', '4.0.7');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_file_uploads`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_file_uploads` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `server_path` varchar(750) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `filesize` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `extension` (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_forms`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_forms` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_name` varchar(150) NOT NULL DEFAULT 'default',
  `form_label` varchar(150) NOT NULL DEFAULT 'default',
  `default_status` varchar(150) NOT NULL DEFAULT 'default',
  `notify_user` char(1) NOT NULL DEFAULT 'n',
  `notify_admin` char(1) NOT NULL DEFAULT 'n',
  `user_email_field` varchar(150) NOT NULL DEFAULT '',
  `user_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_email` text,
  `form_description` text,
  `field_ids` text,
  `field_order` text,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `composer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`form_id`),
  KEY `form_name` (`form_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_freeform_forms`
--

INSERT INTO `exp_freeform_forms` (`form_id`, `site_id`, `form_name`, `form_label`, `default_status`, `notify_user`, `notify_admin`, `user_email_field`, `user_notification_id`, `admin_notification_id`, `admin_notification_email`, `form_description`, `field_ids`, `field_order`, `template_id`, `composer_id`, `author_id`, `entry_date`, `edit_date`, `settings`) VALUES
(2, 1, 'enquiries', 'Enquiries', 'pending', 'n', 'n', '', 0, 0, 'martin@shoot-the-moon.co.uk', '', '', '', 0, 0, 1, 1352298607, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_form_entries_2`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_form_entries_2` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=637 ;

--
-- Dumping data for table `exp_freeform_form_entries_2`
--

INSERT INTO `exp_freeform_form_entries_2` (`entry_id`, `site_id`, `author_id`, `complete`, `ip_address`, `entry_date`, `edit_date`, `status`) VALUES
(1, 1, 1, 'y', '127.0.0.1', 1352298861, 0, 'pending'),
(2, 1, 1, 'y', '127.0.0.1', 1352298940, 0, 'pending'),
(3, 1, 1, 'y', '127.0.0.1', 1352299176, 0, 'pending'),
(4, 1, 1, 'y', '127.0.0.1', 1352299214, 0, 'pending'),
(5, 1, 1, 'y', '127.0.0.1', 1352307421, 0, 'pending'),
(6, 1, 1, 'y', '127.0.0.1', 1352307422, 0, 'pending'),
(7, 1, 1, 'y', '127.0.0.1', 1352307653, 0, 'pending'),
(8, 1, 1, 'y', '127.0.0.1', 1352307654, 0, 'pending'),
(9, 1, 0, 'y', '91.232.96.36', 1358583010, 0, 'pending'),
(10, 1, 0, 'y', '94.102.52.178', 1358883927, 0, 'pending'),
(11, 1, 0, 'y', '92.42.148.196', 1358939720, 0, 'pending'),
(12, 1, 0, 'y', '94.102.52.176', 1359088667, 0, 'pending'),
(13, 1, 0, 'y', '188.143.232.211', 1359402489, 0, 'pending'),
(14, 1, 0, 'y', '89.248.165.134', 1359949420, 0, 'pending'),
(15, 1, 0, 'y', '87.83.17.18', 1360081370, 0, 'pending'),
(16, 1, 0, 'y', '89.248.165.134', 1360114668, 0, 'pending'),
(17, 1, 0, 'y', '78.149.117.249', 1360249007, 0, 'pending'),
(18, 1, 0, 'y', '89.248.165.134', 1360340476, 0, 'pending'),
(19, 1, 0, 'y', '188.143.232.211', 1363070696, 0, 'pending'),
(20, 1, 0, 'y', '188.143.232.31', 1363124385, 0, 'pending'),
(21, 1, 0, 'y', '188.143.232.31', 1363367287, 0, 'pending'),
(22, 1, 0, 'y', '188.143.232.31', 1363629132, 0, 'pending'),
(23, 1, 0, 'y', '188.143.232.31', 1363860733, 0, 'pending'),
(24, 1, 0, 'y', '94.242.237.73', 1363866216, 0, 'pending'),
(25, 1, 0, 'y', '94.242.237.73', 1363967836, 0, 'pending'),
(26, 1, 0, 'y', '188.143.232.31', 1364077347, 0, 'pending'),
(27, 1, 0, 'y', '188.143.232.31', 1364274334, 0, 'pending'),
(28, 1, 0, 'y', '188.143.232.31', 1364507879, 0, 'pending'),
(29, 1, 0, 'y', '188.143.232.31', 1364715495, 0, 'pending'),
(30, 1, 0, 'y', '2.98.136.242', 1364832857, 0, 'pending'),
(31, 1, 0, 'y', '188.143.232.31', 1364907435, 0, 'pending'),
(32, 1, 0, 'y', '37.221.174.111', 1365007799, 0, 'pending'),
(33, 1, 0, 'y', '188.143.232.31', 1365118755, 0, 'pending'),
(34, 1, 0, 'y', '188.143.232.31', 1365250066, 0, 'pending'),
(35, 1, 0, 'y', '78.148.226.222', 1365419817, 0, 'pending'),
(36, 1, 0, 'y', '91.232.96.20', 1365538897, 0, 'pending'),
(37, 1, 0, 'y', '188.143.232.31', 1365560999, 0, 'pending'),
(38, 1, 0, 'y', '188.143.232.31', 1365902730, 0, 'pending'),
(39, 1, 0, 'y', '188.143.232.31', 1366028145, 0, 'pending'),
(40, 1, 0, 'y', '188.143.232.31', 1366153639, 0, 'pending'),
(41, 1, 0, 'y', '188.143.232.31', 1366293623, 0, 'pending'),
(42, 1, 0, 'y', '188.143.232.31', 1366508664, 0, 'pending'),
(43, 1, 0, 'y', '80.2.184.141', 1366575075, 0, 'pending'),
(44, 1, 0, 'y', '188.143.232.31', 1366779858, 0, 'pending'),
(45, 1, 0, 'y', '188.143.234.127', 1366855150, 0, 'pending'),
(46, 1, 0, 'y', '37.221.174.111', 1366886228, 0, 'pending'),
(47, 1, 0, 'y', '188.143.234.127', 1367082377, 0, 'pending'),
(48, 1, 0, 'y', '188.143.232.31', 1367133632, 0, 'pending'),
(49, 1, 0, 'y', '91.232.96.20', 1367220723, 0, 'pending'),
(50, 1, 0, 'y', '188.143.234.127', 1367289747, 0, 'pending'),
(51, 1, 0, 'y', '37.221.174.111', 1367521712, 0, 'pending'),
(52, 1, 0, 'y', '122.170.116.110', 1367588551, 0, 'pending'),
(53, 1, 0, 'y', '80.93.217.46', 1367733245, 0, 'pending'),
(54, 1, 0, 'y', '188.143.234.127', 1367773304, 0, 'pending'),
(55, 1, 0, 'y', '188.143.232.31', 1367968754, 0, 'pending'),
(56, 1, 0, 'y', '86.134.226.212', 1368016225, 0, 'pending'),
(57, 1, 0, 'y', '188.143.232.31', 1368357882, 0, 'pending'),
(58, 1, 0, 'y', '188.143.232.31', 1369416371, 0, 'pending'),
(59, 1, 0, 'y', '188.143.232.31', 1369987325, 0, 'pending'),
(60, 1, 0, 'y', '188.143.232.31', 1370713509, 0, 'pending'),
(61, 1, 0, 'y', '85.242.124.32', 1370769703, 0, 'pending'),
(62, 1, 0, 'y', '188.143.232.31', 1371135523, 0, 'pending'),
(63, 1, 0, 'y', '199.83.92.162', 1371150820, 0, 'pending'),
(64, 1, 0, 'y', '216.99.146.218', 1371203162, 0, 'pending'),
(65, 1, 0, 'y', '216.99.146.218', 1371208027, 0, 'pending'),
(66, 1, 0, 'y', '216.99.146.218', 1371208271, 0, 'pending'),
(67, 1, 0, 'y', '199.83.92.162', 1371219656, 0, 'pending'),
(68, 1, 0, 'y', '216.99.146.218', 1371230862, 0, 'pending'),
(69, 1, 0, 'y', '216.99.146.218', 1371232206, 0, 'pending'),
(70, 1, 0, 'y', '216.99.146.218', 1371236183, 0, 'pending'),
(71, 1, 0, 'y', '216.99.146.218', 1371237681, 0, 'pending'),
(72, 1, 0, 'y', '216.99.146.218', 1371241568, 0, 'pending'),
(73, 1, 0, 'y', '216.99.146.218', 1371243261, 0, 'pending'),
(74, 1, 0, 'y', '216.99.146.218', 1371246986, 0, 'pending'),
(75, 1, 0, 'y', '216.99.146.218', 1371252370, 0, 'pending'),
(76, 1, 0, 'y', '216.99.146.218', 1371263280, 0, 'pending'),
(77, 1, 0, 'y', '216.99.146.218', 1371265166, 0, 'pending'),
(78, 1, 0, 'y', '216.99.146.218', 1371267621, 0, 'pending'),
(79, 1, 0, 'y', '216.99.146.218', 1371269542, 0, 'pending'),
(80, 1, 0, 'y', '216.99.146.218', 1371271848, 0, 'pending'),
(81, 1, 0, 'y', '216.99.146.218', 1371274447, 0, 'pending'),
(82, 1, 0, 'y', '216.99.146.218', 1371276210, 0, 'pending'),
(83, 1, 0, 'y', '216.99.146.218', 1371278472, 0, 'pending'),
(84, 1, 0, 'y', '216.99.146.218', 1371282304, 0, 'pending'),
(85, 1, 0, 'y', '216.99.146.218', 1371284867, 0, 'pending'),
(86, 1, 0, 'y', '216.99.146.218', 1371289086, 0, 'pending'),
(87, 1, 0, 'y', '216.99.146.218', 1371290947, 0, 'pending'),
(88, 1, 0, 'y', '216.99.146.218', 1371292194, 0, 'pending'),
(89, 1, 0, 'y', '216.99.146.218', 1371298260, 0, 'pending'),
(90, 1, 0, 'y', '216.99.146.218', 1371299655, 0, 'pending'),
(91, 1, 0, 'y', '216.99.146.218', 1371303727, 0, 'pending'),
(92, 1, 0, 'y', '216.99.146.218', 1371307105, 0, 'pending'),
(93, 1, 0, 'y', '216.99.146.218', 1371308709, 0, 'pending'),
(94, 1, 0, 'y', '216.99.146.218', 1371312862, 0, 'pending'),
(95, 1, 0, 'y', '216.99.146.218', 1371316332, 0, 'pending'),
(96, 1, 0, 'y', '216.99.146.218', 1371317952, 0, 'pending'),
(97, 1, 0, 'y', '216.99.146.218', 1371322019, 0, 'pending'),
(98, 1, 0, 'y', '216.99.146.218', 1371325402, 0, 'pending'),
(99, 1, 0, 'y', '216.99.146.218', 1371326981, 0, 'pending'),
(100, 1, 0, 'y', '216.99.146.218', 1371331041, 0, 'pending'),
(101, 1, 0, 'y', '216.99.146.218', 1371334437, 0, 'pending'),
(102, 1, 0, 'y', '216.99.146.218', 1371335997, 0, 'pending'),
(103, 1, 0, 'y', '216.99.146.218', 1371340048, 0, 'pending'),
(104, 1, 0, 'y', '216.99.146.218', 1371343449, 0, 'pending'),
(105, 1, 0, 'y', '216.99.146.218', 1371345009, 0, 'pending'),
(106, 1, 0, 'y', '216.99.146.218', 1371349056, 0, 'pending'),
(107, 1, 0, 'y', '216.99.146.218', 1371354044, 0, 'pending'),
(108, 1, 0, 'y', '216.99.146.218', 1371357506, 0, 'pending'),
(109, 1, 0, 'y', '216.99.146.218', 1371358233, 0, 'pending'),
(110, 1, 0, 'y', '216.99.146.218', 1371363059, 0, 'pending'),
(111, 1, 0, 'y', '216.99.152.122', 1371388543, 0, 'pending'),
(112, 1, 0, 'y', '216.99.146.218', 1371388595, 0, 'pending'),
(113, 1, 0, 'y', '216.99.146.218', 1371444305, 0, 'pending'),
(114, 1, 0, 'y', '216.99.146.218', 1371444418, 0, 'pending'),
(115, 1, 0, 'y', '216.99.152.122', 1371444592, 0, 'pending'),
(116, 1, 0, 'y', '216.99.152.122', 1371444642, 0, 'pending'),
(117, 1, 0, 'y', '216.99.146.218', 1371453596, 0, 'pending'),
(118, 1, 0, 'y', '216.99.152.122', 1371453941, 0, 'pending'),
(119, 1, 0, 'y', '216.99.146.218', 1371454703, 0, 'pending'),
(120, 1, 0, 'y', '216.99.152.122', 1371454890, 0, 'pending'),
(121, 1, 0, 'y', '216.99.146.218', 1371462721, 0, 'pending'),
(122, 1, 0, 'y', '216.99.152.122', 1371463384, 0, 'pending'),
(123, 1, 0, 'y', '216.99.146.218', 1371463855, 0, 'pending'),
(124, 1, 0, 'y', '216.99.152.122', 1371464184, 0, 'pending'),
(125, 1, 0, 'y', '216.99.146.218', 1371471867, 0, 'pending'),
(126, 1, 0, 'y', '216.99.152.122', 1371472737, 0, 'pending'),
(127, 1, 0, 'y', '216.99.146.218', 1371473101, 0, 'pending'),
(128, 1, 0, 'y', '216.99.152.122', 1371473670, 0, 'pending'),
(129, 1, 0, 'y', '216.99.146.218', 1371481326, 0, 'pending'),
(130, 1, 0, 'y', '216.99.152.122', 1371482270, 0, 'pending'),
(131, 1, 0, 'y', '216.99.146.218', 1371482368, 0, 'pending'),
(132, 1, 0, 'y', '216.99.152.122', 1371483168, 0, 'pending'),
(133, 1, 0, 'y', '216.99.146.218', 1371490368, 0, 'pending'),
(134, 1, 0, 'y', '216.99.146.218', 1371491407, 0, 'pending'),
(135, 1, 0, 'y', '216.99.152.122', 1371491413, 0, 'pending'),
(136, 1, 0, 'y', '216.99.152.122', 1371492344, 0, 'pending'),
(137, 1, 0, 'y', '216.99.146.218', 1371499404, 0, 'pending'),
(138, 1, 0, 'y', '216.99.146.218', 1371500442, 0, 'pending'),
(139, 1, 0, 'y', '216.99.152.122', 1371500508, 0, 'pending'),
(140, 1, 0, 'y', '216.99.152.122', 1371501466, 0, 'pending'),
(141, 1, 0, 'y', '216.99.146.218', 1371508441, 0, 'pending'),
(142, 1, 0, 'y', '216.99.146.218', 1371509480, 0, 'pending'),
(143, 1, 0, 'y', '216.99.152.122', 1371509625, 0, 'pending'),
(144, 1, 0, 'y', '216.99.152.122', 1371510589, 0, 'pending'),
(145, 1, 0, 'y', '216.99.146.218', 1371517478, 0, 'pending'),
(146, 1, 0, 'y', '216.99.146.218', 1371518525, 0, 'pending'),
(147, 1, 0, 'y', '216.99.152.122', 1371518781, 0, 'pending'),
(148, 1, 0, 'y', '216.99.152.122', 1371519730, 0, 'pending'),
(149, 1, 0, 'y', '216.99.146.218', 1371526603, 0, 'pending'),
(150, 1, 0, 'y', '216.99.146.218', 1371527629, 0, 'pending'),
(151, 1, 0, 'y', '216.99.152.122', 1371528038, 0, 'pending'),
(152, 1, 0, 'y', '216.99.152.122', 1371529017, 0, 'pending'),
(153, 1, 0, 'y', '216.99.146.218', 1371535706, 0, 'pending'),
(154, 1, 0, 'y', '216.99.146.218', 1371536740, 0, 'pending'),
(155, 1, 0, 'y', '216.99.152.122', 1371537469, 0, 'pending'),
(156, 1, 0, 'y', '216.99.152.122', 1371538499, 0, 'pending'),
(157, 1, 0, 'y', '216.99.146.218', 1371544853, 0, 'pending'),
(158, 1, 0, 'y', '216.99.146.218', 1371545887, 0, 'pending'),
(159, 1, 0, 'y', '216.99.152.122', 1371547069, 0, 'pending'),
(160, 1, 0, 'y', '216.99.152.122', 1371548323, 0, 'pending'),
(161, 1, 0, 'y', '216.99.146.218', 1371553944, 0, 'pending'),
(162, 1, 0, 'y', '216.99.146.218', 1371554970, 0, 'pending'),
(163, 1, 0, 'y', '216.99.152.122', 1371556813, 0, 'pending'),
(164, 1, 0, 'y', '216.99.152.122', 1371557974, 0, 'pending'),
(165, 1, 0, 'y', '216.99.146.218', 1371563002, 0, 'pending'),
(166, 1, 0, 'y', '216.99.146.218', 1371564041, 0, 'pending'),
(167, 1, 0, 'y', '216.99.152.122', 1371566042, 0, 'pending'),
(168, 1, 0, 'y', '216.99.152.122', 1371567368, 0, 'pending'),
(169, 1, 0, 'y', '216.99.146.218', 1371572120, 0, 'pending'),
(170, 1, 0, 'y', '216.99.146.218', 1371573119, 0, 'pending'),
(171, 1, 0, 'y', '216.99.152.122', 1371575279, 0, 'pending'),
(172, 1, 0, 'y', '216.99.152.122', 1371576863, 0, 'pending'),
(173, 1, 0, 'y', '216.99.146.218', 1371581345, 0, 'pending'),
(174, 1, 0, 'y', '216.99.146.218', 1371582253, 0, 'pending'),
(175, 1, 0, 'y', '216.99.152.122', 1371584575, 0, 'pending'),
(176, 1, 0, 'y', '216.99.152.122', 1371586174, 0, 'pending'),
(177, 1, 0, 'y', '216.99.146.218', 1371590535, 0, 'pending'),
(178, 1, 0, 'y', '216.99.146.218', 1371591377, 0, 'pending'),
(179, 1, 0, 'y', '216.99.152.122', 1371594178, 0, 'pending'),
(180, 1, 0, 'y', '216.99.152.122', 1371596083, 0, 'pending'),
(181, 1, 0, 'y', '216.99.146.218', 1371599687, 0, 'pending'),
(182, 1, 0, 'y', '216.99.146.218', 1371600472, 0, 'pending'),
(183, 1, 0, 'y', '188.143.234.127', 1371602101, 0, 'pending'),
(184, 1, 0, 'y', '216.99.152.122', 1371603451, 0, 'pending'),
(185, 1, 0, 'y', '216.99.152.122', 1371605475, 0, 'pending'),
(186, 1, 0, 'y', '216.99.146.218', 1371608739, 0, 'pending'),
(187, 1, 0, 'y', '216.99.146.218', 1371609514, 0, 'pending'),
(188, 1, 0, 'y', '216.99.152.122', 1371612793, 0, 'pending'),
(189, 1, 0, 'y', '216.99.152.122', 1371614934, 0, 'pending'),
(190, 1, 0, 'y', '216.99.146.218', 1371617783, 0, 'pending'),
(191, 1, 0, 'y', '216.99.146.218', 1371618552, 0, 'pending'),
(192, 1, 0, 'y', '216.99.152.122', 1371622199, 0, 'pending'),
(193, 1, 0, 'y', '216.99.152.122', 1371624551, 0, 'pending'),
(194, 1, 0, 'y', '216.99.146.218', 1371626937, 0, 'pending'),
(195, 1, 0, 'y', '216.99.146.218', 1371627726, 0, 'pending'),
(196, 1, 0, 'y', '216.99.152.122', 1371631545, 0, 'pending'),
(197, 1, 0, 'y', '216.99.152.122', 1371634341, 0, 'pending'),
(198, 1, 0, 'y', '216.99.146.218', 1371636054, 0, 'pending'),
(199, 1, 0, 'y', '216.99.146.218', 1371636822, 0, 'pending'),
(200, 1, 0, 'y', '216.99.152.122', 1371641456, 0, 'pending'),
(201, 1, 0, 'y', '188.143.232.31', 1371642582, 0, 'pending'),
(202, 1, 0, 'y', '216.99.152.122', 1371644666, 0, 'pending'),
(203, 1, 0, 'y', '216.99.146.218', 1371645144, 0, 'pending'),
(204, 1, 0, 'y', '216.99.146.218', 1371645902, 0, 'pending'),
(205, 1, 0, 'y', '216.99.152.122', 1371651541, 0, 'pending'),
(206, 1, 0, 'y', '216.99.146.218', 1371654262, 0, 'pending'),
(207, 1, 0, 'y', '216.99.146.218', 1371654989, 0, 'pending'),
(208, 1, 0, 'y', '216.99.152.122', 1371655147, 0, 'pending'),
(209, 1, 0, 'y', '216.99.152.122', 1371661544, 0, 'pending'),
(210, 1, 0, 'y', '216.99.146.218', 1371663412, 0, 'pending'),
(211, 1, 0, 'y', '216.99.146.218', 1371664115, 0, 'pending'),
(212, 1, 0, 'y', '216.99.152.122', 1371665078, 0, 'pending'),
(213, 1, 0, 'y', '216.99.152.122', 1371670662, 0, 'pending'),
(214, 1, 0, 'y', '216.99.146.218', 1371672449, 0, 'pending'),
(215, 1, 0, 'y', '216.99.146.218', 1371673150, 0, 'pending'),
(216, 1, 0, 'y', '216.99.152.122', 1371674378, 0, 'pending'),
(217, 1, 0, 'y', '216.99.152.122', 1371679798, 0, 'pending'),
(218, 1, 0, 'y', '216.99.146.218', 1371681487, 0, 'pending'),
(219, 1, 0, 'y', '216.99.146.218', 1371682196, 0, 'pending'),
(220, 1, 0, 'y', '216.99.152.122', 1371683860, 0, 'pending'),
(221, 1, 0, 'y', '216.99.152.122', 1371689100, 0, 'pending'),
(222, 1, 0, 'y', '216.99.146.218', 1371690615, 0, 'pending'),
(223, 1, 0, 'y', '216.99.146.218', 1371691321, 0, 'pending'),
(224, 1, 0, 'y', '216.99.146.218', 1371697794, 0, 'pending'),
(225, 1, 0, 'y', '216.99.152.122', 1371699098, 0, 'pending'),
(226, 1, 0, 'y', '216.99.146.218', 1371701281, 0, 'pending'),
(227, 1, 0, 'y', '216.99.152.122', 1371702916, 0, 'pending'),
(228, 1, 0, 'y', '216.99.146.218', 1371706829, 0, 'pending'),
(229, 1, 0, 'y', '216.99.152.122', 1371708317, 0, 'pending'),
(230, 1, 0, 'y', '216.99.146.218', 1371710316, 0, 'pending'),
(231, 1, 0, 'y', '216.99.152.122', 1371712084, 0, 'pending'),
(232, 1, 0, 'y', '216.99.146.218', 1371715905, 0, 'pending'),
(233, 1, 0, 'y', '216.99.152.122', 1371717559, 0, 'pending'),
(234, 1, 0, 'y', '216.99.146.218', 1371719417, 0, 'pending'),
(235, 1, 0, 'y', '216.99.152.122', 1371721389, 0, 'pending'),
(236, 1, 0, 'y', '216.99.146.218', 1371725020, 0, 'pending'),
(237, 1, 0, 'y', '216.99.152.122', 1371726975, 0, 'pending'),
(238, 1, 0, 'y', '216.99.146.218', 1371728606, 0, 'pending'),
(239, 1, 0, 'y', '216.99.152.122', 1371730788, 0, 'pending'),
(240, 1, 0, 'y', '216.99.146.218', 1371734157, 0, 'pending'),
(241, 1, 0, 'y', '216.99.152.122', 1371736285, 0, 'pending'),
(242, 1, 0, 'y', '216.99.146.218', 1371737662, 0, 'pending'),
(243, 1, 0, 'y', '216.99.152.122', 1371740183, 0, 'pending'),
(244, 1, 0, 'y', '216.99.146.218', 1371743298, 0, 'pending'),
(245, 1, 0, 'y', '216.99.152.122', 1371745998, 0, 'pending'),
(246, 1, 0, 'y', '216.99.146.218', 1371746848, 0, 'pending'),
(247, 1, 0, 'y', '216.99.152.122', 1371749715, 0, 'pending'),
(248, 1, 0, 'y', '216.99.146.218', 1371752410, 0, 'pending'),
(249, 1, 0, 'y', '216.99.152.122', 1371755245, 0, 'pending'),
(250, 1, 0, 'y', '216.99.146.218', 1371755902, 0, 'pending'),
(251, 1, 0, 'y', '216.99.152.122', 1371758873, 0, 'pending'),
(252, 1, 0, 'y', '216.99.146.218', 1371761441, 0, 'pending'),
(253, 1, 0, 'y', '216.99.152.122', 1371764472, 0, 'pending'),
(254, 1, 0, 'y', '216.99.146.218', 1371764937, 0, 'pending'),
(255, 1, 0, 'y', '216.99.152.122', 1371768077, 0, 'pending'),
(256, 1, 0, 'y', '216.99.146.218', 1371770476, 0, 'pending'),
(257, 1, 0, 'y', '216.99.152.122', 1371773671, 0, 'pending'),
(258, 1, 0, 'y', '216.99.146.218', 1371773973, 0, 'pending'),
(259, 1, 0, 'y', '216.99.152.122', 1371777292, 0, 'pending'),
(260, 1, 0, 'y', '216.99.146.218', 1371779513, 0, 'pending'),
(261, 1, 0, 'y', '216.99.152.122', 1371782934, 0, 'pending'),
(262, 1, 0, 'y', '216.99.146.218', 1371783061, 0, 'pending'),
(263, 1, 0, 'y', '216.99.152.122', 1371786658, 0, 'pending'),
(264, 1, 0, 'y', '216.99.146.218', 1371788628, 0, 'pending'),
(265, 1, 0, 'y', '216.99.146.218', 1371792185, 0, 'pending'),
(266, 1, 0, 'y', '216.99.152.122', 1371792345, 0, 'pending'),
(267, 1, 0, 'y', '216.99.152.122', 1371796075, 0, 'pending'),
(268, 1, 0, 'y', '188.143.234.127', 1371796904, 0, 'pending'),
(269, 1, 0, 'y', '216.99.146.218', 1371797706, 0, 'pending'),
(270, 1, 0, 'y', '216.99.146.218', 1371802104, 0, 'pending'),
(271, 1, 0, 'y', '216.99.152.122', 1371802558, 0, 'pending'),
(272, 1, 0, 'y', '199.83.92.162', 1371805291, 0, 'pending'),
(273, 1, 0, 'y', '216.99.152.122', 1371806408, 0, 'pending'),
(274, 1, 0, 'y', '93.115.83.252', 1371840272, 0, 'pending'),
(275, 1, 0, 'y', '188.143.232.31', 1372037263, 0, 'pending'),
(276, 1, 0, 'y', '188.143.234.127', 1372046448, 0, 'pending'),
(277, 1, 0, 'y', '199.83.92.162', 1372096271, 0, 'pending'),
(278, 1, 0, 'y', '199.83.92.162', 1372100022, 0, 'pending'),
(279, 1, 0, 'y', '199.83.92.162', 1372159719, 0, 'pending'),
(280, 1, 0, 'y', '199.83.92.162', 1372164225, 0, 'pending'),
(281, 1, 0, 'y', '199.83.92.162', 1372169276, 0, 'pending'),
(282, 1, 0, 'y', '199.83.92.162', 1372173781, 0, 'pending'),
(283, 1, 0, 'y', '199.83.92.162', 1372178795, 0, 'pending'),
(284, 1, 0, 'y', '199.83.92.162', 1372183343, 0, 'pending'),
(285, 1, 0, 'y', '199.83.92.162', 1372188344, 0, 'pending'),
(286, 1, 0, 'y', '199.83.92.162', 1372192899, 0, 'pending'),
(287, 1, 0, 'y', '199.83.92.162', 1372197899, 0, 'pending'),
(288, 1, 0, 'y', '149.241.194.187', 1372198834, 0, 'pending'),
(289, 1, 0, 'y', '199.83.92.162', 1372202506, 0, 'pending'),
(290, 1, 0, 'y', '199.83.92.162', 1372207462, 0, 'pending'),
(291, 1, 0, 'y', '199.83.92.162', 1372212086, 0, 'pending'),
(292, 1, 0, 'y', '199.83.92.162', 1372217042, 0, 'pending'),
(293, 1, 0, 'y', '199.83.92.162', 1372221945, 0, 'pending'),
(294, 1, 0, 'y', '199.83.92.162', 1372227117, 0, 'pending'),
(295, 1, 0, 'y', '199.83.92.162', 1372232467, 0, 'pending'),
(296, 1, 0, 'y', '199.83.92.162', 1372236809, 0, 'pending'),
(297, 1, 0, 'y', '199.83.92.162', 1372242700, 0, 'pending'),
(298, 1, 0, 'y', '199.83.92.162', 1372246540, 0, 'pending'),
(299, 1, 0, 'y', '199.83.92.162', 1372253874, 0, 'pending'),
(300, 1, 0, 'y', '199.83.92.162', 1372256872, 0, 'pending'),
(301, 1, 0, 'y', '199.83.92.162', 1372265589, 0, 'pending'),
(302, 1, 0, 'y', '199.83.92.162', 1372267232, 0, 'pending'),
(303, 1, 0, 'y', '199.83.92.162', 1372277685, 0, 'pending'),
(304, 1, 0, 'y', '199.83.92.162', 1372277772, 0, 'pending'),
(305, 1, 0, 'y', '199.83.92.162', 1372288744, 0, 'pending'),
(306, 1, 0, 'y', '199.83.92.162', 1372290021, 0, 'pending'),
(307, 1, 0, 'y', '199.83.92.162', 1372300689, 0, 'pending'),
(308, 1, 0, 'y', '199.83.92.162', 1372303266, 0, 'pending'),
(309, 1, 0, 'y', '199.83.92.162', 1372313333, 0, 'pending'),
(310, 1, 0, 'y', '216.99.152.122', 1372324739, 0, 'pending'),
(311, 1, 0, 'y', '216.99.146.218', 1372327344, 0, 'pending'),
(312, 1, 0, 'y', '216.99.152.122', 1372328684, 0, 'pending'),
(313, 1, 0, 'y', '199.83.92.162', 1372330058, 0, 'pending'),
(314, 1, 0, 'y', '216.99.146.218', 1372330478, 0, 'pending'),
(315, 1, 0, 'y', '216.99.152.122', 1372334134, 0, 'pending'),
(316, 1, 0, 'y', '216.99.146.218', 1372336598, 0, 'pending'),
(317, 1, 0, 'y', '216.99.152.122', 1372338556, 0, 'pending'),
(318, 1, 0, 'y', '199.83.92.162', 1372338668, 0, 'pending'),
(319, 1, 0, 'y', '216.99.146.218', 1372339659, 0, 'pending'),
(320, 1, 0, 'y', '216.99.152.122', 1372345158, 0, 'pending'),
(321, 1, 0, 'y', '216.99.146.218', 1372348555, 0, 'pending'),
(322, 1, 0, 'y', '199.83.92.162', 1372351098, 0, 'pending'),
(323, 1, 0, 'y', '216.99.152.122', 1372351351, 0, 'pending'),
(324, 1, 0, 'y', '216.99.146.218', 1372351452, 0, 'pending'),
(325, 1, 0, 'y', '188.143.232.31', 1372560460, 0, 'pending'),
(326, 1, 0, 'y', '91.207.6.138', 1372930040, 0, 'pending'),
(327, 1, 0, 'y', '117.27.138.5', 1373199607, 0, 'pending'),
(328, 1, 0, 'y', '188.143.232.31', 1373261922, 0, 'pending'),
(329, 1, 0, 'y', '188.143.234.127', 1373555186, 0, 'pending'),
(330, 1, 0, 'y', '58.22.10.91', 1373746749, 0, 'pending'),
(331, 1, 0, 'y', '58.22.10.91', 1373868076, 0, 'pending'),
(332, 1, 0, 'y', '188.143.232.31', 1373878952, 0, 'pending'),
(333, 1, 0, 'y', '58.22.10.91', 1374028081, 0, 'pending'),
(334, 1, 0, 'y', '58.22.10.91', 1374147822, 0, 'pending'),
(335, 1, 0, 'y', '58.22.10.91', 1374161768, 0, 'pending'),
(336, 1, 0, 'y', '58.22.10.91', 1374249818, 0, 'pending'),
(337, 1, 0, 'y', '188.143.232.31', 1374281907, 0, 'pending'),
(338, 1, 0, 'y', '58.22.10.91', 1374383257, 0, 'pending'),
(339, 1, 0, 'y', '188.143.234.127', 1374491005, 0, 'pending'),
(340, 1, 0, 'y', '78.141.22.90', 1374491979, 0, 'pending'),
(341, 1, 0, 'y', '188.143.234.127', 1374560996, 0, 'pending'),
(342, 1, 0, 'y', '178.150.142.210', 1374589816, 0, 'pending'),
(343, 1, 0, 'y', '188.143.232.31', 1374686439, 0, 'pending'),
(344, 1, 0, 'y', '46.105.114.75', 1374700985, 0, 'pending'),
(345, 1, 0, 'y', '199.180.100.169', 1374831203, 0, 'pending'),
(346, 1, 0, 'y', '188.143.232.211', 1374981673, 0, 'pending'),
(347, 1, 0, 'y', '58.22.10.91', 1375014813, 0, 'pending'),
(348, 1, 0, 'y', '199.180.100.170', 1375031003, 0, 'pending'),
(349, 1, 0, 'y', '188.143.232.31', 1375064210, 0, 'pending'),
(350, 1, 0, 'y', '58.22.10.91', 1375075015, 0, 'pending'),
(351, 1, 0, 'y', '120.43.31.154', 1375081221, 0, 'pending'),
(352, 1, 0, 'y', '120.43.31.154', 1375104114, 0, 'pending'),
(353, 1, 0, 'y', '188.143.232.211', 1375105746, 0, 'pending'),
(354, 1, 0, 'y', '199.180.100.170', 1375149255, 0, 'pending'),
(355, 1, 0, 'y', '58.22.10.91', 1375258400, 0, 'pending'),
(356, 1, 0, 'y', '110.89.52.225', 1375320226, 0, 'pending'),
(357, 1, 0, 'y', '120.37.225.197', 1375343892, 0, 'pending'),
(358, 1, 0, 'y', '36.248.102.179', 1375361374, 0, 'pending'),
(359, 1, 0, 'y', '58.22.22.90', 1375438242, 0, 'pending'),
(360, 1, 0, 'y', '36.248.20.97', 1375442683, 0, 'pending'),
(361, 1, 0, 'y', '36.251.45.95', 1375447029, 0, 'pending'),
(362, 1, 0, 'y', '36.251.46.76', 1375504359, 0, 'pending'),
(363, 1, 0, 'y', '58.22.23.0', 1375532865, 0, 'pending'),
(364, 1, 0, 'y', '142.4.119.237', 1375620100, 0, 'pending'),
(365, 1, 0, 'y', '188.143.232.31', 1375632448, 0, 'pending'),
(366, 1, 0, 'y', '220.249.165.183', 1375682601, 0, 'pending'),
(367, 1, 0, 'y', '220.200.12.14', 1375694596, 0, 'pending'),
(368, 1, 0, 'y', '112.111.14.195', 1375791957, 0, 'pending'),
(369, 1, 0, 'y', '58.22.10.90', 1375793515, 0, 'pending'),
(370, 1, 0, 'y', '58.22.10.90', 1375794411, 0, 'pending'),
(371, 1, 0, 'y', '58.22.10.90', 1375794466, 0, 'pending'),
(372, 1, 0, 'y', '58.22.10.90', 1375845802, 0, 'pending'),
(373, 1, 0, 'y', '58.22.10.90', 1375847729, 0, 'pending'),
(374, 1, 0, 'y', '36.248.102.78', 1375864670, 0, 'pending'),
(375, 1, 0, 'y', '36.251.47.90', 1375867136, 0, 'pending'),
(376, 1, 0, 'y', '58.22.20.85', 1375867838, 0, 'pending'),
(377, 1, 0, 'y', '220.250.43.77', 1375867942, 0, 'pending'),
(378, 1, 0, 'y', '36.248.23.8', 1375868416, 0, 'pending'),
(379, 1, 0, 'y', '58.22.10.90', 1375900233, 0, 'pending'),
(380, 1, 0, 'y', '58.22.10.90', 1375900236, 0, 'pending'),
(381, 1, 0, 'y', '58.22.10.90', 1375900523, 0, 'pending'),
(382, 1, 0, 'y', '58.22.10.90', 1375955597, 0, 'pending'),
(383, 1, 0, 'y', '58.22.10.90', 1375956431, 0, 'pending'),
(384, 1, 0, 'y', '86.134.126.4', 1375962680, 0, 'pending'),
(385, 1, 0, 'y', '58.22.10.90', 1375988051, 0, 'pending'),
(386, 1, 0, 'y', '58.22.10.90', 1375988061, 0, 'pending'),
(387, 1, 0, 'y', '58.22.10.90', 1375988400, 0, 'pending'),
(388, 1, 0, 'y', '112.111.14.107', 1376022143, 0, 'pending'),
(389, 1, 0, 'y', '211.97.108.178', 1376022414, 0, 'pending'),
(390, 1, 0, 'y', '112.111.12.97', 1376025274, 0, 'pending'),
(391, 1, 0, 'y', '220.250.58.170', 1376049145, 0, 'pending'),
(392, 1, 0, 'y', '220.250.58.170', 1376049451, 0, 'pending'),
(393, 1, 0, 'y', '58.22.10.90', 1376073604, 0, 'pending'),
(394, 1, 0, 'y', '58.22.10.90', 1376073605, 0, 'pending'),
(395, 1, 0, 'y', '58.22.10.90', 1376073608, 0, 'pending'),
(396, 1, 0, 'y', '220.200.12.115', 1376121703, 0, 'pending'),
(397, 1, 0, 'y', '220.250.58.170', 1376130270, 0, 'pending'),
(398, 1, 0, 'y', '36.248.20.194', 1376134545, 0, 'pending'),
(399, 1, 0, 'y', '220.200.12.115', 1376135876, 0, 'pending'),
(400, 1, 0, 'y', '220.250.40.251', 1376136502, 0, 'pending'),
(401, 1, 0, 'y', '220.250.40.251', 1376136555, 0, 'pending'),
(402, 1, 0, 'y', '112.111.14.107', 1376137672, 0, 'pending'),
(403, 1, 0, 'y', '58.22.10.90', 1376138012, 0, 'pending'),
(404, 1, 0, 'y', '58.22.10.90', 1376138058, 0, 'pending'),
(405, 1, 0, 'y', '58.22.10.90', 1376138228, 0, 'pending'),
(406, 1, 0, 'y', '112.111.14.107', 1376138330, 0, 'pending'),
(407, 1, 0, 'y', '58.22.10.90', 1376140203, 0, 'pending'),
(408, 1, 0, 'y', '58.22.10.90', 1376141697, 0, 'pending'),
(409, 1, 0, 'y', '112.111.14.107', 1376248477, 0, 'pending'),
(410, 1, 0, 'y', '220.250.58.170', 1376248921, 0, 'pending'),
(411, 1, 0, 'y', '36.248.20.194', 1376253573, 0, 'pending'),
(412, 1, 0, 'y', '36.248.22.169', 1376278773, 0, 'pending'),
(413, 1, 0, 'y', '61.241.223.184', 1376279805, 0, 'pending'),
(414, 1, 0, 'y', '58.22.10.90', 1376290702, 0, 'pending'),
(415, 1, 0, 'y', '61.241.223.184', 1376298621, 0, 'pending'),
(416, 1, 0, 'y', '36.251.46.214', 1376299340, 0, 'pending'),
(417, 1, 0, 'y', '36.248.22.169', 1376299663, 0, 'pending'),
(418, 1, 0, 'y', '58.22.10.90', 1376308394, 0, 'pending'),
(419, 1, 0, 'y', '58.22.10.90', 1376308420, 0, 'pending'),
(420, 1, 0, 'y', '58.22.10.90', 1376308444, 0, 'pending'),
(421, 1, 0, 'y', '58.22.10.90', 1376308784, 0, 'pending'),
(422, 1, 0, 'y', '36.248.22.169', 1376368437, 0, 'pending'),
(423, 1, 0, 'y', '58.22.10.90', 1376370045, 0, 'pending'),
(424, 1, 0, 'y', '58.22.10.90', 1376371524, 0, 'pending'),
(425, 1, 0, 'y', '112.111.55.151', 1376372920, 0, 'pending'),
(426, 1, 0, 'y', '112.111.55.151', 1376373728, 0, 'pending'),
(427, 1, 0, 'y', '36.251.46.214', 1376374361, 0, 'pending'),
(428, 1, 0, 'y', '220.250.58.170', 1376382857, 0, 'pending'),
(429, 1, 0, 'y', '220.250.58.170', 1376383453, 0, 'pending'),
(430, 1, 0, 'y', '36.248.124.255', 1376454126, 0, 'pending'),
(431, 1, 0, 'y', '36.248.127.94', 1376456333, 0, 'pending'),
(432, 1, 0, 'y', '58.22.10.90', 1376463391, 0, 'pending'),
(433, 1, 0, 'y', '58.22.10.90', 1376470434, 0, 'pending'),
(434, 1, 0, 'y', '58.22.10.90', 1376470807, 0, 'pending'),
(435, 1, 0, 'y', '61.241.223.70', 1376471418, 0, 'pending'),
(436, 1, 0, 'y', '58.22.10.90', 1376471651, 0, 'pending'),
(437, 1, 0, 'y', '58.22.10.90', 1376472084, 0, 'pending'),
(438, 1, 0, 'y', '36.248.100.170', 1376560243, 0, 'pending'),
(439, 1, 0, 'y', '112.111.53.98', 1376561453, 0, 'pending'),
(440, 1, 0, 'y', '58.22.23.41', 1376568421, 0, 'pending'),
(441, 1, 0, 'y', '58.22.23.41', 1376569032, 0, 'pending'),
(442, 1, 0, 'y', '58.22.10.90', 1376623191, 0, 'pending'),
(443, 1, 0, 'y', '58.22.10.90', 1376623609, 0, 'pending'),
(444, 1, 0, 'y', '220.250.58.170', 1376625117, 0, 'pending'),
(445, 1, 0, 'y', '112.111.52.244', 1376626516, 0, 'pending'),
(446, 1, 0, 'y', '220.250.58.170', 1376629840, 0, 'pending'),
(447, 1, 0, 'y', '58.22.10.90', 1376638730, 0, 'pending'),
(448, 1, 0, 'y', '58.22.10.90', 1376638814, 0, 'pending'),
(449, 1, 0, 'y', '58.22.10.90', 1376638932, 0, 'pending'),
(450, 1, 0, 'y', '58.22.10.90', 1376639313, 0, 'pending'),
(451, 1, 0, 'y', '58.22.10.90', 1376640915, 0, 'pending'),
(452, 1, 0, 'y', '36.248.22.36', 1376704192, 0, 'pending'),
(453, 1, 0, 'y', '36.248.124.57', 1376712347, 0, 'pending'),
(454, 1, 0, 'y', '36.248.124.57', 1376713066, 0, 'pending'),
(455, 1, 0, 'y', '58.22.10.90', 1376777847, 0, 'pending'),
(456, 1, 0, 'y', '58.22.10.90', 1376778338, 0, 'pending'),
(457, 1, 0, 'y', '58.22.10.90', 1376779017, 0, 'pending'),
(458, 1, 0, 'y', '58.22.10.90', 1376801948, 0, 'pending'),
(459, 1, 0, 'y', '58.22.10.90', 1376802188, 0, 'pending'),
(460, 1, 0, 'y', '220.250.58.170', 1376892690, 0, 'pending'),
(461, 1, 0, 'y', '175.42.9.217', 1376893765, 0, 'pending'),
(462, 1, 0, 'y', '211.97.108.71', 1376899406, 0, 'pending'),
(463, 1, 0, 'y', '58.22.10.90', 1376904244, 0, 'pending'),
(464, 1, 0, 'y', '58.22.10.90', 1376961770, 0, 'pending'),
(465, 1, 0, 'y', '58.22.10.90', 1376962011, 0, 'pending'),
(466, 1, 0, 'y', '58.22.10.90', 1376962012, 0, 'pending'),
(467, 1, 0, 'y', '58.22.10.90', 1376962169, 0, 'pending'),
(468, 1, 0, 'y', '58.22.10.90', 1376965872, 0, 'pending'),
(469, 1, 0, 'y', '58.22.10.90', 1377116945, 0, 'pending'),
(470, 1, 0, 'y', '58.22.10.90', 1377117060, 0, 'pending'),
(471, 1, 0, 'y', '58.22.10.90', 1377117218, 0, 'pending'),
(472, 1, 0, 'y', '58.22.10.90', 1377117485, 0, 'pending'),
(473, 1, 0, 'y', '58.22.10.90', 1377118678, 0, 'pending'),
(474, 1, 0, 'y', '91.200.14.59', 1377235404, 0, 'pending'),
(475, 1, 0, 'y', '58.22.10.90', 1377271537, 0, 'pending'),
(476, 1, 0, 'y', '58.22.10.90', 1377271914, 0, 'pending'),
(477, 1, 0, 'y', '58.22.10.90', 1377272773, 0, 'pending'),
(478, 1, 0, 'y', '58.22.10.90', 1377272994, 0, 'pending'),
(479, 1, 0, 'y', '58.22.10.90', 1377278261, 0, 'pending'),
(480, 1, 0, 'y', '58.22.10.90', 1377315735, 0, 'pending'),
(481, 1, 0, 'y', '91.200.13.40', 1377388394, 0, 'pending'),
(482, 1, 0, 'y', '58.22.10.90', 1377450664, 0, 'pending'),
(483, 1, 0, 'y', '58.22.10.90', 1377450666, 0, 'pending'),
(484, 1, 0, 'y', '58.22.10.90', 1377451188, 0, 'pending'),
(485, 1, 0, 'y', '58.22.10.90', 1377451522, 0, 'pending'),
(486, 1, 0, 'y', '58.22.10.90', 1377452210, 0, 'pending'),
(487, 1, 0, 'y', '58.22.10.90', 1377579135, 0, 'pending'),
(488, 1, 0, 'y', '112.111.55.71', 1377585071, 0, 'pending'),
(489, 1, 0, 'y', '58.22.10.90', 1377610675, 0, 'pending'),
(490, 1, 0, 'y', '58.22.10.90', 1377610681, 0, 'pending'),
(491, 1, 0, 'y', '58.22.10.90', 1377611487, 0, 'pending'),
(492, 1, 0, 'y', '58.22.10.90', 1377612071, 0, 'pending'),
(493, 1, 0, 'y', '58.22.10.90', 1377619818, 0, 'pending'),
(494, 1, 0, 'y', '58.22.10.93', 1377670345, 0, 'pending'),
(495, 1, 0, 'y', '220.249.167.198', 1377694644, 0, 'pending'),
(496, 1, 0, 'y', '188.143.232.111', 1377746369, 0, 'pending'),
(497, 1, 0, 'y', '58.22.10.93', 1377747433, 0, 'pending'),
(498, 1, 0, 'y', '36.248.21.2', 1377764131, 0, 'pending'),
(499, 1, 0, 'y', '216.244.85.122', 1377771564, 0, 'pending'),
(500, 1, 0, 'y', '216.244.85.122', 1377772436, 0, 'pending'),
(501, 1, 0, 'y', '175.42.8.246', 1377774358, 0, 'pending'),
(502, 1, 0, 'y', '58.22.10.93', 1377776828, 0, 'pending'),
(503, 1, 0, 'y', '58.22.10.93', 1377777655, 0, 'pending'),
(504, 1, 0, 'y', '58.22.10.93', 1377782114, 0, 'pending'),
(505, 1, 0, 'y', '91.121.170.197', 1377831489, 0, 'pending'),
(506, 1, 0, 'y', '188.143.232.111', 1377834274, 0, 'pending'),
(507, 1, 0, 'y', '58.22.10.93', 1377839343, 0, 'pending'),
(508, 1, 0, 'y', '58.22.10.93', 1377839344, 0, 'pending'),
(509, 1, 0, 'y', '220.249.167.158', 1377849853, 0, 'pending'),
(510, 1, 0, 'y', '58.22.10.93', 1377873873, 0, 'pending'),
(511, 1, 0, 'y', '58.22.10.93', 1377873884, 0, 'pending'),
(512, 1, 0, 'y', '188.143.234.127', 1377904462, 0, 'pending'),
(513, 1, 0, 'y', '188.143.232.31', 1377904980, 0, 'pending'),
(514, 1, 0, 'y', '58.22.10.93', 1378100909, 0, 'pending'),
(515, 1, 0, 'y', '58.22.10.93', 1378100946, 0, 'pending'),
(516, 1, 0, 'y', '58.22.10.93', 1378186839, 0, 'pending'),
(517, 1, 0, 'y', '58.22.10.93', 1378186852, 0, 'pending'),
(518, 1, 0, 'y', '58.22.10.93', 1378186905, 0, 'pending'),
(519, 1, 0, 'y', '58.22.10.93', 1378194823, 0, 'pending'),
(520, 1, 0, 'y', '58.22.10.93', 1378202489, 0, 'pending'),
(521, 1, 0, 'y', '220.250.42.207', 1378234574, 0, 'pending'),
(522, 1, 0, 'y', '58.22.10.93', 1378358994, 0, 'pending'),
(523, 1, 0, 'y', '58.22.10.93', 1378358994, 0, 'pending'),
(524, 1, 0, 'y', '58.22.10.93', 1378358995, 0, 'pending'),
(525, 1, 0, 'y', '58.22.10.93', 1378450332, 0, 'pending'),
(526, 1, 0, 'y', '58.22.10.93', 1378450389, 0, 'pending'),
(527, 1, 0, 'y', '195.153.194.169', 1378466088, 0, 'pending'),
(528, 1, 0, 'y', '58.22.10.93', 1378554503, 0, 'pending'),
(529, 1, 0, 'y', '97.107.134.76', 1378556425, 0, 'pending'),
(530, 1, 0, 'y', '58.22.10.93', 1378711826, 0, 'pending'),
(531, 1, 0, 'y', '58.22.10.93', 1378711835, 0, 'pending'),
(532, 1, 0, 'y', '58.22.10.93', 1378712331, 0, 'pending'),
(533, 1, 0, 'y', '192.34.109.162', 1378783311, 0, 'pending'),
(534, 1, 0, 'y', '58.22.10.93', 1378787003, 0, 'pending'),
(535, 1, 0, 'y', '58.22.10.93', 1378798463, 0, 'pending'),
(536, 1, 0, 'y', '58.22.10.93', 1378798467, 0, 'pending'),
(537, 1, 0, 'y', '58.22.10.93', 1378798476, 0, 'pending'),
(538, 1, 0, 'y', '58.22.10.93', 1378798489, 0, 'pending'),
(539, 1, 0, 'y', '58.22.10.93', 1378798720, 0, 'pending'),
(540, 1, 0, 'y', '175.42.8.158', 1378809588, 0, 'pending'),
(541, 1, 0, 'y', '216.244.85.122', 1378811137, 0, 'pending'),
(542, 1, 0, 'y', '220.200.12.255', 1378906242, 0, 'pending'),
(543, 1, 0, 'y', '58.22.10.93', 1378972315, 0, 'pending'),
(544, 1, 0, 'y', '58.22.10.93', 1378972331, 0, 'pending'),
(545, 1, 0, 'y', '58.22.10.93', 1378972336, 0, 'pending'),
(546, 1, 0, 'y', '58.22.10.93', 1378972399, 0, 'pending'),
(547, 1, 0, 'y', '58.22.10.93', 1378972404, 0, 'pending'),
(548, 1, 0, 'y', '175.42.9.121', 1378985545, 0, 'pending'),
(549, 1, 0, 'y', '58.22.10.93', 1379074817, 0, 'pending'),
(550, 1, 0, 'y', '122.177.95.96', 1379076349, 0, 'pending'),
(551, 1, 0, 'y', '58.22.10.93', 1379315002, 0, 'pending'),
(552, 1, 0, 'y', '192.34.109.162', 1379315085, 0, 'pending'),
(553, 1, 0, 'y', '58.22.10.93', 1379315168, 0, 'pending'),
(554, 1, 0, 'y', '58.22.10.93', 1379315259, 0, 'pending'),
(555, 1, 0, 'y', '58.22.10.93', 1379315294, 0, 'pending'),
(556, 1, 0, 'y', '58.22.10.93', 1379319014, 0, 'pending'),
(557, 1, 0, 'y', '36.251.46.144', 1379338219, 0, 'pending'),
(558, 1, 0, 'y', '188.143.234.127', 1379366958, 0, 'pending'),
(559, 1, 0, 'y', '36.248.68.184', 1379425796, 0, 'pending'),
(560, 1, 0, 'y', '36.248.68.184', 1379425798, 0, 'pending'),
(561, 1, 0, 'y', '58.22.10.93', 1379490265, 0, 'pending'),
(562, 1, 0, 'y', '58.22.10.93', 1379490279, 0, 'pending'),
(563, 1, 0, 'y', '58.22.10.93', 1379490329, 0, 'pending'),
(564, 1, 0, 'y', '58.22.10.93', 1379490383, 0, 'pending'),
(565, 1, 0, 'y', '58.22.10.93', 1379490385, 0, 'pending'),
(566, 1, 0, 'y', '5.9.51.68', 1379665410, 0, 'pending'),
(567, 1, 0, 'y', '58.22.10.93', 1379746619, 0, 'pending'),
(568, 1, 0, 'y', '58.22.10.93', 1379746631, 0, 'pending'),
(569, 1, 0, 'y', '58.22.10.93', 1379836680, 0, 'pending'),
(570, 1, 0, 'y', '58.22.10.93', 1379836697, 0, 'pending'),
(571, 1, 0, 'y', '58.22.10.93', 1379859527, 0, 'pending'),
(572, 1, 0, 'y', '220.249.167.164', 1379912270, 0, 'pending'),
(573, 1, 0, 'y', '220.249.167.164', 1379912270, 0, 'pending'),
(574, 1, 0, 'y', '58.22.10.93', 1379918589, 0, 'pending'),
(575, 1, 0, 'y', '58.22.10.93', 1379918618, 0, 'pending'),
(576, 1, 0, 'y', '58.22.10.93', 1379918975, 0, 'pending'),
(577, 1, 0, 'y', '36.248.103.108', 1379934837, 0, 'pending'),
(578, 1, 0, 'y', '58.22.10.93', 1380000280, 0, 'pending'),
(579, 1, 0, 'y', '91.121.170.197', 1382643513, 0, 'pending'),
(580, 1, 0, 'y', '109.111.197.225', 1382698294, 0, 'pending'),
(581, 1, 0, 'y', '188.143.232.111', 1382770694, 0, 'pending'),
(582, 1, 0, 'y', '114.220.79.174', 1383027527, 0, 'pending'),
(583, 1, 1, 'y', '109.111.197.225', 1383040965, 0, 'pending'),
(584, 1, 1, 'y', '109.111.197.225', 1383041116, 0, 'pending'),
(585, 1, 1, 'y', '109.111.197.225', 1383041183, 0, 'pending'),
(586, 1, 1, 'y', '109.111.197.225', 1383041359, 0, 'pending'),
(587, 1, 1, 'y', '109.111.197.225', 1383041492, 0, 'pending'),
(588, 1, 0, 'y', '109.111.197.225', 1383048700, 0, 'pending'),
(589, 1, 0, 'y', '118.97.147.154', 1383338224, 0, 'pending'),
(590, 1, 0, 'y', '178.150.142.210', 1383346683, 0, 'pending'),
(591, 1, 0, 'y', '82.132.233.233', 1383576032, 0, 'pending'),
(592, 1, 0, 'y', '188.143.232.111', 1384565833, 0, 'pending'),
(593, 1, 0, 'y', '188.143.232.31', 1384749198, 0, 'pending'),
(594, 1, 0, 'y', '188.143.232.111', 1385041378, 0, 'pending'),
(595, 1, 0, 'y', '188.143.232.31', 1385225984, 0, 'pending'),
(596, 1, 0, 'y', '188.143.232.111', 1385500643, 0, 'pending'),
(597, 1, 0, 'y', '188.143.232.31', 1385718006, 0, 'pending'),
(598, 1, 0, 'y', '188.143.232.111', 1385964962, 0, 'pending'),
(599, 1, 0, 'y', '80.2.169.242', 1386012917, 0, 'pending'),
(600, 1, 0, 'y', '59.90.219.97', 1386152948, 0, 'pending'),
(601, 1, 0, 'y', '188.143.232.31', 1386353968, 0, 'pending'),
(602, 1, 0, 'y', '188.143.232.31', 1387537902, 0, 'pending'),
(603, 1, 0, 'y', '188.143.232.31', 1388287005, 0, 'pending'),
(604, 1, 0, 'y', '188.143.232.111', 1388724591, 0, 'pending'),
(605, 1, 0, 'y', '188.143.232.111', 1389226779, 0, 'pending'),
(606, 1, 0, 'y', '92.17.193.123', 1389404360, 0, 'pending'),
(607, 1, 0, 'y', '218.104.148.195', 1389600812, 0, 'pending'),
(608, 1, 0, 'y', '88.104.8.102', 1390149096, 0, 'pending'),
(609, 1, 0, 'y', '188.143.232.111', 1390343682, 0, 'pending'),
(610, 1, 0, 'y', '188.143.232.31', 1390381999, 0, 'pending'),
(611, 1, 0, 'y', '188.143.232.111', 1390870903, 0, 'pending'),
(612, 1, 0, 'y', '194.168.77.20', 1390979290, 0, 'pending'),
(613, 1, 0, 'y', '46.233.116.172', 1391012538, 0, 'pending'),
(614, 1, 0, 'y', '188.143.232.111', 1391441851, 0, 'pending'),
(615, 1, 0, 'y', '188.143.232.111', 1391980937, 0, 'pending'),
(616, 1, 0, 'y', '94.242.255.188', 1392382052, 0, 'pending'),
(617, 1, 0, 'y', '188.143.232.111', 1392454543, 0, 'pending'),
(618, 1, 0, 'y', '94.242.255.188', 1392550173, 0, 'pending'),
(619, 1, 0, 'y', '94.242.255.188', 1392564404, 0, 'pending'),
(620, 1, 0, 'y', '188.143.232.31', 1392921469, 0, 'pending'),
(621, 1, 0, 'y', '37.236.167.250', 1392923802, 0, 'pending'),
(622, 1, 0, 'y', '188.143.232.111', 1393010719, 0, 'pending'),
(623, 1, 0, 'y', '188.143.232.31', 1393527457, 0, 'pending'),
(624, 1, 0, 'y', '90.219.253.44', 1393575927, 0, 'pending'),
(625, 1, 0, 'y', '188.143.232.31', 1394086007, 0, 'pending'),
(626, 1, 0, 'y', '94.1.110.11', 1394211156, 0, 'pending'),
(627, 1, 0, 'y', '217.144.149.130', 1394633058, 0, 'pending'),
(628, 1, 0, 'y', '216.151.137.36', 1395372560, 0, 'pending'),
(629, 1, 0, 'y', '188.143.232.111', 1395429969, 0, 'pending'),
(630, 1, 0, 'y', '188.143.232.31', 1395449636, 0, 'pending'),
(631, 1, 0, 'y', '91.121.170.197', 1395490398, 0, 'pending'),
(632, 1, 0, 'y', '216.151.137.36', 1395602732, 0, 'pending'),
(633, 1, 0, 'y', '86.185.77.34', 1395916404, 0, 'pending'),
(634, 1, 0, 'y', '188.143.232.111', 1396016084, 0, 'pending'),
(635, 1, 0, 'y', '188.143.232.31', 1396048239, 0, 'pending'),
(636, 1, 0, 'y', '91.121.170.197', 1396098395, 0, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_multipage_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_multipage_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit` char(1) NOT NULL DEFAULT 'n',
  `data` text,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`),
  KEY `ip_address` (`ip_address`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_notification_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_notification_templates` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `notification_name` varchar(150) NOT NULL DEFAULT 'default',
  `notification_label` varchar(150) NOT NULL DEFAULT 'default',
  `notification_description` text,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `allow_html` char(1) NOT NULL DEFAULT 'n',
  `from_name` varchar(150) NOT NULL DEFAULT '',
  `from_email` varchar(250) NOT NULL DEFAULT '',
  `reply_to_email` varchar(250) NOT NULL DEFAULT '',
  `email_subject` varchar(128) NOT NULL DEFAULT 'default',
  `include_attachments` char(1) NOT NULL DEFAULT 'n',
  `template_data` text,
  PRIMARY KEY (`notification_id`),
  KEY `notification_name` (`notification_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_params`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5159 ;

--
-- Dumping data for table `exp_freeform_params`
--

INSERT INTO `exp_freeform_params` (`params_id`, `entry_date`, `data`) VALUES
(5158, 1396262574, '{"form_id":"2","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"get-in-touch-thankyou","inline_error_return":"get-in-touch","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"name|email|contact_number|message","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"martin@shoot-the-moon.co.uk","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":false,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_preferences`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) DEFAULT NULL,
  `preference_value` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`preference_id`),
  KEY `preference_name` (`preference_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_freeform_preferences`
--

INSERT INTO `exp_freeform_preferences` (`preference_id`, `preference_name`, `preference_value`, `site_id`) VALUES
(1, 'ffp', 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_user_email`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  `email_addresses` text,
  PRIMARY KEY (`email_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_global_variables`
--

CREATE TABLE IF NOT EXISTS `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `exp_global_variables`
--

INSERT INTO `exp_global_variables` (`variable_id`, `site_id`, `variable_name`, `variable_data`, `sync_time`) VALUES
(1, 1, 'lv_thumbs_nav', '		<ul id="switch" class="pie clearfix">\n		\n			{if segment_1=="creative"}\n			{exp:channel:entries channel="packaging|branding|literature|point_of_sale" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="0"}\n			{cf_portfolio_images}\n			<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n			{/cf_portfolio_images}\n			{/exp:channel:entries}\n			{/if}\n			\n			{if segment_1=="photography" AND segment_2==""}\n			{exp:channel:entries channel="food|product|location|set" disable="{lv_disable_basic}" dynamic="off" limit="6"}\n			{cf_portfolio_images}\n			<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n			{/cf_portfolio_images}\n			{/exp:channel:entries}\n			{/if}\n			\n			{if segment_1=="digital" AND segment_2==""}\n			{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6"}\n			{cf_portfolio_images}\n			<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n			{/cf_portfolio_images}\n			{/exp:channel:entries}\n			{/if}\n			\n		</ul>\n', '2013-09-24 10:28:54'),
(2, 1, 'lv_sidebar_address', '				 <p>t/ <span class="tel">0161 205 3311</span></p>\n				 <p><a class="email" href="mailto:hello@shoot-the-moon.co.uk">hello@shoot-the-moon.co.uk</a></p>\n				 <p class="adr">\n				  <span class="street-address">Concept House, Naval Street</span>\n				  <span class="locality">Manchester</span>\n				  <span class="postal-code">M4 6AX</span>\n				 </p>\n', '2013-09-24 10:28:54'),
(3, 1, 'lv_image_matrix', '<div id="matrix">\n	<ul>\n	{cf_rotating_image_blocks limit="9"}\n	{if count == 1}\n	<div class="slide-img">\n		{exp:ce_img:single src="{url}" width="440" height="274" crop="yes" allow_scale_larger="yes"}\n	</div>\n	<ul id="slides">\n	{/if}\n	<li>{exp:ce_img:single src="{url}" width="440" height="274" crop="yes" allow_scale_larger="yes"}</li>\n	{if count == total_files}\n	</ul>\n	{/if}\n	{/cf_rotating_image_blocks}\n	</ul>\n</div>', '2013-11-08 14:26:08'),
(4, 1, 'lv_header', '        	<div id="brand">\n        	{if segment_1 =="creative"}\n        	<img src="/resource/stat/stm-creative.png" alt="Shoot the Moon Creative" />\n        	{if:elseif segment_1 =="photography"}\n         	<img src="/resource/stat/stm-photography.png" alt="Shoot the Moon Creative" />\n			{if:elseif segment_1 =="digital"}\n			<img src="/resource/stat/stm-digital.png" alt="Shoot the Moon Creative" />\n			{if:else}\n			<img src="/resource/stat/stm-consultancy.png" alt="Shoot the Moon Creative" />\n        	{/if}\n        	</div>\n        	\n        	<header>\n	        		<div>\n		        		<nav id="main">\n		        			<ul>\n		        				{if segment_1 == ""}\n								<li class="home here ir"><a href="/"><img src="/resource/stat/homeicn.png"></a></li>\n								{if:else}\n								<li class="home ir"><a href="/"><img src="/resource/stat/homeicn.png"></a></li>\n								{/if}\n		        				{exp:structure:nav include_ul="no" exclude="1|5|31" mode="main"}\n		        			</ul>\n		        		</nav>\n		        		<nav id="sub">\n			        		<ul>\n			        			{exp:structure:nav include_ul="no" exclude="1|2|3|4|31|218" mode="main"}\n			        		</ul>\n		        		</nav>\n		        		<p><a href="http://files.shoot-the-moon.co.uk/">Client Login</a></p>\n		        		<ul class="social">\n		        			<li class="gp"><a href="https://plus.google.com/117189057770871609076" rel="publisher">Google+</a></li>\n		        			<li class="fb"><a href="https://www.facebook.com/stmdesignandphotography">Facebook</a></li>\n		        			<li class="tw"><a href="https://twitter.com/sh00tthem00n">Twitter</a></li>\n		        			<li class="in"><a href="http://www.linkedin.com/in/philipjmarshall">Linkedin</a></li>\n		        		</ul> \n		        	</div>\n	        </header>', '2014-01-21 11:14:14'),
(5, 1, 'lv_footer', '				<p>t/ <span class="tel">0161 205 3311</span></p>\n				<p><a class="email" href="mailto:hello@shoot-the-moon.co.uk">hello@shoot-the-moon.co.uk</a></p>\n				<p class="adr">\n					<span class="street-address">Concept House, Naval Street</span>\n					<span class="locality">Manchester</span>\n					<span class="postal-code">M4 6AX</span>\n				</p>\n				<p class="rar"><img src="/resource/stat/rar.gif" alt="RAR Recommended" /><!--<span class="rar-hover">reccomendedagencies.com is an online sourcing tool for marketing and procurement professionals and annual directory</span> --></p>\n', '2013-11-19 15:36:46'),
(6, 1, 'lv_folio_nav', '				<ul id="folio-nav" class="clearfix" current_class="selected">\n					{exp:structure:nav  start_from="/{segment_1}" include_ul="no"}\n				</ul>', '2013-11-08 14:35:53'),
(7, 1, 'lv_doc_header', '<!DOCTYPE html>\n<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->\n<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->\n<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->\n{exp:channel:entries\n        disable="categories|member_data|pagination"\n        dynamic="yes"\n        limit="1"\n        require_entry="yes"\n    }\n    {if no_results}{redirect=''/''}{/if}\n    <head>\n        <meta charset="{charset}">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">\n        <title>Shoot the Moon | {if browser_title}{browser_title}{if:else}{title}{/if}</title>\n        <meta name="description" content="{meta_description}">\n{/exp:channel:entries}\n        <meta name="viewport" content="width=device-width , initial-scale=1.0">\n        <meta name="author" content="{site_name}">\n        <meta name="copyright" content="Copyright {current_time format=''%Y''} Shoot the moon. All rights reserved.">\n        <meta name="google-site-verification" content="JMWnumUj4Q07wR9exYpnCm1i-xpECB4EdvHoQdR8HSU" />\n        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->\n\n        <link rel="stylesheet" href="/resource/css/normalize.css">\n        <link rel="stylesheet" href="/resource/css/main.css">\n        <script src="/resource/js/vendor/modernizr-2.6.2.min.js"></script>\n        \n        \n        <!--[if lt IE 9]>\n                <script src="/resource/js/vendor/selectivizr-min.js"></script>\n                <link rel="stylesheet" href="/resource/css/lt-ie9.css">\n        <![endif]-->\n        \n        \n        <!--[if gte IE 9]>\n          <style type="text/css">\n            .gradient {\n               filter: none;\n            }\n          </style>\n        <![endif]-->     \n        \n        ', '2014-01-15 12:37:16'),
(8, 1, 'lv_chrome_frame', '<!--[if lt IE 7]>\n            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>\n        <![endif]-->', '2013-09-24 10:28:54'),
(9, 1, 'lv_blockquote', '			{exp:channel:entries channel="testimonials" disable="{lv_disable_basic}" dynamic="off"}\n			{testimonials sort="random" limit="1"}\n			<blockquote>\n				<p>{cf_mx_testimonial}</p>\n				<cite>{cf_mx_name}, {cf_mx_company}</cite>\n			</blockquote>\n			{/testimonials}\n			{/exp:channel:entries}', '2013-09-24 10:28:54'),
(10, 1, 'lv_base_scripts', '        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>\n        <script>window.jQuery || document.write(''<script src="/resource/js/vendor/jquery-1.8.2.min.js"><\\/script>'')</script>\n        <script src="/resource/js/plugins.js"></script>\n				<script src="/resource/js/vendor/jquery.scrollTo-1.4.3.1.js"></script>\n				<script src="/resource/js/vendor/jquery.easing.1.3.js"></script>\n				<script src="/resource/js/vendor/stickysidebar.jquery.min.js"></script>\n        <script src="/resource/js/main.js"></script>\n\n        <!-- Google Analytics: change UA-XXXXX-X to be your site''s ID. -->\n        <script>\n            var _gaq=[[''_setAccount'',''UA-17786774-9''],[''_trackPageview'']];\n            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n            g.src=(''https:''==location.protocol?''//ssl'':''//www'')+''.google-analytics.com/ga.js'';\n            s.parentNode.insertBefore(g,s)}(document,''script''));\n        </script>', '2013-09-24 10:28:54'),
(11, 1, 'lv_disable_basic', 'categories|category_fields|member_data|pagination', '2013-09-24 10:28:54');

-- --------------------------------------------------------

--
-- Table structure for table `exp_gmaps_cache`
--

CREATE TABLE IF NOT EXISTS `exp_gmaps_cache` (
  `cache_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(80) NOT NULL DEFAULT '',
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `date` varchar(35) NOT NULL DEFAULT '',
  `geocoder` varchar(15) NOT NULL DEFAULT '',
  `result_object` text,
  PRIMARY KEY (`cache_id`),
  KEY `lat` (`lat`),
  KEY `lng` (`lng`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `exp_gmaps_cache`
--

INSERT INTO `exp_gmaps_cache` (`cache_id`, `address`, `lat`, `lng`, `date`, `geocoder`, `result_object`) VALUES
(1, 'heerde', 52.389042, 6.038935, '1352391516', 'google_maps', 'O:24:"Geocoder\\Result\\Geocoded":14:{s:11:"\0*\0latitude";d:52.38903999999998717385096824727952480316162109375;s:12:"\0*\0longitude";d:6.038934999999998609609974664635956287384033203125;s:9:"\0*\0bounds";a:4:{s:5:"south";d:52.3565291999999971039869706146419048309326171875;s:4:"west";d:5.95096869999999977807192408363334834575653076171875;s:5:"north";d:52.41770930000001271764631383121013641357421875;s:4:"east";d:6.09050609999999981170049068168736994266510009765625;}s:15:"\0*\0streetNumber";N;s:13:"\0*\0streetName";N;s:15:"\0*\0cityDistrict";N;s:7:"\0*\0city";s:6:"Heerde";s:10:"\0*\0zipcode";s:4:"8181";s:9:"\0*\0county";s:6:"Heerde";s:9:"\0*\0region";s:10:"Gelderland";s:13:"\0*\0regionCode";s:2:"GE";s:10:"\0*\0country";s:15:"The Netherlands";s:14:"\0*\0countryCode";s:2:"nl";s:11:"\0*\0timezone";N;}'),
(2, 'manchester', 53.479252, -2.247926, '1352391731', 'google_maps', 'O:24:"Geocoder\\Result\\Geocoded":14:{s:11:"\0*\0latitude";d:53.47925099999999787314663990400731563568115234375;s:12:"\0*\0longitude";d:-2.247926000000000090750518211279995739459991455078125;s:9:"\0*\0bounds";a:4:{s:5:"south";d:53.39994949999999818146534380502998828887939453125;s:4:"west";d:-2.300096900000000221808704736758954823017120361328125;s:5:"north";d:53.54458790000000334430296788923442363739013671875;s:4:"east";d:-2.147087500000000037658764995285309851169586181640625;}s:15:"\0*\0streetNumber";N;s:13:"\0*\0streetName";N;s:15:"\0*\0cityDistrict";N;s:7:"\0*\0city";s:10:"Manchester";s:10:"\0*\0zipcode";N;s:9:"\0*\0county";s:18:"Greater Manchester";s:9:"\0*\0region";s:7:"England";s:13:"\0*\0regionCode";s:7:"ENGLAND";s:10:"\0*\0country";s:14:"United Kingdom";s:14:"\0*\0countryCode";s:2:"gb";s:11:"\0*\0timezone";N;}'),
(61, 'M46AX', 53.485756, -2.226374, '1395834226', 'google_maps', 'O:24:"Geocoder\\Result\\Geocoded":14:{s:11:"\0*\0latitude";d:53.485754700000001093940227292478084564208984375;s:12:"\0*\0longitude";d:-2.226374199999999969890041029429994523525238037109375;s:9:"\0*\0bounds";a:4:{s:5:"south";d:53.48534240000000039572114474140107631683349609375;s:4:"west";d:-2.22712619999999983377847456722520291805267333984375;s:5:"north";d:53.48607390000000094687493401579558849334716796875;s:4:"east";d:-2.22565830000000008936922313296236097812652587890625;}s:15:"\0*\0streetNumber";N;s:13:"\0*\0streetName";N;s:15:"\0*\0cityDistrict";N;s:7:"\0*\0city";s:10:"Manchester";s:10:"\0*\0zipcode";s:6:"M4 6AX";s:9:"\0*\0county";s:18:"Greater Manchester";s:9:"\0*\0region";N;s:13:"\0*\0regionCode";N;s:10:"\0*\0country";s:14:"United Kingdom";s:14:"\0*\0countryCode";s:2:"gb";s:11:"\0*\0timezone";N;}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_html_buttons`
--

CREATE TABLE IF NOT EXISTS `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_html_buttons`
--

INSERT INTO `exp_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`) VALUES
(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(4, 1, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(5, 1, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img');

-- --------------------------------------------------------

--
-- Table structure for table `exp_layout_publish`
--

CREATE TABLE IF NOT EXISTS `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=150 ;

--
-- Dumping data for table `exp_layout_publish`
--

INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(21, 1, 1, 4, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:5:{s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}}}'),
(22, 1, 6, 4, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:5:{s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}}}'),
(24, 1, 6, 2, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(89, 1, 1, 18, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(90, 1, 6, 18, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(98, 1, 6, 3, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(100, 1, 6, 7, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(101, 1, 1, 8, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(102, 1, 6, 8, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(103, 1, 1, 9, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(104, 1, 6, 9, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(105, 1, 1, 10, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(106, 1, 6, 10, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(107, 1, 1, 14, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(108, 1, 6, 14, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(109, 1, 1, 12, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(110, 1, 6, 12, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');
INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(112, 1, 6, 16, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(114, 1, 6, 15, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(115, 1, 1, 17, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(116, 1, 6, 17, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(123, 1, 1, 20, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(124, 1, 6, 20, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(125, 1, 1, 22, 'a:5:{s:7:"publish";a:3:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(126, 1, 6, 22, 'a:5:{s:7:"publish";a:3:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(128, 1, 6, 19, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(129, 1, 1, 21, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(130, 1, 6, 21, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(132, 1, 6, 23, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(133, 1, 1, 24, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(134, 1, 6, 24, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(135, 1, 1, 16, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(136, 1, 1, 15, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(137, 1, 1, 2, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(138, 1, 1, 3, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');
INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(139, 1, 1, 19, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(140, 1, 1, 23, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(141, 1, 1, 7, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(144, 1, 1, 6, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:6:{s:10:"_tab_label";s:3:"SEO";s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(145, 1, 6, 6, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:6:{s:10:"_tab_label";s:3:"SEO";s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(146, 1, 1, 1, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(147, 1, 6, 1, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(148, 1, 1, 11, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(149, 1, 6, 11, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_reorder_orders`
--

CREATE TABLE IF NOT EXISTS `exp_low_reorder_orders` (
  `set_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  `sort_order` text,
  PRIMARY KEY (`set_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exp_low_reorder_orders`
--

INSERT INTO `exp_low_reorder_orders` (`set_id`, `cat_id`, `sort_order`) VALUES
(1, 0, '|171|200|186|196|195|192|191|190|187|185|184|180|179|181|178|193|175|173|172|174|170|169|168|167|165|166|164|162|163|158|159|157|155|156|189|154|152|153|151|149|148|150|145|147|143|144|142|116|140|141|137|138|139|133|134|130|131|132|127|128|129|126|124|125|120|118|114|115|50|49|194|48|47|198|197|43|41|40|39|38|36|35|34|33|199|14|15|207|208|209|210|213|214|216|221|222|223|224|225|226|227|228|229|234|235|236|247|248|249|250|251|252|253|254|255|256|257|258|259|260|261|262|263|264|265|266|267|268|269|270|271|272|273|274|275|'),
(2, 0, '|190|187|185|186|184|189|116|118|114|115|39|38|221|262|263|264|'),
(3, 0, '|173|172|174|171|170|133|134|130|131|132|127|128|129|126|124|125|41|36|35|34|33|222|270|271|272|273|'),
(4, 0, '|138|180|179|181|175|120|40|141|223|224|225|'),
(5, 0, '|229|209|228|208|213|214|'),
(6, 0, '|233|215|212|232|');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_reorder_sets`
--

CREATE TABLE IF NOT EXISTS `exp_low_reorder_sets` (
  `set_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `set_label` varchar(100) NOT NULL,
  `set_notes` text NOT NULL,
  `new_entries` enum('append','prepend') NOT NULL DEFAULT 'append',
  `clear_cache` enum('y','n') NOT NULL DEFAULT 'y',
  `channels` varchar(255) NOT NULL,
  `cat_option` enum('all','some','one') NOT NULL DEFAULT 'all',
  `cat_groups` varchar(255) NOT NULL,
  `parameters` text NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`set_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_low_reorder_sets`
--

INSERT INTO `exp_low_reorder_sets` (`set_id`, `site_id`, `set_label`, `set_notes`, `new_entries`, `clear_cache`, `channels`, `cat_option`, `cat_groups`, `parameters`, `permissions`) VALUES
(1, 1, 'Portfolio', '', 'append', 'n', '|15|2|3|16|19|7|18|12|10|4|8|14|20|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6MTIwOiJicmFuZGluZ3xjcmVhdGl2ZXxkaWdpdGFsfGRpcmVjdC1tYWlsfGVjcm0tZW1haWx8Zm9vZHxtb2JpbGV8cGFja2FnaW5nfHBlcnNvbmFsfHBob3RvZ3JhcGh5fHByb2R1Y3R8c3RvcmUtY29tbXN8d2Vic2l0ZXMiO30', 'YToxOntpOjY7czoxOiIxIjt9'),
(2, 1, 'Branding', '', 'append', 'n', '|15|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6ODoiYnJhbmRpbmciO30', 'YToxOntpOjY7czoxOiIxIjt9'),
(3, 1, 'Packaging', '', 'append', 'n', '|12|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6OToicGFja2FnaW5nIjt9', 'YToxOntpOjY7czoxOiIwIjt9'),
(4, 1, 'Direct Mail', '', 'append', 'n', '|16|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6MTE6ImRpcmVjdC1tYWlsIjt9', 'YToxOntpOjY7czoxOiIwIjt9'),
(5, 1, 'eCRM', '', 'append', 'n', '|19|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6NDoiZWNybSI7fQ', 'YToxOntpOjY7czoxOiIwIjt9'),
(6, 1, 'Banners', '', 'append', 'n', '|21|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6NzoiYmFubmVycyI7fQ', 'YToxOntpOjY7czoxOiIwIjt9');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_variables`
--

CREATE TABLE IF NOT EXISTS `exp_low_variables` (
  `variable_id` int(6) unsigned NOT NULL,
  `group_id` int(6) unsigned NOT NULL,
  `variable_label` varchar(100) NOT NULL,
  `variable_notes` text NOT NULL,
  `variable_type` varchar(50) NOT NULL,
  `variable_settings` text NOT NULL,
  `variable_order` int(4) unsigned NOT NULL,
  `early_parsing` char(1) NOT NULL DEFAULT 'n',
  `is_hidden` char(1) NOT NULL DEFAULT 'n',
  `save_as_file` char(1) NOT NULL DEFAULT 'n',
  `edit_date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`variable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exp_low_variables`
--

INSERT INTO `exp_low_variables` (`variable_id`, `group_id`, `variable_label`, `variable_notes`, `variable_type`, `variable_settings`, `variable_order`, `early_parsing`, `is_hidden`, `save_as_file`, `edit_date`) VALUES
(1, 1, 'Sidebar Thumbs Nav', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 1, 'y', 'n', 'y', 1384875406),
(2, 1, 'Sidebar Address', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 2, 'y', 'n', 'y', 1384875406),
(3, 1, 'Image Matrix', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 3, 'y', 'n', 'y', 1384875406),
(4, 1, 'Header', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 4, 'y', 'n', 'y', 1384875406),
(5, 1, '', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 5, 'y', 'n', 'y', 1384875406),
(6, 1, '', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 6, 'y', 'n', 'y', 1384875406),
(7, 1, 'Doc Header', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 7, 'y', 'n', 'y', 1384875406),
(8, 1, 'Chrome Frame', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 8, 'y', 'n', 'n', 1384875406),
(9, 1, 'Blockquotes', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 9, 'y', 'n', 'y', 1384875406),
(10, 1, 'Base Scripts', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 10, 'y', 'n', 'y', 1384875406),
(11, 2, 'Basic entries disable paremeters', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 1, 'n', 'n', 'n', 1351245707);

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_variable_groups`
--

CREATE TABLE IF NOT EXISTS `exp_low_variable_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(6) unsigned NOT NULL,
  `group_label` varchar(100) NOT NULL,
  `group_notes` text NOT NULL,
  `group_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_low_variable_groups`
--

INSERT INTO `exp_low_variable_groups` (`group_id`, `site_id`, `group_label`, `group_notes`, `group_order`) VALUES
(1, 1, 'Repeating Elements', '', 0),
(2, 1, 'EE Snippets', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_cols`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `exp_matrix_cols`
--

INSERT INTO `exp_matrix_cols` (`col_id`, `site_id`, `field_id`, `var_id`, `col_name`, `col_label`, `col_instructions`, `col_type`, `col_required`, `col_search`, `col_order`, `col_width`, `col_settings`) VALUES
(1, 1, 2, NULL, 'cf_mx_portfolio_image', 'Image', '', 'assets', 'n', 'n', 0, '10%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjQ6e2k6MDtzOjE6IjQiO2k6MTtzOjE6IjIiO2k6MjtzOjE6IjMiO2k6MztzOjE6IjEiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0='),
(2, 1, 2, NULL, 'cf_mx_supporting_text', 'Supporting Text', '', 'wygwam', 'n', 'n', 1, '70%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30='),
(10, 1, 6, NULL, 'cf_mx_studio_image', 'Image', '', 'assets', 'n', 'n', 0, '10%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjgiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0='),
(11, 1, 6, NULL, 'cf_mx_supporting_text', 'Supporting Text', '', 'wygwam', 'n', 'n', 1, '70%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30='),
(12, 1, 6, NULL, 'cf_mx_discipline', 'Discipline', '', 'pt_dropdown', 'n', 'n', 2, '20%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mzp7czo2OiJQZW9wbGUiO3M6NjoiUGVvcGxlIjtzOjg6IkludGVyaW9yIjtzOjg6IkludGVyaW9yIjtzOjQ6IldvcmsiO3M6NDoiV29yayI7fX0='),
(13, 1, 7, NULL, 'cf_mx_name', 'Name', '', 'text', 'n', 'n', 0, '15%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(14, 1, 7, NULL, 'cf_mx_company', 'Company', '', 'text', 'n', 'n', 1, '15%', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(15, 1, 7, NULL, 'cf_mx_testimonial', 'Testimonial Text', '', 'text', 'n', 'n', 2, '70%', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(16, 1, 9, NULL, 'version_m4v', 'M4v Version', '', 'assets', 'n', 'n', 0, '33%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo0OiJsaXN0IjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO319'),
(17, 1, 9, NULL, 'version_webM', 'WebM Version', '', 'assets', 'n', 'n', 1, '', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo0OiJsaXN0IjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO319'),
(18, 1, 9, NULL, 'version_ogv', 'OGV Version', '', 'assets', 'n', 'n', 2, '', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo0OiJsaXN0IjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO319'),
(19, 1, 9, NULL, 'start_image', 'Start Image', '', 'assets', 'n', 'n', 3, '', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_data`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` text,
  `col_id_13` text,
  `col_id_14` text,
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=573 ;

--
-- Dumping data for table `exp_matrix_data`
--

INSERT INTO `exp_matrix_data` (`row_id`, `site_id`, `entry_id`, `field_id`, `var_id`, `row_order`, `col_id_1`, `col_id_2`, `col_id_10`, `col_id_11`, `col_id_12`, `col_id_13`, `col_id_14`, `col_id_15`, `col_id_16`, `col_id_17`, `col_id_18`, `col_id_19`) VALUES
(3, 1, 4, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 1, 1, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 1, 10, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 1, 2, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 1, 11, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 1, 12, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 1, 13, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 1, 3, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 1, 14, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 1, 15, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 1, 16, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 1, 17, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 1, 18, 2, NULL, 1, '', '<p>\n	It has been said that astronomy is a humbling and character-building experience. There is perhaps no better demonstration of the folly of human conceits than this distant image of our tiny world. To me, it underscores our responsibility to deal more kindly with one another, and to preserve and cherish the pale blue dot, the only home we&#39;ve ever known.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 1, 18, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 1, 19, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 1, 20, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 1, 21, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 1, 18, 2, NULL, 2, '', '<p>\n	Competent means we will never take anything for granted. We will never be found short in our knowledge and in our skills. Mission Control will be perfect. When you leave this meeting today you will go to your office and the first thing you will do there is to write &#39;Tough and Competent&#39; on your blackboards.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 1, 19, 2, NULL, 1, '', '<p>\n	The size and age of the Cosmos are beyond ordinary human understanding. Lost somewhere between immensity and eternity is our tiny planetary home.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 1, 19, 2, NULL, 2, '', '<p>\n	The Earth is a very small stage in a vast cosmic arena. Think of the rivers of blood spilled by all those generals and emperors so that, in glory and triumph, they could become the momentary masters of a fraction of a dot.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 1, 20, 2, NULL, 1, '', '<p>\n	It&#39;s just mind-blowingly awesome. I apologize, and I wish I was more articulate, but it&#39;s hard to be articulate when your mind&#39;s blown&mdash;but in a very good way.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 1, 20, 2, NULL, 2, '', '<p>\n	First, I believe that this nation should commit itself to achieving the goal, before this decade is out, of landing a man on the moon and returning him safely to the earth.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 1, 5, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 1, 31, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 1, 31, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'Lorna Culican', 'Sayers the Bakers', 'Innovative, friendly and totally committed to providing the best designs and ideas — just a few words to describe the team at Shoot the Moon. We can be confident that they will do all they can to ensure the project reaches us on time, will be of the highest standard, and will be to budget.', NULL, NULL, NULL, NULL),
(89, 1, 31, 7, NULL, 2, NULL, NULL, NULL, NULL, NULL, 'Karen Scott', 'Macphie of Glenbervie', 'Brace yourself for jaw-dropping design delivered on-time and on-budget. They are a very friendly, professional team, we enjoy working with them.', NULL, NULL, NULL, NULL),
(90, 1, 31, 7, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'Natalie Hughes', 'Pets at Home', 'We have worked with Shoot the Moon on a variety of projects over the last three years — they have produced some excellent creative for us. Faced with consistently tight deadlines, their resourcefulness really shows through. This, combined with their broad experience makes them a great team to work with.', NULL, NULL, NULL, NULL),
(91, 1, 31, 7, NULL, 4, NULL, NULL, NULL, NULL, NULL, 'Paul Broster', 'Meadow Vale Foods', 'Their creative, technical knowledge and understanding of the food industry makes them very easy to work with and has delivered some excellent results. The STM team have a real enthusiasm for getting it right — great creative, pace and value for money.', NULL, NULL, NULL, NULL),
(92, 1, 32, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 1, 33, 2, NULL, 1, '{filedir_2}packaging/Fish-mongers.jpg', '<p>\n	<span style="color:#d3d3d3;">Packaging for Fisherman&#39;s Finest dog food range, found in Pets at Home.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 1, 33, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 1, 34, 2, NULL, 1, '{filedir_2}packaging/Humdingers.jpg', '<p>\n	<span style="color:#d3d3d3;">Packaging refresh for Humdingers&#39; Fruit Factory fruit snacks range - concept development</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 1, 34, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 1, 35, 2, NULL, 1, '{filedir_2}packaging/count-down.jpg', '<p>\n	<span style="color:#d3d3d3;">Packaging, name generation and branding concept for lower calorie biscuits.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 1, 35, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 1, 36, 2, NULL, 1, '{filedir_2}packaging/Advanced-nut-dog.jpg', '<p>\n	<span style="color:#d3d3d3;">Pets at Home&#39;s popular Advanced Nutrition food is specially formulated to meet the unique nutritional needs of critical stages of pets&#39; development.&nbsp;Branding, tiered packaging and advertising were developed for the range.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 1, 36, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 1, 37, 2, NULL, 1, '{filedir_2}stm-brand-portfolio-2012-25.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 1, 37, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 1, 38, 2, NULL, 1, '{filedir_2}Brand/Hewbys.jpg', '<p>\n	<span style="color:#d3d3d3;">Baked goods brand, specialising in ambient products for travel retail</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 1, 38, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 1, 39, 2, NULL, 1, '{filedir_2}Brand/Harbour-Smokehouse.jpg', '<p>\n	<span style="color:#d3d3d3;">Branding for premuim Scottish Salmon brand</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 1, 39, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 1, 40, 2, NULL, 1, '{filedir_2}dm/Dissoto.jpg', '<p>\n	<span style="color:#d3d3d3;">Disotto Foods brochure</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 1, 40, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 1, 41, 2, NULL, 1, '{filedir_2}dm/3-peaks.jpg', '<p>\n	<span style="color:#d3d3d3;">Packaging for Pets at Home&#39;s range of accessories for dogs who enjoy the great outdoors.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 1, 41, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 1, 42, 2, NULL, 1, '{filedir_2}store-comms/Bird.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 1, 42, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 1, 43, 2, NULL, 1, '{filedir_4}Advanced_Nutrition.jpg', '<p>\n	<span style="color:#d3d3d3;">In-store navigation and key information supports Pets at Home&#39;s Advanced Nutrition food range.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 1, 43, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 1, 44, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0220.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 1, 45, 6, NULL, 1, NULL, NULL, '{filedir_8}studio_3.jpg', '', 'Interior', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 1, 46, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0283.JPG', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 1, 31, 7, NULL, 5, NULL, NULL, NULL, NULL, NULL, 'Theo Van Der Lugt', 'Alpha LSG', 'STM are a pleasure to work with: creative, responsive and very down-to-earth. Straight talking and a focus on the objectives, rather than the mechanics."', NULL, NULL, NULL, NULL),
(119, 1, 31, 7, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'Stephanie Mansfield', 'Lakeland Limited', 'Lakeland has worked with Shoot The Moon for many years as we can always rely on them to pull out all the stops to ensure we meet our deadlines... even if they are very tight! I''ve always found the team friendly, helpful and professional. Shoot The Moon are a pleasure to work with and we look forward to producing many more publications with them.', NULL, NULL, NULL, NULL),
(120, 1, 31, 7, NULL, 7, NULL, NULL, NULL, NULL, NULL, ' Paul Lowings', 'Lakeland Limited', 'We have enjoyed working with Shoot the Moon for a number of years now. The relationship is very warm and friendly - they’re almost like an extension of our in-house marketing team. \nNothing is ever too much trouble and they always impress with how highly responsive they are to any short-term ad hoc needs, whilst keeping our longer term projects moving along on track and within budget. An absolute pleasure to work with!', NULL, NULL, NULL, NULL),
(121, 1, 31, 7, NULL, 8, NULL, NULL, NULL, NULL, NULL, 'Lucy Evans', '', '“Great to work with”, "passionate about perfection”, "Extremely creative”, "Great sense of style”, "No challenge is too great”, "Creates a fun and inspirational working environment.”', NULL, NULL, NULL, NULL),
(122, 1, 31, 7, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'Kelly Eastwood', 'Greencore', 'We have used STM for projects that needed to communicate real foodie values through creative and visually stunning photography - the results have always surpassed our expectations. ', NULL, NULL, NULL, NULL),
(123, 1, 31, 7, NULL, 10, NULL, NULL, NULL, NULL, NULL, 'Joanna Watling', 'Princes Food & Drink Group', 'With an ability to interpret any brief, deliver stunning imagery for use in a wide variety of applications and all at a competitive rate, STM constantly exceed my expectations and make my job easier.', NULL, NULL, NULL, NULL),
(124, 1, 31, 7, NULL, 11, NULL, NULL, NULL, NULL, NULL, 'Fiona Scott', 'Threebrand', 'Great food photography, great attention to detail, great result! Many thanks for a professional job well done.', NULL, NULL, NULL, NULL),
(129, 1, 49, 2, NULL, 1, '{filedir_3}devilish-home.jpg', '<p>\n	<span style="color:#d3d3d3;">Devilishly Delicious dessert brand site</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 1, 49, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 1, 50, 2, NULL, 1, '{filedir_3}active-procurement.jpg', '<p>\n	<span style="color:#a9a9a9;">New site for Active Procurement</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 1, 50, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(241, 1, 105, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0224.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(242, 1, 106, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0228.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, 1, 107, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0230.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(244, 1, 108, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0279.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, 1, 109, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_6820.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, 1, 110, 6, NULL, 1, NULL, NULL, '{filedir_8}photography-studio.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, 1, 111, 6, NULL, 1, NULL, NULL, '{filedir_8}studio_1.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, 1, 112, 6, NULL, 1, NULL, NULL, '{filedir_8}studio_2.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(249, 1, 113, 6, NULL, 1, NULL, NULL, '{filedir_8}studio_3.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(250, 1, 114, 2, NULL, 1, '{filedir_2}Brand/Montana.jpg', '<p>\n	<span style="color:#a9a9a9;">Established British chocolate biscuit brand goes chunky thanks to Shoot the Moon</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, 1, 114, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, 1, 115, 2, NULL, 1, '{filedir_2}Brand/Nuba.jpg', '<p>\n	<span style="color:#d3d3d3;">Brand identity, name generation, packaging, website and advertising was completed for Nuba fresh bottled cocktails. The negative space cocktail glass which appears within the word is adapted for each flavour in the range.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, 1, 115, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(254, 1, 116, 2, NULL, 1, '{filedir_2}Brand/Scorpio-logo.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(255, 1, 116, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(258, 1, 118, 2, NULL, 1, '{filedir_2}Brand/Trophy-logo.jpg', '<p>\n	<span style="color:#d3d3d3;">Trophy biscuit range - Rivington Biscuits</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(259, 1, 118, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(262, 1, 120, 2, NULL, 1, '{filedir_4}josefmeiers.jpg', '<p>\n	<span style="color:#d3d3d3;">Shoot the Moon created surreal characters to feature in a campaign for a new Krauter Likor (herbal liqueur) brand, named Josef Meiers. These included a cross dressing wolf and a goth gnome. Advertising comprised of promotional items, posters and a branded photo opportunity wall of lifesize characters, which bar customers were encouraged to placed their heads, photograph and share themselves in Josef&#39;s world. Cocktail recipe books were given to bar staff and plaster cast gnomes were produced as prizes and giveaways.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(263, 1, 120, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(270, 1, 124, 2, NULL, 1, '{filedir_2}packaging/John-west.jpg', '<p>\n	<span style="color:#d3d3d3;">Infusions range range for John West Salmon</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(271, 1, 124, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, 1, 125, 2, NULL, 1, '{filedir_2}packaging/jucee.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, 1, 125, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(274, 1, 126, 2, NULL, 1, '{filedir_2}packaging/koko_noir_2.jpg', '<p>\n	<span style="color:#d3d3d3;">Koko Noir, an artisan chocolate maker, needed clear, simple branding and packaging with a straightforward range distinction. Bespoke illustrations were drawn for the range&#39;s amazing flavour combinations. Each piece includes idiosyncratic quotes and life advice for fellow chocolate lovers.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(275, 1, 126, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(276, 1, 127, 2, NULL, 1, '{filedir_2}packaging/Kuli.jpg', '<p>\n	<span style="color:#d3d3d3;">Branding and packaging for ku-li&reg;, Macphie of Glenbervie&#39;s range of fruit coulis.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(277, 1, 127, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(278, 1, 128, 2, NULL, 1, '{filedir_2}packaging/laziz.jpg', '<p>\n	<span style="color:#d3d3d3;">Laziz frozen chicken range branding and packaging.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(279, 1, 128, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(280, 1, 129, 2, NULL, 1, '{filedir_2}packaging/meadowvale.jpg', '<p>\n	<span style="color:#d3d3d3;">Branding and packaging for range extensions of Meadow Vale&#39;s chicken products.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(281, 1, 129, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(282, 1, 130, 2, NULL, 1, '{filedir_2}packaging/Molpol.jpg', '<p>\n	<span style="color:#d3d3d3;">Harbour Smokehouse Co. Scottish Salmon packaging for French market. The&nbsp;</span><span style="color: rgb(211, 211, 211);">sought-after&nbsp;</span><span style="color: rgb(211, 211, 211);">Label Rouge mark indicates the authenticity of the produce&#39;s location. Spot silver ink on the pack&#39;s salmon skin texture gives this simple pack a premium feel.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(283, 1, 130, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(284, 1, 131, 2, NULL, 1, '{filedir_2}packaging/Montana-chun.jpg', '<p>\n	<span style="color:#d3d3d3;">Rebrand for established biscuit range</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(285, 1, 131, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(286, 1, 132, 2, NULL, 1, '{filedir_2}packaging/Montana.jpg', '<p>\n	<span style="color:#d3d3d3;">Branding and packaging for all natural, award winning crisps</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(287, 1, 132, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(288, 1, 133, 2, NULL, 1, '{filedir_2}packaging/Morrisons.jpg', '<p>\n	<span style="color:#d3d3d3;">Concept renders for Morrisons range</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, 1, 133, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(290, 1, 134, 2, NULL, 1, '{filedir_2}packaging/Vimto.jpg', '<p>\n	<span style="color:#d3d3d3;">This Vimto multipack uses key elements to secure brand consistency and recognition.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, 1, 134, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, 1, 135, 2, NULL, 1, '{filedir_2}store-comms/Dickies.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(293, 1, 135, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(296, 1, 137, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, 1, 137, 2, NULL, 1, '{filedir_2}store-comms/Pet-club.jpg', '<p>\n	<span style="color:#d3d3d3;">Pets at Home&#39;s Kids&#39; Holiday Pet Club have quiz trails to encourage children and families to visit the entire store during school holidays.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(298, 1, 138, 2, NULL, 1, '{filedir_2}store-comms/Ryman-Furniture-Range-Brochure.jpg', '<p>\n	<span style="color:#d3d3d3;">Ryman Furniture Showroom</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, 1, 138, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, 1, 139, 2, NULL, 1, '{filedir_2}store-comms/Ryman-Furniture-Range-POS.jpg', '<p>\n	<span style="color:#d3d3d3;">Ryman Office Furniture Showroom navigation</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, 1, 139, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(302, 1, 140, 2, NULL, 1, '{filedir_2}store-comms/Ryman-Store-Graphics.jpg', '<p>\n	<span style="color:#d3d3d3;">Ryman In-store navigation and POS for the new technology centre and business services hub</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, 1, 140, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(304, 1, 141, 2, NULL, 1, '{filedir_2}dm/store-open.jpg', '<p>\n	<span style="color:#d3d3d3;">Pets at Home - Store Openings</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(305, 1, 141, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, 1, 142, 2, NULL, 1, '{filedir_4}east.jpg', '<p>\n	<span style="color:#a9a9a9;">Enticing Eastern dishes for various clients</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(307, 1, 142, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, 1, 143, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, 1, 144, 2, NULL, 1, '{filedir_1}oysters.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, 1, 144, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, 1, 147, 2, NULL, 1, '{filedir_4}dessert.jpg', '<p>\n	<span style="color:#a9a9a9;">Examples of desserts shot for a variety of clients.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(317, 1, 147, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(318, 1, 148, 2, NULL, 1, '{filedir_4}food_for_packaging.jpg', '<p>\n	<span style="color:#a9a9a9;">We often shoot food which has to work across packaging ranges, supporting campaigns and editorials.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, 1, 148, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(320, 1, 149, 2, NULL, 1, '{filedir_4}test_custard.jpg', '<p>\n	<span style="color:#a9a9a9;">Inspired direction, smart styling, evocative lighting knowledge and a little bit of skilled retouch (where needed), helps when inviting viewers to imagine the glorious taste.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(321, 1, 149, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(322, 1, 150, 2, NULL, 1, '{filedir_4}food-pack-fronts.jpg', '<p>\n	<span style="color:#a9a9a9;">Examples of food photography used on packaging and advertising.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(323, 1, 150, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(324, 1, 151, 2, NULL, 1, '{filedir_4}Trove-jar-shot.jpg', '<p>\n	<span style="color:#a9a9a9;">Example of images made for Trove, who specialise in real bread and organic preserves.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(325, 1, 151, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(328, 1, 153, 2, NULL, 1, '{filedir_4}food-pr.jpg', '<p>\n	<span style="color:#a9a9a9;">Examples of expressive food shots used in PR, editorials and advertising.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(329, 1, 153, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(331, 1, 154, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(332, 1, 155, 2, NULL, 1, '{filedir_4}Thomson-Airways-gifts.jpg', '<p>\n	<span style="color:#a9a9a9;">Gift photography featured in Thomson Airways inflight magazines.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(333, 1, 155, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(334, 1, 156, 2, NULL, 1, '{filedir_4}Thomson-Airways-beauty.jpg', '<p>\n	<span style="color:#a9a9a9;">Branded beauty and fragrance product shots are carefully directed to be harmonious with both the style of Thomson Airways inflight magazine and the brand&#39;s image.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(335, 1, 156, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(337, 1, 157, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(338, 1, 158, 2, NULL, 1, '{filedir_1}hoover.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(339, 1, 158, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(340, 1, 159, 2, NULL, 1, '{filedir_1}red-kettel.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(341, 1, 159, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(356, 1, 167, 2, NULL, 1, '{filedir_4}authentic-cocktail-comp.jpg', '<p>\n	<span style="color:#a9a9a9;">Manchester Drinks Co. pre-mixed cocktails in a can.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(357, 1, 167, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(358, 1, 168, 2, NULL, 1, '{filedir_1}_Almond-Extract.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(359, 1, 168, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(360, 1, 169, 2, NULL, 1, '{filedir_2}store-comms/Pick-n-Mix-POS.jpg', '<p>\n	<span style="color:#d3d3d3;">Pick &#39;n&#39; Mix small animal accessories brand POS at Pets at Home</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(361, 1, 169, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(362, 1, 170, 2, NULL, 1, '{filedir_2}packaging/Marriages.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(363, 1, 170, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(364, 1, 171, 2, NULL, 1, '{filedir_2}packaging/HarbourSmokehouse-HighlandPark.jpg', '<p>\n	<span style="color:#d3d3d3;">Harbour Smokehouse Co. asked Shoot the Moon to create branding and premium packaging for a luxury salmon range which is slow smoked using Highland Park single malt whisky barrels. Based on the Harbour Smokehouse Co. identity, the packaging employs a die cut product reveal and special inks associated with the finest quality whisky and Scottish salmon.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(365, 1, 171, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(366, 1, 172, 2, NULL, 1, '{filedir_2}packaging/Devilishly_Delicious_300ml.jpg', '<p>\n	<span style="color:#d3d3d3;">Branding and packaging of Devilishly Delicious indulgent range of decadent desserts.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(367, 1, 172, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(368, 1, 173, 2, NULL, 1, '{filedir_2}packaging/Disotto_NY_Deli_Tubs.jpg', '<p>\n	<span style="color:#d3d3d3;">Vintage New York City images help this ice cream packaging in Brighton&#39;s New York Coffee Club</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(369, 1, 173, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(370, 1, 174, 2, NULL, 1, '{filedir_2}packaging/pinkpanther.jpg', '<p>\n	<span style="color:#d3d3d3;">Identity usage guidelines ensure the Pink Panther remains perfectly in character on this biscuit range packaging</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(371, 1, 174, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(372, 1, 175, 2, NULL, 1, '{filedir_2}dm/Lakeland-1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(373, 1, 175, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(378, 1, 178, 2, NULL, 1, '{filedir_2}dm/Lakeland_Win12_Option_2v2.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(379, 1, 178, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(381, 1, 179, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(382, 1, 180, 2, NULL, 1, '{filedir_2}dm/Thomson-Inflight01.jpg', '<p>\n	<span style="color:#d3d3d3;">Thomson Airways&#39; inflight retail magazine demonstrates Shoot the Moon&#39;s design, art direction, photography, layout and press-ready artwork expertise, used to deliver on time and within budget.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(383, 1, 180, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(384, 1, 181, 2, NULL, 1, '{filedir_2}dm/Thomson_Win12_Option_1.jpg', '<p>\n	<span style="color:#d3d3d3;">Thomson Airways inflight magazine - Beauty, gifts and fragrance at 30,000ft</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(385, 1, 181, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(392, 1, 185, 2, NULL, 1, '{filedir_4}KHPC.jpg', '<p>\n	<span style="color:#d3d3d3;">Pets at Home run the free Kids Holiday Pet Club activities during each school holiday in their stores. There are puzzle books, stickers, pet workshops and quiz trails to add to the fun.&nbsp;</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(393, 1, 185, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(394, 1, 186, 2, NULL, 1, '{filedir_2}Brand/Vero_Gelato.jpg', '<p>\n	<span style="color:#d3d3d3;">Branding for DiSotto&#39;s traditional Italian dessert range was applied across packaging, online and offline advertising</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(395, 1, 186, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(396, 1, 187, 2, NULL, 1, '{filedir_2}Brand/Poundbakery.jpg', '<p>\n	<span style="color: rgb(169, 169, 169);">Pound Bakery - Tasty Baking at Tasty Prices<br />\n	This budget bakery chain is mostly enjoyed across the North West</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(397, 1, 187, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(399, 1, 189, 2, NULL, 1, '{filedir_2}Brand/Sayers.jpg', '<p>\n	<span style="color:#d3d3d3;">Branding for high street bakery based mostly in the North West of England</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(400, 1, 189, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(401, 1, 190, 2, NULL, 1, '{filedir_2}Brand/Goodness_Me.jpg', '<p>\n	<span style="color:#d3d3d3;">Branding for Meadow Vale Foods&#39; higher tier chicken range.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(402, 1, 190, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(403, 1, 191, 2, NULL, 1, '{filedir_1}product/spain-shirt.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(404, 1, 191, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, 1, 31, 7, NULL, 12, NULL, NULL, NULL, NULL, NULL, 'Alan Hayes', 'Alpha LSG', 'The Shoot the Moon team always embrace our ideas with such creativity and are spot-on with trends in the digital and print marketing. Speed of delivery is always impressive', NULL, NULL, NULL, NULL),
(406, 1, 31, 7, NULL, 13, NULL, NULL, NULL, NULL, NULL, '', '', '"Thank you all very much for your hard work at such short notice. Really appreciated!"', NULL, NULL, NULL, NULL),
(407, 1, 31, 7, NULL, 14, NULL, NULL, NULL, NULL, NULL, '', '', '"This is bloody amazing.  I LOVE LOVE LOVE it! Thank you once again!"', NULL, NULL, NULL, NULL),
(408, 1, 31, 7, NULL, 15, NULL, NULL, NULL, NULL, NULL, '', '', '"Just wanted to say thanks to you and your designers for all your work over the last few weeks, we’ve not had a lot of time and there have been a lot of amends at short notice so just wanted to say thanks and have a good long weekend..." ', NULL, NULL, NULL, NULL),
(409, 1, 31, 7, NULL, 16, NULL, NULL, NULL, NULL, NULL, '', '', '"Thanks for the help today, you have inspired us all..."', NULL, NULL, NULL, NULL),
(410, 1, 31, 7, NULL, 17, NULL, NULL, NULL, NULL, NULL, 'Anna Massie', 'Macphie of Glenbervie', '"Great shelf stand out and presence on fixture, thanks to all for all your work on this project to date."', NULL, NULL, NULL, NULL),
(411, 1, 31, 7, NULL, 18, NULL, NULL, NULL, NULL, NULL, '', '', '"We’ve just received all the brochures – they look great, thanks to everyone for all your hard work."', NULL, NULL, NULL, NULL),
(412, 1, 31, 7, NULL, 19, NULL, NULL, NULL, NULL, NULL, 'Catriona Marshall', 'Pets at Home', '"Just want to say a BIG well done for all the fantastic work you and your team have done on Advanced Nutrition." ', NULL, NULL, NULL, NULL),
(413, 1, 192, 2, NULL, 1, '{filedir_3}caterforce.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(414, 1, 192, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(415, 1, 193, 2, NULL, 1, '{filedir_3}chefs-selections.jpg', '<p>\n	<span style="color:#a9a9a9;">Caterforce&nbsp;Chefs Selections site</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(416, 1, 193, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(417, 1, 194, 2, NULL, 1, '{filedir_3}devilish-facebook.jpg', '<p>\n	<span style="color:#a9a9a9;">Devilishly Delicious Facebook Creative</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(418, 1, 194, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(419, 1, 195, 2, NULL, 1, '{filedir_3}macphie-homepage.jpg', '<p>\n	<span style="color:#d3d3d3;">new site for macphie.com</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(420, 1, 195, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(421, 1, 196, 2, NULL, 1, '{filedir_3}marriages.jpg', '<p>\n	<span style="color:#a9a9a9;">Marriages site creative</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(422, 1, 196, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(423, 1, 197, 2, NULL, 1, '{filedir_3}meadowvale.jpg', '<p>\n	<span style="color:#a9a9a9;">Meadow Vale Foods site</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(424, 1, 197, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(427, 1, 199, 2, NULL, 1, '{filedir_3}sayers-screen.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(428, 1, 199, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(429, 1, 200, 2, NULL, 1, '{filedir_3}scorpio-screen.jpg', '<p>\n	<span style="color:#d3d3d3;">New site for Scorpio Worldwide with buyer ordering facility - part of a major rebrand and expansion</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(430, 1, 200, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(433, 1, 202, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(434, 1, 203, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(435, 1, 204, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}bat-and-ball.m4v', '{filedir_3}bat-and-ball.webm', '{filedir_3}bat-and-ball.ogv', '{filedir_3}bat-and-ball.jpg'),
(436, 1, 205, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}nets.m4v', '{filedir_3}nets.webm', '{filedir_3}nets.ogv', '{filedir_3}nets.jpg'),
(437, 1, 206, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Jucee_Umbrella.m4v', '{filedir_3}Jucee_Umbrella.webm', '{filedir_3}Jucee_Umbrella.ogv', '{filedir_3}Jucee_Umbrella.jpg'),
(438, 1, 207, 2, NULL, 1, '{filedir_3}two_meadowvale.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(439, 1, 207, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(440, 1, 208, 2, NULL, 1, '{filedir_3}tcx-combined.jpg', '<p>\n	<span style="color:#a9a9a9;">Fly Thomas Cook email</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(441, 1, 208, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(442, 1, 209, 2, NULL, 2, '{filedir_3}pets.jpg', '<p>\n	<span style="color:#a9a9a9;">Pets at Home promotional email</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(443, 1, 209, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(446, 1, 211, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(447, 1, 212, 2, NULL, 1, '{filedir_3}pah-banners-updated.jpg', '<p>\n	<span style="color:#d3d3d3;">Banners help to generate awareness of events and promotions for Pets at Home</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(448, 1, 212, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(449, 1, 213, 2, NULL, 1, '{filedir_3}ryman.jpg', '<p>\n	<span style="color:#a9a9a9;">Ryman email</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(450, 1, 213, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(451, 1, 214, 2, NULL, 1, '{filedir_3}pound-bakery.jpg', '<p>\n	<span style="color:#a9a9a9;">Pound Bakery email</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(452, 1, 214, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(453, 1, 215, 2, NULL, 1, '{filedir_3}mixed-banners.jpg', '<p>\n	<span style="color:#d3d3d3;">Further examples of banner advertising - Caterforce Chef Selections, Sayers the Bakers and Devilishly Delicious</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(454, 1, 215, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(455, 1, 216, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(456, 1, 217, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(457, 1, 218, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(458, 1, 219, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(459, 1, 220, 2, NULL, 1, '{filedir_1}_Almond-Extract.jpg', '<p>\n	Some text goes here</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(460, 1, 220, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(461, 1, 221, 2, NULL, 1, '{filedir_4}rob_winstanley.jpeg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(462, 1, 221, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(463, 1, 222, 2, NULL, 1, '{filedir_4}Jucee.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(464, 1, 222, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(465, 1, 223, 2, NULL, 1, '', '<p>\n	<span style="color:#d3d3d3;">Ryman Catalogue</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(466, 1, 223, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(468, 1, 224, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(470, 1, 225, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(472, 1, 226, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(474, 1, 227, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(475, 1, 209, 2, NULL, 1, '{filedir_3}pah-ecrm.jpg', '<p>\n	<span style="color:#a9a9a9;">Pets at Home store opening email</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(476, 1, 228, 2, NULL, 1, '{filedir_3}jucee-ecrm.jpg', '<p>\n	Jucee styled eCRM</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(477, 1, 228, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(478, 1, 229, 2, NULL, 1, '{filedir_3}ride-away-ecrm.jpg', '<p>\n	<span style="color:#a9a9a9;">Ride-Away Equestrian emails.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(479, 1, 229, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(480, 1, 230, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(481, 1, 231, 2, NULL, 1, '{filedir_4}Thomson-Crew-Comms.jpg', '<p>\n	The BASE crew portal was developed for Thomson Airways to update and advise crew on changes to the in-flight retail range. The portal also provides opportunities for training and crew communication allowing user feedback and increased operational fluidity. Tailored to the user&#39;s operational base and routes, the portal is responsive and is available on iOS, Android, Windows and desktop browsers.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(482, 1, 231, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(483, 1, 232, 2, NULL, 1, '{filedir_3}aunt-bessies-campaign.jpg', '<p>\n	<span style="color:#d3d3d3;">Examples of banner advertising as part of a major rebrand and advertising campaign for Aunt Bessie&#39;s&nbsp;</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(484, 1, 232, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(485, 1, 233, 2, NULL, 1, '{filedir_3}ryman-escalator-creative.jpg', '<p>\n	Promotional banners</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(486, 1, 233, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(489, 1, 235, 2, NULL, 1, '{filedir_3}rob-winstanley.jpg', '<p>\n	As part of a brand refresh by Shoot the Moon, established cycle retailer Rob Winstanley Cycles now has a new site which effectively communicates the links to the esteemed Specialized brand.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(490, 1, 235, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(491, 1, 236, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(492, 1, 237, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(493, 1, 238, 2, NULL, 1, '{filedir_1}location/HapeToys.jpg', '<p>\n	<span style="color:#a9a9a9;">Location shoot for Hape Toys</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(494, 1, 238, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(495, 1, 239, 2, NULL, 1, '{filedir_4}frankie-and-benny-leeds.jpg', '<p>\n	<span style="color:#a9a9a9;">Location shoot for Frankie &amp; Benny&#39;s diner</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(496, 1, 239, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(497, 1, 240, 2, NULL, 1, '{filedir_4}vimto-pr.jpg', '<p>\n	<span style="color:#a9a9a9;">Location shoot for Vimto campaign.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(498, 1, 240, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(499, 1, 241, 2, NULL, 1, '{filedir_4}Photography_location_europe.jpg', '<p>\n	<span style="color:#a9a9a9;">Example of European location shots&nbsp;</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(500, 1, 241, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(501, 1, 242, 2, NULL, 1, '{filedir_4}Chiquito.jpg', '<p>\n	<span style="color:#a9a9a9;">On location for UK-wide Mexican food restaurants,&nbsp;Chiquito.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(502, 1, 242, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(503, 1, 243, 2, NULL, 1, '{filedir_4}TryThai.jpg', '<p>\n	<span style="color:#d3d3d3;">Location shoot at&nbsp;Try Thai, a modern and vibrant Thai restaurant located in the heart of Manchester&#39;s Chinatown.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(504, 1, 243, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(505, 1, 244, 2, NULL, 1, '{filedir_4}Photography_location_manchester.jpg', '<p>\n	<span style="color:#a9a9a9;">Shoot the Moon has covered Manchester events such as&nbsp;Eurocultured, Manchester Parade and the Band On The Wall Mooch Project</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(506, 1, 244, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(507, 1, 245, 2, NULL, 1, '{filedir_4}Photography_location_1137a.jpg', '<p>\n	<span style="color:#a9a9a9;">Examples of evocative, stimulating location images shot by Shoot the Moon&#39;s photographers.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(508, 1, 245, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(509, 1, 246, 2, NULL, 1, '{filedir_4}Hour-Glass.jpg', '<p>\n	<span style="color:#a9a9a9;">Location shoot for Hourglass Caf&eacute; Bar</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(510, 1, 246, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(511, 1, 247, 2, NULL, 1, '{filedir_4}JDWilliams.jpg', '<p>\n	<span style="color:#a9a9a9;">Product photography featured in JD Williams catalogues.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(512, 1, 247, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(513, 1, 248, 2, NULL, 1, '{filedir_4}Thomson_Photography_product_spirits.jpg', '<p>\n	<span style="color:#a9a9a9;">Drinks promotions in Thomson Airways&#39; inflight magazine</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(514, 1, 248, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(515, 1, 249, 2, NULL, 1, '{filedir_4}Photography_product_pets_home_fashion.jpg', '<p>\n	<span style="color:#a9a9a9;">The pets who visit our studio on behalf of Pets at Home are well attended to and</span><span style="color: rgb(169, 169, 169);">, as much as possible,&nbsp;</span><span style="color: rgb(169, 169, 169);">each shoot is carefully planned.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(516, 1, 249, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(517, 1, 250, 2, NULL, 1, '{filedir_4}chicken.jpg', '<p>\n	<span style="color:#a9a9a9;">Our food stylists ensure that your food products are correctly prepared for your photography requirements.&nbsp;</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(518, 1, 250, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(519, 1, 251, 2, NULL, 1, '{filedir_4}breakfast.jpg', '<p>\n	<span style="color:#a9a9a9;">Redolent lighting examples.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(520, 1, 251, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(521, 1, 252, 2, NULL, 1, '{filedir_4}Napolina-pasta-recipe.jpg', '<p>\n	<span style="color:#a9a9a9;">Napolina pasta shoot</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(522, 1, 252, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(523, 1, 253, 2, NULL, 1, '{filedir_4}the_restaurant_group.jpg', '<p>\n	<span style="color:#a9a9a9;">Selection of images produced for The Restaurant Group plc.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(524, 1, 253, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(525, 1, 254, 2, NULL, 1, '{filedir_4}wahaca.jpg', '<p>\n	<span style="color:#a9a9a9;">Shoot for Wahaca Mexican Market Eating, with locations across London.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(526, 1, 254, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(527, 1, 255, 2, NULL, 1, '{filedir_4}test_produce.jpg', '<p>\n	<span style="color:#a9a9a9;">Creating perfect food is artistry in itself</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(528, 1, 255, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(529, 1, 256, 2, NULL, 1, '{filedir_4}lifestyle-food.jpg', '<p>\n	<span style="color:#a9a9a9;">Settings add authenticity and familiarity.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `exp_matrix_data` (`row_id`, `site_id`, `entry_id`, `field_id`, `var_id`, `row_order`, `col_id_1`, `col_id_2`, `col_id_10`, `col_id_11`, `col_id_12`, `col_id_13`, `col_id_14`, `col_id_15`, `col_id_16`, `col_id_17`, `col_id_18`, `col_id_19`) VALUES
(530, 1, 256, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(533, 1, 258, 2, NULL, 1, '{filedir_4}jaffa_recipes.jpg', '<p>\n	<span style="color:#a9a9a9;">Jaffa recipes shoot.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(534, 1, 258, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(535, 1, 259, 2, NULL, 1, '{filedir_4}test_food-cheese-orange_1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(536, 1, 259, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(537, 1, 260, 2, NULL, 1, '{filedir_4}test_beef.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(538, 1, 260, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(539, 1, 261, 2, NULL, 1, '{filedir_4}starbucks.jpg', '<p>\n	<span style="color:#a9a9a9;">Starbucks shoot examples.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(540, 1, 261, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(541, 1, 262, 2, NULL, 1, '{filedir_4}kokonoir-flat.jpg', '<p>\n	<span style="color:#d3d3d3;">Koko Noir, an artisan chocolate maker, needed clear, simple branding and packaging with a straightforward range distinction. Bespoke illustrations were drawn for the range&#39;s amazing flavour combinations. Each piece includes idiosyncratic quotes and life advice for fellow chocolate lovers.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(542, 1, 262, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(543, 1, 263, 2, NULL, 1, '{filedir_4}rideaway.jpg', '<p>\n	<span style="color:#a9a9a9;">Rebrand of leading equestrian clothing and accessories retailer, Ride-away. Live to Ride.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(544, 1, 263, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(545, 1, 264, 2, NULL, 1, '{filedir_4}Jucee001.jpg', '<p>\n	<span style="color:#a9a9a9;">The Jucee rebrand involved packaging, television advertising, a new website and print ads</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(546, 1, 264, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(547, 1, 225, 2, NULL, 1, '{filedir_4}Lakeland2013.jpg', '<p>\n	<span style="color:#a9a9a9;">Lakeland is now published in the UK, UAE and Germany.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(550, 1, 266, 2, NULL, 1, '{filedir_4}Ryman_1.jpg', '<p>\n	<span style="color:#d3d3d3;">Ryman in-store navigation</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(551, 1, 266, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(552, 1, 267, 2, NULL, 1, '{filedir_4}RobWinstanleyBrandPod.jpg', '<p>\n	<span style="color:#a9a9a9;">Branding, POS and navigation</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(553, 1, 267, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(554, 1, 268, 2, NULL, 1, '{filedir_4}Aunt-Bessies-blk.jpg', '<p>\n	<span style="color:#a9a9a9;">Aunt Bessie&#39;s POS and advertising</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(555, 1, 268, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(556, 1, 269, 2, NULL, 1, '{filedir_4}ride-away1.jpg', '<p>\n	<span style="color:#d3d3d3;">New e-commerce site for leading equestrian clothing and accessories retailer as part of rebrand by Shoot the Moon.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(557, 1, 269, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(558, 1, 270, 2, NULL, 1, '{filedir_2}packaging/Wainwrights_cat02_blk.jpg', '<p>\n	<span style="color:#a9a9a9;">The successful translation into a range of cat foods of Wainwright&#39;s popular dog ranges, found nationwide and online in Pets at Home stores.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(559, 1, 270, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(560, 1, 270, 2, NULL, 2, '{filedir_4}bn_wainrights_catsFull.jpg', '<p>\n	<span style="color:#a9a9a9;">Perfect for Cats.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(561, 1, 271, 2, NULL, 1, '{filedir_2}packaging/jucee-2013.jpg', '<p>\n	<span style="color:#a9a9a9;">Rebrand of Jucee squash, juice and juice drink ranges.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(562, 1, 271, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(563, 1, 272, 2, NULL, 1, '{filedir_2}packaging/Wainwrights_Grain-Free-Range.jpg', '<p>\n	<span style="color:#a9a9a9;">An extension to the successful Wainwright&#39;s Dog&#39;s Best Friend range.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(564, 1, 272, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(565, 1, 273, 2, NULL, 1, '{filedir_4}Wainwrights_Puppy_blk.jpg', '<p>\n	<span style="color:#a9a9a9;">Examples of packaging for Wainwright&#39;s Dog&#39;s Best Friend. A colour tiering system is in place for lifestage and flavour varieties.</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(566, 1, 273, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(567, 1, 274, 2, NULL, 1, '{filedir_4}rideaway-01B.jpg', '<p>\n	<span style="color:#a9a9a9;">Location, lifestyle, still life and product photography for major equestrian retailer, Ride-away</span></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(568, 1, 274, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(569, 1, 275, 2, NULL, 1, '{filedir_3}jucee-website.jpg', '<p>\n	A fun and engaging website to showcase the popular Jucee range of drinks.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(570, 1, 275, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(571, 1, 276, 2, NULL, 1, '{filedir_3}alpha-apps.jpg', '<p>\n	Scoping, design and build of a suite of mobile apps to promote the availability of onboard retail products. These have been produced as native apps for both iOS and Android platforms.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(572, 1, 276, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_members`
--

CREATE TABLE IF NOT EXISTS `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_members`
--

INSERT INTO `exp_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `daylight_savings`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`, `rte_enabled`, `rte_toolset_id`) VALUES
(1, 1, 'Admin', 'Admin', '98921df57eca1f348c1deb05150da212cbeea968a601bac04f590763e296838df9173296949a0e101a5e3cb378e9205f2d59bb87b1463f6ca40264c868bf298a', 'cRr:{\\Vr8+jao6rn+8PIN|pl}w^]dn;|!Gqn|-zCB$,#HrWbxEnh/r/|]`YO~:2MInAwa{Q0UfuJt#?+)c`Q''t7D3M3lxj8s6E/-|1)nusw=*caWg#B9`r)qoZB8Q?Tj', 'ec2229b71e1842575a97e29e2f34f08838ab8b28', 'de88dce51563f2806de65b62ac2604d35fc7fffb', NULL, 'martin@shoot-the-moon.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '127.0.0.1', 1351164680, 1390824998, 1393349225, 178, 0, 0, 0, 1393349221, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UTC', 'n', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', '', 'Structure|C=addons_modules&M=show_module_cp&module=structure|1\nAssets|C=addons_modules&M=show_module_cp&module=assets|2\nEdit|C=content_edit|3\nLow Variables|C=addons_modules&M=show_module_cp&module=low_variables|4', 'n', 0, 'y', 0),
(2, 6, 'Editor', 'Editor', '999e767d6f36788d70cc0d7805d7b3732f9ba10a', '', '90ca197b90a9b9702aea0c5ae3d7d4b1524e7d20', NULL, NULL, 'kathryn@shoot-the-moon.co.uk', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '127.0.0.1', 1351165033, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UTC', 'n', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_bulletin_board`
--

CREATE TABLE IF NOT EXISTS `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_data`
--

CREATE TABLE IF NOT EXISTS `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_data`
--

INSERT INTO `exp_member_data` (`member_id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_fields`
--

CREATE TABLE IF NOT EXISTS `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_groups`
--

INSERT INTO `exp_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_admin_accessories`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`) VALUES
(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(6, 1, 'Editors', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_homepage`
--

CREATE TABLE IF NOT EXISTS `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_homepage`
--

INSERT INTO `exp_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`) VALUES
(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0),
(2, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_search`
--

CREATE TABLE IF NOT EXISTS `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_copies`
--

CREATE TABLE IF NOT EXISTS `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_data`
--

CREATE TABLE IF NOT EXISTS `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_folders`
--

CREATE TABLE IF NOT EXISTS `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_message_folders`
--

INSERT INTO `exp_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`) VALUES
(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_listed`
--

CREATE TABLE IF NOT EXISTS `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_modules`
--

CREATE TABLE IF NOT EXISTS `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  `settings` text,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `exp_modules`
--

INSERT INTO `exp_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`, `settings`) VALUES
(1, 'Comment', '2.3', 'y', 'n', NULL),
(2, 'Email', '2.0', 'n', 'n', NULL),
(3, 'Emoticon', '2.0', 'n', 'n', NULL),
(4, 'Jquery', '1.0', 'n', 'n', NULL),
(5, 'Rss', '2.0', 'n', 'n', NULL),
(6, 'Safecracker', '2.1', 'y', 'n', NULL),
(7, 'Search', '2.2', 'n', 'n', NULL),
(8, 'Channel', '2.0.1', 'n', 'n', NULL),
(9, 'Member', '2.1', 'n', 'n', NULL),
(10, 'Stats', '2.0', 'n', 'n', NULL),
(11, 'Rte', '1.0', 'y', 'n', NULL),
(13, 'Assets', '1.2.2', 'y', 'n', NULL),
(14, 'Deeploy_helper', '2.0.3', 'y', 'n', NULL),
(15, 'Field_editor', '1.0.3', 'y', 'n', NULL),
(16, 'Libraree', '1.0.5', 'y', 'n', NULL),
(17, 'Low_reorder', '2.0.4', 'y', 'n', NULL),
(18, 'Low_variables', '2.3.2', 'y', 'n', NULL),
(19, 'Playa', '4.3.3', 'n', 'n', NULL),
(20, 'Updater', '3.1.5', 'y', 'n', NULL),
(21, 'Wygwam', '2.6.3', 'y', 'n', NULL),
(22, 'Zoo_flexible_admin', '1.61', 'y', 'n', NULL),
(23, 'Zoo_triggers', '1.1.10', 'y', 'n', NULL),
(24, 'Structure', '3.3.6', 'y', 'y', NULL),
(25, 'Freeform', '4.0.7', 'y', 'n', NULL),
(26, 'Gmaps', '2.3', 'n', 'n', NULL),
(27, 'Backup_proish', '1.0.5', 'y', 'n', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_module_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_module_member_groups`
--

INSERT INTO `exp_module_member_groups` (`group_id`, `module_id`) VALUES
(6, 1),
(6, 6),
(6, 11),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(6, 16),
(6, 17),
(6, 18),
(6, 20),
(6, 21),
(6, 22),
(6, 23);

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee`
--

CREATE TABLE IF NOT EXISTS `exp_navee` (
  `navee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `navigation_id` int(10) DEFAULT NULL,
  `site_id` int(10) DEFAULT NULL,
  `entry_id` int(10) DEFAULT NULL,
  `channel_id` int(10) DEFAULT NULL,
  `template` int(10) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `include` tinyint(4) DEFAULT NULL,
  `passive` tinyint(4) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  `rel` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `regex` varchar(255) DEFAULT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `custom_kids` varchar(255) DEFAULT NULL,
  `access_key` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`navee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_navee`
--

INSERT INTO `exp_navee` (`navee_id`, `navigation_id`, `site_id`, `entry_id`, `channel_id`, `template`, `type`, `parent`, `text`, `link`, `class`, `id`, `sort`, `include`, `passive`, `datecreated`, `dateupdated`, `ip_address`, `member_id`, `rel`, `name`, `target`, `title`, `regex`, `custom`, `custom_kids`, `access_key`) VALUES
(1, 1, 1, 1, 6, 1, 'guided', 0, 'The Studio', '', '', '', 1, 1, 0, '2012-10-29 11:58:25', '2012-10-29 11:58:25', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(2, 1, 1, 4, 1, 3, 'guided', 0, 'Photography', '', '', '', 2, 1, 0, '2012-10-29 11:59:48', '2012-10-29 11:59:48', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(3, 1, 1, 25, 4, 3, 'guided', 2, 'Food', '', '', '', 1, 1, 0, '2012-10-29 12:04:04', '2012-10-29 12:04:04', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_cache`
--

CREATE TABLE IF NOT EXISTS `exp_navee_cache` (
  `navee_cache_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `navigation_id` int(10) DEFAULT NULL,
  `group_id` smallint(4) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `recursive` tinyint(4) DEFAULT NULL,
  `ignore_include` tinyint(4) DEFAULT NULL,
  `start_from_parent` tinyint(4) DEFAULT NULL,
  `start_from_kid` tinyint(4) DEFAULT NULL,
  `single_parent` tinyint(4) DEFAULT NULL,
  `cache` longtext,
  PRIMARY KEY (`navee_cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_config`
--

CREATE TABLE IF NOT EXISTS `exp_navee_config` (
  `navee_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `k` varchar(255) DEFAULT NULL,
  `v` text,
  PRIMARY KEY (`navee_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `exp_navee_config`
--

INSERT INTO `exp_navee_config` (`navee_config_id`, `site_id`, `k`, `v`) VALUES
(1, 1, 'stylesheet', 'navee.css'),
(2, 1, 'include_index', 'false'),
(3, 1, 'remove_deleted_entries', 'false'),
(4, 1, 'entify_ee_tags', 'false'),
(5, 1, 'force_trailing_slash', 'no'),
(6, 1, 'only_superadmins_can_admin_navs', 'false'),
(7, 1, 'blockedMemberGroups', 'a:2:{i:1;a:1:{i:0;s:1:"6";}i:0;a:1:{i:0;s:1:"6";}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_members`
--

CREATE TABLE IF NOT EXISTS `exp_navee_members` (
  `navee_mem_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `navee_id` int(10) DEFAULT NULL,
  `members` text,
  PRIMARY KEY (`navee_mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_navs`
--

CREATE TABLE IF NOT EXISTS `exp_navee_navs` (
  `navigation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `nav_title` varchar(255) DEFAULT NULL,
  `nav_name` varchar(255) DEFAULT NULL,
  `nav_description` varchar(255) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`navigation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_navee_navs`
--

INSERT INTO `exp_navee_navs` (`navigation_id`, `site_id`, `nav_title`, `nav_name`, `nav_description`, `datecreated`, `dateupdated`, `ip_address`, `member_id`) VALUES
(1, 1, 'main', 'Main', 'Main Navigation', '2012-10-29 11:57:38', '2012-10-29 11:57:38', '127.0.0.1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_online_users`
--

CREATE TABLE IF NOT EXISTS `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57334 ;

--
-- Dumping data for table `exp_online_users`
--

INSERT INTO `exp_online_users` (`online_id`, `site_id`, `member_id`, `in_forum`, `name`, `ip_address`, `date`, `anon`) VALUES
(57306, 1, 0, 'n', '', '157.56.92.151', 1396249876, ''),
(57307, 1, 0, 'n', '', '202.46.61.34', 1396250242, ''),
(57308, 1, 0, 'n', '', '119.63.193.131', 1396250332, ''),
(57309, 1, 0, 'n', '', '180.76.5.20', 1396250904, ''),
(57310, 1, 0, 'n', '', '217.9.193.42', 1396252081, ''),
(57311, 1, 0, 'n', '', '109.111.197.225', 1396261309, ''),
(57312, 1, 0, 'n', '', '68.180.224.179', 1396252379, ''),
(57313, 1, 0, 'n', '', '217.41.47.117', 1396252714, ''),
(57314, 1, 0, 'n', '', '157.56.92.161', 1396253189, ''),
(57315, 1, 0, 'n', '', '157.56.229.188', 1396253236, ''),
(57316, 1, 0, 'n', '', '81.174.155.113', 1396253234, ''),
(57317, 1, 0, 'n', '', '209.133.77.165', 1396254056, ''),
(57318, 1, 0, 'n', '', '220.181.108.123', 1396254128, ''),
(57319, 1, 0, 'n', '', '109.111.197.225', 1396261309, ''),
(57320, 1, 0, 'n', '', '157.56.92.151', 1396255109, ''),
(57321, 1, 0, 'n', '', '66.249.76.138', 1396255344, ''),
(57322, 1, 0, 'n', '', '157.56.229.244', 1396256923, ''),
(57323, 1, 0, 'n', '', '109.111.197.225', 1396261309, ''),
(57324, 1, 0, 'n', '', '220.181.108.83', 1396257424, ''),
(57325, 1, 0, 'n', '', '66.249.64.232', 1396257536, ''),
(57326, 1, 0, 'n', '', '123.125.71.13', 1396257715, ''),
(57327, 1, 0, 'n', '', '46.229.167.149', 1396258062, ''),
(57328, 1, 0, 'n', '', '213.205.240.10', 1396260495, ''),
(57329, 1, 0, 'n', '', '109.111.197.225', 1396261309, ''),
(57330, 1, 0, 'n', '', '220.181.108.76', 1396261584, ''),
(57331, 1, 0, 'n', '', '84.19.42.82', 1396261638, ''),
(57332, 1, 0, 'n', '', '77.75.187.117', 1396261708, ''),
(57333, 1, 0, 'n', '', '37.191.100.2', 1396262576, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_password_lockout`
--

CREATE TABLE IF NOT EXISTS `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `exp_password_lockout`
--

INSERT INTO `exp_password_lockout` (`lockout_id`, `login_date`, `ip_address`, `user_agent`, `username`) VALUES
(1, 1352135798, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(2, 1352293133, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(3, 1352293148, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(4, 1352303529, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(5, 1352893907, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(6, 1353077438, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(7, 1353933615, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(8, 1353933622, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(9, 1354024028, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(10, 1355143275, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11', 'admin'),
(11, 1355143285, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11', 'admin'),
(12, 1355143318, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11', 'admin'),
(13, 1355817752, '87.112.160.113', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(14, 1355817795, '87.112.160.113', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(15, 1356086333, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'stmstaff'),
(16, 1356086342, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'stmstaff'),
(17, 1356186803, '109.145.237.206', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(18, 1357139458, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(19, 1357656551, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(20, 1357656941, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'admin'),
(21, 1357656949, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'admin'),
(22, 1357744654, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(23, 1357834692, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(24, 1357834699, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(25, 1360169895, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(26, 1360578863, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(27, 1362503159, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'admin'),
(28, 1373902341, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 'admin'),
(29, 1382966361, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 'admin'),
(30, 1386260480, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 'shootthemoon'),
(31, 1386260507, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 'admin'),
(32, 1386260516, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 'admin'),
(33, 1389092223, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.73.11 (KHTML, like Gecko) Version/7.0.1 Safari/537.73.11', 'admin'),
(34, 1389092235, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.73.11 (KHTML, like Gecko) Version/7.0.1 Safari/537.73.11', 'administrator'),
(35, 1389092245, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.73.11 (KHTML, like Gecko) Version/7.0.1 Safari/537.73.11', 'administrator'),
(36, 1389092279, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.73.11 (KHTML, like Gecko) Version/7.0.1 Safari/537.73.11', 'shootthemoon'),
(37, 1389789388, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 'admin'),
(38, 1390824976, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 'admin'),
(39, 1390824987, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `exp_ping_servers`
--

CREATE TABLE IF NOT EXISTS `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_playa_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_remember_me`
--

CREATE TABLE IF NOT EXISTS `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_reset_password`
--

CREATE TABLE IF NOT EXISTS `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_revision_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `exp_revision_tracker`
--

INSERT INTO `exp_revision_tracker` (`tracker_id`, `item_id`, `item_table`, `item_field`, `item_date`, `item_author_id`, `item_data`) VALUES
(1, 1, 'exp_templates', 'template_data', 1351167351, 1, '{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n        	<section id="content" class="pie">\n				 {lv_image_matrix}\n        		<h1>Photography</h1>\n        		<div id="intro">\n	        		<p>Still life, lifestyle, location or set photography, shoot the moon has a wealth of experience in creating stunning images for design, advertising, packaging and editorial.</p>\n	        		<p>The portfolio is a good place to start – for those with food image requirements, you will be happy to hear we have a large fully equipped kitchen adjacent to the studio and heaps of frozen and chilled storage. Everything you need is here (or can be pretty sharpish!).</p>\n        		</div>\n        		</p>\n        		<h2>Portfolio</h2>\n        		{lv_folio_nav}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			<li>\n	        				<img src="/img/sample1.jpg" alt="" />\n	        				<div>\n		        				<h3>Image title</h3>\n		        				<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n						<li>\n							<img src="/img/sample2.jpg" alt="" />\n							<div>\n								<h3>Image title</h3>\n								<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n							</div>\n	        				<p class="top"><a href="#">top</a></p>\n						</li>\n						<li>\n							<img src="/img/sample3.jpg" alt="" />\n							<div>\n								<h3>Image title</h3>\n								<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n							</div>\n	        				<p class="top"><a href="#">top</a></p>\n						</li>\n						<li>\n							<img src="/img/sample4.jpg" alt="" />\n							<div>\n								<h3>Image title</h3>\n								<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n							</div>\n	        				<p class="top"><a href="#">top</a></p>\n						</li>\n						<li>\n							<img src="/img/sample5.jpg" alt="" />\n							<div>\n								<h3>Image title</h3>\n								<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n							</div>\n	        				<p class="top"><a href="#">top</a></p>\n						</li>\n						<li>\n							<img src="/img/sample6.jpg" alt="" />\n							<div>\n								<h3>Image title</h3>\n								<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n							</div>\n	        				<p class="top"><a href="#">top</a></p>\n						</li>\n						<li>\n							<img src="/img/sample7.jpg" alt="" />\n							<div>\n								<h3>Image title</h3>\n								<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n							</div>\n	        				<p class="top"><a href="#">top</a></p>\n						</li>\n						<li>\n							<img src="/img/sample8.jpg" alt="" />\n							<div>\n								<h3>Image title</h3>\n								<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n							</div>\n	        				<p class="top"><a href="#">top</a></p>\n						</li>\n						<li>\n							<img src="/img/sample9.jpg" alt="" />\n							<div>\n								<h3>Image title</h3>\n								<p>Suspendisse dignissim posuere tristique. Donec tellus mi, fringilla eget laoreet eget, malesuada ac nisi. Mauris blandit nisi felis. Donec a mauris justo. Quisque suscipit magna ac augue aliquam fringilla. Nulla ac sem tortor. Proin ultrices aliquam aliquet.</p>\n							</div>\n	        				<p class="top"><a href="#">top</a></p>\n						</li>\n	        		</ul>\n        		</div>\n        		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_thumbs_nav}        \n			{lv_blockquote}\n        \n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(2, 1, 'exp_templates', 'template_data', 1351266480, 1, '{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		{lv_folio_nav}\n        		\n         	{exp:channel:entries channel="photography" disable="{lv_disable_basic}" dynamic="on"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{lv_thumbs_nav}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(3, 1, 'exp_templates', 'template_data', 1351266761, 1, '{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		\n         	{exp:channel:entries channel="photography" disable="{lv_disable_basic}" dynamic="on"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_blockquote}\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(5, 1, 'exp_templates', 'template_data', 1351517720, 1, 'Index\n{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		\n         	{exp:channel:entries channel="photography" disable="{lv_disable_basic}" dynamic="on"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_blockquote}\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(7, 8, 'exp_templates', 'template_data', 1352825737, 1, ''),
(8, 5, 'exp_templates', 'template_data', 1353078861, 1, '{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		{lv_folio_nav}\n        		\n        		<div id="content1" class="folio">\n	        		<ul>\n         	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\n	\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			\n	        		\n          	{/exp:channel:entries}\n	        		</ul>\n       		</div>\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n   \n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{embed="sites/discipline-thums"}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(10, 1, 'exp_templates', 'template_data', 1353078861, 1, '{lv_doc_header}\n    </head>\n    <body class="home">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	{lv_image_matrix}\n					\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n         	{exp:channel:entries channel="studio" disable="{lv_disable_basic}" dynamic="off"}\n	  		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_studio_shots}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_studio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_studio_shots}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_blockquote}\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(12, 8, 'exp_templates', 'template_data', 1353078861, 1, '<?php\n\n	$url = $_SERVER[''REQUEST_URI''];\n	$segments = explode("/",$url);\n	if ($segments[2] != "") {\n		$pagination = $segments[2];\n		$offset = substr($pagination, 1);\n	} else {\n		$offset = 0;\n	}\n	\n?>\n\n<ul id="switch" class="pie clearfix">\n\n	{if segment_1=="creative"}\n	{exp:channel:entries channel="packaging|branding|literature|point_of_sale" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n	{if segment_1=="photography"}\n	{exp:channel:entries channel="food|product|location|set" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n	{if segment_1=="digital"}\n	{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n</ul>'),
(20, 10, 'exp_templates', 'template_data', 1360754332, 1, '{lv_doc_header}\n    </head>\n    <body class="thinking">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	<div id="matrix">\n				 		<ul id="slides">\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        </ul>\n				 	</div>\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			\n        		</div>\n        		<!--<div id="intro">\n	        		\n        		</div>-->\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(21, 10, 'exp_templates', 'template_data', 1360754334, 1, '{lv_doc_header}\n    </head>\n    <body class="thinking">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	<div id="matrix">\n				 		<ul id="slides">\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        </ul>\n				 	</div>\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			\n        		</div>\n        		<!--<div id="intro">\n	        		\n        		</div>-->\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(22, 10, 'exp_templates', 'template_data', 1360754387, 1, '{lv_doc_header}\n    </head>\n    <body class="thinking">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	<!--<div id="matrix-sml">-->\n				 		<ul id="slides-single">\n                            {cf_rotating_image_blocks}\n                            <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="199" height="145" crop="yes" allow_scale_larger="yes"}</li>\n                            {/cf_rotating_image_blocks}\n				 		</ul>\n				 	<!--</div>-->\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			{homepage_pull_quote}\n        		</div>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(25, 5, 'exp_templates', 'template_data', 1373902823, 1, 'D{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n        		<h1>{title}</h1>\n					{lv_folio_nav}\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		\n        		<div id="content1" class="folio">\n	        		<ul>\n         	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\n         	\n         				\n         				{if segment_2=="motion"}\n         				\n         			<!-- 	<video id="my_video_1" class="video-js vjs-default-skin" controls\n         				  preload="auto" width="640" height="360" poster="/images/video/bat-and-ball.jpg"\n         				  data-setup="{}">\n         				  <source src="/images/video/bat-and-ball.mp4" type=''video/mp4''>\n         				  <source src="/images/video/bat-and-ball.webm" type=''video/webm''>\n         				  <source src="/images/video/bat-and-ball.ogv" type=''video/ogv''>\n         				</video> -->\n         				\n         				\n         				\n         					{video_upload}\n	        			<li id="id{entry_id}">\n							<video id="my_video_1" class="video-js vjs-default-skin" controls\n							  preload="auto" width="640" height="360" poster="{start_image}"\n							  data-setup="{}">\n							  <source src="{version_m4v}" type=''video/mp4''>\n							  <source src="{version_webm}" type=''video/webm''>\n							  <source src="{version_ogv}" type=''video/ogv''>\n							</video>\n	        			</li>\n         					{/video_upload}\n         					\n         					\n         					\n         					\n         					\n         				\n         				{if:else}\n         				\n         				\n	\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  {/if}\n	        			\n	        		\n          	{/exp:channel:entries}\n	        		</ul>\n       		</div>\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n   \n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{embed="sites/discipline-thums"}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(26, 5, 'exp_templates', 'template_data', 1373903005, 1, '{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n        		<h1>{title}</h1>\n					{lv_folio_nav}\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		\n        		<div id="content1" class="folio">\n	        		<ul>\n         	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\n         	\n         				\n         				{if segment_2=="motion"}\n         				\n         			<!-- 	<video id="my_video_1" class="video-js vjs-default-skin" controls\n         				  preload="auto" width="640" height="360" poster="/images/video/bat-and-ball.jpg"\n         				  data-setup="{}">\n         				  <source src="/images/video/bat-and-ball.mp4" type=''video/mp4''>\n         				  <source src="/images/video/bat-and-ball.webm" type=''video/webm''>\n         				  <source src="/images/video/bat-and-ball.ogv" type=''video/ogv''>\n         				</video> -->\n         				\n         				\n         				\n         					{video_upload}\n	        			<li id="id{entry_id}">\n							<video id="my_video_1" class="video-js vjs-default-skin" controls\n							  preload="auto" width="640" height="360" poster="{start_image}"\n							  data-setup="{}">\n							  <source src="{version_m4v}" type=''video/mp4''>\n							  <source src="{version_webm}" type=''video/webm''>\n							  <source src="{version_ogv}" type=''video/ogv''>\n							</video>\n	        			</li>\n         					{/video_upload}\n         					\n         					\n         					\n         					\n         					\n         				\n         				{if:else}\n         				\n         				\n	\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  {/if}\n	        			\n	        		\n          	{/exp:channel:entries}\n	        		</ul>\n       		</div>\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n   \n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{embed="sites/discipline-thums"}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(29, 3, 'exp_templates', 'template_data', 1382980900, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy|mobile-apps" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(37, 12, 'exp_templates', 'template_data', 1383040294, 1, '{lv_doc_header}\n\n\n    </head>\n    <body class="contact-thankyou">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		<div id="intro">\n			<p>Thank you for time. Your message will be reviewed by a member of the Shoot the Moon team very shortly.</p>\n			<p>Please feel free to call us on <strong>0161 205 3311</strong> if your enquiry is urgent.</p>\n			</div>\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(38, 12, 'exp_templates', 'template_data', 1383040308, 1, '{lv_doc_header}\n\n\n    </head>\n    <body class="contact-thankyou">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		<div id="intro">\n			<p>Thank you for time. Your message will be reviewed by a member of the Shoot the Moon team very shortly.</p>\n			<p>Please feel free to call us on <b>0161 205 3311</b> if your enquiry is urgent.</p>\n			</div>\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(39, 12, 'exp_templates', 'template_data', 1383040330, 1, '{lv_doc_header}\n\n\n    </head>\n    <body class="contact-thankyou">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		<div id="intro">\n			<p>Thank you for time. Your message will be reviewed by a member of the Shoot the Moon team very shortly.</p>\n			<p>Please feel free to call us on <span>0161 205 3311</span> if your enquiry is urgent.</p>\n			</div>\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(40, 12, 'exp_templates', 'template_data', 1383040453, 1, '{lv_doc_header}\n\n\n    </head>\n    <body class="contact-thankyou">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		<div id="intro">\n			<p>Thank you for time. Your message will be reviewed by a member of the Shoot the Moon team very shortly.</p>\n			<p>Please feel free to call us on <span><a href="tel:0161 205 3311">0161 205 3311</a></span> if your enquiry is urgent.</p>\n			</div>\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(41, 12, 'exp_templates', 'template_data', 1383040629, 1, '{lv_doc_header}\n\n\n    </head>\n    <body class="contact-thankyou">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		<div id="intro">\n			<p>Thank you for time. Your message will be reviewed by a member of the Shoot the Moon team very shortly.</p>\n			<p>Please feel free to call us on <span><a href="tel:0161 205 3311">0161 205 3311</a></span> if your enquiry is urgent.</p>\n			<p><a href="/">This link</a> will take you back to the homepage from where you can continue to browse our website.</p>\n			</div>\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(43, 4, 'exp_templates', 'template_data', 1383040975, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message" return="sites/get-in-touch-thankyou"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(44, 4, 'exp_templates', 'template_data', 1383041092, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message" return="sites/get-in-touch-thankyou"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(45, 4, 'exp_templates', 'template_data', 1383041160, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message" return="sites/landing"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(46, 4, 'exp_templates', 'template_data', 1383041329, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message" return="sites/get-in-touch-thankyou"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>');
INSERT INTO `exp_revision_tracker` (`tracker_id`, `item_id`, `item_table`, `item_field`, `item_date`, `item_author_id`, `item_data`) VALUES
(47, 4, 'exp_templates', 'template_data', 1383041472, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message" return="get-in-touch-thankyou"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(48, 4, 'exp_templates', 'template_data', 1383041505, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message" return="get-in-touch-thankyou"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(49, 3, 'exp_templates', 'template_data', 1386261806, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|advertising-comms|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n<iframe src="//player.vimeo.com/video/81114241" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>						\n{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy|mobile-apps" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(50, 3, 'exp_templates', 'template_data', 1386261890, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|advertising-comms|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}						\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy|mobile-apps" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(51, 3, 'exp_templates', 'template_data', 1386261912, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|advertising-comms|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}						\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy|mobile-apps" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n<iframe src="//player.vimeo.com/video/81114241" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(52, 3, 'exp_templates', 'template_data', 1386261945, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|advertising-comms|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}						\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy|mobile-apps" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(53, 5, 'exp_templates', 'template_data', 1386342810, 1, '{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n        		<h1>{title}</h1>\n					{lv_folio_nav}\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		\n        		<div id="content1" class="folio">\n\n	        		<ul>\n         		\n				  {if segment_2=="product"}\n         			<video id="my_video_1" class="video-js vjs-default-skin" controls\n     				  preload="auto" width="640" height="360" poster="/images/video/Protected-Photo-Shoot.jpg"\n     				  data-setup="{}">\n     				  <source src="/images/video/Protected-Photo-Shoot.m4v" type=''video/mp4''>\n     				  <source src="/images/video/Protected-Photo-Shoot.webm" type=''video/webm''>\n     				  <source src="/images/video/Protected-Photo-Shoot.ogv" type=''video/ogv''>\n     				</video>\n				{/if} \n\n         		{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\n         	\n         				{if segment_2=="motion"}\n         				\n         				<!-- <video id="my_video_1" class="video-js vjs-default-skin" controls\n         				  preload="auto" width="640" height="360" poster="/images/video/bat-and-ball.jpg"\n         				  data-setup="{}">\n         				  <source src="/images/video/bat-and-ball.mp4" type=''video/mp4''>\n         				  <source src="/images/video/bat-and-ball.webm" type=''video/webm''>\n         				  <source src="/images/video/bat-and-ball.ogv" type=''video/ogv''>\n         				</video> -->\n         				\n         				{video_upload}\n		        			<li id="id{entry_id}">\n								<video id="my_video_1" class="video-js vjs-default-skin" controls\n								  preload="auto" width="640" height="360" poster="{start_image}"\n								  data-setup="{}">\n								  <source src="{version_m4v}" type=''video/mp4''>\n								  <source src="{version_webm}" type=''video/webm''>\n								  <source src="{version_ogv}" type=''video/ogv''>\n								</video>\n		        			</li>\n     					{/video_upload}\n         				\n         				{if:else}\n\n						{cf_portfolio_images}\n\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  {/if}\n	        			\n	        		\n          	{/exp:channel:entries}\n	        		</ul>\n       		</div>\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n   \n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{embed="sites/discipline-thums"}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_tools`
--

CREATE TABLE IF NOT EXISTS `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_rte_tools`
--

INSERT INTO `exp_rte_tools` (`tool_id`, `name`, `class`, `enabled`) VALUES
(1, 'Blockquote', 'Blockquote_rte', 'y'),
(2, 'Bold', 'Bold_rte', 'y'),
(3, 'Headings', 'Headings_rte', 'y'),
(4, 'Image', 'Image_rte', 'y'),
(5, 'Italic', 'Italic_rte', 'y'),
(6, 'Link', 'Link_rte', 'y'),
(7, 'Ordered List', 'Ordered_list_rte', 'y'),
(8, 'Underline', 'Underline_rte', 'y'),
(9, 'Unordered List', 'Unordered_list_rte', 'y'),
(10, 'View Source', 'View_source_rte', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_toolsets`
--

CREATE TABLE IF NOT EXISTS `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_rte_toolsets`
--

INSERT INTO `exp_rte_toolsets` (`toolset_id`, `member_id`, `name`, `tools`, `enabled`) VALUES
(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_search`
--

CREATE TABLE IF NOT EXISTS `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_search_log`
--

CREATE TABLE IF NOT EXISTS `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_security_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12847 ;

--
-- Dumping data for table `exp_security_hashes`
--

INSERT INTO `exp_security_hashes` (`hash_id`, `date`, `ip_address`, `hash`) VALUES
(12823, 1396108958, '62.210.142.7', 'cf47ff8d10adf94f9a7d02eb725d049c7b78741f'),
(12824, 1396142067, '157.56.92.149', '803ee80d60cfea6d14d47fdf21a600a7cd1a15c4'),
(12825, 1396161724, '122.178.112.249', 'fddad9a4294a2c426299cc3febfc790f3461bced'),
(12826, 1396165664, '157.55.34.100', 'ffc623d328e3788d8e0e8fd74db1fca9f4cbd22c'),
(12827, 1396166721, '223.146.33.155', 'cd253072525bf422225c31353a4e46ff1f55aeb3'),
(12828, 1396166753, '223.146.33.155', '6202a00112ff56a2ec089f87499521f4ab3b1f93'),
(12829, 1396166789, '223.146.33.155', 'ac2ae1b19d055fce194d0bed7f523fd5830b2f74'),
(12830, 1396166858, '223.146.33.155', '0df9021920d19a3574cfd7820dff060eb0c9102d'),
(12831, 1396166925, '223.146.33.155', '2c314fed17b9435f22c099c7a5e25a8353b67b45'),
(12832, 1396166988, '223.146.33.155', 'ba8a0c8730d0ba2ab1d44e9a394cf6afa750d6fb'),
(12833, 1396167007, '223.146.33.155', '76b1dae5e11be045e0d58d318bf62dab4a1a2283'),
(12834, 1396167029, '223.146.33.155', 'a766aa5a7d37256c9656553b52be1c791962a390'),
(12835, 1396167083, '223.146.33.155', 'ccac281268fdbd1cae248d3d6fca3ec58c9b8ad8'),
(12836, 1396172643, '162.213.42.131', 'ae7256865e552767549851cc20ffe83fb68db43b'),
(12837, 1396174742, '178.154.160.29', '1b6a5c0020b63357b5421466c2d86d79f1a026cb'),
(12838, 1396193175, '66.249.64.242', '04f3590e3ec61b28bfcaad8849308dd9b3410d23'),
(12839, 1396203644, '86.160.105.14', '4e1dc40552eac0eb6ffc4c5316c3e477d907ef0a'),
(12840, 1396207989, '86.11.239.82', '8997ca1692408416a023e8b738d924ddde2b9a3d'),
(12841, 1396223364, '5.64.57.66', '756d09f5d89c9680aa15d4073ef2699dd015c220'),
(12842, 1396228923, '207.189.121.44', 'fa0b42e84ad4de2297ef1acf6517a86988ba96d4'),
(12843, 1396241175, '66.249.64.237', 'f4bef5aa61a6ce2c4dd47bc0c1c362939ed984c1'),
(12844, 1396248845, '157.56.92.151', '60c07b4ac4044ca1326db0baf391b738bd1bcf14'),
(12845, 1396249877, '157.56.92.151', 'c2139064c1f6e072f7ae087f14057fd65bf774a6'),
(12846, 1396262575, '37.191.100.2', '8453dfb8d59ed0855c010a374f17ac9225c09f5f');

-- --------------------------------------------------------

--
-- Table structure for table `exp_sessions`
--

CREATE TABLE IF NOT EXISTS `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_sites`
--

CREATE TABLE IF NOT EXISTS `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` longtext,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_sites`
--

INSERT INTO `exp_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`, `site_pages`) VALUES
(1, 'Shoot the Moon', 'default_site', NULL, 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MzI6Imh0dHA6Ly93d3cuc2hvb3QtdGhlLW1vb24uY28udWsvIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjM5OiJodHRwOi8vd3d3LnNob290LXRoZS1tb29uLmNvLnVrL3RoZW1lcy8iO3M6MTU6IndlYm1hc3Rlcl9lbWFpbCI7czoyNzoibWFydGluQHNob290LXRoZS1tb29uLmNvLnVrIjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czo0ODoiaHR0cDovL3d3dy5zaG9vdC10aGUtbW9vbi5jby51ay9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NjI6Ii92YXIvd3d3L3Zob3N0cy9zaG9vdC10aGUtbW9vbi5jby51ay9odHRwZG9jcy9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX2ZvbnQiO3M6MToieSI7czoxMjoiY2FwdGNoYV9yYW5kIjtzOjE6InkiO3M6MjM6ImNhcHRjaGFfcmVxdWlyZV9tZW1iZXJzIjtzOjE6Im4iO3M6MTc6ImVuYWJsZV9kYl9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImVuYWJsZV9zcWxfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJmb3JjZV9xdWVyeV9zdHJpbmciO3M6MToibiI7czoxMzoic2hvd19wcm9maWxlciI7czoxOiJuIjtzOjE4OiJ0ZW1wbGF0ZV9kZWJ1Z2dpbmciO3M6MToibiI7czoxNToiaW5jbHVkZV9zZWNvbmRzIjtzOjE6Im4iO3M6MTM6ImNvb2tpZV9kb21haW4iO3M6MDoiIjtzOjExOiJjb29raWVfcGF0aCI7czowOiIiO3M6MTc6InVzZXJfc2Vzc2lvbl90eXBlIjtzOjE6ImMiO3M6MTg6ImFkbWluX3Nlc3Npb25fdHlwZSI7czoyOiJjcyI7czoyMToiYWxsb3dfdXNlcm5hbWVfY2hhbmdlIjtzOjE6InkiO3M6MTg6ImFsbG93X211bHRpX2xvZ2lucyI7czoxOiJ5IjtzOjE2OiJwYXNzd29yZF9sb2Nrb3V0IjtzOjE6InkiO3M6MjU6InBhc3N3b3JkX2xvY2tvdXRfaW50ZXJ2YWwiO3M6MToiMSI7czoyMDoicmVxdWlyZV9pcF9mb3JfbG9naW4iO3M6MToieSI7czoyMjoicmVxdWlyZV9pcF9mb3JfcG9zdGluZyI7czoxOiJ5IjtzOjI0OiJyZXF1aXJlX3NlY3VyZV9wYXNzd29yZHMiO3M6MToibiI7czoxOToiYWxsb3dfZGljdGlvbmFyeV9wdyI7czoxOiJ5IjtzOjIzOiJuYW1lX29mX2RpY3Rpb25hcnlfZmlsZSI7czowOiIiO3M6MTc6Inhzc19jbGVhbl91cGxvYWRzIjtzOjE6InkiO3M6MTU6InJlZGlyZWN0X21ldGhvZCI7czo4OiJyZWRpcmVjdCI7czo5OiJkZWZ0X2xhbmciO3M6NzoiZW5nbGlzaCI7czo4OiJ4bWxfbGFuZyI7czoyOiJlbiI7czoxMjoic2VuZF9oZWFkZXJzIjtzOjE6InkiO3M6MTE6Imd6aXBfb3V0cHV0IjtzOjE6Im4iO3M6MTM6ImxvZ19yZWZlcnJlcnMiO3M6MToibiI7czoxMzoibWF4X3JlZmVycmVycyI7czozOiI1MDAiO3M6MTE6InRpbWVfZm9ybWF0IjtzOjI6InVzIjtzOjE1OiJzZXJ2ZXJfdGltZXpvbmUiO3M6MzoiVVRDIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJuIjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6MzoiVVRDIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjE6Im4iO3M6MTU6Imhvbm9yX2VudHJ5X2RzdCI7czoxOiJ5IjtzOjEzOiJtYWlsX3Byb3RvY29sIjtzOjQ6Im1haWwiO3M6MTE6InNtdHBfc2VydmVyIjtzOjA6IiI7czoxMzoic210cF91c2VybmFtZSI7czowOiIiO3M6MTM6InNtdHBfcGFzc3dvcmQiO3M6MDoiIjtzOjExOiJlbWFpbF9kZWJ1ZyI7czoxOiJuIjtzOjEzOiJlbWFpbF9jaGFyc2V0IjtzOjU6InV0Zi04IjtzOjE1OiJlbWFpbF9iYXRjaG1vZGUiO3M6MToibiI7czoxNjoiZW1haWxfYmF0Y2hfc2l6ZSI7czowOiIiO3M6MTE6Im1haWxfZm9ybWF0IjtzOjU6InBsYWluIjtzOjk6IndvcmRfd3JhcCI7czoxOiJ5IjtzOjIyOiJlbWFpbF9jb25zb2xlX3RpbWVsb2NrIjtzOjE6IjUiO3M6MjI6ImxvZ19lbWFpbF9jb25zb2xlX21zZ3MiO3M6MToieSI7czo4OiJjcF90aGVtZSI7czo3OiJkZWZhdWx0IjtzOjIxOiJlbWFpbF9tb2R1bGVfY2FwdGNoYXMiO3M6MToibiI7czoxNjoibG9nX3NlYXJjaF90ZXJtcyI7czoxOiJ5IjtzOjEyOiJzZWN1cmVfZm9ybXMiO3M6MToieSI7czoxOToiZGVueV9kdXBsaWNhdGVfZGF0YSI7czoxOiJ5IjtzOjI0OiJyZWRpcmVjdF9zdWJtaXR0ZWRfbGlua3MiO3M6MToibiI7czoxNjoiZW5hYmxlX2NlbnNvcmluZyI7czoxOiJuIjtzOjE0OiJjZW5zb3JlZF93b3JkcyI7czowOiIiO3M6MTg6ImNlbnNvcl9yZXBsYWNlbWVudCI7czowOiIiO3M6MTA6ImJhbm5lZF9pcHMiO3M6MDoiIjtzOjEzOiJiYW5uZWRfZW1haWxzIjtzOjA6IiI7czoxNjoiYmFubmVkX3VzZXJuYW1lcyI7czowOiIiO3M6MTk6ImJhbm5lZF9zY3JlZW5fbmFtZXMiO3M6MDoiIjtzOjEwOiJiYW5fYWN0aW9uIjtzOjg6InJlc3RyaWN0IjtzOjExOiJiYW5fbWVzc2FnZSI7czozNDoiVGhpcyBzaXRlIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZSI7czoxNToiYmFuX2Rlc3RpbmF0aW9uIjtzOjIxOiJodHRwOi8vd3d3LnlhaG9vLmNvbS8iO3M6MTY6ImVuYWJsZV9lbW90aWNvbnMiO3M6MToieSI7czoxMjoiZW1vdGljb25fdXJsIjtzOjM5OiJodHRwOi8vc3RtLXdlYnNpdGU6ODg4OC9pbWFnZXMvc21pbGV5cy8iO3M6MTk6InJlY291bnRfYmF0Y2hfdG90YWwiO3M6NDoiMTAwMCI7czoxNzoibmV3X3ZlcnNpb25fY2hlY2siO3M6MToieSI7czoxNzoiZW5hYmxlX3Rocm90dGxpbmciO3M6MToibiI7czoxNzoiYmFuaXNoX21hc2tlZF9pcHMiO3M6MToieSI7czoxNDoibWF4X3BhZ2VfbG9hZHMiO3M6MjoiMTAiO3M6MTM6InRpbWVfaW50ZXJ2YWwiO3M6MToiOCI7czoxMjoibG9ja291dF90aW1lIjtzOjI6IjMwIjtzOjE1OiJiYW5pc2htZW50X3R5cGUiO3M6NzoibWVzc2FnZSI7czoxNDoiYmFuaXNobWVudF91cmwiO3M6MDoiIjtzOjE4OiJiYW5pc2htZW50X21lc3NhZ2UiO3M6NTA6IllvdSBoYXZlIGV4Y2VlZGVkIHRoZSBhbGxvd2VkIHBhZ2UgbG9hZCBmcmVxdWVuY3kuIjtzOjE3OiJlbmFibGVfc2VhcmNoX2xvZyI7czoxOiJ5IjtzOjE5OiJtYXhfbG9nZ2VkX3NlYXJjaGVzIjtzOjM6IjUwMCI7czoxNzoidGhlbWVfZm9sZGVyX3BhdGgiO3M6NTM6Ii92YXIvd3d3L3Zob3N0cy9zaG9vdC10aGUtbW9vbi5jby51ay9odHRwZG9jcy90aGVtZXMvIjtzOjEwOiJpc19zaXRlX29uIjtzOjE6InkiO3M6MTE6InJ0ZV9lbmFibGVkIjtzOjE6InkiO3M6MjI6InJ0ZV9kZWZhdWx0X3Rvb2xzZXRfaWQiO3M6MToiMSI7fQ==', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6NDc6Imh0dHA6Ly93d3cuc2hvb3QtdGhlLW1vb24uY28udWsvaW1hZ2VzL2F2YXRhcnMvIjtzOjExOiJhdmF0YXJfcGF0aCI7czo2MToiL3Zhci93d3cvdmhvc3RzL3Nob290LXRoZS1tb29uLmNvLnVrL2h0dHBkb2NzL2ltYWdlcy9hdmF0YXJzLyI7czoxNjoiYXZhdGFyX21heF93aWR0aCI7czozOiIxMDAiO3M6MTc6ImF2YXRhcl9tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMzoiYXZhdGFyX21heF9rYiI7czoyOiI1MCI7czoxMzoiZW5hYmxlX3Bob3RvcyI7czoxOiJuIjtzOjk6InBob3RvX3VybCI7czo1MzoiaHR0cDovL3d3dy5zaG9vdC10aGUtbW9vbi5jby51ay9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6Njc6Ii92YXIvd3d3L3Zob3N0cy9zaG9vdC10aGUtbW9vbi5jby51ay9odHRwZG9jcy9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTU6InBob3RvX21heF93aWR0aCI7czozOiIxMDAiO3M6MTY6InBob3RvX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEyOiJwaG90b19tYXhfa2IiO3M6MjoiNTAiO3M6MTY6ImFsbG93X3NpZ25hdHVyZXMiO3M6MToieSI7czoxMzoic2lnX21heGxlbmd0aCI7czozOiI1MDAiO3M6MjE6InNpZ19hbGxvd19pbWdfaG90bGluayI7czoxOiJuIjtzOjIwOiJzaWdfYWxsb3dfaW1nX3VwbG9hZCI7czoxOiJuIjtzOjExOiJzaWdfaW1nX3VybCI7czo2MToiaHR0cDovL3d3dy5zaG9vdC10aGUtbW9vbi5jby51ay9pbWFnZXMvc2lnbmF0dXJlX2F0dGFjaG1lbnRzLyI7czoxMjoic2lnX2ltZ19wYXRoIjtzOjc1OiIvdmFyL3d3dy92aG9zdHMvc2hvb3QtdGhlLW1vb24uY28udWsvaHR0cGRvY3MvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2ODoiL3Zhci93d3cvdmhvc3RzL3Nob290LXRoZS1tb29uLmNvLnVrL2h0dHBkb2NzL2ltYWdlcy9wbV9hdHRhY2htZW50cy8iO3M6MjM6InBydl9tc2dfbWF4X2F0dGFjaG1lbnRzIjtzOjE6IjMiO3M6MjI6InBydl9tc2dfYXR0YWNoX21heHNpemUiO3M6MzoiMjUwIjtzOjIwOiJwcnZfbXNnX2F0dGFjaF90b3RhbCI7czozOiIxMDAiO3M6MTk6InBydl9tc2dfaHRtbF9mb3JtYXQiO3M6NDoic2FmZSI7czoxODoicHJ2X21zZ19hdXRvX2xpbmtzIjtzOjE6InkiO3M6MTc6InBydl9tc2dfbWF4X2NoYXJzIjtzOjQ6IjYwMDAiO3M6MTk6Im1lbWJlcmxpc3Rfb3JkZXJfYnkiO3M6MTE6InRvdGFsX3Bvc3RzIjtzOjIxOiJtZW1iZXJsaXN0X3NvcnRfb3JkZXIiO3M6NDoiZGVzYyI7czoyMDoibWVtYmVybGlzdF9yb3dfbGltaXQiO3M6MjoiMjAiO30=', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJ5IjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo1NjoiL3Zhci93d3cvdmhvc3RzL3Nob290LXRoZS1tb29uLmNvLnVrL2h0dHBkb2NzL3RlbXBsYXRlcy8iO30=', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NTU6Ii92YXIvd3d3L3Zob3N0cy9zaG9vdC10aGUtbW9vbi5jby51ay9odHRwZG9jcy9pbmRleC5waHAiO3M6MzI6ImEyZWJhOGFkMDI3ODcwMGRkNGU5YzY0NjEzZTIyZTJmIjt9', 'YToxOntpOjE7YTozOntzOjM6InVybCI7czozMjoiaHR0cDovL3d3dy5zaG9vdC10aGUtbW9vbi5jby51ay8iO3M6NDoidXJpcyI7YToxNjE6e2k6MTtzOjE6Ii8iO2k6MjE4O3M6MTQ6Ii9vdXItdGhpbmtpbmcvIjtpOjI7czoxMDoiL2NyZWF0aXZlLyI7aToxMztzOjE5OiIvY3JlYXRpdmUvYnJhbmRpbmcvIjtpOjEwO3M6MjA6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvIjtpOjMyO3M6Mjg6Ii9jcmVhdGl2ZS9hZHZlcnRpc2luZy1jb21tcy8iO2k6MTE7czoyMjoiL2NyZWF0aXZlL3N0b3JlLWNvbW1zLyI7aTo0O3M6MTM6Ii9waG90b2dyYXBoeS8iO2k6MTg7czoxODoiL3Bob3RvZ3JhcGh5L2Zvb2QvIjtpOjE5O3M6MjE6Ii9waG90b2dyYXBoeS9wcm9kdWN0LyI7aToyMDtzOjIyOiIvcGhvdG9ncmFwaHkvbG9jYXRpb24vIjtpOjIxO3M6MjI6Ii9waG90b2dyYXBoeS9wZXJzb25hbC8iO2k6MjE5O3M6MjQ6Ii9waG90b2dyYXBoeS9mYWNpbGl0aWVzLyI7aTozO3M6OToiL2RpZ2l0YWwvIjtpOjE0O3M6MTg6Ii9kaWdpdGFsL3dlYnNpdGVzLyI7aToxNTtzOjE0OiIvZGlnaXRhbC9lY3JtLyI7aToyMTE7czoxNzoiL2RpZ2l0YWwvYmFubmVycy8iO2k6MjAyO3M6MTY6Ii9kaWdpdGFsL21vdGlvbi8iO2k6MjMwO3M6MjE6Ii9kaWdpdGFsL21vYmlsZS1hcHBzLyI7aTo1O3M6MTQ6Ii9nZXQtaW4tdG91Y2gvIjtpOjMxO3M6MTQ6Ii90ZXN0aW1vbmlhbHMvIjtpOjM4O3M6MzI6Ii9jcmVhdGl2ZS9icmFuZGluZy9icmFuZGluZy1vbmUvIjtpOjM5O3M6MzI6Ii9jcmVhdGl2ZS9icmFuZGluZy9icmFuZGluZy10d28vIjtpOjExNDtzOjI2OiIvY3JlYXRpdmUvYnJhbmRpbmcvYnJhbmQzLyI7aToxMTU7czoyNjoiL2NyZWF0aXZlL2JyYW5kaW5nL2JyYW5kNC8iO2k6MTE2O3M6MjY6Ii9jcmVhdGl2ZS9icmFuZGluZy9icmFuZDUvIjtpOjExODtzOjI2OiIvY3JlYXRpdmUvYnJhbmRpbmcvYnJhbmQ3LyI7aToxODU7czo0ODoiL2NyZWF0aXZlL2JyYW5kaW5nL2tpZHMtaG9saWRheS1wZXQtY2x1Yi1zdW1tZXIvIjtpOjE4NjtzOjMxOiIvY3JlYXRpdmUvYnJhbmRpbmcvdmVyby1nZWxhdG8vIjtpOjE4NztzOjMyOiIvY3JlYXRpdmUvYnJhbmRpbmcvcG91bmQtYmFrZXJ5LyI7aToxODk7czoyNjoiL2NyZWF0aXZlL2JyYW5kaW5nL3NheWVycy8iO2k6MTkwO3M6MzE6Ii9jcmVhdGl2ZS9icmFuZGluZy9nb29kbmVzcy1tZS8iO2k6MjIxO3M6NDE6Ii9jcmVhdGl2ZS9icmFuZGluZy9yb2Itd2luc3RhbmxleS1jeWNsZXMvIjtpOjMzO3M6MzQ6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvcGFja2FnaW5nLW9uZS8iO2k6MzQ7czozNDoiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmctdHdvLyI7aTozNTtzOjM2OiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZy10aHJlZS8iO2k6MzY7czozNToiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmctZm91ci8iO2k6NDE7czo0MDoiL2NyZWF0aXZlL3BhY2thZ2luZy9saXRlcmF0dXJlLWl0ZW0tb25lLyI7aToxMjQ7czozMToiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmc2LyI7aToxMjU7czozMToiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmc3LyI7aToxMjY7czozMToiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmc4LyI7aToxMjc7czozMToiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmc5LyI7aToxMjg7czozMjoiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmcxMC8iO2k6MTI5O3M6MzI6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvcGFja2FnaW5nMTEvIjtpOjEzMDtzOjMyOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzEyLyI7aToxMzE7czozMjoiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmcxMy8iO2k6MTMyO3M6MzI6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvcGFja2FnaW5nMTQvIjtpOjEzMztzOjMyOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzE1LyI7aToxMzQ7czozMjoiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmcxNi8iO2k6MTcwO3M6MzA6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvbWFycmlhZ2VzLyI7aToxNzE7czo0MDoiL2NyZWF0aXZlL3BhY2thZ2luZy9oYXJib3VyLXNtb2tlLWhvdXNlLyI7aToxNzI7czo0MToiL2NyZWF0aXZlL3BhY2thZ2luZy9kZXZpbGlzaGx5LWRlbGljaW91cy8iO2k6MTczO3M6Mjg6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvZGlzb3R0by8iO2k6MTc0O3M6MzM6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvcGluay1wYW50aGVyLyI7aToyMjI7czozMzoiL2NyZWF0aXZlL3BhY2thZ2luZy9qdWNlZS1kcmlua3MvIjtpOjQwO3M6NDE6Ii9jcmVhdGl2ZS9hZHZlcnRpc2luZy1jb21tcy9icm9jaHVyZS1vbmUvIjtpOjEyMDtzOjMyOiIvY3JlYXRpdmUvYWR2ZXJ0aXNpbmctY29tbXMvZG00LyI7aToxMzg7czo0MjoiL2NyZWF0aXZlL2FkdmVydGlzaW5nLWNvbW1zL3N0b3JlLWNvbW1zLTYvIjtpOjE0MTtzOjQyOiIvY3JlYXRpdmUvYWR2ZXJ0aXNpbmctY29tbXMvc3RvcmUtY29tbXMtOS8iO2k6MTc1O3M6Mzc6Ii9jcmVhdGl2ZS9hZHZlcnRpc2luZy1jb21tcy9sYWtlbGFuZC8iO2k6MTc4O3M6Mzk6Ii9jcmVhdGl2ZS9hZHZlcnRpc2luZy1jb21tcy9sYWtlbGFuZC0zLyI7aToxNzk7czozNjoiL2NyZWF0aXZlL2FkdmVydGlzaW5nLWNvbW1zL3Rob21zb24vIjtpOjE4MDtzOjM4OiIvY3JlYXRpdmUvYWR2ZXJ0aXNpbmctY29tbXMvdGhvbXNvbi0xLyI7aToxODE7czozODoiL2NyZWF0aXZlL2FkdmVydGlzaW5nLWNvbW1zL3Rob21zb24tMi8iO2k6MjIzO3M6NDQ6Ii9jcmVhdGl2ZS9hZHZlcnRpc2luZy1jb21tcy9yeW1hbi1jYXRhbG9ndWUvIjtpOjIyNDtzOjQ0OiIvY3JlYXRpdmUvYWR2ZXJ0aXNpbmctY29tbXMvbGFrZWxhbmQtZ2VybWFuLyI7aToyMjU7czo0NjoiL2NyZWF0aXZlL2FkdmVydGlzaW5nLWNvbW1zL2xha2VsYW5kLW91dGRvb3JzLyI7aTo0MjtzOjQwOiIvY3JlYXRpdmUvc3RvcmUtY29tbXMvcG9pbnQtb2Ytc2FsZS1vbmUvIjtpOjQzO3M6NDA6Ii9jcmVhdGl2ZS9zdG9yZS1jb21tcy9wb2ludC1vZi1zYWxlLXR3by8iO2k6MTM1O3M6MzY6Ii9jcmVhdGl2ZS9zdG9yZS1jb21tcy9zdG9yZS1jb21tcy0zLyI7aToxMzc7czozNjoiL2NyZWF0aXZlL3N0b3JlLWNvbW1zL3N0b3JlLWNvbW1zLTUvIjtpOjEzOTtzOjM2OiIvY3JlYXRpdmUvc3RvcmUtY29tbXMvc3RvcmUtY29tbXMtNy8iO2k6MTQwO3M6MzY6Ii9jcmVhdGl2ZS9zdG9yZS1jb21tcy9zdG9yZS1jb21tcy04LyI7aToxNjk7czozNToiL2NyZWF0aXZlL3N0b3JlLWNvbW1zL3BpY2stYW5kLW1peC8iO2k6MjI2O3M6NTA6Ii9jcmVhdGl2ZS9zdG9yZS1jb21tcy90aG9tc29uLWFpcndheXMtZWF0ZXJ5LW1lbnUvIjtpOjIyNztzOjUzOiIvY3JlYXRpdmUvc3RvcmUtY29tbXMvdGhvbXNvbi1haXJ3YXlzLWluZmxpZ2h0LWd1aWRlLyI7aToxNDI7czoyNDoiL3Bob3RvZ3JhcGh5L2Zvb2QvZm9vZDEvIjtpOjE0MztzOjI0OiIvcGhvdG9ncmFwaHkvZm9vZC9mb29kMi8iO2k6MTQ0O3M6MjQ6Ii9waG90b2dyYXBoeS9mb29kL2Zvb2QzLyI7aToxNDc7czoyNDoiL3Bob3RvZ3JhcGh5L2Zvb2QvZm9vZDYvIjtpOjE0ODtzOjI0OiIvcGhvdG9ncmFwaHkvZm9vZC9mb29kNy8iO2k6MTQ5O3M6MjQ6Ii9waG90b2dyYXBoeS9mb29kL2Zvb2Q4LyI7aToxNTA7czoyNDoiL3Bob3RvZ3JhcGh5L2Zvb2QvZm9vZDkvIjtpOjE1MTtzOjI1OiIvcGhvdG9ncmFwaHkvZm9vZC9mb29kMTAvIjtpOjE1MztzOjI1OiIvcGhvdG9ncmFwaHkvZm9vZC9mb29kMTIvIjtpOjE1NDtzOjMwOiIvcGhvdG9ncmFwaHkvcHJvZHVjdC9wcm9kdWN0MS8iO2k6MTU1O3M6MzA6Ii9waG90b2dyYXBoeS9wcm9kdWN0L3Byb2R1Y3QyLyI7aToxNTY7czozMDoiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvcHJvZHVjdDMvIjtpOjE1NztzOjMwOiIvcGhvdG9ncmFwaHkvcHJvZHVjdC9wcm9kdWN0NC8iO2k6MTU4O3M6MzA6Ii9waG90b2dyYXBoeS9wcm9kdWN0L3Byb2R1Y3Q1LyI7aToxNTk7czozMDoiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvcHJvZHVjdDYvIjtpOjE2NztzOjMyOiIvcGhvdG9ncmFwaHkvcHJvZHVjdC9wcm9kdWN0Ni0xLyI7aToxNjg7czozMDoiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvcHJvZHVjdDcvIjtpOjE5MTtzOjMzOiIvcGhvdG9ncmFwaHkvcHJvZHVjdC9zcGFpbi1zaGlydC8iO2k6MjIwO3M6NDU6Ii9waG90b2dyYXBoeS9mYWNpbGl0aWVzL3RoaXMtaXMtYS10ZXN0LWltYWdlLyI7aTo0OTtzOjM5OiIvZGlnaXRhbC93ZWJzaXRlcy9kZXZpbGlzaGx5LWRlbGljaW91cy8iO2k6NTA7czo0MDoiL2RpZ2l0YWwvd2Vic2l0ZXMvbWFjcGhpZS1vZi1nbGVuYmVydmllLyI7aToxOTI7czozNzoiL2RpZ2l0YWwvd2Vic2l0ZXMvY2F0ZXJmb3JjZS13ZWJzaXRlLyI7aToxOTM7czozNToiL2RpZ2l0YWwvd2Vic2l0ZXMvY2hlZnMtc2VsZWN0aW9ucy8iO2k6MTk0O3M6NTc6Ii9kaWdpdGFsL3dlYnNpdGVzL2RldmlsaXNobHktZGVsaWNpb3VzLWZhY2Vib29rLWNyZWF0aXZlLyI7aToxOTU7czo0MjoiL2RpZ2l0YWwvd2Vic2l0ZXMvbWFjcGhpZS1vZi1nbGVuYmVydmllLTEvIjtpOjE5NjtzOjI4OiIvZGlnaXRhbC93ZWJzaXRlcy9tYXJyaWFnZXMvIjtpOjE5NztzOjI5OiIvZGlnaXRhbC93ZWJzaXRlcy9tZWFkb3d2YWxlLyI7aToxOTk7czozNjoiL2RpZ2l0YWwvd2Vic2l0ZXMvc2F5ZXJzLXRoZS1iYWtlcnkvIjtpOjIwMDtzOjM2OiIvZGlnaXRhbC93ZWJzaXRlcy9zY29ycGlvLXdvcmxkd2lkZS8iO2k6MjM1O3M6NDA6Ii9kaWdpdGFsL3dlYnNpdGVzL3JvYi13aW5zdGFubGV5LWN5Y2xlcy8iO2k6MjA3O3M6MjU6Ii9kaWdpdGFsL2Vjcm0vbWVhZG93dmFsZS8iO2k6MjA4O3M6MzA6Ii9kaWdpdGFsL2Vjcm0vZmx5LXRob21hcy1jb29rLyI7aToyMDk7czoyNzoiL2RpZ2l0YWwvZWNybS9wZXRzLWF0LWhvbWUvIjtpOjIxMztzOjIwOiIvZGlnaXRhbC9lY3JtL3J5bWFuLyI7aToyMTQ7czoyNzoiL2RpZ2l0YWwvZWNybS9wb3VuZC1iYWtlcnkvIjtpOjIyODtzOjI5OiIvZGlnaXRhbC9lY3JtL2p1Y2VlLWludGVybmFsLyI7aToyMjk7czoyNDoiL2RpZ2l0YWwvZWNybS9yaWRlLWF3YXkvIjtpOjIxMjtzOjMwOiIvZGlnaXRhbC9iYW5uZXJzL3BldHMtYXQtaG9tZS8iO2k6MjE1O3M6MzE6Ii9kaWdpdGFsL2Jhbm5lcnMvbWl4ZWQtYmFubmVycy8iO2k6MjMyO3M6NDQ6Ii9kaWdpdGFsL2Jhbm5lcnMvYXVudC1iZXNzaWVzLW5pc2EtY2FtcGFpZ24vIjtpOjIzMztzOjQwOiIvZGlnaXRhbC9iYW5uZXJzL3J5bWFuLWVzY2FsYXRvci1wYW5lbHMvIjtpOjIwMztzOjI5OiIvZGlnaXRhbC9tb3Rpb24vanVjZWUtaWRlbnRzLyI7aToyMDQ7czozNToiL2RpZ2l0YWwvbW90aW9uL2p1Y2VlLWJhdC1hbmQtYmFsbC8iO2k6MjA1O3M6Mjc6Ii9kaWdpdGFsL21vdGlvbi9qdWNlZS1uZXRzLyI7aToyMDY7czozMToiL2RpZ2l0YWwvbW90aW9uL2p1Y2VlLXVtYnJlbGxhLyI7aToyMzE7czozMzoiL2RpZ2l0YWwvbW9iaWxlLWFwcHMvdGhvbWFzLWNvb2svIjtpOjIzNjtzOjM5OiIvcGhvdG9ncmFwaHkvcGVyc29uYWwvY29udGFjdC10aGFua3lvdS8iO2k6MjM3O3M6MjM6Ii9nZXQtaW4tdG91Y2gtdGhhbmt5b3UvIjtpOjIzODtzOjMyOiIvcGhvdG9ncmFwaHkvbG9jYXRpb24vaGFwZS10b3lzLyI7aToyMzk7czozNzoiL3Bob3RvZ3JhcGh5L2xvY2F0aW9uL2ZyYW5raWUtYmVubnlzLyI7aToyNDA7czoyODoiL3Bob3RvZ3JhcGh5L2xvY2F0aW9uL3ZpbXRvLyI7aToyNDE7czoyOToiL3Bob3RvZ3JhcGh5L2xvY2F0aW9uL2V1cm9wZS8iO2k6MjQyO3M6MzE6Ii9waG90b2dyYXBoeS9sb2NhdGlvbi9jaGlxdWl0by8iO2k6MjQzO3M6MzE6Ii9waG90b2dyYXBoeS9sb2NhdGlvbi90cnktdGhhaS8iO2k6MjQ0O3M6NDA6Ii9waG90b2dyYXBoeS9sb2NhdGlvbi9hcm91bmQtbWFuY2hlc3Rlci8iO2k6MjQ1O3M6Mzg6Ii9waG90b2dyYXBoeS9sb2NhdGlvbi9vdGhlci1sb2NhdGlvbnMvIjtpOjI0NjtzOjMzOiIvcGhvdG9ncmFwaHkvbG9jYXRpb24vaG91ci1nbGFzcy8iO2k6MjQ3O3M6MzM6Ii9waG90b2dyYXBoeS9wcm9kdWN0L2pkLXdpbGxpYW1zLyI7aToyNDg7czo0NDoiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvdGhvbXNvbi1haXJ3YXlzLWRyaW5rcy8iO2k6MjQ5O3M6NDY6Ii9waG90b2dyYXBoeS9wcm9kdWN0L3BldHMtYXQtaG9tZS1kb2ctZmFzaGlvbi8iO2k6MjUwO3M6MjY6Ii9waG90b2dyYXBoeS9mb29kL2NoaWNrZW4vIjtpOjI1MTtzOjI4OiIvcGhvdG9ncmFwaHkvZm9vZC9icmVha2Zhc3QvIjtpOjI1MjtzOjI3OiIvcGhvdG9ncmFwaHkvZm9vZC9uYXBvbGluYS8iO2k6MjUzO3M6Mzk6Ii9waG90b2dyYXBoeS9mb29kL3RoZS1yZXN0YXVyYW50LWdyb3VwLyI7aToyNTQ7czo0NzoiL3Bob3RvZ3JhcGh5L2Zvb2Qvd2FoYWNhLW1leGljYW4tbWFya2V0LWVhdGluZy8iO2k6MjU1O3M6NDA6Ii9waG90b2dyYXBoeS9mb29kL3Rlc3QtcHJvZHVjZS1vdmVyaGVhZC8iO2k6MjU2O3M6MzM6Ii9waG90b2dyYXBoeS9mb29kL2xpZmVzdHlsZS1mb29kLyI7aToyNTg7czozMjoiL3Bob3RvZ3JhcGh5L2Zvb2QvamFmZmEtcmVjaXBlcy8iO2k6MjU5O3M6NDI6Ii9waG90b2dyYXBoeS9mb29kL3Rlc3QtZm9vZC1jaGVlc2Utb3JhbmdlLyI7aToyNjA7czoyODoiL3Bob3RvZ3JhcGh5L2Zvb2QvYmVlZi10ZXN0LyI7aToyNjE7czoyODoiL3Bob3RvZ3JhcGh5L2Zvb2Qvc3RhcmJ1Y2tzLyI7aToyNjI7czoyOToiL2NyZWF0aXZlL2JyYW5kaW5nL2tva28tbm9pci8iO2k6MjYzO3M6Mjk6Ii9jcmVhdGl2ZS9icmFuZGluZy9yaWRlLWF3YXkvIjtpOjI2NDtzOjI1OiIvY3JlYXRpdmUvYnJhbmRpbmcvanVjZWUvIjtpOjI2NjtzOjQzOiIvY3JlYXRpdmUvc3RvcmUtY29tbXMvcnltYW4tdGhlLXN0YXRpb25lcnMvIjtpOjI2NztzOjI2OiIvY3JlYXRpdmUvc3RvcmUtY29tbXMvcm9iLyI7aToyNjg7czozNToiL2NyZWF0aXZlL3N0b3JlLWNvbW1zL2F1bnQtYmVzc2llcy8iO2k6MjY5O3M6Mjg6Ii9kaWdpdGFsL3dlYnNpdGVzL3JpZGUtYXdheS8iO2k6MjcwO3M6MzY6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvd2FpbndyaWdodHMtY2F0LyI7aToyNzE7czozNjoiL2NyZWF0aXZlL3BhY2thZ2luZy9qdWNlZS1wYWNrYWdpbmcvIjtpOjI3MjtzOjQzOiIvY3JlYXRpdmUvcGFja2FnaW5nL3dhaW53cmlnaHRzLWdyYWluLWZyZWUvIjtpOjI3MztzOjM4OiIvY3JlYXRpdmUvcGFja2FnaW5nL3dhaW53cmlnaHRzLXB1cHB5LyI7aToyNzQ7czo0ODoiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvcmlkZS1hd2F5LWNvdW50cnktY2xvdGhpbmcvIjtpOjI3NTtzOjMxOiIvZGlnaXRhbC93ZWJzaXRlcy9qdWNlZS1kcmlua3MvIjtpOjI3NjtzOjU0OiIvZGlnaXRhbC9tb2JpbGUtYXBwcy90aG9tYXMtY29vay1pbi1mbGlnaHQtcmV0YWlsLWFwcC8iO31zOjk6InRlbXBsYXRlcyI7YToxNjE6e2k6MTtzOjE6IjEiO2k6MjtzOjE6IjMiO2k6NDtzOjE6IjMiO2k6MztzOjE6IjMiO2k6MjE7czoxOiI1IjtpOjE5O3M6MToiNSI7aToxODtzOjE6IjUiO2k6MjA7czoxOiI1IjtpOjE0O3M6MToiNSI7aToxMTtzOjE6IjUiO2k6MTM7czoxOiI1IjtpOjEwO3M6MToiNSI7aTo1O3M6MToiNCI7aTozMTtzOjE6IjEiO2k6MzI7czoxOiI1IjtpOjMzO3M6MToiNSI7aTozNDtzOjE6IjUiO2k6MzU7czoxOiI1IjtpOjM2O3M6MToiNSI7aTozODtzOjE6IjUiO2k6Mzk7czoxOiI1IjtpOjQwO3M6MToiNSI7aTo0MTtzOjE6IjUiO2k6NDI7czoxOiI1IjtpOjQzO3M6MToiNSI7aTo0OTtzOjE6IjUiO2k6NTA7czoxOiI1IjtpOjExNDtzOjE6IjUiO2k6MTE1O3M6MToiNSI7aToxMTY7czoxOiI1IjtpOjExODtzOjE6IjUiO2k6MTIwO3M6MToiNSI7aToxMjQ7czoxOiI1IjtpOjEyNTtzOjE6IjUiO2k6MTI2O3M6MToiNSI7aToxMjc7czoxOiI1IjtpOjEyODtzOjE6IjUiO2k6MTI5O3M6MToiNSI7aToxMzA7czoxOiI1IjtpOjEzMTtzOjE6IjUiO2k6MTMyO3M6MToiNSI7aToxMzM7czoxOiI1IjtpOjEzNDtzOjE6IjUiO2k6MTM1O3M6MToiNSI7aToxMzc7czoxOiI1IjtpOjEzODtzOjE6IjUiO2k6MTM5O3M6MToiNSI7aToxNDA7czoxOiI1IjtpOjE0MTtzOjE6IjUiO2k6MTQyO3M6MToiNSI7aToxNDM7czoxOiI1IjtpOjE0NDtzOjE6IjUiO2k6MTQ3O3M6MToiNSI7aToxNDg7czoxOiI1IjtpOjE0OTtzOjE6IjUiO2k6MTUwO3M6MToiNSI7aToxNTE7czoxOiI1IjtpOjE1MztzOjE6IjUiO2k6MTU0O3M6MToiNSI7aToxNTU7czoxOiI1IjtpOjE1NjtzOjE6IjUiO2k6MTU3O3M6MToiNSI7aToxNTg7czoxOiI1IjtpOjE1OTtzOjE6IjUiO2k6MTY3O3M6MToiNSI7aToxNjg7czoxOiI1IjtpOjE2OTtzOjE6IjUiO2k6MTcwO3M6MToiNSI7aToxNzE7czoxOiI1IjtpOjE3MjtzOjE6IjUiO2k6MTczO3M6MToiNSI7aToxNzQ7czoxOiI1IjtpOjE3NTtzOjE6IjUiO2k6MTc4O3M6MToiNSI7aToxNzk7czoxOiI1IjtpOjE4MDtzOjE6IjUiO2k6MTgxO3M6MToiNSI7aToxODU7czoxOiI1IjtpOjE4NjtzOjE6IjUiO2k6MTg3O3M6MToiNSI7aToxODk7czoxOiI1IjtpOjE5MDtzOjE6IjUiO2k6MTkxO3M6MToiNSI7aToxOTI7czoxOiI1IjtpOjE5MztzOjE6IjUiO2k6MTk0O3M6MToiNSI7aToxOTU7czoxOiI1IjtpOjE5NjtzOjE6IjUiO2k6MTk3O3M6MToiNSI7aToxOTk7czoxOiI1IjtpOjIwMDtzOjE6IjUiO2k6MjAyO3M6MToiNSI7aToyMDM7czoxOiI1IjtpOjIwNDtzOjE6IjUiO2k6MjA1O3M6MToiNSI7aToyMDY7czoxOiI1IjtpOjE1O3M6MToiNSI7aToyMDc7czoxOiI1IjtpOjIwODtzOjE6IjUiO2k6MjA5O3M6MToiNSI7aToyMTE7czoxOiI1IjtpOjIxMjtzOjE6IjUiO2k6MjEzO3M6MToiNSI7aToyMTQ7czoxOiI1IjtpOjIxNTtzOjE6IjUiO2k6MjE4O3M6MjoiMTAiO2k6MjE5O3M6MToiNSI7aToyMjA7czoxOiI1IjtpOjIyMTtzOjE6IjUiO2k6MjIyO3M6MToiNSI7aToyMjM7czoxOiI1IjtpOjIyNDtzOjE6IjUiO2k6MjI1O3M6MToiNSI7aToyMjY7czoxOiI1IjtpOjIyNztzOjE6IjUiO2k6MjI4O3M6MToiNSI7aToyMjk7czoxOiI1IjtpOjIzMDtzOjE6IjUiO2k6MjMxO3M6MToiNSI7aToyMzI7czoxOiI1IjtpOjIzMztzOjE6IjUiO2k6MjM1O3M6MToiNSI7aToyMzY7czoxOiIzIjtpOjIzNztzOjI6IjEyIjtpOjIzODtzOjE6IjUiO2k6MjM5O3M6MToiNSI7aToyNDA7czoxOiI1IjtpOjI0MTtzOjE6IjUiO2k6MjQyO3M6MToiNSI7aToyNDM7czoxOiI1IjtpOjI0NDtzOjE6IjUiO2k6MjQ1O3M6MToiNSI7aToyNDY7czoxOiI1IjtpOjI0NztzOjE6IjUiO2k6MjQ4O3M6MToiNSI7aToyNDk7czoxOiI1IjtpOjI1MDtzOjE6IjUiO2k6MjUxO3M6MToiNSI7aToyNTI7czoxOiI1IjtpOjI1MztzOjE6IjUiO2k6MjU0O3M6MToiNSI7aToyNTU7czoxOiI1IjtpOjI1NjtzOjE6IjUiO2k6MjU4O3M6MToiNSI7aToyNTk7czoxOiI1IjtpOjI2MDtzOjE6IjUiO2k6MjYxO3M6MToiNSI7aToyNjI7czoxOiI1IjtpOjI2MztzOjE6IjUiO2k6MjY0O3M6MToiNSI7aToyNjY7czoxOiI1IjtpOjI2NztzOjE6IjUiO2k6MjY4O3M6MToiNSI7aToyNjk7czoxOiI1IjtpOjI3MDtzOjE6IjUiO2k6MjcxO3M6MToiNSI7aToyNzI7czoxOiI1IjtpOjI3MztzOjE6IjUiO2k6Mjc0O3M6MToiNSI7aToyNzU7czoxOiI1IjtpOjI3NjtzOjE6IjUiO319fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `exp_snippets`
--

CREATE TABLE IF NOT EXISTS `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_specialty_templates`
--

CREATE TABLE IF NOT EXISTS `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `exp_specialty_templates`
--

INSERT INTO `exp_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`, `sync_time`) VALUES
(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}', '0000-00-00 00:00:00'),
(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n', '0000-00-00 00:00:00'),
(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}', '0000-00-00 00:00:00'),
(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}', '0000-00-00 00:00:00'),
(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}', '0000-00-00 00:00:00'),
(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}', '0000-00-00 00:00:00'),
(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `exp_stats`
--

CREATE TABLE IF NOT EXISTS `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_stats`
--

INSERT INTO `exp_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`) VALUES
(1, 1, 2, 2, 'Editor', 164, 0, 0, 0, 1393348920, 0, 0, 1396262576, 44, 1369144526, 1396267143);

-- --------------------------------------------------------

--
-- Table structure for table `exp_statuses`
--

CREATE TABLE IF NOT EXISTS `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_statuses`
--

INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`) VALUES
(1, 1, 1, 'open', 1, '009933'),
(2, 1, 1, 'closed', 2, '990000'),
(3, 1, 2, 'closed', 1, '990000'),
(4, 1, 2, 'draft', 2, 'B59A42'),
(5, 1, 2, 'submitted', 3, '3E6C89'),
(6, 1, 2, 'open', 4, '009933');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_groups`
--

CREATE TABLE IF NOT EXISTS `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_status_groups`
--

INSERT INTO `exp_status_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Statuses'),
(2, 1, 'Better Workflow');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure`
--

CREATE TABLE IF NOT EXISTS `exp_structure` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `listing_cid` int(6) unsigned NOT NULL DEFAULT '0',
  `lft` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rgt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dead` varchar(100) NOT NULL,
  `hidden` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure`
--

INSERT INTO `exp_structure` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `listing_cid`, `lft`, `rgt`, `dead`, `hidden`) VALUES
(0, 0, 0, 0, 0, 1, 46, 'root', 'n'),
(1, 1, 0, 6, 0, 2, 3, '', 'n'),
(1, 2, 0, 1, 0, 6, 15, '', 'n'),
(1, 3, 0, 1, 0, 28, 39, '', 'n'),
(1, 4, 0, 1, 0, 16, 27, '', 'n'),
(1, 5, 0, 1, 0, 40, 41, '', 'n'),
(1, 10, 2, 1, 12, 9, 10, '', 'n'),
(1, 11, 2, 1, 14, 13, 14, '', 'n'),
(1, 13, 2, 1, 15, 7, 8, '', 'n'),
(1, 14, 3, 3, 20, 29, 30, '', 'n'),
(1, 15, 3, 3, 19, 31, 32, '', 'n'),
(1, 18, 4, 1, 7, 17, 18, '', 'n'),
(1, 19, 4, 1, 8, 19, 20, '', 'n'),
(1, 20, 4, 1, 9, 21, 22, '', 'n'),
(1, 21, 4, 1, 10, 23, 24, '', 'y'),
(1, 31, 0, 11, 0, 42, 43, '', 'n'),
(1, 32, 2, 1, 16, 11, 12, '', 'n'),
(1, 202, 3, 1, 22, 35, 36, '', 'n'),
(1, 211, 3, 1, 21, 33, 34, '', 'n'),
(1, 218, 0, 6, 0, 4, 5, '', 'n'),
(1, 219, 4, 1, 23, 25, 26, '', 'n'),
(1, 230, 3, 1, 24, 37, 38, '', 'n'),
(1, 237, 0, 1, 0, 44, 45, '', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_channels`
--

CREATE TABLE IF NOT EXISTS `exp_structure_channels` (
  `site_id` smallint(5) unsigned NOT NULL,
  `channel_id` mediumint(8) unsigned NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `type` enum('page','listing','asset','unmanaged') NOT NULL DEFAULT 'unmanaged',
  `split_assets` enum('y','n') NOT NULL DEFAULT 'n',
  `show_in_page_selector` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`site_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_channels`
--

INSERT INTO `exp_structure_channels` (`site_id`, `channel_id`, `template_id`, `type`, `split_assets`, `show_in_page_selector`) VALUES
(1, 1, 3, 'page', 'n', 'y'),
(1, 2, 3, 'page', 'n', 'y'),
(1, 3, 3, 'page', 'n', 'y'),
(1, 4, 3, 'page', 'n', 'y'),
(1, 5, 1, 'page', 'n', 'y'),
(1, 6, 1, 'page', 'n', 'y'),
(1, 7, 5, 'listing', 'n', 'y'),
(1, 8, 5, 'listing', 'n', 'y'),
(1, 9, 5, 'listing', 'n', 'y'),
(1, 10, 5, 'listing', 'n', 'y'),
(1, 11, 1, 'page', 'n', 'y'),
(1, 12, 5, 'listing', 'n', 'y'),
(1, 13, 5, 'listing', 'n', 'y'),
(1, 14, 5, 'listing', 'n', 'y'),
(1, 15, 5, 'listing', 'n', 'y'),
(1, 16, 5, 'listing', 'n', 'y'),
(1, 17, 1, 'listing', 'n', 'y'),
(1, 18, 5, 'listing', 'n', 'y'),
(1, 19, 5, 'listing', 'n', 'y'),
(1, 20, 5, 'listing', 'n', 'y'),
(1, 21, 5, 'listing', 'n', 'y'),
(1, 22, 5, 'listing', 'n', 'y'),
(1, 23, 5, 'listing', 'n', 'y'),
(1, 24, 5, 'listing', 'n', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_listings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_listings` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `template_id` int(6) unsigned NOT NULL DEFAULT '0',
  `uri` varchar(75) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_listings`
--

INSERT INTO `exp_structure_listings` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `template_id`, `uri`) VALUES
(1, 33, 10, 12, 5, 'packaging-one'),
(1, 34, 10, 12, 5, 'packaging-two'),
(1, 35, 10, 12, 5, 'packaging-three'),
(1, 36, 10, 12, 5, 'packaging-four'),
(1, 38, 13, 15, 5, 'branding-one'),
(1, 39, 13, 15, 5, 'branding-two'),
(1, 40, 32, 16, 5, 'brochure-one'),
(1, 41, 10, 12, 5, 'literature-item-one'),
(1, 42, 11, 14, 5, 'point-of-sale-one'),
(1, 43, 11, 14, 5, 'point-of-sale-two'),
(1, 49, 14, 20, 5, 'devilishly-delicious'),
(1, 50, 14, 20, 5, 'macphie-of-glenbervie'),
(1, 114, 13, 15, 5, 'brand3'),
(1, 115, 13, 15, 5, 'brand4'),
(1, 116, 13, 15, 5, 'brand5'),
(1, 118, 13, 15, 5, 'brand7'),
(1, 120, 32, 16, 5, 'dm4'),
(1, 124, 10, 12, 5, 'packaging6'),
(1, 125, 10, 12, 5, 'packaging7'),
(1, 126, 10, 12, 5, 'packaging8'),
(1, 127, 10, 12, 5, 'packaging9'),
(1, 128, 10, 12, 5, 'packaging10'),
(1, 129, 10, 12, 5, 'packaging11'),
(1, 130, 10, 12, 5, 'packaging12'),
(1, 131, 10, 12, 5, 'packaging13'),
(1, 132, 10, 12, 5, 'packaging14'),
(1, 133, 10, 12, 5, 'packaging15'),
(1, 134, 10, 12, 5, 'packaging16'),
(1, 135, 11, 14, 5, 'store-comms-3'),
(1, 137, 11, 14, 5, 'store-comms-5'),
(1, 138, 32, 16, 5, 'store-comms-6'),
(1, 139, 11, 14, 5, 'store-comms-7'),
(1, 140, 11, 14, 5, 'store-comms-8'),
(1, 141, 32, 16, 5, 'store-comms-9'),
(1, 142, 18, 7, 5, 'food1'),
(1, 143, 18, 7, 5, 'food2'),
(1, 144, 18, 7, 5, 'food3'),
(1, 147, 18, 7, 5, 'food6'),
(1, 148, 18, 7, 5, 'food7'),
(1, 149, 18, 7, 5, 'food8'),
(1, 150, 18, 7, 5, 'food9'),
(1, 151, 18, 7, 5, 'food10'),
(1, 153, 18, 7, 5, 'food12'),
(1, 154, 19, 8, 5, 'product1'),
(1, 155, 19, 8, 5, 'product2'),
(1, 156, 19, 8, 5, 'product3'),
(1, 157, 19, 8, 5, 'product4'),
(1, 158, 19, 8, 5, 'product5'),
(1, 159, 19, 8, 5, 'product6'),
(1, 167, 19, 8, 5, 'product6-1'),
(1, 168, 19, 8, 5, 'product7'),
(1, 169, 11, 14, 5, 'pick-and-mix'),
(1, 170, 10, 12, 5, 'marriages'),
(1, 171, 10, 12, 5, 'harbour-smoke-house'),
(1, 172, 10, 12, 5, 'devilishly-delicious'),
(1, 173, 10, 12, 5, 'disotto'),
(1, 174, 10, 12, 5, 'pink-panther'),
(1, 175, 32, 16, 5, 'lakeland'),
(1, 178, 32, 16, 5, 'lakeland-3'),
(1, 179, 32, 16, 5, 'thomson'),
(1, 180, 32, 16, 5, 'thomson-1'),
(1, 181, 32, 16, 5, 'thomson-2'),
(1, 185, 13, 15, 5, 'kids-holiday-pet-club-summer'),
(1, 186, 13, 15, 5, 'vero-gelato'),
(1, 187, 13, 15, 5, 'pound-bakery'),
(1, 189, 13, 15, 5, 'sayers'),
(1, 190, 13, 15, 5, 'goodness-me'),
(1, 191, 19, 8, 5, 'spain-shirt'),
(1, 192, 14, 20, 5, 'caterforce-website'),
(1, 193, 14, 20, 5, 'chefs-selections'),
(1, 194, 14, 20, 5, 'devilishly-delicious-facebook-creative'),
(1, 195, 14, 20, 5, 'macphie-of-glenbervie-1'),
(1, 196, 14, 20, 5, 'marriages'),
(1, 197, 14, 20, 5, 'meadowvale'),
(1, 199, 14, 20, 5, 'sayers-the-bakery'),
(1, 200, 14, 20, 5, 'scorpio-worldwide'),
(1, 203, 202, 22, 5, 'jucee-idents'),
(1, 204, 202, 22, 5, 'jucee-bat-and-ball'),
(1, 205, 202, 22, 5, 'jucee-nets'),
(1, 206, 202, 22, 5, 'jucee-umbrella'),
(1, 207, 15, 19, 5, 'meadowvale'),
(1, 208, 15, 19, 5, 'fly-thomas-cook'),
(1, 209, 15, 19, 5, 'pets-at-home'),
(1, 212, 211, 21, 5, 'pets-at-home'),
(1, 213, 15, 19, 5, 'ryman'),
(1, 214, 15, 19, 5, 'pound-bakery'),
(1, 215, 211, 21, 5, 'mixed-banners'),
(1, 220, 219, 23, 5, 'this-is-a-test-image'),
(1, 221, 13, 15, 5, 'rob-winstanley-cycles'),
(1, 222, 10, 12, 5, 'jucee-drinks'),
(1, 223, 32, 16, 5, 'ryman-catalogue'),
(1, 224, 32, 16, 5, 'lakeland-german'),
(1, 225, 32, 16, 5, 'lakeland-outdoors'),
(1, 226, 11, 14, 5, 'thomson-airways-eatery-menu'),
(1, 227, 11, 14, 5, 'thomson-airways-inflight-guide'),
(1, 228, 15, 19, 5, 'jucee-internal'),
(1, 229, 15, 19, 5, 'ride-away'),
(1, 231, 230, 24, 5, 'thomas-cook'),
(1, 232, 211, 21, 5, 'aunt-bessies-nisa-campaign'),
(1, 233, 211, 21, 5, 'ryman-escalator-panels'),
(1, 235, 14, 20, 5, 'rob-winstanley-cycles'),
(1, 236, 21, 10, 3, 'contact-thankyou'),
(1, 238, 20, 9, 5, 'hape-toys'),
(1, 239, 20, 9, 5, 'frankie-bennys'),
(1, 240, 20, 9, 5, 'vimto'),
(1, 241, 20, 9, 5, 'europe'),
(1, 242, 20, 9, 5, 'chiquito'),
(1, 243, 20, 9, 5, 'try-thai'),
(1, 244, 20, 9, 5, 'around-manchester'),
(1, 245, 20, 9, 5, 'other-locations'),
(1, 246, 20, 9, 5, 'hour-glass'),
(1, 247, 19, 8, 5, 'jd-williams'),
(1, 248, 19, 8, 5, 'thomson-airways-drinks'),
(1, 249, 19, 8, 5, 'pets-at-home-dog-fashion'),
(1, 250, 18, 7, 5, 'chicken'),
(1, 251, 18, 7, 5, 'breakfast'),
(1, 252, 18, 7, 5, 'napolina'),
(1, 253, 18, 7, 5, 'the-restaurant-group'),
(1, 254, 18, 7, 5, 'wahaca-mexican-market-eating'),
(1, 255, 18, 7, 5, 'test-produce-overhead'),
(1, 256, 18, 7, 5, 'lifestyle-food'),
(1, 258, 18, 7, 5, 'jaffa-recipes'),
(1, 259, 18, 7, 5, 'test-food-cheese-orange'),
(1, 260, 18, 7, 5, 'beef-test'),
(1, 261, 18, 7, 5, 'starbucks'),
(1, 262, 13, 15, 5, 'koko-noir'),
(1, 263, 13, 15, 5, 'ride-away'),
(1, 264, 13, 15, 5, 'jucee'),
(1, 266, 11, 14, 5, 'ryman-the-stationers'),
(1, 267, 11, 14, 5, 'rob'),
(1, 268, 11, 14, 5, 'aunt-bessies'),
(1, 269, 14, 20, 5, 'ride-away'),
(1, 270, 10, 12, 5, 'wainwrights-cat'),
(1, 271, 10, 12, 5, 'jucee-packaging'),
(1, 272, 10, 12, 5, 'wainwrights-grain-free'),
(1, 273, 10, 12, 5, 'wainwrights-puppy'),
(1, 274, 19, 8, 5, 'ride-away-country-clothing'),
(1, 275, 14, 20, 5, 'jucee-drinks'),
(1, 276, 230, 24, 5, 'thomas-cook-in-flight-retail-app');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_members`
--

CREATE TABLE IF NOT EXISTS `exp_structure_members` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `nav_state` text,
  PRIMARY KEY (`site_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_members`
--

INSERT INTO `exp_structure_members` (`member_id`, `site_id`, `nav_state`) VALUES
(1, 1, 'false');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_settings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `var` varchar(60) NOT NULL,
  `var_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `exp_structure_settings`
--

INSERT INTO `exp_structure_settings` (`id`, `site_id`, `var`, `var_value`) VALUES
(1, 0, 'action_ajax_move', '48'),
(2, 0, 'module_id', '24'),
(10, 1, 'redirect_on_login', 'y'),
(11, 1, 'redirect_on_publish', 'y'),
(12, 1, 'hide_hidden_templates', 'y'),
(13, 1, 'add_trailing_slash', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_templates`
--

CREATE TABLE IF NOT EXISTS `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `exp_templates`
--

INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(1, 1, 1, 'index', 'y', 'webpage', '{lv_doc_header}\n    </head>\n    <body class="home">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	{lv_image_matrix}\n					\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n         	{exp:channel:entries channel="studio" disable="{lv_disable_basic}" dynamic="off"}\n	  		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_studio_shots}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_studio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_studio_shots}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_blockquote}\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n', '', 1353078861, 1, 'n', 0, '', 'n', 'n', 'o', 116134),
(3, 1, 1, 'landing', 'y', 'webpage', '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|advertising-comms|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}						\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy|mobile-apps" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>', '', 1386261945, 1, 'n', 0, '', 'n', 'n', 'o', 21824),
(4, 1, 1, 'get-in-touch', 'y', 'webpage', '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message" return="get-in-touch-thankyou"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>', '', 1383041505, 1, 'n', 0, '', 'n', 'n', 'o', 5189),
(5, 1, 1, 'discipline', 'y', 'webpage', '{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n        		<h1>{title}</h1>\n					{lv_folio_nav}\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		\n        		<div id="content1" class="folio">\n\n	        		<ul>\n         		\n				  {if segment_2=="product"}\n         			<video id="my_video_1" class="video-js vjs-default-skin" controls\n     				  preload="auto" width="640" height="360" poster="/images/video/Protected-Photo-Shoot.jpg"\n     				  data-setup="{}">\n     				  <source src="/images/video/Protected-Photo-Shoot.m4v" type=''video/mp4''>\n     				  <source src="/images/video/Protected-Photo-Shoot.webm" type=''video/webm''>\n     				  <source src="/images/video/Protected-Photo-Shoot.ogv" type=''video/ogv''>\n     				</video>\n				{/if} \n\n         		{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\n         	\n         				{if segment_2=="motion"}\n         				\n         				<!-- <video id="my_video_1" class="video-js vjs-default-skin" controls\n         				  preload="auto" width="640" height="360" poster="/images/video/bat-and-ball.jpg"\n         				  data-setup="{}">\n         				  <source src="/images/video/bat-and-ball.mp4" type=''video/mp4''>\n         				  <source src="/images/video/bat-and-ball.webm" type=''video/webm''>\n         				  <source src="/images/video/bat-and-ball.ogv" type=''video/ogv''>\n         				</video> -->\n         				\n         				{video_upload}\n		        			<li id="id{entry_id}">\n								<video id="my_video_1" class="video-js vjs-default-skin" controls\n								  preload="auto" width="640" height="360" poster="{start_image}"\n								  data-setup="{}">\n								  <source src="{version_m4v}" type=''video/mp4''>\n								  <source src="{version_webm}" type=''video/webm''>\n								  <source src="{version_ogv}" type=''video/ogv''>\n								</video>\n		        			</li>\n     					{/video_upload}\n         				\n         				{if:else}\n\n						{cf_portfolio_images}\n\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  {/if}\n	        			\n	        		\n          	{/exp:channel:entries}\n	        		</ul>\n       		</div>\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n   \n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{embed="sites/discipline-thums"}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n', '', 1386342810, 1, 'n', 0, '', 'n', 'n', 'o', 17656),
(6, 1, 1, 'food', 'y', 'webpage', 'Food\n{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		{lv_folio_nav}\n        		\n			{exp:channel:entries channel="{segment_1}" disable="{lv_disable_basic}" dynamic="off"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{lv_thumbs_nav}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n', NULL, 1351517711, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(7, 1, 1, 'Generic', 'n', 'webpage', 'Index\n{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		\n         	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" dynamic="off"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_blockquote}\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n', '', 1351521337, 0, 'n', 0, '', 'n', 'n', 'o', 0),
(8, 1, 1, 'thumb-nav', 'y', 'webpage', '<?php\n\n	$url = $_SERVER[''REQUEST_URI''];\n	$segments = explode("/",$url);\n	if ($segments[2] != "") {\n		$pagination = $segments[2];\n		$offset = substr($pagination, 1);\n	} else {\n		$offset = 0;\n	}\n	\n?>\n\n<ul id="switch" class="pie clearfix">\n\n	{if segment_1=="creative"}\n	{exp:channel:entries channel="packaging|branding|literature|point_of_sale" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n	{if segment_1=="photography"}\n	{exp:channel:entries channel="food|product|location|set" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n	{if segment_1=="digital"}\n	{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n</ul>', '', 1353078861, 1, 'n', 0, '', 'n', 'y', 'i', 0),
(9, 1, 1, 'discipline-thums', 'y', 'webpage', '<?php\n\n	$url = $_SERVER[''REQUEST_URI''];\n	$segments = explode("/",$url);\n	if ($segments[3] != "") {\n		$pagination = $segments[3];\n		$offset = substr($pagination, 1);\n	} else {\n		$offset = 0;\n	}\n	\n?>\n\n<ul id="switch" class="pie clearfix">\n\n	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	\n</ul>', NULL, 1353077402, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(10, 1, 1, 'thinking', 'y', 'webpage', '{lv_doc_header}\n    </head>\n    <body class="thinking">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	<!--<div id="matrix-sml">-->\n				 		<ul id="slides-single">\n                            {cf_rotating_image_blocks}\n                            <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="199" height="145" crop="yes" allow_scale_larger="yes"}</li>\n                            {/cf_rotating_image_blocks}\n				 		</ul>\n				 	<!--</div>-->\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			{homepage_pull_quote}\n        		</div>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n', '', 1360754387, 1, 'n', 0, '', 'n', 'n', 'o', 3182),
(11, 1, 1, 'sitemap', 'y', 'xml', '<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n\n	{!-- Home --}\n	<url> \n		<loc>{site_url}</loc> \n		<lastmod>{exp:stats}{last_entry_date format="{DATE_W3C}"}{/exp:stats}</lastmod> \n		<changefreq>always</changefreq> \n		<priority>1.0</priority> \n	</url> \n\n\n	{!--Static Content Pages --}\n	{exp:channel:entries channel="not websites|banners|eCRM|Motion|packaging|studio|food|branding|direct-mail|testimonials|personal|store-comms|product|location" disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{if change_frequency}{change_frequency}{if:else}Daily{/if}</changefreq> \n		<priority>{if priority}{priority}{if:else}0.5{/if}</priority> \n	</url> \n	{/exp:channel:entries}\n\n	{!--Generated Content Pages --}\n	{exp:channel:entries channel="websites|banners|eCRM|Motion|packaging|studio|food|branding|direct-mail|testimonials|personal|store-comms|product|location" disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{if change_frequency}{change_frequency}{if:else}Monthly{/if}</changefreq> \n		<priority>{if priority}{priority}{if:else}0.1{/if}</priority> \n	</url> \n	{/exp:channel:entries}\n\n</urlset> ', NULL, 1382968126, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(12, 1, 1, 'get-in-touch-thankyou', 'y', 'webpage', '{lv_doc_header}\n\n\n    </head>\n    <body class="contact-thankyou">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		<div id="intro">\n			<p>Thank you for time. Your message will be reviewed by a member of the Shoot the Moon team very shortly.</p>\n			<p>Please feel free to call us on <span><a href="tel:0161 205 3311">0161 205 3311</a></span> if your enquiry is urgent.</p>\n			<p><a href="/">This link</a> will take you back to the homepage from where you can continue to browse our website.</p>\n			</div>\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>', '', 1383040629, 1, 'n', 0, '', 'n', 'n', 'o', 89);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_template_groups`
--

INSERT INTO `exp_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`) VALUES
(1, 1, 'sites', 1, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_throttle`
--

CREATE TABLE IF NOT EXISTS `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_prefs`
--

CREATE TABLE IF NOT EXISTS `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `exp_upload_prefs`
--

INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`) VALUES
(1, 1, 'Photography', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/photography/', 'http://www.shoot-the-moon.co.uk/images/photography/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(2, 1, 'Creative', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/creative/', 'http://www.shoot-the-moon.co.uk/images/creative/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(3, 1, 'Digital', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/digital/', 'http://www.shoot-the-moon.co.uk/images/digital/', 'all', '', '', '', '', '', '', '', '', '', '', NULL),
(4, 1, 'Air Side', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/airside/', 'http://www.shoot-the-moon.co.uk/images/airside/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(5, 1, 'Miscellaneous (Storage)', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/misc/', 'http://www.shoot-the-moon.co.uk/images/misc/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(6, 1, 'Content', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/content-images/', 'http://www.shoot-the-moon.co.uk/images/content-images/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(7, 1, 'Rotating Image Panels (all)', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/rotating_image_panels/', 'http://www.shoot-the-moon.co.uk/images/rotating_image_panels/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(8, 1, 'Studio (Company)', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/images/studio/', 'http://www.shoot-the-moon.co.uk/images/studio/', 'img', '', '', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_wygwam_configs`
--

CREATE TABLE IF NOT EXISTS `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_wygwam_configs`
--

INSERT INTO `exp_wygwam_configs` (`config_id`, `config_name`, `settings`) VALUES
(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTE6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7aToxMDtzOjY6IlNvdXJjZSI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9'),
(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ=='),
(3, 'Intro Block', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Mzp7aTowO3M6NDoiQm9sZCI7aToxO3M6NjoiSXRhbGljIjtpOjI7czo5OiJUZXh0Q29sb3IiO31zOjY6ImhlaWdodCI7czozOiIxMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6Im4iO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ=='),
(4, 'Portfolio Supporting Text', 'YTo1OntzOjc6InRvb2xiYXIiO2E6NDp7aTowO3M6NjoiRm9ybWF0IjtpOjE7czo0OiJCb2xkIjtpOjI7czo2OiJJdGFsaWMiO2k6MztzOjk6IlRleHRDb2xvciI7fXM6NjoiaGVpZ2h0IjtzOjI6IjczIjtzOjE0OiJyZXNpemVfZW5hYmxlZCI7czoxOiJuIjtzOjExOiJjb250ZW50c0NzcyI7YTowOnt9czoxMDoidXBsb2FkX2RpciI7czowOiIiO30=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_zoo_flexible_admin_menus`
--

CREATE TABLE IF NOT EXISTS `exp_zoo_flexible_admin_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT NULL,
  `group_id` int(4) DEFAULT NULL,
  `nav` text,
  `autopopulate` tinyint(1) DEFAULT NULL,
  `hide_sidebar` tinyint(1) DEFAULT NULL,
  `startpage` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
