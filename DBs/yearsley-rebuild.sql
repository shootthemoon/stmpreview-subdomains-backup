-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2014 at 11:51 AM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yearsley-rebuild`
--

-- --------------------------------------------------------

--
-- Table structure for table `exp_accessories`
--

CREATE TABLE IF NOT EXISTS `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(255) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_accessories`
--

INSERT INTO `exp_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`) VALUES
(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'),
(2, 'Structure_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '3.3.10'),
(3, 'Developer_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.7.1');

-- --------------------------------------------------------

--
-- Table structure for table `exp_actions`
--

CREATE TABLE IF NOT EXISTS `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `exp_actions`
--

INSERT INTO `exp_actions` (`action_id`, `class`, `method`) VALUES
(1, 'Comment', 'insert_new_comment'),
(2, 'Comment_mcp', 'delete_comment_notification'),
(3, 'Comment', 'comment_subscribe'),
(4, 'Comment', 'edit_comment'),
(5, 'Email', 'send_email'),
(6, 'Safecracker', 'submit_entry'),
(7, 'Safecracker', 'combo_loader'),
(8, 'Search', 'do_search'),
(9, 'Channel', 'insert_new_entry'),
(10, 'Channel', 'filemanager_endpoint'),
(11, 'Channel', 'smiley_pop'),
(12, 'Member', 'registration_form'),
(13, 'Member', 'register_member'),
(14, 'Member', 'activate_member'),
(15, 'Member', 'member_login'),
(16, 'Member', 'member_logout'),
(17, 'Member', 'retrieve_password'),
(18, 'Member', 'reset_password'),
(19, 'Member', 'send_member_email'),
(20, 'Member', 'update_un_pw'),
(21, 'Member', 'member_search'),
(22, 'Member', 'member_delete'),
(23, 'Rte', 'get_js'),
(24, 'Assets_mcp', 'upload_file'),
(25, 'Assets_mcp', 'get_files_view_by_folders'),
(26, 'Assets_mcp', 'get_props'),
(27, 'Assets_mcp', 'save_props'),
(28, 'Assets_mcp', 'get_ordered_files_view'),
(29, 'Assets_mcp', 'get_session_id'),
(30, 'Assets_mcp', 'start_index'),
(31, 'Assets_mcp', 'perform_index'),
(32, 'Assets_mcp', 'finish_index'),
(33, 'Assets_mcp', 'get_s3_buckets'),
(34, 'Assets_mcp', 'get_gc_buckets'),
(35, 'Assets_mcp', 'get_rs_containers'),
(36, 'Assets_mcp', 'move_folder'),
(37, 'Assets_mcp', 'rename_folder'),
(38, 'Assets_mcp', 'create_folder'),
(39, 'Assets_mcp', 'delete_folder'),
(40, 'Assets_mcp', 'view_file'),
(41, 'Assets_mcp', 'move_file'),
(42, 'Assets_mcp', 'delete_file'),
(43, 'Assets_mcp', 'view_thumbnail'),
(44, 'Assets_mcp', 'build_sheet'),
(45, 'Assets_mcp', 'get_selected_files'),
(46, 'Structure', 'ajax_move_set_data');

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_files`
--

CREATE TABLE IF NOT EXISTS `exp_assets_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `folder_id` int(10) unsigned NOT NULL,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `source_id` int(10) unsigned DEFAULT NULL,
  `filedir_id` int(4) unsigned DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` int(10) unsigned NOT NULL,
  `alt_text` tinytext,
  `caption` tinytext,
  `author` tinytext,
  `desc` text,
  `location` tinytext,
  `keywords` text,
  `date_modified` int(10) unsigned DEFAULT NULL,
  `kind` varchar(5) DEFAULT NULL,
  `width` int(2) DEFAULT NULL,
  `height` int(2) DEFAULT NULL,
  `size` int(3) DEFAULT NULL,
  `search_keywords` text,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `unq_folder_id__file_name` (`folder_id`,`file_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `exp_assets_files`
--

INSERT INTO `exp_assets_files` (`file_id`, `folder_id`, `source_type`, `source_id`, `filedir_id`, `file_name`, `title`, `date`, `alt_text`, `caption`, `author`, `desc`, `location`, `keywords`, `date_modified`, `kind`, `width`, `height`, `size`, `search_keywords`) VALUES
(1, 1, 'ee', NULL, 1, 'banner-cser.jpg', NULL, 1368619077, NULL, NULL, NULL, NULL, NULL, NULL, 1368619077, 'image', 1280, 326, 59249, 'banner-cser.jpg'),
(2, 1, 'ee', NULL, 1, 'home_bean_banner.jpg', NULL, 1372341824, NULL, NULL, NULL, NULL, NULL, NULL, 1372341824, 'image', 635, 327, 89136, 'home_bean_banner.jpg'),
(3, 1, 'ee', NULL, 1, 'home_logistics_banner.jpg', NULL, 1372341848, NULL, NULL, NULL, NULL, NULL, NULL, 1372341848, 'image', 635, 327, 54273, 'home_logistics_banner.jpg'),
(4, 1, 'ee', NULL, 1, 'banner-about-us.jpg', NULL, 1374161817, NULL, NULL, NULL, NULL, NULL, NULL, 1374161817, 'image', 1280, 326, 217631, 'banner-about-us.jpg'),
(5, 1, 'ee', NULL, 1, 'banner-news.jpg', NULL, 1374164069, NULL, NULL, NULL, NULL, NULL, NULL, 1374164069, 'image', 1280, 326, 123237, 'banner-news.jpg'),
(6, 1, 'ee', NULL, 1, 'banner-directors.jpg', NULL, 1374164859, NULL, NULL, NULL, NULL, NULL, NULL, 1374164859, 'image', 1280, 326, 60713, 'banner-directors.jpg'),
(7, 1, 'ee', NULL, 1, 'banner-structure.jpg', NULL, 1374164875, NULL, NULL, NULL, NULL, NULL, NULL, 1374164875, 'image', 1280, 326, 173302, 'banner-structure.jpg'),
(8, 1, 'ee', NULL, 1, 'banner-locations.jpg', NULL, 1374164922, NULL, NULL, NULL, NULL, NULL, NULL, 1374164922, 'image', 1280, 326, 66153, 'banner-locations.jpg'),
(9, 1, 'ee', NULL, 1, 'banner-history.jpg', NULL, 1374164964, NULL, NULL, NULL, NULL, NULL, NULL, 1374164964, 'image', 1280, 326, 271557, 'banner-history.jpg'),
(10, 1, 'ee', NULL, 1, 'banner-our-business.jpg', NULL, 1374165003, NULL, NULL, NULL, NULL, NULL, NULL, 1374165003, 'image', 1280, 326, 86114, 'banner-our-business.jpg'),
(11, 1, 'ee', NULL, 1, 'banner-logistics.jpg', NULL, 1374165104, NULL, NULL, NULL, NULL, NULL, NULL, 1374165104, 'image', 1280, 326, 86114, 'banner-logistics.jpg'),
(12, 1, 'ee', NULL, 1, 'banner-food-sales.jpg', NULL, 1374165121, NULL, NULL, NULL, NULL, NULL, NULL, 1374165121, 'image', 1280, 326, 99473, 'banner-food-sales.jpg'),
(13, 1, 'ee', NULL, 1, 'banner-belfield.jpg', NULL, 1374165532, NULL, NULL, NULL, NULL, NULL, NULL, 1374165532, 'image', 1280, 326, 155110, 'banner-belfield.jpg'),
(14, 1, 'ee', NULL, 1, 'banner-icepak.jpg', NULL, 1374165562, NULL, NULL, NULL, NULL, NULL, NULL, 1374165562, 'image', 1280, 326, 197504, 'banner-icepak.jpg'),
(15, 1, 'ee', NULL, 1, 'banner-lucky-red.jpeg', NULL, 1374165577, NULL, NULL, NULL, NULL, NULL, NULL, 1374165577, 'image', 1280, 326, 147841, 'banner-lucky-red.jpeg'),
(16, 1, 'ee', NULL, 1, 'banner-careers.jpg', NULL, 1374165606, NULL, NULL, NULL, NULL, NULL, NULL, 1374165606, 'image', 1280, 326, 48841, 'banner-careers.jpg'),
(17, 1, 'ee', NULL, 1, 'banner-contact-us.jpg', NULL, 1374165641, NULL, NULL, NULL, NULL, NULL, NULL, 1374165641, 'image', 1280, 326, 38831, 'banner-contact-us.jpg'),
(18, 1, 'ee', NULL, 1, 'banner-cookies.jpg', NULL, 1374166319, NULL, NULL, NULL, NULL, NULL, NULL, 1374166319, 'image', 1280, 326, 86881, 'banner-cookies.jpg'),
(19, 1, 'ee', NULL, 1, 'banner-sitemap.jpg', NULL, 1374166350, NULL, NULL, NULL, NULL, NULL, NULL, 1374166350, 'image', 1280, 326, 46331, 'banner-sitemap.jpg'),
(20, 1, 'ee', NULL, 1, 'YearsleyGroupAboutUs.jpg', NULL, 1374233314, NULL, NULL, NULL, NULL, NULL, NULL, 1374233314, 'image', 250, 124, 18224, 'YearsleyGroupAboutUs.jpg'),
(21, 1, 'ee', NULL, 1, 'panel-belfield.jpg', NULL, 1374241396, NULL, NULL, NULL, NULL, NULL, NULL, 1374241396, 'image', 278, 139, 14731, 'panel-belfield.jpg'),
(22, 1, 'ee', NULL, 1, 'panel-icepak.jpg', NULL, 1374242961, NULL, NULL, NULL, NULL, NULL, NULL, 1374242961, 'image', 278, 139, 17422, 'panel-icepak.jpg'),
(23, 1, 'ee', NULL, 1, 'panel-lucky-red.jpg', NULL, 1374242985, NULL, NULL, NULL, NULL, NULL, NULL, 1374242985, 'image', 278, 139, 16866, 'panel-lucky-red.jpg'),
(24, 1, 'ee', NULL, 1, 'ob-belfield2.jpg', NULL, 1374482581, NULL, NULL, NULL, NULL, NULL, NULL, 1374482581, 'image', 128, 129, 8067, 'ob-belfield2.jpg'),
(25, 1, 'ee', NULL, 1, 'ob-belfield.jpg', NULL, 1374482581, NULL, NULL, NULL, NULL, NULL, NULL, 1374482581, 'image', 128, 129, 8877, 'ob-belfield.jpg'),
(26, 1, 'ee', NULL, 1, '0b-food-sales2.jpg', NULL, 1374482581, NULL, NULL, NULL, NULL, NULL, NULL, 1374482581, 'image', 128, 129, 9209, '0b-food-sales2.jpg'),
(27, 1, 'ee', NULL, 1, 'ob-food-sales.jpg', NULL, 1374482581, NULL, NULL, NULL, NULL, NULL, NULL, 1374482581, 'image', 128, 129, 7383, 'ob-food-sales.jpg'),
(28, 1, 'ee', NULL, 1, 'ob-icepak2.jpg', NULL, 1374482581, NULL, NULL, NULL, NULL, NULL, NULL, 1374482581, 'image', 128, 129, 10532, 'ob-icepak2.jpg'),
(29, 1, 'ee', NULL, 1, 'ob-icepak.jpg', NULL, 1374482581, NULL, NULL, NULL, NULL, NULL, NULL, 1374482581, 'image', 128, 129, 8781, 'ob-icepak.jpg'),
(30, 1, 'ee', NULL, 1, 'ob-logistics.jpg', NULL, 1374482582, NULL, NULL, NULL, NULL, NULL, NULL, 1374482582, 'image', 128, 129, 8125, 'ob-logistics.jpg'),
(31, 1, 'ee', NULL, 1, 'ob-logistics2.jpg', NULL, 1374482582, NULL, NULL, NULL, NULL, NULL, NULL, 1374482582, 'image', 128, 129, 10644, 'ob-logistics2.jpg'),
(32, 1, 'ee', NULL, 1, 'ob-lucky-red.jpg', NULL, 1374482582, NULL, NULL, NULL, NULL, NULL, NULL, 1374482582, 'image', 128, 128, 17490, 'ob-lucky-red.jpg'),
(33, 1, 'ee', NULL, 1, 'ob-lucky-red2.jpg', NULL, 1374482582, NULL, NULL, NULL, NULL, NULL, NULL, 1374482582, 'image', 128, 129, 7173, 'ob-lucky-red2.jpg'),
(34, 1, 'ee', NULL, 1, 'cser3.jpg', NULL, 1374574424, NULL, NULL, NULL, NULL, NULL, NULL, 1374574424, 'image', 128, 128, 11077, 'cser3.jpg'),
(35, 1, 'ee', NULL, 1, 'cser1.jpg', NULL, 1374574424, NULL, NULL, NULL, NULL, NULL, NULL, 1374574424, 'image', 128, 128, 29902, 'cser1.jpg'),
(36, 1, 'ee', NULL, 1, 'cser2.jpg', NULL, 1374574424, NULL, NULL, NULL, NULL, NULL, NULL, 1374574424, 'image', 128, 128, 36008, 'cser2.jpg'),
(37, 1, 'ee', NULL, 1, 'cser4.jpg', NULL, 1374574424, NULL, NULL, NULL, NULL, NULL, NULL, 1374574424, 'image', 128, 128, 17704, 'cser4.jpg'),
(38, 1, 'ee', NULL, 1, 'banner-cser2.jpg', NULL, 1374576509, NULL, NULL, NULL, NULL, NULL, NULL, 1374576509, 'image', 1280, 326, 219277, 'banner-cser2.jpg'),
(39, 1, 'ee', NULL, 1, 'royal-manchester-childrens-hospital.jpg', 'This years charity', 1374576682, NULL, NULL, NULL, NULL, NULL, NULL, 1374576682, 'image', 288, 199, 46652, 'royal-manchester-childrens-hospital.jpg,This years charity'),
(40, 1, 'ee', NULL, 1, 'Yearsley_2012_report_Final.pdf', NULL, 1374577985, NULL, NULL, NULL, 'For more information about our progress so far and targets for the future, view our most up to date CSER report.', NULL, NULL, 1374577985, 'pdf', NULL, NULL, 1579011, 'Yearsley_2012_report_Final.pdf,For more information about our progress so far and targets for the future, view our most up to date CSER report.'),
(41, 1, 'ee', NULL, 1, 'cser5.jpg', NULL, 1374581141, NULL, NULL, NULL, NULL, NULL, NULL, 1374581141, 'image', 128, 128, 18426, 'cser5.jpg'),
(42, 1, 'ee', NULL, 1, 'cser6.jpg', NULL, 1374581141, NULL, NULL, NULL, NULL, NULL, NULL, 1374581141, 'image', 128, 128, 24330, 'cser6.jpg'),
(43, 1, 'ee', NULL, 1, 'cser7.jpg', NULL, 1374581141, NULL, NULL, NULL, NULL, NULL, NULL, 1374581141, 'image', 128, 128, 8642, 'cser7.jpg'),
(44, 1, 'ee', NULL, 1, 'cser8.jpg', NULL, 1374581141, NULL, NULL, NULL, NULL, NULL, NULL, 1374581141, 'image', 128, 128, 20003, 'cser8.jpg'),
(45, 1, 'ee', NULL, 1, 'Dec2012.pdf', 'Newsletter December 2012 Edition', 1374588022, NULL, NULL, NULL, NULL, NULL, NULL, 1374588022, 'pdf', NULL, NULL, 949800, 'Dec2012.pdf,Newsletter December 2012 Edition'),
(46, 1, 'ee', NULL, 1, 'May2013_Newsletter.pdf', 'Newsletter May 2013 Edition', 1374588022, NULL, NULL, NULL, NULL, NULL, NULL, 1374588022, 'pdf', NULL, NULL, 2278211, 'May2013_Newsletter.pdf,Newsletter May 2013 Edition'),
(47, 1, 'ee', NULL, 1, 'newsletter.pdf', 'Newsletter June 2013 Edition', 1374588022, NULL, NULL, NULL, NULL, NULL, NULL, 1374588022, 'pdf', NULL, NULL, 4503416, 'newsletter.pdf,Newsletter June 2013 Edition'),
(48, 1, 'ee', NULL, 1, 'Nov2012.pdf', 'Newsletter November 2012 Edition', 1374588022, NULL, NULL, NULL, NULL, NULL, NULL, 1374588022, 'pdf', NULL, NULL, 889847, 'Nov2012.pdf,Newsletter November 2012 Edition'),
(49, 1, 'ee', NULL, 1, 'Solar_Panels.jpg', NULL, 1374608016, NULL, NULL, NULL, NULL, NULL, NULL, 1374608016, 'image', 618, 335, 179397, 'Solar_Panels.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_folders`
--

CREATE TABLE IF NOT EXISTS `exp_assets_folders` (
  `folder_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `folder_name` varchar(255) NOT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  `filedir_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`folder_id`),
  UNIQUE KEY `unq_source_type__source_id__filedir_id__parent_id__folder_name` (`source_type`,`source_id`,`filedir_id`,`parent_id`,`folder_name`),
  UNIQUE KEY `unq_source_type__source_id__filedir_id__full_path` (`source_type`,`source_id`,`filedir_id`,`full_path`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_assets_folders`
--

INSERT INTO `exp_assets_folders` (`folder_id`, `source_type`, `folder_name`, `full_path`, `parent_id`, `source_id`, `filedir_id`) VALUES
(1, 'ee', 'Content', '', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_index_data`
--

CREATE TABLE IF NOT EXISTS `exp_assets_index_data` (
  `session_id` char(36) DEFAULT NULL,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `source_id` int(10) unsigned DEFAULT NULL,
  `offset` int(10) unsigned DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `filesize` int(10) unsigned DEFAULT NULL,
  `type` enum('file','folder') DEFAULT NULL,
  `record_id` int(10) unsigned DEFAULT NULL,
  UNIQUE KEY `unq__session_id__source_type__source_id__offset` (`session_id`,`source_type`,`source_id`,`offset`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_rackspace_access`
--

CREATE TABLE IF NOT EXISTS `exp_assets_rackspace_access` (
  `connection_key` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `storage_url` varchar(255) NOT NULL,
  `cdn_url` varchar(255) NOT NULL,
  PRIMARY KEY (`connection_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_selections`
--

CREATE TABLE IF NOT EXISTS `exp_assets_selections` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `col_id` int(6) unsigned DEFAULT NULL,
  `row_id` int(10) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `sort_order` int(4) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  KEY `file_id` (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `col_id` (`col_id`),
  KEY `row_id` (`row_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_assets_selections`
--

INSERT INTO `exp_assets_selections` (`file_id`, `entry_id`, `field_id`, `col_id`, `row_id`, `var_id`, `sort_order`, `is_draft`) VALUES
(30, 14, 5, NULL, NULL, NULL, 1, 0),
(31, 14, 5, NULL, NULL, NULL, 0, 0),
(27, 15, 5, NULL, NULL, NULL, 0, 0),
(26, 15, 5, NULL, NULL, NULL, 1, 0),
(25, 16, 5, NULL, NULL, NULL, 0, 0),
(24, 16, 5, NULL, NULL, NULL, 1, 0),
(29, 17, 5, NULL, NULL, NULL, 0, 0),
(28, 17, 5, NULL, NULL, NULL, 1, 0),
(32, 18, 5, NULL, NULL, NULL, 0, 0),
(33, 18, 5, NULL, NULL, NULL, 1, 0),
(37, 20, 5, NULL, NULL, NULL, 3, 0),
(34, 20, 5, NULL, NULL, NULL, 2, 0),
(36, 20, 5, NULL, NULL, NULL, 1, 0),
(35, 20, 5, NULL, NULL, NULL, 0, 0),
(39, 19, 6, 6, 4, NULL, 0, 0),
(40, 19, 6, 5, 4, NULL, 0, 0),
(44, 4, 5, NULL, NULL, NULL, 3, 0),
(43, 4, 5, NULL, NULL, NULL, 2, 0),
(42, 4, 5, NULL, NULL, NULL, 1, 0),
(41, 4, 5, NULL, NULL, NULL, 0, 0),
(40, 4, 6, 5, 5, NULL, 0, 0),
(45, 5, 7, NULL, NULL, NULL, 0, 0),
(46, 5, 7, NULL, NULL, NULL, 1, 0),
(47, 5, 7, NULL, NULL, NULL, 2, 0),
(48, 5, 7, NULL, NULL, NULL, 3, 0),
(40, 20, 6, 5, 6, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_sources`
--

CREATE TABLE IF NOT EXISTS `exp_assets_sources` (
  `source_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(2) NOT NULL DEFAULT 's3',
  `name` varchar(255) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  PRIMARY KEY (`source_id`),
  UNIQUE KEY `unq_source_type__source_id` (`source_type`,`source_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_captcha`
--

CREATE TABLE IF NOT EXISTS `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_categories`
--

CREATE TABLE IF NOT EXISTS `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_fields`
--

CREATE TABLE IF NOT EXISTS `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_field_data`
--

CREATE TABLE IF NOT EXISTS `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_groups`
--

CREATE TABLE IF NOT EXISTS `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_posts`
--

CREATE TABLE IF NOT EXISTS `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channels`
--

CREATE TABLE IF NOT EXISTS `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(255) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `exp_channels`
--

INSERT INTO `exp_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`) VALUES
(1, 1, 'homepage', 'Homepage', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1368619020, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(2, 1, 'about_us', 'About us', 'http://yearsleymay:8888/index.php', NULL, 'en', 5, 0, 1373973300, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(3, 1, 'our_businesses', 'Our Businesses', 'http://yearsleymay:8888/index.php', NULL, 'en', 5, 0, 1373979120, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(4, 1, 'business_structure', 'Business Structure', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1373979000, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(5, 1, 'directors', 'Directors', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1373979000, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(6, 1, 'locations', 'Locations', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1373979000, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(7, 1, 'history', 'History', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1373979000, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(8, 1, 'cser', 'CSER', 'http://yearsleymay:8888/index.php', NULL, 'en', 2, 0, 1373979180, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(9, 1, 'news_articles', 'News Articles', 'http://yearsleymay:8888/index.php', NULL, 'en', 4, 0, 1374251400, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(10, 1, 'news_landing', 'News Landing', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1373973300, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(11, 1, 'careers', 'Careers', 'http://yearsleymay:8888/index.php', NULL, 'en', 0, 0, 0, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(12, 1, 'careers_items', 'Careers Items', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1374671340, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(13, 1, 'contact_us', 'Contact Us', 'http://yearsleymay:8888/index.php', NULL, 'en', 0, 0, 0, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(14, 1, 'site_map', 'Site Map', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1373975400, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(15, 1, 'cookie_usage', 'Cookie Usage', 'http://yearsleymay:8888/index.php', NULL, 'en', 1, 0, 1373975340, 0, '', 1, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_data`
--

CREATE TABLE IF NOT EXISTS `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_data`
--

INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_2`, `field_ft_2`, `field_id_3`, `field_ft_3`, `field_id_4`, `field_ft_4`, `field_id_5`, `field_ft_5`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`) VALUES
(1, 1, 1, '<h2>\n	Home</h2>\n<p>\n	Yearsley Group is leading the way in frozen foods via its two divisions: logistics and food.</p>\n<p>\n	Yearsley Logistics is the UK&rsquo;s largest logistics service provider in the frozen food sector, as well as offering ambient, chilled and freight forwarding solutions.</p>\n<p>\n	Yearsley Food markets and supplies a full range of innovative frozen products to customers in all sectors of the marketplace including export.</p>', 'none', '', 'none', '', 'none', 'czowOiIiOw==', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(2, 1, 2, '<h1>\n	The Yearsley Group</h1>\n<figure style="text-align: center;">\n	<img alt="" src="{filedir_1}YearsleyGroupAboutUs.jpg" /></figure>\n<p>\n	Yearsley Group is leading the way in frozen foods via its two divisions: logistics and food sales.</p>\n<p>\n	Yearsley Logistics is the UK&rsquo;s largest logistics service provider in the frozen food sector, as well as offering ambient, chilled and freight forwarding solutions. It has 13 sites nationally, a total capacity of 335,000 pallets and a fleet of over 300 temperature controlled vehicles delivering nationwide.</p>\n<p>\n	Yearsley Food markets and supplies a full range of innovative frozen products to customers in all sectors of the marketplace including export. It also offers exclusive imported frozen foods under its Belfield brand and IcePak, the Group&rsquo;s specialist frozen seafood supplier serves the wholesale and restaurant market.</p>\n<p>\n	An industry recognised organisation, Yearsley Group has BRC and ISO 14001 accreditations and plays an active part in the British Frozen Food Federation and the RHA.</p>\n<p>\n	The future of the company is based on delivering a sustainable business model, focused on innovation, agility and customer service excellence. A commitment to re-investment has recently seen the &pound;20 million expansion of the Heywood Cold Store, which will double the capacity at head office to 80,000 pallets. In addition, the development of our first &lsquo;Super Hub&rsquo; consolidation centre in the Midlands, following the purchase of the Hams Hall depot, which brought the Cold Store network to 13 sites.</p>', 'none', '{filedir_1}banner-about-us.jpg', 'none', '', 'none', 'czowOiIiOw==', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(8, 1, 15, '<p>​</p>\n\n\n\n<p>&nbsp;</p>', 'none', '{filedir_1}banner-cookies.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(3, 1, 2, '<p>​THIS PAGE HAS NO CONTENT - IT JUST REDIRECTS INTO LOGISTICS PAGE</p>\n\n\n\n<p>&nbsp;</p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(4, 1, 2, '<h2>\n	&#8203;CSER</h2>\n<p>\n	As a family company, we run our business for the long term, in a transparent and responsible way. Because of this, we are naturally concerned about the environmental footprint we leave for future generations.</p>\n<p>\n	Our business has many impacts on the environment, local communities and other groups including our employees, suppliers and customers. To do this we have developed a CSER strategy, objectives and targets. This is successfully helping us to identify and develop ways to manage our business sustainably and responsibly.</p>\n<p>\n	One key tool in helping us manage CSER in our business is our Social and Environmental Management System. We worked hard to make the system robust and effective, which has been recognised with ISO14001 accreditation.</p>\n<p>\n	CSER is a broad topic so we are focussing on our biggest impacts, these include:</p>\n<ul>\n	<li>\n		Reducing travel miles &ndash; and food miles for our customers</li>\n	<li>\n		Reducing our carbon footprint on electricity and fuel by adopting many initiatives around operational efficiency as well as developing renewable technologies.</li>\n	<li>\n		Protecting our employees&#39; health and safety</li>\n	<li>\n		Looking at how we can provide more sustainable products, in particular fish</li>\n	<li>\n		For more information about our progress so far and targets for the future can be found in our CSER report.</li>\n</ul>', 'none', '{filedir_1}banner-cser2.jpg', 'none', '', 'none', 'czowOiIiOw==', 'none', 'cser5.jpg\ncser6.jpg\ncser7.jpg\ncser8.jpg', 'none', '1', 'none', '', 'none', '', 'none', '', 'none'),
(5, 1, 10, '<p>\n	&#8203;PLEASE SELECT EITHER <strong>ADD</strong> OR <strong>EDIT</strong> FROM THE PREVIOUS SCREEN</p>', 'none', '{filedir_1}banner-news.jpg', 'none', '', 'none', 'czowOiIiOw==', 'none', '', 'none', '', 'none', 'Dec2012.pdf\nMay2013_Newsletter.pdf\nnewsletter.pdf\nNov2012.pdf', 'none', '', 'none', '', 'none'),
(6, 1, 2, '', 'none', '{filedir_1}banner-careers.jpg', 'none', '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '1', 'none'),
(7, 1, 2, '<p>​</p>\n\n\n\n<p>&nbsp;</p>', 'none', '{filedir_1}banner-contact-us.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(9, 1, 14, '<p>​</p>\n\n\n\n<p>&nbsp;</p>', 'none', '{filedir_1}banner-sitemap.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(10, 1, 4, '<p>​</p>\n\n\n\n<p>&nbsp;</p>', 'none', '{filedir_1}banner-structure.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(11, 1, 5, '<p>​</p>\n\n\n\n<p>&nbsp;</p>', 'none', '{filedir_1}banner-directors.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(12, 1, 6, '<p>​</p>\n\n\n\n<p>&nbsp;</p>', 'none', '{filedir_1}banner-locations.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(13, 1, 7, '<p>​</p>\n\n\n\n<p>&nbsp;</p>', 'none', '{filedir_1}banner-history.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(14, 1, 3, '<h2>\n	&#8203;Logistics</h2>\n<p>\n	As the largest frozen food logistics service provider in the UK, we can offer customers large and small, day one for day two, 7 days a week into major retailers, as well as an un-rivalled foodservice distribution network.</p>', 'none', '{filedir_1}banner-logistics.jpg', 'none', '', 'none', 'czowOiIiOw==', 'none', 'ob-logistics.jpg\nob-logistics2.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(15, 1, 3, '<h2>\n	&#8203;Food Sales</h2>\n<p>\n	Supplying a comprehensive range of frozen foods nationwide to retail, foodservice and the cost sector, we have a reputation for outstanding customer service.</p>', 'none', '{filedir_1}banner-food-sales.jpg', 'none', '', 'none', 'czowOiIiOw==', 'none', '0b-food-sales2.jpg\nob-food-sales.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(16, 1, 3, '<h2>\n	&#8203;Belfield</h2>\n<p>\n	Supplying major retail, foodservice and industrial clients with a bespoke range of quality frozen products imported exclusively from our overseas manufacturing partners.</p>', 'none', '{filedir_1}banner-belfield.jpg', 'none', '', 'none', '1', 'none', 'ob-belfield2.jpg\nob-belfield.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(17, 1, 3, '<h2>\n	&#8203;IcePak</h2>\n<p>\n	Specialising in the global sourcing and supply of frozen seafood and fish, we provide an extensive range whilst maintaining price stability and quality.</p>', 'none', '{filedir_1}banner-icepak.jpg', 'none', '', 'none', '1', 'none', 'ob-icepak2.jpg\nob-icepak.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(18, 1, 3, '<h2>\n	&#8203;Lucky Red</h2>\n<p>\n	Wholesaler and distributor of frozen, chilled and ambient products to the Chinese market with a strong focus on seafood and poultry.</p>', 'none', '{filedir_1}banner-lucky-red.jpeg', 'none', '', 'none', '1', 'none', 'ob-lucky-red.jpg\nob-lucky-red2.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(19, 1, 8, '<h2>\n	Causes we support</h2>\n<ul>\n	<li>\n		Employee nominated charity &ndash; Royal Manchester Children&rsquo;s Hospital has benefited from various fundraising activities</li>\n	<li>\n		Donations sent to depot nominated local charities instead of corporate Christmas cards sent to clients.</li>\n	<li>\n		Links with local schools developed to support school projects &amp; show employment opportunities.</li>\n	<li>\n		Local sports teams and community initiatives supported with time and or money</li>\n	<li>\n		Encouraging employees to live a healthier life, reduced gym memberships offered, in-house events arranged</li>\n</ul>', 'none', '{filedir_1}banner-cser2.jpg', 'none', '', 'none', 'czowOiIiOw==', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none'),
(20, 1, 8, '<h2>\n	Our commitment to the environment</h2>\n<ul>\n	<li>\n		&#8203;Reduction of Electricity use by 8% by 2015</li>\n	<li>\n		Investment of &pound;5million on renewable energy at 8 out of 13 depots</li>\n	<li>\n		Reduced fuel usage by increasing pallets per vehicle (+5%yoy), decreasing empty running (-3%yoy) and improving driver performance (+3%yoy)</li>\n	<li>\n		Reduce CO2 emissions by 10% on both trailers &amp; units</li>\n	<li>\n		Reduce packaging used and office waste sent to landfill</li>\n	<li>\n		Source more fish from sustainable sources</li>\n	<li>\n		Implement an anti corruption policy</li>\n</ul>', 'none', '{filedir_1}banner-cser.jpg', 'none', '', 'none', 'czowOiIiOw==', 'none', 'cser3.jpg\ncser1.jpg\ncser2.jpg\ncser4.jpg', 'none', '1', 'none', '', 'none', '', 'none', '', 'none'),
(21, 1, 9, '<p>\n	Hot Cross Buns, Hot Cross Buns........ Yearsley Group was singing the traditional rhyme this Easter with more than 15 million buns stored in their cold stores.</p>\n<p>\n	Tens of thousands of pallets of Britain&#39;s finest treats were stored at two of Yearsley Group&#39;s depots in the Midlands. The Coleshill and Chesterfield cold stores were close to the manufacturing sites of these festive breads.</p>\n<p>\n	Tim Moran, Yearsley Group Sales Director said, "It is quite common for us to store seasonal lines, with stocks building up to high levels prior to the event and then delivered within a short period of time. Obvious examples are turkeys and mince pies at Christmas and ice cream in the summer. Our experience of this type of sale means we are very accomplished at it. Major benefits that we offer our clients include the fact that we can deliver on our own fleet of vehicles which means we have full control. Also NetStock our on-line information system is vital for both us and our clients at such times. Clients can view all the information they need about stocks and deliveries and inform us of any extra deliveries on-line and at real time. This obviously cuts down on time taken to contact an individual to get this information required."</p>\n<p>\n	15 Million Hot Cross buns seems like a mountain of bread but the systems Yearsley Group have in place ensured all our daughters and sons got theirs this Easter!!</p>', 'none', '', 'none', '', 'none', 'czowOiIiOw==', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(22, 1, 9, '<p>\n	Profiteroles, Gateaux, Roulades, Eclairs, Sponges, Slices .... Whatever dessert you need Mr King&#39;s has the answer.</p>\n<p>\n	Belfield launched the Mr Kings range in 2009 with just 1 product, the cream slice. The product was so successful obtaining many listings and consumer sales that more products were immediately added to the range. Subsequent launches now take the total range to 14 with more exciting products in development.</p>\n<p>\n	The relationship that Belfield has with its supplying partners means that if a client requests a bespoke product or a variation on a theme these can be developed, tested and added to the range very quickly.</p>\n<p>\n	So, what is the secret to the success? Firstly the range is of good quality but with a price point that won&#39;t stretch the pocket. The recommended retail prices are from &pound;1.00 to &pound;2.99. Secondly Mr King&#39;s range is filling a gap in the market with a product that people want. The products are the ideal dessert to be eaten at any time with a price point that means they don&#39;t have to be saved for the weekend or special occasions.</p>\n<p>\n	For further information on these and Belfield&#39;s other product ranges, Tel 01706 694610 , email enquiries@belfield.co.uk.or visit www.belfield.co.uk</p>', 'none', '', 'none', '', 'none', 'czowOiIiOw==', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(23, 1, 9, '<p>\n	Yearsley Group, the UK&#39;s largest cold storage and distribution provider, has appointed Trafford-based Russells Construction to carry out a refurbishment at its Grimsby depot. The &pound;500,000 contract will see the installation of a new 63,000 sq ft superflat, insulated concrete floor.</p>\n<p>\n	With a &pound;130m turnover Yearsley Group has 12 sites across the UK and has instructed Russells Construction to complete a number of contracts around the country over recent years, including a new 100,000 sq/ft cold store in the north east town of Seaham and works to their Birmingham and Gillingham stores. This latest 10-week project is set to start immediately.</p>\n<p>\n	Jonathan Baker Commercial Director at Yearsley Group said &#39;Our policy is to have efficient facilities and this investment is ensuring we maintain a sustainable supply chain.&#39;</p>\n<p>\n	Gareth Russell, construction director at Russells Construction, said: &#39;The Yearsley Group is a longstanding client of ours and we understand their business well. This new insulated floor will enable the Grimsby store to control storage conditions with even greater accuracy and improve energy efficiency.&#39;</p>', 'none', '', 'none', '', 'none', 'czowOiIiOw==', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(24, 1, 9, '<p><img alt="" src="{filedir_1}Solar_Panels.jpg" style="height:335px; width:618px" /></p>\n\n<p>Yearsley Group is set to submit a planning application for a new 160,000sq ft cold storage and distribution facility close to its UK headquarters in Heywood. The frozen food specialist hopes to acquire a neighbouring 13.6 acre plot to allow for the expansion of its North West operation, in a move that could create around 150 new jobs for the area.</p>\n\n<p>The site is part of the Hareshill Distribution Park and already has outline planning consent for development for employment use. This latest application, due to be registered with Rochdale Borough Council next week, covers details such as layout, design and landscaping.</p>\n\n<p>If approved, the plan would see investment of more than &pound;20m in the acquisition of the land and construction of a new cold store, which would be built to high environmental rating standards. The expansion forms part of the Yearsley Group&#39;s mid- to long-term growth strategy, complementing the existing Heywood operation which employs around 300 people.</p>\n\n<p>Harry Yearsley, managing director of Yearsley Group, said the proposal demonstrated the firm&#39;s commitment to the area. He said: "As our business continues to grow, we need to expand our current storage facility and distribution capacity to meet demand from customers and improve the efficiency of our operations.</p>\n\n<p>"We invested heavily in making Heywood the centre of our UK operation in 1999 and it is the obvious location for our future expansion. The site is ideal for logistics distribution with good links to the M66, and would support our existing facility. We have almost 300 employees in Heywood, with the vast majority living within a 10-mile radius, and we are committed to supporting the economy of the Heywood and wider Rochdale area through the recruitment of local staff."</p>\n\n<p>John Hudson, chief executive of the Rochdale Development Agency, said: "We have had a long association with the Yearsley Group and it is pleasing to see this important local company making plans for another major investment in the borough. The opportunity to create perhaps 150 new jobs is clearly welcome and helps consolidate the presence of this growing company in Heywood".</p>\n\n<p>Councillor Peter Williams, Rochdale&#39;s Cabinet member for Corporate Management and Economic Regeneration, added: "We&#39;ll need to examine the planning aspects of the proposed development, but we welcome Yearsley Group&#39;s commitment to investing in the borough and creating new jobs. It also shows once again that Heywood is well placed for accommodating successful and expanding businesses."</p>\n\n<p>Yearsley Group operates in two sectors: frozen food sales and cold storage and distribution. The new facility would expand the capacity of the storage and logistics division. The proposed building would be similar in style to the neighbouring facility, providing a floor space of around 160,000 sq ft. The height of the proposed building would be within the parameters set by the outline planning permission, and would be built to the environmental rating - BREEAM &#39;Very Good&#39; - standard. The plan includes provision for 111 car parking spaces. HGV parking and 20 employee cycle spaces will also be provided, with access via Hareshill Road and Hill Top Road. All current and new employees would be encouraged to respect the company&#39;s green travel plan, promoting car sharing, cycling and walking, and the use of public transport. Additional HGV traffic would be directed, as at present, along the distribution park&#39;s dedicated logistics route to the M66, avoiding Heywood&#39;s main residential areas.</p>', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(25, 1, 12, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_entries_autosave`
--

CREATE TABLE IF NOT EXISTS `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_fields`
--

CREATE TABLE IF NOT EXISTS `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `exp_channel_fields`
--

INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`) VALUES
(1, 1, 1, 'cf_body_text', 'Body Text', '', 'wygwam', '', '0', 0, 0, 10, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(2, 1, 1, 'cf_banner_image', 'Banner Image', '', 'file', '', '0', 0, 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjE6IjEiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(3, 1, 1, 'cf_homepage_banner_images', 'Homepage Banner Images', '', 'matrix', '', '0', 0, 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO319'),
(4, 1, 1, 'cf_logo_panel', 'Logo Panel Link', '', 'matrix', '', '0', 0, 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjMiO2k6MTtzOjE6IjQiO319'),
(5, 1, 1, 'cf_side_images', 'Side Images', '', 'assets', '', '0', 0, 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czo0OiJlZToxIjt9czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJsYXJnZSI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToieSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(6, 1, 1, 'cf_optional_side_panel_content', 'Optional Side Panel Content', '', 'matrix', '', '0', 0, 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 6, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtzOjE6IjUiO2k6MTtzOjE6IjYiO2k6MjtzOjE6IjciO319'),
(7, 1, 1, 'newsletter_downloads', 'Newsletter Downloads', 'Right click on the file icon and select ''Edit'' to add a Title.', 'assets', '', '0', 0, 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 7, 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czo0OiJlZToxIjt9czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToieSI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToieSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(8, 1, 1, 'career_post_information', 'Career Post Information', '', 'matrix', '', '0', 0, 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 8, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjQ6e2k6MDtzOjE6IjgiO2k6MTtzOjE6IjkiO2k6MjtzOjI6IjEwIjtpOjM7czoyOiIxMSI7fX0='),
(9, 1, 1, 'cf_careers_page_intro_text', 'Careers page introduction text', '', 'matrix', '', '0', 0, 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 9, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjI6IjEyIjtpOjE7czoyOiIxMyI7fX0=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_member_groups`
--

INSERT INTO `exp_channel_member_groups` (`group_id`, `channel_id`) VALUES
(6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_titles`
--

CREATE TABLE IF NOT EXISTS `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `exp_channel_titles`
--

INSERT INTO `exp_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`) VALUES
(1, 1, 1, 1, NULL, '127.0.0.1', 'Home', 'home', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1368619020, '2013', '05', '15', 0, 0, 20130723122406, 0, 0),
(2, 1, 2, 1, NULL, '127.0.0.1', 'About Us', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373973240, '2013', '07', '16', 0, 0, 20130722082802, 0, 0),
(3, 1, 2, 1, NULL, '127.0.0.1', 'Our Business', 'our-business', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373973240, '2013', '07', '16', 0, 0, 20130719133502, 0, 0),
(4, 1, 2, 1, NULL, '127.0.0.1', 'CSER Pledge', 'cser-pledge', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373973240, '2013', '07', '16', 0, 0, 20130723120824, 0, 0),
(5, 1, 10, 1, NULL, '127.0.0.1', 'Latest News', 'news-and-media', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373973300, '2013', '07', '16', 0, 0, 20130723140219, 0, 0),
(6, 1, 2, 1, NULL, '127.0.0.1', 'Careers', 'careers', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373973300, '2013', '07', '16', 0, 0, 20130724134009, 0, 0),
(7, 1, 2, 1, NULL, '127.0.0.1', 'Contact Us', 'contact-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373973300, '2013', '07', '16', 0, 0, 20130718165031, 0, 0),
(8, 1, 15, 1, NULL, '127.0.0.1', 'Cookie Usage', 'cookie-usage', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373975340, '2013', '07', '16', 0, 0, 20130718165203, 0, 0),
(9, 1, 14, 1, NULL, '127.0.0.1', 'Site Map', 'site-map', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373975400, '2013', '07', '16', 0, 0, 20130718165234, 0, 0),
(10, 1, 4, 1, NULL, '127.0.0.1', 'Business Structure', 'business-structure', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979000, '2013', '07', '16', 0, 0, 20130718162802, 0, 0),
(11, 1, 5, 1, NULL, '127.0.0.1', 'Directors', 'directors', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979000, '2013', '07', '16', 0, 0, 20130718162812, 0, 0),
(12, 1, 6, 1, NULL, '127.0.0.1', 'Locations', 'locations', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979000, '2013', '07', '16', 0, 0, 20130718162845, 0, 0),
(13, 1, 7, 1, NULL, '127.0.0.1', 'History', 'history', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979000, '2013', '07', '16', 0, 0, 20130718162928, 0, 0),
(14, 1, 3, 1, NULL, '127.0.0.1', 'Logistics', 'logistics', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979060, '2013', '07', '16', 0, 0, 20130722085104, 0, 0),
(15, 1, 3, 1, NULL, '127.0.0.1', 'Food Sales', 'food-sales', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979120, '2013', '07', '16', 0, 0, 20130722085133, 0, 0),
(16, 1, 3, 1, NULL, '127.0.0.1', 'Belfield', 'belfield', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979120, '2013', '07', '16', 0, 0, 20130722085206, 0, 0),
(17, 1, 3, 1, NULL, '127.0.0.1', 'IcePak', 'icepak', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979120, '2013', '07', '16', 0, 0, 20130722085230, 0, 0),
(18, 1, 3, 1, NULL, '127.0.0.1', 'Lucky Red', 'lucky-red', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979120, '2013', '07', '16', 0, 0, 20130722085256, 0, 0),
(19, 1, 8, 1, NULL, '127.0.0.1', 'Causes we support', 'causes-we-support', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979180, '2013', '07', '16', 0, 0, 20130723112050, 0, 0),
(20, 1, 8, 1, NULL, '127.0.0.1', 'Our Commitment to the Environment', 'our-commitment-to-the-environment', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1373979180, '2013', '07', '16', 0, 0, 20130723141944, 0, 0),
(21, 1, 9, 1, NULL, '127.0.0.1', '15 Million Hot Cross Buns were in Yearsley Group’s Stores This Easter', '15-million-hot-cross-buns-were-in-yearsley-groups-stores-this-easter', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1374245220, '2013', '07', '19', 0, 0, 20130719144902, 0, 0),
(22, 1, 9, 1, NULL, '127.0.0.1', 'Belfields New Mr Kings Range Is Ruling The Dessert Market', 'belfields-new-mr-kings-range-is-ruling-the-dessert-market', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1374247620, '2013', '07', '19', 0, 0, 20130719155034, 0, 0),
(23, 1, 9, 1, NULL, '127.0.0.1', 'Yearsley Group Invest £0.5m at Grimsby Depot', 'yearsley-group-invest-0.5m-at-grimsby-depot', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1374247920, '2013', '07', '19', 0, 0, 20130719153307, 0, 0),
(24, 1, 9, 1, NULL, '127.0.0.1', '£20m expansion plan for Yearsley Group at Heywood', '20m-expansion-plan-for-yearsley-group-at-heywood', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1374251400, '2013', '07', '19', 0, 0, 20130724120554, 0, 0),
(25, 1, 12, 1, NULL, '127.0.0.1', 'A position with Yearsley', 'a-position-with-yearsley', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1374671340, '2013', '07', '24', 0, 0, 20130724165225, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_comments`
--

CREATE TABLE IF NOT EXISTS `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`),
  KEY `comment_date_idx` (`comment_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_comment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_log`
--

CREATE TABLE IF NOT EXISTS `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `exp_cp_log`
--

INSERT INTO `exp_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`) VALUES
(1, 1, 1, 'Admin', '127.0.0.1', 1368618605, 'Logged in'),
(2, 1, 1, 'Admin', '127.0.0.1', 1368618913, 'Field Group Created:&nbsp;All Fields'),
(3, 1, 1, 'Admin', '127.0.0.1', 1368618979, 'Channel Created:&nbsp;&nbsp;Homepage'),
(4, 1, 1, 'Admin', '127.0.0.1', 1372325444, 'Logged in'),
(5, 1, 1, 'Admin', '127.0.0.1', 1372325966, 'Member Group Created:&nbsp;&nbsp;Yearsley'),
(6, 1, 1, 'Admin', '127.0.0.1', 1372343330, 'Logged in'),
(7, 1, 1, 'Admin', '127.0.0.1', 1372343364, 'Logged in'),
(8, 1, 1, 'Admin', '127.0.0.1', 1372771688, 'Logged in'),
(9, 1, 1, 'Admin', '127.0.0.1', 1372772196, 'Channel Created:&nbsp;&nbsp;About us'),
(10, 1, 1, 'Admin', '127.0.0.1', 1373905631, 'Logged in'),
(11, 1, 1, 'Admin', '127.0.0.1', 1373972054, 'Logged in'),
(12, 1, 1, 'Admin', '127.0.0.1', 1373974692, 'Channel Created:&nbsp;&nbsp;Our Businesses'),
(13, 1, 1, 'Admin', '127.0.0.1', 1373974729, 'Channel Created:&nbsp;&nbsp;Business Structure'),
(14, 1, 1, 'Admin', '127.0.0.1', 1373974741, 'Channel Created:&nbsp;&nbsp;Directors'),
(15, 1, 1, 'Admin', '127.0.0.1', 1373974754, 'Channel Created:&nbsp;&nbsp;Locations'),
(16, 1, 1, 'Admin', '127.0.0.1', 1373974767, 'Channel Created:&nbsp;&nbsp;History'),
(17, 1, 1, 'Admin', '127.0.0.1', 1373974798, 'Channel Created:&nbsp;&nbsp;CSER'),
(18, 1, 1, 'Admin', '127.0.0.1', 1373974810, 'Channel Created:&nbsp;&nbsp;News Articles'),
(19, 1, 1, 'Admin', '127.0.0.1', 1373974838, 'Channel Created:&nbsp;&nbsp;News Landing'),
(20, 1, 1, 'Admin', '127.0.0.1', 1373974861, 'Channel Created:&nbsp;&nbsp;Careers'),
(21, 1, 1, 'Admin', '127.0.0.1', 1373974893, 'Channel Created:&nbsp;&nbsp;Careers Items'),
(22, 1, 1, 'Admin', '127.0.0.1', 1373974904, 'Channel Created:&nbsp;&nbsp;Contact Us'),
(23, 1, 1, 'Admin', '127.0.0.1', 1373974925, 'Channel Created:&nbsp;&nbsp;Site Map'),
(24, 1, 1, 'Admin', '127.0.0.1', 1373975014, 'Channel Created:&nbsp;&nbsp;Cookie Usage'),
(25, 1, 1, 'Admin', '127.0.0.1', 1374061676, 'Logged in'),
(26, 1, 1, 'Admin', '127.0.0.1', 1374161588, 'Logged in'),
(27, 1, 1, 'Admin', '127.0.0.1', 1374227093, 'Logged in'),
(28, 1, 1, 'Admin', '127.0.0.1', 1374232854, 'Logged in'),
(29, 1, 1, 'Admin', '127.0.0.1', 1374481596, 'Logged in'),
(30, 1, 1, 'Admin', '127.0.0.1', 1374493648, 'Logged in'),
(31, 1, 1, 'Admin', '127.0.0.1', 1374573658, 'Logged in'),
(32, 1, 1, 'Admin', '127.0.0.1', 1374589155, 'Logged in'),
(33, 1, 1, 'Admin', '127.0.0.1', 1374602681, 'Logged in'),
(34, 1, 1, 'Admin', '127.0.0.1', 1374602778, 'Logged in'),
(35, 1, 1, 'Admin', '127.0.0.1', 1374607906, 'Logged in'),
(36, 1, 1, 'Admin', '127.0.0.1', 1374665397, 'Logged in'),
(37, 1, 1, 'Admin', '127.0.0.1', 1374667511, 'Logged in'),
(38, 1, 1, 'Admin', '127.0.0.1', 1374675019, 'Logged in'),
(39, 1, 1, 'Admin', '127.0.0.1', 1374682876, 'Logged in');

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_search_index`
--

CREATE TABLE IF NOT EXISTS `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_developer_log`
--

CREATE TABLE IF NOT EXISTS `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `template_name` varchar(100) DEFAULT NULL,
  `template_group` varchar(100) DEFAULT NULL,
  `addon_module` varchar(100) DEFAULT NULL,
  `addon_method` varchar(100) DEFAULT NULL,
  `snippets` text,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_mg`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_ml`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_console_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(45) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_ping_status`
--

CREATE TABLE IF NOT EXISTS `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_versioning`
--

CREATE TABLE IF NOT EXISTS `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_extensions`
--

CREATE TABLE IF NOT EXISTS `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `exp_extensions`
--

INSERT INTO `exp_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`) VALUES
(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y'),
(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y'),
(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y'),
(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y'),
(5, 'Snippet_sync_ext', 'sessions_end', 'sessions_end', 'a:7:{s:6:"hashes";a:0:{}s:6:"prefix";s:5:"snip:";s:16:"subfolder_suffix";s:1:"_";s:19:"suffix_as_separator";s:1:"n";s:17:"msm_shared_folder";s:6:"shared";s:9:"hide_menu";s:1:"n";s:14:"enable_cleanup";s:1:"n";}', 2, '1.3.5', 'y'),
(6, 'Snippet_sync_ext', 'show_full_control_panel_end', 'show_full_control_panel_end', 'a:7:{s:6:"hashes";a:0:{}s:6:"prefix";s:5:"snip:";s:16:"subfolder_suffix";s:1:"_";s:19:"suffix_as_separator";s:1:"n";s:17:"msm_shared_folder";s:6:"shared";s:9:"hide_menu";s:1:"n";s:14:"enable_cleanup";s:1:"n";}', 2, '1.3.5', 'y'),
(7, 'Assets_ext', 'channel_entries_query_result', 'channel_entries_query_result', '', 10, '2.1.3', 'y'),
(8, 'Assets_ext', 'file_after_save', 'file_after_save', '', 9, '2.1.3', 'y'),
(9, 'Assets_ext', 'files_after_delete', 'files_after_delete', '', 8, '2.1.3', 'y'),
(10, 'Low_variables_ext', 'sessions_end', 'sessions_end', 'a:8:{s:11:"license_key";s:36:"58007bbf-79cf-45b0-b225-5b3d344aef5b";s:10:"can_manage";a:1:{i:0;s:1:"1";}s:11:"clear_cache";s:1:"y";s:16:"register_globals";s:1:"y";s:20:"register_member_data";s:1:"n";s:13:"save_as_files";s:1:"y";s:9:"file_path";s:81:"/Users/martinsmith/Dropbox/localserver/yearsley/resource/templates/low_variables/";s:13:"enabled_types";a:17:{i:0;s:6:"assets";i:1;s:12:"low_checkbox";i:2;s:18:"low_checkbox_group";i:3;s:14:"low_reorder_vt";i:4;s:6:"matrix";i:5;s:15:"low_radio_group";i:6;s:10:"low_select";i:7;s:21:"low_select_categories";i:8;s:19:"low_select_channels";i:9;s:18:"low_select_entries";i:10;s:16:"low_select_files";i:11;s:9:"structure";i:12;s:9:"low_table";i:13;s:14:"low_text_input";i:14;s:7:"low_rte";i:15;s:6:"wygwam";i:16;s:12:"low_textarea";}}', 2, '2.3.5', 'y'),
(11, 'Low_variables_ext', 'template_fetch_template', 'template_fetch_template', 'a:8:{s:11:"license_key";s:36:"58007bbf-79cf-45b0-b225-5b3d344aef5b";s:10:"can_manage";a:1:{i:0;s:1:"1";}s:11:"clear_cache";s:1:"y";s:16:"register_globals";s:1:"y";s:20:"register_member_data";s:1:"n";s:13:"save_as_files";s:1:"y";s:9:"file_path";s:81:"/Users/martinsmith/Dropbox/localserver/yearsley/resource/templates/low_variables/";s:13:"enabled_types";a:17:{i:0;s:6:"assets";i:1;s:12:"low_checkbox";i:2;s:18:"low_checkbox_group";i:3;s:14:"low_reorder_vt";i:4;s:6:"matrix";i:5;s:15:"low_radio_group";i:6;s:10:"low_select";i:7;s:21:"low_select_categories";i:8;s:19:"low_select_channels";i:9;s:18:"low_select_entries";i:10;s:16:"low_select_files";i:11;s:9:"structure";i:12;s:9:"low_table";i:13;s:14:"low_text_input";i:14;s:7:"low_rte";i:15;s:6:"wygwam";i:16;s:12:"low_textarea";}}', 2, '2.3.5', 'y'),
(12, 'Structure_ext', 'entry_submission_redirect', 'entry_submission_redirect', '', 10, '3.3.10', 'y'),
(13, 'Structure_ext', 'cp_member_login', 'cp_member_login', '', 10, '3.3.10', 'y'),
(14, 'Structure_ext', 'sessions_start', 'sessions_start', '', 10, '3.3.10', 'y'),
(15, 'Structure_ext', 'channel_module_create_pagination', 'channel_module_create_pagination', '', 9, '3.3.10', 'y'),
(16, 'Structure_ext', 'wygwam_config', 'wygwam_config', '', 10, '3.3.10', 'y'),
(17, 'Structure_ext', 'core_template_route', 'core_template_route', '', 10, '3.3.10', 'y'),
(18, 'Structure_ext', 'entry_submission_end', 'entry_submission_end', '', 10, '3.3.10', 'y'),
(19, 'Structure_ext', 'safecracker_submit_entry_end', 'safecracker_submit_entry_end', '', 10, '3.3.10', 'y'),
(20, 'Structure_ext', 'template_post_parse', 'template_post_parse', '', 10, '3.3.10', 'y'),
(21, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.5.6', 'y'),
(22, 'Ab_pagination_ext', 'on_channel_module_create_pagination', 'channel_module_create_pagination', 'a:3:{s:24:"ab_pagination_tag_prefix";s:4:"abp_";s:25:"ab_pagination_strict_urls";s:1:"y";s:34:"ab_pagination_enable_query_strings";s:1:"n";}', 10, '1.6.2', 'y'),
(23, 'Ab_pagination_ext', 'on_template_post_parse', 'template_post_parse', 'a:3:{s:24:"ab_pagination_tag_prefix";s:4:"abp_";s:25:"ab_pagination_strict_urls";s:1:"y";s:34:"ab_pagination_enable_query_strings";s:1:"n";}', 10, '1.6.2', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `exp_fieldtypes`
--

INSERT INTO `exp_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`) VALUES
(1, 'select', '1.0', 'YTowOnt9', 'n'),
(2, 'text', '1.0', 'YTowOnt9', 'n'),
(3, 'textarea', '1.0', 'YTowOnt9', 'n'),
(4, 'date', '1.0', 'YTowOnt9', 'n'),
(5, 'file', '1.0', 'YTowOnt9', 'n'),
(6, 'multi_select', '1.0', 'YTowOnt9', 'n'),
(7, 'checkboxes', '1.0', 'YTowOnt9', 'n'),
(8, 'radio', '1.0', 'YTowOnt9', 'n'),
(9, 'relationship', '1.0', 'YTowOnt9', 'n'),
(10, 'rte', '1.0', 'YTowOnt9', 'n'),
(11, 'assets', '2.1.3', 'YTowOnt9', 'y'),
(12, 'low_variables', '2.3.5', 'YTowOnt9', 'n'),
(13, 'structure', '3.3.10', 'YToxOntzOjE5OiJzdHJ1Y3R1cmVfbGlzdF90eXBlIjtzOjU6InBhZ2VzIjt9', 'n'),
(14, 'wygwam', '3.0.2', 'YTowOnt9', 'y'),
(15, 'matrix', '2.5.5', 'YTowOnt9', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_formatting`
--

CREATE TABLE IF NOT EXISTS `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `exp_field_formatting`
--

INSERT INTO `exp_field_formatting` (`formatting_id`, `field_id`, `field_fmt`) VALUES
(1, 1, 'none'),
(2, 1, 'br'),
(3, 1, 'xhtml'),
(4, 2, 'none'),
(5, 2, 'br'),
(6, 2, 'xhtml'),
(7, 3, 'none'),
(8, 3, 'br'),
(9, 3, 'xhtml'),
(10, 4, 'none'),
(11, 4, 'br'),
(12, 4, 'xhtml'),
(13, 5, 'none'),
(14, 5, 'br'),
(15, 5, 'xhtml'),
(16, 6, 'none'),
(17, 6, 'br'),
(18, 6, 'xhtml'),
(19, 7, 'none'),
(20, 7, 'br'),
(21, 7, 'xhtml'),
(22, 8, 'none'),
(23, 8, 'br'),
(24, 8, 'xhtml'),
(25, 9, 'none'),
(26, 9, 'br'),
(27, 9, 'xhtml');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_groups`
--

CREATE TABLE IF NOT EXISTS `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_field_groups`
--

INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'All Fields');

-- --------------------------------------------------------

--
-- Table structure for table `exp_files`
--

CREATE TABLE IF NOT EXISTS `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `exp_files`
--

INSERT INTO `exp_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `credit`, `location`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`) VALUES
(1, 1, 'banner-cser.jpg', 1, '/Users/martinsmith/Desktop/yearsley/images/uploads/content/banner-cser.jpg', 'image/jpeg', 'banner-cser.jpg', 59249, NULL, NULL, NULL, 1, 1368619077, 1, 1368619077, '326 1280'),
(2, 1, 'banner-about-us.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-about-us.jpg', 'image/jpeg', 'banner-about-us.jpg', 217631, NULL, NULL, NULL, 1, 1374161817, 1, 1374161817, '326 1280'),
(3, 1, 'banner-news.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-news.jpg', 'image/jpeg', 'banner-news.jpg', 123238, NULL, NULL, NULL, 1, 1374164068, 1, 1374164068, '326 1280'),
(4, 1, 'banner-directors.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-directors.jpg', 'image/jpeg', 'banner-directors.jpg', 60713, NULL, NULL, NULL, 1, 1374164859, 1, 1374164859, '326 1280'),
(5, 1, 'banner-structure.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-structure.jpg', 'image/jpeg', 'banner-structure.jpg', 173302, NULL, NULL, NULL, 1, 1374164875, 1, 1374164875, '326 1280'),
(6, 1, 'banner-locations.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-locations.jpg', 'image/jpeg', 'banner-locations.jpg', 66150, NULL, NULL, NULL, 1, 1374164922, 1, 1374164922, '326 1280'),
(7, 1, 'banner-history.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-history.jpg', 'image/jpeg', 'banner-history.jpg', 271555, NULL, NULL, NULL, 1, 1374164964, 1, 1374164964, '326 1280'),
(8, 1, 'banner-our-business.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-our-business.jpg', 'image/jpeg', 'banner-our-business.jpg', 86118, NULL, NULL, NULL, 1, 1374165003, 1, 1374165003, '326 1280'),
(9, 1, 'banner-logistics.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-logistics.jpg', 'image/jpeg', 'banner-logistics.jpg', 86118, NULL, NULL, NULL, 1, 1374165104, 1, 1374165104, '326 1280'),
(10, 1, 'banner-food-sales.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-food-sales.jpg', 'image/jpeg', 'banner-food-sales.jpg', 99471, NULL, NULL, NULL, 1, 1374165121, 1, 1374165121, '326 1280'),
(11, 1, 'banner-belfield.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-belfield.jpg', 'image/jpeg', 'banner-belfield.jpg', 155105, NULL, NULL, NULL, 1, 1374165532, 1, 1374165532, '326 1280'),
(12, 1, 'banner-icepak.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-icepak.jpg', 'image/jpeg', 'banner-icepak.jpg', 197509, NULL, NULL, NULL, 1, 1374165562, 1, 1374165562, '326 1280'),
(13, 1, 'banner-lucky-red.jpeg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-lucky-red.jpeg', 'image/jpeg', 'banner-lucky-red.jpeg', 147845, NULL, NULL, NULL, 1, 1374165577, 1, 1374165577, '326 1280'),
(14, 1, 'banner-careers.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-careers.jpg', 'image/jpeg', 'banner-careers.jpg', 48845, NULL, NULL, NULL, 1, 1374165605, 1, 1374165605, '326 1280'),
(15, 1, 'banner-contact-us.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-contact-us.jpg', 'image/jpeg', 'banner-contact-us.jpg', 38830, NULL, NULL, NULL, 1, 1374165641, 1, 1374165641, '326 1280'),
(16, 1, 'banner-cookies.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-cookies.jpg', 'image/jpeg', 'banner-cookies.jpg', 86876, NULL, NULL, NULL, 1, 1374166318, 1, 1374166318, '326 1280'),
(17, 1, 'banner-sitemap.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-sitemap.jpg', 'image/jpeg', 'banner-sitemap.jpg', 46336, NULL, NULL, NULL, 1, 1374166350, 1, 1374166350, '326 1280'),
(18, 1, 'YearsleyGroupAboutUs.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/YearsleyGroupAboutUs.jpg', 'image/jpeg', 'YearsleyGroupAboutUs.jpg', 18227, NULL, NULL, NULL, 1, 1374233314, 1, 1374233314, '124 250'),
(19, 1, 'panel-belfield.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/panel-belfield.jpg', 'image/jpeg', 'panel-belfield.jpg', 14735, NULL, NULL, NULL, 1, 1374241396, 1, 1374241396, '139 278'),
(20, 1, 'panel-icepak.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/panel-icepak.jpg', 'image/jpeg', 'panel-icepak.jpg', 17418, NULL, NULL, NULL, 1, 1374242961, 1, 1374242961, '139 278'),
(21, 1, 'panel-lucky-red.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/panel-lucky-red.jpg', 'image/jpeg', 'panel-lucky-red.jpg', 16865, NULL, NULL, NULL, 1, 1374242985, 1, 1374242985, '139 278'),
(22, 1, 'ob-belfield.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-belfield.jpg', 'image/jpeg', 'ob-belfield.jpg', 8877, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '129 128'),
(23, 1, 'ob-belfield2.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-belfield2.jpg', 'image/jpeg', 'ob-belfield2.jpg', 8067, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '129 128'),
(24, 1, '0b-food-sales2.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/0b-food-sales2.jpg', 'image/jpeg', '0b-food-sales2.jpg', 9209, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '129 128'),
(25, 1, 'ob-food-sales.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-food-sales.jpg', 'image/jpeg', 'ob-food-sales.jpg', 7383, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '129 128'),
(26, 1, 'ob-icepak2.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-icepak2.jpg', 'image/jpeg', 'ob-icepak2.jpg', 10532, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '129 128'),
(27, 1, 'ob-icepak.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-icepak.jpg', 'image/jpeg', 'ob-icepak.jpg', 8781, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '129 128'),
(28, 1, 'ob-logistics.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-logistics.jpg', 'image/jpeg', 'ob-logistics.jpg', 8125, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '129 128'),
(29, 1, 'ob-logistics2.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-logistics2.jpg', 'image/jpeg', 'ob-logistics2.jpg', 10644, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '129 128'),
(30, 1, 'ob-lucky-red.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-lucky-red.jpg', 'image/jpeg', 'ob-lucky-red.jpg', 17490, NULL, NULL, NULL, 1, 1374482581, 1, 1374482581, '128 128'),
(31, 1, 'ob-lucky-red2.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/ob-lucky-red2.jpg', 'image/jpeg', 'ob-lucky-red2.jpg', 7173, NULL, NULL, NULL, 1, 1374482582, 1, 1374482582, '129 128'),
(32, 1, 'cser3.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/cser3.jpg', 'image/jpeg', 'cser3.jpg', 11077, NULL, NULL, NULL, 1, 1374574424, 1, 1374574424, '128 128'),
(33, 1, 'cser1.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/cser1.jpg', 'image/jpeg', 'cser1.jpg', 29902, NULL, NULL, NULL, 1, 1374574424, 1, 1374574424, '128 128'),
(34, 1, 'cser2.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/cser2.jpg', 'image/jpeg', 'cser2.jpg', 36008, NULL, NULL, NULL, 1, 1374574424, 1, 1374574424, '128 128'),
(35, 1, 'cser4.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/cser4.jpg', 'image/jpeg', 'cser4.jpg', 17704, NULL, NULL, NULL, 1, 1374574424, 1, 1374574424, '128 128'),
(36, 1, 'banner-cser2.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/banner-cser2.jpg', 'image/jpeg', 'banner-cser2.jpg', 219277, NULL, NULL, NULL, 1, 1374576509, 1, 1374576509, '326 1280'),
(37, 1, 'royal-manchester-childrens-hospital.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/royal-manchester-childrens-hospital.jpg', 'image/jpeg', 'royal-manchester-childrens-hospital.jpg', 46652, NULL, NULL, NULL, 1, 1374576682, 1, 1374576682, '199 288'),
(38, 1, 'Yearsley_2012_report_Final.pdf', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/Yearsley_2012_report_Final.pdf', 'application/pdf', 'Yearsley_2012_report_Final.pdf', 1579011, NULL, NULL, NULL, 1, 1374577985, 1, 1374577985, ''),
(39, 1, 'cser5.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/cser5.jpg', 'image/jpeg', 'cser5.jpg', 18426, NULL, NULL, NULL, 1, 1374581141, 1, 1374581141, '128 128'),
(40, 1, 'cser6.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/cser6.jpg', 'image/jpeg', 'cser6.jpg', 24330, NULL, NULL, NULL, 1, 1374581141, 1, 1374581141, '128 128'),
(41, 1, 'cser7.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/cser7.jpg', 'image/jpeg', 'cser7.jpg', 8642, NULL, NULL, NULL, 1, 1374581141, 1, 1374581141, '128 128'),
(42, 1, 'cser8.jpg', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/cser8.jpg', 'image/jpeg', 'cser8.jpg', 20003, NULL, NULL, NULL, 1, 1374581141, 1, 1374581141, '128 128'),
(43, 1, 'Dec2012.pdf', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/Dec2012.pdf', 'application/pdf', 'Dec2012.pdf', 949800, NULL, NULL, NULL, 1, 1374588022, 1, 1374588022, ''),
(44, 1, 'May2013_Newsletter.pdf', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/May2013_Newsletter.pdf', 'application/pdf', 'May2013_Newsletter.pdf', 2278211, NULL, NULL, NULL, 1, 1374588022, 1, 1374588022, ''),
(45, 1, 'newsletter.pdf', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/newsletter.pdf', 'application/pdf', 'newsletter.pdf', 4503416, NULL, NULL, NULL, 1, 1374588022, 1, 1374588022, ''),
(46, 1, 'Nov2012.pdf', 1, '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/Nov2012.pdf', 'application/pdf', 'Nov2012.pdf', 889847, NULL, NULL, NULL, 1, 1374588022, 1, 1374588022, ''),
(47, 1, 'Solar_Panels.jpg', 1, '/Users/martin/Dropbox/localserver/yearsley/images/uploads/content/Solar_Panels.jpg', 'image/jpeg', 'Solar_Panels.jpg', 179395, NULL, NULL, NULL, 1, 1374608015, 1, 1374608015, '335 618');

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_categories`
--

CREATE TABLE IF NOT EXISTS `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_dimensions`
--

CREATE TABLE IF NOT EXISTS `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_watermarks`
--

CREATE TABLE IF NOT EXISTS `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_global_variables`
--

CREATE TABLE IF NOT EXISTS `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_global_variables`
--

INSERT INTO `exp_global_variables` (`variable_id`, `site_id`, `variable_name`, `variable_data`) VALUES
(1, 1, 'lv_doc_header', '<!doctype html>\n  <!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n  <!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n  <!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n \n  <head> \n    <meta charset="UTF-8"> \n    \n    <!-- TITLE -->\n    <title>Project Name</title>\n    \n    <!-- META -->\n    <meta name="description" content="" />\n    \n    <!-- STYLES -->\n    <link rel="stylesheet" href="/resource/css/style.css" />\n    \n    <!-- MODERNIZR -->\n    <script src="/resource/js/libs/modernizr.min.js"></script>'),
(2, 1, 'lv_footer', '    <footer id="footer">\n      <div class="nine60">\n        <nav>\n          <ul>\n            {exp:structure:nav mode="main" include_ul="no" exclude="1|2|3|4|6|7"}\n          </ul>\n        </nav>\n        <ul class="social">\n          <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n          <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n          <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n        </ul>\n      </div>\n      <div>\n        <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n      </div>\n    </footer>\n    <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n'),
(3, 1, 'lv_base_scripts', '    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="resource/js/plugins.js"></script>\n    <script src="resource/js/global.js"></script>'),
(4, 1, 'lv_header', '    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            {exp:structure:nav mode="main" include_ul="no" exclude="8|9"}\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            {exp:structure:breadcrumb here_as_title="yes" inc_here="yes" inc_home="yes" wrap_each="li"}\n          </ul>\n        </div>\n      </div>\n    </header>');

-- --------------------------------------------------------

--
-- Table structure for table `exp_html_buttons`
--

CREATE TABLE IF NOT EXISTS `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_html_buttons`
--

INSERT INTO `exp_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`) VALUES
(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(4, 1, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(5, 1, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img');

-- --------------------------------------------------------

--
-- Table structure for table `exp_layout_publish`
--

CREATE TABLE IF NOT EXISTS `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `exp_layout_publish`
--

INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(7, 1, 1, 4, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(8, 1, 6, 4, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(5, 1, 1, 5, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(6, 1, 6, 5, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(9, 1, 1, 6, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(10, 1, 6, 6, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(11, 1, 1, 7, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(12, 1, 6, 7, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(29, 1, 1, 3, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(30, 1, 6, 3, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(15, 1, 1, 15, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(16, 1, 6, 15, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(17, 1, 1, 14, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(18, 1, 6, 14, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(19, 1, 1, 1, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(20, 1, 6, 1, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(25, 1, 1, 9, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');
INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(26, 1, 6, 9, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(41, 1, 1, 10, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(42, 1, 6, 10, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(37, 1, 1, 8, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(38, 1, 6, 8, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(47, 1, 1, 2, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(48, 1, 6, 2, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(49, 1, 1, 12, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(50, 1, 6, 12, 'a:5:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:4;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_variables`
--

CREATE TABLE IF NOT EXISTS `exp_low_variables` (
  `variable_id` int(6) unsigned NOT NULL,
  `group_id` int(6) unsigned NOT NULL DEFAULT '0',
  `variable_label` varchar(100) NOT NULL DEFAULT '',
  `variable_notes` text NOT NULL,
  `variable_type` varchar(50) NOT NULL DEFAULT 'low_textarea',
  `variable_settings` text NOT NULL,
  `variable_order` int(4) unsigned NOT NULL DEFAULT '0',
  `early_parsing` char(1) NOT NULL DEFAULT 'n',
  `is_hidden` char(1) NOT NULL DEFAULT 'n',
  `save_as_file` char(1) NOT NULL DEFAULT 'n',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variable_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_low_variables`
--

INSERT INTO `exp_low_variables` (`variable_id`, `group_id`, `variable_label`, `variable_notes`, `variable_type`, `variable_settings`, `variable_order`, `early_parsing`, `is_hidden`, `save_as_file`, `edit_date`) VALUES
(1, 1, 'Doc Header', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 1, 'y', 'n', 'y', 1373906020),
(2, 1, 'Footer', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 2, 'y', 'n', 'y', 1373978866),
(3, 1, 'Base Scripts', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 3, 'y', 'n', 'y', 1373972554),
(4, 1, 'Header', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 4, 'y', 'n', 'y', 1374582189);

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_variable_groups`
--

CREATE TABLE IF NOT EXISTS `exp_low_variable_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(6) unsigned NOT NULL DEFAULT '1',
  `group_label` varchar(100) NOT NULL DEFAULT '',
  `group_notes` text NOT NULL,
  `group_order` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_low_variable_groups`
--

INSERT INTO `exp_low_variable_groups` (`group_id`, `site_id`, `group_label`, `group_notes`, `group_order`) VALUES
(1, 1, 'Page Includes', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_cols`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `exp_matrix_cols`
--

INSERT INTO `exp_matrix_cols` (`col_id`, `site_id`, `field_id`, `var_id`, `col_name`, `col_label`, `col_instructions`, `col_type`, `col_required`, `col_search`, `col_order`, `col_width`, `col_settings`) VALUES
(1, 1, 3, NULL, 'cf_left_image', 'Left Image', '', 'assets', 'n', 'n', 0, '50%', 'YTo2OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjQ6ImVlOjEiO31zOjQ6InZpZXciO3M6NjoidGh1bWJzIjtzOjEwOiJ0aHVtYl9zaXplIjtzOjU6InNtYWxsIjtzOjE0OiJzaG93X2ZpbGVuYW1lcyI7czoxOiJuIjtzOjk6InNob3dfY29scyI7YToxOntpOjA7czo0OiJuYW1lIjt9czo1OiJtdWx0aSI7czoxOiJ5Ijt9'),
(2, 1, 3, NULL, 'cf_right_image', 'Right Image', '', 'assets', 'n', 'n', 1, '50%', 'YTo2OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjQ6ImVlOjEiO31zOjQ6InZpZXciO3M6NjoidGh1bWJzIjtzOjEwOiJ0aHVtYl9zaXplIjtzOjU6InNtYWxsIjtzOjE0OiJzaG93X2ZpbGVuYW1lcyI7czoxOiJuIjtzOjk6InNob3dfY29scyI7YToxOntpOjA7czo0OiJuYW1lIjt9czo1OiJtdWx0aSI7czoxOiJ5Ijt9'),
(3, 1, 4, NULL, 'cf_logo_image', 'Logo Image', '', 'file', 'n', 'n', 0, '15%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9'),
(4, 1, 4, NULL, 'cf_logo_image_link', 'Logo Image Link', 'You will need to make sure you have the full URL, including the http:// at the beginning.', 'text', 'n', 'n', 1, '85%', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(5, 1, 6, NULL, 'cf_optional_cser-report', 'Opt. CSER Report', 'Right click on the icon to add description text.', 'assets', 'n', 'n', 0, '20%', 'YTo2OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjQ6ImVlOjEiO31zOjQ6InZpZXciO3M6NDoibGlzdCI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6Mjp7aTowO3M6NDoibmFtZSI7aToxO3M6NDoiZGF0ZSI7fXM6NToibXVsdGkiO3M6MToibiI7fQ=='),
(6, 1, 6, NULL, 'cf_supported_charity', 'Opt. Supported Charity', '', 'assets', 'n', 'n', 1, '20%', 'YTo2OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjQ6ImVlOjEiO31zOjQ6InZpZXciO3M6NDoibGlzdCI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6Mjp7aTowO3M6NDoibmFtZSI7aToxO3M6NDoiZGF0ZSI7fXM6NToibXVsdGkiO3M6MToibiI7fQ=='),
(7, 1, 6, NULL, 'cf_content_block', 'Opt. Content Block', '', 'wygwam', 'n', 'n', 2, '60%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30='),
(8, 1, 8, NULL, 'cf_work_type', 'Work Type', '', 'text', 'n', 'n', 0, '10%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(9, 1, 8, NULL, 'cf_salary', 'Salary', '', 'text', 'n', 'n', 1, '10%', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(10, 1, 8, NULL, 'cf_job_location', 'Location', '', 'text', 'n', 'n', 2, '15%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(11, 1, 8, NULL, 'cf_supporting_text', 'Supporting Text', '', 'wygwam', 'n', 'n', 3, '65%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30='),
(12, 1, 9, NULL, 'cf_jobs_available', 'Jobs Available', '', 'wygwam', 'n', 'n', 0, '50%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30='),
(13, 1, 9, NULL, 'cf_no_vacancies', 'No Vacancies', '', 'wygwam', 'n', 'n', 1, '50%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_data`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_3` text,
  `col_id_4` text,
  `col_id_5` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` text,
  `col_id_13` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `exp_matrix_data`
--

INSERT INTO `exp_matrix_data` (`row_id`, `site_id`, `entry_id`, `field_id`, `var_id`, `is_draft`, `row_order`, `col_id_1`, `col_id_2`, `col_id_3`, `col_id_4`, `col_id_5`, `col_id_6`, `col_id_7`, `col_id_8`, `col_id_9`, `col_id_10`, `col_id_11`, `col_id_12`, `col_id_13`) VALUES
(1, 1, 16, 4, NULL, 0, 1, NULL, NULL, '{filedir_1}panel-belfield.jpg', 'http://www.belfield.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 17, 4, NULL, 0, 1, NULL, NULL, '{filedir_1}panel-icepak.jpg', 'http://', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, 18, 4, NULL, 0, 1, NULL, NULL, '{filedir_1}panel-lucky-red.jpg', 'http://', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 19, 6, NULL, 0, 1, NULL, NULL, NULL, NULL, 'Yearsley_2012_report_Final.pdf', 'royal-manchester-childrens-hospital.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 1, 4, 6, NULL, 0, 1, NULL, NULL, NULL, NULL, 'Yearsley_2012_report_Final.pdf', '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 1, 20, 6, NULL, 0, 1, NULL, NULL, NULL, NULL, 'Yearsley_2012_report_Final.pdf', '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 1, 25, 8, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Driver', '20,000', 'Heywood', '<p>Lorem ipsum Laborum reprehenderit proident dolore laborum incididunt nulla esse qui Excepteur esse in est nisi sed ut mollit cillum elit Duis laboris velit nostrud veniam consequat magna Duis ullamco voluptate quis in laboris.</p>', NULL, NULL),
(8, 1, 6, 6, NULL, 0, 1, NULL, NULL, NULL, NULL, '', '', '<h3>Working with us</h3>\n\n<p>Want to work for a Nationwide company with a proven track record of success?</p>\n\n<p>Want a job that offers both security and progression?</p>\n\n<p>Then apply now&hellip;.</p>', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 1, 6, 9, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<p>Below are the current vacancies within Yearsley.</p>', '<p>&#8203;No vacancies at present. Please check back soon or alternatively send your CV for future consideration to hr@yearsley.co.uk</p>');

-- --------------------------------------------------------

--
-- Table structure for table `exp_members`
--

CREATE TABLE IF NOT EXISTS `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '28',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_members`
--

INSERT INTO `exp_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`, `rte_enabled`, `rte_toolset_id`) VALUES
(1, 1, 'Admin', 'Admin', 'c040c112fec666669c294029ae0b4005843ed389048574a450482b77bcc16515d570816e0daa7ad3dac879c0a804d980430fc00625c4dfb64c01382442990dc7', 'uL5l$-9wJm0_K|6wduTU_u-.''f0[z~!LS\\_FyLeV[=,*,SX{Sg9[kbNtQ>Ad?pY"D3pa>$_J-6z.F:,*D"k%Grh@,,??OUCqC/)v5FW''MrT`g{Z%Ms>K}=a^ya2,C#,^', 'ab38c56086614834dc54e9bc45746afe7f3b5c3b', 'ab297e48bdb7c2a521d09d695657f4a26c2b42f7', NULL, 'martin@shoot-the-moon.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '127.0.0.1', 1368618572, 1374675587, 1374684667, 25, 0, 0, 0, 1374671498, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'Europe/London', 'n', 'us', NULL, NULL, NULL, NULL, '28', NULL, '18', '', 'Structure|C=addons_modules&M=show_module_cp&module=structure|1\nLow Variables|C=addons_modules&M=show_module_cp&module=low_variables|2', 'n', 0, 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_bulletin_board`
--

CREATE TABLE IF NOT EXISTS `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_data`
--

CREATE TABLE IF NOT EXISTS `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_data`
--

INSERT INTO `exp_member_data` (`member_id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_fields`
--

CREATE TABLE IF NOT EXISTS `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_groups`
--

INSERT INTO `exp_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`) VALUES
(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(6, 1, 'Yearsley', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_homepage`
--

CREATE TABLE IF NOT EXISTS `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_homepage`
--

INSERT INTO `exp_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`) VALUES
(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_search`
--

CREATE TABLE IF NOT EXISTS `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_copies`
--

CREATE TABLE IF NOT EXISTS `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_data`
--

CREATE TABLE IF NOT EXISTS `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_folders`
--

CREATE TABLE IF NOT EXISTS `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_message_folders`
--

INSERT INTO `exp_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`) VALUES
(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_listed`
--

CREATE TABLE IF NOT EXISTS `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_modules`
--

CREATE TABLE IF NOT EXISTS `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `exp_modules`
--

INSERT INTO `exp_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`) VALUES
(1, 'Comment', '2.3.1', 'y', 'n'),
(2, 'Email', '2.0', 'n', 'n'),
(3, 'Emoticon', '2.0', 'n', 'n'),
(4, 'Jquery', '1.0', 'n', 'n'),
(5, 'Rss', '2.0', 'n', 'n'),
(6, 'Safecracker', '2.1', 'y', 'n'),
(7, 'Search', '2.2.1', 'n', 'n'),
(8, 'Channel', '2.0.1', 'n', 'n'),
(9, 'Member', '2.1', 'n', 'n'),
(10, 'Stats', '2.0', 'n', 'n'),
(11, 'Rte', '1.0', 'y', 'n'),
(12, 'Assets', '2.1.3', 'y', 'n'),
(13, 'Deeploy_helper', '2.0.3', 'y', 'n'),
(14, 'Low_variables', '2.3.5', 'y', 'n'),
(15, 'Structure', '3.3.10', 'y', 'y'),
(16, 'Wygwam', '3.0.2', 'y', 'n'),
(17, 'Ab_pagination', '1.6.2', 'n', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_module_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_module_member_groups`
--

INSERT INTO `exp_module_member_groups` (`group_id`, `module_id`) VALUES
(6, 1),
(6, 6),
(6, 11);

-- --------------------------------------------------------

--
-- Table structure for table `exp_online_users`
--

CREATE TABLE IF NOT EXISTS `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `exp_online_users`
--

INSERT INTO `exp_online_users` (`online_id`, `site_id`, `member_id`, `in_forum`, `name`, `ip_address`, `date`, `anon`) VALUES
(50, 1, 0, 'n', '', '127.0.0.1', 1374684861, ''),
(55, 1, 0, 'n', '', '127.0.0.1', 1374684861, ''),
(56, 1, 1, 'n', 'Admin', '127.0.0.1', 1374684767, ''),
(53, 1, 0, 'n', '', '127.0.0.1', 1374684861, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_password_lockout`
--

CREATE TABLE IF NOT EXISTS `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ping_servers`
--

CREATE TABLE IF NOT EXISTS `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_relationships` (
  `relationship_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `child_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`relationship_id`),
  KEY `parent_id` (`parent_id`),
  KEY `child_id` (`child_id`),
  KEY `field_id` (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_remember_me`
--

CREATE TABLE IF NOT EXISTS `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_reset_password`
--

CREATE TABLE IF NOT EXISTS `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_revision_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_revision_tracker`
--

INSERT INTO `exp_revision_tracker` (`tracker_id`, `item_id`, `item_table`, `item_field`, `item_date`, `item_author_id`, `item_data`) VALUES
(1, 1, 'exp_templates', 'template_data', 1372343582, 1, '<!doctype html>\n  <!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n  <!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n  <!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n \n  <head> \n    <meta charset="UTF-8"> \n    \n    <!-- TITLE -->\n    <title>Project Name</title>\n    \n    <!-- META -->\n    <meta name="description" content="" />\n    \n    <!-- STYLES -->\n    <link rel="stylesheet" href="/resource/css/style.css" />\n    \n    <!-- MODERNIZR -->\n    <script src="/resource/js/libs/modernizr.min.js"></script>\n  </head>\n\n  <body>\n    <div id="container"> \n      <!-- HEADER -->\n      <header id="header" role="banner">\n\n        <div class="nine60">\n          <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n        </div>\n        <nav>\n          <div class="nine60">\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n              <li><a href="">Item Four</a></li>\n            </ul>\n          </div>\n        </nav>\n        <div class="breadcrumbs">\n          <div class="nine60"><p>You are here:</p>\n            <ul>\n              <li><a href="">Home</a></li>\n            </ul>\n          </div>\n        </div>\n      </header>\n      <div id="banners">\n        <div class="img1"><img src="/dummies/home_bean_banner.jpg" /></div>\n        <div class="img2"><img src="/dummies/home_logistics_banner.jpg" /></div>\n      </div>\n      \n      <!-- CONTENT -->\n      <div id="content">\n          <div class="nine60">\n          <section>\n            <div class="main left">\n              <h1>Home</h1>\n              <p>Yearsley Group is leading the way in frozen foods via its two divisions: logistics and food.</p>\n              <p>Yearsley Logistics is the UK’s largest logistics service provider in the frozen food sector, as well as offering ambient, chilled and freight forwarding solutions.</p>\n              <p>Yearsley Food markets and supplies a full range of innovative frozen products to customers in all sectors of the marketplace including export.</p>\n            </div>\n          </section>\n          <aside class="right">\n            <h2>Latest news and updates</h2>\n            <ul class="newsFeed">\n              <li>\n                <p class="date">22nd April 2013</p>\n                <img src="/dummies/dummy1.jpg" />\n                <p>Lorem ipsum Occaecat ea incididunt occaecat ex eiusmod ad laborum nisi non in magna.</p>\n                <p><a href="">Read more...</a></p>\n              </li>\n              <li>\n                <p class="date">22nd April 2013</p>\n                <img src="/dummies/dummy1.jpg" />\n                <p>Lorem ipsum Occaecat ea incididunt occaecat ex eiusmod ad laborum nisi non in magna.</p>\n                <p><a href="">Read more...</a></p>\n              </li>\n            </ul>\n            <p class="more"><a href="">More news</a></p>\n          </aside>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n              <ul>\n                <li><a href="">Item One</a></li>\n                <li><a href="">Item Two</a></li>\n                <li><a href="">Item Three</a></li>\n              </ul>\n           </nav>\n           <ul class="social">\n              <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n              <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n              <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n           </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="/resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="/resource/js/plugins.js"></script>\n    <script src="/resource/js/global.js"></script>\n  </body>\n</html>\n'),
(2, 1, 'exp_templates', 'template_data', 1373905814, 1, '<!doctype html>\n  <!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n  <!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n  <!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n \n  <head> \n    <meta charset="UTF-8"> \n    \n    <!-- TITLE -->\n    <title>Project Name</title>\n    \n    <!-- META -->\n    <meta name="description" content="" />\n    \n    <!-- STYLES -->\n    <link rel="stylesheet" href="/resource/css/style.css" />\n    \n    <!-- MODERNIZR -->\n    <script src="/resource/js/libs/modernizr.min.js"></script>\n  </head>\n\n  <body>\n    <div id="container"> \n      <!-- HEADER -->\n      <header id="header" role="banner">\n\n        <div class="nine60">\n          <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n        </div>\n        <nav>\n          <div class="nine60">\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n              <li><a href="">Item Four</a></li>\n            </ul>\n          </div>\n        </nav>\n        <div class="breadcrumbs">\n          <div class="nine60"><p>You are here:</p>\n            <ul>\n              <li><a href="">Home</a></li>\n            </ul>\n          </div>\n        </div>\n      </header>\n      <div id="banners">\n        <div class="img1"><img src="/dummies/home_bean_banner.jpg" /></div>\n        <div class="img2"><img src="/dummies/home_logistics_banner.jpg" /></div>\n      </div>\n      \n      <!-- CONTENT -->\n      <div id="content">\n          <div class="nine60">\n          <section>\n            <div class="main left">\n              <h1>Home</h1>\n              <p>Yearsley Group is leading the way in frozen foods via its two divisions: logistics and food.</p>\n              <p>Yearsley Logistics is the UK’s largest logistics service provider in the frozen food sector, as well as offering ambient, chilled and freight forwarding solutions.</p>\n              <p>Yearsley Food markets and supplies a full range of innovative frozen products to customers in all sectors of the marketplace including export.</p>\n            </div>\n          </section>\n          <aside class="right">\n            <h2>Latest news and updates</h2>\n            <ul class="newsFeed">\n              <li>\n                <p class="date">22nd April 2013</p>\n                <img src="/dummies/dummy1.jpg" />\n                <p>Lorem ipsum Occaecat ea incididunt occaecat ex eiusmod ad laborum nisi non in magna.</p>\n                <p><a href="">Read more...</a></p>\n              </li>\n              <li>\n                <p class="date">22nd April 2013</p>\n                <img src="/dummies/dummy1.jpg" />\n                <p>Lorem ipsum Occaecat ea incididunt occaecat ex eiusmod ad laborum nisi non in magna.</p>\n                <p><a href="">Read more...</a></p>\n              </li>\n            </ul>\n            <p class="more"><a href="">More news</a></p>\n          </aside>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n              <ul>\n                <li><a href="">Item One</a></li>\n                <li><a href="">Item Two</a></li>\n                <li><a href="">Item Three</a></li>\n              </ul>\n           </nav>\n           <ul class="social">\n              <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n              <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n              <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n           </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="/resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="/resource/js/plugins.js"></script>\n    <script src="/resource/js/global.js"></script>\n  </body>\n</html>\n'),
(3, 25, 'exp_templates', 'template_data', 1374240830, 1, '{exp:structure:first_child_redirect}'),
(4, 25, 'exp_templates', 'template_data', 1374240837, 1, '{exp:structure:first_child_redirect}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_tools`
--

CREATE TABLE IF NOT EXISTS `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_rte_tools`
--

INSERT INTO `exp_rte_tools` (`tool_id`, `name`, `class`, `enabled`) VALUES
(1, 'Blockquote', 'Blockquote_rte', 'y'),
(2, 'Bold', 'Bold_rte', 'y'),
(3, 'Headings', 'Headings_rte', 'y'),
(4, 'Image', 'Image_rte', 'y'),
(5, 'Italic', 'Italic_rte', 'y'),
(6, 'Link', 'Link_rte', 'y'),
(7, 'Ordered List', 'Ordered_list_rte', 'y'),
(8, 'Underline', 'Underline_rte', 'y'),
(9, 'Unordered List', 'Unordered_list_rte', 'y'),
(10, 'View Source', 'View_source_rte', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_toolsets`
--

CREATE TABLE IF NOT EXISTS `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_rte_toolsets`
--

INSERT INTO `exp_rte_toolsets` (`toolset_id`, `member_id`, `name`, `tools`, `enabled`) VALUES
(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_search`
--

CREATE TABLE IF NOT EXISTS `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_search_log`
--

CREATE TABLE IF NOT EXISTS `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_security_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2032 ;

--
-- Dumping data for table `exp_security_hashes`
--

INSERT INTO `exp_security_hashes` (`hash_id`, `date`, `session_id`, `hash`) VALUES
(2031, 1374684860, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'd25401713a7907c608745291b3abbea46806327e'),
(2030, 1374684860, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '1672db855f28c58cfbddeb618eeba6e8f0548149'),
(2029, 1374684859, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '12ea507438ce26190f9d6f6adddaa71b2d07ef53'),
(2028, 1374684859, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '22d81e617510e6bc37ca48901c891cd0071c6a75'),
(2027, 1374684859, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '4974fac3fa81fa4dbde0c8627886e39d42c193c0'),
(2026, 1374684852, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '18f2fa1bd4d5dbe0eadb52fe3f64371c47298fbe'),
(2025, 1374684851, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '12695d3a2f75ad24d78873231ae8f4fa7e4bc135'),
(2024, 1374684851, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'fe893043442c218647557c39f38dca1b1b6665ab'),
(2023, 1374684850, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '213fe17a30a5e48bfc982c799e438891729bdd52'),
(2022, 1374684850, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '9ec3dae2f26b0da1e12c2917ba4e25400c6a3968'),
(2021, 1374684788, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '3cb090fd39d031b28153b5806a651368a37ec868'),
(2020, 1374684788, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '6fb4aa3be168e521d81c717849666696392e53bb'),
(2019, 1374684788, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'eef18efbd0eabe535594a71989ce16fa4c40f72f'),
(2018, 1374684787, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'b81d572182ded970afd75b49c4c68fc75d2b8783'),
(2017, 1374684745, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '0e4cda2c252c68b0b39e2882a11b87c884d8b65c'),
(2015, 1374684741, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '8146eabe3af868399317cb02960609babbe35e0e'),
(2014, 1374684740, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'ab1b05214551b761e9963f010dbd19ac8f9b4772'),
(2013, 1374684740, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'c3b13c949ef80ec83fb7d691dc2971334d37e3f9'),
(2016, 1374684745, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'f0241d43c768c9c9d3d4bf4c8bf4b9aa70cefbd7'),
(2011, 1374684738, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '53e3c1db98965d96034375376d10c5b3973aa175'),
(2010, 1374684425, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '4ebe8b974c6f85df7f6b6f43b69482d1a7d3d99d'),
(2009, 1374684424, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'a5900f7fbaa76fe3d005ceeed0173ab207655176'),
(2008, 1374684390, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '1fb53fd6ae787d534ad0160738f29b74a2fb9069'),
(2007, 1374684346, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'a236a1fb8fb2895fd3bc1aab3bf56e817d104baf'),
(2006, 1374684342, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'c9ce0044a439fe5196a9fe7b81ec00b6fa3e3bbd'),
(2005, 1374684336, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'd155956acf58e5aa67fcf65ecf53118b87094a0d'),
(2004, 1374683898, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '1533f3fabb5c795592960de5b9a897ce92612828'),
(2002, 1374683894, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'b5891d4a70def52dd6adfcac81734c39830d6c5d'),
(2001, 1374683894, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'f60efe3bb2992c45d1cfa530fdd1317a2680c0dc'),
(2000, 1374683893, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'b44297a8dd3114d27c7c56a1c7c9cfc216decc6d'),
(2003, 1374683898, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'a30bf62c1354f1a6acf4b698a2568b33f73d5d13'),
(1998, 1374683891, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '0d693ef01aa0cb4faa3f2a0793da5fcd6a0251ca'),
(1997, 1374683888, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'd7a1c2bab79a6b339b3e3d39c60901158564be94'),
(1996, 1374683239, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'c57483ab4643c2af48c7616edb087fc3d63d9bcf'),
(1994, 1374683222, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '946735f4232b501f56bd707abc6af88ca4ef6458'),
(1993, 1374683208, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'ec3aedd8838601ed2b8124ad93c4b4c53cd49471'),
(1992, 1374683207, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'acc6f50135042846f0cf4031d11bf93f8b9ed1ce'),
(1991, 1374683207, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '69a8b4b3205532d86287cb3037ccf8992da70704'),
(1995, 1374683239, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'f98b97cb0ccd40d667faf44246067e5723acd161'),
(1989, 1374683205, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '5bc8641272ea3af1943bce653d74de86ce003f27'),
(1988, 1374683190, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'e5534c2274f1b3a96f86382a004556804f6d0ba6'),
(1986, 1374683177, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'ff8cc9acf50e1a7570fa4f36a2ac919e6104c5a3'),
(1985, 1374683176, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '4144f6ee5d96004658ff0a660f5fbfa8f28567af'),
(1984, 1374683176, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'ef86ecce0ff778b1e02be75b9af3cef611070ad8'),
(1987, 1374683189, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'd20e956bef3cf859537458a36421c52c83c37df9'),
(1982, 1374683174, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'fbe211ff1ed5b5ff108b38258ce1f2a7bd20ecbc'),
(1981, 1374683169, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'dc73cba52ab5155e6f88190f3094042d16437bfe'),
(1980, 1374682917, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '0d8106957431de2d96122160c513130ded9457ae'),
(1979, 1374682917, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'af07925444e13518c2ed7acaa5a2815863d20e80'),
(1978, 1374682885, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 'c39e4a5c9d2cf8dda667788d34038b330f963386'),
(1977, 1374682876, 'f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', '47d00ec9320db8eb913cbd121df54c4a6bbf342c'),
(1972, 1374682131, '0', '5e48ffa8827ac98fb793e98c3cbffcb31edb5cc9'),
(1976, 1374682876, '0', '670a3d3a238e1b1b6d491a67930150e5b1c985a6'),
(1974, 1374682697, '0', 'b7c348b8fa53e590d68bdca4c24cb6665b91fc67'),
(1975, 1374682698, '0', '862993652e27695dfa0cb51a22995a44084219a7');

-- --------------------------------------------------------

--
-- Table structure for table `exp_sessions`
--

CREATE TABLE IF NOT EXISTS `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `fingerprint` varchar(40) NOT NULL,
  `sess_start` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_sessions`
--

INSERT INTO `exp_sessions` (`session_id`, `member_id`, `admin_sess`, `ip_address`, `user_agent`, `fingerprint`, `sess_start`, `last_activity`) VALUES
('f0e0c6f70f0368f95e6b42b62cd7428c53f9d46b', 1, 1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 'b113baa55a7740d0739c977dee5f1389', 1374682876, 1374684860);

-- --------------------------------------------------------

--
-- Table structure for table `exp_sites`
--

CREATE TABLE IF NOT EXISTS `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` longtext,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_sites`
--

INSERT INTO `exp_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`, `site_pages`) VALUES
(1, 'Yearsley Group', 'default_site', NULL, 'YTo5MTp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MjQ6Imh0dHA6Ly95ZWFyc2xleW1heTo4ODg4LyI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czozMToiaHR0cDovL3llYXJzbGV5bWF5Ojg4ODgvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjI3OiJtYXJ0aW5Ac2hvb3QtdGhlLW1vb24uY28udWsiO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjQwOiJodHRwOi8veWVhcnNsZXltYXk6ODg4OC9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NTI6Ii9Vc2Vycy9tYXJ0aW5zbWl0aC9Ecm9wYm94L3llYXJzbGV5L2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czoxMzoiRXVyb3BlL0xvbmRvbiI7czoxMzoic2VydmVyX29mZnNldCI7czowOiIiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czoxMzoiRXVyb3BlL0xvbmRvbiI7czoxNToiaG9ub3JfZW50cnlfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6Mzk6Imh0dHA6Ly95ZWFyc2xleW1heTo4ODg4L2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo1NToiL1VzZXJzL21hcnRpbnNtaXRoL0Ryb3Bib3gvbG9jYWxzZXJ2ZXIveWVhcnNsZXkvdGhlbWVzLyI7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO3M6NjoiY3BfdXJsIjtzOjQwOiJodHRwOi8veWVhcnNsZXltYXk6ODg4OC9zeXN0ZW0vaW5kZXgucGhwIjt9', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6Mzk6Imh0dHA6Ly95ZWFyc2xleW1heTo4ODg4L2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTE6Ii9Vc2Vycy9tYXJ0aW5zbWl0aC9Ecm9wYm94L3llYXJzbGV5L2ltYWdlcy9hdmF0YXJzLyI7czoxNjoiYXZhdGFyX21heF93aWR0aCI7czozOiIxMDAiO3M6MTc6ImF2YXRhcl9tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMzoiYXZhdGFyX21heF9rYiI7czoyOiI1MCI7czoxMzoiZW5hYmxlX3Bob3RvcyI7czoxOiJuIjtzOjk6InBob3RvX3VybCI7czo0NToiaHR0cDovL3llYXJzbGV5bWF5Ojg4ODgvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjEwOiJwaG90b19wYXRoIjtzOjU3OiIvVXNlcnMvbWFydGluc21pdGgvRHJvcGJveC95ZWFyc2xleS9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTU6InBob3RvX21heF93aWR0aCI7czozOiIxMDAiO3M6MTY6InBob3RvX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEyOiJwaG90b19tYXhfa2IiO3M6MjoiNTAiO3M6MTY6ImFsbG93X3NpZ25hdHVyZXMiO3M6MToieSI7czoxMzoic2lnX21heGxlbmd0aCI7czozOiI1MDAiO3M6MjE6InNpZ19hbGxvd19pbWdfaG90bGluayI7czoxOiJuIjtzOjIwOiJzaWdfYWxsb3dfaW1nX3VwbG9hZCI7czoxOiJuIjtzOjExOiJzaWdfaW1nX3VybCI7czo1MzoiaHR0cDovL3llYXJzbGV5bWF5Ojg4ODgvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTI6InNpZ19pbWdfcGF0aCI7czo2NToiL1VzZXJzL21hcnRpbnNtaXRoL0Ryb3Bib3gveWVhcnNsZXkvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo1ODoiL1VzZXJzL21hcnRpbnNtaXRoL0Ryb3Bib3gveWVhcnNsZXkvaW1hZ2VzL3BtX2F0dGFjaG1lbnRzLyI7czoyMzoicHJ2X21zZ19tYXhfYXR0YWNobWVudHMiO3M6MToiMyI7czoyMjoicHJ2X21zZ19hdHRhY2hfbWF4c2l6ZSI7czozOiIyNTAiO3M6MjA6InBydl9tc2dfYXR0YWNoX3RvdGFsIjtzOjM6IjEwMCI7czoxOToicHJ2X21zZ19odG1sX2Zvcm1hdCI7czo0OiJzYWZlIjtzOjE4OiJwcnZfbXNnX2F1dG9fbGlua3MiO3M6MToieSI7czoxNzoicHJ2X21zZ19tYXhfY2hhcnMiO3M6NDoiNjAwMCI7czoxOToibWVtYmVybGlzdF9vcmRlcl9ieSI7czoxMToidG90YWxfcG9zdHMiO3M6MjE6Im1lbWJlcmxpc3Rfc29ydF9vcmRlciI7czo0OiJkZXNjIjtzOjIwOiJtZW1iZXJsaXN0X3Jvd19saW1pdCI7czoyOiIyMCI7fQ==', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJ5IjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJ5IjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo2NzoiL1VzZXJzL21hcnRpbnNtaXRoL0Ryb3Bib3gvbG9jYWxzZXJ2ZXIveWVhcnNsZXkvcmVzb3VyY2UvdGVtcGxhdGVzLyI7fQ==', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjU3OiIvVXNlcnMvbWFydGluc21pdGgvRHJvcGJveC9sb2NhbHNlcnZlci95ZWFyc2xleS9pbmRleC5waHAiO3M6MzI6IjQ3NjAwZTRhODJjZjM5NDJiOThmY2E4YmRjOGIyOWEwIjtzOjc6ImVtYWlsZWQiO2E6MDp7fX0=', 'YToxOntpOjE7YTozOntzOjM6InVybCI7czoyNDoiaHR0cDovL3llYXJzbGV5bWF5Ojg4ODgvIjtzOjQ6InVyaXMiO2E6MjU6e2k6MTtzOjE6Ii8iO2k6MjtzOjEwOiIvYWJvdXQtdXMvIjtpOjEwO3M6Mjk6Ii9hYm91dC11cy9idXNpbmVzcy1zdHJ1Y3R1cmUvIjtpOjExO3M6MjA6Ii9hYm91dC11cy9kaXJlY3RvcnMvIjtpOjEyO3M6MjA6Ii9hYm91dC11cy9sb2NhdGlvbnMvIjtpOjEzO3M6MTg6Ii9hYm91dC11cy9oaXN0b3J5LyI7aTozO3M6MTQ6Ii9vdXItYnVzaW5lc3MvIjtpOjE0O3M6MjQ6Ii9vdXItYnVzaW5lc3MvbG9naXN0aWNzLyI7aToxNTtzOjI1OiIvb3VyLWJ1c2luZXNzL2Zvb2Qtc2FsZXMvIjtpOjE2O3M6MjM6Ii9vdXItYnVzaW5lc3MvYmVsZmllbGQvIjtpOjE3O3M6MjE6Ii9vdXItYnVzaW5lc3MvaWNlcGFrLyI7aToxODtzOjI0OiIvb3VyLWJ1c2luZXNzL2x1Y2t5LXJlZC8iO2k6NDtzOjEzOiIvY3Nlci1wbGVkZ2UvIjtpOjE5O3M6MzE6Ii9jc2VyLXBsZWRnZS9jYXVzZXMtd2Utc3VwcG9ydC8iO2k6MjA7czo0NzoiL2NzZXItcGxlZGdlL291ci1jb21taXRtZW50LXRvLXRoZS1lbnZpcm9ubWVudC8iO2k6NTtzOjE2OiIvbmV3cy1hbmQtbWVkaWEvIjtpOjY7czo5OiIvY2FyZWVycy8iO2k6NztzOjEyOiIvY29udGFjdC11cy8iO2k6ODtzOjE0OiIvY29va2llLXVzYWdlLyI7aTo5O3M6MTA6Ii9zaXRlLW1hcC8iO2k6MjE7czo4NToiL25ld3MtYW5kLW1lZGlhLzE1LW1pbGxpb24taG90LWNyb3NzLWJ1bnMtd2VyZS1pbi15ZWFyc2xleS1ncm91cHMtc3RvcmVzLXRoaXMtZWFzdGVyLyI7aToyMjtzOjc0OiIvbmV3cy1hbmQtbWVkaWEvYmVsZmllbGRzLW5ldy1tci1raW5ncy1yYW5nZS1pcy1ydWxpbmctdGhlLWRlc3NlcnQtbWFya2V0LyI7aToyMztzOjU5OiIvbmV3cy1hbmQtbWVkaWEveWVhcnNsZXktZ3JvdXAtaW52ZXN0LTA1bS1hdC1ncmltc2J5LWRlcG90LyI7aToyNDtzOjY1OiIvbmV3cy1hbmQtbWVkaWEvMjBtLWV4cGFuc2lvbi1wbGFuLWZvci15ZWFyc2xleS1ncm91cC1hdC1oZXl3b29kLyI7aToyNTtzOjM0OiIvY2FyZWVycy9hLXBvc2l0aW9uLXdpdGgteWVhcnNsZXkvIjt9czo5OiJ0ZW1wbGF0ZXMiO2E6MjU6e2k6MTtzOjE6IjEiO2k6MjtzOjE6IjIiO2k6MztzOjI6IjI1IjtpOjQ7czoxOiIyIjtpOjU7czoyOiIxMCI7aTo2O3M6MToiNCI7aTo3O3M6MToiNiI7aTo4O3M6MToiMiI7aTo5O3M6MToiMiI7aToxMDtzOjI6IjEyIjtpOjExO3M6MToiNyI7aToxMjtzOjE6IjgiO2k6MTM7czoxOiIyIjtpOjE0O3M6MjoiMTEiO2k6MTU7czoyOiIxMSI7aToxNjtzOjI6IjExIjtpOjE3O3M6MjoiMTEiO2k6MTg7czoyOiIxMSI7aToxOTtzOjE6IjIiO2k6MjA7czoxOiIyIjtpOjIxO3M6MToiOSI7aToyMjtzOjE6IjkiO2k6MjM7czoxOiI5IjtpOjI0O3M6MToiOSI7aToyNTtzOjE6IjMiO319fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `exp_snippets`
--

CREATE TABLE IF NOT EXISTS `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_specialty_templates`
--

CREATE TABLE IF NOT EXISTS `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `exp_specialty_templates`
--

INSERT INTO `exp_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`) VALUES
(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),
(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),
(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),
(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),
(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),
(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),
(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),
(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),
(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),
(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),
(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),
(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),
(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),
(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),
(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),
(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_stats`
--

CREATE TABLE IF NOT EXISTS `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_stats`
--

INSERT INTO `exp_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`) VALUES
(1, 1, 1, 1, 'Admin', 25, 0, 0, 0, 1374671340, 0, 0, 1374684861, 28, 1374602715, 1375178380);

-- --------------------------------------------------------

--
-- Table structure for table `exp_statuses`
--

CREATE TABLE IF NOT EXISTS `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_statuses`
--

INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`) VALUES
(1, 1, 1, 'open', 1, '009933'),
(2, 1, 1, 'closed', 2, '990000');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_groups`
--

CREATE TABLE IF NOT EXISTS `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_status_groups`
--

INSERT INTO `exp_status_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Statuses');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure`
--

CREATE TABLE IF NOT EXISTS `exp_structure` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `listing_cid` int(6) unsigned NOT NULL DEFAULT '0',
  `lft` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rgt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dead` varchar(100) NOT NULL,
  `hidden` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure`
--

INSERT INTO `exp_structure` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `listing_cid`, `lft`, `rgt`, `dead`, `hidden`) VALUES
(0, 0, 0, 0, 0, 1, 42, 'root', 'n'),
(1, 1, 0, 1, 0, 2, 3, '', 'n'),
(1, 2, 0, 2, 0, 4, 13, '', 'n'),
(1, 3, 0, 2, 0, 14, 25, '', 'n'),
(1, 4, 0, 2, 0, 26, 31, '', 'n'),
(1, 5, 0, 2, 9, 32, 33, '', 'n'),
(1, 6, 0, 2, 12, 34, 35, '', 'n'),
(1, 7, 0, 2, 0, 36, 37, '', 'n'),
(1, 8, 0, 15, 0, 38, 39, '', 'n'),
(1, 9, 0, 14, 0, 40, 41, '', 'n'),
(1, 10, 2, 4, 0, 5, 6, '', 'n'),
(1, 11, 2, 5, 0, 7, 8, '', 'n'),
(1, 12, 2, 6, 0, 9, 10, '', 'n'),
(1, 13, 2, 7, 0, 11, 12, '', 'n'),
(1, 14, 3, 3, 0, 15, 16, '', 'n'),
(1, 15, 3, 3, 0, 17, 18, '', 'n'),
(1, 16, 3, 3, 0, 19, 20, '', 'n'),
(1, 17, 3, 3, 0, 21, 22, '', 'n'),
(1, 18, 3, 3, 0, 23, 24, '', 'n'),
(1, 19, 4, 8, 0, 27, 28, '', 'n'),
(1, 20, 4, 8, 0, 29, 30, '', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_channels`
--

CREATE TABLE IF NOT EXISTS `exp_structure_channels` (
  `site_id` smallint(5) unsigned NOT NULL,
  `channel_id` mediumint(8) unsigned NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `type` enum('page','listing','asset','unmanaged') NOT NULL DEFAULT 'unmanaged',
  `split_assets` enum('y','n') NOT NULL DEFAULT 'n',
  `show_in_page_selector` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`site_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_channels`
--

INSERT INTO `exp_structure_channels` (`site_id`, `channel_id`, `template_id`, `type`, `split_assets`, `show_in_page_selector`) VALUES
(1, 2, 2, 'page', 'n', 'y'),
(1, 1, 1, 'page', 'n', 'y'),
(1, 4, 11, 'page', 'n', 'y'),
(1, 11, 4, 'page', 'n', 'y'),
(1, 12, 3, 'listing', 'n', 'y'),
(1, 13, 6, 'page', 'n', 'y'),
(1, 15, 2, 'page', 'n', 'y'),
(1, 8, 2, 'page', 'n', 'y'),
(1, 5, 7, 'page', 'n', 'y'),
(1, 7, 0, 'page', 'n', 'y'),
(1, 6, 8, 'page', 'n', 'y'),
(1, 9, 9, 'listing', 'n', 'y'),
(1, 10, 10, 'page', 'n', 'y'),
(1, 3, 11, 'page', 'n', 'y'),
(1, 14, 2, 'page', 'n', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_listings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_listings` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `template_id` int(6) unsigned NOT NULL DEFAULT '0',
  `uri` varchar(75) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_listings`
--

INSERT INTO `exp_structure_listings` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `template_id`, `uri`) VALUES
(1, 21, 5, 9, 9, '15-million-hot-cross-buns-were-in-yearsley-groups-stores-this-easter'),
(1, 22, 5, 9, 9, 'belfields-new-mr-kings-range-is-ruling-the-dessert-market'),
(1, 23, 5, 9, 9, 'yearsley-group-invest-0.5m-at-grimsby-depot'),
(1, 24, 5, 9, 9, '20m-expansion-plan-for-yearsley-group-at-heywood'),
(1, 25, 6, 12, 3, 'a-position-with-yearsley');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_members`
--

CREATE TABLE IF NOT EXISTS `exp_structure_members` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `nav_state` text,
  PRIMARY KEY (`site_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_settings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `var` varchar(60) NOT NULL,
  `var_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `exp_structure_settings`
--

INSERT INTO `exp_structure_settings` (`id`, `site_id`, `var`, `var_value`) VALUES
(1, 0, 'action_ajax_move', '46'),
(2, 0, 'module_id', '15'),
(17, 1, 'add_trailing_slash', 'y'),
(16, 1, 'hide_hidden_templates', 'y'),
(15, 1, 'redirect_on_publish', 'y'),
(14, 1, 'redirect_on_login', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_templates`
--

CREATE TABLE IF NOT EXISTS `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `exp_templates`
--

INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(1, 1, 1, 'index', 'y', 'webpage', '<!doctype html>\n  <!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n  <!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n  <!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n \n  <head> \n    <meta charset="UTF-8"> \n    \n    <!-- TITLE -->\n    <title>Project Name</title>\n    \n    <!-- META -->\n    <meta name="description" content="" />\n    \n    <!-- STYLES -->\n    <link rel="stylesheet" href="/resource/css/style.css" />\n    \n    <!-- MODERNIZR -->\n    <script src="/resource/js/libs/modernizr.min.js"></script>\n  </head>\n\n  <body>\n    <div id="container"> \n      <!-- HEADER -->\n      <header id="header" role="banner">\n\n        <div class="nine60">\n          <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n        </div>\n        <nav>\n          <div class="nine60">\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n              <li><a href="">Item Four</a></li>\n            </ul>\n          </div>\n        </nav>\n        <div class="breadcrumbs">\n          <div class="nine60"><p>You are here:</p>\n            <ul>\n              <li><a href="">Home</a></li>\n            </ul>\n          </div>\n        </div>\n      </header>\n      <div id="banners">\n        <div class="img1"><img src="/dummies/home_bean_banner.jpg" /></div>\n        <div class="img2"><img src="/dummies/home_logistics_banner.jpg" /></div>\n      </div>\n      \n      <!-- CONTENT -->\n      <div id="content">\n          <div class="nine60">\n          <section>\n            <div class="main left">\n              <h1>Home</h1>\n              <p>Yearsley Group is leading the way in frozen foods via its two divisions: logistics and food.</p>\n              <p>Yearsley Logistics is the UK’s largest logistics service provider in the frozen food sector, as well as offering ambient, chilled and freight forwarding solutions.</p>\n              <p>Yearsley Food markets and supplies a full range of innovative frozen products to customers in all sectors of the marketplace including export.</p>\n            </div>\n          </section>\n          <aside class="right">\n            <h2>Latest news and updates</h2>\n            <ul class="newsFeed">\n              <li>\n                <p class="date">22nd April 2013</p>\n                <img src="/dummies/dummy1.jpg" />\n                <p>Lorem ipsum Occaecat ea incididunt occaecat ex eiusmod ad laborum nisi non in magna.</p>\n                <p><a href="">Read more...</a></p>\n              </li>\n              <li>\n                <p class="date">22nd April 2013</p>\n                <img src="/dummies/dummy1.jpg" />\n                <p>Lorem ipsum Occaecat ea incididunt occaecat ex eiusmod ad laborum nisi non in magna.</p>\n                <p><a href="">Read more...</a></p>\n              </li>\n            </ul>\n            <p class="more"><a href="">More news</a></p>\n          </aside>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n              <ul>\n                <li><a href="">Item One</a></li>\n                <li><a href="">Item Two</a></li>\n                <li><a href="">Item Three</a></li>\n              </ul>\n           </nav>\n           <ul class="social">\n              <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n              <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n              <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n           </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="/resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="/resource/js/plugins.js"></script>\n    <script src="/resource/js/global.js"></script>\n  </body>\n</html>\n', '', 1373905814, 1, 'n', 0, '', 'n', 'n', 'o', 160),
(2, 1, 1, '2col', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="/resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="/resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>About Us</h1>\n          <ul class="subNav">\n            <li><a href="">Item One</a></li>\n            <li class="selected"><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n            <li><a href="">Item Five</a></li>\n          </ul>\n      </div>\n      <img src="/dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside>\n          <h3>Accreditations</h3>\n          <ul class="newsFeed">\n            <li>\n              <p class="date">22nd April 2013</p>\n              <img src="/dummies/dummy1.jpg" />\n              <p>Lorem ipsum Occaecat ea incididunt occaecat ex eiusmod ad laborum nisi non in magna.</p>\n              <p><a href="">Read more...</a></p>\n            </li>\n            <li>\n              <p class="date">22nd April 2013</p>\n              <img src="/dummies/dummy1.jpg" />\n              <p>Lorem ipsum Occaecat ea incididunt occaecat ex eiusmod ad laborum nisi non in magna.</p>\n              <p><a href="">Read more...</a></p>\n            </li>\n          </ul>\n          <p class="more"><a href="">More news</a></p>\n        </aside>\n        <section>\n          <div class="main">\n            <h1>The Yearsley Group</h1>\n            <p><img src="/dummies/about_img.jpg" />Yearsley Group is leading the way in frozen foods via its two divisions: logistics and food sales.</p>\n\n            <p>Yearsley Logistics is the UK’s largest logistics service provider in the frozen food sector, as well as offering ambient, chilled and freight forwarding solutions.\n              It has 13 sites nationally, a total capacity of 320,000 pallets and a fleet of over 300 temperature controlled vehicles delivering nationwide.</p>\n\n              <p>Yearsley Food markets and supplies a full range of innovative frozen products to customers in all sectors of the marketplace including export. It also offers exclusive imported frozen foods under its Belfield brand and IcePak, the Group’s specialist frozen seafood supplier serves the wholesale and restaurant market.</p>\n\n              <p>An industry recognised organisation, Yearsley Group has BRC and ISO 14001 accreditations and plays an active part in the British Frozen Food Federation and the RHA.</p>\n\n              <p>The future of the company is based on delivering a sustainable business model, focused on innovation, agility and customer service excellence. A commitment to re-investment has recently seen the development of our first ‘Super Hub’ consolidation centre at Heywood, which will be completed May 2013, as well as the purchase of Hams Hall in the Midlands, bringing our Cold Store network to 13 sites.</p>\n              <p class="pageLink"><a href="">Next: Food Sales</a></p>\n            </div>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="/resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="/resource/js/plugins.js"></script>\n    <script src="/resource/js/global.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 409),
(3, 1, 1, 'careers-item', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>Careers</h1>\n      </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside>\n          <h3>Working with us</h3>\n          <p>Want to work for a Nationwide company with a proven track record of success?</p>\n          <p>Want a job that offers both security and progression?</p>\n          <p>Then apply now….</p>\n        </aside>\n        <section>\n          <div class="main careers-item">\n            <h2>Title of the vacancy</h2>\n            <ul>\n              <li>Work type: <span>Sales</span></li>\n              <li>Salary: <span>£22,000</span></li>\n              <li>Location: <span>Heywood</span></li>\n            </ul>\n            <p>Lorem ipsum Laborum reprehenderit proident dolore laborum incididunt nulla esse qui Excepteur esse in est nisi sed ut mollit cillum elit Duis laboris velit nostrud veniam consequat magna Duis ullamco voluptate quis in laboris.</p>\n            <p class="pageLink back"><a href="">Back</a></p>\n            <h3>Apply now</h3>\n            <form class="contact-form">\n              <input type="hidden" name="params_id" value="17423" />\n              <input type="hidden" name="XID" value="d9d14bf6f164a7d740c3eb0dd0aef94cf3cc6981" />                        \n              <div>\n                <label>Your first name*:</label>\n                <input type="text" name="firstname" value="" id="freeform_firstname" maxlength="150" class="required"  />\n              </div>\n              <div>\n                <label>Your last name*:</label>\n                <input type="text" name="lastname" value="" id="freeform_lastname" maxlength="150" class="required"  />\n              </div>\n              <div>\n                <label>Your email address*:</label>\n                <input type="text" name="email" value="" id="freeform_email" maxlength="150" class="required"  />\n              </div>\n               <div>\n                <label>Your phone number:</label>\n                <input type="text" name="number" value="" id="freeform_number" maxlength="150"  />\n              </div>\n              <div class="cvUpload">\n                <label>Upload your CV:</label>\n                <input type="file" name="cvupload[0]" value="" id="freeform_cvupload0"  />\n              </div>\n              <div>\n                <label>Covering note*:</label>\n                <textarea name="cover-note" cols="50" rows="6" id="freeform_note" class="required" ></textarea>\n              </div>\n              <div class="optin">\n                <input type="checkbox" name="newsletter_signup[]" value="Yes" checked/>\n                <label>Please un-tick the box if you WOULD NOT like to register for the Yearsley Group e-newsletter and other relevant email communications from us and our associated companies.</label>\n              </div>\n              <div class="captcha">\n                <label>Please enter the word you see in the image below:</label>\n                <div>\n                  <img src="http://yearsley.stmpreview.co.uk/images/captchas/1369214964.1324.jpg" width="140" height="30" style="border:0;" alt=" " />\n                  <input type="text" name="captcha" size="20" class="required captcha" />\n                </div>\n                <input type="submit" name="submit" value="Send" class="submit">\n              </div>\n            </form>\n          </div>\n        </section>\n      </div>\n    </div>\n\n    <!-- FOOTER -->\n    <footer id="footer">\n      <div class="nine60">\n        <nav>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n        </nav>\n        <ul class="social">\n          <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n          <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n          <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n        </ul>\n      </div>\n      <div>\n        <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n      </div>\n    </footer>\n    <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n  </div>\n\n  <!-- SCRIPTS -->\n  <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n  <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n  <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n  <script src="resource/js/plugins.js"></script>\n  <script src="resource/js/global.js"></script>\n</body>\n</html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 3),
(4, 1, 1, 'careers', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>Careers</h1>\n       </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside>\n          <h3>Working with us</h3>\n          <p>Want to work for a Nationwide company with a proven track record of success?</p>\n          <p>Want a job that offers both security and progression?</p>\n          <p>Then apply now….</p>\n        </aside>\n          <section>\n            <div class="main careers">\n              <h2>Careers: Current opportunities</h2>\n              <p>No vacancies at present. Please check back soon or alternatively send your CV for future consideration to hr@yearsley.co.uk</p>\n              <p>Below are the current vacancies within Yearsley.</p>\n              <ul>\n                <li>\n                  <h4>This is the title of the job</h4>\n                  <ul>\n                    <li>Work type: <span>Sales</span></li>\n                    <li>Salary: <span>£22,000</span></li>\n                    <li>Location: <span>Heywood</span></li>\n                  </ul>\n                  <p>Lorem ipsum Laborum reprehenderit proident dolore laborum incididunt nulla esse qui Excepteur esse in est nisi sed ut mollit cillum elit Duis laboris velit nostrud veniam consequat magna Duis ullamco voluptate quis in laboris.</p>\n                  <p class="pageLink"><a href="">Full details</a></p>\n                </li>\n               <li>\n                  <h4>This is the title of the job</h4>\n                  <ul>\n                    <li>Work type: <span>Sales</span></li>\n                    <li>Salary: <span>£22,000</span></li>\n                    <li>Location: <span>Heywood</span></li>\n                  </ul>\n                  <p>Lorem ipsum Laborum reprehenderit proident dolore laborum incididunt nulla esse qui Excepteur esse in est nisi sed ut mollit cillum elit Duis laboris velit nostrud veniam consequat magna Duis ullamco voluptate quis in laboris.</p>\n                  <p class="pageLink"><a href="">Full details</a></p>\n                </li>\n              </ul>\n            </div>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="resource/js/plugins.js"></script>\n    <script src="resource/js/global.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 229),
(5, 1, 1, 'contact-thankyou', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>Contact Us</h1>\n      </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside class="contactDetails">\n          <h3>Contact Info</h3>\n          <p class="headOffice"><strong>Head Office:</strong> Hareshill Road, Heywood,<br>\n          Lancashire, OL10 2TP</p>\n          <p>If you know the department you would like to reach, please call:</p>\n\n          <ul>\n            <li><strong>Logistics Division:</strong><br>\n            Tel: 01706 694680</li>\n            <li><strong>Food Sales Division:</strong><br>\n            Tel: 01706 694600</li>\n            <li><strong>Belfield:</strong><br>\n            Tel: 01706 694610</li>\n            <li><strong>IcePak:</strong><br>\n            Tel: 01706 694655</li>\n            <li><strong>Lucky Red:</strong><br>\n            Tel: 0161 6437888</li>\n            <li><strong>Accounts:</strong><br>\n            Tel: 01706 694640</li>\n            <li><strong>Human Resources:</strong><br>\n            Tel: 01706 694277</li>\n            <li><strong>Marketing:</strong><br>\n            Tel: 01706 694648</li>\n          </ul>\n          <div><p><strong>For General Enquiries please call:</strong><br>\n            01706 694600</p></div>\n        </aside>\n          <section>\n            <div class="main">\n              <h2>Thank you</h2>\n              <p>Your information has been forwarded to our enquiries team and we''ll be in-touch if appropriate..</p>\n              <p>To receive regular updates from Yearsley you can sign up to our newsletter below.</p>\n            </div>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <div class="newsletter">\n          <form class="newsletter-form nine60">\n              <label>Want news updates in your inbox from the Yearsley Group? Enter your email address...</label>\n              <input type="text" name="firstname" value="" id="freeform_firstname" maxlength="150" class="required"  />\n              <input type="image" src="/resource/stat/newsletter-submit.jpg" name="submit" value="Send" class="submit">\n            </form>\n        </div>\n      <footer id="footer">\n         <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div class="info">\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="/resource/js/main.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(6, 1, 1, 'contact', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>Contact Us</h1>\n      </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside class="contactDetails">\n          <h3>Contact Info</h3>\n          <p class="headOffice"><strong>Head Office:</strong> Hareshill Road, Heywood,<br>\n          Lancashire, OL10 2TP</p>\n          <p>If you know the department you would like to reach, please call:</p>\n\n          <ul>\n            <li><strong>Logistics Division:</strong><br>\n            Tel: 01706 694680</li>\n            <li><strong>Food Sales Division:</strong><br>\n            Tel: 01706 694600</li>\n            <li><strong>Belfield:</strong><br>\n            Tel: 01706 694610</li>\n            <li><strong>IcePak:</strong><br>\n            Tel: 01706 694655</li>\n            <li><strong>Lucky Red:</strong><br>\n            Tel: 0161 6437888</li>\n            <li><strong>Accounts:</strong><br>\n            Tel: 01706 694640</li>\n            <li><strong>Human Resources:</strong><br>\n            Tel: 01706 694277</li>\n            <li><strong>Marketing:</strong><br>\n            Tel: 01706 694648</li>\n          </ul>\n          <div><p><strong>For General Enquiries please call:</strong><br>\n            01706 694600</p></div>\n        </aside>\n          <section>\n            <div class="main">\n              <h2>Drop us a line</h2>\n              <p>If you have any queries, please complete the form below or contact us on the phone numbers provided.</p>\n              <form class="contact-form">\n                <input type="hidden" name="params_id" value="17423" />\n                <input type="hidden" name="XID" value="d9d14bf6f164a7d740c3eb0dd0aef94cf3cc6981" />                        \n                  <div>\n                    <label>Your first name*:</label>\n                    <input type="text" name="firstname" value="" id="freeform_firstname" maxlength="150" class="required"  />\n                  </div>\n                  <div>\n                    <label>Your last name*:</label>\n                    <input type="text" name="lastname" value="" id="freeform_lastname" maxlength="150" class="required"  />\n                  </div>\n                  <div>\n                    <label>Your email address*:</label>\n                    <input type="text" name="email" value="" id="freeform_email" maxlength="150" class="required"  />\n                  </div>\n                  <div>\n                    <label>Subject*:</label>\n                    <input type="text" name="subject" value="" id="freeform_subject" maxlength="150" class="required"  />\n                  </div>\n                  <div>\n                    <label>Your message*:</label>\n                    <textarea name="message" cols="50" rows="6" id="freeform_message" class="required" ></textarea>\n                  </div>\n                  <div class="optin">\n                      <input type="checkbox" name="newsletter_signup[]" value="Yes" checked/>\n                      <label>Please un-tick the box if you WOULD NOT like to register for the Yearsley Group e-newsletter and other relevant email communications from us and our associated companies.</label>\n                  </div>\n                  <div class="captcha">\n                    <label>Please enter the word you see in the image below:</label>\n                    <div>\n                      <img src="http://yearsley.stmpreview.co.uk/images/captchas/1369214964.1324.jpg" width="140" height="30" style="border:0;" alt=" " />\n                      <input type="text" name="captcha" size="20" class="required captcha" />\n                    </div>\n                    <input type="submit" name="submit" value="Send" class="submit">\n                  </div>\n                </form>\n            </div>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <div class="newsletter">\n          <form class="newsletter-form nine60">\n              <label>Want news updates in your inbox from the Yearsley Group? Enter your email address...</label>\n              <input type="text" name="firstname" value="" id="freeform_firstname" maxlength="150" class="required"  />\n              <input type="image" src="/resource/stat/newsletter-submit.jpg" name="submit" value="Send" class="submit">\n            </form>\n        </div>\n      <footer id="footer">\n         <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div class="info">\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="/resource/js/main.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 5),
(7, 1, 1, 'directors', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>About Us</h1>\n          <ul class="subNav">\n            <li><a href="">About Us</a></li>\n            <li><a href="">Business Structure</a></li>\n            <li class="selected"><a href="">Directors</a></li>\n            <li><a href="">Locations</a></li>\n            <li><a href="">History</a></li>\n          </ul>\n      </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside class="singleImg">\n          <h2>Directors</h2>\n          <p><img src="/dummies/Harry-Yearsley.jpg" /></p>\n          <h3>Harry Yearsley</h3>\n          <p>Harry joined the business in 1982 and was made Managing Director in 1998 at the age of 32. Harry has experience in all aspects of the supply chain, and has been instrumental in the considerable growth and development of the business in the last 15 years.</p>\n        </aside>\n          <section>\n            <div class="main">\n              <ul class="directors">\n                <li>\n                  <h3>Jonathan Baker</h3>\n                  <img src="/dummies/dir_jb.jpg" />\n                  <p>Jonathan has 30 years’ experience in frozen food sales and the supply chain. Following a long and successful period in the food business, Jonathan is now responsible for all company sales, marketing and procurement, as well as the CSER strategy.</p>\n                </li>\n                <li>\n                  <h3>Jonathan Baker</h3>\n                  <img src="/dummies/dir_jb.jpg" />\n                  <p>Jonathan has 30 years’ experience in frozen food sales and the supply chain. Following a long and successful period in the food business, Jonathan is now responsible for all company sales, marketing and procurement, as well as the CSER strategy.</p>\n                </li>\n                <li>\n                  <h3>Jonathan Baker</h3>\n                  <img src="/dummies/dir_jb.jpg" />\n                  <p>Jonathan has 30 years’ experience in frozen food sales and the supply chain. Following a long and successful period in the food business, Jonathan is now responsible for all company sales, marketing and procurement, as well as the CSER strategy.</p>\n                </li>\n                <li>\n                  <h3>Jonathan Baker</h3>\n                  <img src="/dummies/dir_jb.jpg" />\n                  <p>Jonathan has 30 years’ experience in frozen food sales and the supply chain. Following a long and successful period in the food business, Jonathan is now responsible for all company sales, marketing and procurement, as well as the CSER strategy.</p>\n                </li>\n                <li>\n                  <h3>Jonathan Baker</h3>\n                  <img src="/dummies/dir_jb.jpg" />\n                  <p>Jonathan has 30 years’ experience in frozen food sales and the supply chain. Following a long and successful period in the food business, Jonathan is now responsible for all company sales, marketing and procurement, as well as the CSER strategy.</p>\n                </li>\n              </ul>\n              <p class="pageLink"><a href="">Next: Locations</a></p>\n            </div>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="resource/js/plugins.js"></script>\n    <script src="resource/js/global.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 32);
INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(8, 1, 1, 'locations', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>About Us</h1>\n        <ul class="subNav">\n          <li><a href="">Item One</a></li>\n          <li class="selected"><a href="">Item Two</a></li>\n          <li><a href="">Item Three</a></li>\n          <li><a href="">Item Four</a></li>\n          <li><a href="">Item Five</a></li>\n        </ul>\n      </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside class="locations">\n          <h3>How to use</h3>\n          <p class="no-bold">Click on a site below to focus in the main map. Click the arrow to expand location.</p>\n          <h4 class="location-hdr">13 Locations</h4>\n          <ul class="location">\n            <li><a href="" id="heywood"><span>1</span> Heywood (Head Office)</a><br>\n              <span class="location-info"><strong>Address:</strong><br>\n                Hareshill Road, Heywood,</br>Lancashire, OL10 2TP</br>\n                <strong>Tel:</strong> 01706 694 600\n              </span>\n            </li>\n            <li><a href="" id="belle-eau-park"><span>2</span> Belle Eau Park</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Belle Eau Park, Bilsthorpe, Newark, Notts, NG22 8TX\n              </span>\n            </li>\n            <li><a href="" id="bellshill"><span>3</span> Bellshill</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Belgowan Street, North Industrial Estate, Bellshill, Lanarkshire, ML4 3LB\n              </span>\n            </li>\n            <li><a href="" id="bristol"><span>4</span> Bristol</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Garanor Way,<br> Portbury, Bristol, BS20 7XT\n              </span>\n            </li>\n            <li><a href="" id="coleshill"><span>5</span> Coleshill</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Gorsey Lane, Coleshill, Birmingham, B46 1JU\n              </span>\n            </li>\n            <li><a href="" id="gillingham"><span>6</span> Gillingham</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Valentine Close, Gillingham Business Park, Gillingham, Kent, ME8 0QR\n              </span>\n            </li>\n            <li><a href="" id="grimsby"><span>7</span> Grimsby</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Marsden Road, Grimsby,<br> N.E. Lincs, DN31 3FS\n              </span>\n            </li>\n            <li><a href="" id="hams-hall"><span>8</span> Hams Hall</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Unit 7, Faraday Avenue, Hams Hall Distribution Park, Coleshill,<br> Birmingham, B46 1AL\n              </span>\n            </li>\n            <li><a href="" id="holmewood"><span>9</span> Holmewood</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Tupton Way, Holmewood Industrial Estate, Chesterfield, Derbyshire, S42 5BX\n              </span>\n            </li>\n            <li><a href="" id="rochdale"><span>10</span> Rochdale</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Albert Royds St, Rochdale,<br> Lancashire, OL16 5AA\n              </span>\n            </li>\n            <li><a href="" id="scunthorpe"><span>11</span> Scunthorpe</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Normanby Road, Cupola Way,<br> Scunthorpe, DN15 9YJ\n              </span>\n            </li>\n            <li><a href="" id="seaham"><span>12</span> Seaham</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Fox Cover industrial Est. Admiralty Way, Dawdon, Seaham, SR7 7DN\n              </span>\n            </li>\n            <li><a href="" id="wisbech"><span>13</span> Wisbech</a>\n              <span class="location-info"><strong>Address:</strong><br>\n                Weasenham Lane, Wisbech,<br> Cambridgeshire, PE13 2RN\n              </span>\n            </li>\n          </ul>\n\n\n        </aside>\n        <section>\n          <div class="main">\n            <h1>Locations</h1>\n            <div class="location-map">\n             <img src="/resource/stat/uk_map.png" class="map">\n             <div class="icon heywood"><a href="" id="heywood">1</a></div>\n             <div class="icon belle-eau-park"><a href="" id="belle-eau-park">2</a></div>\n             <div class="icon bellshill"><a href="" id="bellshill">3</a></div>\n             <div class="icon bristol"><a href="" id="bristol">4</a></div>\n             <div class="icon coleshill"><a href="" id="coleshill">5</a></div>\n             <div class="icon gillingham"><a href="" id="gillingham">6</a></div>\n             <div class="icon grimsby"><a href="" id="grimsby">7</a></div>\n             <div class="icon hams-hall"><a href="" id="hams-hall">8</a></div>\n             <div class="icon holmewood"><a href="" id="holmewood">9</a></div>\n             <div class="icon rochdale"><a href="" id="rochdale">10</a></div>\n             <div class="icon scunthorpe"><a href="" id="scunthorpe">11</a></div>\n             <div class="icon seaham"><a href="" id="seaham">12</a></div>\n             <div class="icon wisbech"><a href="" id="wisbech">13</a></div>\n\n             <div class="popup">\n              <div class="pop-wrap">\n                <img src="/resource/stat/pop_arrow_down.png" width="30" height="16" class="pop-arrow">\n                <div class="heywood info">\n                  <h3>Heywood (Head Office)</h3>\n                  <p>Hareshill Road, Heywood, Lancashire, OL10 2TP</p>\n                </div>\n                <div class="belle-eau-park info">\n                  <h3>Belle Eau Park</h3>\n                  <p>Belle Eau Park, Bilsthorpe, Newark, Notts, NG22 8TX</p>\n                </div>\n                <div class="bellshill info">\n                  <h3>Bellshill</h3>\n                  <p>Belgowan Street, North Industrial Estate, Bellshill, Lanarkshire, ML4 3LB</p>\n                </div>\n                <div class="bristol info">\n                  <h3>Bristol</h3>\n                  <p>Garanor Way, Portbury, Bristol, BS20 7XT</p>\n                </div>\n                <div class="coleshill info">\n                  <h3>Coleshill</h3>\n                  <p>Gorsey Lane, Coleshill, Birmingham, B46 1JU</p>\n                </div>\n                <div class="gillingham info">\n                  <h3>Gillingham</h3>\n                  <p>Valentine Close, Gillingham Business Park, Gillingham, Kent, ME8 0QR</p>\n                </div>\n                <div class="grimsby info">\n                  <h3>Grimsby</h3>\n                  <p>Marsden Road, Grimsby, N.E. Lincs, DN31 3FS</p>\n                </div>\n                <div class="hams-hall info">\n                  <h3>Hams Hall</h3>\n                  <p>Unit 7, Faraday Avenue, Hams Hall Distribution Park, Coleshill, Birmingham, B46 1AL</p>\n                </div>\n                <div class="holmewood info">\n                  <h3>Holmewood</h3>\n                  <p>Tupton Way, Holmewood Industrial Estate, Chesterfield, Derbyshire, S42 5BX</p>\n                </div>\n                <div class="rochdale info">\n                  <h3>Rochdale</h3>\n                  <p>Albert Royds St, Rochdale, Lancashire, OL16 5AA</p>\n                </div>\n                <div class="scunthorpe info">\n                  <h3>Scunthorpe</h3>\n                  <p>Normanby Road, Cupola Way, Scunthorpe, DN15 9YJ</p>\n                </div>\n                <div class="seaham info">\n                  <h3>Seaham</h3>\n                  <p>Fox Cover industrial Est. Admiralty Way, Dawdon, Seaham, SR7 7DN</p>\n                </div>\n                <div class="wisbech info">\n                  <h3>Wisbech</h3>\n                  <p>Weasenham Lane, Wisbech, Cambridgeshire, PE13 2RN</p>\n                </div>\n              </div>\n            </div>\n          </section>\n        </div>\n      </div>\n\n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n\n    <!-- SCRIPTS -->\n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="/resource/js/main.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 17),
(9, 1, 1, 'news-item', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>Latest News</h1>\n        <ul class="newsPDFs">\n          <li><a href="">Newsletter December 2012 Edition</a></li>\n          <li><a href="">Newsletter November 2012 Edition</a></li>\n        </ul>\n      </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside>\n          <h3>Archive</h3>\n          <ul class="newsArchive">\n            <li>2013\n              <ul>\n                <li><a href="">May</a>(1)</li>\n                <li><a href="">April</a>(2)</li>\n                <li><a href="">February</a>(4)</li>\n              </ul>\n            </li>\n            <li>2012\n              <ul>\n                <li><a href="">May</a>(1)</li>\n                <li><a href="">March</a>(3)</li>\n                <li><a href="">January</a>(5)</li>\n              </ul>\n            </li>\n          </ul>\n        </aside>\n          <section>\n            <div class="main">\n              <h2>Yearsley Logistics total spend now £5million on Renewable Energy</h2>\n              <p><img src="dummies/news.jpg" />\n              Yearsley Logistics, the UK’s largest frozen logistics provider, have installed a further £1 million worth of new solar panels across their Hams Hall, Heywood and Holmewood depots as they continue their focus on reducing electricity use. The 3120 new panels will generate 1,252,500 kilowatt hours per annum across the 3 sites, and will contribute to improving energy use by 8% by 2015 as part of their ISO 14001 environmental accreditation as well as improving their CO2 emissions from power use.</p>\n\n              <p>Having solar panels across 8 of its 13 depots will help to reduce the reliance on the national grid. Yearsley Logistics also has plans to install other renewable solutions such as wind power at depots around the UK. These installations come one month after the introduction of the group’s first urban trailers to the fleet, with the aim of further reducing the carbon footprint from its nationwide food sales operation around towns and urban areas.</p>\n\n              <p>Yearsley Group are committed to sustainability and have in the last year implemented management systems for reducing waste road miles and installed energy efficient LED lighting and doors within their cold stores. All of these green initiatives were recently recognised with their ‘Energy and Carbon Management’ nominations as a sustainability champion in the 2013 2degrees sustainability awards</p>\n              <p class="newsDate">15.05.2013</p>\n              <p class="pageLink back"><a href="">Back</a></p>\n            </div>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="resource/js/plugins.js"></script>\n    <script src="resource/js/global.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 85),
(10, 1, 1, 'news', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>Latest News</h1>\n        <ul class="newsPDFs">\n          <li><a href="">Newsletter December 2012 Edition</a></li>\n          <li><a href="">Newsletter November 2012 Edition</a></li>\n        </ul>\n      </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside>\n          <h3>Archive</h3>\n          <ul class="newsArchive">\n            <li>2013\n              <ul>\n                <li><a href="">May</a>(1)</li>\n                <li><a href="">April</a>(2)</li>\n                <li><a href="">February</a>(4)</li>\n              </ul>\n            </li>\n            <li>2012\n              <ul>\n                <li><a href="">May</a>(1)</li>\n                <li><a href="">March</a>(3)</li>\n                <li><a href="">January</a>(5)</li>\n              </ul>\n            </li>\n          </ul>\n        </aside>\n          <section>\n            <div class="main">\n              <h2>Latest News</h2>\n              <ul class="newsItems">\n                <li>\n                  <h3><a href="">Yearsley Logistics total spend now £5million on Renewable Energy</a></h3>\n                  <p>Additional Investment in roof-mounted solar panel installations of £1million.</p>\n                  <p class="newsDate">15.05.2013</p>\n                  <p class="pageLink"><a href="">Read more</a></p>\n                </li>\n                <li>\n                  <h3><a href="">Yearsley Logistics total spend now £5million on Renewable Energy</a></h3>\n                  <p>Additional Investment in roof-mounted solar panel installations of £1million.</p>\n                  <p class="newsDate">15.05.2013</p>\n                  <p class="pageLink"><a href="">Read more</a></p>\n                </li>\n                <li>\n                  <h3><a href="">Yearsley Logistics total spend now £5million on Renewable Energy</a></h3>\n                  <p>Additional Investment in roof-mounted solar panel installations of £1million.</p>\n                  <p class="newsDate">15.05.2013</p>\n                  <p class="pageLink"><a href="">Read more</a></p>\n                </li>\n              </ul>\n              <p class="newsPagination">Page 1 of 10 pages  <a href="">1</a> <a href="">2</a> <a href="">3</a>&nbsp;&nbsp;<a href="">Last &rsaquo;</a></p>\n            </div>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="resource/js/plugins.js"></script>\n    <script src="resource/js/global.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 198),
(11, 1, 1, 'our-businesses', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="/resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="/resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="/">Item One</a></li>\n            <li><a href="/">Item Two</a></li>\n            <li><a href="/">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="/">Item One</a></li>\n            <li><a href="/">Item Two</a></li>\n            <li><a href="/">Item Three</a></li>\n            <li><a href="/">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="/">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>Our Businesses</h1>\n          <ul class="subNav">\n            <li><a href="/">Item One</a></li>\n            <li class="selected"><a href="/">Logistics</a></li>\n            <li><a href="/">Food Sales</a></li>\n            <li><a href="/">Belfield</a></li>\n            <li><a href="/">IcePak</a></li>\n            <li><a href="/">Lucky Red</a></li>\n          </ul>\n          <p class="siteLink"><a href="/"><img src="//resource/stat/logolink-belfield.jpg" /></a></p>\n      </div>\n      <img src="/dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <aside>\n          <ul class="sideImages">\n            <li><img src="/dummies/logistics1.jpg" /></li>\n            <li><img src="/dummies/logistics2.jpg" /></li>\n           </ul>\n        </aside>\n        <section>\n          <div class="main">\n            <h1>Business Section</h1>\n            <p>As the largest frozen food logistics service provider in the UK, we can offer customers large and small, day one for day two, 7 days a week into major retailers, as well as an un-rivalled foodservice distribution network.</p>\n              <p class="pageLink"><a href="/">Next: Food Sales</a></p>\n            </div>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="/">Item One</a></li>\n              <li><a href="/">Item Two</a></li>\n              <li><a href="/">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href="/"><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href="/"><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href="/"><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="/http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="/resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="/resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="/resource/js/plugins.js"></script>\n    <script src="/resource/js/global.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 369),
(12, 1, 1, 'structure', 'y', 'webpage', '<!doctype html>\n<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->\n<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->\n<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->\n\n<head> \n  <meta charset="UTF-8"> \n\n  <!-- TITLE -->\n  <title>Project Name</title>\n\n  <!-- META -->\n  <meta name="description" content="" />\n\n  <!-- STYLES -->\n  <link rel="stylesheet" href="resource/css/style.css" />\n\n  <!-- MODERNIZR -->\n  <script src="resource/js/libs/modernizr.min.js"></script>\n</head>\n\n<body>\n  <div id="container"> \n    <!-- HEADER -->\n    <header id="header" role="banner">\n\n      <div class="nine60">\n        <div class="brand"><img src="/resource/stat/yearsley-group-logo.png"></div>\n      </div>\n      <nav>\n        <div class="nine60">\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n          </ul>\n          <ul>\n            <li><a href="">Item One</a></li>\n            <li><a href="">Item Two</a></li>\n            <li><a href="">Item Three</a></li>\n            <li><a href="">Item Four</a></li>\n          </ul>\n        </div>\n      </nav>\n      <div class="breadcrumbs">\n        <div class="nine60"><p>You are here:</p>\n          <ul>\n            <li><a href="">Home</a></li>\n          </ul>\n        </div>\n      </div>\n    </header>\n    <div id="banners">\n      <div class="nine60">\n        <h1>About Us</h1>\n          <ul class="subNav">\n            <li><a href="">About Us</a></li>\n            <li><a href="">Business Structure</a></li>\n            <li><a href="">Directors</a></li>\n            <li class="selected"><a href="">Locations</a></li>\n            <li><a href="">History</a></li>\n          </ul>\n      </div>\n      <img src="dummies/cser-page.jpg" width="100%" height="325px" />\n    </div>\n\n    <!-- CONTENT -->\n    <div id="content">\n      <div class="nine60">\n        <h2>Business Structure</h2>\n        <section>\n          <ul class="level1">\n            <li><span><img src="/dummies/bs_yearsley-group.jpg" /></span>\n              <ul class="level2">\n                <li class=""><span><img src="/dummies/bs_header-logistics.jpg" /></span>\n                  <ul class="level3">\n                    <li><a href=""><img src="/dummies/bs_coldstore.jpg" /></a></li>\n                    <li><a href=""><img src="/dummies/bs_distribution.jpg" /></a></li>\n                    <li><a href=""><img src="/dummies/bs_ambient.jpg" /></a></li>\n                    <li><a href=""><img src="/dummies/bs_logistics.jpg" /></a></li>\n                  </ul>\n                </li>\n                <li><span><img src="/dummies/bs_header-food.jpg" /></span>\n                  <ul class="level3">\n                    <li><a href=""><img src="/dummies/bs_foodsales.jpg" /></a></li>\n                    <li><a href=""><img src="/dummies/bs_belfield.jpg" /></a></li>\n                    <li><a href=""><img src="/dummies/bs_icepak.jpg" /></a></li>\n                    <li><a href=""><img src="/dummies/bs_luckyred.jpg" /></a></li>\n                  </ul>\n                </li>\n              </ul>\n\n            </li>\n          </ul>\n        <p class="pageLink"><a href="">Next: Directors</a></p>\n          </section>\n        </div>\n      </div>\n      \n      <!-- FOOTER -->\n      <footer id="footer">\n        <div class="nine60">\n          <nav>\n            <ul>\n              <li><a href="">Item One</a></li>\n              <li><a href="">Item Two</a></li>\n              <li><a href="">Item Three</a></li>\n            </ul>\n          </nav>\n          <ul class="social">\n            <li class="fb"><a href=""><span>Like us on Facebook</span></a></li>\n            <li class="tw"><a href=""><span>Follow us on Twitter</span></a></li>\n            <li class="ld"><a href=""><span>Find us on Linkedin</span></a></li>\n          </ul>\n        </div>\n        <div>\n          <p class="nine60">&copy; Harry Yearsley Ltd Hareshill Road Heywood, Greater Manchester, OL10 2TP</p>\n        </div>\n      </footer>\n      <div class="colophon nine60"><p>Website design &amp; build by: Shoot the Moon Digital</p></div>\n    </div>\n    \n    <!-- SCRIPTS -->\n    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>\n    <script>window.jQuery || document.write(''<script src="resource/js/libs/jquery-1.9.1.min.js">\\x3C/script>'')</script>\n    <!--[if (gte IE 6)&(lte IE 8)]><script src="resource/js/libs/selectivizr.min.js"></script><![endif]-->\n    <script src="resource/js/plugins.js"></script>\n    <script src="resource/js/global.js"></script>\n  </body>\n  </html>\n', NULL, 1373905845, 1, 'n', 0, '', 'n', 'n', 'o', 35),
(25, 1, 1, 'Structure_Redirect', 'y', 'webpage', '{exp:structure:first_child_redirect}', '', 1374240837, 1, 'n', 0, '', 'n', 'n', 'o', 27);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`),
  KEY `group_name_idx` (`group_name`),
  KEY `group_order_idx` (`group_order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_template_groups`
--

INSERT INTO `exp_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`) VALUES
(1, 1, 'Site', 1, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_throttle`
--

CREATE TABLE IF NOT EXISTS `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_upload_no_access`
--

INSERT INTO `exp_upload_no_access` (`upload_id`, `upload_loc`, `member_group`) VALUES
(1, 'cp', 6);

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_prefs`
--

CREATE TABLE IF NOT EXISTS `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_upload_prefs`
--

INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`) VALUES
(1, 1, 'Content', '/Users/martinsmith/Dropbox/localserver/yearsley/images/uploads/content/', 'http://yearsleymay:8888/images/uploads/content/', 'all', '', '', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_wygwam_configs`
--

CREATE TABLE IF NOT EXISTS `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_wygwam_configs`
--

INSERT INTO `exp_wygwam_configs` (`config_id`, `config_name`, `settings`) VALUES
(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9'),
(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ=='),
(3, 'Extended', 'YTo2OntzOjc6InRvb2xiYXIiO2E6MTE6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6NToiSW1hZ2UiO2k6NjtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NztzOjEyOiJCdWxsZXRlZExpc3QiO2k6ODtzOjQ6IkxpbmsiO2k6OTtzOjY6IlVubGluayI7aToxMDtzOjY6IlNvdXJjZSI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MToiMSI7czoxMjoiZWRpdGluZ0Jsb2NrIjtzOjE6InkiO30='),
(4, 'Optional Side Panel Content', 'YTo2OntzOjc6InRvb2xiYXIiO2E6Njp7aTowO3M6NjoiRm9ybWF0IjtpOjE7czo0OiJCb2xkIjtpOjI7czo2OiJJdGFsaWMiO2k6MztzOjU6IkltYWdlIjtpOjQ7czo0OiJMaW5rIjtpOjU7czo2OiJTb3VyY2UiO31zOjY6ImhlaWdodCI7czoyOiI2MCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToibiI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjtzOjEyOiJlZGl0aW5nQmxvY2siO3M6MToieSI7fQ==');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
