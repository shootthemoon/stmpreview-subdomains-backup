-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2014 at 11:49 AM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stmwebsite_`
--

-- --------------------------------------------------------

--
-- Table structure for table `exp_accessories`
--

CREATE TABLE IF NOT EXISTS `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_accessories`
--

INSERT INTO `exp_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`) VALUES
(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'),
(3, 'Ep_better_workflow_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.4'),
(4, 'Structure_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '3.3.6');

-- --------------------------------------------------------

--
-- Table structure for table `exp_actions`
--

CREATE TABLE IF NOT EXISTS `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `exp_actions`
--

INSERT INTO `exp_actions` (`action_id`, `class`, `method`) VALUES
(1, 'Comment', 'insert_new_comment'),
(2, 'Comment_mcp', 'delete_comment_notification'),
(3, 'Comment', 'comment_subscribe'),
(4, 'Comment', 'edit_comment'),
(5, 'Email', 'send_email'),
(6, 'Safecracker', 'submit_entry'),
(7, 'Safecracker', 'combo_loader'),
(8, 'Search', 'do_search'),
(9, 'Channel', 'insert_new_entry'),
(10, 'Channel', 'filemanager_endpoint'),
(11, 'Channel', 'smiley_pop'),
(12, 'Member', 'registration_form'),
(13, 'Member', 'register_member'),
(14, 'Member', 'activate_member'),
(15, 'Member', 'member_login'),
(16, 'Member', 'member_logout'),
(17, 'Member', 'retrieve_password'),
(18, 'Member', 'reset_password'),
(19, 'Member', 'send_member_email'),
(20, 'Member', 'update_un_pw'),
(21, 'Member', 'member_search'),
(22, 'Member', 'member_delete'),
(23, 'Rte', 'get_js'),
(25, 'Assets_mcp', 'get_subfolders'),
(26, 'Assets_mcp', 'upload_file'),
(27, 'Assets_mcp', 'get_files_view_by_folders'),
(28, 'Assets_mcp', 'get_props'),
(29, 'Assets_mcp', 'save_props'),
(30, 'Assets_mcp', 'get_ordered_files_view'),
(31, 'Assets_mcp', 'move_folder'),
(32, 'Assets_mcp', 'create_folder'),
(33, 'Assets_mcp', 'delete_folder'),
(34, 'Assets_mcp', 'view_file'),
(35, 'Assets_mcp', 'move_file'),
(36, 'Assets_mcp', 'delete_file'),
(37, 'Assets_mcp', 'build_sheet'),
(38, 'Assets_mcp', 'get_selected_files'),
(39, 'Libraree', 'save_to_file'),
(40, 'Playa_mcp', 'filter_entries'),
(41, 'Updater', 'ACT_general_router'),
(42, 'Zoo_flexible_admin', 'ajax_preview'),
(43, 'Zoo_flexible_admin', 'ajax_load_tree'),
(44, 'Zoo_flexible_admin', 'ajax_load_settings'),
(45, 'Zoo_flexible_admin', 'ajax_save_tree'),
(46, 'Zoo_flexible_admin', 'ajax_remove_tree'),
(47, 'Zoo_flexible_admin', 'ajax_copy_tree'),
(48, 'Structure', 'ajax_move_set_data'),
(49, 'Freeform', 'save_form'),
(50, 'Backup_proish', 'cron');

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets`
--

CREATE TABLE IF NOT EXISTS `exp_assets` (
  `asset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` int(10) unsigned DEFAULT NULL,
  `alt_text` tinytext,
  `caption` tinytext,
  `author` tinytext,
  `desc` text,
  `location` tinytext,
  `keywords` text,
  PRIMARY KEY (`asset_id`),
  UNIQUE KEY `unq_file_path` (`file_path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=320 ;

--
-- Dumping data for table `exp_assets`
--

INSERT INTO `exp_assets` (`asset_id`, `file_path`, `title`, `date`, `alt_text`, `caption`, `author`, `desc`, `location`, `keywords`) VALUES
(1, '{filedir_7}photography/ss_sample_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '{filedir_7}photography/ss_sample_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '{filedir_7}photography/ss_sample_3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '{filedir_1}food/sample4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '{filedir_1}food/sample1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '{filedir_1}product/sample2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '{filedir_1}location/sample9.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '{filedir_1}food/sample5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '{filedir_1}product/sample3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '{filedir_1}location/sample7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '{filedir_1}food/sample6.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '{filedir_1}food/sample8.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '{filedir_7}1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '{filedir_7}2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '{filedir_7}3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '{filedir_2}stm-brand-portfolio-2012-10.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '{filedir_2}stm-brand-portfolio-2012-11.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '{filedir_2}stm-brand-portfolio-2012-20.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '{filedir_2}stm-brand-portfolio-2012-16.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '{filedir_2}stm-brand-portfolio-2012-25.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '{filedir_2}stm-brand-portfolio-2012-4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '{filedir_2}stm-brand-portfolio-2012-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '{filedir_2}stm-brand-portfolio-2012-28.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '{filedir_7}creative/stm-brand-portfolio-2012-26.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '{filedir_7}creative/stm-brand-portfolio-2012-27.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '{filedir_7}creative/stm-brand-portfolio-2012-28.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, '{filedir_2}stm-brand-portfolio-2012-30.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, '{filedir_2}stm-brand-portfolio-2012-33.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, '{filedir_2}stm-brand-portfolio-2012-32.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '{filedir_8}DSC_0222.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '{filedir_8}DSC_0226.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '{filedir_8}DSC_0283.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, '{filedir_4}Original_Squash_«_Jucee.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, '{filedir_3}Alpha---Special-Offers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, '{filedir_7}digital/Untitled-1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, '{filedir_7}digital/Untitled-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, '{filedir_7}digital/Untitled-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '{filedir_7}digital/Untitled-1_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '{filedir_7}digital/ident.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, '{filedir_3}devilsishly.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '{filedir_3}Macphie_Development.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, '{filedir_7}photography/ph1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, '{filedir_7}photography/ph10.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, '{filedir_7}photography/ph11.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, '{filedir_7}photography/ph12.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, '{filedir_7}photography/ph13.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, '{filedir_7}photography/ph14.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '{filedir_7}photography/ph15.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, '{filedir_7}photography/ph16.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, '{filedir_7}photography/ph17.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, '{filedir_7}photography/ph18.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, '{filedir_7}photography/ph2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, '{filedir_7}photography/ph3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, '{filedir_7}photography/ph4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, '{filedir_7}photography/ph5.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, '{filedir_7}photography/ph6.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, '{filedir_7}photography/ph7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, '{filedir_7}photography/ph8.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, '{filedir_1}food/ph2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, '{filedir_1}food/ph12.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, '{filedir_1}food/ph4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, '{filedir_1}product/ph13.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, '{filedir_1}product/ph15.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, '{filedir_1}product/ph16.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, '{filedir_1}product/ph14.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, '{filedir_7}studio/olly.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, '{filedir_7}studio/darren.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, '{filedir_1}food/40-coconut-seafood-soup.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, '{filedir_1}food/50-thai-green-curry-1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, '{filedir_1}food/52-hot-thick-red-curry.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '{filedir_1}food/60-stir-fry-garlic-and-pepper.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, '{filedir_1}food/67-three-flavour-sauce-with-duck.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, '{filedir_1}food/85-red-curry-lobster-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, '{filedir_1}food/_15-steamed-mussels.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, '{filedir_1}food/carrs-pastie.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, '{filedir_1}food/chicken-teryaki-best.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, '{filedir_1}food/chickpea,aubergine&butternut-squash-pasta.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, '{filedir_1}food/ColdWaterPrawnsF1307.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, '{filedir_1}food/corned-beef-pie-3-RTsteam.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, '{filedir_1}food/DSC_5065.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, '{filedir_1}food/fish-shop-best.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, '{filedir_1}food/fruit-tart-on-white.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, '{filedir_1}food/herby-chicken.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, '{filedir_1}food/IMG_6015.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, '{filedir_1}food/Napolina-pasta-recipe.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, '{filedir_1}food/nic-test-10.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, '{filedir_1}food/nic-test-14.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, '{filedir_1}food/nic-test-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, '{filedir_1}food/nic-test-4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, '{filedir_1}food/nic-test-7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, '{filedir_1}food/nic-test-one.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, '{filedir_1}product/black-face-watch.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, '{filedir_1}product/Dita-and-Gaga-4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, '{filedir_1}product/DKNY-golden-RT.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, '{filedir_1}product/IlluminateBowlStack.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, '{filedir_1}product/Illuminated3Bowls.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, '{filedir_1}product/IlluminatePlaceSetting.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, '{filedir_1}product/Job_0047.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, '{filedir_1}product/Marc-Jacobs-Background.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, '{filedir_1}product/milk-chocolate.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, '{filedir_1}product/Pages-20-21-Background.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, '{filedir_1}product/Pierre-Cardin.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, '{filedir_1}product/Pierre-fasion.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, '{filedir_1}product/Spicebomb-Page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, '{filedir_1}product/ted-Baker-Window.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, '{filedir_1}product/YSL-Shocking.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, '{filedir_1}people/_K3D3129.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, '{filedir_1}people/_K3D3151.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, '{filedir_1}people/_K3D3156.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, '{filedir_1}people/_K3D3472.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, '{filedir_1}people/_K3D3587.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, '{filedir_1}people/Cardin-ad.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, '{filedir_1}people/Pad-Thai-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, '{filedir_1}people/PCX0510L05---MODEL-C-(Cynthia)2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, '{filedir_1}people/PXX0026W---MODEL-B-(Bella)2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, '{filedir_1}people/Sweet-Mandarin4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, '{filedir_1}location/_K3D1712.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, '{filedir_1}location/chef-cooking-front-cover.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, '{filedir_1}location/DSC_0324.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, '{filedir_1}location/DSC_0407.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, '{filedir_1}location/Page1Movement.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, '{filedir_1}location/Shot2Restaurant3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, '{filedir_1}location/Shot3ProductBenefits.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, '{filedir_1}location/Shot4Equation3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, '{filedir_1}location/Shot6Med3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, '{filedir_1}location/Shot7Pattern2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, '{filedir_1}location/various-17a.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, '{filedir_8}studio_3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, '{filedir_8}DSC_0220.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, '{filedir_8}DSC_0224.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, '{filedir_8}DSC_0228.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, '{filedir_8}DSC_0230.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, '{filedir_8}DSC_0279.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, '{filedir_8}DSC_6820.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, '{filedir_8}photography-studio.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, '{filedir_8}studio_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, '{filedir_8}studio_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, '{filedir_2}Brand/country-style-1-tezzer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, '{filedir_2}Brand/Harbour-Smokehouse.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, '{filedir_7}creative/country-style-1-tezzer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, '{filedir_7}creative/Country-style-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, '{filedir_7}creative/Harbour-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, '{filedir_7}creative/Highbury-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, '{filedir_7}creative/Montana-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, '{filedir_7}creative/Trophy-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, '{filedir_2}Brand/Hewbys.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, '{filedir_2}Brand/Montana.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, '{filedir_2}Brand/Nuba.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, '{filedir_2}Brand/Scorpio-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, '{filedir_2}Brand/Swizzles-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, '{filedir_2}Brand/Trophy-logo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, '{filedir_2}dm/3-peaks-tezzer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, '{filedir_2}dm/3-peaks.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, '{filedir_2}dm/Aggreko-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, '{filedir_2}dm/aggreko.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, '{filedir_2}dm/Dissoto.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, '{filedir_2}dm/josef-meirs.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, '{filedir_2}dm/Lakeland-1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, '{filedir_2}dm/Thompson-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, '{filedir_2}dm/Thomson-1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(164, '{filedir_7}creative/3-peaks-tezzer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(165, '{filedir_7}creative/Aggreko-Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(166, '{filedir_7}creative/Josef-Meirs-Teezer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, '{filedir_7}creative/Lakeland-Teezer1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, '{filedir_7}creative/Lakeland-Teezer2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, '{filedir_7}creative/Goodness-me-Tezzer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(170, '{filedir_7}creative/Humdingers-teezer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(171, '{filedir_7}creative/Montana-teezer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(172, '{filedir_7}creative/Teezer-temp.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(173, '{filedir_7}creative/Vimto-tezzer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(174, '{filedir_2}packaging/Advanced-nut-dog.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(175, '{filedir_2}packaging/count-down.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(176, '{filedir_2}packaging/Fish-mongers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(177, '{filedir_2}packaging/Humdingers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(178, '{filedir_2}packaging/John-west.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(179, '{filedir_2}packaging/jucee.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(180, '{filedir_2}packaging/koko-noir.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(181, '{filedir_2}packaging/Kuli.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(182, '{filedir_2}packaging/laziz.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, '{filedir_2}packaging/meadowvale.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(184, '{filedir_2}packaging/Molpol.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(185, '{filedir_2}packaging/Montana-chun.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, '{filedir_2}packaging/Montana.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(187, '{filedir_2}packaging/Morrisons.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(188, '{filedir_2}packaging/Vimto.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(189, '{filedir_2}store-comms/Advanced-nutrition.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, '{filedir_2}store-comms/Bird.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(191, '{filedir_2}store-comms/Dickies.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(192, '{filedir_2}store-comms/Distribution.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(193, '{filedir_2}store-comms/Pet-club.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(194, '{filedir_2}store-comms/Ryman-Furniture-Range-Brochure.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(195, '{filedir_2}store-comms/Ryman-Furniture-Range-POS.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, '{filedir_2}store-comms/Ryman-Store-Graphics.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, '{filedir_2}store-comms/store-open.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(198, '{filedir_7}studio/Camera-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(199, '{filedir_7}studio/DSC_0216.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(200, '{filedir_7}studio/DSC_0220.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(201, '{filedir_7}studio/DSC_0224.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(202, '{filedir_7}studio/DSC_0228.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(203, '{filedir_7}studio/DSC_0230.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(204, '{filedir_7}studio/DSC_0279.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(205, '{filedir_7}studio/DSC_6820.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(206, '{filedir_7}studio/photography-studio.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(207, '{filedir_7}studio/studio_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(208, '{filedir_7}studio/studio_2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(209, '{filedir_7}studio/studio_3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(210, '{filedir_7}studio/DSC_0283.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(211, '{filedir_1}hot-thick-red-curry.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(212, '{filedir_1}orchid-banquet.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(213, '{filedir_1}oysters.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(214, '{filedir_1}pannacotta.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(215, '{filedir_1}pea-soup.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(216, '{filedir_1}reduced-fat-custard.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(217, '{filedir_1}rice-wraps.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(218, '{filedir_1}runny-cheese.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(219, '{filedir_1}salad-red.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(220, '{filedir_1}three-flavour-sauce-with-duck.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(221, '{filedir_1}shippams-RS.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(222, '{filedir_1}steak-and-pomigranate.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(223, '{filedir_1}Spicebomb-Page-NEW.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(224, '{filedir_1}white-aviator-watch.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(225, '{filedir_1}Winter-2012-Ed-2-Front-Cover-1-v2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(226, '{filedir_1}YSL-Shocking.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(227, '{filedir_1}hoover.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(228, '{filedir_1}red-kettel.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(229, '{filedir_1}K3D3256.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(230, '{filedir_1}MG0936..jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(231, '{filedir_1}IMG_6086-RT.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(232, '{filedir_1}IMG_6189-RT.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(234, '{filedir_1}personal--49.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(235, '{filedir_1}personal-copy.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(238, '{filedir_1}_Almond-Extract.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(239, '{filedir_1}authentic-cocktail-on-the-beach-.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(240, '{filedir_1}IMG_6012-RT.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(241, '{filedir_2}store-comms/Pick-n-Mix-POS.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(242, '{filedir_2}packaging/Marriages.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, '{filedir_2}packaging/HarbourSmokehouse-HighlandPark.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(244, '{filedir_2}packaging/Devilishly_Delicious_300ml.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, '{filedir_2}packaging/Disotto_NY_Deli_Tubs.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, '{filedir_2}packaging/pinkpanther.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, '{filedir_2}dm/Lakeland_Win12_Option_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, '{filedir_2}dm/Lakeland_Win12_Option_2v1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(249, '{filedir_2}dm/Lakeland_Win12_Option_2v2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(250, '{filedir_2}dm/Thomson_Win12_Option_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, '{filedir_2}dm/Thomson_Win12_Option_2v1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, '{filedir_2}dm/Thomson_Win12_Option_2v2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, '{filedir_2}Brand/Kids_Holiday_Pet_Club_Feb.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(254, '{filedir_2}Brand/Kids_Holiday_Pet_Club_Summer.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(255, '{filedir_2}Brand/Vero_Gelato.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(256, '{filedir_2}Brand/Poundbakery.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(257, '{filedir_2}Brand/Sayers.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(258, '{filedir_2}Brand/Goodness_Me.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(259, '{filedir_1}product/spain-shirt.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(260, '{filedir_3}active-procurement.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(261, '{filedir_3}caterforce.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(262, '{filedir_3}chefs-selections.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(263, '{filedir_3}devilish-home.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(264, '{filedir_3}devilish-facebook.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(265, '{filedir_3}macphie-homepage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(266, '{filedir_3}marriages.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(267, '{filedir_3}meadowvale.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(268, '{filedir_3}jucee-page.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(269, '{filedir_3}sayers-screen.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(270, '{filedir_3}scorpio-screen.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(271, '{filedir_3}Yearsley-Screen.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, '{filedir_7}digital/active-procurement.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, '{filedir_7}digital/chefs-selections.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(274, '{filedir_7}digital/delicious.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(275, '{filedir_7}digital/devilish-facebook.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(276, '{filedir_7}digital/jucee-eshot.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(277, '{filedir_7}digital/macphie.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(278, '{filedir_7}digital/marriages.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(279, '{filedir_7}digital/meadowvale-ecrm.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(280, '{filedir_7}digital/meadowvale-inspired.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(281, '{filedir_7}digital/meadowvale.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(282, '{filedir_7}digital/pet-club.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(283, '{filedir_7}digital/scorpio.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(284, '{filedir_7}digital/theo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(285, '{filedir_3}bat-and-ball.m4v', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(286, '{filedir_3}bat-and-ball.webm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(287, '{filedir_3}bat-and-ball.ogv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(288, '{filedir_3}bat-and-ball.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, '{filedir_3}nets.m4v', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(290, '{filedir_3}nets.webm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, '{filedir_3}nets.ogv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, '{filedir_3}nets.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(293, '{filedir_3}Jucee_Umbrella.m4v', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(294, '{filedir_3}Jucee_Umbrella.webm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(295, '{filedir_3}Jucee_Umbrella.ogv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(296, '{filedir_3}Jucee_Umbrella.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, '{filedir_3}two_meadowvale.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(298, '{filedir_3}tcx-combined.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, '{filedir_3}pets.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, '{filedir_3}jucee.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, '{filedir_3}pah-banners.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(302, '{filedir_3}ryman.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, '{filedir_3}pound-bakery.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(304, '{filedir_3}mixed-banners.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(305, '{filedir_2}dm/store-open.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, '{filedir_2}dm/Back_to_School_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(307, '{filedir_2}dm/Ride_Away_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(308, '{filedir_2}dm/Thomson_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, '{filedir_2}store-comms/Ryman_1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, '{filedir_7}1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, '{filedir_7}2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(312, '{filedir_7}3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(313, '{filedir_7}4.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(314, '{filedir_7}5.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(315, '{filedir_7}6.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, '{filedir_7}7.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(317, '{filedir_7}8.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(318, '{filedir_7}9.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, '{filedir_7}10.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_entries`
--

CREATE TABLE IF NOT EXISTS `exp_assets_entries` (
  `asset_id` int(10) unsigned DEFAULT NULL,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `col_id` int(6) unsigned DEFAULT NULL,
  `row_id` int(10) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `asset_order` int(4) unsigned DEFAULT NULL,
  KEY `asset_id` (`asset_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `col_id` (`col_id`),
  KEY `row_id` (`row_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_assets_entries`
--

INSERT INTO `exp_assets_entries` (`asset_id`, `entry_id`, `field_id`, `col_id`, `row_id`, `var_id`, `asset_order`) VALUES
(20, 37, 2, 1, 101, NULL, 0),
(32, 46, 6, 10, 117, NULL, 0),
(131, 45, 6, 10, 116, NULL, 0),
(132, 44, 6, 10, 115, NULL, 0),
(133, 105, 6, 10, 241, NULL, 0),
(134, 106, 6, 10, 242, NULL, 0),
(135, 107, 6, 10, 243, NULL, 0),
(136, 108, 6, 10, 244, NULL, 0),
(137, 109, 6, 10, 245, NULL, 0),
(138, 110, 6, 10, 246, NULL, 0),
(139, 111, 6, 10, 247, NULL, 0),
(140, 112, 6, 10, 248, NULL, 0),
(131, 113, 6, 10, 249, NULL, 0),
(151, 115, 2, 1, 252, NULL, 0),
(154, 118, 2, 1, 258, NULL, 0),
(150, 114, 2, 1, 250, NULL, 0),
(149, 38, 2, 1, 103, NULL, 0),
(142, 39, 2, 1, 105, NULL, 0),
(188, 134, 2, 1, 290, NULL, 0),
(187, 133, 2, 1, 288, NULL, 0),
(185, 131, 2, 1, 284, NULL, 0),
(184, 130, 2, 1, 282, NULL, 0),
(186, 132, 2, 1, 286, NULL, 0),
(182, 128, 2, 1, 278, NULL, 0),
(181, 127, 2, 1, 276, NULL, 0),
(183, 129, 2, 1, 280, NULL, 0),
(179, 125, 2, 1, 272, NULL, 0),
(178, 124, 2, 1, 270, NULL, 0),
(180, 126, 2, 1, 274, NULL, 0),
(175, 35, 2, 1, 97, NULL, 0),
(174, 36, 2, 1, 99, NULL, 0),
(176, 33, 2, 1, 93, NULL, 0),
(177, 34, 2, 1, 95, NULL, 0),
(159, 40, 2, 1, 107, NULL, 0),
(160, 120, 2, 1, 262, NULL, 0),
(196, 140, 2, 1, 302, NULL, 0),
(193, 137, 2, 1, 297, NULL, 0),
(195, 139, 2, 1, 300, NULL, 0),
(191, 135, 2, 1, 292, NULL, 0),
(189, 43, 2, 1, 113, NULL, 0),
(190, 42, 2, 1, 111, NULL, 0),
(194, 138, 2, 1, 298, NULL, 0),
(211, 142, 2, 1, 306, NULL, 0),
(212, 143, 2, 1, 308, NULL, 0),
(213, 144, 2, 1, 310, NULL, 0),
(214, 145, 2, 1, 312, NULL, 0),
(216, 147, 2, 1, 316, NULL, 0),
(217, 148, 2, 1, 318, NULL, 0),
(218, 149, 2, 1, 320, NULL, 0),
(219, 150, 2, 1, 322, NULL, 0),
(220, 151, 2, 1, 324, NULL, 0),
(221, 152, 2, 1, 326, NULL, 0),
(222, 153, 2, 1, 328, NULL, 0),
(223, 154, 2, 1, 330, NULL, 0),
(224, 155, 2, 1, 332, NULL, 0),
(225, 156, 2, 1, 334, NULL, 0),
(226, 157, 2, 1, 336, NULL, 0),
(227, 158, 2, 1, 338, NULL, 0),
(228, 159, 2, 1, 340, NULL, 0),
(229, 160, 2, 1, 342, NULL, 0),
(230, 161, 2, 1, 344, NULL, 0),
(231, 162, 2, 1, 346, NULL, 0),
(232, 163, 2, 1, 348, NULL, 0),
(234, 165, 2, 1, 352, NULL, 0),
(235, 166, 2, 1, 354, NULL, 0),
(238, 168, 2, 1, 358, NULL, 0),
(239, 167, 2, 1, 356, NULL, 0),
(240, 164, 2, 1, 351, NULL, 0),
(241, 169, 2, 1, 360, NULL, 0),
(242, 170, 2, 1, 362, NULL, 0),
(243, 171, 2, 1, 364, NULL, 0),
(244, 172, 2, 1, 366, NULL, 0),
(245, 173, 2, 1, 368, NULL, 0),
(246, 174, 2, 1, 370, NULL, 0),
(161, 175, 2, 1, 372, NULL, 0),
(249, 178, 2, 1, 378, NULL, 0),
(162, 179, 2, 1, 380, NULL, 0),
(163, 180, 2, 1, 382, NULL, 0),
(250, 181, 2, 1, 384, NULL, 0),
(253, 184, 2, 1, 390, NULL, 0),
(254, 185, 2, 1, 392, NULL, 0),
(255, 186, 2, 1, 394, NULL, 0),
(256, 187, 2, 1, 396, NULL, 0),
(258, 190, 2, 1, 401, NULL, 0),
(259, 191, 2, 1, 403, NULL, 0),
(156, 41, 2, 1, 109, NULL, 0),
(152, 116, 2, 1, 254, NULL, 0),
(257, 189, 2, 1, 399, NULL, 0),
(260, 50, 2, 1, 131, NULL, 0),
(261, 192, 2, 1, 413, NULL, 0),
(262, 193, 2, 1, 415, NULL, 0),
(263, 49, 2, 1, 129, NULL, 0),
(264, 194, 2, 1, 417, NULL, 0),
(266, 196, 2, 1, 421, NULL, 0),
(267, 197, 2, 1, 423, NULL, 0),
(268, 198, 2, 1, 425, NULL, 0),
(269, 199, 2, 1, 427, NULL, 0),
(270, 200, 2, 1, 429, NULL, 0),
(285, 204, 9, 16, 435, NULL, 0),
(286, 204, 9, 17, 435, NULL, 0),
(287, 204, 9, 18, 435, NULL, 0),
(288, 204, 9, 19, 435, NULL, 0),
(289, 205, 9, 16, 436, NULL, 0),
(290, 205, 9, 17, 436, NULL, 0),
(291, 205, 9, 18, 436, NULL, 0),
(292, 205, 9, 19, 436, NULL, 0),
(293, 206, 9, 16, 437, NULL, 0),
(294, 206, 9, 17, 437, NULL, 0),
(295, 206, 9, 18, 437, NULL, 0),
(296, 206, 9, 19, 437, NULL, 0),
(297, 207, 2, 1, 438, NULL, 0),
(298, 208, 2, 1, 440, NULL, 0),
(299, 209, 2, 1, 442, NULL, 0),
(301, 212, 2, 1, 447, NULL, 0),
(303, 214, 2, 1, 451, NULL, 0),
(302, 213, 2, 1, 449, NULL, 0),
(304, 215, 2, 1, 453, NULL, 0),
(265, 195, 2, 1, 419, NULL, 0),
(305, 141, 2, 1, 304, NULL, 0),
(238, 220, 2, 1, 459, NULL, 0),
(306, 221, 2, 1, 461, NULL, 0),
(307, 222, 2, 1, 463, NULL, 0),
(308, 223, 2, 1, 465, NULL, 0),
(309, 224, 2, 1, 467, NULL, 0),
(143, 2, 1, NULL, NULL, NULL, 0),
(144, 2, 1, NULL, NULL, NULL, 1),
(145, 2, 1, NULL, NULL, NULL, 2),
(146, 2, 1, NULL, NULL, NULL, 3),
(147, 2, 1, NULL, NULL, NULL, 4),
(24, 2, 1, NULL, NULL, NULL, 5),
(25, 2, 1, NULL, NULL, NULL, 6),
(26, 2, 1, NULL, NULL, NULL, 7),
(148, 2, 1, NULL, NULL, NULL, 8),
(164, 2, 1, NULL, NULL, NULL, 9),
(165, 2, 1, NULL, NULL, NULL, 10),
(166, 2, 1, NULL, NULL, NULL, 11),
(167, 2, 1, NULL, NULL, NULL, 12),
(168, 2, 1, NULL, NULL, NULL, 13),
(169, 2, 1, NULL, NULL, NULL, 14),
(170, 2, 1, NULL, NULL, NULL, 15),
(171, 2, 1, NULL, NULL, NULL, 16),
(172, 2, 1, NULL, NULL, NULL, 17),
(272, 3, 1, NULL, NULL, NULL, 0),
(273, 3, 1, NULL, NULL, NULL, 1),
(274, 3, 1, NULL, NULL, NULL, 2),
(275, 3, 1, NULL, NULL, NULL, 3),
(276, 3, 1, NULL, NULL, NULL, 4),
(277, 3, 1, NULL, NULL, NULL, 5),
(278, 3, 1, NULL, NULL, NULL, 6),
(279, 3, 1, NULL, NULL, NULL, 7),
(281, 3, 1, NULL, NULL, NULL, 8),
(282, 3, 1, NULL, NULL, NULL, 9),
(283, 3, 1, NULL, NULL, NULL, 10),
(13, 5, 1, NULL, NULL, NULL, 0),
(198, 218, 1, NULL, NULL, NULL, 0),
(70, 218, 1, NULL, NULL, NULL, 1),
(199, 218, 1, NULL, NULL, NULL, 2),
(200, 218, 1, NULL, NULL, NULL, 3),
(13, 4, 1, NULL, NULL, NULL, 0),
(14, 4, 1, NULL, NULL, NULL, 1),
(2, 4, 1, NULL, NULL, NULL, 2),
(1, 4, 1, NULL, NULL, NULL, 3),
(3, 4, 1, NULL, NULL, NULL, 4),
(45, 4, 1, NULL, NULL, NULL, 5),
(46, 4, 1, NULL, NULL, NULL, 6),
(47, 4, 1, NULL, NULL, NULL, 7),
(48, 4, 1, NULL, NULL, NULL, 8),
(49, 4, 1, NULL, NULL, NULL, 9),
(50, 4, 1, NULL, NULL, NULL, 10),
(51, 4, 1, NULL, NULL, NULL, 11),
(52, 4, 1, NULL, NULL, NULL, 12),
(53, 4, 1, NULL, NULL, NULL, 13),
(54, 4, 1, NULL, NULL, NULL, 14),
(55, 4, 1, NULL, NULL, NULL, 15),
(56, 4, 1, NULL, NULL, NULL, 16),
(57, 4, 1, NULL, NULL, NULL, 17),
(58, 4, 1, NULL, NULL, NULL, 18),
(59, 4, 1, NULL, NULL, NULL, 19),
(60, 4, 1, NULL, NULL, NULL, 20),
(61, 4, 1, NULL, NULL, NULL, 21),
(310, 1, 1, NULL, NULL, NULL, 0),
(311, 1, 1, NULL, NULL, NULL, 1),
(312, 1, 1, NULL, NULL, NULL, 2),
(313, 1, 1, NULL, NULL, NULL, 3),
(314, 1, 1, NULL, NULL, NULL, 4),
(315, 1, 1, NULL, NULL, NULL, 5),
(316, 1, 1, NULL, NULL, NULL, 6),
(317, 1, 1, NULL, NULL, NULL, 7),
(318, 1, 1, NULL, NULL, NULL, 8),
(319, 1, 1, NULL, NULL, NULL, 9);

-- --------------------------------------------------------

--
-- Table structure for table `exp_backup_proish_settings`
--

CREATE TABLE IF NOT EXISTS `exp_backup_proish_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(30) NOT NULL DEFAULT '',
  `setting_value` text NOT NULL,
  `serialized` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_backup_proish_settings`
--

INSERT INTO `exp_backup_proish_settings` (`id`, `setting_key`, `setting_value`, `serialized`) VALUES
(1, 'backup_file_location', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/', 0),
(2, 'backup_store_location', '/var/www/vhosts/shoot-the-moon.co.uk/httpdocs/backups/', 0),
(3, 'auto_threshold', '0', 0),
(4, 'exclude_paths', 'a:1:{i:0;s:0:"";}', 1),
(5, 'cron_notify_emails', 'a:0:{}', 1),
(6, 'cron_attach_backups', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_captcha`
--

CREATE TABLE IF NOT EXISTS `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_categories`
--

CREATE TABLE IF NOT EXISTS `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_fields`
--

CREATE TABLE IF NOT EXISTS `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_field_data`
--

CREATE TABLE IF NOT EXISTS `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_groups`
--

CREATE TABLE IF NOT EXISTS `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_posts`
--

CREATE TABLE IF NOT EXISTS `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channels`
--

CREATE TABLE IF NOT EXISTS `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `exp_channels`
--

INSERT INTO `exp_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`) VALUES
(1, 1, 'landing_page', 'Landing Page', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 14, 0, 1357921715, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(2, 1, 'creative', 'Creative', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 0, 0, 0, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(3, 1, 'digital', 'Digital', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 2, 0, 1351250272, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(4, 1, 'photography', 'Photography', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 1, 0, 1360677707, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(5, 1, 'the_studio', 'The Studio', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 0, 0, 0, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(6, 1, 'homepage', 'Homepage', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 2, 0, 1360753898, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(7, 1, 'food', 'food', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 11, 0, 1353413584, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(8, 1, 'product', 'Product', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 9, 0, 1348747984, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(9, 1, 'location', 'Location', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 2, 0, 1321272784, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(10, 1, 'personal', 'Personal', 'http://stm.stmpreview.co.uk//index.php', '', 'en', 5, 0, 1343218384, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(11, 1, 'testimonials', 'Testimonials', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 1, 0, 1351521484, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(12, 1, 'packaging', 'packaging', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 20, 0, 1357314424, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(14, 1, 'store-comms', 'Store Comms', 'http://stm.stmpreview.co.uk//index.php', '', 'en', 6, 0, 1379947001, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(15, 1, 'branding', 'Branding', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 12, 0, 1346760784, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(16, 1, 'direct-mail', 'Direct Mail', 'http://stm.stmpreview.co.uk//index.php', '', 'en', 12, 0, 1379947005, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(17, 1, 'studio', 'Studio', 'http://stm.stmpreview.co.uk//', '', 'en', 12, 0, 1356277204, 0, '', 2, 'open', 1, NULL, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(18, 1, 'mobile', 'Mobile', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 0, 0, 0, 0, '', 2, 'open', 1, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(19, 1, 'ecrm', 'eCRM', 'http://stm.stmpreview.co.uk//index.php', '', 'en', 4, 0, 1357920473, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(20, 1, 'websites', 'Websites', 'http://stm.stmpreview.co.uk//index.php', '', 'en', 12, 0, 1358242912, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(21, 1, 'banners', 'Banners', 'http://stm.stmpreview.co.uk//index.php', '', 'en', 2, 0, 1357921771, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(22, 1, 'motion', 'Motion', 'http://stm.stmpreview.co.uk//index.php', NULL, 'en', 4, 0, 1357916352, 0, '', 2, 'open', 2, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(23, 1, 'facilities', 'Facilities', 'http://stm.stmpreview.co.uk//index.php', '', 'en', 0, 0, 0, 0, '', 2, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_data`
--

CREATE TABLE IF NOT EXISTS `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` text,
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_data`
--

INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_2`, `field_ft_2`, `field_id_3`, `field_ft_3`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_10`, `field_ft_10`, `field_id_15`, `field_ft_15`, `field_id_16`, `field_ft_16`, `field_id_17`, `field_ft_17`, `field_id_18`, `field_ft_18`) VALUES
(1, 1, 6, '{filedir_7}1.png\n{filedir_7}2.png\n{filedir_7}3.png\n{filedir_7}4.png\n{filedir_7}5.png\n{filedir_7}6.png\n{filedir_7}7.png\n{filedir_7}8.png\n{filedir_7}9.png\n{filedir_7}10.png', 'none', '', 'none', '<p>\n	Shoot the Moon is a collective of experienced creatives, illustrators, developers and photographers with a large chunk of retail, marketing and technical experience.</p>\n<p>\n	We work with a whole host of clients from small fledgling businesses right up to some of the bigger household brands.</p>\n<p>\n	Jobs vary from packaging and branding to product photography, POS &amp; literature, both online and offline. Have a look through the portfolios and Show Reels; we&#39;ve included a range of recent work. If you would like to meet up or drop in for a coffee, give us a call or send an e-mail and we&#39;ll be in touch.</p>', 'none', '1', 'none', '', 'none', '<p>\n	Flexible, straight talking, and offering heaps of effective ideas &ndash; placing understanding, quality, and consistency above all else.</p>', 'none', '', 'none', '', 'none', 'Creative Agency Manchester - Shoot The Moon', 'none', 'Shoot the Moon are a experienced,  creative agency based in Manchester specialising in packaging and retail communications,  photography and digital marketing', 'none', 'Daily', 'none', '0.5', 'none'),
(2, 1, 1, '{filedir_7}creative/stm-brand-portfolio-2012-26.jpg\n{filedir_7}creative/stm-brand-portfolio-2012-27.jpg\n{filedir_7}creative/stm-brand-portfolio-2012-28.jpg\n{filedir_7}creative/country-style-1-tezzer-temp.jpg\n{filedir_7}creative/Country-style-Teezer-temp.jpg\n{filedir_7}creative/Harbour-Teezer-temp.jpg\n{filedir_7}creative/Highbury-Teezer-temp.jpg\n{filedir_7}creative/Montana-Teezer-temp.jpg\n{filedir_7}creative/Trophy-Teezer-temp.jpg\n{filedir_7}creative/3-peaks-tezzer.jpg\n{filedir_7}creative/Aggreko-Teezer-temp.jpg\n{filedir_7}creative/Josef-Meirs-Teezer.jpg\n{filedir_7}creative/Lakeland-Teezer1.jpg\n{filedir_7}creative/Lakeland-Teezer2.jpg\n{filedir_7}creative/Goodness-me-Tezzer.jpg\n{filedir_7}creative/Humdingers-teezer.jpg\n{filedir_7}creative/Montana-teezer.jpg\n{filedir_7}creative/Teezer-temp.jpg', 'none', '', 'none', '<p>\n	Whether you&#39;re looking for fresh new creative, inspiring images or you&#39;re simply curious about what a new perspective on your existing creative could bring, we&#39;re sure we can help you find what you&#39;re looking for.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Branding, Packaging, Direct Mail & Creative From Shoot The Moon - Manchester', 'none', 'Imaginative thinking, creative flair & exacting technique come together to create outstanding creative at Manchester based Shoot the Moon', 'none', 'Never', 'none', '0', 'none'),
(3, 1, 1, '{filedir_7}digital/active-procurement.jpg\n{filedir_7}digital/chefs-selections.jpg\n{filedir_7}digital/delicious.jpg\n{filedir_7}digital/devilish-facebook.jpg\n{filedir_7}digital/jucee-eshot.jpg\n{filedir_7}digital/macphie.jpg\n{filedir_7}digital/marriages.jpg\n{filedir_7}digital/meadowvale-ecrm.jpg\n{filedir_7}digital/meadowvale.jpg\n{filedir_7}digital/pet-club.jpg\n{filedir_7}digital/scorpio.jpg', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Digital Design in Manchester From Shoot the Moon', 'none', 'We''ve got the experience in digital design to create an online presence that your customers won''t forget. View the projects we''ve worked on…', 'none', 'Never', 'none', '0', 'none'),
(4, 1, 1, '{filedir_7}photography/ss_sample_1.jpg\n{filedir_7}photography/ss_sample_2.jpg\n{filedir_7}photography/ss_sample_3.jpg\n{filedir_7}1.jpg\n{filedir_7}2.jpg\n{filedir_7}photography/ph1.jpg\n{filedir_7}photography/ph10.jpg\n{filedir_7}photography/ph11.jpg\n{filedir_7}photography/ph12.jpg\n{filedir_7}photography/ph13.jpg\n{filedir_7}photography/ph14.jpg\n{filedir_7}photography/ph15.jpg\n{filedir_7}photography/ph16.jpg\n{filedir_7}photography/ph17.jpg\n{filedir_7}photography/ph18.jpg\n{filedir_7}photography/ph2.jpg\n{filedir_7}photography/ph3.jpg\n{filedir_7}photography/ph4.jpg\n{filedir_7}photography/ph5.jpg\n{filedir_7}photography/ph6.jpg\n{filedir_7}photography/ph7.jpg\n{filedir_7}photography/ph8.jpg', 'none', '', 'none', '<p>\n	Still life, lifestyle, location or set photography; Shoot the Moon has a wealth of experience in creating stunning images for design, advertising, packaging and editorial.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Commercial Photography From Shoot the Moon - Manchester', 'none', 'Our talented photography team have worked with household brands to create iconic images at our in-house studio in Manchester.', 'none', 'Weekly', 'none', '0.5', 'none'),
(5, 1, 1, '{filedir_7}1.jpg', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Shoot the Moon - Contact Us on 0161 205 3311', 'none', 'Want to find out more about what we do?  Have a question or comment? Get in touch via our contact form.', 'none', 'Never', 'none', '0', 'none'),
(10, 1, 1, '', 'none', '', 'none', '<p>\n	With over 60 years combined experience in packaging development, we offer an end to end service from concept development through to repro. We can also manage the print process on your behalf and with the advantage of having photography in-house, the service is seamless.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Packaging Design in Manchester from Shoot the Moon', 'none', 'With over 60 years combined experience in package development, Shoot the Moon are the agency to come to for excellence in package design.', 'none', 'Never', 'none', '0', 'none'),
(11, 1, 1, '', 'none', '', 'none', '<p>\n	From a full revamp to individual pieces of POS, we have experience in both retail and wholesale, B2B and B2C &ndash; creating clear, concise communication that works, drives footfall and clearly reflects your category priorities.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', ' Store Comms Design From Shoot the Moon - Manchester', 'none', 'Whether you want a full revamp or development within your current guidelines, we create store comms which clearly communicates your brand and drives sales & footfall', 'none', 'Never', 'none', '0', 'none'),
(12, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(13, 1, 1, '', 'none', '', 'none', '<p>\n	New brands or a polish, or repositioning, of an existing brand &ndash; working with our clients from conception to implementation and ongoing brand integrity across all media.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Branding & Rebranding in Manchester From Shoot The Moon', 'none', 'Whether you want a new brand, rebrand or just a refresh we’ve got the experience & creativity to deliver stunning results.', 'none', 'Never', 'none', '0', 'none'),
(14, 1, 3, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Experienced Web Design in Manchester From Shoot the Moon', 'none', 'Our web design work brings together functionality & aesthetics to create sites which energise and connect with both existing and new customers.', 'none', 'Never', 'none', '0', 'none'),
(15, 1, 3, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'eCRM, ePublishing & eMarketing Design in Manchester From Shoot the Moon', 'none', 'We can frame your eCRM work with design that will engage & motivate your customers, creating email communication with sound ROI.', 'none', 'Never', 'none', '0', 'none'),
(16, 1, 3, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(17, 1, 3, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(18, 1, 1, '', 'none', '1', 'none', '<p>\n	Creating perfect food is artistry in itself. We have a roster of acclaimed stylists, each with their own niche. Leave it to us or in conjunction with your own development team we&#39;ll come up with a range of tempting ideas and stunning images.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Food Photography in Manchester From Shoot the Moon', 'none', 'With a roster of acclaimed stylists, each with their own niche, our food photography is truly mouth-watering.', 'none', 'Never', 'none', '0', 'none'),
(19, 1, 1, '', 'none', '1', 'none', '<p>\n	If you want to talk megapixels we&#39;d be happy to oblige &ndash; but take it as read that we have invested heavily in &#39;kit&#39; to give our team the best tools to work with.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'The Best Product Photography in Manchester From Shoot the Moon', 'none', 'Combining the best kit on the market with our natural eye & technical experience, we make images which let your products sell themselves.', 'none', 'Never', 'none', '0', 'none'),
(20, 1, 1, '', 'none', '1', 'none', '<p>\n	Be it a few shots at your office or a full project managed shoot &ndash; we are experienced in developing style boards, and selecting locations and models, to ensure the end result is a true reflection of your brand.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Location Photography From Shoot the Moon - Manchester', 'none', 'Day or night; indoors, outdoor; nationally or internationally - our location shoots capture the perfect images.', 'none', 'Never', 'none', '0', 'none'),
(21, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Our Personal Photography From Shoot the Moon - Manchester', 'none', 'Our team of photographers love what they do & that means the shutter keeps clicking after work.', 'none', 'Never', 'none', '0', 'none'),
(31, 1, 11, '', 'none', '', 'none', '', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(32, 1, 1, '', 'none', '', 'none', '<p>\n	Corporate literature, sales brochures, catalogues, price lists, full sales presenters, newsletters, magazines... the combination of good creative, stunning photography, technical expertise and support from illustrators &amp; copywriters, all makes for a good team effort and happy customers.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Direct Mail Creative Agency in Manchester - Shoot The Moon', 'none', 'Our Manchester based team use their years of experience to produce direct mail with impact to achieve results.', 'none', 'Never', 'none', '0', 'none'),
(33, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(34, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(35, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(36, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(38, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(39, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(40, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(41, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(42, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(43, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(44, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(45, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(46, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(47, 1, 20, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(49, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(50, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(105, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(106, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(107, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(108, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(109, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(110, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(111, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(112, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(113, 1, 17, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(114, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(115, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(116, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(118, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(120, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(124, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(125, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(126, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(127, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(128, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(129, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(130, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(131, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(132, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(133, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(134, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(135, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(137, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(138, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(139, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(140, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(141, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(142, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(143, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(144, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(145, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(147, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(148, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(149, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(150, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(151, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(152, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(153, 1, 7, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(154, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(155, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(156, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(157, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(158, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(159, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(160, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(161, 1, 9, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(162, 1, 10, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(163, 1, 10, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(164, 1, 10, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(165, 1, 10, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(166, 1, 10, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(167, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(168, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(169, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(170, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(171, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(172, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(173, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(174, 1, 12, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(175, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(178, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(179, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(180, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(181, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(184, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(185, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(186, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(187, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(189, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(190, 1, 15, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(191, 1, 8, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(192, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(193, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(194, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(195, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(196, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(197, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(198, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(199, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(200, 1, 20, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(202, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'Digital Video Creation From Shoot the Moon - Manchester', 'none', 'We’ve got the creative experience & technical know-how to develop campaigns that deliver increased brand engagement.', 'none', 'Never', 'none', '0', 'none'),
(203, 1, 22, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(204, 1, 22, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(205, 1, 22, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(206, 1, 22, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(207, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(208, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(209, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(211, 1, 1, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', 'Digital Banner Design From Shoot the Moon in Manchester', 'none', 'As part of your digital strategy we can create eye-catching banners designed to engage and drive the traffic.', 'none', 'Never', 'none', '0', 'none'),
(212, 1, 21, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(213, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(214, 1, 19, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(215, 1, 21, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(216, 1, 4, '', 'none', '', 'none', '', 'none', '1', 'none', '', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu molestie leo. Integer aliquet nibh et leo facilisis varius. Praesent eu nulla mi, at iaculis neque. Suspendisse tincidunt elementum rutrum. Nullam tortor enim, pharetra non pharetra quis, dictum in est. Vestibulum porttitor vehicula tellus, molestie ultrices elit rhoncus venenatis. Vivamus vel tellus velit. Proin vitae est neque, vel tristique velit. In hac habitasse platea dictumst. Integer egestas, purus non aliquam vehicula, turpis erat lacinia magna, eu suscipit turpis elit a orci. Maecenas tempus mi ac quam malesuada eget feugiat nulla tempus. Aenean orci elit, hendrerit at venenatis quis, pretium non enim.</p>', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(217, 1, 1, '', 'none', '', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu molestie leo. Integer aliquet nibh et leo facilisis varius. Praesent eu nulla mi, at iaculis neque. Suspendisse tincidunt elementum rutrum. Nullam tortor enim, pharetra non pharetra quis, dictum in est. Vestibulum porttitor vehicula tellus, molestie ultrices elit rhoncus venenatis. Vivamus vel tellus velit. Proin vitae est neque, vel tristique velit. In hac habitasse platea dictumst. Integer egestas, purus non aliquam vehicula, turpis erat lacinia magna, eu suscipit turpis elit a orci. Maecenas tempus mi ac quam malesuada eget feugiat nulla tempus. Aenean orci elit, hendrerit at venenatis quis, pretium non enim.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(218, 1, 6, '{filedir_7}studio/darren.jpg\n{filedir_7}studio/Camera-2.jpg\n{filedir_7}studio/DSC_0216.jpg\n{filedir_7}studio/DSC_0220.jpg', 'none', '', 'none', '<p>\n	...and starts with a thorough understanding of the desired result.</p>\n<p>\n	Creative should not be open to interpretation or simply emotive to look at; it needs a desired outcome and insight into what will underpin delivery.</p>\n<p>\n	Our broad creative experience, wide marketing knowledge, ability to digest insight and a thirst for really getting underneath a project, means that we usually get involved way before there is a creative brief in sight.</p>\n<p>\n	Indeed, we offer the best value when simply faced with a client&#39;s dilemma - we&#39;ll construct the brief once we&#39;ve helped to define the strategy...</p>', 'none', '1', 'none', '', 'none', '<p>\n	It&#39;s all in<br />\n	the thinking...</p>', 'none', '', NULL, '<h2 class="rightheads-title">\n	Getting the right heads around the table</h2>\n<p>\n	Outside the agency, we have a number of specialist resources to support thinking, from primary research and localistion expertise to scenario planning for NPD.</p>', 'none', 'Shoot the Moon - Our approach to the Creative and Branding', 'none', 'Manchester based Shoot the Moon put the client and their clients'' customers at the centre of everything they do.', 'none', 'Never', 'none', '0', 'none'),
(219, 1, 1, '', 'none', '', 'none', '<p>\n	Dapibus porro nulla sem exercitationem est massa quisquam incidunt porta corporis animi doloribus tortor magnis accusantium nascetur hic convallis nec exercitationem ullamcorper vestibulum natus torquent ullamco phasellus, iaculis. Rerum class.</p>', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(220, 1, 23, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(221, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', 'Never', 'none', '0', 'none'),
(222, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', 'Never', 'none', '0', 'none'),
(223, 1, 16, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', 'Never', 'none', '0', 'none'),
(224, 1, 14, '', 'none', '1', 'none', '', 'none', '1', 'none', '', 'none', '', 'none', '', NULL, '', 'none', '', 'none', '', 'none', 'Never', 'none', '0', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_entries_autosave`
--

CREATE TABLE IF NOT EXISTS `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_fields`
--

CREATE TABLE IF NOT EXISTS `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `exp_channel_fields`
--

INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`) VALUES
(1, 1, 1, 'cf_rotating_image_blocks', 'Rotating Image Blocks', 'These are the images fading at the top of each department page', 'assets', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 0, 0, 'n', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YToxMDp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czoxOiI3Ijt9czo1OiJtdWx0aSI7czoxOiJ5IjtzOjQ6InZpZXciO3M6NjoidGh1bWJzIjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO31zOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(2, 1, 1, 'cf_portfolio_images', 'Portfolio Images', 'These are the main folio shots - Don''t worry about uploading the correct size as the CMS will scale for you.', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 0, 0, 'n', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MToiMSI7aToxO3M6MToiMiI7fX0='),
(3, 1, 1, 'cf_introduction_text', 'Introduction text', '', 'wygwam', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(6, 1, 1, 'cf_studio_shots', 'Studio Shots', 'These are the company shots - Don''t worry about uploading the correct size as the CMS will scale for you.', 'matrix', '', '0', 0, 0, 'channel', 15, 'title', 'desc', 0, 0, 0, 'n', 'ltr', 'y', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO3M6MjoiMTAiO2k6MTtzOjI6IjExIjtpOjI7czoyOiIxMiI7fX0='),
(7, 1, 1, 'testimonials', 'Testimonials', '', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO2k6MTM7aToxO2k6MTQ7aToyO2k6MTU7fX0='),
(8, 1, 1, 'homepage_pull_quote', 'Homepage Pull Quote', '', 'wygwam', '', '0', 0, 0, 'channel', 15, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(9, 1, 2, 'video_upload', 'Video upload', 'You will first need to use a conversion tool such as http://easyhtml5video.com/ This will help you convert your video into the formats required below.', 'matrix', '', '0', 0, 0, 'channel', 15, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjQ6e2k6MDtzOjI6IjE2IjtpOjE7czoyOiIxNyI7aToyO3M6MjoiMTgiO2k6MztzOjI6IjE5Ijt9fQ=='),
(10, 1, 1, 'cf_rightheads_text', 'Thinkingpage Right heads Text', '', 'wygwam', '', '0', 0, 0, 'channel', 21, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(15, 1, 1, 'browser_title', 'Browser Title', '', 'text', '', '0', 0, 0, 'channel', 21, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(16, 1, 1, 'meta_description', 'Meta Description', '', 'text', '', '0', 0, 0, 'channel', 21, 'title', 'desc', 0, 6, 165, 'n', 'ltr', 'n', 'n', 'none', 'n', 9, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(17, 1, 1, 'change_frequency', 'Change Frequency', '', 'select', 'Never\nDaily\nWeekly\nMonthly\nAlways', 'n', 0, 0, 'channel', 21, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(18, 1, 1, 'priority', 'Priority', '', 'select', '0\n0.1\n0.2\n0.3\n0.4\n0.5\n0.6\n0.7\n0.8\n0.9\n1', 'n', 0, 0, 'channel', 21, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 11, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_titles`
--

CREATE TABLE IF NOT EXISTS `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=225 ;

--
-- Dumping data for table `exp_channel_titles`
--

INSERT INTO `exp_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `pentry_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `dst_enabled`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`) VALUES
(1, 1, 6, 1, 0, NULL, '127.0.0.1', 'The Studio', 'the-studio', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351165147, 'n', '2012', '10', '25', 0, 0, 20131010130408, 0, 0),
(2, 1, 1, 1, 0, NULL, '127.0.0.1', 'Creative', 'creative', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351165224, 'n', '2012', '10', '25', 0, 0, 20130924085225, 0, 0),
(3, 1, 1, 1, 0, NULL, '127.0.0.1', 'Digital', 'digital', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351165215, 'n', '2012', '10', '25', 0, 0, 20130924085516, 0, 0),
(4, 1, 1, 1, 0, NULL, '127.0.0.1', 'Photography', 'photography', 'open', 'y', 0, 0, 0, 0, 'n', 'y', 1351165255, 'n', '2012', '10', '25', 0, 0, 20130924085756, 0, 0),
(5, 1, 1, 1, 0, NULL, '127.0.0.1', 'Get in Touch', 'get-in-touch', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351165276, 'n', '2012', '10', '25', 0, 0, 20130924085717, 0, 0),
(10, 1, 1, 1, 0, NULL, '127.0.0.1', 'Packaging', 'packaging', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250057, 'n', '2012', '10', '26', 0, 0, 20130924085318, 0, 0),
(11, 1, 1, 1, 0, NULL, '127.0.0.1', 'Store Comms', 'store-comms', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250203, 'n', '2012', '10', '26', 0, 0, 20130924085444, 0, 0),
(12, 1, 1, 1, 0, NULL, '127.0.0.1', 'Advertising', 'advertising', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1351250164, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(13, 1, 1, 1, 0, NULL, '127.0.0.1', 'Branding', 'branding', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250203, 'n', '2012', '10', '26', 0, 0, 20130924085244, 0, 0),
(14, 1, 3, 1, 0, NULL, '127.0.0.1', 'Websites', 'websites', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250270, 'n', '2012', '10', '26', 0, 0, 20130924085651, 0, 0),
(15, 1, 3, 1, 0, NULL, '127.0.0.1', 'eCRM', 'ecrm', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250272, 'n', '2012', '10', '26', 0, 0, 20130924085553, 0, 0),
(16, 1, 3, 1, 0, NULL, '127.0.0.1', 'Strategy', 'marketing', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1351250224, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(17, 1, 3, 1, 0, NULL, '109.111.197.225', 'Mobile', 'mobile', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1351250284, 'n', '2012', '10', '26', 0, 0, 20130111124905, 0, 0),
(18, 1, 1, 1, 0, NULL, '127.0.0.1', 'Food', 'food', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250236, 'n', '2012', '10', '26', 0, 0, 20130924085817, 0, 0),
(19, 1, 1, 1, 0, NULL, '127.0.0.1', 'Product', 'product', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1335183199, 'n', '2012', '04', '23', 0, 0, 20130924085920, 0, 0),
(20, 1, 1, 1, 0, NULL, '127.0.0.1', 'Location', 'location', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250255, 'n', '2012', '10', '26', 0, 0, 20130924085836, 0, 0),
(21, 1, 1, 1, 0, NULL, '127.0.0.1', 'Personal', 'personal', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351250395, 'n', '2012', '10', '26', 0, 0, 20130924085856, 0, 0),
(31, 1, 11, 1, 0, NULL, '109.111.197.225', 'Testimonials', 'testimonials', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351521484, 'n', '2012', '10', '29', 0, 0, 20130111124905, 0, 0),
(32, 1, 1, 1, 0, NULL, '127.0.0.1', 'Direct Mail', 'direct-mail', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349784779, 'n', '2012', '10', '09', 0, 0, 20130924085300, 0, 0),
(33, 1, 12, 1, 0, NULL, '109.111.197.225', 'Fish Mongers', 'packaging-one', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345810384, 'n', '2012', '08', '24', 0, 0, 20130111124905, 0, 0),
(34, 1, 12, 1, 0, NULL, '109.111.197.225', 'Humdingers - Concept Development', 'packaging-two', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1342613584, 'n', '2012', '07', '18', 0, 0, 20130111124905, 0, 0),
(35, 1, 12, 1, 0, NULL, '109.111.197.225', 'Countdown - Rivington Biscuits', 'packaging-three', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1321877584, 'n', '2011', '11', '21', 0, 0, 20130111124905, 0, 0),
(36, 1, 12, 1, 0, NULL, '109.111.197.225', 'Advanced Nutrition - Pets at Home', 'packaging-four', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327925584, 'n', '2012', '01', '30', 0, 0, 20130111124905, 0, 0),
(38, 1, 15, 1, 0, NULL, '109.111.197.225', 'Hewby''s Bakehouse', 'branding-one', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327666384, 'n', '2012', '01', '27', 0, 0, 20130111124905, 0, 0),
(39, 1, 15, 1, 0, NULL, '109.111.197.225', 'Harbour Smokehouse', 'branding-two', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339416784, 'n', '2012', '06', '11', 0, 0, 20130111124905, 0, 0),
(40, 1, 16, 1, 0, NULL, '109.111.197.225', 'Disotto Foods', 'brochure-one', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1337688784, 'n', '2012', '05', '22', 0, 0, 20130111124905, 0, 0),
(41, 1, 12, 1, 0, NULL, '109.111.197.225', 'Literature Item One', 'literature-item-one', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327925584, 'n', '2012', '01', '30', 0, 0, 20130111124905, 0, 0),
(42, 1, 14, 1, 0, NULL, '109.111.197.225', 'Pets at Home - Wild Bird', 'point-of-sale-one', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1337602384, 'n', '2012', '05', '21', 0, 0, 20130111124905, 0, 0),
(43, 1, 14, 1, 0, NULL, '109.111.197.225', 'Advanced Nutrition - Pets at Home', 'point-of-sale-two', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351532164, 'n', '2012', '10', '29', 0, 0, 20130111124905, 0, 0),
(44, 1, 17, 1, 0, NULL, '109.157.36.243', 'People', 'people', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1352137144, 'n', '2012', '11', '05', 0, 0, 20130111124905, 0, 0),
(45, 1, 17, 1, 0, NULL, '109.157.36.243', 'Thinking', 'thinking', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1352137204, 'n', '2012', '11', '05', 0, 0, 20130111124905, 0, 0),
(46, 1, 17, 1, 0, NULL, '109.157.36.243', 'Jamie', 'jamie', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1352137204, 'n', '2012', '11', '05', 0, 0, 20130111124905, 0, 0),
(47, 1, 20, 1, 0, NULL, '109.111.197.225', 'Princes Jucee', 'princes-jucee', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1352453464, 'n', '2012', '11', '09', 0, 0, 20130111124905, 0, 0),
(49, 1, 20, 1, 0, NULL, '109.111.197.225', 'Devilishly Delicious', 'devilishly-delicious', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353082384, 'n', '2012', '11', '16', 0, 0, 20130111124905, 0, 0),
(50, 1, 20, 1, 0, NULL, '109.111.197.225', 'Active Procurement', 'macphie-of-glenbervie', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353082924, 'n', '2012', '11', '16', 0, 0, 20130111124905, 0, 0),
(105, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio3', 'studio3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277084, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(106, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio4', 'studio4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277084, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(107, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio5', 'studio5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277084, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(108, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio6', 'studio6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277144, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(109, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio7', 'studio7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277144, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(110, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio8', 'studio8', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277144, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(111, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio9', 'studio9', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277204, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(112, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio10', 'studio10', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277204, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(113, 1, 17, 1, 0, NULL, '109.157.36.243', 'Studio11', 'studio11', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356277204, 'n', '2012', '12', '23', 0, 0, 20130111124905, 0, 0),
(114, 1, 15, 1, 0, NULL, '109.111.197.225', 'Montana', 'brand3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1331554384, 'n', '2012', '03', '12', 0, 0, 20130111124905, 0, 0),
(115, 1, 15, 1, 0, NULL, '109.111.197.225', 'Nuba Cocktails', 'brand4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339762384, 'n', '2012', '06', '15', 0, 0, 20130111124905, 0, 0),
(116, 1, 15, 1, 0, NULL, '109.111.197.225', 'Scorpio Worldwide', 'brand5', 'open', 'y', 0, 0, 0, 0, 'y', 'y', 1343909584, 'n', '2012', '08', '02', 0, 0, 20130111124905, 0, 0),
(118, 1, 15, 1, 0, NULL, '109.111.197.225', 'Trophy - Rivington Biscuits', 'brand7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1329048784, 'n', '2012', '02', '12', 0, 0, 20130111124905, 0, 0),
(120, 1, 16, 1, 0, NULL, '109.111.197.225', 'Josef Meirs', 'dm4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350821584, 'n', '2012', '10', '21', 0, 0, 20130111124905, 0, 0),
(124, 1, 12, 1, 0, NULL, '109.111.197.225', 'Morpol', 'packaging6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345551184, 'n', '2012', '08', '21', 0, 0, 20130111124905, 0, 0),
(125, 1, 12, 1, 0, NULL, '109.111.197.225', 'Jucee - Princes Food & Drink Group', 'packaging7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1354277584, 'n', '2012', '11', '30', 0, 0, 20130111124905, 0, 0),
(126, 1, 12, 1, 0, NULL, '109.111.197.225', 'Kokonoir', 'packaging8', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1340107984, 'n', '2012', '06', '19', 0, 0, 20130111124905, 0, 0),
(127, 1, 12, 1, 0, NULL, '109.111.197.225', 'Kuli', 'packaging9', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1336997584, 'n', '2012', '05', '14', 0, 0, 20130111124905, 0, 0),
(128, 1, 12, 1, 0, NULL, '109.111.197.225', 'Laziz', 'packaging10', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1323087184, 'n', '2011', '12', '05', 0, 0, 20130111124905, 0, 0),
(129, 1, 12, 1, 0, NULL, '109.111.197.225', 'Meadow Vale Foods', 'packaging11', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1334751184, 'n', '2012', '04', '18', 0, 0, 20130111124905, 0, 0),
(130, 1, 12, 1, 0, NULL, '109.111.197.225', 'Harbour Smokehouse', 'packaging12', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1356342244, 'n', '2012', '12', '24', 0, 0, 20130111124905, 0, 0),
(131, 1, 12, 1, 0, NULL, '109.111.197.225', 'Montana - Rivington Biscuits', 'packaging13', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353327184, 'n', '2012', '11', '19', 0, 0, 20130111124905, 0, 0),
(132, 1, 12, 1, 0, NULL, '109.111.197.225', 'Fairfield Farm Crisps', 'packaging14', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1342527184, 'n', '2012', '07', '17', 0, 0, 20130111124905, 0, 0),
(133, 1, 12, 1, 0, NULL, '109.111.197.225', 'CP Foods Concepts', 'packaging15', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327061584, 'n', '2012', '01', '20', 0, 0, 20130111124905, 0, 0),
(134, 1, 12, 1, 0, NULL, '109.111.197.225', 'Vimto', 'packaging16', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1321877584, 'n', '2011', '11', '21', 0, 0, 20130111124905, 0, 0),
(135, 1, 14, 1, 0, NULL, '109.111.197.225', 'Dickies - Concept Work', 'store-comms-3', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1356342844, 'n', '2012', '12', '24', 0, 0, 20130111124905, 0, 0),
(137, 1, 14, 1, 0, NULL, '109.111.197.225', 'Kids'' Holiday Pet Club', 'store-comms-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1329221584, 'n', '2012', '02', '14', 0, 0, 20130111124905, 0, 0),
(138, 1, 16, 1, 0, NULL, '109.111.197.225', 'Ryman Furniture Showroom', 'store-comms-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1337170384, 'n', '2012', '05', '16', 0, 0, 20130111124905, 0, 0),
(139, 1, 14, 1, 0, NULL, '109.111.197.225', 'Ryman Furniture Showroom', 'store-comms-7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1327320784, 'n', '2012', '01', '23', 0, 0, 20130111124905, 0, 0),
(140, 1, 14, 1, 0, NULL, '109.111.197.225', 'Ryman the Stationers', 'store-comms-8', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1332072784, 'n', '2012', '03', '18', 0, 0, 20130111124905, 0, 0),
(141, 1, 16, 1, 0, NULL, '109.111.197.225', 'Pets at Home - Store Openings', 'store-comms-9', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1337688804, 'n', '2012', '05', '22', 0, 0, 20130116111825, 0, 0),
(142, 1, 7, 1, 0, NULL, '109.111.197.225', 'food1', 'food1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353413584, 'n', '2012', '11', '20', 0, 0, 20130111124905, 0, 0),
(143, 1, 7, 1, 0, NULL, '109.111.197.225', 'food2', 'food2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350994384, 'n', '2012', '10', '23', 0, 0, 20130111124905, 0, 0),
(144, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food3', 'food3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353240784, 'n', '2012', '11', '18', 0, 0, 20130111124905, 0, 0),
(145, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food4', 'food4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346674384, 'n', '2012', '09', '03', 0, 0, 20130111124905, 0, 0),
(147, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food6', 'food6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346847184, 'n', '2012', '09', '05', 0, 0, 20130111124905, 0, 0),
(148, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food7', 'food7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345032784, 'n', '2012', '08', '15', 0, 0, 20130111124905, 0, 0),
(149, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food8', 'food8', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1343995984, 'n', '2012', '08', '03', 0, 0, 20130111124905, 0, 0),
(150, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food9', 'food9', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349698384, 'n', '2012', '10', '08', 0, 0, 20130111124905, 0, 0),
(151, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food10', 'food10', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346069584, 'n', '2012', '08', '27', 0, 0, 20130111124905, 0, 0),
(152, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food11', 'food11', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1347883984, 'n', '2012', '09', '17', 0, 0, 20130111124905, 0, 0),
(153, 1, 7, 1, 0, NULL, '109.111.197.225', 'Food12', 'food12', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350994384, 'n', '2012', '10', '23', 0, 0, 20130111124905, 0, 0),
(154, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product1', 'product1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339416784, 'n', '2012', '06', '11', 0, 0, 20130111124905, 0, 0),
(155, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product2', 'product2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1330085584, 'n', '2012', '02', '24', 0, 0, 20130111124905, 0, 0),
(156, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product3', 'product3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1348747984, 'n', '2012', '09', '27', 0, 0, 20130111124905, 0, 0),
(157, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product4', 'product4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1332763984, 'n', '2012', '03', '26', 0, 0, 20130111124905, 0, 0),
(158, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product5', 'product5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1326197584, 'n', '2012', '01', '10', 0, 0, 20130111124905, 0, 0),
(159, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product6', 'product6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1336911184, 'n', '2012', '05', '13', 0, 0, 20130111124905, 0, 0),
(160, 1, 9, 1, 0, NULL, '109.111.197.225', 'Location1', 'location1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1321272784, 'n', '2011', '11', '14', 0, 0, 20130111124905, 0, 0),
(161, 1, 9, 1, 0, NULL, '109.111.197.225', 'Location2', 'location2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1318853584, 'n', '2011', '10', '17', 0, 0, 20130111124905, 0, 0),
(162, 1, 10, 1, 0, NULL, '109.111.197.225', 'Personal1', 'personal1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1334664784, 'n', '2012', '04', '17', 0, 0, 20130111124905, 0, 0),
(163, 1, 10, 1, 0, NULL, '109.111.197.225', 'Personal2', 'personal2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1328875984, 'n', '2012', '02', '10', 0, 0, 20130111124905, 0, 0),
(164, 1, 10, 1, 0, NULL, '109.111.197.225', 'Personal3', 'personal3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1337688784, 'n', '2012', '05', '22', 0, 0, 20130111124905, 0, 0),
(165, 1, 10, 1, 0, NULL, '109.111.197.225', 'Personal4', 'personal4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1330344784, 'n', '2012', '02', '27', 0, 0, 20130111124905, 0, 0),
(166, 1, 10, 1, 0, NULL, '109.111.197.225', 'Personal 5', 'personal-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1343218384, 'n', '2012', '07', '25', 0, 0, 20130111124905, 0, 0),
(167, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product6', 'product61', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339330384, 'n', '2012', '06', '10', 0, 0, 20130111124905, 0, 0),
(168, 1, 8, 1, 0, NULL, '109.111.197.225', 'Product7', 'product7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1334664784, 'n', '2012', '04', '17', 0, 0, 20130111124905, 0, 0),
(169, 1, 14, 1, 0, NULL, '109.111.197.225', 'Pick and Mix', 'pick-and-mix', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1324383184, 'n', '2011', '12', '20', 0, 0, 20130111124905, 0, 0),
(170, 1, 12, 1, 0, NULL, '109.111.197.225', 'Marriages', 'marriages', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1357314415, 'n', '2013', '01', '04', 0, 0, 20130123163956, 0, 0),
(171, 1, 12, 1, 0, NULL, '109.111.197.225', 'Harbour Smoke House', 'harbour-smoke-house', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357314424, 'n', '2013', '01', '04', 0, 0, 20130111124905, 0, 0),
(172, 1, 12, 1, 0, NULL, '109.111.197.225', 'Devilishly Delicious', 'devilishly-delicious', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1338207184, 'n', '2012', '05', '28', 0, 0, 20130111124905, 0, 0),
(173, 1, 12, 1, 0, NULL, '109.111.197.225', 'Disotto', 'disotto', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1333368784, 'n', '2012', '04', '02', 0, 0, 20130111124905, 0, 0),
(174, 1, 12, 1, 0, NULL, '109.111.197.225', 'Pink Panther', 'pink-panther', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1348229584, 'n', '2012', '09', '21', 0, 0, 20130111124905, 0, 0),
(175, 1, 16, 1, 0, NULL, '109.111.197.225', 'Lakeland', 'lakeland', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349352784, 'n', '2012', '10', '04', 0, 0, 20130111124905, 0, 0),
(178, 1, 16, 1, 0, NULL, '109.111.197.225', 'Lakeland', 'lakeland3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1341231184, 'n', '2012', '07', '02', 0, 0, 20130111124905, 0, 0),
(179, 1, 16, 1, 0, NULL, '109.111.197.225', 'Thomson', 'thomson', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1341663184, 'n', '2012', '07', '07', 0, 0, 20130111124905, 0, 0),
(180, 1, 16, 1, 0, NULL, '109.111.197.225', 'Thomson', 'thomson1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350907984, 'n', '2012', '10', '22', 0, 0, 20130111124905, 0, 0),
(181, 1, 16, 1, 0, NULL, '109.111.197.225', 'Thomson', 'thomson2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1326629584, 'n', '2012', '01', '15', 0, 0, 20130111124905, 0, 0),
(184, 1, 15, 1, 0, NULL, '109.111.197.225', 'Kids Holiday Pet Club Winter', 'kids-holiday-pet-club-winter', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1329567184, 'n', '2012', '02', '18', 0, 0, 20130111124905, 0, 0),
(185, 1, 15, 1, 0, NULL, '109.111.197.225', 'Kids Holiday Pet Club Summer', 'kids-holiday-pet-club-summer', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346760784, 'n', '2012', '09', '04', 0, 0, 20130111124905, 0, 0),
(186, 1, 15, 1, 0, NULL, '109.111.197.225', 'Vero Gelato', 'vero-gelato', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345983184, 'n', '2012', '08', '26', 0, 0, 20130111124905, 0, 0),
(187, 1, 15, 1, 0, NULL, '109.111.197.225', 'Pound Bakery', 'pound-bakery', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1335010384, 'n', '2012', '04', '21', 0, 0, 20130111124905, 0, 0),
(189, 1, 15, 1, 0, NULL, '109.111.197.225', 'Sayers', 'sayers', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1316520784, 'n', '2011', '09', '20', 0, 0, 20130111124905, 0, 0),
(190, 1, 15, 1, 0, NULL, '109.111.197.225', 'Goodness Me', 'goodness-me', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1339935184, 'n', '2012', '06', '17', 0, 0, 20130111124905, 0, 0),
(191, 1, 8, 1, 0, NULL, '109.111.197.225', 'Spain Shirt', 'spain-shirt', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1345464784, 'n', '2012', '08', '20', 0, 0, 20130111124905, 0, 0),
(192, 1, 20, 1, 0, NULL, '109.111.197.225', 'Caterforce Website', 'caterforce-website', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346242384, 'n', '2012', '08', '29', 0, 0, 20130111124905, 0, 0),
(193, 1, 20, 1, 0, NULL, '109.111.197.225', 'Chefs Selections', 'chefs-selections', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1349957584, 'n', '2012', '10', '11', 0, 0, 20130111124905, 0, 0),
(194, 1, 20, 1, 0, NULL, '109.111.197.225', 'Devilishly Delicious Facebook Creative', 'devilishly-delicious-facebook-creative', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1353082324, 'n', '2012', '11', '16', 0, 0, 20130111124905, 0, 0),
(195, 1, 20, 1, 0, NULL, '109.111.197.225', 'Macphie of Glenbervie', 'macphie-of-glenbervie1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358242912, 'n', '2013', '01', '15', 0, 0, 20130115094153, 0, 0),
(196, 1, 20, 1, 0, NULL, '109.111.197.225', 'Marriages', 'marriages', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1350389584, 'n', '2012', '10', '16', 0, 0, 20130111124905, 0, 0),
(197, 1, 20, 1, 0, NULL, '109.111.197.225', 'Meadowvale', 'meadowvale', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351787944, 'n', '2012', '11', '01', 0, 0, 20130111124905, 0, 0),
(198, 1, 20, 1, 0, NULL, '109.111.197.225', 'Jucee Soft Drinks', 'jucee-soft-drinks', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351874644, 'n', '2012', '11', '02', 0, 0, 20130111124905, 0, 0),
(199, 1, 20, 1, 0, NULL, '109.111.197.225', 'Sayers the Bakery', 'sayers-the-bakery', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1340971984, 'n', '2012', '06', '29', 0, 0, 20130111124905, 0, 0),
(200, 1, 20, 1, 0, NULL, '109.111.197.225', 'Scorpio Worldwide', 'scorpio-worldwide', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357836664, 'n', '2013', '01', '10', 0, 0, 20130111124905, 0, 0),
(202, 1, 1, 1, 0, NULL, '127.0.0.1', 'Motion', 'motion', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357909350, 'n', '2013', '01', '11', 0, 0, 20130924085631, 0, 0),
(203, 1, 22, 1, 0, NULL, '109.111.197.225', 'Jucee Idents', 'jucee-idents', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357910095, 'n', '2013', '01', '11', 0, 0, 20130111131455, 0, 0),
(204, 1, 22, 1, 0, NULL, '109.111.197.225', 'Jucee Bat and Ball', 'jucee-bat-and-ball', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357915358, 'n', '2013', '01', '11', 0, 0, 20130111144238, 0, 0),
(205, 1, 22, 1, 0, NULL, '109.111.197.225', 'Jucee Nets', 'jucee-nets', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357915992, 'n', '2013', '01', '11', 0, 0, 20130111145312, 0, 0),
(206, 1, 22, 1, 0, NULL, '109.111.197.225', 'Jucee Umbrella', 'jucee-umbrella', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357916352, 'n', '2013', '01', '11', 0, 0, 20130111145912, 0, 0),
(207, 1, 19, 1, 0, NULL, '109.111.197.225', 'Meadowvale', 'meadowvale', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1357920897, 'n', '2013', '01', '11', 0, 0, 20130114102558, 0, 0),
(208, 1, 19, 1, 0, NULL, '109.111.197.225', 'Fly Thomas Cook', 'fly-thomas-cook', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357834354, 'n', '2013', '01', '10', 0, 0, 20130111161535, 0, 0),
(209, 1, 19, 1, 0, NULL, '109.111.197.225', 'Pets at Home', 'pets-at-home', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357920473, 'n', '2013', '01', '11', 0, 0, 20130111160753, 0, 0),
(211, 1, 1, 1, 0, NULL, '127.0.0.1', 'Banners', 'banners', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357921715, 'n', '2013', '01', '11', 0, 0, 20130924085536, 0, 0),
(212, 1, 21, 1, 0, NULL, '109.111.197.225', 'Pets at Home', 'pets-at-home', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357921771, 'n', '2013', '01', '11', 0, 0, 20130111162931, 0, 0),
(213, 1, 19, 1, 0, NULL, '109.111.197.225', 'Ryman', 'ryman', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357814420, 'n', '2013', '01', '10', 0, 0, 20130114104021, 0, 0),
(214, 1, 19, 1, 0, NULL, '109.111.197.225', 'Pound Bakery', 'pound-bakery', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357727939, 'n', '2013', '01', '09', 0, 0, 20130109103859, 0, 0),
(215, 1, 21, 1, 0, NULL, '109.111.197.225', 'Mixed Banners', 'mixed-banners', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357814891, 'n', '2013', '01', '10', 0, 0, 20130114104812, 0, 0),
(216, 1, 4, 1, 0, NULL, '109.111.197.225', 'Facilities', 'facilities', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360677707, 'n', '2013', '02', '12', 0, 0, 20130212140147, 0, 0),
(217, 1, 1, 1, 0, NULL, '109.111.197.225', 'Facilities', 'facilities', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360678026, 'n', '2013', '02', '12', 0, 0, 20130212140807, 0, 0),
(218, 1, 6, 1, 0, NULL, '127.0.0.1', 'Our Thinking', 'our-thinking', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1360753898, 'n', '2013', '02', '13', 0, 0, 20130924085739, 0, 0),
(219, 1, 1, 1, 0, NULL, '192.168.90.119', 'Facilities', 'facilities1', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360754684, 'n', '2013', '02', '13', 0, 0, 20130307120145, 0, 0),
(220, 1, 23, 1, 0, NULL, '192.168.90.119', 'This is a test image', 'this-is-a-test-image', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1360755583, 'n', '2013', '02', '13', 0, 0, 20130307114944, 0, 0),
(221, 1, 16, 1, 0, NULL, '127.0.0.1', 'Rymans-back-to-school', 'rymans-back-to-school', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1379946766, 'n', '2013', '09', '23', 0, 0, 20130923143246, 0, 0),
(222, 1, 16, 1, 0, NULL, '127.0.0.1', 'Ride Away', 'ride-away', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1379946939, 'n', '2013', '09', '23', 0, 0, 20130923143539, 0, 0),
(223, 1, 16, 1, 0, NULL, '127.0.0.1', 'Thomson Inflight', 'thomson-inflight', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1379947005, 'n', '2013', '09', '23', 0, 0, 20130923143645, 0, 0),
(224, 1, 14, 1, 0, NULL, '127.0.0.1', 'Ryman Store Comms', 'ryman-store-comms', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1379947001, 'n', '2013', '09', '23', 0, 0, 20130923143641, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_comments`
--

CREATE TABLE IF NOT EXISTS `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_comment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_log`
--

CREATE TABLE IF NOT EXISTS `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `exp_cp_log`
--

INSERT INTO `exp_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`) VALUES
(1, 1, 1, 'Admin', '127.0.0.1', 1351164716, 'Logged in'),
(2, 1, 1, 'Admin', '127.0.0.1', 1351164989, 'Member Group Created:&nbsp;&nbsp;Editors'),
(3, 1, 1, 'Admin', '127.0.0.1', 1351165033, 'Member profile created:&nbsp;&nbsp;Editor'),
(4, 1, 1, 'Admin', '127.0.0.1', 1351165051, 'Field Group Created:&nbsp;Generic'),
(5, 1, 1, 'Admin', '127.0.0.1', 1351165073, 'Channel Created:&nbsp;&nbsp;Landing Page'),
(6, 1, 1, 'Admin', '127.0.0.1', 1351165091, 'Channel Created:&nbsp;&nbsp;Creative'),
(7, 1, 1, 'Admin', '127.0.0.1', 1351165100, 'Channel Created:&nbsp;&nbsp;Digital'),
(8, 1, 1, 'Admin', '127.0.0.1', 1351165111, 'Channel Created:&nbsp;&nbsp;Photography'),
(9, 1, 1, 'Admin', '127.0.0.1', 1351165184, 'Channel Created:&nbsp;&nbsp;The Studio'),
(10, 1, 1, 'Admin', '127.0.0.1', 1351243234, 'Channel Created:&nbsp;&nbsp;Homepage'),
(11, 1, 1, 'Admin', '127.0.0.1', 1351516758, 'Channel Created:&nbsp;&nbsp;food'),
(12, 1, 1, 'Admin', '127.0.0.1', 1351516769, 'Channel Created:&nbsp;&nbsp;Product'),
(13, 1, 1, 'Admin', '127.0.0.1', 1351516795, 'Channel Created:&nbsp;&nbsp;Location'),
(14, 1, 1, 'Admin', '127.0.0.1', 1351516806, 'Channel Created:&nbsp;&nbsp;Set'),
(15, 1, 1, 'Admin', '127.0.0.1', 1351521273, 'Channel Created:&nbsp;&nbsp;Testimonials'),
(16, 1, 1, 'Admin', '127.0.0.1', 1351527474, 'Channel Created:&nbsp;&nbsp;packaging'),
(17, 1, 1, 'Admin', '127.0.0.1', 1351527486, 'Channel Created:&nbsp;&nbsp;Advertising'),
(18, 1, 1, 'Admin', '127.0.0.1', 1351527502, 'Channel Created:&nbsp;&nbsp;Point of Sale'),
(19, 1, 1, 'Admin', '127.0.0.1', 1351527514, 'Channel Created:&nbsp;&nbsp;Branding'),
(20, 1, 1, 'Admin', '127.0.0.1', 1351527527, 'Channel Created:&nbsp;&nbsp;brochure'),
(21, 1, 1, 'Admin', '127.0.0.1', 1351531033, 'Channel Deleted:&nbsp;&nbsp;Advertising'),
(22, 1, 1, 'Admin', '127.0.0.1', 1352136247, 'Channel Created:&nbsp;&nbsp;studio'),
(23, 1, 1, 'Admin', '109.111.197.225', 1352453140, 'Channel Created:&nbsp;&nbsp;Mobile'),
(24, 1, 1, 'Admin', '109.111.197.225', 1352453153, 'Channel Created:&nbsp;&nbsp;eCRM'),
(25, 1, 1, 'Admin', '109.111.197.225', 1352453166, 'Channel Created:&nbsp;&nbsp;Websites'),
(26, 1, 1, 'Admin', '109.111.197.225', 1352453189, 'Channel Created:&nbsp;&nbsp;Strategy'),
(27, 1, 1, 'Admin', '127.0.0.1', 1353941306, 'Logged out'),
(28, 1, 1, 'Admin', '127.0.0.1', 1354707937, 'Logged out'),
(29, 1, 1, 'Admin', '109.111.197.225', 1354723890, 'Logged out'),
(30, 1, 1, 'Admin', '109.111.197.225', 1356344726, 'Logged out'),
(31, 1, 1, 'Admin', '109.111.197.225', 1357315976, 'Logged out'),
(32, 1, 1, 'Admin', '109.111.197.225', 1357643633, 'Logged out'),
(33, 1, 1, 'Admin', '109.111.197.225', 1357660139, 'Logged out'),
(34, 1, 1, 'Admin', '109.111.197.225', 1357743361, 'Logged out'),
(35, 1, 1, 'Admin', '109.111.197.225', 1357743674, 'Logged out'),
(36, 1, 1, 'Admin', '109.111.197.225', 1357743756, 'Logged out'),
(37, 1, 1, 'Admin', '109.111.197.225', 1357743789, 'Logged out'),
(38, 1, 1, 'Admin', '109.111.197.225', 1357743921, 'Logged out'),
(39, 1, 1, 'Admin', '109.111.197.225', 1357838293, 'Logged out'),
(40, 1, 1, 'Admin', '109.111.197.225', 1357909507, 'Channel Created:&nbsp;&nbsp;Motion'),
(41, 1, 1, 'Admin', '109.111.197.225', 1357914809, 'Field Group Created:&nbsp;Video'),
(42, 1, 1, 'Admin', '109.111.197.225', 1357918299, 'Logged out'),
(43, 1, 1, 'Admin', '109.111.197.225', 1357922705, 'Logged out'),
(44, 1, 1, 'Admin', '109.111.197.225', 1358173045, 'Logged out'),
(45, 1, 1, 'Admin', '109.111.197.225', 1358244522, 'Logged out'),
(46, 1, 1, 'Admin', '109.111.197.225', 1358959258, 'Logged out'),
(47, 1, 1, 'Admin', '109.111.197.225', 1360170814, 'Logged out'),
(48, 1, 1, 'Admin', '127.0.0.1', 1360754723, 'Channel Created:&nbsp;&nbsp;Facilities'),
(49, 1, 1, 'Admin', '127.0.0.1', 1379932569, 'Field Group Created:&nbsp;SEO'),
(50, 1, 1, 'Admin', '127.0.0.1', 1379933188, 'Field group Deleted:&nbsp;&nbsp;SEO'),
(51, 1, 1, 'Admin', '127.0.0.1', 1380014261, 'Logged out'),
(52, 1, 1, 'Admin', '127.0.0.1', 1381400581, 'Logged out');

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_search_index`
--

CREATE TABLE IF NOT EXISTS `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_developer_log`
--

CREATE TABLE IF NOT EXISTS `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_mg`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_ml`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_console_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_ping_status`
--

CREATE TABLE IF NOT EXISTS `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_versioning`
--

CREATE TABLE IF NOT EXISTS `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts` (
  `ep_entry_drafts_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) NOT NULL,
  `author_id` int(10) NOT NULL,
  `channel_id` int(10) NOT NULL,
  `site_id` int(10) NOT NULL,
  `status` varchar(255) NOT NULL,
  `url_title` varchar(255) NOT NULL,
  `draft_data` text,
  `expiration_date` int(10) NOT NULL,
  `edit_date` int(10) NOT NULL,
  `entry_date` int(10) NOT NULL,
  PRIMARY KEY (`ep_entry_drafts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts_auth`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts_auth` (
  `ep_auth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `timestamp` int(10) NOT NULL,
  PRIMARY KEY (`ep_auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts_thirdparty`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts_thirdparty` (
  `entry_id` int(10) NOT NULL,
  `field_id` int(10) NOT NULL,
  `type` varchar(255) NOT NULL,
  `row_id` varchar(25) NOT NULL,
  `row_order` int(10) NOT NULL,
  `col_id` int(10) NOT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_roles`
--

CREATE TABLE IF NOT EXISTS `exp_ep_roles` (
  `site_id` int(10) NOT NULL,
  `role` varchar(255) NOT NULL,
  `states` text,
  PRIMARY KEY (`site_id`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_ep_roles`
--

INSERT INTO `exp_ep_roles` (`site_id`, `role`, `states`) VALUES
(1, 'editor', 'a:7:{s:9:"null|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"draft|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:14:"submitted|null";a:4:{s:5:"label";s:26:"bwf_status_label_submitted";s:8:"can_edit";b:0;s:11:"can_preview";b:0;s:7:"buttons";a:1:{i:0;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:9:"open|null";a:2:{s:5:"label";s:21:"bwf_status_label_live";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"open|draft";a:2:{s:5:"label";s:27:"bwf_status_label_draft_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}i:2;a:5:{i:0;N;i:1;s:10:"epBwfDraft";i:2;s:6:"delete";i:3;s:21:"bwf_btn_discard_draft";i:4;b:0;}}}s:14:"open|submitted";a:4:{s:5:"label";s:31:"bwf_status_label_submitted_live";s:8:"can_edit";b:0;s:11:"can_preview";b:0;s:7:"buttons";a:1:{i:0;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:11:"closed|null";a:2:{s:5:"label";s:25:"bwf_status_label_archived";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}}'),
(1, 'publisher', 'a:7:{s:9:"null|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"draft|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:14:"submitted|null";a:2:{s:5:"label";s:26:"bwf_status_label_submitted";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:9:"open|null";a:2:{s:5:"label";s:21:"bwf_status_label_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:6:"closed";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_archive";i:4;b:0;}i:1;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:2;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"open|draft";a:2:{s:5:"label";s:27:"bwf_status_label_draft_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfDraft";i:2;s:7:"replace";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}i:2;a:5:{i:0;N;i:1;s:10:"epBwfDraft";i:2;s:6:"delete";i:3;s:21:"bwf_btn_discard_draft";i:4;b:0;}}}s:14:"open|submitted";a:2:{s:5:"label";s:31:"bwf_status_label_submitted_live";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfDraft";i:2;s:7:"replace";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:11:"closed|null";a:2:{s:5:"label";s:23:"bwf_status_label_closed";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_settings`
--

CREATE TABLE IF NOT EXISTS `exp_ep_settings` (
  `site_id` int(10) NOT NULL,
  `class` varchar(255) NOT NULL,
  `settings` text,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_extensions`
--

CREATE TABLE IF NOT EXISTS `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `exp_extensions`
--

INSERT INTO `exp_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`) VALUES
(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y'),
(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y'),
(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y'),
(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y'),
(14, 'Assets_ext', 'channel_entries_query_result', 'channel_entries_query_result', '', 10, '1.2.2', 'y'),
(15, 'Field_editor_ext', 'cp_menu_array', 'cp_menu_array', 'a:2:{s:13:"global_prefix";s:0:"";s:14:"group_prefixes";a:1:{i:1;s:3:"cf_";}}', 10, '1.0.3', 'y'),
(16, 'Libraree_ext', 'process', 'sessions_start', 'a:4:{s:11:"license_key";s:36:"eb01f217-108c-4de0-915b-6c4a358b09d5";s:4:"path";s:58:"/var/www/vhosts/stmpreview.co.uk/subdomains/stm/templates/";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 1, '1.0.5', 'y'),
(17, 'Libraree_ext', 'process_check', 'cp_js_end', 'a:4:{s:11:"license_key";s:36:"eb01f217-108c-4de0-915b-6c4a358b09d5";s:4:"path";s:58:"/var/www/vhosts/stmpreview.co.uk/subdomains/stm/templates/";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 2, '1.0.5', 'y'),
(18, 'Libraree_ext', 'low_variables_delete', 'low_variables_delete', 'a:4:{s:11:"license_key";s:36:"eb01f217-108c-4de0-915b-6c4a358b09d5";s:4:"path";s:58:"/var/www/vhosts/stmpreview.co.uk/subdomains/stm/templates/";s:16:"snippets_enabled";s:1:"1";s:24:"global_variables_enabled";s:1:"1";}', 2, '1.0.5', 'y'),
(19, 'Low_reorder_ext', 'entry_submission_end', 'entry_submission_end', 'a:0:{}', 5, '2.0.4', 'y'),
(20, 'Low_reorder_ext', 'channel_entries_query_result', 'channel_entries_query_result', 'a:0:{}', 5, '2.0.4', 'y'),
(21, 'Low_variables_ext', 'sessions_end', 'sessions_end', 'a:7:{s:11:"license_key";s:36:"58007bbf-79cf-45b0-b225-5b3d344aef5b";s:10:"can_manage";a:2:{i:0;s:1:"6";i:1;s:1:"1";}s:16:"register_globals";s:1:"y";s:20:"register_member_data";s:1:"n";s:13:"save_as_files";s:1:"y";s:9:"file_path";s:78:"/Users/martinsmith/Desktop/www/system/expressionengine/templates/low_variables";s:13:"enabled_types";a:1:{i:0;s:12:"low_textarea";}}', 2, '2.3.2', 'y'),
(22, 'Low_variables_ext', 'template_fetch_template', 'template_fetch_template', 'a:7:{s:11:"license_key";s:36:"58007bbf-79cf-45b0-b225-5b3d344aef5b";s:10:"can_manage";a:2:{i:0;s:1:"6";i:1;s:1:"1";}s:16:"register_globals";s:1:"y";s:20:"register_member_data";s:1:"n";s:13:"save_as_files";s:1:"y";s:9:"file_path";s:78:"/Users/martinsmith/Desktop/www/system/expressionengine/templates/low_variables";s:13:"enabled_types";a:1:{i:0;s:12:"low_textarea";}}', 2, '2.3.2', 'y'),
(23, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y'),
(24, 'Updater_ext', 'cp_menu_array', 'cp_menu_array', 'a:0:{}', 100, '3.1.5', 'y'),
(25, 'Zoo_flexible_admin_ext', 'cp_css_end', 'cp_css_end', '', 1, '1.6', 'y'),
(26, 'Zoo_flexible_admin_ext', 'cp_js_end', 'cp_js_end', '', 1, '1.6', 'y'),
(27, 'Zoo_flexible_admin_ext', 'sessions_end', 'sessions_end', '', 1, '1.6', 'y'),
(28, 'Zoo_triggers_ext', 'hook_sessions_start', 'sessions_start', 'a:2:{s:8:"triggers";a:4:{s:3:"tag";a:1:{i:0;s:3:"tag";}s:8:"category";a:1:{i:0;s:8:"category";}s:7:"archive";a:1:{i:0;s:7:"archive";}s:6:"author";a:1:{i:0;s:6:"author";}}s:8:"settings";a:19:{s:16:"entries_operator";s:1:"|";s:31:"entries_title_categories_prefix";s:15:" in categories ";s:32:"entries_title_categories_postfix";s:0:"";s:34:"entries_title_categories_separator";s:2:", ";s:39:"entries_title_categories_separator_last";s:5:" and ";s:28:"entries_title_archives_first";s:5:"month";s:29:"entries_title_archives_prefix";s:4:" in ";s:30:"entries_title_archives_postfix";s:1:".";s:32:"entries_title_archives_separator";s:1:" ";s:27:"entries_title_archives_year";s:1:"Y";s:28:"entries_title_archives_month";s:1:"F";s:24:"entries_title_tag_prefix";s:9:" in tags ";s:25:"entries_title_tag_postfix";s:0:"";s:27:"entries_title_tag_separator";s:2:", ";s:32:"entries_title_tag_separator_last";s:5:" and ";s:27:"entries_title_author_prefix";s:12:" written by ";s:28:"entries_title_author_postfix";s:0:"";s:30:"entries_title_author_separator";s:2:", ";s:35:"entries_title_author_separator_last";s:5:" and ";}}', 9, '1.1.10', 'y'),
(29, 'Zoo_triggers_ext', 'hook_channel_module_create_pagination', 'channel_module_create_pagination', '', 10, '1.1.10', 'y'),
(30, 'Zoo_triggers_ext', 'hook_cp_css_end', 'cp_css_end', '', 1, '1.1.10', 'y'),
(31, 'Zoo_triggers_ext', 'hook_cp_js_end', 'cp_js_end', '', 1, '1.1.10', 'y'),
(32, 'Ep_better_workflow_ext', 'on_sessions_start', 'sessions_start', 'a:0:{}', 8, '1.4', 'y'),
(33, 'Ep_better_workflow_ext', 'on_entry_submission_start', 'entry_submission_start', 'a:0:{}', 10, '1.4', 'y'),
(34, 'Ep_better_workflow_ext', 'on_entry_submission_ready', 'entry_submission_ready', 'a:0:{}', 10, '1.4', 'y'),
(35, 'Ep_better_workflow_ext', 'on_entry_submission_end', 'entry_submission_end', 'a:0:{}', 10, '1.4', 'y'),
(36, 'Ep_better_workflow_ext', 'on_publish_form_entry_data', 'publish_form_entry_data', 'a:0:{}', 10, '1.4', 'y'),
(37, 'Ep_better_workflow_ext', 'on_publish_form_channel_preferences', 'publish_form_channel_preferences', 'a:0:{}', 10, '1.4', 'y'),
(38, 'Ep_better_workflow_ext', 'on_channel_entries_row', 'channel_entries_row', 'a:0:{}', 10, '1.4', 'y'),
(39, 'Ep_better_workflow_ext', 'on_channel_entries_query_result', 'channel_entries_query_result', 'a:0:{}', 8, '1.4', 'y'),
(40, 'Ep_better_workflow_ext', 'on_cp_js_end', 'cp_js_end', 'a:0:{}', 10, '1.4', 'y'),
(41, 'Ep_better_workflow_ext', 'on_template_post_parse', 'template_post_parse', 'a:0:{}', 100, '1.4', 'y'),
(42, 'Ep_better_workflow_ext', 'on_matrix_data_query', 'matrix_data_query', 'a:0:{}', 10, '1.4', 'y'),
(43, 'Ep_better_workflow_ext', 'on_playa_data_query', 'playa_data_query', 'a:0:{}', 10, '1.4', 'y'),
(44, 'Ep_better_workflow_ext', 'on_playa_fetch_rels_query', 'playa_fetch_rels_query', 'a:0:{}', 10, '1.4', 'y'),
(45, 'Ep_better_workflow_ext', 'on_zenbu_filter_by_status', 'zenbu_filter_by_status', 'a:0:{}', 100, '1.4', 'y'),
(46, 'Ep_better_workflow_ext', 'on_zenbu_modify_status_display', 'zenbu_modify_status_display', 'a:0:{}', 100, '1.4', 'y'),
(47, 'Ep_better_workflow_ext', 'on_zenbu_modify_title_display', 'zenbu_modify_title_display', 'a:0:{}', 100, '1.4', 'y'),
(48, 'Ce_img_aws_ext', 'pre_parse', 'ce_img_pre_parse', 'a:0:{}', 9, '1.0', 'y'),
(49, 'Ce_img_aws_ext', 'update_valid_params', 'ce_img_start', 'a:0:{}', 9, '1.0', 'y'),
(50, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.4.3', 'y'),
(51, 'Structure_ext', 'entry_submission_redirect', 'entry_submission_redirect', '', 10, '3.3.6', 'y'),
(52, 'Structure_ext', 'cp_member_login', 'cp_member_login', '', 10, '3.3.6', 'y'),
(53, 'Structure_ext', 'sessions_start', 'sessions_start', '', 10, '3.3.6', 'y'),
(54, 'Structure_ext', 'channel_module_create_pagination', 'channel_module_create_pagination', '', 9, '3.3.6', 'y'),
(55, 'Structure_ext', 'wygwam_config', 'wygwam_config', '', 10, '3.3.6', 'y'),
(56, 'Structure_ext', 'core_template_route', 'core_template_route', '', 10, '3.3.6', 'y'),
(57, 'Structure_ext', 'entry_submission_end', 'entry_submission_end', '', 10, '3.3.6', 'y'),
(58, 'Structure_ext', 'safecracker_submit_entry_end', 'safecracker_submit_entry_end', '', 10, '3.3.6', 'y'),
(59, 'Structure_ext', 'template_post_parse', 'template_post_parse', '', 10, '3.3.6', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `exp_fieldtypes`
--

INSERT INTO `exp_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`) VALUES
(1, 'select', '1.0', 'YTowOnt9', 'n'),
(2, 'text', '1.0', 'YTowOnt9', 'n'),
(3, 'textarea', '1.0', 'YTowOnt9', 'n'),
(4, 'date', '1.0', 'YTowOnt9', 'n'),
(5, 'file', '1.0', 'YTowOnt9', 'n'),
(6, 'multi_select', '1.0', 'YTowOnt9', 'n'),
(7, 'checkboxes', '1.0', 'YTowOnt9', 'n'),
(8, 'radio', '1.0', 'YTowOnt9', 'n'),
(9, 'rel', '1.0', 'YTowOnt9', 'n'),
(10, 'rte', '1.0', 'YTowOnt9', 'n'),
(12, 'assets', '1.2.2', 'YTowOnt9', 'y'),
(13, 'low_variables', '2.3.2', 'YTowOnt9', 'n'),
(14, 'playa', '4.3.3', 'YTowOnt9', 'y'),
(15, 'wygwam', '2.6.3', 'YToyOntzOjExOiJsaWNlbnNlX2tleSI7czowOiIiO3M6MTI6ImZpbGVfYnJvd3NlciI7czo2OiJhc3NldHMiO30=', 'y'),
(16, 'matrix', '2.4.3', 'YTowOnt9', 'y'),
(17, 'pt_checkboxes', '1.0.3', 'YTowOnt9', 'n'),
(18, 'pt_dropdown', '1.0.3', 'YTowOnt9', 'n'),
(19, 'pt_multiselect', '1.0.3', 'YTowOnt9', 'n'),
(20, 'pt_radio_buttons', '1.0.3', 'YTowOnt9', 'n'),
(21, 'structure', '3.3.6', 'YTowOnt9', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_formatting`
--

CREATE TABLE IF NOT EXISTS `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `exp_field_formatting`
--

INSERT INTO `exp_field_formatting` (`formatting_id`, `field_id`, `field_fmt`) VALUES
(1, 1, 'none'),
(2, 1, 'br'),
(3, 1, 'xhtml'),
(4, 2, 'none'),
(5, 2, 'br'),
(6, 2, 'xhtml'),
(7, 3, 'none'),
(8, 3, 'br'),
(9, 3, 'xhtml'),
(16, 6, 'none'),
(17, 6, 'br'),
(18, 6, 'xhtml'),
(19, 7, 'none'),
(20, 7, 'br'),
(21, 7, 'xhtml'),
(22, 8, 'none'),
(23, 8, 'br'),
(24, 8, 'xhtml'),
(25, 9, 'none'),
(26, 9, 'br'),
(27, 9, 'xhtml'),
(28, 10, 'none'),
(29, 10, 'br'),
(30, 10, 'xhtml'),
(43, 15, 'none'),
(44, 15, 'br'),
(45, 15, 'xhtml'),
(46, 16, 'none'),
(47, 16, 'br'),
(48, 16, 'xhtml'),
(49, 17, 'none'),
(50, 17, 'br'),
(51, 17, 'xhtml'),
(52, 18, 'none'),
(53, 18, 'br'),
(54, 18, 'xhtml');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_groups`
--

CREATE TABLE IF NOT EXISTS `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_field_groups`
--

INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Generic'),
(2, 1, 'Video');

-- --------------------------------------------------------

--
-- Table structure for table `exp_files`
--

CREATE TABLE IF NOT EXISTS `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=136 ;

--
-- Dumping data for table `exp_files`
--

INSERT INTO `exp_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `credit`, `location`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`) VALUES
(1, 1, '1.jpg', 7, '/Users/martinsmith/Desktop/www/images/rotating_image_panels/1.jpg', 'image/jpeg', '1.jpg', 57831, NULL, NULL, NULL, 1, 1351526635, 1, 1351526635, '466 464'),
(2, 1, '3.jpg', 7, '/Users/martinsmith/Desktop/www/images/rotating_image_panels/3.jpg', 'image/jpeg', '3.jpg', 77316, NULL, NULL, NULL, 1, 1351526635, 1, 1351526635, '717 596'),
(3, 1, '2.jpg', 7, '/Users/martinsmith/Desktop/www/images/rotating_image_panels/2.jpg', 'image/jpeg', '2.jpg', 76815, NULL, NULL, NULL, 1, 1351526635, 1, 1351526635, '722 604'),
(4, 1, 'stm-brand-portfolio-2012-3.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-3.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-3.jpg', 194112, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1096 1549'),
(5, 1, 'stm-brand-portfolio-2012-4.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-4.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-4.jpg', 150997, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1108 1549'),
(6, 1, 'stm-brand-portfolio-2012-6.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-6.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-6.jpg', 164988, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1108 1549'),
(7, 1, 'stm-brand-portfolio-2012-7.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-7.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-7.jpg', 251958, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1108 1549'),
(8, 1, 'stm-brand-portfolio-2012-8.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-8.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-8.jpg', 191932, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1108 1549'),
(9, 1, 'stm-brand-portfolio-2012-9.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-9.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-9.jpg', 189878, NULL, NULL, NULL, 1, 1351527780, 1, 1351527780, '1109 1549'),
(10, 1, 'stm-brand-portfolio-2012-10.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-10.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-10.jpg', 132467, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1108 1549'),
(11, 1, 'stm-brand-portfolio-2012-11.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-11.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-11.jpg', 167100, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1109 1549'),
(12, 1, 'stm-brand-portfolio-2012-12.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-12.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-12.jpg', 229840, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1109 1549'),
(13, 1, 'stm-brand-portfolio-2012-13.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-13.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-13.jpg', 132202, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1108 1549'),
(14, 1, 'stm-brand-portfolio-2012-14.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-14.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-14.jpg', 146279, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1109 1549'),
(15, 1, 'stm-brand-portfolio-2012-15.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-15.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-15.jpg', 201585, NULL, NULL, NULL, 1, 1351527781, 1, 1351527781, '1108 1549'),
(16, 1, 'stm-brand-portfolio-2012-16.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-16.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-16.jpg', 211481, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1109 1549'),
(17, 1, 'stm-brand-portfolio-2012-17.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-17.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-17.jpg', 156160, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1108 1549'),
(18, 1, 'stm-brand-portfolio-2012-18.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-18.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-18.jpg', 153833, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1106 1549'),
(19, 1, 'stm-brand-portfolio-2012-20.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-20.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-20.jpg', 224514, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1108 1549'),
(20, 1, 'stm-brand-portfolio-2012-19.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-19.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-19.jpg', 180734, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1101 1549'),
(21, 1, 'stm-brand-portfolio-2012-21.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-21.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-21.jpg', 263011, NULL, NULL, NULL, 1, 1351527782, 1, 1351527782, '1108 1549'),
(22, 1, 'stm-brand-portfolio-2012-22.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-22.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-22.jpg', 222677, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1108 1549'),
(23, 1, 'stm-brand-portfolio-2012-23.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-23.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-23.jpg', 188589, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1109 1549'),
(24, 1, 'stm-brand-portfolio-2012-24.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-24.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-24.jpg', 186677, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1108 1549'),
(25, 1, 'stm-brand-portfolio-2012-26.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-26.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-26.jpg', 233105, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1109 1549'),
(26, 1, 'stm-brand-portfolio-2012-25.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-25.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-25.jpg', 371597, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1281 1777'),
(27, 1, 'stm-brand-portfolio-2012-27.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-27.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-27.jpg', 275051, NULL, NULL, NULL, 1, 1351527783, 1, 1351527783, '1109 1548'),
(28, 1, 'stm-brand-portfolio-2012-28.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-28.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-28.jpg', 217450, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1108 1549'),
(29, 1, 'stm-brand-portfolio-2012-29.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-29.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-29.jpg', 216274, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1108 1549'),
(30, 1, 'stm-brand-portfolio-2012-30.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-30.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-30.jpg', 237888, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1104 1549'),
(31, 1, 'stm-brand-portfolio-2012-31.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-31.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-31.jpg', 220081, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1104 1549'),
(32, 1, 'stm-brand-portfolio-2012-32.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-32.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-32.jpg', 243955, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1109 1548'),
(33, 1, 'stm-brand-portfolio-2012-33.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-33.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-33.jpg', 208068, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1109 1548'),
(34, 1, 'stm-brand-portfolio-2012-34.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-34.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-34.jpg', 253880, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1109 1549'),
(35, 1, 'stm-brand-portfolio-2012-35.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-35.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-35.jpg', 194088, NULL, NULL, NULL, 1, 1351527784, 1, 1351527784, '1108 1549'),
(36, 1, 'stm-brand-portfolio-2012-36.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-36.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-36.jpg', 309436, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1105 1549'),
(37, 1, 'stm-brand-portfolio-2012-37.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-37.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-37.jpg', 267919, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1109 1549'),
(38, 1, 'stm-brand-portfolio-2012-38.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-38.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-38.jpg', 362128, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1108 1549'),
(39, 1, 'stm-brand-portfolio-2012-39.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-39.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-39.jpg', 375876, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1109 1549'),
(40, 1, 'stm-brand-portfolio-2012-40.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-40.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-40.jpg', 243773, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1108 1549'),
(41, 1, 'stm-brand-portfolio-2012-41.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-41.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-41.jpg', 278535, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1108 1549'),
(42, 1, 'stm-brand-portfolio-2012-42.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-42.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-42.jpg', 233821, NULL, NULL, NULL, 1, 1351527785, 1, 1351527785, '1109 1549'),
(43, 1, 'stm-brand-portfolio-2012-43.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-43.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-43.jpg', 311881, NULL, NULL, NULL, 1, 1351527786, 1, 1351527786, '1109 1549'),
(44, 1, 'stm-brand-portfolio-2012-44.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-44.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-44.jpg', 278643, NULL, NULL, NULL, 1, 1351527786, 1, 1351527786, '1109 1549'),
(45, 1, 'stm-brand-portfolio-2012-45.jpg', 2, '/Users/martinsmith/Desktop/www/images/creative/stm-brand-portfolio-2012-45.jpg', 'image/jpeg', 'stm-brand-portfolio-2012-45.jpg', 169638, NULL, NULL, NULL, 1, 1351527786, 1, 1351527786, '1109 1549'),
(48, 1, 'Alpha---Special-Offers.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Alpha---Special-Offers.jpg', 'image/jpeg', 'Alpha---Special-Offers.jpg', 608989, NULL, NULL, NULL, 1, 1352454534, 1, 1353082484, '1416 727'),
(49, 1, 'devilsishly.jpg', 3, '/Users/martinsmith/Desktop/shoot-the-moon/XXXXXXXXX-website/www/images/digital/devilsishly.jpg', 'image/jpeg', 'devilsishly.jpg', 423516, NULL, NULL, NULL, 1, 1353082423, 1, 1353082484, '771 1059'),
(50, 1, 'Original_Squash_ÃÂ«_Jucee.jpeg', 4, '/Users/martinsmith/Desktop/shoot-the-moon/XXXXXXXXX-website/www/images/airside/Original_Squash_ÃÂ«_Jucee.jpeg', 'image/jpeg', 'Original_Squash_ÃÂ«_Jucee.jpeg', 916953, NULL, NULL, NULL, 1, 1352886843, 1, 1352886843, '1070 1030'),
(51, 1, 'Macphie_Development.jpeg', 3, '/Users/martinsmith/Desktop/shoot-the-moon/XXXXXXXXX-website/www/images/digital/Macphie_Development.jpeg', 'image/jpeg', 'Macphie_Development.jpeg', 914112, NULL, NULL, NULL, 1, 1353082981, 1, 1353082981, '1660 1006'),
(52, 1, 'hot-thick-red-curry.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/hot-thick-red-curry.jpg', 'image/jpeg', 'hot-thick-red-curry.jpg', 341257, NULL, NULL, NULL, 1, 1357307068, 1, 1357307068, '855 640'),
(54, 1, 'IMG_6086-RT.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/IMG_6086-RT.jpg', 'image/jpeg', 'IMG_6086-RT.jpg', 180778, NULL, NULL, NULL, 1, 1357307069, 1, 1357307069, '480 640'),
(55, 1, 'hoover.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/hoover.jpg', 'image/jpeg', 'hoover.jpg', 131046, NULL, NULL, NULL, 1, 1357307070, 1, 1357307070, '487 640'),
(56, 1, 'K3D3256.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/K3D3256.jpg', 'image/jpeg', 'K3D3256.jpg', 113382, NULL, NULL, NULL, 1, 1357307071, 1, 1357307071, '489 640'),
(57, 1, 'IMG_6189-RT.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/IMG_6189-RT.jpg', 'image/jpeg', 'IMG_6189-RT.jpg', 425731, NULL, NULL, NULL, 1, 1357307071, 1, 1357307071, '960 640'),
(58, 1, 'MG0936..jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/MG0936..jpg', 'image/jpeg', 'MG0936..jpg', 695778, NULL, NULL, NULL, 1, 1357307072, 1, 1357307072, '960 640'),
(59, 1, 'oysters.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/oysters.jpg', 'image/jpeg', 'oysters.jpg', 354291, NULL, NULL, NULL, 1, 1357307072, 1, 1357307072, '683 640'),
(60, 1, 'pannacotta.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/pannacotta.jpg', 'image/jpeg', 'pannacotta.jpg', 94151, NULL, NULL, NULL, 1, 1357307072, 1, 1357307072, '350 640'),
(61, 1, 'red-kettel.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/red-kettel.jpg', 'image/jpeg', 'red-kettel.jpg', 109775, NULL, NULL, NULL, 1, 1357307073, 1, 1357307073, '640 640'),
(62, 1, 'pea-soup.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/pea-soup.jpg', 'image/jpeg', 'pea-soup.jpg', 473889, NULL, NULL, NULL, 1, 1357307073, 1, 1357307073, '1060 640'),
(63, 1, 'orchid-banquet.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/orchid-banquet.jpg', 'image/jpeg', 'orchid-banquet.jpg', 345511, NULL, NULL, NULL, 1, 1357307073, 1, 1357307073, '431 640'),
(64, 1, 'reduced-fat-custard.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/reduced-fat-custard.jpg', 'image/jpeg', 'reduced-fat-custard.jpg', 132085, NULL, NULL, NULL, 1, 1357307073, 1, 1357307073, '587 640'),
(65, 1, 'runny-cheese.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/runny-cheese.jpg', 'image/jpeg', 'runny-cheese.jpg', 197027, NULL, NULL, NULL, 1, 1357307074, 1, 1357307074, '724 640'),
(66, 1, 'rice-wraps.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/rice-wraps.jpg', 'image/jpeg', 'rice-wraps.jpg', 258433, NULL, NULL, NULL, 1, 1357307074, 1, 1357307074, '640 640'),
(67, 1, 'shippams-RS.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/shippams-RS.jpg', 'image/jpeg', 'shippams-RS.jpg', 309843, NULL, NULL, NULL, 1, 1357307076, 1, 1357307076, '753 640'),
(68, 1, 'salad-red.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/salad-red.jpg', 'image/jpeg', 'salad-red.jpg', 670572, NULL, NULL, NULL, 1, 1357307076, 1, 1357307076, '956 640'),
(69, 1, 'Spicebomb-Page-NEW.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/Spicebomb-Page-NEW.jpg', 'image/jpeg', 'Spicebomb-Page-NEW.jpg', 131852, NULL, NULL, NULL, 1, 1357307077, 1, 1357307077, '427 640'),
(70, 1, 'salmon-sizzler.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/salmon-sizzler.jpg', 'image/jpeg', 'salmon-sizzler.jpg', 376602, NULL, NULL, NULL, 1, 1357307077, 1, 1357307077, '780 640'),
(71, 1, 'small-eggs.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/small-eggs.jpg', 'image/jpeg', 'small-eggs.jpg', 225266, NULL, NULL, NULL, 1, 1357307077, 1, 1357307077, '720 640'),
(72, 1, 'steak-and-pomigranate.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/steak-and-pomigranate.jpg', 'image/jpeg', 'steak-and-pomigranate.jpg', 203284, NULL, NULL, NULL, 1, 1357307077, 1, 1357307077, '640 640'),
(73, 1, 'white-aviator-watch.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/white-aviator-watch.jpg', 'image/jpeg', 'white-aviator-watch.jpg', 203779, NULL, NULL, NULL, 1, 1357307078, 1, 1357307078, '741 640'),
(74, 1, 'Trove-jar-shot.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/Trove-jar-shot.jpg', 'image/jpeg', 'Trove-jar-shot.jpg', 386066, NULL, NULL, NULL, 1, 1357307079, 1, 1357307079, '909 640'),
(75, 1, 'three-flavour-sauce-with-duck.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/three-flavour-sauce-with-duck.jpg', 'image/jpeg', 'three-flavour-sauce-with-duck.jpg', 331446, NULL, NULL, NULL, 1, 1357307079, 1, 1357307079, '638 640'),
(76, 1, 'Winter-2012-Ed-2-Front-Cover-1-v2.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/Winter-2012-Ed-2-Front-Cover-1-v2.jpg', 'image/jpeg', 'Winter-2012-Ed-2-Front-Cover-1-v2.jpg', 265413, NULL, NULL, NULL, 1, 1357307079, 1, 1357307079, '810 640'),
(77, 1, 'YSL-Shocking.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/YSL-Shocking.jpg', 'image/jpeg', 'YSL-Shocking.jpg', 351022, NULL, NULL, NULL, 1, 1357307079, 1, 1357307079, '816 640'),
(84, 1, 'personal--49.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/personal--49.jpg', 'image/jpeg', 'personal--49.jpg', 480465, NULL, NULL, NULL, 1, 1357312052, 1, 1357312052, '956 640'),
(85, 1, 'personal-copy.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/personal-copy.jpg', 'image/jpeg', 'personal-copy.jpg', 145922, NULL, NULL, NULL, 1, 1357312082, 1, 1357312082, '427 640'),
(88, 1, 'Screen_Shot_2013-01-04_at_14.59.20.png', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/Screen_Shot_2013-01-04_at_14.59.20.png', 'image/png', 'Screen_Shot_2013-01-04_at_14.59.20.png', 454181, NULL, NULL, NULL, 1, 1357312214, 1, 1357312214, '640 457'),
(89, 1, '_Almond-Extract.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/_Almond-Extract.jpg', 'image/jpeg', '_Almond-Extract.jpg', 210441, NULL, NULL, NULL, 1, 1357312398, 1, 1357312398, '594 640'),
(90, 1, 'hot-thick-red-curry_1.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/hot-thick-red-curry_1.jpg', 'image/jpeg', 'hot-thick-red-curry_1.jpg', 341257, NULL, NULL, NULL, 1, 1357312398, 1, 1357312398, '855 640'),
(91, 1, 'authentic-cocktail-on-the-beach-.jpeg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/authentic-cocktail-on-the-beach-.jpeg', 'image/jpeg', 'authentic-cocktail-on-the-beach-.jpeg', 126577, NULL, NULL, NULL, 1, 1357312398, 1, 1357312398, '636 640'),
(92, 1, 'IMG_6012-RT.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/IMG_6012-RT.jpg', 'image/jpeg', 'IMG_6012-RT.jpg', 595574, NULL, NULL, NULL, 1, 1357312399, 1, 1357312399, '960 640'),
(93, 1, 'active-procurement.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/active-procurement.jpg', 'image/jpeg', 'active-procurement.jpg', 73143, NULL, NULL, NULL, 1, 1357834834, 1, 1357834834, '594 640'),
(94, 1, 'Untitled-1.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Untitled-1.jpg', 'image/jpeg', 'Untitled-1.jpg', 93306, NULL, NULL, NULL, 1, 1357834982, 1, 1357834982, '594 640'),
(95, 1, 'caterforce.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/caterforce.jpg', 'image/jpeg', 'caterforce.jpg', 93306, NULL, NULL, NULL, 1, 1357835005, 1, 1357835005, '594 640'),
(96, 1, 'chefs-selections.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/chefs-selections.jpg', 'image/jpeg', 'chefs-selections.jpg', 68893, NULL, NULL, NULL, 1, 1357835130, 1, 1357835130, '410 640'),
(97, 1, 'devilish-home.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/devilish-home.jpg', 'image/jpeg', 'devilish-home.jpg', 49609, NULL, NULL, NULL, 1, 1357835267, 1, 1357835267, '382 640'),
(98, 1, 'devilish-facebook.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/devilish-facebook.jpg', 'image/jpeg', 'devilish-facebook.jpg', 73086, NULL, NULL, NULL, 1, 1357835430, 1, 1357835430, '382 640'),
(99, 1, 'macphie-homepage.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/macphie-homepage.jpg', 'image/jpeg', 'macphie-homepage.jpg', 113981, NULL, NULL, NULL, 1, 1357835718, 1, 1357835718, '594 640'),
(100, 1, 'marriages.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/marriages.jpg', 'image/jpeg', 'marriages.jpg', 85445, NULL, NULL, NULL, 1, 1357835867, 1, 1357835867, '594 640'),
(101, 1, 'meadowvale.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/meadowvale.jpg', 'image/jpeg', 'meadowvale.jpg', 120958, NULL, NULL, NULL, 1, 1357835988, 1, 1357835988, '594 640'),
(102, 1, 'jucee-page.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/jucee-page.jpg', 'image/jpeg', 'jucee-page.jpg', 117021, NULL, NULL, NULL, 1, 1357836291, 1, 1357836291, '594 640'),
(103, 1, 'sayers-screen.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/sayers-screen.jpg', 'image/jpeg', 'sayers-screen.jpg', 95218, NULL, NULL, NULL, 1, 1357836511, 1, 1357836511, '448 640'),
(104, 1, 'scorpio-screen.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/scorpio-screen.jpg', 'image/jpeg', 'scorpio-screen.jpg', 116624, NULL, NULL, NULL, 1, 1357836735, 1, 1357836735, '1000 640'),
(105, 1, 'Yearsley-Screen.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Yearsley-Screen.jpg', 'image/jpeg', 'Yearsley-Screen.jpg', 86051, NULL, NULL, NULL, 1, 1357836900, 1, 1357836900, '506 640'),
(106, 1, 'bat-and-ball.m4v', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/bat-and-ball.m4v', 'video/m4v', 'bat-and-ball.m4v', 1062429, NULL, NULL, NULL, 1, 1357915377, 1, 1357915377, ''),
(107, 1, 'bat-and-ball.webm', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/bat-and-ball.webm', 'video/webm', 'bat-and-ball.webm', 1103137, NULL, NULL, NULL, 1, 1357915392, 1, 1357915392, ''),
(108, 1, 'bat-and-ball.ogv', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/bat-and-ball.ogv', 'video/ogg', 'bat-and-ball.ogv', 1533453, NULL, NULL, NULL, 1, 1357915404, 1, 1357915404, ''),
(109, 1, 'bat-and-ball.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/bat-and-ball.jpg', 'image/jpeg', 'bat-and-ball.jpg', 32797, NULL, NULL, NULL, 1, 1357915414, 1, 1357915414, '360 640'),
(110, 1, 'nets.m4v', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/nets.m4v', 'video/m4v', 'nets.m4v', 1375891, NULL, NULL, NULL, 1, 1357916067, 1, 1357916067, ''),
(111, 1, 'nets.webm', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/nets.webm', 'video/webm', 'nets.webm', 1019725, NULL, NULL, NULL, 1, 1357916081, 1, 1357916081, ''),
(112, 1, 'nets.ogv', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/nets.ogv', 'video/ogg', 'nets.ogv', 1935004, NULL, NULL, NULL, 1, 1357916097, 1, 1357916097, ''),
(113, 1, 'nets.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/nets.jpg', 'image/jpeg', 'nets.jpg', 35855, NULL, NULL, NULL, 1, 1357916108, 1, 1357916108, '360 640'),
(114, 1, 'Jucee_Umbrella.m4v', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Jucee_Umbrella.m4v', 'video/m4v', 'Jucee_Umbrella.m4v', 1594112, NULL, NULL, NULL, 1, 1357916379, 1, 1357916379, ''),
(115, 1, 'Jucee_Umbrella.webm', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Jucee_Umbrella.webm', 'video/webm', 'Jucee_Umbrella.webm', 1018600, NULL, NULL, NULL, 1, 1357916388, 1, 1357916388, ''),
(116, 1, 'Jucee_Umbrella.ogv', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Jucee_Umbrella.ogv', 'video/ogg', 'Jucee_Umbrella.ogv', 2112926, NULL, NULL, NULL, 1, 1357916399, 1, 1357916399, ''),
(117, 1, 'Jucee_Umbrella.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/Jucee_Umbrella.jpg', 'image/jpeg', 'Jucee_Umbrella.jpg', 32654, NULL, NULL, NULL, 1, 1357916409, 1, 1357916409, '360 640'),
(118, 1, 'two_meadowvale.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/two_meadowvale.jpg', 'image/jpeg', 'two_meadowvale.jpg', 99451, NULL, NULL, NULL, 1, 1357919243, 1, 1357919243, '571 640'),
(119, 1, 'tcx-combined.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/tcx-combined.jpg', 'image/jpeg', 'tcx-combined.jpg', 228420, NULL, NULL, NULL, 1, 1357920314, 1, 1357920314, '1402 640'),
(120, 1, 'pets.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/pets.jpg', 'image/jpeg', 'pets.jpg', 175553, NULL, NULL, NULL, 1, 1357920470, 1, 1357920470, '1226 640'),
(121, 1, 'jucee.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/jucee.jpg', 'image/jpeg', 'jucee.jpg', 242879, NULL, NULL, NULL, 1, 1357920705, 1, 1357920705, '1406 640'),
(122, 1, 'pah-banners.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/pah-banners.jpg', 'image/jpeg', 'pah-banners.jpg', 165735, NULL, NULL, NULL, 1, 1357921769, 1, 1357921769, '894 640'),
(123, 1, 'ryman.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/ryman.jpg', 'image/jpeg', 'ryman.jpg', 223510, NULL, NULL, NULL, 1, 1358159660, 1, 1358159660, '1472 640'),
(124, 1, 'pound-bakery.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/pound-bakery.jpg', 'image/jpeg', 'pound-bakery.jpg', 255415, NULL, NULL, NULL, 1, 1358159990, 1, 1358159990, '1500 640'),
(125, 1, 'mixed-banners.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/mixed-banners.jpg', 'image/jpeg', 'mixed-banners.jpg', 121485, NULL, NULL, NULL, 1, 1358160461, 1, 1358160461, '788 640'),
(126, 1, '9.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/9.png', 'image/png', '9.png', 2593, NULL, NULL, NULL, 1, 1381410219, 1, 1381410219, '320 470'),
(127, 1, '10.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/10.png', 'image/png', '10.png', 2512, NULL, NULL, NULL, 1, 1381410219, 1, 1381410219, '320 470'),
(128, 1, '8.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/8.png', 'image/png', '8.png', 2582, NULL, NULL, NULL, 1, 1381410219, 1, 1381410219, '320 470'),
(129, 1, '6.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/6.png', 'image/png', '6.png', 2538, NULL, NULL, NULL, 1, 1381410220, 1, 1381410220, '320 470'),
(130, 1, '7.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/7.png', 'image/png', '7.png', 2220, NULL, NULL, NULL, 1, 1381410220, 1, 1381410220, '320 470'),
(131, 1, '5.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/5.png', 'image/png', '5.png', 2371, NULL, NULL, NULL, 1, 1381410220, 1, 1381410220, '320 470'),
(132, 1, '4.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/4.png', 'image/png', '4.png', 2116, NULL, NULL, NULL, 1, 1381410220, 1, 1381410220, '320 470'),
(133, 1, '2.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/2.png', 'image/png', '2.png', 2402, NULL, NULL, NULL, 1, 1381410220, 1, 1381410220, '320 470'),
(134, 1, '3.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/3.png', 'image/png', '3.png', 2578, NULL, NULL, NULL, 1, 1381410220, 1, 1381410220, '320 470'),
(135, 1, '1.png', 7, '/Users/kieranduff/Sites/shootthemoon/stm-0100001-web/www/images/rotating_image_panels/1.png', 'image/png', '1.png', 1887, NULL, NULL, NULL, 1, 1381410220, 1, 1381410220, '320 470');

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_categories`
--

CREATE TABLE IF NOT EXISTS `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_dimensions`
--

CREATE TABLE IF NOT EXISTS `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_watermarks`
--

CREATE TABLE IF NOT EXISTS `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_composer_layouts`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_composer_layouts` (
  `composer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composer_data` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `preview` char(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`composer_id`),
  KEY `preview` (`preview`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_composer_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_composer_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `template_name` varchar(150) NOT NULL DEFAULT 'default',
  `template_label` varchar(150) NOT NULL DEFAULT 'default',
  `template_description` text,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_data` text,
  `param_data` text,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fields`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `field_name` varchar(150) NOT NULL DEFAULT 'default',
  `field_label` varchar(150) NOT NULL DEFAULT 'default',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `settings` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'n',
  `submissions_page` char(1) NOT NULL DEFAULT 'y',
  `moderation_page` char(1) NOT NULL DEFAULT 'y',
  `composer_use` char(1) NOT NULL DEFAULT 'y',
  `field_description` text,
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_freeform_fields`
--

INSERT INTO `exp_freeform_fields` (`field_id`, `site_id`, `field_name`, `field_label`, `field_type`, `settings`, `author_id`, `entry_date`, `edit_date`, `required`, `submissions_page`, `moderation_page`, `composer_use`, `field_description`) VALUES
(1, 1, 'first_name', 'First Name', 'text', '{"field_length":150,"field_content_type":"any"}', 1, 1352298407, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s first name.'),
(2, 1, 'last_name', 'Last Name', 'text', '{"field_length":150,"field_content_type":"any"}', 1, 1352298407, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s last name.'),
(3, 1, 'email', 'Email', 'text', '{"field_length":150,"field_content_type":"email"}', 1, 1352298407, 0, 'n', 'y', 'y', 'y', 'A basic email field for collecting stuff like an email address.'),
(4, 1, 'user_message', 'Message', 'textarea', '{"field_ta_rows":6}', 1, 1352298407, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s message.');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fieldtypes` (
  `fieldtype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldtype_name` varchar(250) DEFAULT NULL,
  `settings` text,
  `default_field` char(1) NOT NULL DEFAULT 'n',
  `version` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`fieldtype_id`),
  KEY `fieldtype_name` (`fieldtype_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_freeform_fieldtypes`
--

INSERT INTO `exp_freeform_fieldtypes` (`fieldtype_id`, `fieldtype_name`, `settings`, `default_field`, `version`) VALUES
(1, 'file_upload', '[]', 'n', '4.0.7'),
(2, 'mailinglist', '[]', 'n', '4.0.7'),
(3, 'text', '[]', 'n', '4.0.7'),
(4, 'textarea', '[]', 'n', '4.0.7');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_file_uploads`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_file_uploads` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `server_path` varchar(750) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `filesize` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `extension` (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_forms`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_forms` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_name` varchar(150) NOT NULL DEFAULT 'default',
  `form_label` varchar(150) NOT NULL DEFAULT 'default',
  `default_status` varchar(150) NOT NULL DEFAULT 'default',
  `notify_user` char(1) NOT NULL DEFAULT 'n',
  `notify_admin` char(1) NOT NULL DEFAULT 'n',
  `user_email_field` varchar(150) NOT NULL DEFAULT '',
  `user_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_email` text,
  `form_description` text,
  `field_ids` text,
  `field_order` text,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `composer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`form_id`),
  KEY `form_name` (`form_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_freeform_forms`
--

INSERT INTO `exp_freeform_forms` (`form_id`, `site_id`, `form_name`, `form_label`, `default_status`, `notify_user`, `notify_admin`, `user_email_field`, `user_notification_id`, `admin_notification_id`, `admin_notification_email`, `form_description`, `field_ids`, `field_order`, `template_id`, `composer_id`, `author_id`, `entry_date`, `edit_date`, `settings`) VALUES
(2, 1, 'enquiries', 'Enquiries', 'pending', 'n', 'n', '', 0, 0, 'martin@shoot-the-moon.co.uk', '', '', '', 0, 0, 1, 1352298607, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_form_entries_2`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_form_entries_2` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `exp_freeform_form_entries_2`
--

INSERT INTO `exp_freeform_form_entries_2` (`entry_id`, `site_id`, `author_id`, `complete`, `ip_address`, `entry_date`, `edit_date`, `status`) VALUES
(1, 1, 1, 'y', '127.0.0.1', 1352298861, 0, 'pending'),
(2, 1, 1, 'y', '127.0.0.1', 1352298940, 0, 'pending'),
(3, 1, 1, 'y', '127.0.0.1', 1352299176, 0, 'pending'),
(4, 1, 1, 'y', '127.0.0.1', 1352299214, 0, 'pending'),
(5, 1, 1, 'y', '127.0.0.1', 1352307421, 0, 'pending'),
(6, 1, 1, 'y', '127.0.0.1', 1352307422, 0, 'pending'),
(7, 1, 1, 'y', '127.0.0.1', 1352307653, 0, 'pending'),
(8, 1, 1, 'y', '127.0.0.1', 1352307654, 0, 'pending'),
(9, 1, 0, 'y', '91.232.96.36', 1358583010, 0, 'pending'),
(10, 1, 0, 'y', '94.102.52.178', 1358883927, 0, 'pending'),
(11, 1, 0, 'y', '92.42.148.196', 1358939720, 0, 'pending'),
(12, 1, 0, 'y', '94.102.52.176', 1359088667, 0, 'pending'),
(13, 1, 0, 'y', '188.143.232.211', 1359402489, 0, 'pending'),
(14, 1, 0, 'y', '89.248.165.134', 1359949420, 0, 'pending'),
(15, 1, 0, 'y', '87.83.17.18', 1360081370, 0, 'pending'),
(16, 1, 0, 'y', '89.248.165.134', 1360114668, 0, 'pending'),
(17, 1, 0, 'y', '78.149.117.249', 1360249007, 0, 'pending'),
(18, 1, 0, 'y', '89.248.165.134', 1360340476, 0, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_multipage_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_multipage_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit` char(1) NOT NULL DEFAULT 'n',
  `data` text,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`),
  KEY `ip_address` (`ip_address`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_notification_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_notification_templates` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `notification_name` varchar(150) NOT NULL DEFAULT 'default',
  `notification_label` varchar(150) NOT NULL DEFAULT 'default',
  `notification_description` text,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `allow_html` char(1) NOT NULL DEFAULT 'n',
  `from_name` varchar(150) NOT NULL DEFAULT '',
  `from_email` varchar(250) NOT NULL DEFAULT '',
  `reply_to_email` varchar(250) NOT NULL DEFAULT '',
  `email_subject` varchar(128) NOT NULL DEFAULT 'default',
  `include_attachments` char(1) NOT NULL DEFAULT 'n',
  `template_data` text,
  PRIMARY KEY (`notification_id`),
  KEY `notification_name` (`notification_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_params`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=589 ;

--
-- Dumping data for table `exp_freeform_params`
--

INSERT INTO `exp_freeform_params` (`params_id`, `entry_date`, `data`) VALUES
(588, 1392378346, '{"form_id":"2","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"get-in-touch","inline_error_return":"get-in-touch","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"name|email|contact_number|message","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"martin@shoot-the-moon.co.uk","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":false,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_preferences`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) DEFAULT NULL,
  `preference_value` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`preference_id`),
  KEY `preference_name` (`preference_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_freeform_preferences`
--

INSERT INTO `exp_freeform_preferences` (`preference_id`, `preference_name`, `preference_value`, `site_id`) VALUES
(1, 'ffp', 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_user_email`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  `email_addresses` text,
  PRIMARY KEY (`email_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_global_variables`
--

CREATE TABLE IF NOT EXISTS `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `exp_global_variables`
--

INSERT INTO `exp_global_variables` (`variable_id`, `site_id`, `variable_name`, `variable_data`, `sync_time`) VALUES
(1, 1, 'lv_thumbs_nav', '		<ul id="switch" class="pie clearfix">\n		\n			{if segment_1=="creative"}\n			{exp:channel:entries channel="packaging|branding|literature|point_of_sale" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="0"}\n			{cf_portfolio_images}\n			<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n			{/cf_portfolio_images}\n			{/exp:channel:entries}\n			{/if}\n			\n			{if segment_1=="photography" AND segment_2==""}\n			{exp:channel:entries channel="food|product|location|set" disable="{lv_disable_basic}" dynamic="off" limit="6"}\n			{cf_portfolio_images}\n			<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n			{/cf_portfolio_images}\n			{/exp:channel:entries}\n			{/if}\n			\n			{if segment_1=="digital" AND segment_2==""}\n			{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6"}\n			{cf_portfolio_images}\n			<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n			{/cf_portfolio_images}\n			{/exp:channel:entries}\n			{/if}\n			\n		</ul>\n', '2013-09-24 10:28:54'),
(2, 1, 'lv_sidebar_address', '				<p class="header-info">\n					<span>t/ 0161 205 3311</span><br>\n					<span><a class="email" href="mailto:hello@shoot-the-moon.co.uk">hello@shoot-the-moon.co.uk</a></span><br>\n					<span>Concept House, Naval Street Manchester M4 6AX</span>\n				</p>', '2013-10-09 10:15:54'),
(3, 1, 'lv_image_matrix', '<div id="matrix">\n	<ul>\n	{cf_rotating_image_blocks limit="3"}\n	{if count == 1}\n	<div class="slide-img">\n		{exp:ce_img:single src="{url}" width="440" height="274" crop="yes" allow_scale_larger="yes"}\n	</div>\n	<ul id="slides">\n	{/if}\n	<li>{exp:ce_img:single src="{url}" width="440" height="274" crop="yes" allow_scale_larger="yes"}</li>\n	{if count == total_files}\n	</ul>\n	{/if}\n	{/cf_rotating_image_blocks}\n	</ul>\n</div>', '2013-09-24 10:28:54'),
(4, 1, 'lv_header', '<div class="outwrap">\n	<header>\n		<div class="container nav-container">\n			<nav class=" sixteen columns alpha omega">\n				<input type="checkbox" id="toggle">\n				<label for="toggle" class="mini-nav-button">&#9776;</label>\n				<div class="social four columns alpha omega">\n					<ul>\n		        		<li class="fb"><a href="">Facebook</a></li>\n		        		<li class="tw"><a href="">Twitter</a></li>\n		        		<li class="in"><a href="">Linkedin</a></li>\n		    		</ul>\n		    	</div>\n		    	<ul class="twelve columns nav-main alpha omega">\n		    		<li class="home here ir"><a href="/"><img src="resource/stat/homeicn.png" alt=""></a></li><li><a href="/our-thinking">Our thinking</a></li><li><a href="/creative">Creative</a></li><li><a href="/photography">Photography</a></li><li><a href="/digital">Digital</a></li><li class="sub-nav"><a href="/get-in-touch">Get in touch</a></li><li class="sub-nav"><a href="http://files.shoot-the-moon.co.uk/">Client Login</a></li>\n		    	</ul>\n			</nav>\n		</div>\n	</header>', '2013-10-09 10:22:30'),
(5, 1, 'lv_footer', '	<footer class="container">\n		<p class="eight columns alpha omega footer-info">\n			<span>t/ 0161 205 3311</span><br>\n			<span><a class="email" href="mailto:hello@shoot-the-moon.co.uk">hello@shoot-the-moon.co.uk</a></span><br>\n			<span>Concept House, Naval Street Manchester M4 6AX</span>\n		</p>\n		<div class="eight columns alpha omega footer-image"><a href=""><img src="resource/stat/rar.gif" alt="RAR Recommended" ></a></div>\n	</footer>\n</div>', '2013-10-09 10:19:44'),
(6, 1, 'lv_folio_nav', '					<ul>\n						{exp:structure:nav start_from="/{segment_1}" include_ul="no"}\n					</ul>', '2013-10-09 10:54:14'),
(7, 1, 'lv_doc_header', '<!DOCTYPE html>\n\n\n<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->\n<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->\n<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->\n<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->\n\n\n{exp:channel:entries\n        disable="categories|member_data|pagination"\n        dynamic="yes"\n        limit="1"\n        require_entry="yes"\n    }\n    {if no_results}{redirect=''/''}{/if}\n\n<head>\n\n    <!-- Basic Page Needs\n  ================================================== -->\n    <meta charset="{charset}">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">\n    <title>Shoot the Moon | {if browser_title}{browser_title}{if:else}{title}{/if}</title>\n    <meta name="description" content="{meta_description}">\n    <meta name="copyright" content="Copyright {current_time format=''%Y''} Shoot the moon. All rights reserved.">\n    <meta name="viewport" content="width=device-width , initial-scale=1.0">\n    <meta name="author" content="{site_name}">\n\n    <!-- Mobile Specific Metas\n  ================================================== -->\n    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">\n\n    <!-- CSS\n  ================================================== -->\n    <link rel="stylesheet" href="/resource/css/root.css">\n\n    <!--[if lt IE 9]>\n        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>\n    <![endif]-->\n\n    <!-- Favicons\n    ================================================== -->\n        \n{/exp:channel:entries}', '2013-10-09 10:03:34'),
(8, 1, 'lv_chrome_frame', '<!--[if lt IE 7]>\n            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>\n        <![endif]-->', '2013-09-24 10:28:54'),
(9, 1, 'lv_blockquote', '			{exp:channel:entries channel="testimonials" disable="{lv_disable_basic}" dynamic="off"}\n			{testimonials sort="random" limit="1"}\n			<blockquote>\n				<p>{cf_mx_testimonial}</p>\n				<cite>{cf_mx_name}, {cf_mx_company}</cite>\n			</blockquote>\n			{/testimonials}\n			{/exp:channel:entries}', '2013-09-24 10:28:54'),
(10, 1, 'lv_base_scripts', '        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>\n        <script>window.jQuery || document.write(''<script src="/resource/js/vendor/jquery-1.8.2.min.js"><\\/script>'')</script>\n        <script src="/resource/js/hammer.js"></script>\n        <script src="/resource/js/plugins.js"></script>\n                <script src="/resource/js/vendor/jquery.scrollTo-1.4.3.1.js"></script>\n                <script src="/resource/js/vendor/jquery.easing.1.3.js"></script>\n                <script src="/resource/js/vendor/stickysidebar.jquery.min.js"></script>\n        <script src="/resource/js/main.js"></script>\n        <script src="/resource/js/vendor/picturefill-master/picturefill.js"></script>\n\n        <!-- Google Analytics: change UA-XXXXX-X to be your site''s ID. -->\n        <script>\n            var _gaq=[[''_setAccount'',''UA-XXXXX-X''],[''_trackPageview'']];\n            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n            g.src=(''https:''==location.protocol?''//ssl'':''//www'')+''.google-analytics.com/ga.js'';\n            s.parentNode.insertBefore(g,s)}(document,''script''));\n        </script>\n', '2013-10-21 10:03:56'),
(11, 1, 'lv_disable_basic', 'categories|category_fields|member_data|pagination', '2013-09-24 10:28:54'),
(12, 1, 'lv_brand', '{if segment_1 =="creative"}\n	<img src="/resource/stat/stm-creative.png" alt="Shoot the Moon Creative" />\n{if:elseif segment_1 =="photography"}\n 	<img src="/resource/stat/stm-photography.png" alt="Shoot the Moon Creative" />\n{if:elseif segment_1 =="digital"}\n	<img src="/resource/stat/stm-digital.png" alt="Shoot the Moon Creative" />\n{if:else}\n	<img src="/resource/stat/stm-consultancy.png" alt="Shoot the Moon Creative" />\n{/if}', '2013-10-09 12:23:58');

-- --------------------------------------------------------

--
-- Table structure for table `exp_gmaps_cache`
--

CREATE TABLE IF NOT EXISTS `exp_gmaps_cache` (
  `cache_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(80) NOT NULL DEFAULT '',
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `date` varchar(35) NOT NULL DEFAULT '',
  `geocoder` varchar(15) NOT NULL DEFAULT '',
  `result_object` text,
  PRIMARY KEY (`cache_id`),
  KEY `lat` (`lat`),
  KEY `lng` (`lng`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `exp_gmaps_cache`
--

INSERT INTO `exp_gmaps_cache` (`cache_id`, `address`, `lat`, `lng`, `date`, `geocoder`, `result_object`) VALUES
(1, 'heerde', 52.389042, 6.038935, '1352391516', 'google_maps', 'O:24:"Geocoder\\Result\\Geocoded":14:{s:11:"\0*\0latitude";d:52.38903999999998717385096824727952480316162109375;s:12:"\0*\0longitude";d:6.038934999999998609609974664635956287384033203125;s:9:"\0*\0bounds";a:4:{s:5:"south";d:52.3565291999999971039869706146419048309326171875;s:4:"west";d:5.95096869999999977807192408363334834575653076171875;s:5:"north";d:52.41770930000001271764631383121013641357421875;s:4:"east";d:6.09050609999999981170049068168736994266510009765625;}s:15:"\0*\0streetNumber";N;s:13:"\0*\0streetName";N;s:15:"\0*\0cityDistrict";N;s:7:"\0*\0city";s:6:"Heerde";s:10:"\0*\0zipcode";s:4:"8181";s:9:"\0*\0county";s:6:"Heerde";s:9:"\0*\0region";s:10:"Gelderland";s:13:"\0*\0regionCode";s:2:"GE";s:10:"\0*\0country";s:15:"The Netherlands";s:14:"\0*\0countryCode";s:2:"nl";s:11:"\0*\0timezone";N;}'),
(2, 'manchester', 53.479252, -2.247926, '1352391731', 'google_maps', 'O:24:"Geocoder\\Result\\Geocoded":14:{s:11:"\0*\0latitude";d:53.47925099999999787314663990400731563568115234375;s:12:"\0*\0longitude";d:-2.247926000000000090750518211279995739459991455078125;s:9:"\0*\0bounds";a:4:{s:5:"south";d:53.39994949999999818146534380502998828887939453125;s:4:"west";d:-2.300096900000000221808704736758954823017120361328125;s:5:"north";d:53.54458790000000334430296788923442363739013671875;s:4:"east";d:-2.147087500000000037658764995285309851169586181640625;}s:15:"\0*\0streetNumber";N;s:13:"\0*\0streetName";N;s:15:"\0*\0cityDistrict";N;s:7:"\0*\0city";s:10:"Manchester";s:10:"\0*\0zipcode";N;s:9:"\0*\0county";s:18:"Greater Manchester";s:9:"\0*\0region";s:7:"England";s:13:"\0*\0regionCode";s:7:"ENGLAND";s:10:"\0*\0country";s:14:"United Kingdom";s:14:"\0*\0countryCode";s:2:"gb";s:11:"\0*\0timezone";N;}'),
(19, 'M46AX', 53.485756, -2.226374, '1392378348', 'google_maps', 'O:24:"Geocoder\\Result\\Geocoded":14:{s:11:"\0*\0latitude";d:53.485754700000001093940227292478084564208984375;s:12:"\0*\0longitude";d:-2.226374199999999969890041029429994523525238037109375;s:9:"\0*\0bounds";a:4:{s:5:"south";d:53.4853422999999992271114024333655834197998046875;s:4:"west";d:-2.2271263000000001142097971751354634761810302734375;s:5:"north";d:53.486074000000002115484676323831081390380859375;s:4:"east";d:-2.225658399999999925711335890810005366802215576171875;}s:15:"\0*\0streetNumber";N;s:13:"\0*\0streetName";N;s:15:"\0*\0cityDistrict";s:7:"Ancoats";s:7:"\0*\0city";s:10:"Manchester";s:10:"\0*\0zipcode";s:6:"M4 6AX";s:9:"\0*\0county";s:18:"Greater Manchester";s:9:"\0*\0region";N;s:13:"\0*\0regionCode";N;s:10:"\0*\0country";s:14:"United Kingdom";s:14:"\0*\0countryCode";s:2:"gb";s:11:"\0*\0timezone";N;}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_html_buttons`
--

CREATE TABLE IF NOT EXISTS `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_html_buttons`
--

INSERT INTO `exp_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`) VALUES
(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(4, 1, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(5, 1, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img');

-- --------------------------------------------------------

--
-- Table structure for table `exp_layout_publish`
--

CREATE TABLE IF NOT EXISTS `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=142 ;

--
-- Dumping data for table `exp_layout_publish`
--

INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(21, 1, 1, 4, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:5:{s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}}}'),
(22, 1, 6, 4, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:5:{s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}}}'),
(23, 1, 1, 2, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(24, 1, 6, 2, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(74, 1, 6, 6, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(89, 1, 1, 18, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(90, 1, 6, 18, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(98, 1, 6, 3, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(99, 1, 1, 7, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(100, 1, 6, 7, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(101, 1, 1, 8, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(102, 1, 6, 8, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(103, 1, 1, 9, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(104, 1, 6, 9, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(105, 1, 1, 10, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(106, 1, 6, 10, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(107, 1, 1, 14, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');
INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(108, 1, 6, 14, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(109, 1, 1, 12, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(110, 1, 6, 12, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(112, 1, 6, 16, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(113, 1, 1, 15, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(114, 1, 6, 15, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(115, 1, 1, 17, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(116, 1, 6, 17, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(118, 1, 6, 11, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(120, 1, 6, 1, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(124, 1, 6, 20, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(125, 1, 1, 22, 'a:5:{s:7:"publish";a:3:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(126, 1, 6, 22, 'a:5:{s:7:"publish";a:3:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:4:"true";s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(127, 1, 1, 19, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(128, 1, 6, 19, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(130, 1, 6, 21, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(131, 1, 1, 23, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(132, 1, 6, 23, 'a:5:{s:7:"publish";a:13:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');
INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(135, 1, 1, 20, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(136, 1, 1, 6, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(137, 1, 1, 3, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(138, 1, 1, 21, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(139, 1, 1, 1, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(140, 1, 1, 11, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(141, 1, 1, 16, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:1;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:3;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:7:{s:10:"_tab_label";s:7:"Options";s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:5:{s:10:"_tab_label";s:3:"SEO";i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_reorder_orders`
--

CREATE TABLE IF NOT EXISTS `exp_low_reorder_orders` (
  `set_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  `sort_order` text,
  PRIMARY KEY (`set_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exp_low_reorder_orders`
--

INSERT INTO `exp_low_reorder_orders` (`set_id`, `cat_id`, `sort_order`) VALUES
(1, 0, '|171|200|186|196|195|192|191|190|187|185|184|180|179|181|178|193|175|173|172|174|170|169|168|167|165|166|164|162|163|158|159|157|155|156|189|154|152|153|151|149|148|150|145|147|143|144|142|116|140|141|137|138|139|133|134|130|131|132|127|128|129|126|124|125|120|118|114|115|50|49|194|48|47|198|197|43|41|40|39|38|36|35|34|33|199|14|15|207|208|209|210|213|214|216|221|222|223|224|'),
(2, 0, '|190|187|185|186|184|189|116|118|114|115|39|38|'),
(3, 0, '|173|172|174|171|170|133|134|130|131|132|127|128|129|126|124|125|41|36|35|34|33|'),
(4, 0, '|180|179|181|178|175|138|120|40|141|221|222|223|');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_reorder_sets`
--

CREATE TABLE IF NOT EXISTS `exp_low_reorder_sets` (
  `set_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `set_label` varchar(100) NOT NULL,
  `set_notes` text NOT NULL,
  `new_entries` enum('append','prepend') NOT NULL DEFAULT 'append',
  `clear_cache` enum('y','n') NOT NULL DEFAULT 'y',
  `channels` varchar(255) NOT NULL,
  `cat_option` enum('all','some','one') NOT NULL DEFAULT 'all',
  `cat_groups` varchar(255) NOT NULL,
  `parameters` text NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`set_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_low_reorder_sets`
--

INSERT INTO `exp_low_reorder_sets` (`set_id`, `site_id`, `set_label`, `set_notes`, `new_entries`, `clear_cache`, `channels`, `cat_option`, `cat_groups`, `parameters`, `permissions`) VALUES
(1, 1, 'Portfolio', '', 'append', 'n', '|15|2|3|16|19|7|18|12|10|4|8|14|20|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6MTIwOiJicmFuZGluZ3xjcmVhdGl2ZXxkaWdpdGFsfGRpcmVjdC1tYWlsfGVjcm0tZW1haWx8Zm9vZHxtb2JpbGV8cGFja2FnaW5nfHBlcnNvbmFsfHBob3RvZ3JhcGh5fHByb2R1Y3R8c3RvcmUtY29tbXN8d2Vic2l0ZXMiO30', 'YToxOntpOjY7czoxOiIxIjt9'),
(2, 1, 'Branding', '', 'append', 'n', '|15|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6ODoiYnJhbmRpbmciO30', 'YToxOntpOjY7czoxOiIxIjt9'),
(3, 1, 'Packaging', '', 'append', 'n', '|12|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6OToicGFja2FnaW5nIjt9', 'YToxOntpOjY7czoxOiIwIjt9'),
(4, 1, 'Direct Mail', '', 'append', 'n', '|16|', 'all', '', 'YToyOntzOjY6InN0YXR1cyI7czo0OiJvcGVuIjtzOjc6ImNoYW5uZWwiO3M6MTE6ImRpcmVjdC1tYWlsIjt9', 'YToxOntpOjY7czoxOiIwIjt9');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_variables`
--

CREATE TABLE IF NOT EXISTS `exp_low_variables` (
  `variable_id` int(6) unsigned NOT NULL,
  `group_id` int(6) unsigned NOT NULL,
  `variable_label` varchar(100) NOT NULL,
  `variable_notes` text NOT NULL,
  `variable_type` varchar(50) NOT NULL,
  `variable_settings` text NOT NULL,
  `variable_order` int(4) unsigned NOT NULL,
  `early_parsing` char(1) NOT NULL DEFAULT 'n',
  `is_hidden` char(1) NOT NULL DEFAULT 'n',
  `save_as_file` char(1) NOT NULL DEFAULT 'n',
  `edit_date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`variable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exp_low_variables`
--

INSERT INTO `exp_low_variables` (`variable_id`, `group_id`, `variable_label`, `variable_notes`, `variable_type`, `variable_settings`, `variable_order`, `early_parsing`, `is_hidden`, `save_as_file`, `edit_date`) VALUES
(1, 1, 'Sidebar Thumbs Nav', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 1, 'y', 'n', 'y', 1352385416),
(2, 1, 'Sidebar Address', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 2, 'y', 'n', 'y', 1351250592),
(3, 1, 'Image Matrix', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 3, 'y', 'n', 'y', 1351526727),
(4, 1, 'Header', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 4, 'y', 'n', 'y', 1351522302),
(5, 1, '', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 5, 'y', 'n', 'y', 1351522785),
(6, 1, '', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 6, 'y', 'n', 'y', 1352369983),
(7, 1, 'Doc Header', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 7, 'y', 'n', 'y', 1351250592),
(8, 1, 'Chrome Frame', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 8, 'y', 'n', 'n', 1351250512),
(9, 1, 'Blockquotes', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 9, 'y', 'n', 'y', 1351522192),
(10, 1, 'Base Scripts', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 10, 'y', 'n', 'y', 1351250592),
(11, 2, 'Basic entries disable paremeters', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 1, 'n', 'n', 'n', 1351245707),
(12, 1, '', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', 11, 'y', 'n', 'y', 1381321529);

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_variable_groups`
--

CREATE TABLE IF NOT EXISTS `exp_low_variable_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(6) unsigned NOT NULL,
  `group_label` varchar(100) NOT NULL,
  `group_notes` text NOT NULL,
  `group_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_low_variable_groups`
--

INSERT INTO `exp_low_variable_groups` (`group_id`, `site_id`, `group_label`, `group_notes`, `group_order`) VALUES
(1, 1, 'Repeating Elements', '', 0),
(2, 1, 'EE Snippets', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_cols`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `exp_matrix_cols`
--

INSERT INTO `exp_matrix_cols` (`col_id`, `site_id`, `field_id`, `var_id`, `col_name`, `col_label`, `col_instructions`, `col_type`, `col_required`, `col_search`, `col_order`, `col_width`, `col_settings`) VALUES
(1, 1, 2, NULL, 'cf_mx_portfolio_image', 'Image', '', 'assets', 'n', 'n', 0, '10%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjQ6e2k6MDtzOjE6IjQiO2k6MTtzOjE6IjIiO2k6MjtzOjE6IjMiO2k6MztzOjE6IjEiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0='),
(2, 1, 2, NULL, 'cf_mx_supporting_text', 'Supporting Text', '', 'wygwam', 'n', 'n', 1, '70%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30='),
(10, 1, 6, NULL, 'cf_mx_studio_image', 'Image', '', 'assets', 'n', 'n', 0, '10%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjgiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0='),
(11, 1, 6, NULL, 'cf_mx_supporting_text', 'Supporting Text', '', 'wygwam', 'n', 'n', 1, '70%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30='),
(12, 1, 6, NULL, 'cf_mx_discipline', 'Discipline', '', 'pt_dropdown', 'n', 'n', 2, '20%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mzp7czo2OiJQZW9wbGUiO3M6NjoiUGVvcGxlIjtzOjg6IkludGVyaW9yIjtzOjg6IkludGVyaW9yIjtzOjQ6IldvcmsiO3M6NDoiV29yayI7fX0='),
(13, 1, 7, NULL, 'cf_mx_name', 'Name', '', 'text', 'n', 'n', 0, '15%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(14, 1, 7, NULL, 'cf_mx_company', 'Company', '', 'text', 'n', 'n', 1, '15%', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(15, 1, 7, NULL, 'cf_mx_testimonial', 'Testimonial Text', '', 'text', 'n', 'n', 2, '70%', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(16, 1, 9, NULL, 'version_m4v', 'M4v Version', '', 'assets', 'n', 'n', 0, '33%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo0OiJsaXN0IjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO319'),
(17, 1, 9, NULL, 'version_webM', 'WebM Version', '', 'assets', 'n', 'n', 1, '', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo0OiJsaXN0IjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO319'),
(18, 1, 9, NULL, 'version_ogv', 'OGV Version', '', 'assets', 'n', 'n', 2, '', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo0OiJsaXN0IjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO319'),
(19, 1, 9, NULL, 'start_image', 'Start Image', '', 'assets', 'n', 'n', 3, '', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_data`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` text,
  `col_id_13` text,
  `col_id_14` text,
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=469 ;

--
-- Dumping data for table `exp_matrix_data`
--

INSERT INTO `exp_matrix_data` (`row_id`, `site_id`, `entry_id`, `field_id`, `var_id`, `row_order`, `col_id_1`, `col_id_2`, `col_id_10`, `col_id_11`, `col_id_12`, `col_id_13`, `col_id_14`, `col_id_15`, `col_id_16`, `col_id_17`, `col_id_18`, `col_id_19`) VALUES
(3, 1, 4, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 1, 1, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 1, 10, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 1, 2, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 1, 11, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 1, 12, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 1, 13, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 1, 3, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 1, 14, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 1, 15, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 1, 16, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 1, 17, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 1, 18, 2, NULL, 1, '', '<p>\n	It has been said that astronomy is a humbling and character-building experience. There is perhaps no better demonstration of the folly of human conceits than this distant image of our tiny world. To me, it underscores our responsibility to deal more kindly with one another, and to preserve and cherish the pale blue dot, the only home we&#39;ve ever known.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 1, 18, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 1, 19, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 1, 20, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 1, 21, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 1, 18, 2, NULL, 2, '', '<p>\n	Competent means we will never take anything for granted. We will never be found short in our knowledge and in our skills. Mission Control will be perfect. When you leave this meeting today you will go to your office and the first thing you will do there is to write &#39;Tough and Competent&#39; on your blackboards.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 1, 19, 2, NULL, 1, '', '<p>\n	The size and age of the Cosmos are beyond ordinary human understanding. Lost somewhere between immensity and eternity is our tiny planetary home.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 1, 19, 2, NULL, 2, '', '<p>\n	The Earth is a very small stage in a vast cosmic arena. Think of the rivers of blood spilled by all those generals and emperors so that, in glory and triumph, they could become the momentary masters of a fraction of a dot.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 1, 20, 2, NULL, 1, '', '<p>\n	It&#39;s just mind-blowingly awesome. I apologize, and I wish I was more articulate, but it&#39;s hard to be articulate when your mind&#39;s blown&mdash;but in a very good way.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 1, 20, 2, NULL, 2, '', '<p>\n	First, I believe that this nation should commit itself to achieving the goal, before this decade is out, of landing a man on the moon and returning him safely to the earth.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 1, 5, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 1, 31, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 1, 31, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'Lorna Culican', 'Sayers the Bakers', 'Innovative, friendly and totally committed to providing the best designs and ideas — just a few words to describe the team at Shoot the Moon. We can be confident that they will do all they can to ensure the project reaches us on time, will be of the highest standard, and will be to budget.', NULL, NULL, NULL, NULL),
(89, 1, 31, 7, NULL, 2, NULL, NULL, NULL, NULL, NULL, 'Karen Scott', 'Macphie of Glenbervie', 'Brace yourself for jaw-dropping design delivered on-time and on-budget. They are a very friendly, professional team, we enjoy working with them.', NULL, NULL, NULL, NULL),
(90, 1, 31, 7, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'Natalie Hughes', 'Pets at Home', 'We have worked with Shoot the Moon on a variety of projects over the last three years — they have produced some excellent creative for us. Faced with consistently tight deadlines, their resourcefulness really shows through. This, combined with their broad experience makes them a great team to work with.', NULL, NULL, NULL, NULL),
(91, 1, 31, 7, NULL, 4, NULL, NULL, NULL, NULL, NULL, 'Paul Broster', 'Meadow Vale Foods', 'Their creative, technical knowledge and understanding of the food industry makes them very easy to work with and has delivered some excellent results. The STM team have a real enthusiasm for getting it right — great creative, pace and value for money.', NULL, NULL, NULL, NULL),
(92, 1, 32, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 1, 33, 2, NULL, 1, '{filedir_2}packaging/Fish-mongers.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 1, 33, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 1, 34, 2, NULL, 1, '{filedir_2}packaging/Humdingers.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 1, 34, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 1, 35, 2, NULL, 1, '{filedir_2}packaging/count-down.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 1, 35, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 1, 36, 2, NULL, 1, '{filedir_2}packaging/Advanced-nut-dog.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 1, 36, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 1, 37, 2, NULL, 1, '{filedir_2}stm-brand-portfolio-2012-25.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 1, 37, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 1, 38, 2, NULL, 1, '{filedir_2}Brand/Hewbys.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 1, 38, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 1, 39, 2, NULL, 1, '{filedir_2}Brand/Harbour-Smokehouse.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 1, 39, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 1, 40, 2, NULL, 1, '{filedir_2}dm/Dissoto.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 1, 40, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 1, 41, 2, NULL, 1, '{filedir_2}dm/3-peaks.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 1, 41, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 1, 42, 2, NULL, 1, '{filedir_2}store-comms/Bird.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 1, 42, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 1, 43, 2, NULL, 1, '{filedir_2}store-comms/Advanced-nutrition.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 1, 43, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 1, 44, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0220.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 1, 45, 6, NULL, 1, NULL, NULL, '{filedir_8}studio_3.jpg', '', 'Interior', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 1, 46, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0283.JPG', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 1, 31, 7, NULL, 5, NULL, NULL, NULL, NULL, NULL, 'Theo Van Der Lugt', 'Alpha LSG', 'STM are a pleasure to work with: creative, responsive and very down-to-earth. Straight talking and a focus on the objectives, rather than the mechanics."', NULL, NULL, NULL, NULL),
(119, 1, 31, 7, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'Stephanie Mansfield', 'Lakeland Limited', 'Lakeland has worked with Shoot The Moon for many years as we can always rely on them to pull out all the stops to ensure we meet our deadlines... even if they are very tight! I''ve always found the team friendly, helpful and professional. Shoot The Moon are a pleasure to work with and we look forward to producing many more publications with them.', NULL, NULL, NULL, NULL),
(120, 1, 31, 7, NULL, 7, NULL, NULL, NULL, NULL, NULL, ' Paul Lowings', 'Lakeland Limited', 'We have enjoyed working with Shoot the Moon for a number of years now. The relationship is very warm and friendly - they’re almost like an extension of our in-house marketing team. \nNothing is ever too much trouble and they always impress with how highly responsive they are to any short-term ad hoc needs, whilst keeping our longer term projects moving along on track and within budget. An absolute pleasure to work with!', NULL, NULL, NULL, NULL),
(121, 1, 31, 7, NULL, 8, NULL, NULL, NULL, NULL, NULL, 'Lucy Evans', '', '“Great to work with”, "passionate about perfection”, "Extremely creative”, "Great sense of style”, "No challenge is too great”, "Creates a fun and inspirational working environment.”', NULL, NULL, NULL, NULL),
(122, 1, 31, 7, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'Kelly Eastwood', 'Greencore', 'We have used STM for projects that needed to communicate real foodie values through creative and visually stunning photography - the results have always surpassed our expectations. ', NULL, NULL, NULL, NULL),
(123, 1, 31, 7, NULL, 10, NULL, NULL, NULL, NULL, NULL, 'Joanna Watling', 'Princes Food & Drink Group', 'With an ability to interpret any brief, deliver stunning imagery for use in a wide variety of applications and all at a competitive rate, STM constantly exceed my expectations and make my job easier.', NULL, NULL, NULL, NULL),
(124, 1, 31, 7, NULL, 11, NULL, NULL, NULL, NULL, NULL, 'Fiona Scott', 'Threebrand', 'Great food photography, great attention to detail, great result! Many thanks for a professional job well done.', NULL, NULL, NULL, NULL),
(126, 1, 47, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 1, 49, 2, NULL, 1, '{filedir_3}devilish-home.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 1, 49, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 1, 50, 2, NULL, 1, '{filedir_3}active-procurement.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 1, 50, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(241, 1, 105, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0224.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(242, 1, 106, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0228.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, 1, 107, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0230.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(244, 1, 108, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_0279.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, 1, 109, 6, NULL, 1, NULL, NULL, '{filedir_8}DSC_6820.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, 1, 110, 6, NULL, 1, NULL, NULL, '{filedir_8}photography-studio.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, 1, 111, 6, NULL, 1, NULL, NULL, '{filedir_8}studio_1.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, 1, 112, 6, NULL, 1, NULL, NULL, '{filedir_8}studio_2.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(249, 1, 113, 6, NULL, 1, NULL, NULL, '{filedir_8}studio_3.jpg', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(250, 1, 114, 2, NULL, 1, '{filedir_2}Brand/Montana.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, 1, 114, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, 1, 115, 2, NULL, 1, '{filedir_2}Brand/Nuba.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, 1, 115, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(254, 1, 116, 2, NULL, 1, '{filedir_2}Brand/Scorpio-logo.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(255, 1, 116, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(258, 1, 118, 2, NULL, 1, '{filedir_2}Brand/Trophy-logo.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(259, 1, 118, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(262, 1, 120, 2, NULL, 1, '{filedir_2}dm/josef-meirs.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(263, 1, 120, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(270, 1, 124, 2, NULL, 1, '{filedir_2}packaging/John-west.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(271, 1, 124, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, 1, 125, 2, NULL, 1, '{filedir_2}packaging/jucee.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, 1, 125, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(274, 1, 126, 2, NULL, 1, '{filedir_2}packaging/koko-noir.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(275, 1, 126, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(276, 1, 127, 2, NULL, 1, '{filedir_2}packaging/Kuli.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(277, 1, 127, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(278, 1, 128, 2, NULL, 1, '{filedir_2}packaging/laziz.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(279, 1, 128, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(280, 1, 129, 2, NULL, 1, '{filedir_2}packaging/meadowvale.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(281, 1, 129, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(282, 1, 130, 2, NULL, 1, '{filedir_2}packaging/Molpol.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(283, 1, 130, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(284, 1, 131, 2, NULL, 1, '{filedir_2}packaging/Montana-chun.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(285, 1, 131, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(286, 1, 132, 2, NULL, 1, '{filedir_2}packaging/Montana.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(287, 1, 132, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(288, 1, 133, 2, NULL, 1, '{filedir_2}packaging/Morrisons.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, 1, 133, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(290, 1, 134, 2, NULL, 1, '{filedir_2}packaging/Vimto.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, 1, 134, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, 1, 135, 2, NULL, 1, '{filedir_2}store-comms/Dickies.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(293, 1, 135, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(296, 1, 137, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, 1, 137, 2, NULL, 1, '{filedir_2}store-comms/Pet-club.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(298, 1, 138, 2, NULL, 1, '{filedir_2}store-comms/Ryman-Furniture-Range-Brochure.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, 1, 138, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, 1, 139, 2, NULL, 1, '{filedir_2}store-comms/Ryman-Furniture-Range-POS.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, 1, 139, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(302, 1, 140, 2, NULL, 1, '{filedir_2}store-comms/Ryman-Store-Graphics.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, 1, 140, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(304, 1, 141, 2, NULL, 1, '{filedir_2}dm/store-open.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(305, 1, 141, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, 1, 142, 2, NULL, 1, '{filedir_1}hot-thick-red-curry.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(307, 1, 142, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(308, 1, 143, 2, NULL, 1, '{filedir_1}orchid-banquet.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, 1, 143, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, 1, 144, 2, NULL, 1, '{filedir_1}oysters.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, 1, 144, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(312, 1, 145, 2, NULL, 1, '{filedir_1}pannacotta.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(313, 1, 145, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, 1, 147, 2, NULL, 1, '{filedir_1}reduced-fat-custard.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(317, 1, 147, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(318, 1, 148, 2, NULL, 1, '{filedir_1}rice-wraps.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, 1, 148, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(320, 1, 149, 2, NULL, 1, '{filedir_1}runny-cheese.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(321, 1, 149, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(322, 1, 150, 2, NULL, 1, '{filedir_1}salad-red.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(323, 1, 150, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(324, 1, 151, 2, NULL, 1, '{filedir_1}three-flavour-sauce-with-duck.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(325, 1, 151, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(326, 1, 152, 2, NULL, 1, '{filedir_1}shippams-RS.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(327, 1, 152, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(328, 1, 153, 2, NULL, 1, '{filedir_1}steak-and-pomigranate.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(329, 1, 153, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(330, 1, 154, 2, NULL, 1, '{filedir_1}Spicebomb-Page-NEW.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(331, 1, 154, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(332, 1, 155, 2, NULL, 1, '{filedir_1}white-aviator-watch.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(333, 1, 155, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(334, 1, 156, 2, NULL, 1, '{filedir_1}Winter-2012-Ed-2-Front-Cover-1-v2.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(335, 1, 156, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(336, 1, 157, 2, NULL, 1, '{filedir_1}YSL-Shocking.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(337, 1, 157, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(338, 1, 158, 2, NULL, 1, '{filedir_1}hoover.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(339, 1, 158, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(340, 1, 159, 2, NULL, 1, '{filedir_1}red-kettel.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(341, 1, 159, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(342, 1, 160, 2, NULL, 1, '{filedir_1}K3D3256.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(343, 1, 160, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(344, 1, 161, 2, NULL, 1, '{filedir_1}MG0936..jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(345, 1, 161, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(346, 1, 162, 2, NULL, 1, '{filedir_1}IMG_6086-RT.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(347, 1, 162, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(348, 1, 163, 2, NULL, 1, '{filedir_1}IMG_6189-RT.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(349, 1, 163, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(350, 1, 164, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(351, 1, 164, 2, NULL, 1, '{filedir_1}IMG_6012-RT.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(352, 1, 165, 2, NULL, 1, '{filedir_1}personal--49.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(353, 1, 165, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(354, 1, 166, 2, NULL, 1, '{filedir_1}personal-copy.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(355, 1, 166, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(356, 1, 167, 2, NULL, 1, '{filedir_1}authentic-cocktail-on-the-beach-.jpeg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(357, 1, 167, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(358, 1, 168, 2, NULL, 1, '{filedir_1}_Almond-Extract.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(359, 1, 168, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(360, 1, 169, 2, NULL, 1, '{filedir_2}store-comms/Pick-n-Mix-POS.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(361, 1, 169, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(362, 1, 170, 2, NULL, 1, '{filedir_2}packaging/Marriages.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(363, 1, 170, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(364, 1, 171, 2, NULL, 1, '{filedir_2}packaging/HarbourSmokehouse-HighlandPark.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(365, 1, 171, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(366, 1, 172, 2, NULL, 1, '{filedir_2}packaging/Devilishly_Delicious_300ml.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(367, 1, 172, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(368, 1, 173, 2, NULL, 1, '{filedir_2}packaging/Disotto_NY_Deli_Tubs.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(369, 1, 173, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(370, 1, 174, 2, NULL, 1, '{filedir_2}packaging/pinkpanther.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(371, 1, 174, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(372, 1, 175, 2, NULL, 1, '{filedir_2}dm/Lakeland-1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(373, 1, 175, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(378, 1, 178, 2, NULL, 1, '{filedir_2}dm/Lakeland_Win12_Option_2v2.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(379, 1, 178, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(380, 1, 179, 2, NULL, 1, '{filedir_2}dm/Thompson-2.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(381, 1, 179, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(382, 1, 180, 2, NULL, 1, '{filedir_2}dm/Thomson-1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(383, 1, 180, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(384, 1, 181, 2, NULL, 1, '{filedir_2}dm/Thomson_Win12_Option_1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(385, 1, 181, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(390, 1, 184, 2, NULL, 1, '{filedir_2}Brand/Kids_Holiday_Pet_Club_Feb.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(391, 1, 184, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(392, 1, 185, 2, NULL, 1, '{filedir_2}Brand/Kids_Holiday_Pet_Club_Summer.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(393, 1, 185, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(394, 1, 186, 2, NULL, 1, '{filedir_2}Brand/Vero_Gelato.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(395, 1, 186, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(396, 1, 187, 2, NULL, 1, '{filedir_2}Brand/Poundbakery.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(397, 1, 187, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(399, 1, 189, 2, NULL, 1, '{filedir_2}Brand/Sayers.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(400, 1, 189, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(401, 1, 190, 2, NULL, 1, '{filedir_2}Brand/Goodness_Me.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(402, 1, 190, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(403, 1, 191, 2, NULL, 1, '{filedir_1}product/spain-shirt.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(404, 1, 191, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, 1, 31, 7, NULL, 12, NULL, NULL, NULL, NULL, NULL, 'Alan Hayes', 'Alpha LSG', 'The Shoot the Moon team always embrace our ideas with such creativity and are spot-on with trends in the digital and print marketing. Speed of delivery is always impressive', NULL, NULL, NULL, NULL),
(406, 1, 31, 7, NULL, 13, NULL, NULL, NULL, NULL, NULL, '', '', '"Thank you all very much for your hard work at such short notice. Really appreciated!"', NULL, NULL, NULL, NULL),
(407, 1, 31, 7, NULL, 14, NULL, NULL, NULL, NULL, NULL, '', '', '"This is bloody amazing.  I LOVE LOVE LOVE it! Thank you once again!"', NULL, NULL, NULL, NULL),
(408, 1, 31, 7, NULL, 15, NULL, NULL, NULL, NULL, NULL, '', '', '"Just wanted to say thanks to you and your designers for all your work over the last few weeks, we’ve not had a lot of time and there have been a lot of amends at short notice so just wanted to say thanks and have a good long weekend..." ', NULL, NULL, NULL, NULL),
(409, 1, 31, 7, NULL, 16, NULL, NULL, NULL, NULL, NULL, '', '', '"Thanks for the help today, you have inspired us all..."', NULL, NULL, NULL, NULL),
(410, 1, 31, 7, NULL, 17, NULL, NULL, NULL, NULL, NULL, 'Anna Massie', 'Macphie of Glenbervie', '"Great shelf stand out and presence on fixture, thanks to all for all your work on this project to date."', NULL, NULL, NULL, NULL),
(411, 1, 31, 7, NULL, 18, NULL, NULL, NULL, NULL, NULL, '', '', '"We’ve just received all the brochures – they look great, thanks to everyone for all your hard work."', NULL, NULL, NULL, NULL),
(412, 1, 31, 7, NULL, 19, NULL, NULL, NULL, NULL, NULL, 'Catriona Marshall', 'Pets at Home', '"Just want to say a BIG well done for all the fantastic work you and your team have done on Advanced Nutrition." ', NULL, NULL, NULL, NULL),
(413, 1, 192, 2, NULL, 1, '{filedir_3}caterforce.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(414, 1, 192, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(415, 1, 193, 2, NULL, 1, '{filedir_3}chefs-selections.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(416, 1, 193, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(417, 1, 194, 2, NULL, 1, '{filedir_3}devilish-facebook.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(418, 1, 194, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(419, 1, 195, 2, NULL, 1, '{filedir_3}macphie-homepage.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(420, 1, 195, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(421, 1, 196, 2, NULL, 1, '{filedir_3}marriages.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(422, 1, 196, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(423, 1, 197, 2, NULL, 1, '{filedir_3}meadowvale.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(424, 1, 197, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(425, 1, 198, 2, NULL, 1, '{filedir_3}jucee-page.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(426, 1, 198, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(427, 1, 199, 2, NULL, 1, '{filedir_3}sayers-screen.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(428, 1, 199, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(429, 1, 200, 2, NULL, 1, '{filedir_3}scorpio-screen.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(430, 1, 200, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(433, 1, 202, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(434, 1, 203, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(435, 1, 204, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}bat-and-ball.m4v', '{filedir_3}bat-and-ball.webm', '{filedir_3}bat-and-ball.ogv', '{filedir_3}bat-and-ball.jpg'),
(436, 1, 205, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}nets.m4v', '{filedir_3}nets.webm', '{filedir_3}nets.ogv', '{filedir_3}nets.jpg'),
(437, 1, 206, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Jucee_Umbrella.m4v', '{filedir_3}Jucee_Umbrella.webm', '{filedir_3}Jucee_Umbrella.ogv', '{filedir_3}Jucee_Umbrella.jpg'),
(438, 1, 207, 2, NULL, 1, '{filedir_3}two_meadowvale.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(439, 1, 207, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(440, 1, 208, 2, NULL, 1, '{filedir_3}tcx-combined.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(441, 1, 208, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(442, 1, 209, 2, NULL, 1, '{filedir_3}pets.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(443, 1, 209, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(446, 1, 211, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(447, 1, 212, 2, NULL, 1, '{filedir_3}pah-banners.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(448, 1, 212, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(449, 1, 213, 2, NULL, 1, '{filedir_3}ryman.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(450, 1, 213, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(451, 1, 214, 2, NULL, 1, '{filedir_3}pound-bakery.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(452, 1, 214, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(453, 1, 215, 2, NULL, 1, '{filedir_3}mixed-banners.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(454, 1, 215, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(455, 1, 216, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(456, 1, 217, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(457, 1, 218, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(458, 1, 219, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(459, 1, 220, 2, NULL, 1, '{filedir_1}_Almond-Extract.jpg', '<p>\n	Some text goes here</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(460, 1, 220, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(461, 1, 221, 2, NULL, 1, '{filedir_2}dm/Back_to_School_1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(462, 1, 221, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(463, 1, 222, 2, NULL, 1, '{filedir_2}dm/Ride_Away_1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(464, 1, 222, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(465, 1, 223, 2, NULL, 1, '{filedir_2}dm/Thomson_1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(466, 1, 223, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(467, 1, 224, 2, NULL, 1, '{filedir_2}store-comms/Ryman_1.jpg', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(468, 1, 224, 6, NULL, 1, NULL, NULL, '', '', 'People', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_members`
--

CREATE TABLE IF NOT EXISTS `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_members`
--

INSERT INTO `exp_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `daylight_savings`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`, `rte_enabled`, `rte_toolset_id`) VALUES
(1, 1, 'Admin', 'Admin', '98921df57eca1f348c1deb05150da212cbeea968a601bac04f590763e296838df9173296949a0e101a5e3cb378e9205f2d59bb87b1463f6ca40264c868bf298a', 'cRr:{\\Vr8+jao6rn+8PIN|pl}w^]dn;|!Gqn|-zCB$,#HrWbxEnh/r/|]`YO~:2MInAwa{Q0UfuJt#?+)c`Q''t7D3M3lxj8s6E/-|1)nusw=*caWg#B9`r)qoZB8Q?Tj', 'ec2229b71e1842575a97e29e2f34f08838ab8b28', 'de88dce51563f2806de65b62ac2604d35fc7fffb', NULL, 'martin@shoot-the-moon.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '127.0.0.1', 1351164680, 1384855851, 1386688415, 141, 0, 0, 0, 1379947062, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UTC', 'n', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', '', 'Structure|C=addons_modules&M=show_module_cp&module=structure|1\nAssets|C=addons_modules&M=show_module_cp&module=assets|2', 'n', 0, 'y', 0),
(2, 6, 'Editor', 'Editor', '999e767d6f36788d70cc0d7805d7b3732f9ba10a', '', '90ca197b90a9b9702aea0c5ae3d7d4b1524e7d20', NULL, NULL, 'kathryn@shoot-the-moon.co.uk', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '127.0.0.1', 1351165033, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UTC', 'n', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_bulletin_board`
--

CREATE TABLE IF NOT EXISTS `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_data`
--

CREATE TABLE IF NOT EXISTS `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_data`
--

INSERT INTO `exp_member_data` (`member_id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_fields`
--

CREATE TABLE IF NOT EXISTS `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_groups`
--

INSERT INTO `exp_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_admin_accessories`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`) VALUES
(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(6, 1, 'Editors', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_homepage`
--

CREATE TABLE IF NOT EXISTS `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_homepage`
--

INSERT INTO `exp_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`) VALUES
(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0),
(2, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_search`
--

CREATE TABLE IF NOT EXISTS `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_copies`
--

CREATE TABLE IF NOT EXISTS `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_data`
--

CREATE TABLE IF NOT EXISTS `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_folders`
--

CREATE TABLE IF NOT EXISTS `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_message_folders`
--

INSERT INTO `exp_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`) VALUES
(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_listed`
--

CREATE TABLE IF NOT EXISTS `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_modules`
--

CREATE TABLE IF NOT EXISTS `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  `settings` text,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `exp_modules`
--

INSERT INTO `exp_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`, `settings`) VALUES
(1, 'Comment', '2.3', 'y', 'n', NULL),
(2, 'Email', '2.0', 'n', 'n', NULL),
(3, 'Emoticon', '2.0', 'n', 'n', NULL),
(4, 'Jquery', '1.0', 'n', 'n', NULL),
(5, 'Rss', '2.0', 'n', 'n', NULL),
(6, 'Safecracker', '2.1', 'y', 'n', NULL),
(7, 'Search', '2.2', 'n', 'n', NULL),
(8, 'Channel', '2.0.1', 'n', 'n', NULL),
(9, 'Member', '2.1', 'n', 'n', NULL),
(10, 'Stats', '2.0', 'n', 'n', NULL),
(11, 'Rte', '1.0', 'y', 'n', NULL),
(13, 'Assets', '1.2.2', 'y', 'n', NULL),
(14, 'Deeploy_helper', '2.0.3', 'y', 'n', NULL),
(15, 'Field_editor', '1.0.3', 'y', 'n', NULL),
(16, 'Libraree', '1.0.5', 'y', 'n', NULL),
(17, 'Low_reorder', '2.0.4', 'y', 'n', NULL),
(18, 'Low_variables', '2.3.2', 'y', 'n', NULL),
(19, 'Playa', '4.3.3', 'n', 'n', NULL),
(20, 'Updater', '3.1.5', 'y', 'n', NULL),
(21, 'Wygwam', '2.6.3', 'y', 'n', NULL),
(22, 'Zoo_flexible_admin', '1.61', 'y', 'n', NULL),
(23, 'Zoo_triggers', '1.1.10', 'y', 'n', NULL),
(24, 'Structure', '3.3.6', 'y', 'y', NULL),
(25, 'Freeform', '4.0.7', 'y', 'n', NULL),
(26, 'Gmaps', '2.3', 'n', 'n', NULL),
(27, 'Backup_proish', '1.0.5', 'y', 'n', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_module_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_module_member_groups`
--

INSERT INTO `exp_module_member_groups` (`group_id`, `module_id`) VALUES
(6, 1),
(6, 6),
(6, 11),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(6, 16),
(6, 17),
(6, 18),
(6, 20),
(6, 21),
(6, 22),
(6, 23);

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee`
--

CREATE TABLE IF NOT EXISTS `exp_navee` (
  `navee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `navigation_id` int(10) DEFAULT NULL,
  `site_id` int(10) DEFAULT NULL,
  `entry_id` int(10) DEFAULT NULL,
  `channel_id` int(10) DEFAULT NULL,
  `template` int(10) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `include` tinyint(4) DEFAULT NULL,
  `passive` tinyint(4) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  `rel` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `regex` varchar(255) DEFAULT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `custom_kids` varchar(255) DEFAULT NULL,
  `access_key` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`navee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_navee`
--

INSERT INTO `exp_navee` (`navee_id`, `navigation_id`, `site_id`, `entry_id`, `channel_id`, `template`, `type`, `parent`, `text`, `link`, `class`, `id`, `sort`, `include`, `passive`, `datecreated`, `dateupdated`, `ip_address`, `member_id`, `rel`, `name`, `target`, `title`, `regex`, `custom`, `custom_kids`, `access_key`) VALUES
(1, 1, 1, 1, 6, 1, 'guided', 0, 'The Studio', '', '', '', 1, 1, 0, '2012-10-29 11:58:25', '2012-10-29 11:58:25', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(2, 1, 1, 4, 1, 3, 'guided', 0, 'Photography', '', '', '', 2, 1, 0, '2012-10-29 11:59:48', '2012-10-29 11:59:48', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(3, 1, 1, 25, 4, 3, 'guided', 2, 'Food', '', '', '', 1, 1, 0, '2012-10-29 12:04:04', '2012-10-29 12:04:04', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_cache`
--

CREATE TABLE IF NOT EXISTS `exp_navee_cache` (
  `navee_cache_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `navigation_id` int(10) DEFAULT NULL,
  `group_id` smallint(4) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `recursive` tinyint(4) DEFAULT NULL,
  `ignore_include` tinyint(4) DEFAULT NULL,
  `start_from_parent` tinyint(4) DEFAULT NULL,
  `start_from_kid` tinyint(4) DEFAULT NULL,
  `single_parent` tinyint(4) DEFAULT NULL,
  `cache` longtext,
  PRIMARY KEY (`navee_cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_config`
--

CREATE TABLE IF NOT EXISTS `exp_navee_config` (
  `navee_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `k` varchar(255) DEFAULT NULL,
  `v` text,
  PRIMARY KEY (`navee_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `exp_navee_config`
--

INSERT INTO `exp_navee_config` (`navee_config_id`, `site_id`, `k`, `v`) VALUES
(1, 1, 'stylesheet', 'navee.css'),
(2, 1, 'include_index', 'false'),
(3, 1, 'remove_deleted_entries', 'false'),
(4, 1, 'entify_ee_tags', 'false'),
(5, 1, 'force_trailing_slash', 'no'),
(6, 1, 'only_superadmins_can_admin_navs', 'false'),
(7, 1, 'blockedMemberGroups', 'a:2:{i:1;a:1:{i:0;s:1:"6";}i:0;a:1:{i:0;s:1:"6";}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_members`
--

CREATE TABLE IF NOT EXISTS `exp_navee_members` (
  `navee_mem_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `navee_id` int(10) DEFAULT NULL,
  `members` text,
  PRIMARY KEY (`navee_mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_navs`
--

CREATE TABLE IF NOT EXISTS `exp_navee_navs` (
  `navigation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `nav_title` varchar(255) DEFAULT NULL,
  `nav_name` varchar(255) DEFAULT NULL,
  `nav_description` varchar(255) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`navigation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_navee_navs`
--

INSERT INTO `exp_navee_navs` (`navigation_id`, `site_id`, `nav_title`, `nav_name`, `nav_description`, `datecreated`, `dateupdated`, `ip_address`, `member_id`) VALUES
(1, 1, 'main', 'Main', 'Main Navigation', '2012-10-29 11:57:38', '2012-10-29 11:57:38', '127.0.0.1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_online_users`
--

CREATE TABLE IF NOT EXISTS `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4873 ;

--
-- Dumping data for table `exp_online_users`
--

INSERT INTO `exp_online_users` (`online_id`, `site_id`, `member_id`, `in_forum`, `name`, `ip_address`, `date`, `anon`) VALUES
(4872, 1, 0, 'n', '', '109.111.197.225', 1396255994, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_password_lockout`
--

CREATE TABLE IF NOT EXISTS `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `exp_password_lockout`
--

INSERT INTO `exp_password_lockout` (`lockout_id`, `login_date`, `ip_address`, `user_agent`, `username`) VALUES
(1, 1352135798, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(2, 1352293133, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(3, 1352293148, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(4, 1352303529, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(5, 1352893907, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(6, 1353077438, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(7, 1353933615, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(8, 1353933622, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(9, 1354024028, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(10, 1355143275, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11', 'admin'),
(11, 1355143285, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11', 'admin'),
(12, 1355143318, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11', 'admin'),
(13, 1355817752, '87.112.160.113', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(14, 1355817795, '87.112.160.113', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(15, 1356086333, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'stmstaff'),
(16, 1356086342, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'stmstaff'),
(17, 1356186803, '109.145.237.206', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(18, 1357139458, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(19, 1357656551, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(20, 1357656941, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'admin'),
(21, 1357656949, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'admin'),
(22, 1357744654, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(23, 1357834692, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(24, 1357834699, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(25, 1360169895, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(26, 1360578863, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(27, 1362503159, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'admin'),
(28, 1381318221, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safari/537.36', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `exp_ping_servers`
--

CREATE TABLE IF NOT EXISTS `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_playa_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_remember_me`
--

CREATE TABLE IF NOT EXISTS `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_reset_password`
--

CREATE TABLE IF NOT EXISTS `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_revision_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `exp_revision_tracker`
--

INSERT INTO `exp_revision_tracker` (`tracker_id`, `item_id`, `item_table`, `item_field`, `item_date`, `item_author_id`, `item_data`) VALUES
(5, 1, 'exp_templates', 'template_data', 1351517720, 1, 'Index\n{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		\n         	{exp:channel:entries channel="photography" disable="{lv_disable_basic}" dynamic="on"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_blockquote}\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(7, 8, 'exp_templates', 'template_data', 1352825737, 1, ''),
(8, 5, 'exp_templates', 'template_data', 1353078861, 1, '{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		{lv_folio_nav}\n        		\n        		<div id="content1" class="folio">\n	        		<ul>\n         	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\n	\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			\n	        		\n          	{/exp:channel:entries}\n	        		</ul>\n       		</div>\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n   \n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{embed="sites/discipline-thums"}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(10, 1, 'exp_templates', 'template_data', 1353078861, 1, '{lv_doc_header}\n    </head>\n    <body class="home">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	{lv_image_matrix}\n					\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n         	{exp:channel:entries channel="studio" disable="{lv_disable_basic}" dynamic="off"}\n	  		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_studio_shots}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_studio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_studio_shots}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_blockquote}\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(12, 8, 'exp_templates', 'template_data', 1353078861, 1, '<?php\n\n	$url = $_SERVER[''REQUEST_URI''];\n	$segments = explode("/",$url);\n	if ($segments[2] != "") {\n		$pagination = $segments[2];\n		$offset = substr($pagination, 1);\n	} else {\n		$offset = 0;\n	}\n	\n?>\n\n<ul id="switch" class="pie clearfix">\n\n	{if segment_1=="creative"}\n	{exp:channel:entries channel="packaging|branding|literature|point_of_sale" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n	{if segment_1=="photography"}\n	{exp:channel:entries channel="food|product|location|set" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n	{if segment_1=="digital"}\n	{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	{/exp:channel:entries}\n	{/if}\n	\n</ul>'),
(14, 4, 'exp_templates', 'template_data', 1357743255, 1, 'Get in Touch\n{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message so that one of our staff can respond accordingly"></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(15, 4, 'exp_templates', 'template_data', 1357743338, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message so that one of our staff can respond accordingly"></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(16, 3, 'exp_templates', 'template_data', 1357905693, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" sort="random" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(17, 3, 'exp_templates', 'template_data', 1357905788, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" orderby="random" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(18, 3, 'exp_templates', 'template_data', 1357905835, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(19, 4, 'exp_templates', 'template_data', 1358159460, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(20, 10, 'exp_templates', 'template_data', 1360754332, 1, '{lv_doc_header}\n    </head>\n    <body class="thinking">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	<div id="matrix">\n				 		<ul id="slides">\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        </ul>\n				 	</div>\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			\n        		</div>\n        		<!--<div id="intro">\n	        		\n        		</div>-->\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(21, 10, 'exp_templates', 'template_data', 1360754334, 1, '{lv_doc_header}\n    </head>\n    <body class="thinking">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	<div id="matrix">\n				 		<ul id="slides">\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n                        </ul>\n				 	</div>\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			\n        		</div>\n        		<!--<div id="intro">\n	        		\n        		</div>-->\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(22, 10, 'exp_templates', 'template_data', 1360754387, 1, '{lv_doc_header}\n    </head>\n    <body class="thinking">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	<!--<div id="matrix-sml">-->\n				 		<ul id="slides-single">\n                            {cf_rotating_image_blocks}\n                            <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="199" height="145" crop="yes" allow_scale_larger="yes"}</li>\n                            {/cf_rotating_image_blocks}\n				 		</ul>\n				 	<!--</div>-->\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			{homepage_pull_quote}\n        		</div>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(26, 11, 'exp_templates', 'template_data', 1379941938, 1, '<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n\n	{!-- Home --}\n	<url> \n		<loc>{site_url}</loc> \n		<lastmod>{exp:stats}{last_entry_date format="{DATE_W3C}"}{/exp:stats}</lastmod> \n		<changefreq>always</changefreq> \n		<priority>1.0</priority> \n	</url> \n\n\n	{!-- Content Pages --}\n	{exp:channel:entries channel="content" disable="categories|member_data|pagination|trackbacks" dynamic="off" status="Open"}\n\n	{exp:channel:entries disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{change_frequency}</changefreq> \n		<priority>{priority}</priority> \n	</url> \n	{/exp:channel:entries} \n\n</urlset> '),
(27, 11, 'exp_templates', 'template_data', 1379941952, 1, '<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n\n	{!-- Home --}\n	<url> \n		<loc>{site_url}</loc> \n		<lastmod>{exp:stats}{last_entry_date format="{DATE_W3C}"}{/exp:stats}</lastmod> \n		<changefreq>always</changefreq> \n		<priority>1.0</priority> \n	</url> \n\n\n	{!-- Content Pages --}\n	{exp:channel:entries channel="content" disable="categories|member_data|pagination|trackbacks" dynamic="off" status="Open"}\n\n	{exp:channel:entries disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{change_frequency}</changefreq> \n		<priority>{priority}</priority> \n	</url> \n	{/exp:channel:entries} \n\n</urlset> '),
(28, 9, 'exp_templates', 'template_data', 1379942089, 1, '<?php\n\n	$url = $_SERVER[''REQUEST_URI''];\n	$segments = explode("/",$url);\n	if ($segments[3] != "") {\n		$pagination = $segments[3];\n		$offset = substr($pagination, 1);\n	} else {\n		$offset = 0;\n	}\n	\n?>\n\n<ul id="switch" class="pie clearfix">\n\n	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	\n	{if segment_2=="motion"}\n	\n	\n	\n	{video_upload}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{start_image}" width="95" height="95" crop="yes"}</a></li>\n	{/video_upload}\n			\n	{if:else}\n	\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	\n	{/if}\n	{/exp:channel:entries}\n	\n</ul>'),
(29, 5, 'exp_templates', 'template_data', 1379942089, 1, '{lv_doc_header}\r\n    </head>\r\n    <body>\r\n        {lv_chrome_frame}\r\n        \r\n        \r\n\r\n        <div id="wrap" class="clearfix">\r\n        	{lv_header}\r\n	       	<section id="content" class="pie">\r\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\r\n        		<h1>{title}</h1>\r\n					{lv_folio_nav}\r\n        		<div id="intro">\r\n	        		{cf_introduction_text}\r\n        		</div>\r\n        	{/exp:channel:entries}\r\n        		\r\n        		\r\n        		<div id="content1" class="folio">\r\n	        		<ul>\r\n         	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\n         	\n         				\n         				{if segment_2=="motion"}\n         				\n         			<!-- 	<video id="my_video_1" class="video-js vjs-default-skin" controls\n         				  preload="auto" width="640" height="360" poster="/images/video/bat-and-ball.jpg"\n         				  data-setup="{}">\n         				  <source src="/images/video/bat-and-ball.mp4" type=''video/mp4''>\n         				  <source src="/images/video/bat-and-ball.webm" type=''video/webm''>\n         				  <source src="/images/video/bat-and-ball.ogv" type=''video/ogv''>\n         				</video> -->\n         				\n         				\n         				\n         					{video_upload}\n	        			<li id="id{entry_id}">\n							<video id="my_video_1" class="video-js vjs-default-skin" controls\n							  preload="auto" width="640" height="360" poster="{start_image}"\n							  data-setup="{}">\n							  <source src="{version_m4v}" type=''video/mp4''>\n							  <source src="{version_webm}" type=''video/webm''>\n							  <source src="{version_ogv}" type=''video/ogv''>\n							</video>\n	        			</li>\n         					{/video_upload}\n         					\n         					\n         					\n         					\n         					\n         				\n         				{if:else}\n         				\n         				\r\n	\r\n	        			{cf_portfolio_images}\r\n	        			<li id="id{entry_id}">\r\n									<div class="image-wrap">\r\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\r\n		        				{if cf_mx_supporting_text}\r\n										<div class="img-txt">\r\n		        					{cf_mx_supporting_text}\r\n		        				</div>\r\n										{/if}\r\n									</div>\r\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\r\n	        			</li>\r\n	        			{/cf_portfolio_images}\r\n	        			\r\n	        			{paginate}\r\n	        				<p class="paginate">	\r\n	        			      {if previous_page}\r\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\r\n	        			      {/if}\r\n	        			      {if next_page}\r\n	        			        <a href="{auto_path}">More examples</a>\r\n	        			      {/if}\r\n	        			     </p>\r\n	        			  {/paginate}\n	        			  \n	        			  {/if}\r\n	        			\r\n	        		\r\n          	{/exp:channel:entries}\r\n	        		</ul>\r\n       		</div>\r\n       		<div id="content2" class="dn folio">PRODUCT</div>\r\n        		<div id="content3" class="dn folio">LOCATION</div>\r\n        		<div id="content4" class="dn folio">SETS</div>\r\n        		\r\n        		\r\n        		\r\n   \r\n        		\r\n        	</section>\r\n			<section id="side" class="vcard">\r\n				 {lv_sidebar_address}\r\n			</section>\r\n			<div id="side-thumbs">\r\n			{embed="sites/discipline-thums"}\r\n			</div>      \r\n			{lv_blockquote}\r\n        	\r\n	        <footer class="vcard">\r\n				 {lv_footer}\r\n	        </footer>\r\n        \r\n        \r\n        </div>\r\n\r\n        {lv_base_scripts}\r\n    </body>\r\n</html>\r\n'),
(30, 6, 'exp_templates', 'template_data', 1379942089, 1, 'Food\n{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		{lv_folio_nav}\n        		\n			{exp:channel:entries channel="{segment_1}" disable="{lv_disable_basic}" dynamic="off"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{lv_thumbs_nav}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(31, 4, 'exp_templates', 'template_data', 1379942089, 1, '{lv_doc_header}\n\n\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix contact">\n        	{lv_header}\n			<section id="content" class="pie">\n			{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n			<h1>{title}</h1>\n			<div id="intro">\n				{cf_introduction_text}\n			</div>\n			{/exp:channel:entries}\n         		\n        		{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message"}\n        		<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		\n        		\n        		<div class="row">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				</div>\n				<div class="row">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		</div>\n        		<div class="row">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		</div>\n        		<div class="row"><label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		</div>\n        		<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n        		{/exp:freeform:form}\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n				 \n				 {exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 \n			</section>\n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(32, 1, 'exp_templates', 'template_data', 1379942089, 1, '{lv_doc_header}\n    </head>\n    <body class="home">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	<div id="matrix">\n				 		<ul>\n				 		{cf_rotating_image_blocks}\n				 		{if count == 1}\n				 		<div class="slide-img">\n				 			{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}\n				 		</div>\n				 		<ul id="slides">\n				 		{/if}\n				 		<li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n				 		{if count == total_files}\n				 		</ul>\n				 		{/if}\n				 		{/cf_rotating_image_blocks}\n				 		</ul>\n				 	</div>\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			{homepage_pull_quote}\n        		</div>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(33, 3, 'exp_templates', 'template_data', 1379942089, 1, '	{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<!-- {lv_image_matrix} -->\n        		<h1 class="h1alt">{title}</h1>\n					{lv_folio_nav}\n        		\n					{lv_image_matrix}\n        		\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		        		\n       		\n         	\n         	\n         	\n         		<div id="content1" class="folio">\n\n	        		<ul>\n						{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n						{/if}\n						\n	        			{cf_portfolio_images}\n	        			<li id="id{entry_id}">\n									<div class="image-wrap">\n										{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\n		        				{if cf_mx_supporting_text}\n										<div class="img-txt">\n		        					{cf_mx_supporting_text}\n		        				</div>\n										{/if}\n									</div>\n	        				<p class="top"><a href="#wrap" class="smoothScroll">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        			\n	        			\n	        			{paginate}\n	        				<p class="paginate">	\n	        			      {if previous_page}\n	        			          <a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			      {/if}\n	        			      {if next_page}\n	        			        <a href="{auto_path}">More examples</a>\n	        			      {/if}\n	        			     </p>\n	        			  {/paginate}\n	        			  \n	        			  \n	        			{/exp:channel:entries}\n\n	        		</ul>\n	        		\n	        		\n       		</div>\n          	\n          	\n          	\n         	\n          	\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        		\n        		\n        		\n        		\n        		\n        		\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			\n			<div id="side-thumbs">\n				{embed="sites/thumb-nav"}\n				{!--lv_thumbs_nav--}\n			</div>\n			      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>'),
(34, 11, 'exp_templates', 'template_data', 1379942089, 1, '<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n\n	{!-- Home --}\n	<url> \n		<loc>{site_url}</loc> \n		<lastmod>{exp:stats}{last_entry_date format="{DATE_W3C}"}{/exp:stats}</lastmod> \n		<changefreq>always</changefreq> \n		<priority>1.0</priority> \n	</url> \n\n\n	{!-- Content Pages --}\n	{exp:channel:entries disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{change_frequency}</changefreq> \n		<priority>{priority}</priority> \n	</url> \n	{/exp:channel:entries} \n\n</urlset> '),
(35, 10, 'exp_templates', 'template_data', 1379942089, 1, '{lv_doc_header}\n    </head>\n    <body class="thinking">\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 	\n					\n        		<h1>{title}</h1>\n        		<div id="pullquote">\n        			{homepage_pull_quote}\n        		</div>\n        		<div id="intro">\n	        		<!--<div id="matrix-sml">-->\n				 		<!--<ul id="slides-single">\n                            {cf_rotating_image_blocks}\n                            <li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="199" height="145" crop="yes" allow_scale_larger="yes"}</li>\n                            {/cf_rotating_image_blocks}\n				 		</ul>-->\n				 	<!--</div>-->\n                    {cf_introduction_text}\n                </div>\n                <div id="rightheads">\n                	{cf_rightheads_text}\n                </div>\n        	{/exp:channel:entries}\n         	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n '),
(36, 8, 'exp_templates', 'template_data', 1379942089, 1, '<?php\r\n\r\n	$url = $_SERVER[''REQUEST_URI''];\r\n	$segments = explode("/",$url);\r\n	if ($segments[2] != "") {\r\n		$pagination = $segments[2];\r\n		$offset = substr($pagination, 1);\r\n	} else {\r\n		$offset = 0;\r\n	}\r\n	\r\n?>\r\n<ul id="switch" class="pie clearfix">\r\n	{if segment_1=="creative"}\r\n	{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n	{if segment_1=="photography"}\r\n	{exp:channel:entries channel="food|product|location|personal" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n	{if segment_1=="digital"}\r\n	{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n</ul>'),
(37, 11, 'exp_templates', 'template_data', 1379942121, 1, '<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n\n	{!-- Home --}\n	<url> \n		<loc>{site_url}</loc> \n		<lastmod>{exp:stats}{last_entry_date format="{DATE_W3C}"}{/exp:stats}</lastmod> \n		<changefreq>always</changefreq> \n		<priority>1.0</priority> \n	</url> \n\n\n	{!-- Content Pages --}\n	{exp:channel:entries disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{change_frequency}</changefreq> \n		<priority>{priority}</priority> \n	</url> \n	{/exp:channel:entries} \n\n</urlset> ');
INSERT INTO `exp_revision_tracker` (`tracker_id`, `item_id`, `item_table`, `item_field`, `item_date`, `item_author_id`, `item_data`) VALUES
(38, 5, 'exp_templates', 'template_data', 1382536061, 1, '{lv_doc_header}\r\n	</head>\r\n	<body>\r\n	{lv_chrome_frame}\r\n	{lv_header}\r\n	<div class="container">\r\n		<div class="eleven columns alpha omega content">\r\n			<div class="twelve columns row content-inner alpha omega">\r\n				<h1 class="six columns omega brand">{lv_brand}</h1>\r\n				{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\r\n				<h1 class="four columns page-header alpha">{title}</h1>\r\n			</div>\r\n			<div class="eleven omega discipline-display">\r\n				<div class="disp-tab">\r\n					{lv_folio_nav}\r\n				</div>\r\n				<div class="disp-header">{cf_introduction_text}</div>\r\n				{/exp:channel:entries}\r\n				<div class="disp-content">\r\n					<ul>\r\n					{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\r\n\r\n         				{if segment_2=="motion"}\r\n         				\r\n    	   					{video_upload}\r\n			        			<li id="id{entry_id}">\r\n										<video id="my_video_1" class="video-js vjs-default-skin" controls \r\n										preload="auto" width="640" height="360" poster="{start_image}"\r\n										data-setup="{}">\r\n										<source src="{version_m4v}" type=''video/mp4''>\r\n										<source src="{version_webm}" type=''video/webm''>\r\n								  		<source src="{version_ogv}" type=''video/ogv''>\r\n									</video>\r\n	        					</li>\r\n         					{/video_upload}\r\n\r\n         				{if:else}\r\n\r\n							{cf_portfolio_images}\r\n	        				<li id="id{entry_id}">\r\n								{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\r\n			        			{if cf_mx_supporting_text}\r\n									<div class="img-txt">\r\n			        					{cf_mx_supporting_text}\r\n		    	    				</div>\r\n								{/if}\r\n							</li>\r\n	        				{/cf_portfolio_images}\r\n\r\n		        			{paginate}\r\n		        				<p class="paginate">	\r\n	    	    			    {if previous_page}\r\n	        				    	<a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\r\n	        				    {/if}\r\n	        				    {if next_page}\r\n	        				    	<a href="{auto_path}">More examples</a>\r\n		        			    {/if}\r\n		        			    </p>\r\n	    	    			{/paginate}\r\n			\r\n						{/if}\r\n	        				        		\r\n          			{/exp:channel:entries}\r\n          			<ul>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="five columns alpha omega side-content">\r\n			{lv_sidebar_address}\r\n			{lv_blockquote}\r\n		</div>\r\n		<div id="side-thumbs">\r\n				{embed="sites/thumb-nav"}\r\n				{!--lv_thumbs_nav--}\r\n		</div>\r\n	</div><!-- container -->\r\n	<a href="#" class="return">&uarr;</a>\r\n	{lv_footer}\r\n    {lv_base_scripts}\r\n    </body>\r\n</html>\r\n'),
(39, 10, 'exp_templates', 'template_data', 1382536061, 1, '{lv_doc_header}\n    </head>\n    <body>\n    {lv_chrome_frame}\n    {lv_header}\n    <div class="container content">\n    {exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n        <div class="sixteen columns row content-inner alpha omega">\n            <h1 class="eight columns omega brand"><img src="resource/stat/shoot-the-moon.jpg" alt=""></h1>\n            <h1 class="four columns page-header alpha">{title}</h1>\n            <div class="four columns offset-by-four omega header-contact">\n                <p class="header-info">\n                    <span>t/ 0161 205 3311</span><br>\n                    <span>hello@shoot-the-moon.co.uk</span><br>\n                    <span>Concept House, Naval Street Manchester M4 6AX</span>\n                </p>\n            </div>\n        </div>\n        <div class="sixteen columns alpha omega ot-page">\n            <h1 class="five columns page-title">{homepage_pull_quote}</h1>\n            <div class="six columns ot-intro">\n                {cf_introduction_text}\n            </div>\n            <div class="offset-by-ten six columns alpha ot-sub">\n                {cf_rightheads_text}\n            </div>\n        </div>\n        {/exp:channel:entries}\n    </div><!-- container -->\n    {lv_footer}\n        {lv_base_scripts}\n    </body>\n</html>'),
(40, 4, 'exp_templates', 'template_data', 1382536061, 1, '{lv_doc_header}\n	</head>\n	<body>\n	{lv_chrome_frame}\n	{lv_header}\n	<div class="container">\n		{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n		<div class="eleven columns alpha omega content contact">\n			<div class="twelve columns row content-inner alpha omega">\n				<h1 class="six columns omega brand">{lv_brand}</h1>\n				<h1 class="four columns page-header alpha">{title}</h1>\n			</div>\n			<div class="eleven contact-form">\n				<div id="intro">\n				{cf_introduction_text}\n				{/exp:channel:entries}\n				</div>\n\n				{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message"}\n				<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		<label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		<input type="submit" name="submit" value="Submit your enquiry" class="submit">\n        		{/exp:freeform:form}\n\n			</div>\n		</div>\n		<div class="five columns alpha omega side-content">\n			{lv_sidebar_address}\n			{exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 {lv_blockquote}\n		</div>\n	</div><!-- container -->\n	{lv_footer}\n        {lv_base_scripts}\n    </body>\n</html>'),
(41, 3, 'exp_templates', 'template_data', 1382536061, 1, '{lv_doc_header}\n	</head>\n	<body class="landing">\n	{lv_chrome_frame}\n	{lv_header}\n	<div class="container">\n		<div class="eleven columns alpha omega content">\n			<div class="twelve columns row content-inner alpha omega">\n				<h1 class="six columns omega brand">{lv_brand}</h1>\n				{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<h1 class="four columns page-header alpha">{title}</h1>\n			</div>\n			<div class="eleven omega discipline-display">\n				<div class="disp-tab">\n					{lv_folio_nav}\n				</div>\n				<div class="disp-header">{cf_introduction_text}</div>\n				{/exp:channel:entries}\n				<div class="disp-content">\n					<ul>\n					{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n					{/if}\n					{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n					{/if}\n					{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n					{/if}\n\n					{cf_portfolio_images}\n						<li id="id{entry_id}">\n							<span data-picture data-alt="Portfolio Image">\n    							<span data-src="{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''300'' output=''{made}''}"></span>\n    							<span data-src="{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''380'' output=''{made}''}" data-media="(min-width: 400px)"></span>\n    							<span data-src="{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''780'' output=''{made}''}" data-media="(min-width: 800px)"></span>\n    							<span data-src="{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''980'' output=''{made}''}" data-media="(min-width: 1000px)"></span>\n    							<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->\n    							<noscript>\n    								{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''640'' output=''<img src="{made}" alt="Portfolio Image">''}\n    							</noscript>\n							</span>\n   							{if cf_mx_supporting_text}\n								<div class="img-txt">\n   									{cf_mx_supporting_text}\n   								</div>\n							{/if}\n						</li>\n					{/cf_portfolio_images}\n\n		        	{paginate}\n		        		<p class="paginate">	\n	    	    		    {if previous_page}\n	        			    	<a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			    {/if}\n	        			    {if next_page}\n	        			    	<a href="{auto_path}">More examples</a>\n		        		    {/if}\n		        	    </p>\n	    	    	{/paginate}\n			\n						        				        		\n          			{/exp:channel:entries}\n          			<ul>\n				</div>\n			</div>\n		</div>\n		<div class="five columns alpha omega side-content">\n			{lv_sidebar_address}\n			{lv_blockquote}\n		</div>\n		<div class="side-thumbs">\n			{embed="sites/thumb-nav"}\n			{!--lv_thumbs_nav--}\n		</div>\n	</div><!-- container -->\n	<a href="#" class="return">&uarr;</a>\n	{lv_footer}\n	<div class="bg"></div>\n    {lv_base_scripts}\n    </body>\n</html>\n'),
(42, 6, 'exp_templates', 'template_data', 1382536061, 1, 'Food\n{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		{lv_folio_nav}\n        		\n			{exp:channel:entries channel="{segment_1}" disable="{lv_disable_basic}" dynamic="off"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{lv_thumbs_nav}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(43, 9, 'exp_templates', 'template_data', 1382536061, 1, '<?php\n\n	$url = $_SERVER[''REQUEST_URI''];\n	$segments = explode("/",$url);\n	if ($segments[3] != "") {\n		$pagination = $segments[3];\n		$offset = substr($pagination, 1);\n	} else {\n		$offset = 0;\n	}\n	\n?>\n\n<ul id="switch" class="pie clearfix">\n\n	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	\n	{if segment_2=="motion"}\n	\n	\n	\n	{video_upload}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{start_image}" width="95" height="95" crop="yes"}</a></li>\n	{/video_upload}\n			\n	{if:else}\n	\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	\n	{/if}\n	{/exp:channel:entries}\n	\n</ul>'),
(44, 8, 'exp_templates', 'template_data', 1382536061, 1, '<?php\r\n\r\n	$url = $_SERVER[''REQUEST_URI''];\r\n	$segments = explode("/",$url);\r\n	if ($segments[2] != "") {\r\n		$pagination = $segments[2];\r\n		$offset = substr($pagination, 1);\r\n	} \r\n	\r\n?>\r\n<ul id="switch" class="pie clearfix">\r\n	{if segment_1=="creative"}\r\n	{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n	{if segment_1=="photography"}\r\n	{exp:channel:entries channel="food|product|location|personal" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n	{if segment_1=="digital"}\r\n	{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n</ul>'),
(45, 11, 'exp_templates', 'template_data', 1382536061, 1, '<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n\n	{!-- Home --}\n	<url> \n		<loc>{site_url}</loc> \n		<lastmod>{exp:stats}{last_entry_date format="{DATE_W3C}"}{/exp:stats}</lastmod> \n		<changefreq>always</changefreq> \n		<priority>1.0</priority> \n	</url> \n\n\n	{!--Static Content Pages --}\n	{exp:channel:entries channel="not websites|banners|eCRM|Motion|packaging|studio|food|branding|direct-mail|testimonials|personal|store-comms|product|location" disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{if change_frequency}{change_frequency}{if:else}Daily{/if}</changefreq> \n		<priority>{if priority}{priority}{if:else}0.5{/if}</priority> \n	</url> \n	{/exp:channel:entries}\n\n	{!--Generated Content Pages --}\n	{exp:channel:entries channel="websites|banners|eCRM|Motion|packaging|studio|food|branding|direct-mail|testimonials|personal|store-comms|product|location" disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{if change_frequency}{change_frequency}{if:else}Monthly{/if}</changefreq> \n		<priority>{if priority}{priority}{if:else}0.1{/if}</priority> \n	</url> \n	{/exp:channel:entries}\n\n</urlset> '),
(46, 1, 'exp_templates', 'template_data', 1382536061, 1, '{lv_doc_header}\n	</head>\n	<body class="home">\n	{lv_chrome_frame}\n	{lv_header}\n	<div class="container content">\n		{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n		<div class="sixteen columns row content-inner alpha omega">\n			<h1 class="eight columns omega brand">{lv_brand}</h1>\n			<h1 class="four columns page-header alpha">{title}</h1>\n			<div class="four columns offset-by-four omega header-contact">\n				{lv_sidebar_address}\n			</div>\n		</div>\n		<div class="container">\n			<div class="sixteen columns">\n				<div id="matrix">\n				 		<ul>\n				 		{cf_rotating_image_blocks}\n				 		{if count == 1}\n				 		<div class="slide-img" id="slide-area">\n				 			{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}\n				 		</div>\n				 		<ul id="slides">\n				 		{/if}\n				 		<li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n				 		{if count == total_files}\n				 		</ul>\n				 		{/if}\n				 		{/cf_rotating_image_blocks}\n				 		</ul>\n				 	</div>\n			</div>\n			<div class="eight columns pull-quote omega">\n					{homepage_pull_quote}\n			</div>\n			<div class="eight columns intro alpha">\n				{cf_introduction_text}\n			</div>\n		</div>\n		{/exp:channel:entries}\n	</div><!-- container -->\n		{lv_footer}\n        {lv_base_scripts}\n    </body>\n</html>\n'),
(47, 1, 'exp_templates', 'template_data', 1382536158, 1, '{lv_doc_header}\n	</head>\n	<body class="home">\n	{lv_chrome_frame}\n	{lv_header}\n	<div class="container content">\n		{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n		<div class="sixteen columns row content-inner alpha omega">\n			<h1 class="eight columns omega brand">{lv_brand}</h1>\n			<h1 class="four columns page-header alpha">{title}</h1>\n			<div class="four columns offset-by-four omega header-contact">\n				{lv_sidebar_address}\n			</div>\n		</div>\n		<div class="container">\n			<div class="sixteen columns slider">\n				<div id="matrix">\n				 		<ul>\n				 		{cf_rotating_image_blocks}\n				 		{if count == 1}\n				 		<div class="slide-img" id="slide-area">\n				 			{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}\n				 		</div>\n				 		<ul id="slides">\n				 		{/if}\n				 		<li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n				 		{if count == total_files}\n				 		</ul>\n				 		{/if}\n				 		{/cf_rotating_image_blocks}\n				 		</ul>\n				 	</div>\n			</div>\n			<div class="eight columns pull-quote omega">\n					{homepage_pull_quote}\n			</div>\n			<div class="eight columns intro alpha">\n				{cf_introduction_text}\n			</div>\n		</div>\n		{/exp:channel:entries}\n	</div><!-- container -->\n		{lv_footer}\n        {lv_base_scripts}\n    </body>\n</html>\n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_tools`
--

CREATE TABLE IF NOT EXISTS `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_rte_tools`
--

INSERT INTO `exp_rte_tools` (`tool_id`, `name`, `class`, `enabled`) VALUES
(1, 'Blockquote', 'Blockquote_rte', 'y'),
(2, 'Bold', 'Bold_rte', 'y'),
(3, 'Headings', 'Headings_rte', 'y'),
(4, 'Image', 'Image_rte', 'y'),
(5, 'Italic', 'Italic_rte', 'y'),
(6, 'Link', 'Link_rte', 'y'),
(7, 'Ordered List', 'Ordered_list_rte', 'y'),
(8, 'Underline', 'Underline_rte', 'y'),
(9, 'Unordered List', 'Unordered_list_rte', 'y'),
(10, 'View Source', 'View_source_rte', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_toolsets`
--

CREATE TABLE IF NOT EXISTS `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_rte_toolsets`
--

INSERT INTO `exp_rte_toolsets` (`toolset_id`, `member_id`, `name`, `tools`, `enabled`) VALUES
(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_search`
--

CREATE TABLE IF NOT EXISTS `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_search_log`
--

CREATE TABLE IF NOT EXISTS `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_security_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_sessions`
--

CREATE TABLE IF NOT EXISTS `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_sites`
--

CREATE TABLE IF NOT EXISTS `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` longtext,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_sites`
--

INSERT INTO `exp_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`, `site_pages`) VALUES
(1, 'Shoot the Moon', 'default_site', NULL, 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MDoiIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjM2OiJodHRwOi8vc3RtLnN0bXByZXZpZXcuY28udWsvL3RoZW1lcy8iO3M6MTU6IndlYm1hc3Rlcl9lbWFpbCI7czoyNzoibWFydGluQHNob290LXRoZS1tb29uLmNvLnVrIjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czo0NToiaHR0cDovL3N0bS5zdG1wcmV2aWV3LmNvLnVrLy9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NjQ6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMvc3RtL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czozOiJVVEMiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6Im4iO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czozOiJVVEMiO3M6MTY6ImRlZmF1bHRfc2l0ZV9kc3QiO3M6MToibiI7czoxNToiaG9ub3JfZW50cnlfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6Mzk6Imh0dHA6Ly9zdG0td2Vic2l0ZTo4ODg4L2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo1NToiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG0vdGhlbWVzLyI7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6NDQ6Imh0dHA6Ly9zdG0uc3RtcHJldmlldy5jby51ay8vaW1hZ2VzL2F2YXRhcnMvIjtzOjExOiJhdmF0YXJfcGF0aCI7czo2MzoiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG0vaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjUwOiJodHRwOi8vc3RtLnN0bXByZXZpZXcuY28udWsvL2ltYWdlcy9tZW1iZXJfcGhvdG9zLyI7czoxMDoicGhvdG9fcGF0aCI7czo2OToiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG0vaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTg6Imh0dHA6Ly9zdG0uc3RtcHJldmlldy5jby51ay8vaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTI6InNpZ19pbWdfcGF0aCI7czo3NzoiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG0vaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo3MDoiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG0vaW1hZ2VzL3BtX2F0dGFjaG1lbnRzLyI7czoyMzoicHJ2X21zZ19tYXhfYXR0YWNobWVudHMiO3M6MToiMyI7czoyMjoicHJ2X21zZ19hdHRhY2hfbWF4c2l6ZSI7czozOiIyNTAiO3M6MjA6InBydl9tc2dfYXR0YWNoX3RvdGFsIjtzOjM6IjEwMCI7czoxOToicHJ2X21zZ19odG1sX2Zvcm1hdCI7czo0OiJzYWZlIjtzOjE4OiJwcnZfbXNnX2F1dG9fbGlua3MiO3M6MToieSI7czoxNzoicHJ2X21zZ19tYXhfY2hhcnMiO3M6NDoiNjAwMCI7czoxOToibWVtYmVybGlzdF9vcmRlcl9ieSI7czoxMToidG90YWxfcG9zdHMiO3M6MjE6Im1lbWJlcmxpc3Rfc29ydF9vcmRlciI7czo0OiJkZXNjIjtzOjIwOiJtZW1iZXJsaXN0X3Jvd19saW1pdCI7czoyOiIyMCI7fQ==', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJ5IjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo1ODoiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG0vdGVtcGxhdGVzLyI7fQ==', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YTo0OntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NjY6Ii9Vc2Vycy9raWVyYW5kdWZmL1NpdGVzL3Nob290dGhlbW9vbi9zdG0tMDEwMDAwMS13ZWIvd3d3L2luZGV4LnBocCI7czozMjoiYTJlYmE4YWQwMjc4NzAwZGQ0ZTljNjQ2MTNlMjJlMmYiO3M6NjA6Ii9Vc2Vycy9raWVyYW5kdWZmL1NpdGVzL3N0bS1yZXNwb25zaXZlLXZlcnNpb24vd3d3L2luZGV4LnBocCI7czozMjoiYTJlYmE4YWQwMjc4NzAwZGQ0ZTljNjQ2MTNlMjJlMmYiO3M6NTc6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMvc3RtL2luZGV4LnBocCI7czozMjoiYTJlYmE4YWQwMjc4NzAwZGQ0ZTljNjQ2MTNlMjJlMmYiO30=', 'YToxOntpOjE7YTozOntzOjM6InVybCI7czoyMDoiaHR0cDovL3Nob290dGhlbW9vbi8iO3M6NDoidXJpcyI7YToxMjQ6e2k6MTtzOjE6Ii8iO2k6MjE4O3M6MTM6Ii9vdXItdGhpbmtpbmciO2k6MjtzOjk6Ii9jcmVhdGl2ZSI7aToxMztzOjE4OiIvY3JlYXRpdmUvYnJhbmRpbmciO2k6MTA7czoxOToiL2NyZWF0aXZlL3BhY2thZ2luZyI7aTozMjtzOjIxOiIvY3JlYXRpdmUvZGlyZWN0LW1haWwiO2k6MTE7czoyMToiL2NyZWF0aXZlL3N0b3JlLWNvbW1zIjtpOjQ7czoxMjoiL3Bob3RvZ3JhcGh5IjtpOjE4O3M6MTc6Ii9waG90b2dyYXBoeS9mb29kIjtpOjE5O3M6MjA6Ii9waG90b2dyYXBoeS9wcm9kdWN0IjtpOjIwO3M6MjE6Ii9waG90b2dyYXBoeS9sb2NhdGlvbiI7aToyMTtzOjIxOiIvcGhvdG9ncmFwaHkvcGVyc29uYWwiO2k6MztzOjg6Ii9kaWdpdGFsIjtpOjE0O3M6MTc6Ii9kaWdpdGFsL3dlYnNpdGVzIjtpOjIwMjtzOjE1OiIvZGlnaXRhbC9tb3Rpb24iO2k6MTU7czoxMzoiL2RpZ2l0YWwvZWNybSI7aToyMTE7czoxNjoiL2RpZ2l0YWwvYmFubmVycyI7aTo1O3M6MTM6Ii9nZXQtaW4tdG91Y2giO2k6MzE7czoxMzoiL3Rlc3RpbW9uaWFscyI7aTozODtzOjMxOiIvY3JlYXRpdmUvYnJhbmRpbmcvYnJhbmRpbmctb25lIjtpOjM5O3M6MzE6Ii9jcmVhdGl2ZS9icmFuZGluZy9icmFuZGluZy10d28iO2k6MTE0O3M6MjU6Ii9jcmVhdGl2ZS9icmFuZGluZy9icmFuZDMiO2k6MTE1O3M6MjU6Ii9jcmVhdGl2ZS9icmFuZGluZy9icmFuZDQiO2k6MTE2O3M6MjU6Ii9jcmVhdGl2ZS9icmFuZGluZy9icmFuZDUiO2k6MTE4O3M6MjU6Ii9jcmVhdGl2ZS9icmFuZGluZy9icmFuZDciO2k6MTg0O3M6NDc6Ii9jcmVhdGl2ZS9icmFuZGluZy9raWRzLWhvbGlkYXktcGV0LWNsdWItd2ludGVyIjtpOjE4NTtzOjQ3OiIvY3JlYXRpdmUvYnJhbmRpbmcva2lkcy1ob2xpZGF5LXBldC1jbHViLXN1bW1lciI7aToxODY7czozMDoiL2NyZWF0aXZlL2JyYW5kaW5nL3Zlcm8tZ2VsYXRvIjtpOjE4NztzOjMxOiIvY3JlYXRpdmUvYnJhbmRpbmcvcG91bmQtYmFrZXJ5IjtpOjE4OTtzOjI1OiIvY3JlYXRpdmUvYnJhbmRpbmcvc2F5ZXJzIjtpOjE5MDtzOjMwOiIvY3JlYXRpdmUvYnJhbmRpbmcvZ29vZG5lc3MtbWUiO2k6MzM7czozMzoiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmctb25lIjtpOjM0O3M6MzM6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvcGFja2FnaW5nLXR3byI7aTozNTtzOjM1OiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZy10aHJlZSI7aTozNjtzOjM0OiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZy1mb3VyIjtpOjQxO3M6Mzk6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvbGl0ZXJhdHVyZS1pdGVtLW9uZSI7aToxMjQ7czozMDoiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmc2IjtpOjEyNTtzOjMwOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzciO2k6MTI2O3M6MzA6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvcGFja2FnaW5nOCI7aToxMjc7czozMDoiL2NyZWF0aXZlL3BhY2thZ2luZy9wYWNrYWdpbmc5IjtpOjEyODtzOjMxOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzEwIjtpOjEyOTtzOjMxOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzExIjtpOjEzMDtzOjMxOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzEyIjtpOjEzMTtzOjMxOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzEzIjtpOjEzMjtzOjMxOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzE0IjtpOjEzMztzOjMxOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzE1IjtpOjEzNDtzOjMxOiIvY3JlYXRpdmUvcGFja2FnaW5nL3BhY2thZ2luZzE2IjtpOjE3MDtzOjI5OiIvY3JlYXRpdmUvcGFja2FnaW5nL21hcnJpYWdlcyI7aToxNzE7czozOToiL2NyZWF0aXZlL3BhY2thZ2luZy9oYXJib3VyLXNtb2tlLWhvdXNlIjtpOjE3MjtzOjQwOiIvY3JlYXRpdmUvcGFja2FnaW5nL2RldmlsaXNobHktZGVsaWNpb3VzIjtpOjE3MztzOjI3OiIvY3JlYXRpdmUvcGFja2FnaW5nL2Rpc290dG8iO2k6MTc0O3M6MzI6Ii9jcmVhdGl2ZS9wYWNrYWdpbmcvcGluay1wYW50aGVyIjtpOjQwO3M6MzQ6Ii9jcmVhdGl2ZS9kaXJlY3QtbWFpbC9icm9jaHVyZS1vbmUiO2k6MTIwO3M6MjU6Ii9jcmVhdGl2ZS9kaXJlY3QtbWFpbC9kbTQiO2k6MTM4O3M6MzU6Ii9jcmVhdGl2ZS9kaXJlY3QtbWFpbC9zdG9yZS1jb21tcy02IjtpOjE0MTtzOjM1OiIvY3JlYXRpdmUvZGlyZWN0LW1haWwvc3RvcmUtY29tbXMtOSI7aToxNzU7czozMDoiL2NyZWF0aXZlL2RpcmVjdC1tYWlsL2xha2VsYW5kIjtpOjE3ODtzOjMyOiIvY3JlYXRpdmUvZGlyZWN0LW1haWwvbGFrZWxhbmQtMyI7aToxNzk7czoyOToiL2NyZWF0aXZlL2RpcmVjdC1tYWlsL3Rob21zb24iO2k6MTgwO3M6MzE6Ii9jcmVhdGl2ZS9kaXJlY3QtbWFpbC90aG9tc29uLTEiO2k6MTgxO3M6MzE6Ii9jcmVhdGl2ZS9kaXJlY3QtbWFpbC90aG9tc29uLTIiO2k6NDI7czozOToiL2NyZWF0aXZlL3N0b3JlLWNvbW1zL3BvaW50LW9mLXNhbGUtb25lIjtpOjQzO3M6Mzk6Ii9jcmVhdGl2ZS9zdG9yZS1jb21tcy9wb2ludC1vZi1zYWxlLXR3byI7aToxMzU7czozNToiL2NyZWF0aXZlL3N0b3JlLWNvbW1zL3N0b3JlLWNvbW1zLTMiO2k6MTM3O3M6MzU6Ii9jcmVhdGl2ZS9zdG9yZS1jb21tcy9zdG9yZS1jb21tcy01IjtpOjEzOTtzOjM1OiIvY3JlYXRpdmUvc3RvcmUtY29tbXMvc3RvcmUtY29tbXMtNyI7aToxNDA7czozNToiL2NyZWF0aXZlL3N0b3JlLWNvbW1zL3N0b3JlLWNvbW1zLTgiO2k6MTY5O3M6MzQ6Ii9jcmVhdGl2ZS9zdG9yZS1jb21tcy9waWNrLWFuZC1taXgiO2k6MTQyO3M6MjM6Ii9waG90b2dyYXBoeS9mb29kL2Zvb2QxIjtpOjE0MztzOjIzOiIvcGhvdG9ncmFwaHkvZm9vZC9mb29kMiI7aToxNDQ7czoyMzoiL3Bob3RvZ3JhcGh5L2Zvb2QvZm9vZDMiO2k6MTQ1O3M6MjM6Ii9waG90b2dyYXBoeS9mb29kL2Zvb2Q0IjtpOjE0NztzOjIzOiIvcGhvdG9ncmFwaHkvZm9vZC9mb29kNiI7aToxNDg7czoyMzoiL3Bob3RvZ3JhcGh5L2Zvb2QvZm9vZDciO2k6MTQ5O3M6MjM6Ii9waG90b2dyYXBoeS9mb29kL2Zvb2Q4IjtpOjE1MDtzOjIzOiIvcGhvdG9ncmFwaHkvZm9vZC9mb29kOSI7aToxNTE7czoyNDoiL3Bob3RvZ3JhcGh5L2Zvb2QvZm9vZDEwIjtpOjE1MjtzOjI0OiIvcGhvdG9ncmFwaHkvZm9vZC9mb29kMTEiO2k6MTUzO3M6MjQ6Ii9waG90b2dyYXBoeS9mb29kL2Zvb2QxMiI7aToxNTQ7czoyOToiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvcHJvZHVjdDEiO2k6MTU1O3M6Mjk6Ii9waG90b2dyYXBoeS9wcm9kdWN0L3Byb2R1Y3QyIjtpOjE1NjtzOjI5OiIvcGhvdG9ncmFwaHkvcHJvZHVjdC9wcm9kdWN0MyI7aToxNTc7czoyOToiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvcHJvZHVjdDQiO2k6MTU4O3M6Mjk6Ii9waG90b2dyYXBoeS9wcm9kdWN0L3Byb2R1Y3Q1IjtpOjE1OTtzOjI5OiIvcGhvdG9ncmFwaHkvcHJvZHVjdC9wcm9kdWN0NiI7aToxNjc7czozMToiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvcHJvZHVjdDYtMSI7aToxNjg7czoyOToiL3Bob3RvZ3JhcGh5L3Byb2R1Y3QvcHJvZHVjdDciO2k6MTkxO3M6MzI6Ii9waG90b2dyYXBoeS9wcm9kdWN0L3NwYWluLXNoaXJ0IjtpOjE2MDtzOjMxOiIvcGhvdG9ncmFwaHkvbG9jYXRpb24vbG9jYXRpb24xIjtpOjE2MTtzOjMxOiIvcGhvdG9ncmFwaHkvbG9jYXRpb24vbG9jYXRpb24yIjtpOjE2MjtzOjMxOiIvcGhvdG9ncmFwaHkvcGVyc29uYWwvcGVyc29uYWwxIjtpOjE2MztzOjMxOiIvcGhvdG9ncmFwaHkvcGVyc29uYWwvcGVyc29uYWwyIjtpOjE2NDtzOjMxOiIvcGhvdG9ncmFwaHkvcGVyc29uYWwvcGVyc29uYWwzIjtpOjE2NTtzOjMxOiIvcGhvdG9ncmFwaHkvcGVyc29uYWwvcGVyc29uYWw0IjtpOjE2NjtzOjMyOiIvcGhvdG9ncmFwaHkvcGVyc29uYWwvcGVyc29uYWwtNSI7aTo0NztzOjMxOiIvZGlnaXRhbC93ZWJzaXRlcy9wcmluY2VzLWp1Y2VlIjtpOjQ5O3M6Mzg6Ii9kaWdpdGFsL3dlYnNpdGVzL2RldmlsaXNobHktZGVsaWNpb3VzIjtpOjUwO3M6Mzk6Ii9kaWdpdGFsL3dlYnNpdGVzL21hY3BoaWUtb2YtZ2xlbmJlcnZpZSI7aToxOTI7czozNjoiL2RpZ2l0YWwvd2Vic2l0ZXMvY2F0ZXJmb3JjZS13ZWJzaXRlIjtpOjE5MztzOjM0OiIvZGlnaXRhbC93ZWJzaXRlcy9jaGVmcy1zZWxlY3Rpb25zIjtpOjE5NDtzOjU2OiIvZGlnaXRhbC93ZWJzaXRlcy9kZXZpbGlzaGx5LWRlbGljaW91cy1mYWNlYm9vay1jcmVhdGl2ZSI7aToxOTU7czo0MToiL2RpZ2l0YWwvd2Vic2l0ZXMvbWFjcGhpZS1vZi1nbGVuYmVydmllLTEiO2k6MTk2O3M6Mjc6Ii9kaWdpdGFsL3dlYnNpdGVzL21hcnJpYWdlcyI7aToxOTc7czoyODoiL2RpZ2l0YWwvd2Vic2l0ZXMvbWVhZG93dmFsZSI7aToxOTg7czozNToiL2RpZ2l0YWwvd2Vic2l0ZXMvanVjZWUtc29mdC1kcmlua3MiO2k6MTk5O3M6MzU6Ii9kaWdpdGFsL3dlYnNpdGVzL3NheWVycy10aGUtYmFrZXJ5IjtpOjIwMDtzOjM1OiIvZGlnaXRhbC93ZWJzaXRlcy9zY29ycGlvLXdvcmxkd2lkZSI7aToyMDM7czoyODoiL2RpZ2l0YWwvbW90aW9uL2p1Y2VlLWlkZW50cyI7aToyMDQ7czozNDoiL2RpZ2l0YWwvbW90aW9uL2p1Y2VlLWJhdC1hbmQtYmFsbCI7aToyMDU7czoyNjoiL2RpZ2l0YWwvbW90aW9uL2p1Y2VlLW5ldHMiO2k6MjA2O3M6MzA6Ii9kaWdpdGFsL21vdGlvbi9qdWNlZS11bWJyZWxsYSI7aToyMDc7czoyNDoiL2RpZ2l0YWwvZWNybS9tZWFkb3d2YWxlIjtpOjIwODtzOjI5OiIvZGlnaXRhbC9lY3JtL2ZseS10aG9tYXMtY29vayI7aToyMDk7czoyNjoiL2RpZ2l0YWwvZWNybS9wZXRzLWF0LWhvbWUiO2k6MjEzO3M6MTk6Ii9kaWdpdGFsL2Vjcm0vcnltYW4iO2k6MjE0O3M6MjY6Ii9kaWdpdGFsL2Vjcm0vcG91bmQtYmFrZXJ5IjtpOjIxMjtzOjI5OiIvZGlnaXRhbC9iYW5uZXJzL3BldHMtYXQtaG9tZSI7aToyMTU7czozMDoiL2RpZ2l0YWwvYmFubmVycy9taXhlZC1iYW5uZXJzIjtpOjIxOTtzOjIzOiIvcGhvdG9ncmFwaHkvZmFjaWxpdGllcyI7aToyMjA7czo0NDoiL3Bob3RvZ3JhcGh5L2ZhY2lsaXRpZXMvdGhpcy1pcy1hLXRlc3QtaW1hZ2UiO2k6MjIxO3M6NDM6Ii9jcmVhdGl2ZS9kaXJlY3QtbWFpbC9yeW1hbnMtYmFjay10by1zY2hvb2wiO2k6MjIyO3M6MzE6Ii9jcmVhdGl2ZS9kaXJlY3QtbWFpbC9yaWRlLWF3YXkiO2k6MjIzO3M6Mzg6Ii9jcmVhdGl2ZS9kaXJlY3QtbWFpbC90aG9tc29uLWluZmxpZ2h0IjtpOjIyNDtzOjM5OiIvY3JlYXRpdmUvc3RvcmUtY29tbXMvcnltYW4tc3RvcmUtY29tbXMiO31zOjk6InRlbXBsYXRlcyI7YToxMjQ6e2k6MTtzOjE6IjEiO2k6MjtzOjE6IjMiO2k6NDtzOjE6IjMiO2k6MztzOjE6IjMiO2k6MjE7czoxOiI1IjtpOjE5O3M6MToiNSI7aToxODtzOjE6IjUiO2k6MjA7czoxOiI1IjtpOjE0O3M6MToiNSI7aToxMTtzOjE6IjUiO2k6MTM7czoxOiI1IjtpOjEwO3M6MToiNSI7aTo1O3M6MToiNCI7aTozMTtzOjE6IjEiO2k6MzI7czoxOiI1IjtpOjMzO3M6MToiNSI7aTozNDtzOjE6IjUiO2k6MzU7czoxOiI1IjtpOjM2O3M6MToiNSI7aTozODtzOjE6IjUiO2k6Mzk7czoxOiI1IjtpOjQwO3M6MToiNSI7aTo0MTtzOjE6IjUiO2k6NDI7czoxOiI1IjtpOjQzO3M6MToiNSI7aTo0NztzOjE6IjUiO2k6NDk7czoxOiI1IjtpOjUwO3M6MToiNSI7aToxMTQ7czoxOiI1IjtpOjExNTtzOjE6IjUiO2k6MTE2O3M6MToiNSI7aToxMTg7czoxOiI1IjtpOjEyMDtzOjE6IjUiO2k6MTI0O3M6MToiNSI7aToxMjU7czoxOiI1IjtpOjEyNjtzOjE6IjUiO2k6MTI3O3M6MToiNSI7aToxMjg7czoxOiI1IjtpOjEyOTtzOjE6IjUiO2k6MTMwO3M6MToiNSI7aToxMzE7czoxOiI1IjtpOjEzMjtzOjE6IjUiO2k6MTMzO3M6MToiNSI7aToxMzQ7czoxOiI1IjtpOjEzNTtzOjE6IjUiO2k6MTM3O3M6MToiNSI7aToxMzg7czoxOiI1IjtpOjEzOTtzOjE6IjUiO2k6MTQwO3M6MToiNSI7aToxNDE7czoxOiI1IjtpOjE0MjtzOjE6IjUiO2k6MTQzO3M6MToiNSI7aToxNDQ7czoxOiI1IjtpOjE0NTtzOjE6IjUiO2k6MTQ3O3M6MToiNSI7aToxNDg7czoxOiI1IjtpOjE0OTtzOjE6IjUiO2k6MTUwO3M6MToiNSI7aToxNTE7czoxOiI1IjtpOjE1MjtzOjE6IjUiO2k6MTUzO3M6MToiNSI7aToxNTQ7czoxOiI1IjtpOjE1NTtzOjE6IjUiO2k6MTU2O3M6MToiNSI7aToxNTc7czoxOiI1IjtpOjE1ODtzOjE6IjUiO2k6MTU5O3M6MToiNSI7aToxNjA7czoxOiI1IjtpOjE2MTtzOjE6IjUiO2k6MTYyO3M6MToiNSI7aToxNjM7czoxOiI1IjtpOjE2NDtzOjE6IjUiO2k6MTY1O3M6MToiNSI7aToxNjY7czoxOiI1IjtpOjE2NztzOjE6IjUiO2k6MTY4O3M6MToiNSI7aToxNjk7czoxOiI1IjtpOjE3MDtzOjE6IjUiO2k6MTcxO3M6MToiNSI7aToxNzI7czoxOiI1IjtpOjE3MztzOjE6IjUiO2k6MTc0O3M6MToiNSI7aToxNzU7czoxOiI1IjtpOjE3ODtzOjE6IjUiO2k6MTc5O3M6MToiNSI7aToxODA7czoxOiI1IjtpOjE4MTtzOjE6IjUiO2k6MTg0O3M6MToiNSI7aToxODU7czoxOiI1IjtpOjE4NjtzOjE6IjUiO2k6MTg3O3M6MToiNSI7aToxODk7czoxOiI1IjtpOjE5MDtzOjE6IjUiO2k6MTkxO3M6MToiNSI7aToxOTI7czoxOiI1IjtpOjE5MztzOjE6IjUiO2k6MTk0O3M6MToiNSI7aToxOTU7czoxOiI1IjtpOjE5NjtzOjE6IjUiO2k6MTk3O3M6MToiNSI7aToxOTg7czoxOiI1IjtpOjE5OTtzOjE6IjUiO2k6MjAwO3M6MToiNSI7aToyMDI7czoxOiI1IjtpOjIwMztzOjE6IjUiO2k6MjA0O3M6MToiNSI7aToyMDU7czoxOiI1IjtpOjIwNjtzOjE6IjUiO2k6MTU7czoxOiI1IjtpOjIwNztzOjE6IjUiO2k6MjA4O3M6MToiNSI7aToyMDk7czoxOiI1IjtpOjIxMTtzOjE6IjUiO2k6MjEyO3M6MToiNSI7aToyMTM7czoxOiI1IjtpOjIxNDtzOjE6IjUiO2k6MjE1O3M6MToiNSI7aToyMTg7czoyOiIxMCI7aToyMTk7czoxOiI1IjtpOjIyMDtzOjE6IjUiO2k6MjIxO3M6MToiNSI7aToyMjI7czoxOiI1IjtpOjIyMztzOjE6IjUiO2k6MjI0O3M6MToiNSI7fX19');

-- --------------------------------------------------------

--
-- Table structure for table `exp_snippets`
--

CREATE TABLE IF NOT EXISTS `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_specialty_templates`
--

CREATE TABLE IF NOT EXISTS `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  `sync_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `exp_specialty_templates`
--

INSERT INTO `exp_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`, `sync_time`) VALUES
(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>', '0000-00-00 00:00:00'),
(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}', '0000-00-00 00:00:00'),
(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n', '0000-00-00 00:00:00'),
(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}', '0000-00-00 00:00:00'),
(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}', '0000-00-00 00:00:00'),
(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}', '0000-00-00 00:00:00'),
(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}', '0000-00-00 00:00:00'),
(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}', '0000-00-00 00:00:00'),
(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}', '0000-00-00 00:00:00'),
(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `exp_stats`
--

CREATE TABLE IF NOT EXISTS `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_stats`
--

INSERT INTO `exp_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`) VALUES
(1, 1, 2, 2, 'Editor', 130, 0, 0, 0, 1379947005, 0, 0, 1396255994, 28, 1360332289, 1396724394);

-- --------------------------------------------------------

--
-- Table structure for table `exp_statuses`
--

CREATE TABLE IF NOT EXISTS `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_statuses`
--

INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`) VALUES
(1, 1, 1, 'open', 1, '009933'),
(2, 1, 1, 'closed', 2, '990000'),
(3, 1, 2, 'closed', 1, '990000'),
(4, 1, 2, 'draft', 2, 'B59A42'),
(5, 1, 2, 'submitted', 3, '3E6C89'),
(6, 1, 2, 'open', 4, '009933');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_groups`
--

CREATE TABLE IF NOT EXISTS `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_status_groups`
--

INSERT INTO `exp_status_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Statuses'),
(2, 1, 'Better Workflow');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure`
--

CREATE TABLE IF NOT EXISTS `exp_structure` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `listing_cid` int(6) unsigned NOT NULL DEFAULT '0',
  `lft` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rgt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dead` varchar(100) NOT NULL,
  `hidden` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure`
--

INSERT INTO `exp_structure` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `listing_cid`, `lft`, `rgt`, `dead`, `hidden`) VALUES
(0, 0, 0, 0, 0, 1, 42, 'root', 'n'),
(1, 1, 0, 6, 0, 2, 3, '', 'n'),
(1, 2, 0, 1, 0, 6, 15, '', 'n'),
(1, 3, 0, 1, 0, 28, 37, '', 'n'),
(1, 4, 0, 1, 0, 16, 27, '', 'n'),
(1, 5, 0, 1, 0, 38, 39, '', 'n'),
(1, 10, 2, 1, 12, 9, 10, '', 'n'),
(1, 11, 2, 1, 14, 13, 14, '', 'n'),
(1, 13, 2, 1, 15, 7, 8, '', 'n'),
(1, 14, 3, 3, 20, 29, 30, '', 'n'),
(1, 15, 3, 3, 19, 33, 34, '', 'n'),
(1, 18, 4, 1, 7, 17, 18, '', 'n'),
(1, 19, 4, 1, 8, 19, 20, '', 'n'),
(1, 20, 4, 1, 9, 21, 22, '', 'n'),
(1, 21, 4, 1, 10, 23, 24, '', 'n'),
(1, 31, 0, 11, 0, 40, 41, '', 'n'),
(1, 32, 2, 1, 16, 11, 12, '', 'n'),
(1, 202, 3, 1, 22, 31, 32, '', 'n'),
(1, 211, 3, 1, 21, 35, 36, '', 'n'),
(1, 218, 0, 6, 0, 4, 5, '', 'n'),
(1, 219, 4, 1, 23, 25, 26, '', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_channels`
--

CREATE TABLE IF NOT EXISTS `exp_structure_channels` (
  `site_id` smallint(5) unsigned NOT NULL,
  `channel_id` mediumint(8) unsigned NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `type` enum('page','listing','asset','unmanaged') NOT NULL DEFAULT 'unmanaged',
  `split_assets` enum('y','n') NOT NULL DEFAULT 'n',
  `show_in_page_selector` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`site_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_channels`
--

INSERT INTO `exp_structure_channels` (`site_id`, `channel_id`, `template_id`, `type`, `split_assets`, `show_in_page_selector`) VALUES
(1, 1, 3, 'page', 'n', 'y'),
(1, 2, 3, 'page', 'n', 'y'),
(1, 3, 3, 'page', 'n', 'y'),
(1, 4, 3, 'page', 'n', 'y'),
(1, 5, 1, 'page', 'n', 'y'),
(1, 6, 1, 'page', 'n', 'y'),
(1, 7, 5, 'listing', 'n', 'y'),
(1, 8, 5, 'listing', 'n', 'y'),
(1, 9, 5, 'listing', 'n', 'y'),
(1, 10, 5, 'listing', 'n', 'y'),
(1, 11, 1, 'page', 'n', 'y'),
(1, 12, 5, 'listing', 'n', 'y'),
(1, 13, 5, 'listing', 'n', 'y'),
(1, 14, 5, 'listing', 'n', 'y'),
(1, 15, 5, 'listing', 'n', 'y'),
(1, 16, 5, 'listing', 'n', 'y'),
(1, 17, 1, 'listing', 'n', 'y'),
(1, 18, 5, 'listing', 'n', 'y'),
(1, 19, 5, 'listing', 'n', 'y'),
(1, 20, 5, 'listing', 'n', 'y'),
(1, 21, 5, 'listing', 'n', 'y'),
(1, 22, 5, 'listing', 'n', 'y'),
(1, 23, 5, 'listing', 'n', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_listings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_listings` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `template_id` int(6) unsigned NOT NULL DEFAULT '0',
  `uri` varchar(75) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_listings`
--

INSERT INTO `exp_structure_listings` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `template_id`, `uri`) VALUES
(1, 33, 10, 12, 5, 'packaging-one'),
(1, 34, 10, 12, 5, 'packaging-two'),
(1, 35, 10, 12, 5, 'packaging-three'),
(1, 36, 10, 12, 5, 'packaging-four'),
(1, 38, 13, 15, 5, 'branding-one'),
(1, 39, 13, 15, 5, 'branding-two'),
(1, 40, 32, 16, 5, 'brochure-one'),
(1, 41, 10, 12, 5, 'literature-item-one'),
(1, 42, 11, 14, 5, 'point-of-sale-one'),
(1, 43, 11, 14, 5, 'point-of-sale-two'),
(1, 47, 14, 20, 5, 'princes-jucee'),
(1, 49, 14, 20, 5, 'devilishly-delicious'),
(1, 50, 14, 20, 5, 'macphie-of-glenbervie'),
(1, 114, 13, 15, 5, 'brand3'),
(1, 115, 13, 15, 5, 'brand4'),
(1, 116, 13, 15, 5, 'brand5'),
(1, 118, 13, 15, 5, 'brand7'),
(1, 120, 32, 16, 5, 'dm4'),
(1, 124, 10, 12, 5, 'packaging6'),
(1, 125, 10, 12, 5, 'packaging7'),
(1, 126, 10, 12, 5, 'packaging8'),
(1, 127, 10, 12, 5, 'packaging9'),
(1, 128, 10, 12, 5, 'packaging10'),
(1, 129, 10, 12, 5, 'packaging11'),
(1, 130, 10, 12, 5, 'packaging12'),
(1, 131, 10, 12, 5, 'packaging13'),
(1, 132, 10, 12, 5, 'packaging14'),
(1, 133, 10, 12, 5, 'packaging15'),
(1, 134, 10, 12, 5, 'packaging16'),
(1, 135, 11, 14, 5, 'store-comms-3'),
(1, 137, 11, 14, 5, 'store-comms-5'),
(1, 138, 32, 16, 5, 'store-comms-6'),
(1, 139, 11, 14, 5, 'store-comms-7'),
(1, 140, 11, 14, 5, 'store-comms-8'),
(1, 141, 32, 16, 5, 'store-comms-9'),
(1, 142, 18, 7, 5, 'food1'),
(1, 143, 18, 7, 5, 'food2'),
(1, 144, 18, 7, 5, 'food3'),
(1, 145, 18, 7, 5, 'food4'),
(1, 147, 18, 7, 5, 'food6'),
(1, 148, 18, 7, 5, 'food7'),
(1, 149, 18, 7, 5, 'food8'),
(1, 150, 18, 7, 5, 'food9'),
(1, 151, 18, 7, 5, 'food10'),
(1, 152, 18, 7, 5, 'food11'),
(1, 153, 18, 7, 5, 'food12'),
(1, 154, 19, 8, 5, 'product1'),
(1, 155, 19, 8, 5, 'product2'),
(1, 156, 19, 8, 5, 'product3'),
(1, 157, 19, 8, 5, 'product4'),
(1, 158, 19, 8, 5, 'product5'),
(1, 159, 19, 8, 5, 'product6'),
(1, 160, 20, 9, 5, 'location1'),
(1, 161, 20, 9, 5, 'location2'),
(1, 162, 21, 10, 5, 'personal1'),
(1, 163, 21, 10, 5, 'personal2'),
(1, 164, 21, 10, 5, 'personal3'),
(1, 165, 21, 10, 5, 'personal4'),
(1, 166, 21, 10, 5, 'personal-5'),
(1, 167, 19, 8, 5, 'product6-1'),
(1, 168, 19, 8, 5, 'product7'),
(1, 169, 11, 14, 5, 'pick-and-mix'),
(1, 170, 10, 12, 5, 'marriages'),
(1, 171, 10, 12, 5, 'harbour-smoke-house'),
(1, 172, 10, 12, 5, 'devilishly-delicious'),
(1, 173, 10, 12, 5, 'disotto'),
(1, 174, 10, 12, 5, 'pink-panther'),
(1, 175, 32, 16, 5, 'lakeland'),
(1, 178, 32, 16, 5, 'lakeland-3'),
(1, 179, 32, 16, 5, 'thomson'),
(1, 180, 32, 16, 5, 'thomson-1'),
(1, 181, 32, 16, 5, 'thomson-2'),
(1, 184, 13, 15, 5, 'kids-holiday-pet-club-winter'),
(1, 185, 13, 15, 5, 'kids-holiday-pet-club-summer'),
(1, 186, 13, 15, 5, 'vero-gelato'),
(1, 187, 13, 15, 5, 'pound-bakery'),
(1, 189, 13, 15, 5, 'sayers'),
(1, 190, 13, 15, 5, 'goodness-me'),
(1, 191, 19, 8, 5, 'spain-shirt'),
(1, 192, 14, 20, 5, 'caterforce-website'),
(1, 193, 14, 20, 5, 'chefs-selections'),
(1, 194, 14, 20, 5, 'devilishly-delicious-facebook-creative'),
(1, 195, 14, 20, 5, 'macphie-of-glenbervie-1'),
(1, 196, 14, 20, 5, 'marriages'),
(1, 197, 14, 20, 5, 'meadowvale'),
(1, 198, 14, 20, 5, 'jucee-soft-drinks'),
(1, 199, 14, 20, 5, 'sayers-the-bakery'),
(1, 200, 14, 20, 5, 'scorpio-worldwide'),
(1, 203, 202, 22, 5, 'jucee-idents'),
(1, 204, 202, 22, 5, 'jucee-bat-and-ball'),
(1, 205, 202, 22, 5, 'jucee-nets'),
(1, 206, 202, 22, 5, 'jucee-umbrella'),
(1, 207, 15, 19, 5, 'meadowvale'),
(1, 208, 15, 19, 5, 'fly-thomas-cook'),
(1, 209, 15, 19, 5, 'pets-at-home'),
(1, 212, 211, 21, 5, 'pets-at-home'),
(1, 213, 15, 19, 5, 'ryman'),
(1, 214, 15, 19, 5, 'pound-bakery'),
(1, 215, 211, 21, 5, 'mixed-banners'),
(1, 220, 219, 23, 5, 'this-is-a-test-image'),
(1, 221, 32, 16, 5, 'rymans-back-to-school'),
(1, 222, 32, 16, 5, 'ride-away'),
(1, 223, 32, 16, 5, 'thomson-inflight'),
(1, 224, 11, 14, 5, 'ryman-store-comms');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_members`
--

CREATE TABLE IF NOT EXISTS `exp_structure_members` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `nav_state` text,
  PRIMARY KEY (`site_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_members`
--

INSERT INTO `exp_structure_members` (`member_id`, `site_id`, `nav_state`) VALUES
(1, 1, 'false');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_settings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `var` varchar(60) NOT NULL,
  `var_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `exp_structure_settings`
--

INSERT INTO `exp_structure_settings` (`id`, `site_id`, `var`, `var_value`) VALUES
(1, 0, 'action_ajax_move', '48'),
(2, 0, 'module_id', '24'),
(10, 1, 'redirect_on_login', 'y'),
(11, 1, 'redirect_on_publish', 'y'),
(12, 1, 'hide_hidden_templates', 'y'),
(13, 1, 'add_trailing_slash', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_templates`
--

CREATE TABLE IF NOT EXISTS `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `exp_templates`
--

INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(1, 1, 1, 'index', 'y', 'webpage', '{lv_doc_header}\n	</head>\n	<body class="home">\n	{lv_chrome_frame}\n	{lv_header}\n	<div class="container content">\n		{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n		<div class="sixteen columns row content-inner alpha omega">\n			<h1 class="eight columns omega brand">{lv_brand}</h1>\n			<h1 class="four columns page-header alpha">{title}</h1>\n			<div class="four columns offset-by-four omega header-contact">\n				{lv_sidebar_address}\n			</div>\n		</div>\n		<div class="container">\n			<div class="sixteen columns slider">\n				<div id="matrix">\n				 		<ul>\n				 		{cf_rotating_image_blocks}\n				 		{if count == 1}\n				 		<div class="slide-img" id="slide-area">\n				 			{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}\n				 		</div>\n				 		<ul id="slides">\n				 		{/if}\n				 		<li class="{switch=''odd|even''}">{exp:ce_img:single src="{url}" width="470" height="320" crop="yes" allow_scale_larger="yes"}</li>\n				 		{if count == total_files}\n				 		</ul>\n				 		{/if}\n				 		{/cf_rotating_image_blocks}\n				 		</ul>\n				 	</div>\n			</div>\n			<div class="eight columns pull-quote omega">\n					{homepage_pull_quote}\n			</div>\n			<div class="eight columns intro alpha">\n				{cf_introduction_text}\n			</div>\n		</div>\n		{/exp:channel:entries}\n	</div><!-- container -->\n		{lv_footer}\n        {lv_base_scripts}\n    </body>\n</html>\n', '', 1382536158, 1, 'n', 0, '', 'n', 'n', 'o', 12021),
(3, 1, 1, 'landing', 'y', 'webpage', '{lv_doc_header}\n	</head>\n	<body class="landing">\n	{lv_chrome_frame}\n	{lv_header}\n	<div class="container">\n		<div class="eleven columns alpha omega content">\n			<div class="twelve columns row content-inner alpha omega">\n				<h1 class="six columns omega brand">{lv_brand}</h1>\n				{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				<h1 class="four columns page-header alpha">{title}</h1>\n			</div>\n			<div class="eleven omega discipline-display">\n				<div class="disp-tab">\n					{lv_folio_nav}\n				</div>\n				<div class="disp-header">{cf_introduction_text}</div>\n				{/exp:channel:entries}\n				<div class="disp-content">\n					<ul>\n					{if segment_1=="creative"}\n						{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n					{/if}\n					{if segment_1=="photography"}\n						{exp:channel:entries channel="food|product|location|personal|facilities" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n					{/if}\n					{if segment_1=="digital"}\n						{exp:channel:entries channel="ecrm|websites|banners|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" paginate="bottom"}\n					{/if}\n\n					{cf_portfolio_images}\n						<li id="id{entry_id}">\n							<span data-picture data-alt="Portfolio Image">\n    							<span data-src="{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''300'' output=''{made}''}"></span>\n    							<span data-src="{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''380'' output=''{made}''}" data-media="(min-width: 400px)"></span>\n    							<span data-src="{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''780'' output=''{made}''}" data-media="(min-width: 800px)"></span>\n    							<span data-src="{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''980'' output=''{made}''}" data-media="(min-width: 1000px)"></span>\n    							<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->\n    							<noscript>\n    								{exp:ce_img:single src=''{cf_mx_portfolio_image}'' width=''640'' output=''<img src="{made}" alt="Portfolio Image">''}\n    							</noscript>\n							</span>\n   							{if cf_mx_supporting_text}\n								<div class="img-txt">\n   									{cf_mx_supporting_text}\n   								</div>\n							{/if}\n						</li>\n					{/cf_portfolio_images}\n\n		        	{paginate}\n		        		<p class="paginate">	\n	    	    		    {if previous_page}\n	        			    	<a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\n	        			    {/if}\n	        			    {if next_page}\n	        			    	<a href="{auto_path}">More examples</a>\n		        		    {/if}\n		        	    </p>\n	    	    	{/paginate}\n			\n						        				        		\n          			{/exp:channel:entries}\n          			<ul>\n				</div>\n			</div>\n		</div>\n		<div class="five columns alpha omega side-content">\n			{lv_sidebar_address}\n			{lv_blockquote}\n		</div>\n		<div class="side-thumbs">\n			{embed="sites/thumb-nav"}\n			{!--lv_thumbs_nav--}\n		</div>\n	</div><!-- container -->\n	<a href="#" class="return">&uarr;</a>\n	{lv_footer}\n	<div class="bg"></div>\n    {lv_base_scripts}\n    </body>\n</html>\n', '', 1382536061, 1, 'n', 0, '', 'n', 'n', 'o', 3806),
(4, 1, 1, 'get-in-touch', 'y', 'webpage', '{lv_doc_header}\n	</head>\n	<body>\n	{lv_chrome_frame}\n	{lv_header}\n	<div class="container">\n		{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n		<div class="eleven columns alpha omega content contact">\n			<div class="twelve columns row content-inner alpha omega">\n				<h1 class="six columns omega brand">{lv_brand}</h1>\n				<h1 class="four columns page-header alpha">{title}</h1>\n			</div>\n			<div class="eleven contact-form">\n				<div id="intro">\n				{cf_introduction_text}\n				{/exp:channel:entries}\n				</div>\n\n				{exp:freeform:form collection="Enquiries" form:class="contact-form" required="name|email|contact_number|message"}\n				<input type="hidden" name="recipient_email" value="martin@shoot-the-moon.co.uk">\n        		<label>First Name*</label>\n        		<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n				<label>Surname*</label>\n				<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n        		<label>Email Address*</label>\n        		<input type="text" name="email" value="" class="required" placeholder="Your email address">\n        		<label>Message*</label>\n        		<textarea name="message" class="required" placeholder="Please leave a small message and we''ll be in-touch."></textarea>\n        		<input type="submit" name="submit" value="Submit your enquiry" class="submit">\n        		{/exp:freeform:form}\n\n			</div>\n		</div>\n		<div class="five columns alpha omega side-content">\n			{lv_sidebar_address}\n			{exp:gmaps:geocoding \n				 	address="M46AX"\n					map_types="roadmap|terrain|cloudmade"\n					width="230"\n					height="300"\n					zoom="12"\n			 	 	marker="yes"\n			 	 	map_style:default:saturation="-120"\n			 	 	map_style:road.arterial:element_type="geometry"\n			 	 	map_style:road.arterial:hue="#dddddd"\n			 	 	map_style:road.arterial:saturation="50"\n			 		map_style:poi.business:visibility="off"\n			 		zoom_control="no"\n			 		pan_control="no"\n				 	map_type_control="no"\n				 	scale_control="no"\n				 	street_view_control="no"\n				 	show_traffic="no"\n				 }\n				 {lv_blockquote}\n		</div>\n	</div><!-- container -->\n	{lv_footer}\n        {lv_base_scripts}\n    </body>\n</html>', '', 1382536061, 1, 'n', 0, '', 'n', 'n', 'o', 601),
(5, 1, 1, 'discipline', 'y', 'webpage', '{lv_doc_header}\r\n	</head>\r\n	<body>\r\n	{lv_chrome_frame}\r\n	{lv_header}\r\n	<div class="container">\r\n		<div class="eleven columns alpha omega content">\r\n			<div class="twelve columns row content-inner alpha omega">\r\n				<h1 class="six columns omega brand">{lv_brand}</h1>\r\n				{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\r\n				<h1 class="four columns page-header alpha">{title}</h1>\r\n			</div>\r\n			<div class="eleven omega discipline-display">\r\n				<div class="disp-tab">\r\n					{lv_folio_nav}\r\n				</div>\r\n				<div class="disp-header">{cf_introduction_text}</div>\r\n				{/exp:channel:entries}\r\n				<div class="disp-content">\r\n					<ul>\r\n					{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" limit="6" dynamic="off"}\r\n\r\n         				{if segment_2=="motion"}\r\n         				\r\n    	   					{video_upload}\r\n			        			<li id="id{entry_id}">\r\n										<video id="my_video_1" class="video-js vjs-default-skin" controls \r\n										preload="auto" width="640" height="360" poster="{start_image}"\r\n										data-setup="{}">\r\n										<source src="{version_m4v}" type=''video/mp4''>\r\n										<source src="{version_webm}" type=''video/webm''>\r\n								  		<source src="{version_ogv}" type=''video/ogv''>\r\n									</video>\r\n	        					</li>\r\n         					{/video_upload}\r\n\r\n         				{if:else}\r\n\r\n							{cf_portfolio_images}\r\n	        				<li id="id{entry_id}">\r\n								{exp:ce_img:single src="{cf_mx_portfolio_image}" width="640" min="640"}\r\n			        			{if cf_mx_supporting_text}\r\n									<div class="img-txt">\r\n			        					{cf_mx_supporting_text}\r\n		    	    				</div>\r\n								{/if}\r\n							</li>\r\n	        				{/cf_portfolio_images}\r\n\r\n		        			{paginate}\r\n		        				<p class="paginate">	\r\n	    	    			    {if previous_page}\r\n	        				    	<a href="{auto_path}">Back</a>&nbsp;&nbsp;|&nbsp;\r\n	        				    {/if}\r\n	        				    {if next_page}\r\n	        				    	<a href="{auto_path}">More examples</a>\r\n		        			    {/if}\r\n		        			    </p>\r\n	    	    			{/paginate}\r\n			\r\n						{/if}\r\n	        				        		\r\n          			{/exp:channel:entries}\r\n          			<ul>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="five columns alpha omega side-content">\r\n			{lv_sidebar_address}\r\n			{lv_blockquote}\r\n		</div>\r\n		<div id="side-thumbs">\r\n				{embed="sites/thumb-nav"}\r\n				{!--lv_thumbs_nav--}\r\n		</div>\r\n	</div><!-- container -->\r\n	<a href="#" class="return">&uarr;</a>\r\n	{lv_footer}\r\n    {lv_base_scripts}\r\n    </body>\r\n</html>\r\n', NULL, 1382536061, 1, 'n', 0, '', 'n', 'n', 'o', 2193),
(6, 1, 1, 'food', 'y', 'webpage', 'Food\n{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		{lv_folio_nav}\n        		\n			{exp:channel:entries channel="{segment_1}" disable="{lv_disable_basic}" dynamic="off"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			<div id="side-thumbs">\n			{lv_thumbs_nav}\n			</div>      \n			{lv_blockquote}\n        	\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n', NULL, 1382536061, 1, 'n', 0, '', 'n', 'n', 'o', 1),
(7, 1, 1, 'Generic', 'n', 'webpage', 'Index\n{lv_doc_header}\n    </head>\n    <body>\n        {lv_chrome_frame}\n        \n        \n\n        <div id="wrap" class="clearfix">\n        	{lv_header}\n	       	<section id="content" class="pie">\n         	{exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n				 {lv_image_matrix}\n        		<h1>{title}</h1>\n        		<div id="intro">\n	        		{cf_introduction_text}\n        		</div>\n        	{/exp:channel:entries}\n        		\n        		<h2>View our Portfolio</h2>\n        		\n         	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" dynamic="off"}\n        		<div id="content1" class="folio">\n	        		<ul>\n	        			{cf_portfolio_images}\n	        			<li>\n	        				{exp:ce_img:single src="{cf_mx_portfolio_image}" max="640" min="640"}\n	        				<div>\n	        					{cf_mx_supporting_text}\n	        				</div>\n	        				<p class="top"><a href="#">top</a></p>\n	        			</li>\n	        			{/cf_portfolio_images}\n	        		</ul>\n       		</div>\n          	{/exp:channel:entries}\n       		<div id="content2" class="dn folio">PRODUCT</div>\n        		<div id="content3" class="dn folio">LOCATION</div>\n        		<div id="content4" class="dn folio">SETS</div>\n        	</section>\n			<section id="side" class="vcard">\n				 {lv_sidebar_address}\n			</section>\n			{lv_blockquote}\n	        <footer class="vcard">\n				 {lv_footer}\n	        </footer>\n        \n        \n        </div>\n\n        {lv_base_scripts}\n    </body>\n</html>\n', '', 1351521337, 0, 'n', 0, '', 'n', 'n', 'o', 0),
(8, 1, 1, 'thumb-nav', 'y', 'webpage', '<?php\r\n\r\n	$url = $_SERVER[''REQUEST_URI''];\r\n	$segments = explode("/",$url);\r\n	if ($segments[2] != "") {\r\n		$pagination = $segments[2];\r\n		$offset = substr($pagination, 1);\r\n	} \r\n	\r\n?>\r\n<ul id="switch" class="pie clearfix">\r\n	{if segment_1=="creative"}\r\n	{exp:channel:entries channel="packaging|branding|direct-mail|store-comms" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n	{if segment_1=="photography"}\r\n	{exp:channel:entries channel="food|product|location|personal" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n	{if segment_1=="digital"}\r\n	{exp:channel:entries channel="ecrm|websites|mobile|strategy" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\r\n	{cf_portfolio_images}\r\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="100" height="100" crop="yes"}</a></li>\r\n	{/cf_portfolio_images}\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	\r\n</ul>', '', 1382536061, 1, 'n', 0, '', 'n', 'y', 'i', 0),
(9, 1, 1, 'discipline-thums', 'y', 'webpage', '<?php\n\n	$url = $_SERVER[''REQUEST_URI''];\n	$segments = explode("/",$url);\n	if ($segments[3] != "") {\n		$pagination = $segments[3];\n		$offset = substr($pagination, 1);\n	} else {\n		$offset = 0;\n	}\n	\n?>\n\n<ul id="switch" class="pie clearfix">\n\n	{exp:channel:entries channel="{segment_2}" disable="{lv_disable_basic}" dynamic="off" limit="6" offset="<?php echo $offset ?>"}\n	\n	{if segment_2=="motion"}\n	\n	\n	\n	{video_upload}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{start_image}" width="95" height="95" crop="yes"}</a></li>\n	{/video_upload}\n			\n	{if:else}\n	\n	{cf_portfolio_images}\n	<li><a href="#id{entry_id}" class="smoothScroll">{exp:ce_img:single src="{cf_mx_portfolio_image}" width="95" height="95" crop="yes"}</a></li>\n	{/cf_portfolio_images}\n	\n	{/if}\n	{/exp:channel:entries}\n	\n</ul>', NULL, 1382536061, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(10, 1, 1, 'thinking', 'y', 'webpage', '{lv_doc_header}\n    </head>\n    <body>\n    {lv_chrome_frame}\n    {lv_header}\n    <div class="container content">\n    {exp:channel:entries disable="{lv_disable_basic}" dynamic="on"}\n        <div class="sixteen columns row content-inner alpha omega">\n            <h1 class="eight columns omega brand"><img src="resource/stat/shoot-the-moon.jpg" alt=""></h1>\n            <h1 class="four columns page-header alpha">{title}</h1>\n            <div class="four columns offset-by-four omega header-contact">\n                <p class="header-info">\n                    <span>t/ 0161 205 3311</span><br>\n                    <span>hello@shoot-the-moon.co.uk</span><br>\n                    <span>Concept House, Naval Street Manchester M4 6AX</span>\n                </p>\n            </div>\n        </div>\n        <div class="sixteen columns alpha omega ot-page">\n            <h1 class="five columns page-title">{homepage_pull_quote}</h1>\n            <div class="six columns ot-intro">\n                {cf_introduction_text}\n            </div>\n            <div class="offset-by-ten six columns alpha ot-sub">\n                {cf_rightheads_text}\n            </div>\n        </div>\n        {/exp:channel:entries}\n    </div><!-- container -->\n    {lv_footer}\n        {lv_base_scripts}\n    </body>\n</html>', '', 1382536061, 1, 'n', 0, '', 'n', 'n', 'o', 106),
(11, 1, 1, 'sitemap', 'y', 'xml', '<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n\n	{!-- Home --}\n	<url> \n		<loc>{site_url}</loc> \n		<lastmod>{exp:stats}{last_entry_date format="{DATE_W3C}"}{/exp:stats}</lastmod> \n		<changefreq>always</changefreq> \n		<priority>1.0</priority> \n	</url> \n\n\n	{!--Static Content Pages --}\n	{exp:channel:entries channel="not websites|banners|eCRM|Motion|packaging|studio|food|branding|direct-mail|testimonials|personal|store-comms|product|location" disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{if change_frequency}{change_frequency}{if:else}Daily{/if}</changefreq> \n		<priority>{if priority}{priority}{if:else}0.5{/if}</priority> \n	</url> \n	{/exp:channel:entries}\n\n	{!--Generated Content Pages --}\n	{exp:channel:entries channel="websites|banners|eCRM|Motion|packaging|studio|food|branding|direct-mail|testimonials|personal|store-comms|product|location" disable="categories|member_data|pagination" dynamic="no" status="Open"}\n	<url> \n		<loc>{site_url}site/{url_title}</loc> \n		<lastmod>{gmt_edit_date format="{DATE_W3C}"}</lastmod> \n		<changefreq>{if change_frequency}{change_frequency}{if:else}Monthly{/if}</changefreq> \n		<priority>{if priority}{priority}{if:else}0.1{/if}</priority> \n	</url> \n	{/exp:channel:entries}\n\n</urlset> ', '', 1382536061, 1, 'n', 0, '', 'n', 'n', 'o', 45);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_template_groups`
--

INSERT INTO `exp_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`) VALUES
(1, 1, 'sites', 1, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_throttle`
--

CREATE TABLE IF NOT EXISTS `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_prefs`
--

CREATE TABLE IF NOT EXISTS `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `exp_upload_prefs`
--

INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`) VALUES
(1, 1, 'Photography', '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/photography/', 'http://stm.stmpreview.co.uk//images/photography/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(2, 1, 'Creative', '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/creative/', 'http://stm.stmpreview.co.uk//images/creative/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(3, 1, 'Digital', '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/digital/', 'http://stm.stmpreview.co.uk//images/digital/', 'all', '', '', '', '', '', '', '', '', '', '', NULL),
(4, 1, 'Air Side', '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/airside/', 'http://stm.stmpreview.co.uk//images/airside/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(5, 1, 'Miscellaneous (Storage)', '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/misc/', 'http://stm.stmpreview.co.uk//images/misc/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(6, 1, 'Content', '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/content-images/', 'http://stm.stmpreview.co.uk//images/content-images/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(7, 1, 'Rotating Image Panels (all)', '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/rotating_image_panels/', 'http://stm.stmpreview.co.uk//images/rotating_image_panels/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(8, 1, 'Studio (Company)', '/var/www/vhosts/stmpreview.co.uk/subdomains/stm/images/studio/', 'http://stm.stmpreview.co.uk//images/studio/', 'img', '', '', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_wygwam_configs`
--

CREATE TABLE IF NOT EXISTS `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_wygwam_configs`
--

INSERT INTO `exp_wygwam_configs` (`config_id`, `config_name`, `settings`) VALUES
(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTE6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7aToxMDtzOjY6IlNvdXJjZSI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9'),
(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ=='),
(3, 'Intro Block', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Mzp7aTowO3M6NDoiQm9sZCI7aToxO3M6NjoiSXRhbGljIjtpOjI7czo5OiJUZXh0Q29sb3IiO31zOjY6ImhlaWdodCI7czozOiIxMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6Im4iO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ=='),
(4, 'Portfolio Supporting Text', 'YTo1OntzOjc6InRvb2xiYXIiO2E6NDp7aTowO3M6NjoiRm9ybWF0IjtpOjE7czo0OiJCb2xkIjtpOjI7czo2OiJJdGFsaWMiO2k6MztzOjk6IlRleHRDb2xvciI7fXM6NjoiaGVpZ2h0IjtzOjI6IjczIjtzOjE0OiJyZXNpemVfZW5hYmxlZCI7czoxOiJuIjtzOjExOiJjb250ZW50c0NzcyI7YTowOnt9czoxMDoidXBsb2FkX2RpciI7czowOiIiO30=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_zoo_flexible_admin_menus`
--

CREATE TABLE IF NOT EXISTS `exp_zoo_flexible_admin_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT NULL,
  `group_id` int(4) DEFAULT NULL,
  `nav` text,
  `autopopulate` tinyint(1) DEFAULT NULL,
  `hide_sidebar` tinyint(1) DEFAULT NULL,
  `startpage` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
