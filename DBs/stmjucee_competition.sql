-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2014 at 11:49 AM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stmjucee_competition`
--

-- --------------------------------------------------------

--
-- Table structure for table `gooey_form_assignment`
--

CREATE TABLE IF NOT EXISTS `gooey_form_assignment` (
  `assign_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`assign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gooey_form_data`
--

CREATE TABLE IF NOT EXISTS `gooey_form_data` (
  `form_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_submitted` varchar(255) NOT NULL,
  `form_data` longtext NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gooey_main_assignment`
--

CREATE TABLE IF NOT EXISTS `gooey_main_assignment` (
  `assign_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `draw_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  PRIMARY KEY (`assign_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `gooey_main_assignment`
--

INSERT INTO `gooey_main_assignment` (`assign_id`, `draw_id`, `question_id`) VALUES
(1, 5, 1),
(2, 3, 1),
(3, 1, 1),
(14, 10, 10),
(5, 7, 5),
(13, 9, 9),
(12, 9, 8),
(11, 8, 7),
(10, 7, 5),
(15, 11, 11),
(16, 11, 11),
(17, 12, 12),
(18, 13, 13),
(19, 14, 13),
(20, 14, 8),
(21, 15, 14),
(22, 16, 14),
(23, 16, 15),
(24, 1, 14),
(25, 16, 15),
(26, 16, 15),
(27, 16, 16),
(28, 16, 16),
(29, 17, 17),
(30, 18, 18),
(31, 19, 19),
(32, 20, 20),
(33, 21, 21),
(34, 22, 21);

-- --------------------------------------------------------

--
-- Table structure for table `gooey_main_draws`
--

CREATE TABLE IF NOT EXISTS `gooey_main_draws` (
  `draw_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `draw_headline` varchar(255) NOT NULL,
  `draw_details` text NOT NULL,
  `draw_banner` varchar(255) NOT NULL,
  `draw_month` int(11) NOT NULL,
  `draw_year` int(11) NOT NULL,
  `draw_email` int(11) NOT NULL,
  PRIMARY KEY (`draw_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `gooey_main_draws`
--

INSERT INTO `gooey_main_draws` (`draw_id`, `draw_headline`, `draw_details`, `draw_banner`, `draw_month`, `draw_year`, `draw_email`) VALUES
(1, 'WIN A FAMILY SLEDGING EXPERIENCE!', 'Winter is here but that doesn’t mean you still can’t have fun! Jucee are offering you the chance to win a fabulous sledging experience for 2 adults and 2 children aged up to 16 years.<br /><br />There are a number of locations across the UK for you to choose from and to help you get there we’re also giving the lucky winner £50 towards travel expenses.<br /><br />So to be in with a chance of winning, answer the following question:\r\n\r\n', '', 2, 2012, 0),
(7, 'Win tickets for a family of four to the movies with snacks and drinks allowance*', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\"><ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>Winners can select from a choice of cinema chains and independent cinemas with venues across the UK.</li>\r\n<li>Cinema tickets for general advertised screenings only.</li>\r\n<li>Some cinema chains will exclude central London venues including Leicester Square.</li>\r\n<li>The prize includes £20 allowance for snacks and drinks (must be used in one transaction, no change given).</li>\r\n<li>Dates are subject to availability at the venue chosen by the winner.</li> \r\n<li>Transport to and from the chosen venue will be the responsibility of the winner.</li>\r\n<li>The prize must be used within 4 months of receipt.</li>\r\n<li>Additional terms and conditions of the chosen venue will apply as well as the Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a>.</li>\r\n</ul>\r\n</div>\r\n\r\n<p>Popcorn at the ready? You and your family could be on the way to the movies with a family ticket for four, including refreshments*</p>\r\n<p>There are 2 sets of tickets to be won. To be in with a chance of winning, answer the following question:</p>\r\n\r\n', '', 3, 2012, 0),
(8, 'WIN a Merlin Annual Family Pass for four people*', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n\r\n<ul>\r\n<li>Prize is one standard Merlin Annual Family Pass, valid for one year from date of issue.</li>\r\n<li>The merlin pass is for a family of 4 consisting of either two individuals 12 years of age and over and two individuals under 12 years of age, or one individual 12 years of age and over and three individuals under 12 years of age.</li>\r\n<li>Merlin Annual Passes allow free entry to a range of attractions that are owned/operated by the Merlin Entertainments Group including  Alton Towers Theme Park, LEGOLAND® Windsor Resort, LEGOLAND Discover Centre Manchester, THORPE PARK, Chessington World of Adventures, Madame Tussauds London & Blackpool, Warwick Castle, the EDF Energy London Eye, SEA LIFE centres & Sanctuaries, the Blackpool Tower attractions including Jungle Jims, the Blackpool Circus, Blackpool Ballroom, Blackpool Tower Eye and the Dungeons.</li>\r\n<li>For the full terms and conditions for use of the Merlin Family pass (including details of restrictions of use relating to dates and times at specific venues) please visit <a href=\\"http://www.merlinannualpass.co.uk\\">www.merlinannualpass.co.uk</a> – It will be the full responsibly of the winner to have read the terms and conditions of use.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>\r\n\r\n<p>More fun then one family can handle! You could be on your way to a Merlin theme park attraction for white knuckle thrills and a host of other attractions with an annual family pass for four people*.</p>\r\n<p>To be in with a chance of winning, answer the following question:</p>\r\n', '', 4, 2012, 0),
(9, 'WIN A SWINGING FAMILY DAY OUT AT GO APE!', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>Winners can select from a choice of venues across the UK.</li>\r\n<li>The prize includes lunch for two adults and two children.</li> \r\n<li>Dates are subject to availability and must be pre-booked.</li>\r\n<li>The prize must be used within 6 months of receipt unless agreed otherwise in writing\r\n<li>For the full terms and conditions for use of the Go Ape Experience (including details of restrictions of participation please visit <a href=\\"http://www.goape.co.uk/terms\\">www.goape.co.uk/terms</a>) – It will be the full responsibly of the winner to make themselves aware of the terms before visiting their chosen venue.</li>\r\n<li>£50 towards travel expenses will be supplied to the winner.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>\r\n\r\n<p>High level thrills and fun in the treetops at one of the UK\\''s favourite adventure activity centres.</p>\r\n<p>To be in with a chance of winning, answer the following question:</p>', '', 5, 2012, 0),
(10, 'WIN A THEME PARK FAMILY DAY PASS', '<p>Choose from the UK’s leading theme park attractions and feel like a VIP with a queue jump pass*</p>\r\n<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>Full day admission to chosen park</li>\r\n<li>Winners can select from a choice of theme parks across the UK</li>\r\n<li>Queue jump pass will be included where available</li>\r\n<li>Prize must be taken by 31st July 2013</li>\r\n<li>Travel to and from the chosen location will be the responsibility of the winner</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>\r\n\r\n<p>To be in with a chance of winning, answer the following question:</p>', '', 6, 2012, 0),
(11, 'WIN A SAFARI PARK FAMILY DAY PASS', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years</li>\r\n<li>Full day admission to chosen park</li>\r\n<li>Includes lunch for two adults and two children</li>\r\n<li>Winners can select from a choice of wildlife and safari parks across the UK</li>\r\n<li>Prize must be taken by 31st July 2013</li>\r\n<li>Travel to and from the chosen location will be the responsibility of the winner</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 7, 2012, 0),
(12, 'WIN A FAMILY WEEKEND AT ALTON TOWERS', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years</li>\r\n<li>2 day admission to Alton Towers</li>\r\n<li>1 night accommodation (including breakfast) in a family room in a hotel within an approximate 15 minute drive of the park</li>\r\n<li>Car parking included, however travel is the responsibility of the winner</li>\r\n<li>Dates are subject to availability and must be pre-booked</li>\r\n<li>The prize must be taken by 31st August 2013</li>\r\n<li>Certain dates including all Bank holidays are excluded from available dates</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 8, 2012, 0),
(13, 'WIN A HALLOWEEN SPOOKY EXPERIENCE*', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>Winners will be able to select from choice of four locations/spooky experiences including Alton Towers Scarefest, Edinburgh Castle, Brighton Ghost Hunt and London Eye storytelling.</li>\r\n<li>Prize choices will have specific dates that they must be taken on. These dates are on or around Halloween.</li>\r\n<li>All choices will be subject to availability and must be pre-booked.</li>\r\n<li>Travel to and from the chosen location will be the responsibility of the winner.</li>\r\n<li>Accommodation is not included within the prize.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 9, 2012, 0),
(14, 'WIN A FAMILY PANTO EXPERIENCE FOR FOUR*', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>Winners can select from a choice of venues close to their location.</li>\r\n<li>Dates are subject to availability and must be pre-booked.</li>\r\n<li>All choices will be subject to availability and must be pre-booked.</li>\r\n<li>Prize to include pre-show meal (to a maximum value of £80.00)</li>\r\n<li>Travel to and from the chosen location will be the responsibility of the winner.</li>\r\n<li>The prize must be taken by 31st January 2013.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>\r\n\r\n\r\n\r\n\r\n \r\n\r\n\r\n', '', 10, 2012, 0),
(15, 'WIN BREAKFAST WITH SANTA AT WARWICK CASTLE* ', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>1 night accommodation in a family room the night before the winner goes to Warwick Castle for breakfast.</li>\r\n<li>Breakfast with Santa.</li>\r\n<li>Full day admission to Warwick Castle and grounds (excluding the Castle Dungeon and Merlin The Dragon Tower).</li>\r\n<li>Dates are subject to availability and must be pre-booked.</li>\r\n<li>Prize must be taken during December 2012.</li>\r\n<li>Although travel is the responsibility of the winner, £50 towards travel expenses will be supplied.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 11, 2012, 0),
(17, 'WIN A COLLECTION OF DISNEY DVDs', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize consists of 30 x Disney DVD films.</li>\r\n<li>Films will be a pre-selected range, no choice of film titles is available.</li>\r\n<li>Films provided will carry a film classification of either U or PG.</li>\r\n<li>DVDs will be sent to the winner within 28 days of the prize draw.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 12, 2012, 0),
(18, 'WIN A FAMILY TEN-PIN BOWLING TRIP*', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>Winners can select from a choice of venues across the UK.</li>\r\n<li>The prize includes 2 games of bowling.</li>\r\n<li>During the bowling, up to 3 drinks per person will be included.</li>\r\n<li>A meal for the 4 participants is included and this can be taken before or after the bowling depending on the venue and time chosen.</li>\r\n<li>Details of the meal can be supplied once a venue is chosen by the winner.</li>\r\n<li>Dates and times are subject to availability and must be pre-booked. </li>\r\n<li>The prize must be taken within 4 months of receipt.</li>\r\n<li>Travel to and from the venue will be the responsibility of the winner.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>\r\n', '', 1, 2013, 0),
(19, 'WIN A 1 DAY FAMILY ADMISSION TO LEGOLAND WINDSOR', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>1 day family admission to Legoland Windsor.</li>\r\n<li>1 night accommodation in a family room at a hotel within 8 miles of the park (choice of the night before or night of the day out). Breakfast and evening meal included. The hotel will be selected by the promoter, a choice of hotel will not be available to the winner.</li>\r\n<li>Dates are subject to availability and must be pre-booked.</li>\r\n<li>A meal for the 4 participants is included and this can be taken before or after the bowling depending on the venue and time chosen.</li>\r\n<li>Dates are subject to availability and must be pre-booked.</li>\r\n<li>Dates and times are subject to availability and must be pre-booked. </li>\r\n<li>The prize must be taken by 31st August 2013.</li>\r\n<li>Certain dates including all Bank holidays are excluded from available dates.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 2, 2013, 0),
(20, 'FAMILY ADMISSION TO THE WARNER BROS STUDIO TOUR..\\''THE MAKING OF HARRY POTTER\\''', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>Prize is for two adults and two children aged up to 16 years.</li>\r\n<li>Family admission to Warner Bros. Studio Tour, a behind-the-scenes walking tour which immerses visitors into the world of film-making, featuring authentic sets, costumes and props from the Harry Potter film series.</li>\r\n<li>The Studio Tour is located 20 miles from Central London at Warner Bros. Studios Leavesden where the Harry Potter movies were filmed.</li>\r\n<li>Lunch included.</li>\r\n<li>Dates are subject to availability and must be pre-booked.</li>\r\n<li>The prize must be taken by 31st August 2013.</li>\r\n<li>Certain dates including all Bank holidays are excluded from available dates.</li>\r\n<li>The prize must be taken by 31st August 2013.</li>\r\n<li>Closing date for entries is 31st March 2013.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 3, 2013, 0),
(21, 'WIN A 3 NIGHT FAMILY STAY AT CENTER PARCS', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>A 3 nights stay at a choice of CenterParcs locations. Self-catering Accommodation in a Woodland Lodge for two adults and two children aged up to 16 years.</li>\r\n<li>Â£100 to spend on a choice of additional activities.</li>\r\n<li>Dates are subject to availability and must be pre-booked.</li>\r\n<li>The break must be taken during 2013.</li>\r\n<li>Easter, Christmas and all Bank holidays are excluded from available dates.</li>\r\n<li>Travel to and from the chosen location will be the responsibility of the winner.</li>\r\n<li>It is the responsibility of each traveller to inform the prize provider of any wheelchair or similar access requirements when claiming the prize, adhere to any applicable health and safety guidelines, the rules and regulations of venue and those of any of any of the activity providers.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 6, 2013, 0),
(22, 'WIN A 3 NIGHT FAMILY STAY AT CENTER PARCS', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">*Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n<ul>\r\n<li>A 3 nights stay at a choice of CenterParcs locations. Self-catering Accommodation in a Woodland Lodge for two adults and two children aged up to 16 years.</li>\r\n<li>Â£100 to spend on a choice of additional activities.</li>\r\n<li>Dates are subject to availability and must be pre-booked.</li>\r\n<li>The break must be taken during 2013.</li>\r\n<li>Easter, Christmas and all Bank holidays are excluded from available dates.</li>\r\n<li>Travel to and from the chosen location will be the responsibility of the winner.</li>\r\n<li>It is the responsibility of each traveller to inform the prize provider of any wheelchair or similar access requirements when claiming the prize, adhere to any applicable health and safety guidelines, the rules and regulations of venue and those of any of any of the activity providers.</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>', '', 9, 2013, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gooey_main_options`
--

CREATE TABLE IF NOT EXISTS `gooey_main_options` (
  `option_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question_id` bigint(20) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  `option_winner` varchar(50) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `gooey_main_options`
--

INSERT INTO `gooey_main_options` (`option_id`, `question_id`, `option_name`, `option_winner`) VALUES
(13, 0, '3', 'true'),
(12, 0, '6', 'false'),
(11, 0, '4', 'false'),
(14, 5, 'Spain', 'false'),
(7, 4, 'Kris Kringle', 'true'),
(8, 4, 'Mr Red Man', 'false'),
(9, 4, 'David Cameron', 'false'),
(17, 5, 'Brazil', 'true'),
(16, 5, 'Thailand', 'false'),
(18, 6, 'An Apple', 'true'),
(19, 6, 'An Orange', 'false'),
(20, 6, 'A Pineapple', 'false'),
(21, 7, '4', 'true'),
(22, 7, '2', 'false'),
(23, 7, '1', 'false'),
(24, 8, '50 years', 'false'),
(25, 8, '150 years', 'false'),
(26, 8, '100 years', 'true'),
(27, 9, 'Paris', 'false'),
(28, 9, 'London', 'false'),
(29, 9, 'New York', 'true'),
(30, 10, '20', 'false'),
(31, 10, '30', 'false'),
(32, 10, '40', 'true'),
(33, 11, 'Get bigger', 'false'),
(34, 11, 'Get smaller', 'true'),
(35, 11, 'Stay the same size', 'false'),
(36, 12, '3', 'false'),
(37, 12, '5', 'false'),
(40, 12, '8', 'true'),
(41, 13, 'The Golden Fruit', 'false'),
(42, 13, 'The Fruit of Kings', 'true'),
(43, 13, 'The Fortune Fruit', 'false'),
(44, 0, '25% recyclable', 'false'),
(45, 0, '50% recyclable', 'false'),
(46, 0, '100% recyclable', 'true'),
(47, 0, 'Apple', 'true'),
(48, 0, 'Pineapple', 'false'),
(49, 0, 'Orange', 'false'),
(50, 0, 'Apple', 'true'),
(51, 0, 'Pineapple', 'false'),
(54, 17, 'Apple', 'true'),
(53, 0, 'Orange', 'false'),
(55, 17, 'Pineapple', 'false'),
(56, 17, 'Orange', 'false'),
(57, 18, 'Ireland', 'false'),
(58, 18, 'Thailand', 'true'),
(59, 18, 'Alaska', 'false'),
(60, 19, '7', 'false'),
(61, 19, '12', 'true'),
(62, 19, '15', 'false'),
(63, 20, '200', 'false'),
(64, 20, '400', 'false'),
(65, 20, '600', 'true'),
(66, 21, 'Ozzie', 'false'),
(67, 21, 'Archie', 'true'),
(68, 21, 'Peggy', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `gooey_main_questions`
--

CREATE TABLE IF NOT EXISTS `gooey_main_questions` (
  `question_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question_name` text NOT NULL,
  `question_type` varchar(255) NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `gooey_main_questions`
--

INSERT INTO `gooey_main_questions` (`question_id`, `question_name`, `question_type`) VALUES
(4, 'What Is Santa\\''s Real Name?', 'super'),
(6, 'Issac Newton discovered gravity with the help of:', 'super'),
(5, 'Where does Ozzie Orange come from?', 'main'),
(7, 'How many Pineapples does it take to make one carton of Jucee 100% Pure Pineapple', 'main'),
(8, 'How long can an apple tree live for?', 'main'),
(9, 'Which city is nicknamed The Big Apple?', 'main'),
(10, 'How many suggested servings are there in a 2L Jucee squash bottle?', 'main'),
(11, 'As a pineapple plant gets older, its pineapples...?', 'main'),
(12, 'How many No Added Sugar variants are available within the Jucee squash range?', 'main'),
(13, 'Pineapples used to be so rare that people called them', 'main'),
(18, 'Where does Peggy Pineapple come from?', 'main'),
(17, 'Which fruit helped Sir Isaac Newton discover gravity?', 'main'),
(19, 'How many Jucee squash flavours are there in total?', 'main'),
(20, 'How many different kinds of oranges are there?', 'main'),
(21, 'What is the name of our apple character on our Jucee soft drinks range?', 'main');

-- --------------------------------------------------------

--
-- Table structure for table `gooey_super_assignment`
--

CREATE TABLE IF NOT EXISTS `gooey_super_assignment` (
  `assign_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `draw_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  PRIMARY KEY (`assign_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `gooey_super_assignment`
--

INSERT INTO `gooey_super_assignment` (`assign_id`, `draw_id`, `question_id`) VALUES
(3, 7, 4),
(2, 1, 4),
(4, 7, 6);

-- --------------------------------------------------------

--
-- Table structure for table `gooey_super_draws`
--

CREATE TABLE IF NOT EXISTS `gooey_super_draws` (
  `draw_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `draw_headline` varchar(255) NOT NULL,
  `draw_details` text NOT NULL,
  `draw_banner` varchar(255) NOT NULL,
  `draw_month` int(11) NOT NULL,
  `draw_year` int(11) NOT NULL,
  `draw_email` int(11) NOT NULL,
  PRIMARY KEY (`draw_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `gooey_super_draws`
--

INSERT INTO `gooey_super_draws` (`draw_id`, `draw_headline`, `draw_details`, `draw_banner`, `draw_month`, `draw_year`, `draw_email`) VALUES
(7, 'WIN A UK FAMILY HOLIDAY FOR FOUR', '<p><a id=\\"prizepop\\" href=\\"#prizeterms\\" title=\\"Prize Terms & Conditions\\">Click here for Prize Terms & Conditions</a></p>\r\n<div id=\\"prizeterms\\">\r\n\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n<li>3</li>\r\n<li>4</li>\r\n<li>The Jucee Price Draw <a href=\\"http://www.jucee.co.uk/terms\\">terms and conditions</a> are also applicable.</li>\r\n</ul>\r\n</div>\r\n\r\n<p>Enjoy a ‘staycation’ with Jucee in some\r\nof the UKs’ most beautiful holiday spots.</p>\r\n<p>To be in with a chance of winning, answer the following question:</p>', '', 4, 2012, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gooey_super_options`
--

CREATE TABLE IF NOT EXISTS `gooey_super_options` (
  `option_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question_id` bigint(20) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  `option_winner` varchar(50) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gooey_super_questions`
--

CREATE TABLE IF NOT EXISTS `gooey_super_questions` (
  `question_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question_name` text NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
