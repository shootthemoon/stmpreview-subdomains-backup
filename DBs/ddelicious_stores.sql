-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2014 at 11:41 AM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ddelicious_stores`
--

-- --------------------------------------------------------

--
-- Table structure for table `store_locator`
--

CREATE TABLE IF NOT EXISTS `store_locator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(160) NOT NULL,
  `logo` varchar(160) NOT NULL,
  `address` varchar(160) NOT NULL,
  `lat` varchar(20) NOT NULL,
  `lng` varchar(20) NOT NULL,
  `url` varchar(160) NOT NULL,
  `description` text NOT NULL,
  `tel` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `country` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=196 ;

--
-- Dumping data for table `store_locator`
--

INSERT INTO `store_locator` (`id`, `user_id`, `post_id`, `category_id`, `name`, `logo`, `address`, `lat`, `lng`, `url`, `description`, `tel`, `email`, `city`, `country`, `created`) VALUES
(1, 0, 0, 0, 'Allanhill Farm Shop', '', 'Allanhill Farm, St Andrews, Fife, KY16 8LJ', '56.3230433', '-2.784665000000018', 'http://www.allanhill.co.uk/shop.htm', '', '', '', '', '', '2012-08-02 16:14:19'),
(2, 0, 0, 0, 'Ardross Farm Shop', '', 'Ardross Farm, Elie, Fife, KY9 1EU', '56.19548899999999', '-2.7977694999999585', 'http://www.ardrossfarm.co.uk/index.php', '', '01333 331400', '', '', '', '2012-08-02 16:16:28'),
(3, 0, 0, 2, 'Booths Hala Road', '', 'Hala Road	Scotforth,	Lancaster,	Lancashire, LA1 4SG', '54.0275384', '-2.793349600000056', 'http://www.booths.co.uk/store/lancaster/', '', '01524 67415', '', '', '', '2012-08-02 16:20:23'),
(4, 0, 0, 0, 'Alloway Post Office & Village Store', '', '29 Alloway, Alloway, Ayrshire, KA7 4PY', '55.432799', '-4.633251900000005', '', '', '', '', '', '', '2012-08-03 10:22:57'),
(5, 0, 0, 0, 'Andrew Gordon Butchery & Fine Foods', '', '35-37 Chattan Place	, Aberdeen, AB10 6RB', '57.13819299999999', '-2.1211289000000306', 'http://www.andrewgordonbutchery.com/', '', '01224 587553', '', '', '', '2012-08-03 10:25:20'),
(6, 0, 0, 0, 'Andrew Gordon Butchery & Fine Foods', '', '32 Evan Street, Stonehaven, Kincardineshire, AB39 2ET', '56.963628', '-2.2118123000000196', 'http://www.andrewgordonbutchery.com/', '', '01569 767855', '', '', '', '2012-08-03 10:27:12'),
(7, 0, 0, 0, 'Ardhasaig Filling Station', '', 'Ardhasaig, Isle of Harris, HS3 3AJ', '57.92340589999999', '-6.850855499999966', '', '', '01859 502066', '', '', '', '2012-08-03 10:28:34'),
(8, 0, 0, 0, 'Auchengree Farm Shop', '', 'Auchengree, Beith, Ayrshire, KA14 3BU', '55.73704919999999', '-4.665479000000005', 'http://auchengree.bpweb.net/', '', '01294 834 625', '', '', '', '2012-08-03 10:30:10'),
(9, 0, 0, 0, 'Auchengree Farm Shop', '', 'Auchengree, Beith, Ayrshire, KA14 3BU', '55.73704919999999', '-4.665479000000005', 'http://auchengree.bpweb.net/', '', '01294 834 625', '', '', '', '2012-08-03 10:30:10'),
(10, 0, 0, 0, 'Berits and Brown', '', 'Main Street, Kippen, FK8 3DN', '56.1264134', '-4.171656299999995', 'http://www.beritsandbrown.com/', '', '01786 870077', '', '', '', '2012-08-03 10:32:10'),
(11, 0, 0, 0, 'Berryfields Auchtydonald Ltd', '', 'Auchtydonald, Peterhead, Aberdeenshire, AB42 4XD', '57.52207199999999', '-1.9649581999999555', '', '', '01771 623707', '', '', '', '2012-08-03 10:33:31'),
(12, 0, 0, 2, 'Booths Ball Street', '', '11 Ball Street, Poulton, Blackpool, Lancashire, FY6 7BA', '53.8475717', '-2.991915699999936', 'http://www.booths.co.uk/store/blackpool-poulton/', '', '01253 891163', '', '', '', '2012-08-03 10:35:12'),
(13, 0, 0, 2, 'Booths Scotland Road', '', 'Scotland Road, Carnforth, Lancashire, LA5 9JZ', '54.13256939999999', '-2.7630123999999796', 'http://www.booths.co.uk/store/carnforth/', '', '01524 736680', '', '', '', '2012-08-03 10:36:23'),
(14, 0, 0, 2, 'Booths New Market Street', '', 'New Market Street, Chorley, Lancashire, PR7 1DB', '53.65380469999999', '-2.6304519000000255', 'http://www.booths.co.uk/store/chorley/', '', '01257 265737', '', '', '', '2012-08-03 10:37:29'),
(15, 0, 0, 2, 'Booths Station Road', '', 'Station Road, Clitheroe, Lancashire, BB7 2JT', '53.7257845', '-1.7100047000000131', 'http://www.booths.co.uk/store/clitheroe/', '', '01200 427325', '', '', '', '2012-08-03 10:38:29'),
(16, 0, 0, 2, 'Booths Cherestanc Square', '', 'Cherestanc Square, Park Hill Road, Garstang, Lancashire, PR3 1EF', '53.9005848', '-2.775567599999931', 'http://www.booths.co.uk/store/garstang/', '', '01995 603236', '', '', '', '2012-08-03 10:39:54'),
(17, 0, 0, 2, 'Booths Station Road', '', '20 Station Road, Hesketh Bank, Lancashire, PR4 6SN', '53.6980131', '-2.8387142999999924', 'http://www.booths.co.uk/store/hesketh-bank/', '', '01772 811493', '', '', '', '2012-08-03 10:41:10'),
(18, 0, 0, 2, 'Booths Wainwrights Yard', '', 'Wainwrights Yard, Kendal, Cumbria, LA9 4DP', '54.3281869', '-2.7480643999999756', 'http://www.booths.co.uk/store/kendal/', '', '01539 742370', '', '', '', '2012-08-03 10:43:24'),
(19, 0, 0, 2, 'Booths Tithebarn Street', '', 'Tithebarn Street, Keswick, Cumbria, CA12 5EA', '54.6017783', '-3.140460100000041', 'http://www.booths.co.uk/store/keswick/', '', '01768 773518', '', '', '', '2012-08-03 10:46:21'),
(20, 0, 0, 2, 'Booths Dodgeson Croft Road', '', 'Dodgeson Croft Road, Kirkby Lonsdale, Cumbria, LA6 2HG', '54.2011811', '-2.599908199999959', 'http://www.booths.co.uk/store/kirkby-lonsdale/', '', '01524 273443', '', '', '', '2012-08-03 10:47:26'),
(21, 0, 0, 2, 'Booths Stanley Road', '', 'Stanley Road, Knutsford, Cheshire, WA16 0BS', '53.3022773', '-2.377294500000062', 'http://www.booths.co.uk/store/knutsford/', '', '01565 652522', '', '', '', '2012-08-03 10:48:36'),
(22, 0, 0, 2, 'Booths Liverpool Road', '', 'Liverpool Road, Longton, Lancashire	, PR4 5NB', '53.7229979', '-2.7906722000000173', 'http://www.booths.co.uk/store/preston-longton/', '', '01772 613192', '', '', '', '2012-08-03 10:49:33'),
(23, 0, 0, 2, 'Booths Haven Road', '', 'Haven Road, Lytham	, Lancashire, FY8 5EG', '53.7385702', '-2.9563157999999703', 'http://www.booths.co.uk/store/lytham-haven-road/', '', '01253 795561', '', '', '', '2012-08-03 10:50:22'),
(24, 0, 0, 2, 'Booths Highfield Road', '', 'Highfield Road, Marton, Lancashire, FY4 5AP', '53.7888384', '-3.027773400000001', 'http://www.booths.co.uk/store/blackpool-marton/', '', '01253 767852', '', '', '', '2012-08-03 10:51:27'),
(25, 0, 0, 2, 'Booths Berry Lane', '', 'Berry Lane, Longridge, Preston, Lancashire, PR3 3NH', '53.8313852', '-2.602831100000003', 'http://www.booths.co.uk/store/preston-longridge/', '', '01772 780737', '', '', '', '2012-08-03 10:52:59'),
(26, 0, 0, 2, 'Booths Millbrook Way', '', 'Millbrook Way, Penwortham, Preston, Lancashire, PR1 0XW', '53.7375705', '-2.7284956999999395', 'http://www.booths.co.uk/store/preston-penwortham/', '', '01772 752771', '', '', '', '2012-08-03 10:54:22'),
(27, 0, 0, 2, 'Booths Sharoe Green Lane', '', '256/270 Sharoe Green Lane, Fulwood, Preston, Lancashire, PR2 9HD', '53.7923494', '-2.705553000000009', 'http://www.booths.co.uk/store/preston-fulwood/', '', '01772 716027', '', '', '', '2012-08-03 10:55:39'),
(28, 0, 0, 2, 'Booths Oubas Hill', '', 'Oubas Hill	, Canal Street, Canal Street, Cumbria	, LA12 7LY', '54.19843840000001', '-3.084179899999981', 'http://www.booths.co.uk/store/ulverston/', '', '01229 588003', '', '', '', '2012-08-03 14:25:14'),
(29, 0, 0, 2, 'Booths The Old Station', '', 'The Old Station, Victoria Street, Windermere, Cumbria, LA23 1QA', '54.3797547', '-2.905514700000026', 'http://www.booths.co.uk/store/windermere/', '', '01539 446114', '', '', '', '2012-08-03 14:26:19'),
(30, 0, 0, 2, 'Booths Leeds Road', '', 'Leeds Road, Ilkley, West Yorkshire, LS29 8EE', '53.92878', '-1.8078918999999587', 'http://www.booths.co.uk/store/ilkley/', '', '01943 605000', '', '', '', '2012-08-03 14:27:03'),
(31, 0, 0, 2, 'Booths Marshall Way', '', 'Marshall Way, Ripon, North Yorkshire, HG4 2BT', '54.137619', '-1.5256729999999834', 'http://www.booths.co.uk/store/ripon/', '', '01765 698540', '', '', '', '2012-08-03 14:29:21'),
(32, 0, 0, 2, 'Booths Kirkgate', '', 'Kirkgate, Settle, North Yorkshire, BD24 9BP', '54.0706155', '-2.281889500000034', 'http://www.booths.co.uk/store/settle/', '', '01729 825190', '', '', '', '2012-08-03 14:30:41'),
(33, 0, 0, 2, 'Booths Woodlands Road', '', '38/40 Woodlands Road, Andsell, Lancashire, FY8 4ER', '53.7433089', '-2.9913335999999617', 'http://www.booths.co.uk/store/lytham-ansdell/', '', '01253 736160', '', '', '', '2012-08-03 14:31:58'),
(34, 0, 0, 0, 'Castleton Farm Shop', '', 'Laurencekirk, Aberdeenshire, AB30 1JX', '56.89915079999999', '-2.395146699999941', 'http://www.castletonfarmshop.co.uk/', '', '01561 320409', '', '', '', '2012-08-03 14:33:05'),
(35, 0, 0, 0, 'Caulders Braidbar', '', 'Mearnskirk, Newton Mearns, G77 6RS', '55.7630209', '-4.331387800000016', 'http://www.caulders.co.uk/', '', '0141 639 0777', '', '', '', '2012-08-03 14:34:03'),
(36, 0, 0, 0, 'Costcutter Turriff', '', '35 High St, Turriff, Aberdeenshire, AB53 4EJ', '57.5375324', '-2.462992500000041', 'http://www.costcutter.com/', '', '018885 63229', '', '', '', '2012-08-03 14:35:04'),
(37, 0, 0, 0, 'Courtyard Shop, Courtyard Deli & Gifts', '', 'Nr Aberfeldy, Kenmore, Perthshire, PH15 2HN', '56.58782619999999', '-4.002331099999992', 'http://www.taymouthcourtyard.co.uk/', '', '01887 830 756', '', '', '', '2012-08-03 14:36:28'),
(38, 0, 0, 0, 'Craigies Farm Deli & CafÃ©', '', 'West Craigie Farm, South Queensferry, Edinburgh, EH30 9TR', '55.9729458', '-3.3160056000000395', 'http://www.craigies.co.uk/', '', '0131 319 1048', '', '', '', '2012-08-03 14:37:45'),
(39, 0, 0, 0, 'Dean''s', '', 'Dean''s, Huntly, Aberdeenshire, AB54 8JX', '57.4451705', '-2.7957810000000336', 'http://www.deans.co.uk/', '', '01466 792086', '', '', '', '2012-08-03 14:40:49'),
(40, 0, 0, 3, 'Dobbies Garden Centre Blackpool Road', '', 'Blackpool Road, Clifton, Near Preston, PR4 0XL', '53.7681642', '-2.7566240999999536', 'http://www.dobbies.com/storelocator/search-Clifton%2c-Lancashire-PR4-20-10/store-S026', '', '01772 683844', '', '', '', '2012-08-03 14:42:36'),
(41, 0, 0, 3, 'Dobbies Garden Centre Bentham''s Way', '', 'Bentham''s Way, Southport	, Lancashire, PR8 4HX', '53.6268936', '-2.9976556999999957', 'http://www.dobbies.com/storelocator/search-southport-20-10/store-S036', '', '01704 552920', '', '', '', '2012-08-03 14:45:33'),
(42, 0, 0, 3, 'Dobbies Garden Centre Mendip Avenue', '', 'Mendip Avenue, Fosseway Industrial Estate, Shepton Mallet, Somerset, BA4 4PE', '51.1834536', '-2.5296779000000242', 'http://www.dobbies.com/storelocator/search-shepton-mallet-20-10/store-S039', '', '01749 333490', '', '', '', '2012-08-03 14:51:34'),
(43, 0, 0, 3, 'Dobbies Garden Centre Strawberry Hill', '', 'Strawberry Hill, Saintfield Road, Lisburn, BT27 5PG', '54.4889433', '-5.989108499999929', 'http://www.dobbies.com/storelocator/store-S035', '', '02892 626960', '', '', '', '2012-08-03 14:52:56'),
(44, 0, 0, 3, 'Dobbies Garden Centre New Park Farm', '', 'New Park Farm, Whitemyres, Lang Stracht, Aberdeen, AB15 6XH', '57.1512427', '-2.1626380000000154', 'http://www.dobbies.com/storelocator/search-aberdeen-20-10/store-S024', '', '01224 686 000', '', '', '', '2012-08-03 14:54:14'),
(45, 0, 0, 3, 'Dobbies Garden Centre Old Toll', '', 'Old Toll, Holmston, Ayr, KA6 5JJ', '55.45503850000001', '-4.588387399999988', 'http://www.dobbies.com/storelocator/search-ayr-20-10/store-S028', '', '01292 294750', '', '', '', '2012-08-03 14:55:14'),
(46, 0, 0, 3, 'Dobbies Garden Centre Ethiebeaton Park', '', 'Ethiebeaton Park, Monifeith, Dundee, DD5 4HB', '56.49027049999999', '-2.8338903000000073', 'http://www.dobbies.com/storelocator/search-dundee-20-10/store-S012', '', '01382 530333', '', '', '', '2012-08-03 14:56:38'),
(47, 0, 0, 3, 'Dobbies Garden Centre Melville Nursery', '', 'Melville Nursery, Lasswade, Edinburgh, Midlothian, EH18 1AZ', '55.89653389999999', '-3.0996512000000394', 'http://www.dobbies.com/storelocator/search-lasswade-20-10/store-S011', '', '0131 663 1941', '', '', '', '2012-08-03 14:57:37'),
(48, 0, 0, 3, 'Dobbies Garden Centre King''s Inch Drive', '', '75 King''s Inch Drive, Renfrew, Glasgow, G51 4FB', '55.86906', '-4.356033700000012', 'http://www.dobbies.com/storelocator/search-renfrew-20-10/store-S041', '', '0141 886 8660', '', '', '', '2012-08-03 14:58:44'),
(49, 0, 0, 3, 'Dobbies Garden Centre Boclair Road', '', 'Boclair Road, Milngavie, Glasgow, G62 6EP', '55.9262506', '-4.290766899999994', 'http://www.dobbies.com/storelocator/search-renfrew-20-10/store-S022', '', '01360 620721', '', '', '', '2012-08-03 14:59:34'),
(50, 0, 0, 3, 'Dobbies Garden Centre Beughburn', '', 'Beughburn, Houston Mains, Livingston, Midlothian, EH52 6PA', '55.9216139', '-3.517998000000034', 'http://www.dobbies.com/storelocator/search-livingston-20-10/store-S042', '', '', '', '', '', '2012-08-03 15:00:49'),
(51, 0, 0, 3, 'Dobbies Garden Centre Drip Road', '', 'Drip Road, Craigforth, Stirling, FK9 4UF', '56.13243319999999', '-3.9619324000000233', 'http://www.dobbies.com/storelocator/search-Stirling-20-10/store-S029', '', '01506 812000', '', '', '', '2012-08-03 15:01:48'),
(52, 0, 0, 3, 'Dobbies Garden Centre Orton Grange', '', 'Orton Grange, Carlisle, Cumbria, CA5 6LB', '54.8582659', '-3.0123591999999917', 'http://www.dobbies.com/storelocator/search-carlisle-20-10/store-S043', '', '01228 713520', '', '', '', '2012-08-03 15:02:43'),
(53, 0, 0, 3, 'Dobbies Garden Centre Street House Farm', '', 'Street House Farm, Ponteland, Newcastle Upon Tyne, NE20 9BT', '55.0686707', '-1.7327806000000692', 'http://www.dobbies.com/storelocator/search-ponteland-20-10/store-S014', '', '01661 820202', '', '', '', '2012-08-03 15:03:49'),
(54, 0, 0, 3, 'Dobbies Garden Centre Belvedere Lane', '', 'Belvedere Lane, Watling Street, Fenny Stratford	, Milton Keynes, MK17 9JH', '51.9971715', '-0.7093651999999793', 'http://www.dobbies.com/storelocator/search-milton-keynes-20-10/store-S032', '', '01908 364890', '', '', '', '2012-08-03 15:05:09'),
(55, 0, 0, 3, 'Dobbies Garden Centre Cross Lane Farm', '', 'Cross Lane Farm, Nuneaton Road, Mancetter near Atherstone, Warwickshire, CV9 1RF', '52.5621857', '-1.5229031000000077', 'http://www.dobbies.com/storelocator/search-atherstone-20-10/store-S027', '', '01827 715511', '', '', '', '2012-08-03 15:06:43'),
(56, 0, 0, 3, 'Dobbies Garden Centre Siddington', '', 'Siddington, Circenster, Gloucestershire, GL7 6EU', '51.6960539', '-1.9531079999999292', 'http://www.dobbies.com/storelocator/search-Ciren-20-10/store-S030', '', '01285 884 300', '', '', '', '2012-08-03 15:07:46'),
(57, 0, 0, 3, 'Dobbies Garden Centre Rutherford Road', '', 'Rutherford Road, Eureka Leisure Park, Ashford, Kent, TN25 6PA', '51.12148759999999', '0.930155099999979', 'http://www.dobbies.com/storelocator/search-kent-20-10/store-S044', '', '01233 619360', '', '', '', '2012-08-03 15:08:40'),
(58, 0, 0, 3, 'Dobbies Garden Centre Hyde End Road', '', '166 Hyde End Road, Shinfield, Reading, RG2 9ER', '51.4004005', '-0.9494661999999607', 'http://www.dobbies.com/storelocator/search-reading-20-10/store-S033', '', '01189 884822', '', '', '', '2012-08-03 15:09:58'),
(59, 0, 0, 0, 'Dowhill Farm', '', 'Turnberry, Girvan, Ayrshire, KA26 9JP', '55.311327', '-4.835017999999991', '', '', '01655 331 517', '', '', '', '2012-08-03 15:11:58'),
(60, 0, 0, 0, 'Elie Deli', '', '55 High Street, Leven, Fife, KY9 1BZ', '56.1904798', '-2.8197208000000273', 'http://www.eliedeli.co.uk/', '', '01333 330 323', '', '', '', '2012-08-03 15:12:48'),
(61, 0, 0, 0, 'Fields of Perthshire', '', '22 Dunkeld Street, Aberfeldy, Perthshire, PH15 2AA', '56.6200078', '-3.8636814999999842', 'http://www.highlandperthshire.com/shops/aberfeldy-area-shops/fields-of-perthshire.aspx', '', '', '', '', '', '2012-08-03 15:22:08'),
(62, 0, 0, 0, 'George Strachan Ltd of Deeside', '', 'Main Street, Aboyne, Aberdeenshire, AB34 5HT', '57.0761811', '-2.779301300000043', 'http://georgestrachanltd.co.uk/', '', '01339 886121', '', '', '', '2012-08-03 15:25:00'),
(63, 0, 0, 0, 'Giuliani''s Deli', '', '28 Henderson Street, Bridge of Allan, FK9 4HR', '56.1551374', '-3.949002999999948', '', '', '01786 833906', '', '', '', '2012-08-03 15:28:19'),
(64, 0, 0, 0, 'Glenbarr Nursery', '', 'Glenbarr, Tarbert, Argyll, PA29 6UT', '55.565541', '-5.697598999999968', 'http://www.glenbarr-nursery.co.uk/Home.html', '', '01583 421200', '', '', '', '2012-08-03 15:29:14'),
(65, 0, 0, 0, 'Gloagburn Farm Shop', '', 'Gloagburn Farm, Tibbermore, Perth, PH1 1QL', '56.39532320000001', '-3.547762299999931', 'http://www.gloagburnfarmshop.co.uk/index.htm', '', '01738 840864', '', '', '', '2012-08-03 15:30:07'),
(66, 0, 0, 0, 'Gordon & MacPhail', '', '58-60 South Street, Elgin, Moray, IV30 1JY', '57.64769209999999', '-3.315092800000002', 'http://www.gordonandmacphail.com/', '', '01343 545110', '', '', '', '2012-08-03 15:30:59'),
(67, 0, 0, 0, 'Hammerton Store', '', '336 Great Western Road, Aberdeen, AB10 6LX', '57.1350448', '-2.1267735999999786', 'http://www.hammertonstore.co.uk/', '', '01224 324449', '', '', '', '2012-08-03 15:32:01'),
(68, 0, 0, 0, 'Happy Plant Garden Centre', '', 'North Street, Mintlaw, Peterhead, AB42 5HH', '57.52934370000001', '-2.003304999999955', 'http://www.happyplant.co.uk/', '', '01771 623344', '', '', '', '2012-08-03 15:35:03'),
(69, 0, 0, 0, 'Househill Farm Shop', '', 'Househill Mains, Nairn, IV12 5RY', '57.5732222', '-3.8620237999999745', '', '', '01667 452548', '', '', '', '2012-08-03 15:37:18'),
(70, 0, 0, 0, 'J.L. Gill', '', '26 West High Street, Crieff, PH7 4DL', '56.373238', '-3.8424198999999817', '', '', '01764 652396', '', '', '', '2012-08-03 15:38:26'),
(71, 0, 0, 0, 'Kellockbank Country Emporium', '', 'St. Sairs, Colpy, Insch, Aberdeenshire, AB52 6TG', '57.38162980000001', '-2.5967143000000306', 'http://www.kellockbank.co.uk/', '', '01464 851114', '', '', '', '2012-08-03 15:41:40'),
(72, 0, 0, 0, 'Kimbles Chocolate Ltd', '', 'Upper Mall, St Enoch Centre, Glasgow, G1 4BW', '55.8570696', '-4.252892400000064', 'http://www.ilovekimbles.co.uk/', '', '0141 249 9955', '', '', '', '2012-08-03 15:43:16'),
(73, 0, 0, 0, 'Kirktown Garden Centre', '', 'Fetteresso	, Stonehaven, Aberdeenshire, AB39 3UP', '56.960709', '-2.2528750000000173', 'http://www.kirktowngardencentre.co.uk/', '', '01569 766887', '', '', '', '2012-08-03 15:45:24'),
(74, 0, 0, 0, 'Knowes Farm Shop', '', 'by East Linton, East Lothian, EH42 1XJ', '55.99106620000001', '-2.6247551999999814', 'http://www.knowesfarmshop.co.uk/contact/', '', '01620 860 010', '', '', '', '2012-08-03 15:50:42'),
(75, 0, 0, 5, 'Lakeland Balmoral Plaza', '', 'Balmoral Plaza, Boucher Road, Belfast, BT12 6HR', '54.5797131', '-5.9619158000000425', 'http://www.lakeland.co.uk/StoreLocator.action;jsessionid=F4B1B58395F9BE234EB49F51C32088B0.app2', '', '028 9068 3229', '', '', '', '2012-08-03 15:52:22'),
(76, 0, 0, 5, 'Lakeland Union Street', '', '154 Union Street, Aberdeen, AB10 1QT', '57.1453155', '-2.103562000000011', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01224 620683', '', '', '', '2012-08-03 15:53:36'),
(77, 0, 0, 5, 'Lakeland Hanover Street', '', '55 Hanover Street, Edinburgh, EH2 2PJ', '55.9533488', '-3.1969923000000335', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0131 220 3947', '', '', '', '2012-08-03 15:54:36'),
(78, 0, 0, 5, 'Lakeland Buchanan Galleries', '', '18a Buchanan Galleries, Buchanan Street, Glasgow, G1 2FF', '55.8629772', '-4.252460400000018', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0141 331 1112', '', '', '', '2012-08-03 15:57:08'),
(79, 0, 0, 5, 'Lakeland St John Street', '', '50 St John Street, Perth, PH1 5SP', '56.3962081', '-3.427248400000053', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01738 621124', '', '', '', '2012-08-03 15:58:45'),
(80, 0, 0, 5, 'Lakeland Eldon Gardens', '', 'Eldon Gardens, Newcastle Upon Tyne, NE1 7RA', '54.9756598', '-1.6168514999999388', 'http://www.lakeland.co.uk/StoreLocator.action', '', '0191 261 2776', '', '', '', '2012-08-03 16:00:11'),
(81, 0, 0, 1, 'Waitrose Sovereign Way', '', 'Sovereign Way,	Tonbridge	, TN9 1RG', '51.1925233', '0.27847189999999955', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/667.html', '', '01732 358964', '', '', '', '2012-08-03 16:00:20'),
(82, 0, 0, 5, 'Lakeland Arnison Retail Park', '', 'Arnison Retail Park, Pity Me, Durham, DH1 5GB', '54.8067438', '-1.5817652000000635', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0191 386 4666', '', '', '', '2012-08-03 16:01:06'),
(83, 0, 0, 1, 'Waitrose 140 Northolt Road ', '', '140 Northolt Road, South Harrow, HA2 0EG', '51.5669481', '-0.3500947999999653', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/219.html', '', '020 8423 1955', '', '', '', '2012-08-03 16:01:28'),
(84, 0, 0, 1, 'Waitrose 311-313 Ashley Road', '', '311-313 Ashley Road, Poole, BH14 0AP', '50.7287668', '-1.9394128999999793', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/766.false.html', '', '01202 718051', '', '', '', '2012-08-03 16:02:15'),
(85, 0, 0, 1, 'Waitrose The Lairage', '', 'The Lairage, Bedford Road, Hitchin, SG5 2UG', '51.95626110000001', '-0.2856366999999409', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/685.html', '', '01462 423399', '', '', '', '2012-08-03 16:03:08'),
(86, 0, 0, 5, 'Lakeland Eastgate Row North', '', '5 Eastgate Row North, Chester, CH1 1LQ', '53.19058', '-2.8906037000000424', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01244 313758', '', '', '', '2012-08-03 16:04:38'),
(87, 0, 0, 1, 'Waitrose Honeybourne Way', '', 'Honeybourne Way, Cheltenham, GL50 3QW', '51.90325300000001', '-2.0870872000000418', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/663.html', '', '01242 241425', '', '', '', '2012-08-03 16:04:54'),
(88, 0, 0, 5, 'Lakeland Wilmslow Road,', '', '227 Wilmslow Road, Handforth	, SK9 3JZ', '53.3579227', '-2.2193045999999867', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0161 437 5511', '', '', '', '2012-08-03 16:05:50'),
(89, 0, 0, 1, 'Waitrose St Johns Well Lane', '', 'St Johns Well Lane, Berkhamsted, HP4 1HS', '51.76264270000001', '-0.5698712999999316', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/223.html', '', '01442 877505', '', '', '', '2012-08-03 16:05:58'),
(90, 0, 0, 5, 'Lakeland South John Street', '', '27 South John Street	, Liverpool, L1 8BU', '53.4037762', '-2.987286700000027', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0151 707 6047', '', '', '', '2012-08-03 16:06:44'),
(91, 0, 0, 1, 'Waitrose Fossetts Way', '', 'Fossetts Way, Eastern Avenue, Southend-on-Sea, SS2 4DQ', '51.5562567', '0.7257353999999623', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/213.html', '', '01702 603403', '', '', '', '2012-08-03 16:07:07'),
(92, 0, 0, 1, 'Waitrose 1-3 Ecclesall Road', '', '1-3 Ecclesall Road, Sheffield, S11 8HY', '53.3731222', '-1.481136900000024', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/695.html', '', '0114 272 2027', '', '', '', '2012-08-03 16:08:41'),
(93, 0, 0, 1, 'Waitrose Stranford Road', '', 'Stranford Road, Lymington, SO41 9GF', '50.7572248', '-1.5496055999999498', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/798.html', '', '01590 671949', '', '', '', '2012-08-03 16:09:28'),
(94, 0, 0, 1, 'Waitrose Sheep Street', '', 'Sheep Street, Cirenchester, GL7 1SZ', '51.71318249999999', '-1.9703342000000248', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/220.html', '', '01285 643733', '', '', '', '2012-08-03 16:10:07'),
(95, 0, 0, 1, 'Waitrose Station Road', '', 'Station Road, Cheadle Hulme, SK8 7AE', '53.3748275', '-2.1871787999999697', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/710.html', '', '0161 486 1702', '', '', '', '2012-08-03 16:10:44'),
(96, 0, 0, 5, 'Lakeland Alexandra Buildings', '', 'Alexandra Buildings, Windermere, Cumbria, LA23 1BQ', '54.3791642', '-2.9017652', 'http://www.lakeland.co.uk/StoreLocator.action', '', '015394 88100', '', '', '', '2012-08-03 16:11:05'),
(97, 0, 0, 1, 'Waitrose 1 South Road', '', '1 South Road, Beckenham, BR3 1SD', '51.4122085', '-0.0240364999999656', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/212.html', '', '020 8663 4898', '', '', '', '2012-08-03 16:11:38'),
(98, 0, 0, 1, 'Waitrose 92-96 High Road', '', '92-96 High Road, South Woodford, London, E18 2NA', '51.5939267', '0.013566299999979492', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/231.html', '', '020 8989 0022', '', '', '', '2012-08-03 16:12:16'),
(99, 0, 0, 5, 'Lakeland Butcher Row', '', '24 Butcher Row	, Beverley, HU17 0AB', '53.8414629', '-0.4277677999999696', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01482 888789', '', '', '', '2012-08-03 16:12:22'),
(100, 0, 0, 1, 'Waitrose The Furlong', '', 'The Furlong, Ringwood, BH24 1AT', '50.8476277', '-1.7927039999999579', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/203.html', '', '01425 477729', '', '', '', '2012-08-03 16:12:59'),
(101, 0, 0, 5, 'Lakeland James Street', '', '48 James Street	, Harrogate, HG1 1RF', '53.99247339999999', '-1.5418164999999817', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01423 526811', '', '', '', '2012-08-03 16:13:14'),
(102, 0, 0, 5, 'Lakeland High Street	', '', '130/132 High Street	, Northallerton, DL7 8PQ', '54.3377273', '-1.4334837999999763', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01609 777440', '', '', '', '2012-08-03 16:14:58'),
(103, 0, 0, 5, 'Lakeland The Arcade', '', '28-29 The Arcade, Meadowhall, Sheffield, S9 1EH', '53.4148625', '-1.4101190000000088', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0114 256 9091', '', '', '', '2012-08-03 16:16:04'),
(104, 0, 0, 5, 'Lakeland High Ousegate', '', '9 High Ousegate, York, YO1 8RZ', '53.9580846', '-1.081521500000008', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01904 627737', '', '', '', '2012-08-03 16:17:00'),
(105, 0, 0, 5, 'Lakeland Bicester Avenue Retail Park', '', 'Unit 4 Bicester Avenue Retail Park, Oxford Road, Bicester, OX25 2NY', '51.869744', '-1.1943578000000343', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01869 246596', '', '', '', '2012-08-03 16:18:03'),
(106, 0, 0, 5, 'Lakeland Station Street', '', '4 Station Street, St. Marks Retail Park, Lincoln, LN5 7EY', '53.2247549', '-0.5435771999999588', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01522 546980', '', '', '', '2012-08-03 16:19:35'),
(107, 0, 0, 5, 'Lakeland Wheeler Gate', '', '2 - 4 Wheeler Gate, Nottingham, NG1 2NB', '52.9527172', '-1.1506827000000612', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0115 9508124', '', '', '', '2012-08-03 16:20:35'),
(108, 0, 0, 5, 'Lakeland Church Street', '', '7 - 8 Church Street, Peterborough, PE1 1XB', '52.5722779', '-0.2438644999999724', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01733 561154', '', '', '', '2012-08-03 16:21:30'),
(109, 0, 0, 5, 'Lakeland The Promenade', '', '13-17 The Promenade, Cheltenham, GL50 1LN', '51.90082109999999', '-2.07557369999995', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01242 227855', '', '', '', '2012-08-03 16:22:35'),
(110, 0, 0, 5, 'Lakeland Mardol', '', '6/7 Mardol, Shrewsbury, SY1 1PY', '52.7084225', '-2.755531000000019', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01743 272539', '', '', '', '2012-08-03 16:23:47'),
(111, 0, 0, 5, 'Lakeland Mill Lane Arcade', '', '15 Mill Lane Arcade, Touchwood, Solihull, B91 3GS', '52.4124038', '-1.7793878999999606', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0121 711 2601', '', '', '', '2012-08-03 16:27:42'),
(112, 0, 0, 5, 'Lakeland Henley Street', '', '4/5 Henley Street, Stratford-Upon-Avon, CV37 6PT', '52.1932272', '-1.7074588000000404', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01789 262100', '', '', '', '2012-08-03 16:29:00'),
(113, 0, 0, 5, 'Lakeland Crown Walk', '', '190b Crown Walk, Westfield, Derby, DE1 2NP', '52.9205233', '-1.474237300000027', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01332 367377', '', '', '', '2012-08-03 16:29:56'),
(114, 0, 0, 5, 'Lakeland Market Square', '', '21-23 Market Square, Witney, OX28 6AD', '51.7836717', '-1.4847372000000405', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01993 706667', '', '', '', '2012-08-03 16:30:55'),
(115, 0, 0, 1, 'Waitrose 51 Bertie Road', '', '51 Bertie Road, Kenilworth, CV8 1JP', '52.3419596', '-1.5760155000000395', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/460.html', '', '01926 858461', '', '', '', '2012-08-03 16:31:54'),
(116, 0, 0, 1, 'Waitrose Via Ravenna', '', 'Via Ravenna, Chichester, PO19 1RD', '50.83303369999999', '-0.7894116999999596', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/208.html', '', '01243 537512', '', '', '', '2012-08-03 16:32:37'),
(117, 0, 0, 5, 'Lakeland St Swithins Street', '', '13 - 14 St Swithins Street, Worcester, WR1 2PS', '52.19287440000001', '-2.2202925999999934', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01905 729746', '', '', '', '2012-08-06 08:22:26'),
(118, 0, 0, 5, 'Lakeland 55/57 Grand Arcade', '', '55/57 Grand Arcade, St David''s 2 Dewi Sant, Cardiff	, CF10 2EL', '51.4794357', '-3.1744215999999597', 'http://www.lakeland.co.uk/StoreLocator.action', '', '0292 023 5050', '', '', '', '2012-08-06 08:24:46'),
(119, 0, 0, 5, 'Lakeland Parc Trostre', '', 'Parc Trostre, Llanelli, SA14 9UY', '51.6770935', '-4.118214699999953', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01554 777 444', '', '', '', '2012-08-06 08:26:31'),
(120, 0, 0, 5, 'Lakeland New Bond Street', '', '21 New Bond Street, Bath, BA1 1BA', '51.3831234', '-2.3602899999999636', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01225 443355', '', '', '', '2012-08-06 08:30:20'),
(121, 0, 0, 5, 'Lakeland 1-4 West Village', '', '1-4 West Village, Castlepoint Shopping Park, Bournemouth, BH8 9UT', '50.752821', '-1.8439171999999644', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01202 549 737', '', '', '', '2012-08-06 08:31:33'),
(122, 0, 0, 5, 'Lakeland High Street', '', '227 High Street, Exeter, EX4 3LR', '50.724124', '-3.5307057999999643', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01392 211136', '', '', '', '2012-08-06 08:34:03'),
(123, 0, 0, 5, 'Lakeland Market Walk', '', '3/4 Market Walk, Salisbury, SP1 1BT', '51.0696411', '-1.7972706000000471', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01722 338769', '', '', '', '2012-08-06 08:35:07'),
(124, 0, 0, 5, 'Lakeland East Street', '', '15 East Street, Taunton, TA1 3LP', '51.01456289999999', '-3.1001972999999907', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01823 423163', '', '', '', '2012-08-06 08:36:13'),
(125, 0, 0, 5, 'Lakeland Boscawen Street', '', '32 Boscawen Street, Truro, TR1 2QQ', '50.2635501', '-5.050170699999967', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01872 242334', '', '', '', '2012-08-06 08:38:23'),
(126, 0, 0, 5, 'Lakeland Longmarket', '', '2-4 Longmarket, Canterbury, CT1 2JS', '51.2785622', '1.0819060999999692', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01227 453623', '', '', '', '2012-08-06 08:39:13'),
(127, 0, 0, 5, 'Lakeland North Street', '', '64 North Street, Chichester, PO19 1LT', '50.8386546', '-0.7784481999999571', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01243 530066', '', '', '', '2012-08-06 08:40:07'),
(128, 0, 0, 5, 'Lakeland Cornfield Road', '', '17 Cornfield Road, Eastbourne, BN21 4QD', '50.7677616', '0.2844211000000314', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01323 439811', '', '', '', '2012-08-06 08:53:38'),
(129, 0, 0, 5, 'Lakeland Oracle Shopping Centre', '', 'Unit L30 Oracle Shopping Centre, Minster Street, Reading	, RG1 2AQ', '51.4536453', '-0.9713166000000228', 'http://www.lakeland.co.uk/StoreDetail.action', '', '0118 939 1708', '', '', '', '2012-08-06 08:54:30'),
(130, 0, 0, 5, 'Lakeland Monson Road', '', '7 - 9 Monson Road, Tunbridge Wells, TN1 1LS', '51.1331374', '0.26558249999993677', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01892 529699', '', '', '', '2012-08-06 08:55:31'),
(131, 0, 0, 5, 'Lakeland Brent South Retail Park', '', 'Brent South Retail Park, Tilling Road, Brent Cross, London, NW2 1LS', '51.5715922', '-0.22473009999998794', 'http://www.lakeland.co.uk/StoreLocator.action', '', '020 8830 8123', '', '', '', '2012-08-06 08:56:38'),
(132, 0, 0, 5, 'Lakeland Market Square', '', '22 Market Square, Bromley, BR1 1NA', '51.4056844', '0.01528880000000754', 'http://www.lakeland.co.uk/StoreLocator.action', '', '0208 466 0372', '', '', '', '2012-08-06 09:02:27'),
(133, 0, 0, 5, 'Lakeland High Street', '', '113 High Street, Epsom, KT19 8DT', '51.3331098', '-0.26783320000004096', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01372 749098', '', '', '', '2012-08-06 09:03:13'),
(134, 0, 0, 5, 'Lakeland Bluewater Shopping Centre', '', 'Bluewater Shopping Centre, Upper Thames Walk, Greenhithe, Kent, DA9 9SQ', '51.4383395', '0.2709134000000404', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01322 624192', '', '', '', '2012-08-06 09:04:27'),
(135, 0, 0, 5, 'Lakeland Swan Lane', '', '7 - 11 Swan Lane, Guildford, GU1 4EQ', '51.2359616', '-0.5745256000000154', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01483 440626', '', '', '', '2012-08-06 09:05:18'),
(136, 0, 0, 5, 'Lakeland Thames Street', '', '23-25 Thames Street, Kingston, KT1 1PH', '51.4109209', '-0.3067164999999932', 'http://www.lakeland.co.uk/StoreLocator.action', '', '020 8546 3671', '', '', '', '2012-08-06 09:06:12'),
(137, 0, 0, 5, 'Lakeland King Edward Court', '', '12/13 King Edward Court, Windsor, London, SL4 1TF', '51.4885071', '-0.5973973999999771', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01753 842646', '', '', '', '2012-08-06 09:07:11'),
(138, 0, 0, 5, 'Lakeland Sidney Street', '', '52 Sidney Street, Cambridge, Cambridgeshire, CB2 3HX', '52.2066792', '0.12032890000000407', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01223 301418', '', '', '', '2012-08-06 09:08:03'),
(139, 0, 0, 5, 'Lakeland Springfield Road', '', '14 Springfield Road, Chelmsford, CM2 6FA', '51.73423769999999', '0.49339800000007017', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01245 264019', '', '', '', '2012-08-06 09:09:03'),
(140, 0, 0, 5, 'Lakeland The Ancient House', '', 'The Ancient House, 30 Buttermarket, Ipswich, Suffolk, IP1 1BT', '52.05693249999999', '1.1558136999999533', 'http://www.lakeland.co.uk/StoreLocator.action', '', '01473 214144', '', '', '', '2012-08-06 09:10:04'),
(141, 0, 0, 5, 'Lakeland St Stephens Street', '', '16/18 St Stephens Street, Norwich, NR1 3SA', '52.6256753', '1.2926830999999765', 'http://www.lakeland.co.uk/StoreDetail.action', '', '01603 763357', '', '', '', '2012-08-06 09:10:56'),
(142, 0, 0, 0, 'Loch Fyne Oysters Cairndow Farm Shop', '', 'Clachan, Cairndow, Argyll	, PA26 8BL', '56.27170599999999', '-4.929039999999986', 'http://www.lochfyne.com/About-Us/Loch-Fyne-Businesses/Loch-Fyne-Oysters/Cairndow-shop', '', '01499 600483', '', '', '', '2012-08-06 09:12:09'),
(143, 0, 0, 0, 'Lochaber Farm Shop', '', 'Unit 5 Lochaber Rural Complex	, Torlundy, Fort William, PH33 6SQ', '56.8557612', '-5.015568300000041', 'http://www.lochaberfarmshop.com/location.asp', '', '', '', '', '', '2012-08-06 09:13:00'),
(144, 0, 0, 0, 'Mitchells of Inverurie', '', '20-22 Market Place, Inverurie, AB51 3XN', '57.2837369', '-2.3742839999999887', 'http://www.mitchells-scotland.com/contact.php', '', '01467 621389', '', '', '', '2012-08-06 09:14:04'),
(145, 0, 0, 0, 'Morgan McVeighâ€™s', '', 'Colpy, Aberdeenshire, AB52 6UY', '57.38014499999999', '-2.5937659999999596', 'http://www.morganmcveighs.com/directions.htm', '', '01464 841399', '', '', '', '2012-08-06 09:14:48'),
(146, 0, 0, 0, 'Muddy Boots', '', 'Old Manse, Balmalcolm, Cupar, Fife, KY15 7TJ', '56.26264', '-3.1001218999999764', 'http://www.muddybootsfife.com/', '', '01337 831222', '', '', '', '2012-08-06 09:15:37'),
(147, 0, 0, 0, 'Murchies', '', '24 High Street, Kingussie, PH21 1HR', '57.0799634', '-4.0516691000000264', '', '', '01540 661227', '', '', '', '2012-08-06 09:16:13'),
(148, 0, 0, 0, 'Newton Dee Stores', '', 'North Deeside Rd, Bieldside, Aberdeen, AB15 9DX', '57.11655', '-2.1806607999999414', '', '', '01224 868609', '', '', '', '2012-08-06 09:17:07'),
(149, 0, 0, 0, 'Patisserie Francoise', '', '138a Byres Road, Glasgow, G12 8TD', '55.87363850000001', '-4.295137000000068', '', '', '0141 334 1882', '', '', '', '2012-08-06 09:17:40'),
(150, 0, 0, 0, 'Peel Farm Shop', '', 'Lintrathen, By Kirriemuir, Angus	, DD8 5JJ', '56.6706594', '-3.188128699999993', 'http://www.peelfarm.com/', '', '01575 560214', '', '', '', '2012-08-06 09:18:56'),
(151, 0, 0, 0, 'Peggy''s Tearoom', '', 'Main Street, Kirkoswald, Ayr, KA19 8HY', '55.3303562', '-4.774908800000048', '', '', '01655 760283', '', '', '', '2012-08-06 09:19:31'),
(152, 0, 0, 0, 'Phoenix Community Store', '', 'The Park, Findhorn, Forres, IV36 3TZ', '57.65103000000001', '-3.5917100000000346', 'http://www.phoenixshop.co.uk/', '', '01309 690 110', '', '', '', '2012-08-06 09:20:17'),
(153, 0, 0, 0, 'Raemoir Garden Centre', '', 'Raemoir Road, Banchory, Aberdeenshire, AB31 4EJ', '57.057956', '-2.492352100000062', 'http://www.raemoirgardencentre.co.uk/', '', '01330 825059', '', '', '', '2012-08-06 09:21:12'),
(154, 0, 0, 0, 'Reids of Dollar', '', '62 Bridge Street, Dollar, Clackmannanshire, FK14 7DG', '56.1627697', '-3.6744885000000522', '', '', '01259 742544', '', '', '', '2012-08-06 09:21:49'),
(155, 0, 0, 0, 'Robertsonâ€™s of Broughty Ferry', '', '234 Brook Street, Broughty Ferry, Dundee, Angus, DD5 2AH', '56.4669705', '-2.8743813999999475', 'http://www.robertsonofbroughtyferry.co.uk/contact.php', '', '01382 739277', '', '', '', '2012-08-06 09:22:37'),
(156, 0, 0, 0, 'Robertsonâ€™s The Larder', '', 'Tomich Farm Shop, Beauly, Inverness-Shire, IV4 7AS', '57.4982197', '-4.454755500000033', 'http://www.robertsonstomichfarmshop.co.uk/', '', '01463 782181', '', '', '', '2012-08-06 09:23:32'),
(157, 0, 0, 0, 'Rothiemurchus Visitor Centre', '', 'Rothiemurchus By Aviemore, Inverness, Inverness-Shire, PH22 1QH', '57.1762242', '-3.8173914000000195', 'http://www.rothiemurchus.net/Pages/Shop%20and%20Eat/Estate%20Farm%20Shop.html', '', '01479 812800', '', '', '', '2012-08-06 09:24:29'),
(158, 0, 0, 0, 'Sterling Mills', '', 'Moss Road, Tillicoultry, Clackmannanshire, FK13 6HQ', '56.1506722', '-3.7388607999999977', 'http://www.sterlingmills.com/', '', '01259 752100', '', '', '', '2012-08-06 09:25:26'),
(159, 0, 0, 0, 'Strachans', '', 'Main Street, Aboyne, AB34 5HX', '57.0765596', '-2.7776596999999583', 'http://www.georgestrachanltd.co.uk/', '', '013398 86121', '', '', '', '2012-08-06 09:26:52'),
(160, 0, 0, 0, 'Strathfillans Wigwams', '', 'Tyndrum, Crianlarich, Perthshire, FK20 8RU', '56.4102148', '-4.655472799999984', 'http://www.wigwamholidays.com/Strathfillan_Farm_Shop', '', '01838 400251', '', '', '', '2012-08-06 09:28:04'),
(161, 0, 0, 4, 'Superquinn Ballinteer Avenue', '', 'Ballinteer Avenue, Ballinteer, Dublin 16, Dublin', '53.2763589', '-6.246705099999986', 'http://www.superquinn.ie/storelocator/165', '', '01 2989131', '', '', '', '2012-08-06 09:29:10'),
(162, 0, 0, 4, 'Superquinn Blackrock Shopping Centre', '', 'Blackrock Shopping Centre, Blackrock, C.Dublin, Dublin', '53.2894625', '-6.200205099999948', 'http://www.superquinn.ie/storelocator/165', '', '01 2831511', '', '', '', '2012-08-06 09:30:22'),
(163, 0, 0, 4, 'Superquinn Main Street', '', 'Main Street, Blanchardstown, Dublin 15, Dublin', '53.38691590000001', '-6.377481900000021', 'http://www.superquinn.ie/storelocator/165', '', '01 8210611', '', '', '', '2012-08-06 09:31:15'),
(164, 0, 0, 4, 'Superquinn Castle Street', '', 'Castle Street, Bray, Co.Wicklow, Ireland', '53.2066398', '-6.111972100000003', 'http://www.superquinn.ie/storelocator/165', '', '01 2867779', '', '', '', '2012-08-06 09:32:04'),
(165, 0, 0, 4, 'Superquinn Kennedy Avenue', '', 'Kennedy Avenue, Carlow, Ireland', '52.8350769', '-6.929820100000029', 'http://www.superquinn.ie/storelocator/165', '', '059 9130077', '', '', '', '2012-08-06 09:32:52'),
(166, 0, 0, 4, 'Superquinn Charlesland', '', 'Charlesland, Greystores, Wicklow', '53.126276', '-6.0695137999999815', 'http://www.superquinn.ie/storelocator/165', '', '01 2878040', '', '', '', '2012-08-06 09:33:55'),
(167, 0, 0, 4, 'Superquinn Emmet Street', '', 'Emmet Street, Clonmel, Co.Tipperary, Ireland', '52.3547618', '-7.6993089000000055', 'http://www.superquinn.ie/storelocator/165', '', '052 6127222', '', '', '', '2012-08-06 09:34:52'),
(168, 0, 0, 4, 'Superquinn McKee Avenue', '', 'McKee Avenue, Finglas, Dublin 11, Ireland', '53.3910572', '-6.298232699999971', 'http://www.superquinn.ie/storelocator/165', '', '01 8341182', '', '', '', '2012-08-06 09:35:42'),
(169, 0, 0, 4, 'Superquinn Heuston South Quarter', '', 'Unit 9A-C Heuston South Quarter, St Johns Road West, Dublin', '53.3494426', '-6.260082499999953', 'http://www.superquinn.ie/storelocator/165', '', '01 6400990', '', '', '', '2012-08-06 09:36:51'),
(170, 0, 0, 4, 'Superquinn Market Cross Centre', '', 'Market Cross Centre, James Street, Kilkenny', '52.6541454', '-7.244787900000006', 'http://www.superquinn.ie/storelocator/165', '', '056 7752444', '', '', '', '2012-08-06 09:37:52'),
(171, 0, 0, 4, 'Superquinn Knocklyon Road', '', 'Knocklyon Road, Templeogue, Dublin 16, Ireland', '53.2919316', '-6.311582300000055', 'http://www.superquinn.ie/storelocator/165', '', '01 4942421', '', '', '', '2012-08-06 09:38:43'),
(172, 0, 0, 4, 'Superquinn Newcastle Road', '', 'Newcastle Road, Lucan, Co.Dublin, Ireland', '53.3449908', '-6.451670100000001', 'http://www.superquinn.ie/storelocator/165', '', '01 6240277', '', '', '', '2012-08-06 09:39:33'),
(173, 0, 0, 4, 'Superquinn Northside Shopping Centre', '', 'Northside Shopping Centre, Coolock	, Dublin 17, Ireland', '53.39521020000001', '-6.2141096999999945', 'http://www.superquinn.ie/storelocator/165', '', '01 8477111', '', '', '', '2012-08-06 09:40:31'),
(174, 0, 0, 4, 'Superquinn Portlaoise Shopping Centre', '', 'Unit 1 Portlaoise Shopping Centre, Abbeyleix Road, Laois', '52.92478', '-7.332409999999982', 'http://www.superquinn.ie/storelocator/165', '', '057 8688728', '', '', '', '2012-08-06 09:41:21'),
(175, 0, 0, 4, 'Superquinn Ranelagh Road', '', '31-33 Ranelagh Road, Dublin 6, Dublin, Ireland', '53.32577569999999', '-6.255086500000061', 'http://www.superquinn.ie/storelocator/165', '', '01 4964270', '', '', '', '2012-08-06 09:42:14'),
(176, 0, 0, 4, 'Superquinn Highfield Road', '', '13 Highfield Road, Rathgar, Dublin 6, Ireland', '53.31278450000001', '-6.265605599999958', 'http://www.superquinn.ie/storelocator/165', '', '01 4964430', '', '', '', '2012-08-06 09:43:09'),
(177, 0, 0, 4, 'Superquinn Sundrive Road', '', 'Sundrive Road, Kimmage, Dublin 12, Ireland', '53.3250585', '-6.2963015999999925', 'http://www.superquinn.ie/storelocator/165', '', '01 4921844', '', '', '', '2012-08-06 09:44:00'),
(178, 0, 0, 4, 'Superquinn Sutton Cross', '', 'Sutton Cross, Sutton	, Dublin 13, Ireland', '53.38946989999999', '-6.110116000000062', 'http://www.superquinn.ie/storelocator/165', '', '01 8322744', '', '', '', '2012-08-06 09:44:57'),
(179, 0, 0, 4, 'Superquinn The Pavillion Shopping Centre', '', 'The Pavillion Shopping Centre, Swords, Dublin', '53.4557467', '-6.219740600000023', 'http://www.superquinn.ie/storelocator/165', '', '01 8907074', '', '', '', '2012-08-06 09:45:38'),
(180, 0, 0, 4, 'Superquinn Tyrrelstown Shopping Centre', '', 'Tyrrelstown Shopping Centre, Dublin 15, Dublin, Ireland', '53.4061879', '-6.4443690000000515', 'http://www.superquinn.ie/storelocator/165', '', '01 8274004', '', '', '', '2012-08-06 09:46:26'),
(181, 0, 0, 4, 'Superquinn Walkinstown', '', 'Walkinstown, Dublin 12, Dublin, Ireland', '53.3192335', '-6.3320966', 'http://www.superquinn.ie/storelocator/165', '', '01 4505565', '', '', '', '2012-08-06 09:47:39'),
(182, 0, 0, 4, 'Superquinn Kilbarry Centre', '', 'Kilbarry Centre, Tramore Road, Waterford, Ireland', '52.2107167', '-7.120951900000023', 'http://www.superquinn.ie/storelocator/165', '', '051 370040', '', '', '', '2012-08-06 09:48:34'),
(183, 0, 0, 4, 'Superquinn Dublin Road', '', 'Dublin Road, Castletroy, Limerick, Ireland', '52.66734229999999', '-8.558244699999932', 'http://www.superquinn.ie/storelocator/165', '', '061 501140', '', '', '', '2012-08-06 09:49:25'),
(184, 0, 0, 0, 'Taste Deli', '', '47 High St, Linlithgow, EH49 7ED', '55.9767502', '-3.598665399999959', 'http://www.taste-deli-cafe.co.uk/', '', '01506 844 445', '', '', '', '2012-08-06 09:50:35'),
(185, 0, 0, 0, 'Taymouth Courtyard', '', 'Mains of Taymouth, Kenmore, Near Aberfeldy, Perthshire, PH15 2HN', '56.58782619999999', '-4.002331099999992', 'http://www.taymouthcourtyard.co.uk/location/', '', '01887 830756', '', '', '', '2012-08-06 09:51:36'),
(186, 0, 0, 0, 'The Canny Scot''s Shop', '', 'Gretna Green Group Ltd, Gretna Green, Dumfries & Galloway, DG16 5EA', '55.0050093', '-3.0620040000000017', '', '', '01461 338 441', '', '', '', '2012-08-06 09:52:16'),
(187, 0, 0, 0, 'The Deli Shop Keith', '', '153 - 155 Mid Street, Keith, AB55 5BJ', '57.54154229999999', '-2.9497490999999627', 'http://www.thedelishopkeith.co.uk/', '', '01542 887058', '', '', '', '2012-08-06 09:53:00'),
(188, 0, 0, 0, 'The Larder', '', '45 High Street, Alness, Ross-Shire, IV17 0PT', '57.6959767', '-4.25471749999997', 'http://www.victoriana-florist.co.uk/acatalog/Luxury_Hampers.html', '', '01349 883347', '', '', '', '2012-08-06 09:53:57'),
(189, 0, 0, 0, 'The Mains of Drum Garden Centre', '', 'Drumoak, Banchory, Aberdeenshire, AB31 5AN', '57.0950505', '-2.3157862000000478', 'http://www.mainsofdrum.co.uk/', '', '01330 811000', '', '', '', '2012-08-06 09:55:06'),
(190, 0, 0, 0, 'The Millers', '', 'North Lurg, Midmar, Inverurie, Aberdeenshire, AB51 7NB', '57.1452687', '-2.5044573999999784', 'http://www.millersmidmar.co.uk/', '', '01330 833462', '', '', '', '2012-08-06 09:55:54'),
(191, 0, 0, 0, 'Victor Hugo Deli', '', '26/27 Melville Terrace, Edinburgh, EH9 1LP', '55.9394656', '-3.187206100000026', 'http://www.victorhugodeli.com/aboutus.html', '', '0131 667 1827', '', '', '', '2012-08-06 09:56:44'),
(192, 0, 0, 1, 'Waitrose Merthyr Road', '', 'Merthyr Road, Llanfoist, Abergavenny, NP7 9LL', '51.81786280000001', '-3.0281961000000592', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/683.false.html', '', '01874 851184 ', '', '', '', '2012-08-06 09:57:39'),
(193, 0, 0, 1, 'Waitrose Abbey Close', '', 'Abbey Close, Abingdon, OX14 3HL', '51.6717975', '-1.2802328000000216', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/211.html', '', '01235 535003', '', '', '', '2012-08-06 09:58:30'),
(194, 0, 0, 1, 'Waitrose Penn Road', '', 'Penn Road, Beaconsfield, HP9 2PW', '51.6162588', '-0.6497759999999744', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/177.html', '', '01494 676818', '', '', '', '2012-08-06 09:59:25'),
(195, 0, 0, 1, 'Waitrose Fitzroy Street', '', '6-8 Fitzroy Street, Cambridge, CB1 1EW', '52.20690279999999', '0.13068139999995765', 'http://www.waitrose.com/content/waitrose/en/bf_home/bf/552.html', '', '0800 188 884', '', '', '', '2012-08-06 10:00:20');

-- --------------------------------------------------------

--
-- Table structure for table `store_locator_category`
--

CREATE TABLE IF NOT EXISTS `store_locator_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `marker_icon` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `store_locator_category`
--

INSERT INTO `store_locator_category` (`id`, `name`, `marker_icon`, `image`) VALUES
(1, 'Waitrose', '', ''),
(2, 'Booths', '', ''),
(3, 'Dobbies Garden Centre', '', ''),
(4, 'Superquinn', '', ''),
(5, 'Lakeland', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
