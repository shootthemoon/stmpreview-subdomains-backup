-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2014 at 11:49 AM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stmbrands_db_stage`
--

-- --------------------------------------------------------

--
-- Table structure for table `exp_accessories`
--

CREATE TABLE IF NOT EXISTS `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_accessories`
--

INSERT INTO `exp_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`) VALUES
(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'),
(3, 'Ep_better_workflow_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.5'),
(5, 'Structure_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '3.3.4');

-- --------------------------------------------------------

--
-- Table structure for table `exp_actions`
--

CREATE TABLE IF NOT EXISTS `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `exp_actions`
--

INSERT INTO `exp_actions` (`action_id`, `class`, `method`) VALUES
(1, 'Comment', 'insert_new_comment'),
(2, 'Comment_mcp', 'delete_comment_notification'),
(3, 'Comment', 'comment_subscribe'),
(4, 'Comment', 'edit_comment'),
(5, 'Email', 'send_email'),
(6, 'Safecracker', 'submit_entry'),
(7, 'Safecracker', 'combo_loader'),
(8, 'Search', 'do_search'),
(9, 'Channel', 'insert_new_entry'),
(10, 'Channel', 'filemanager_endpoint'),
(11, 'Channel', 'smiley_pop'),
(12, 'Member', 'registration_form'),
(13, 'Member', 'register_member'),
(14, 'Member', 'activate_member'),
(15, 'Member', 'member_login'),
(16, 'Member', 'member_logout'),
(17, 'Member', 'retrieve_password'),
(18, 'Member', 'reset_password'),
(19, 'Member', 'send_member_email'),
(20, 'Member', 'update_un_pw'),
(21, 'Member', 'member_search'),
(22, 'Member', 'member_delete'),
(23, 'Rte', 'get_js'),
(25, 'Assets_mcp', 'get_subfolders'),
(26, 'Assets_mcp', 'upload_file'),
(27, 'Assets_mcp', 'get_files_view_by_folders'),
(28, 'Assets_mcp', 'get_props'),
(29, 'Assets_mcp', 'save_props'),
(30, 'Assets_mcp', 'get_ordered_files_view'),
(31, 'Assets_mcp', 'move_folder'),
(32, 'Assets_mcp', 'create_folder'),
(33, 'Assets_mcp', 'delete_folder'),
(34, 'Assets_mcp', 'view_file'),
(35, 'Assets_mcp', 'move_file'),
(36, 'Assets_mcp', 'delete_file'),
(37, 'Assets_mcp', 'build_sheet'),
(38, 'Assets_mcp', 'get_selected_files'),
(40, 'Structure', 'ajax_move_set_data'),
(41, 'Freeform', 'insert_new_entry'),
(42, 'Freeform', 'retrieve_entries'),
(43, 'Freeform', 'delete_freeform_notification'),
(44, 'Zoo_flexible_admin', 'ajax_preview'),
(45, 'Zoo_flexible_admin', 'ajax_load_tree'),
(46, 'Zoo_flexible_admin', 'ajax_load_settings'),
(47, 'Zoo_flexible_admin', 'ajax_save_tree'),
(48, 'Zoo_flexible_admin', 'ajax_remove_tree'),
(49, 'Zoo_flexible_admin', 'ajax_copy_tree'),
(50, 'Backup_proish', 'cron'),
(51, 'Playa_mcp', 'filter_entries');

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets`
--

CREATE TABLE IF NOT EXISTS `exp_assets` (
  `asset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` int(10) unsigned DEFAULT NULL,
  `alt_text` tinytext,
  `caption` tinytext,
  `author` tinytext,
  `desc` text,
  `location` tinytext,
  `keywords` text,
  PRIMARY KEY (`asset_id`),
  UNIQUE KEY `unq_file_path` (`file_path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `exp_assets`
--

INSERT INTO `exp_assets` (`asset_id`, `file_path`, `title`, `date`, `alt_text`, `caption`, `author`, `desc`, `location`, `keywords`) VALUES
(1, '{filedir_1}dummy-4.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '{filedir_3}dummy-4.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '{filedir_3}dummy-4_thum.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '{filedir_3}dummy-4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '{filedir_3}dummy-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '{filedir_3}dummy-5.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '{filedir_3}dummy-6.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '{filedir_3}dummy-7.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '{filedir_1}nubu_mojito.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '{filedir_1}nubu_maitai.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '{filedir_1}nubu_cosmopolitan.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '{filedir_3}CONSUMABLES.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '{filedir_3}COSMETICS.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '{filedir_3}ACCESSORIES.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '{filedir_3}CONSUMABLES_HERO.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '{filedir_3}COSMETICS_HERO.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '{filedir_3}ACCESSORIES_HERO.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '{filedir_3}hero-Jackpot.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '{filedir_1}lightly-salted.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '{filedir_1}sea-salt.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '{filedir_1}chedar.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '{filedir_1}sweet-chilli.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '{filedir_1}roast-beef.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '{filedir_3}Shimmer-Stack-hero.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '{filedir_1}Bronze-eyes.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '{filedir_1}Smokey-eyes.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, '{filedir_1}Little-angels.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, '{filedir_1}Nail-Collection.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, '{filedir_1}Shimmer-Stack.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '{filedir_3}Black.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '{filedir_1}Black.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '{filedir_1}Bronze.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, '{filedir_1}Gold.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, '{filedir_1}Leopard.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, '{filedir_3}Hero-Tipsy-Feet.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, '{filedir_3}Hero-Ice-Diamonds.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, '{filedir_1}Black-mimi.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, '{filedir_1}Blue-mini.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, '{filedir_1}Pink-mini.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, '{filedir_1}Purple-mini.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '{filedir_1}White-mini.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '{filedir_3}Hero-Nuba.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, '{filedir_1}mojito.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '{filedir_1}mai-tai.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, '{filedir_1}cosmo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, '{filedir_1}cosmo-420-x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, '{filedir_1}mojito-420-x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, '{filedir_1}on-the-beach420-x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, '{filedir_1}pina-420-x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, '{filedir_3}Glamourous-bra-strap-420x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '{filedir_1}bra-strap-black-stone-SG-row.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, '{filedir_1}bra-strap-multi-best.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, '{filedir_1}bra-strap-pearl.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, '{filedir_1}bra-strap-silver-clear-stone.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, '{filedir_3}choc-brownie.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, '{filedir_1}choc-brownie.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, '{filedir_3}Authentic-hero.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, '{filedir_1}bra-strap-black-stone-with-bars.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, '{filedir_1}silver-strap-x-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, '{filedir_3}Hewbys.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, '{filedir_3}ispyEye.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, '{filedir_3}nuba1-567x606.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, '{filedir_3}Bra-Straps-Large1.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, '{filedir_3}iceDiamond.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, '{filedir_3}Janice.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, '{filedir_3}janice-smith.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_assets_entries`
--

CREATE TABLE IF NOT EXISTS `exp_assets_entries` (
  `asset_id` int(10) unsigned DEFAULT NULL,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `col_id` int(6) unsigned DEFAULT NULL,
  `row_id` int(10) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `asset_order` int(4) unsigned DEFAULT NULL,
  KEY `asset_id` (`asset_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `col_id` (`col_id`),
  KEY `row_id` (`row_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_assets_entries`
--

INSERT INTO `exp_assets_entries` (`asset_id`, `entry_id`, `field_id`, `col_id`, `row_id`, `var_id`, `asset_order`) VALUES
(16, 8, 6, 8, 48, NULL, 0),
(13, 8, 6, 9, 48, NULL, 0),
(17, 9, 6, 8, 49, NULL, 0),
(14, 9, 6, 9, 49, NULL, 0),
(19, 17, 2, 7, 9, NULL, 0),
(20, 17, 2, 7, 10, NULL, 0),
(21, 17, 2, 7, 11, NULL, 0),
(22, 17, 2, 7, 12, NULL, 0),
(23, 17, 2, 7, 13, NULL, 0),
(18, 17, 7, 13, 40, NULL, 0),
(18, 17, 7, 13, 51, NULL, 0),
(19, 17, 2, 7, 52, NULL, 0),
(20, 17, 2, 7, 53, NULL, 0),
(21, 17, 2, 7, 54, NULL, 0),
(22, 17, 2, 7, 55, NULL, 0),
(23, 17, 2, 7, 56, NULL, 0),
(56, 24, 2, 7, 39, NULL, 0),
(60, 24, 7, 13, 45, NULL, 0),
(61, 14, 13, NULL, NULL, NULL, 0),
(64, 30, 13, NULL, NULL, NULL, 0),
(15, 7, 6, 8, 67, NULL, 0),
(12, 7, 6, 9, 67, NULL, 0),
(19, 17, 2, 7, 58, NULL, 0),
(20, 17, 2, 7, 59, NULL, 0),
(21, 17, 2, 7, 60, NULL, 0),
(22, 17, 2, 7, 61, NULL, 0),
(23, 17, 2, 7, 62, NULL, 0),
(18, 17, 7, 13, 57, NULL, 0),
(43, 21, 2, 7, 28, NULL, 0),
(44, 21, 2, 7, 29, NULL, 0),
(45, 21, 2, 7, 30, NULL, 0),
(42, 21, 7, 13, 44, NULL, 0),
(46, 22, 2, 7, 31, NULL, 0),
(47, 22, 2, 7, 32, NULL, 0),
(48, 22, 2, 7, 33, NULL, 0),
(49, 22, 2, 7, 34, NULL, 0),
(57, 22, 7, 13, 50, NULL, 0),
(31, 19, 2, 7, 19, NULL, 0),
(32, 19, 2, 7, 20, NULL, 0),
(33, 19, 2, 7, 21, NULL, 0),
(34, 19, 2, 7, 22, NULL, 0),
(35, 19, 7, 13, 42, NULL, 0),
(37, 20, 2, 7, 23, NULL, 0),
(38, 20, 2, 7, 24, NULL, 0),
(39, 20, 2, 7, 25, NULL, 0),
(40, 20, 2, 7, 26, NULL, 0),
(41, 20, 2, 7, 27, NULL, 0),
(36, 20, 7, 13, 43, NULL, 0),
(25, 18, 2, 7, 14, NULL, 0),
(26, 18, 2, 7, 15, NULL, 0),
(27, 18, 2, 7, 16, NULL, 0),
(28, 18, 2, 7, 17, NULL, 0),
(29, 18, 2, 7, 18, NULL, 0),
(24, 18, 7, 13, 41, NULL, 0),
(51, 23, 2, 7, 35, NULL, 0),
(52, 23, 2, 7, 36, NULL, 0),
(53, 23, 2, 7, 37, NULL, 0),
(54, 23, 2, 7, 38, NULL, 0),
(59, 23, 2, 7, 64, NULL, 0),
(58, 23, 2, 7, 65, NULL, 0),
(50, 23, 7, 13, 46, NULL, 0),
(61, 33, 13, NULL, NULL, NULL, 0),
(63, 28, 13, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_backup_proish_settings`
--

CREATE TABLE IF NOT EXISTS `exp_backup_proish_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(30) NOT NULL DEFAULT '',
  `setting_value` text NOT NULL,
  `serialized` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_backup_proish_settings`
--

INSERT INTO `exp_backup_proish_settings` (`id`, `setting_key`, `setting_value`, `serialized`) VALUES
(1, 'backup_file_location', '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands', 0),
(2, 'backup_store_location', '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/backups', 0),
(3, 'auto_threshold', '0', 0),
(4, 'exclude_paths', 'a:1:{i:0;s:0:"";}', 1),
(5, 'cron_notify_emails', 'a:0:{}', 1),
(6, 'cron_attach_backups', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_captcha`
--

CREATE TABLE IF NOT EXISTS `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_categories`
--

CREATE TABLE IF NOT EXISTS `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_categories`
--

INSERT INTO `exp_categories` (`cat_id`, `site_id`, `group_id`, `parent_id`, `cat_name`, `cat_url_title`, `cat_description`, `cat_image`, `cat_order`) VALUES
(1, 1, 1, 0, 'Consumables', 'consumables', '', '0', 2),
(2, 1, 1, 0, 'Cosmetics', 'cosmetics', '', '0', 3),
(3, 1, 1, 0, 'Accessories', 'accessories', '', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_fields`
--

CREATE TABLE IF NOT EXISTS `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_field_data`
--

CREATE TABLE IF NOT EXISTS `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_category_field_data`
--

INSERT INTO `exp_category_field_data` (`cat_id`, `site_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_groups`
--

CREATE TABLE IF NOT EXISTS `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_category_groups`
--

INSERT INTO `exp_category_groups` (`group_id`, `site_id`, `group_name`, `sort_order`, `exclude_group`, `field_html_formatting`, `can_edit_categories`, `can_delete_categories`) VALUES
(1, 1, 'Products', 'a', 0, 'all', '|6', '|6');

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_posts`
--

CREATE TABLE IF NOT EXISTS `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channels`
--

CREATE TABLE IF NOT EXISTS `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_channels`
--

INSERT INTO `exp_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`) VALUES
(2, 1, 'homepage', 'Homepage', 'http://stmbrands.stmpreview.co.uk/index.php', NULL, 'en', 1, 0, 1354900389, 0, '', 2, 'draft', 2, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(3, 1, 'product-category', 'Product Category', 'http://stmbrands.stmpreview.co.uk/index.php', '', 'en', 3, 0, 1355239053, 0, '', 2, 'draft', 4, NULL, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(6, 1, 'product-brand', 'Product Brand', 'http://stmbrands.stmpreview.co.uk/', '', 'en', 8, 0, 1355333129, 0, '', 2, 'draft', 1, 2, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(7, 1, 'generic', 'Generic', 'http://stmbrands.stmpreview.co.uk/', NULL, 'en', 4, 0, 1357306214, 0, '', 2, 'draft', 6, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(8, 1, 'news', 'News', 'http://stmbrands.stmpreview.co.uk/', NULL, 'en', 4, 0, 1357814295, 0, '', 2, 'draft', 3, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(9, 1, 'procurement_logistics', 'Procurement & Logistics', 'http://stmbrands.stmpreview.co.uk/', NULL, 'en', 0, 0, 0, 0, NULL, 2, 'draft', NULL, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0),
(10, 1, 'seo', 'SEO', 'http://stmbrands.stmpreview.co.uk/index.php', NULL, 'en', 0, 0, 0, 0, '', 2, 'draft', NULL, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, '', NULL, 'y', NULL, 'n', 10, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_data`
--

CREATE TABLE IF NOT EXISTS `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_data`
--

INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_2`, `field_ft_2`, `field_id_5`, `field_ft_5`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_10`, `field_ft_10`, `field_id_12`, `field_ft_12`, `field_id_13`, `field_ft_13`, `field_id_22`, `field_ft_22`) VALUES
(1, 1, 2, '', 'none', '', 'none', '', 'none', '', 'none', 'Delivering innovation to the inflight & travel retail sector <br />- a refreshingly integrated approach to brand development, creative & marketing support, purchasing, sales & distribution...', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(7, 1, 3, '', 'none', 'Continuing to be a key driver of sales growth within the sector - new ideas, differentiation and incremental sales are essential to sustain this.  Our NPD programme and brands secured within the portfolio, deliver both the consumer experience and the margin and technical requirements stipulated by the industry.', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(8, 1, 3, '', 'none', 'So what will be the New Big Thing? That''s always the question. Being immersed in the travel industry and the P&C market, makes our team ideally placed to identify opportunities and nurture brands, to deliver category growth.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(9, 1, 3, '', 'none', 'Be it an absolute "must have" or an impulse purchase; understanding the consumer profile, motivation and selecting winning products and the correct positioning is crucial to delivering category potential.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none'),
(13, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '', 'none', '', 'none'),
(14, 1, 8, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '<p>\n	Shoot the Moon are supporting iSPY 2013 as digital sponsors The agreement was announced earlier this month and will see the iSPY website rebuilt, improved and managed by the agency. See <a href="http://www.ispy-international.com/">www.ispy-international.com</a></p>', 'none', '<p>\n	Shoot the Moon are supporting iSPY 2013 as digital sponsors The agreement was announced earlier this month and will see the iSPY website rebuilt, improved and managed by the agency. See <a href="http://www.ispy-international.com/">www.ispy-international.com</a></p>', 'none', '{filedir_3}ispyEye.jpeg', 'none', '', 'none'),
(16, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(17, 1, 6, '1', 'none', '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(18, 1, 6, '1', 'none', '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(19, 1, 6, '1', 'none', '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(20, 1, 6, '1', 'none', '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(21, 1, 6, '1', 'none', '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(22, 1, 6, '1', 'none', '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(23, 1, 6, '1', 'none', '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(24, 1, 6, '1', 'none', '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(25, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(27, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none'),
(28, 1, 8, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '<p>\n	Available from STM Brands&hellip;&nbsp;Glamorous Bra Straps &ndash; Diamante, Suede and Glass Stones. Transform your look in seconds with a pair of jewelled bra straps!</p>', 'none', '<h2>\n	Available now from STM Brands&hellip;</h2>\n<p>\n	Glamorous Bra Straps &ndash; Diamante, Suede and Glass Stones. Transform your look in seconds with a pair of jewelled bra straps!</p>\n<p>\n	These&nbsp; bra straps are easy to wear with no fuss or adjustments necessary. One size fits all.</p>\n<ol>\n	<li>\n		First, you need a multi way, a strapless, or a bra that has detachable straps and remove existing straps from bra.</li>\n	<li>\n		Using the same fastening loops, simply replace with your new glamorous bra straps.</li>\n	<li>\n		Put your bra on as normal with your matching outfit and enjoy the glamorous experience.</li>\n	<li>\n		Glamorous Bra Straps will look stunning with the following style of outfits :</li>\n	<li>\n		Off the shoulder tops</li>\n	<li>\n		Small strappy tops or dresses</li>\n	<li>\n		Halter neck tops Tops that have a deep cut at the front or back of shoulders</li>\n	<li>\n		Strapless tops</li>\n	<li>\n		Bikinis</li>\n	<li>\n		Swimming costumes</li>\n</ol>\n<p>\n	<strong>Don&rsquo;t go out without them!</strong></p>', 'none', '{filedir_3}Bra-Straps-Large1.jpeg', 'none', '', NULL),
(29, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '<p>\n	Coming together with a wide range of experience spanning manufacture, inflight, foodservice and highstreet retail; our team are perfectly placed to develop and nurture products for the inflight and travel retail sector.</p>\n<p>\n	Creatively, the business is supported by our sister agency, Shoot the Moon; comprising of a specialist team of designers, artworkers, photographers and marketeers, with specialist skills in retail comms, packaging and advertising.</p>\n<p>\n	Whether you are looking for bespoke development from scratch, support and representation within the sector, or are a sector buyer looking for innovation, we would love to meet up to discuss the opportunities.</p>', 'none'),
(30, 1, 8, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '<p>\n	<strong>Travel straighteners from Ice Diamond &ndash; exclusive to STM Brands</strong></p>\n<p>\n	At just 150mm long, this versatile mini iron can be your constant companion.<br />\n	Compact yet powerful, this iron reaches 200&deg;C in 90 seconds and gives salon style results on shorter hair making it ideal for both men and women!!</p>', 'none', '<h2>\n	Travel straighteners from Ice Diamond &ndash; exclusive to STM Brands</h2>\n<p>\n	At just 150mm long, this versatile mini iron can be your constant companion.</p>\n<p>\n	Compact yet powerful, this iron reaches 200&deg;C in 90 seconds and gives salon style results on shorter hair making it ideal for both men and women!!</p>\n<p>\n	Other features include:</p>\n<ul>\n	<li>\n		Floating Tourmaline plates with advanced ionic technology</li>\n	<li>\n		2 metre 360&deg; tangle free professional swivel cord</li>\n	<li>\n		Elegantly styled with diamante inset and supplied with Ice Diamond travel pouch</li>\n	<li>\n		Dual Voltage</li>\n	<li>\n		Fitted with EU/UK adaptor plug</li>\n</ul>', 'none', '{filedir_3}iceDiamond.jpeg', 'none', '', NULL),
(33, 1, 8, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '<p>\n	<strong>Almost ready for an exciting week in Brighton...</strong></p>\n<p>\n	If you are attending and would like to meet up for a chat during the week, please call me on 07800 897672.&nbsp; Phil &amp; Oli will also be down Wednesday to Saturday.</p>', 'none', '<p>\n	<a href="http://www.ispy-international.com">iSPY 2013</a>&nbsp;sponsored by STM Brands &amp; <a href="http://www.shoot-the-moon.co.uk">Shoot the Moon Design Consultants</a></p>', 'none', '{filedir_3}ispyEye.jpeg', 'none', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_entries_autosave`
--

CREATE TABLE IF NOT EXISTS `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_fields`
--

CREATE TABLE IF NOT EXISTS `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `exp_channel_fields`
--

INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`) VALUES
(2, 1, 1, 'products', 'Products', '', 'matrix', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO3M6MToiNSI7aToxO3M6MToiNyI7aToyO3M6MToiNiI7fX0='),
(5, 1, 4, 'description', 'Description', '', 'textarea', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(6, 1, 4, 'images', 'Images', '', 'matrix', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjgiO2k6MTtzOjE6IjkiO319'),
(7, 1, 1, 'brand', 'Brand', '', 'matrix', '', '0', 0, 0, 'channel', 7, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTMiO2k6MTtzOjI6IjE0Ijt9fQ=='),
(8, 1, 2, 'homepage_copy', 'Homepage Copy', '', 'textarea', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(9, 1, 2, 'snippet_copy', 'Snippet Copy', '', 'matrix', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtzOjI6IjEwIjtpOjE7czoyOiIxMSI7aToyO3M6MjoiMTIiO319'),
(10, 1, 3, 'news_snippet', 'News Snippet', '', 'wygwam', '', '0', 0, 0, 'channel', 7, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo5OntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(12, 1, 3, 'news_story', 'News Story', '', 'wygwam', '', '0', 0, 0, 'channel', 7, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
(13, 1, 3, 'news_image', 'News Image', '', 'assets', '', '0', 0, 0, 'channel', 0, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YToxMDp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czoxOiIzIjt9czo1OiJtdWx0aSI7czoxOiJuIjtzOjQ6InZpZXciO3M6NjoidGh1bWJzIjtzOjk6InNob3dfY29scyI7YTo0OntpOjA7czo0OiJuYW1lIjtpOjE7czo2OiJmb2xkZXIiO2k6MjtzOjQ6ImRhdGUiO2k6MztzOjQ6InNpemUiO31zOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(22, 1, 6, 'body_text', 'Body Text', '', 'wygwam', '', '0', 0, 0, 'channel', 7, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_member_groups`
--

INSERT INTO `exp_channel_member_groups` (`group_id`, `channel_id`) VALUES
(6, 2),
(6, 3),
(6, 6),
(6, 7),
(6, 8),
(6, 9);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_titles`
--

CREATE TABLE IF NOT EXISTS `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `exp_channel_titles`
--

INSERT INTO `exp_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `pentry_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `dst_enabled`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`) VALUES
(1, 1, 2, 1, 0, NULL, '109.111.197.225', 'STM Brands', 'stm-brands', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1354900389, 'n', '2012', '12', '07', 0, 0, 20130315173110, 0, 0),
(7, 1, 3, 1, 0, NULL, '109.111.197.225', 'Food and Drink', 'food-and-drink', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355235126, 'n', '2012', '12', '11', 0, 0, 20130110092207, 0, 0),
(8, 1, 3, 1, 0, NULL, '109.111.197.225', 'Cosmetics', 'cosmetics', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355235359, 'n', '2012', '12', '11', 0, 0, 20130110093600, 0, 0),
(9, 1, 3, 1, 0, NULL, '109.111.197.225', 'Accessories', 'accessories', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355239053, 'n', '2012', '12', '11', 0, 0, 20130110092534, 0, 0),
(13, 1, 7, 1, 0, NULL, '127.0.0.1', 'News', 'news', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355311718, 'n', '2012', '12', '12', 0, 0, 20121212113039, 0, 0),
(14, 1, 8, 1, 0, NULL, '109.111.197.225', 'STM Brands & Shoot the Moon confirm support for iSPY 2013', 'stm-brands-and-shoot-the-moon-confirm-support-for-ispy-2013', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1346406910, 'n', '2012', '08', '31', 0, 0, 20130107095611, 0, 0),
(16, 1, 7, 1, 0, NULL, '127.0.0.1', 'Get in Touch', 'get-in-touch', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355314780, 'n', '2012', '12', '12', 0, 0, 20121212131941, 0, 0),
(17, 1, 6, 1, 0, NULL, '109.111.197.225', 'Jackpots', 'jackpots', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355326130, 'n', '2012', '12', '12', 0, 0, 20130110092251, 0, 0),
(18, 1, 6, 1, 0, NULL, '109.111.197.225', 'Me Me Me', 'me-me-me', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355328988, 'n', '2012', '12', '12', 0, 0, 20130110093729, 0, 0),
(19, 1, 6, 1, 0, NULL, '109.111.197.225', 'Tipsyfeet', 'tipsyfeet', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355329921, 'n', '2012', '12', '12', 0, 0, 20130110093002, 0, 0),
(20, 1, 6, 1, 0, NULL, '109.111.197.225', 'Ice Diamonds', 'ice-diamonds', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355330477, 'n', '2012', '12', '12', 0, 0, 20130110093118, 0, 0),
(21, 1, 6, 1, 0, NULL, '109.111.197.225', 'Nuba Cocktails', 'nuba-cocktails', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355330943, 'n', '2012', '12', '12', 0, 0, 20130110092404, 0, 0),
(22, 1, 6, 1, 0, NULL, '109.111.197.225', 'The Authentic Cocktail', 'the-authentic-cocktail', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355331545, 'n', '2012', '12', '12', 0, 0, 20130110092506, 0, 0),
(23, 1, 6, 1, 0, NULL, '109.111.197.225', 'Glamorous Bra Straps', 'glamorous-bra-straps', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355332492, 'n', '2012', '12', '12', 0, 0, 20130110101353, 0, 0),
(24, 1, 6, 1, 0, NULL, '109.111.197.225', 'Hewby''s Bakehouse', 'Hewbys-Bakehouse', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355333129, 'n', '2012', '12', '12', 0, 0, 20121221115730, 0, 0),
(25, 1, 7, 1, 0, NULL, '109.111.197.225', 'ALL SEO', 'seo', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1355408542, 'n', '2012', '12', '13', 0, 0, 20121213142323, 0, 0),
(27, 1, 7, 1, 0, NULL, '109.111.197.225', 'Our Distribution and Procurement', 'our-distribution-and-procurement', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1355746261, 'n', '2012', '12', '17', 0, 0, 20130110094902, 0, 0),
(28, 1, 8, 1, 0, NULL, '109.111.197.225', 'A touch of glamour onboard Thomson Airways', 'add-a-touch-of-glamour', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357814295, 'n', '2013', '01', '10', 0, 0, 20130110104016, 0, 0),
(29, 1, 7, 1, 0, NULL, '109.111.197.225', 'Our Team', 'our-team', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357306214, 'n', '2013', '01', '04', 0, 0, 20130107111115, 0, 0),
(30, 1, 8, 1, 0, NULL, '109.111.197.225', 'Perfect for Packing…', 'perfect-for-packing', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351591477, 'n', '2012', '10', '30', 0, 0, 20130107100938, 0, 0),
(33, 1, 8, 1, 0, NULL, '109.111.197.225', 'All packed for iSPY', 'all-packed-for-ispy', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1357813722, 'n', '2013', '01', '10', 0, 0, 20130110103643, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_comments`
--

CREATE TABLE IF NOT EXISTS `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_comment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_log`
--

CREATE TABLE IF NOT EXISTS `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `exp_cp_log`
--

INSERT INTO `exp_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`) VALUES
(1, 1, 1, 'Admin', '127.0.0.1', 1354898169, 'Logged in'),
(2, 1, 1, 'Admin', '127.0.0.1', 1354899912, 'Channel Created:&nbsp;&nbsp;Products'),
(3, 1, 1, 'Admin', '127.0.0.1', 1354899936, 'Category Group Created:&nbsp;&nbsp;Products'),
(4, 1, 1, 'Admin', '127.0.0.1', 1354900019, 'Field Group Created:&nbsp;Products'),
(5, 1, 1, 'Admin', '127.0.0.1', 1354900123, 'Channel Created:&nbsp;&nbsp;Homepage'),
(6, 1, 1, 'Admin', '127.0.0.1', 1354900257, 'Logged out'),
(7, 1, 1, 'Admin', '127.0.0.1', 1354900567, 'Channel Created:&nbsp;&nbsp;Product Landing'),
(8, 1, 1, 'Admin', '127.0.0.1', 1354900742, 'Field Group Created:&nbsp;Generic'),
(9, 1, 1, 'Admin', '127.0.0.1', 1355222927, 'Channel Created:&nbsp;&nbsp;News Articles'),
(10, 1, 1, 'Admin', '127.0.0.1', 1355222966, 'Channel Created:&nbsp;&nbsp;News Landing'),
(11, 1, 1, 'Admin', '127.0.0.1', 1355223082, 'Field Group Created:&nbsp;News'),
(12, 1, 1, 'Admin', '127.0.0.1', 1355229075, 'Logged in'),
(13, 1, 1, 'Admin', '127.0.0.1', 1355234519, 'Logged out'),
(14, 1, 1, 'Admin', '127.0.0.1', 1355234533, 'Logged in'),
(15, 1, 1, 'Admin', '127.0.0.1', 1355239399, 'Channel Created:&nbsp;&nbsp;Product Brand'),
(16, 1, 1, 'Admin', '127.0.0.1', 1355239710, 'Channel Created:&nbsp;&nbsp;Product Brand'),
(17, 1, 1, 'Admin', '127.0.0.1', 1355240227, 'Channel Deleted:&nbsp;&nbsp;Product'),
(18, 1, 1, 'Admin', '127.0.0.1', 1355240328, 'Custom Field Deleted:&nbsp;Homepage Product Panels'),
(19, 1, 1, 'Admin', '127.0.0.1', 1355245625, 'Field Group Created:&nbsp;Product Category'),
(20, 1, 1, 'Admin', '127.0.0.1', 1355245923, 'Channel Deleted:&nbsp;&nbsp;News Landing'),
(21, 1, 1, 'Admin', '127.0.0.1', 1355245929, 'Channel Deleted:&nbsp;&nbsp;Consumable Products'),
(22, 1, 1, 'Admin', '127.0.0.1', 1355247932, 'Custom Field Deleted:&nbsp;Brand Image'),
(23, 1, 1, 'Admin', '127.0.0.1', 1355247936, 'Custom Field Deleted:&nbsp;Brand Thumbnail'),
(24, 1, 1, 'Admin', '127.0.0.1', 1355304671, 'Logged in'),
(25, 1, 1, 'Admin', '127.0.0.1', 1355311650, 'Channel Created:&nbsp;&nbsp;Generic'),
(26, 1, 1, 'Admin', '127.0.0.1', 1355311756, 'Channel Created:&nbsp;&nbsp;News'),
(27, 1, 1, 'Admin', '127.0.0.1', 1355319974, 'Channel Created:&nbsp;&nbsp;Procurement & Logistics'),
(28, 1, 1, 'Admin', '127.0.0.1', 1355320098, 'Channel Deleted:&nbsp;&nbsp;News Articles'),
(29, 1, 1, 'Admin', '127.0.0.1', 1355324127, 'Logged out'),
(30, 1, 1, 'Admin', '127.0.0.1', 1355324370, 'Logged in'),
(31, 1, 1, 'Admin', '109.111.197.225', 1355391727, 'Logged in'),
(32, 1, 1, 'Admin', '109.111.197.225', 1355393385, 'Member Group Created:&nbsp;&nbsp;STM Staff'),
(33, 1, 1, 'Admin', '109.111.197.225', 1355393449, 'Member profile created:&nbsp;&nbsp;stmstaff'),
(34, 1, 2, 'stmstaff', '109.111.197.225', 1355393832, 'Logged in'),
(35, 1, 1, 'Admin', '109.111.197.225', 1355395235, 'Custom Field Deleted:&nbsp;Brand Description'),
(36, 1, 2, 'stmstaff', '109.111.197.225', 1355397324, 'Logged out'),
(37, 1, 1, 'Admin', '109.111.197.225', 1355408247, 'Channel Created:&nbsp;&nbsp;SEO'),
(38, 1, 1, 'Admin', '109.111.197.225', 1355408266, 'Custom Field Deleted:&nbsp;SEO'),
(39, 1, 1, 'Admin', '109.111.197.225', 1355408288, 'Field Group Created:&nbsp;SEO'),
(40, 1, 1, 'Admin', '109.111.197.225', 1355500943, 'Field group Deleted:&nbsp;&nbsp;SEO'),
(41, 1, 1, 'Admin', '109.111.197.225', 1355746514, 'Field Group Created:&nbsp;Generic'),
(42, 1, 2, 'stmstaff', '109.111.197.225', 1356086464, 'Logged out'),
(43, 1, 1, 'Admin', '109.111.197.225', 1356090947, 'Logged out'),
(44, 1, 1, 'Admin', '109.111.197.225', 1356103757, 'Logged out'),
(45, 1, 1, 'Admin', '109.111.197.225', 1357556739, 'Screen name was changed to:&nbsp;&nbsp;Janice'),
(46, 1, 1, 'Admin', '109.111.197.225', 1357812843, 'Logged out'),
(47, 1, 1, 'Admin', '109.111.197.225', 1357813413, 'Logged out'),
(48, 1, 1, 'Admin', '109.111.197.225', 1357814582, 'Logged out'),
(49, 1, 1, 'Admin', '109.111.197.225', 1360159764, 'Logged out'),
(50, 1, 1, 'Admin', '109.111.197.225', 1360159862, 'Logged out'),
(51, 1, 1, 'Admin', '109.111.197.225', 1360160064, 'Logged out'),
(52, 1, 1, 'Admin', '109.111.197.225', 1360160612, 'Logged out');

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_search_index`
--

CREATE TABLE IF NOT EXISTS `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_developer_log`
--

CREATE TABLE IF NOT EXISTS `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_mg`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_ml`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_console_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_ping_status`
--

CREATE TABLE IF NOT EXISTS `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_versioning`
--

CREATE TABLE IF NOT EXISTS `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts` (
  `ep_entry_drafts_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) NOT NULL,
  `author_id` int(10) NOT NULL,
  `channel_id` int(10) NOT NULL,
  `site_id` int(10) NOT NULL,
  `status` varchar(255) NOT NULL,
  `url_title` varchar(255) NOT NULL,
  `draft_data` text,
  `expiration_date` int(10) NOT NULL,
  `edit_date` int(10) NOT NULL,
  `entry_date` int(10) NOT NULL,
  PRIMARY KEY (`ep_entry_drafts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts_auth`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts_auth` (
  `ep_auth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `timestamp` int(10) NOT NULL,
  PRIMARY KEY (`ep_auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_entry_drafts_thirdparty`
--

CREATE TABLE IF NOT EXISTS `exp_ep_entry_drafts_thirdparty` (
  `entry_id` int(10) NOT NULL,
  `field_id` int(10) NOT NULL,
  `type` varchar(255) NOT NULL,
  `row_id` varchar(25) NOT NULL,
  `row_order` int(10) NOT NULL,
  `col_id` int(10) NOT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_roles`
--

CREATE TABLE IF NOT EXISTS `exp_ep_roles` (
  `site_id` int(10) NOT NULL,
  `role` varchar(255) NOT NULL,
  `states` text,
  PRIMARY KEY (`site_id`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_ep_roles`
--

INSERT INTO `exp_ep_roles` (`site_id`, `role`, `states`) VALUES
(1, 'editor', 'a:7:{s:9:"null|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"draft|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:14:"submitted|null";a:4:{s:5:"label";s:26:"bwf_status_label_submitted";s:8:"can_edit";b:0;s:11:"can_preview";b:0;s:7:"buttons";a:1:{i:0;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:9:"open|null";a:2:{s:5:"label";s:21:"bwf_status_label_live";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"open|draft";a:2:{s:5:"label";s:27:"bwf_status_label_draft_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}i:2;a:5:{i:0;N;i:1;s:10:"epBwfDraft";i:2;s:6:"delete";i:3;s:21:"bwf_btn_discard_draft";i:4;b:0;}}}s:14:"open|submitted";a:4:{s:5:"label";s:31:"bwf_status_label_submitted_live";s:8:"can_edit";b:0;s:11:"can_preview";b:0;s:7:"buttons";a:1:{i:0;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:11:"closed|null";a:2:{s:5:"label";s:25:"bwf_status_label_archived";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:9:"submitted";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:14:"bwf_btn_submit";i:4;b:1;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}}'),
(1, 'publisher', 'a:7:{s:9:"null|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"draft|null";a:2:{s:5:"label";s:22:"bwf_status_label_draft";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:14:"submitted|null";a:2:{s:5:"label";s:26:"bwf_status_label_submitted";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:9:"open|null";a:2:{s:5:"label";s:21:"bwf_status_label_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:6:"closed";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_archive";i:4;b:0;}i:1;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:2;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"create";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}s:10:"open|draft";a:2:{s:5:"label";s:27:"bwf_status_label_draft_live";s:7:"buttons";a:3:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfDraft";i:2;s:7:"replace";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}i:2;a:5:{i:0;N;i:1;s:10:"epBwfDraft";i:2;s:6:"delete";i:3;s:21:"bwf_btn_discard_draft";i:4;b:0;}}}s:14:"open|submitted";a:2:{s:5:"label";s:31:"bwf_status_label_submitted_live";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfDraft";i:2;s:7:"replace";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfDraft";i:2;s:6:"update";i:3;s:23:"bwf_btn_revert_to_draft";i:4;b:0;}}}s:11:"closed|null";a:2:{s:5:"label";s:23:"bwf_status_label_closed";s:7:"buttons";a:2:{i:0;a:5:{i:0;s:4:"open";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:15:"bwf_btn_publish";i:4;b:0;}i:1;a:5:{i:0;s:5:"draft";i:1;s:10:"epBwfEntry";i:2;s:6:"update";i:3;s:21:"bwf_btn_save_as_draft";i:4;b:0;}}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_ep_settings`
--

CREATE TABLE IF NOT EXISTS `exp_ep_settings` (
  `site_id` int(10) NOT NULL,
  `class` varchar(255) NOT NULL,
  `settings` text,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_ep_settings`
--

INSERT INTO `exp_ep_settings` (`site_id`, `class`, `settings`) VALUES
(1, 'Ep_better_workflow_ext', 'a:4:{s:8:"channels";a:6:{s:4:"id_7";a:3:{s:13:"uses_workflow";s:3:"yes";s:8:"template";s:10:"site/index";s:18:"notification_group";s:1:"1";}s:4:"id_2";a:3:{s:13:"uses_workflow";s:3:"yes";s:8:"template";s:10:"site/index";s:18:"notification_group";s:1:"1";}s:4:"id_8";a:3:{s:13:"uses_workflow";s:3:"yes";s:8:"template";s:9:"site/news";s:18:"notification_group";s:1:"1";}s:4:"id_9";a:3:{s:13:"uses_workflow";s:3:"yes";s:8:"template";s:10:"site/index";s:18:"notification_group";s:1:"1";}s:4:"id_6";a:3:{s:13:"uses_workflow";s:3:"yes";s:8:"template";s:19:"site/single_product";s:18:"notification_group";s:1:"1";}s:4:"id_3";a:3:{s:13:"uses_workflow";s:3:"yes";s:8:"template";s:21:"site/category_landing";s:18:"notification_group";s:1:"1";}}s:12:"bwf_channels";a:6:{i:0;s:1:"7";i:1;s:1:"2";i:2;s:1:"8";i:3;s:1:"9";i:4;s:1:"6";i:5;s:1:"3";}s:6:"groups";a:2:{s:4:"id_1";a:1:{s:4:"role";s:9:"Publisher";}s:4:"id_6";a:1:{s:4:"role";s:9:"Publisher";}}s:8:"advanced";a:8:{s:18:"redirect_on_action";s:9:"structure";s:30:"disable_preview_on_new_entries";s:2:"no";s:24:"display_full_preview_url";s:2:"no";s:15:"ignore_page_url";s:2:"no";s:16:"remove_index_php";s:2:"no";s:10:"log_events";s:3:"yes";s:11:"show_errors";s:2:"no";s:24:"enable_external_previews";s:2:"no";}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_extensions`
--

CREATE TABLE IF NOT EXISTS `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `exp_extensions`
--

INSERT INTO `exp_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`) VALUES
(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y'),
(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y'),
(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y'),
(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y'),
(14, 'Assets_ext', 'channel_entries_query_result', 'channel_entries_query_result', '', 10, '1.2.2', 'y'),
(15, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.5.1', 'y'),
(16, 'Ep_better_workflow_ext', 'on_sessions_start', 'sessions_start', 'a:0:{}', 8, '1.5', 'y'),
(17, 'Ep_better_workflow_ext', 'on_entry_submission_start', 'entry_submission_start', 'a:0:{}', 10, '1.5', 'y'),
(18, 'Ep_better_workflow_ext', 'on_entry_submission_ready', 'entry_submission_ready', 'a:0:{}', 10, '1.5', 'y'),
(19, 'Ep_better_workflow_ext', 'on_entry_submission_end', 'entry_submission_end', 'a:0:{}', 10, '1.5', 'y'),
(20, 'Ep_better_workflow_ext', 'on_publish_form_entry_data', 'publish_form_entry_data', 'a:0:{}', 10, '1.5', 'y'),
(21, 'Ep_better_workflow_ext', 'on_publish_form_channel_preferences', 'publish_form_channel_preferences', 'a:0:{}', 10, '1.5', 'y'),
(22, 'Ep_better_workflow_ext', 'on_channel_entries_row', 'channel_entries_row', 'a:0:{}', 10, '1.5', 'y'),
(23, 'Ep_better_workflow_ext', 'on_channel_entries_query_result', 'channel_entries_query_result', 'a:0:{}', 8, '1.5', 'y'),
(24, 'Ep_better_workflow_ext', 'on_cp_js_end', 'cp_js_end', 'a:0:{}', 10, '1.5', 'y'),
(25, 'Ep_better_workflow_ext', 'on_template_post_parse', 'template_post_parse', 'a:0:{}', 100, '1.5', 'y'),
(26, 'Ep_better_workflow_ext', 'on_playa_data_query', 'playa_data_query', 'a:0:{}', 10, '1.5', 'y'),
(27, 'Ep_better_workflow_ext', 'on_playa_fetch_rels_query', 'playa_fetch_rels_query', 'a:0:{}', 10, '1.5', 'y'),
(28, 'Ep_better_workflow_ext', 'on_zenbu_filter_by_status', 'zenbu_filter_by_status', 'a:0:{}', 100, '1.5', 'y'),
(29, 'Ep_better_workflow_ext', 'on_zenbu_modify_status_display', 'zenbu_modify_status_display', 'a:0:{}', 100, '1.5', 'y'),
(30, 'Ep_better_workflow_ext', 'on_zenbu_modify_title_display', 'zenbu_modify_title_display', 'a:0:{}', 100, '1.5', 'y'),
(40, 'Structure_ext', 'entry_submission_redirect', 'entry_submission_redirect', '', 10, '3.3.4', 'y'),
(41, 'Structure_ext', 'cp_member_login', 'cp_member_login', '', 10, '3.3.4', 'y'),
(42, 'Structure_ext', 'sessions_start', 'sessions_start', '', 10, '3.3.4', 'y'),
(43, 'Structure_ext', 'channel_module_create_pagination', 'channel_module_create_pagination', '', 9, '3.3.4', 'y'),
(44, 'Structure_ext', 'wygwam_config', 'wygwam_config', '', 10, '3.3.4', 'y'),
(45, 'Structure_ext', 'core_template_route', 'core_template_route', '', 10, '3.3.4', 'y'),
(46, 'Structure_ext', 'entry_submission_end', 'entry_submission_end', '', 10, '3.3.4', 'y'),
(47, 'Structure_ext', 'safecracker_submit_entry_end', 'safecracker_submit_entry_end', '', 10, '3.3.4', 'y'),
(48, 'Structure_ext', 'template_post_parse', 'template_post_parse', '', 10, '3.3.4', 'y'),
(49, 'Zoo_flexible_admin_ext', 'cp_css_end', 'cp_css_end', '', 1, '1.62', 'y'),
(50, 'Zoo_flexible_admin_ext', 'cp_js_end', 'cp_js_end', '', 1, '1.62', 'y'),
(51, 'Zoo_flexible_admin_ext', 'sessions_end', 'sessions_end', '', 1, '1.62', 'y'),
(52, 'Field_editor_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0.3', 'y'),
(53, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y'),
(54, 'Low_variables_ext', 'sessions_end', 'sessions_end', 'a:7:{s:11:"license_key";s:36:"58007bbf-79cf-45b0-b225-5b3d344aef5b";s:10:"can_manage";a:1:{i:0;s:1:"1";}s:16:"register_globals";s:1:"y";s:20:"register_member_data";s:1:"n";s:13:"save_as_files";s:1:"n";s:9:"file_path";s:0:"";s:13:"enabled_types";a:1:{i:0;s:12:"low_textarea";}}', 2, '2.3.2', 'y'),
(55, 'Low_variables_ext', 'template_fetch_template', 'template_fetch_template', 'a:7:{s:11:"license_key";s:36:"58007bbf-79cf-45b0-b225-5b3d344aef5b";s:10:"can_manage";a:1:{i:0;s:1:"1";}s:16:"register_globals";s:1:"y";s:20:"register_member_data";s:1:"n";s:13:"save_as_files";s:1:"n";s:9:"file_path";s:0:"";s:13:"enabled_types";a:1:{i:0;s:12:"low_textarea";}}', 2, '2.3.2', 'y'),
(56, 'Low_reorder_ext', 'entry_submission_end', 'entry_submission_end', 'a:0:{}', 5, '2.0.4', 'y'),
(57, 'Low_reorder_ext', 'channel_entries_query_result', 'channel_entries_query_result', 'a:0:{}', 5, '2.0.4', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `exp_fieldtypes`
--

INSERT INTO `exp_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`) VALUES
(1, 'select', '1.0', 'YTowOnt9', 'n'),
(2, 'text', '1.0', 'YTowOnt9', 'n'),
(3, 'textarea', '1.0', 'YTowOnt9', 'n'),
(4, 'date', '1.0', 'YTowOnt9', 'n'),
(5, 'file', '1.0', 'YTowOnt9', 'n'),
(6, 'multi_select', '1.0', 'YTowOnt9', 'n'),
(7, 'checkboxes', '1.0', 'YTowOnt9', 'n'),
(8, 'radio', '1.0', 'YTowOnt9', 'n'),
(9, 'rel', '1.0', 'YTowOnt9', 'n'),
(10, 'rte', '1.0', 'YTowOnt9', 'n'),
(12, 'assets', '1.2.2', 'YTowOnt9', 'y'),
(14, 'matrix', '2.5.1', 'YTowOnt9', 'y'),
(16, 'structure', '3.3.4', 'YTowOnt9', 'n'),
(17, 'wygwam', '2.6.3', 'YTowOnt9', 'y'),
(18, 'pt_dropdown', '1.0.3', 'YTowOnt9', 'n'),
(19, 'pt_checkboxes', '1.0.3', 'YTowOnt9', 'n'),
(20, 'pt_multiselect', '1.0.3', 'YTowOnt9', 'n'),
(21, 'pt_radio_buttons', '1.0.3', 'YTowOnt9', 'n'),
(22, 'playa', '4.3.3', 'YTowOnt9', 'y'),
(23, 'low_variables', '2.3.2', 'YTowOnt9', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_formatting`
--

CREATE TABLE IF NOT EXISTS `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `exp_field_formatting`
--

INSERT INTO `exp_field_formatting` (`formatting_id`, `field_id`, `field_fmt`) VALUES
(4, 2, 'none'),
(5, 2, 'br'),
(6, 2, 'xhtml'),
(13, 5, 'none'),
(14, 5, 'br'),
(15, 5, 'xhtml'),
(16, 6, 'none'),
(17, 6, 'br'),
(18, 6, 'xhtml'),
(19, 7, 'none'),
(20, 7, 'br'),
(21, 7, 'xhtml'),
(22, 8, 'none'),
(23, 8, 'br'),
(24, 8, 'xhtml'),
(25, 9, 'none'),
(26, 9, 'br'),
(27, 9, 'xhtml'),
(28, 10, 'none'),
(29, 10, 'br'),
(30, 10, 'xhtml'),
(34, 12, 'none'),
(35, 12, 'br'),
(36, 12, 'xhtml'),
(37, 13, 'none'),
(38, 13, 'br'),
(39, 13, 'xhtml'),
(64, 22, 'none'),
(65, 22, 'br'),
(66, 22, 'xhtml');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_groups`
--

CREATE TABLE IF NOT EXISTS `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_field_groups`
--

INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Products'),
(2, 1, 'Homepage'),
(3, 1, 'News'),
(4, 1, 'Product Category'),
(6, 1, 'Generic');

-- --------------------------------------------------------

--
-- Table structure for table `exp_files`
--

CREATE TABLE IF NOT EXISTS `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `exp_files`
--

INSERT INTO `exp_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `credit`, `location`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`) VALUES
(1, 1, 'dummy-4.jpeg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/dummy-4.jpeg', 'image/jpeg', 'dummy-4.jpeg', 89409, NULL, NULL, NULL, 1, 1355241331, 1, 1355241331, '606 567'),
(2, 1, 'dummy-4.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-4.jpeg', 'image/jpeg', 'dummy-4.jpeg', 89409, NULL, NULL, NULL, 1, 1355244856, 1, 1355244856, '606 567'),
(3, 1, 'dummy-4_thum.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-4_thum.jpg', 'image/jpeg', 'dummy-4_thum.jpg', 15952, NULL, NULL, NULL, 1, 1355244968, 1, 1355244968, '189 195'),
(4, 1, 'dummy-4_thum_1.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-4_thum_1.jpg', 'image/jpeg', 'dummy-4_thum_1.jpg', 15449, NULL, NULL, NULL, 1, 1355245192, 1, 1355245192, '195 195'),
(5, 1, 'dummy-4_thum.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-4_thum.jpg', 'image/jpeg', 'dummy-4_thum.jpg', 15449, NULL, NULL, NULL, 1, 1355245217, 1, 1355245217, '195 195'),
(6, 1, 'dummy-4.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-4.jpg', 'image/jpeg', 'dummy-4.jpg', 59539, NULL, NULL, NULL, 1, 1355245494, 1, 1355245494, '436 420'),
(7, 1, 'dummy-3.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-3.jpg', 'image/jpeg', 'dummy-3.jpg', 20893, NULL, NULL, NULL, 1, 1355247349, 1, 1355247349, '250 460'),
(8, 1, 'dummy-5.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-5.jpeg', 'image/jpeg', 'dummy-5.jpeg', 30864, NULL, NULL, NULL, 1, 1355247361, 1, 1355247361, '227 280'),
(9, 1, 'dummy-4.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-4.jpg', 'image/jpeg', 'dummy-4.jpg', 58485, NULL, NULL, NULL, 1, 1355248155, 1, 1355248155, '420 420'),
(10, 1, 'dummy-6.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-6.jpeg', 'image/jpeg', 'dummy-6.jpeg', 14706, NULL, NULL, NULL, 1, 1355304849, 1, 1355304849, '224 280'),
(11, 1, 'dummy-7.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/dummy-7.jpeg', 'image/jpeg', 'dummy-7.jpeg', 16352, NULL, NULL, NULL, 1, 1355304935, 1, 1355304935, '227 280'),
(12, 1, 'nubu_mojito.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/nubu_mojito.jpg', 'image/jpeg', 'nubu_mojito.jpg', 69413, NULL, NULL, NULL, 1, 1355307136, 1, 1355307136, '420 420'),
(13, 1, 'nubu_maitai.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/nubu_maitai.jpg', 'image/jpeg', 'nubu_maitai.jpg', 85949, NULL, NULL, NULL, 1, 1355307215, 1, 1355307215, '420 420'),
(14, 1, 'nubu_cosmopolitan.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/nubu_cosmopolitan.jpg', 'image/jpeg', 'nubu_cosmopolitan.jpg', 71776, NULL, NULL, NULL, 1, 1355307301, 1, 1355307301, '420 420'),
(15, 1, 'CONSUMABLES.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/CONSUMABLES.jpg', 'image/jpeg', 'CONSUMABLES.jpg', 43160, NULL, NULL, NULL, 1, 1355325458, 1, 1355325458, '227 280'),
(16, 1, 'COSMETICS.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/COSMETICS.jpg', 'image/jpeg', 'COSMETICS.jpg', 25958, NULL, NULL, NULL, 1, 1355325498, 1, 1355325498, '227 280'),
(17, 1, 'ACCESSORIES.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/ACCESSORIES.jpg', 'image/jpeg', 'ACCESSORIES.jpg', 26451, NULL, NULL, NULL, 1, 1355325529, 1, 1355325529, '227 280'),
(18, 1, 'COSMETICS_HERO.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/COSMETICS_HERO.jpg', 'image/jpeg', 'COSMETICS_HERO.jpg', 42554, NULL, NULL, NULL, 1, 1355325694, 1, 1355325694, '290 420'),
(19, 1, 'ACCESSORIES_HERO.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/ACCESSORIES_HERO.jpg', 'image/jpeg', 'ACCESSORIES_HERO.jpg', 43627, NULL, NULL, NULL, 1, 1355325702, 1, 1355325702, '290 420'),
(20, 1, 'CONSUMABLES_HERO.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/CONSUMABLES_HERO.jpg', 'image/jpeg', 'CONSUMABLES_HERO.jpg', 71497, NULL, NULL, NULL, 1, 1355325702, 1, 1355325702, '290 420'),
(21, 1, 'hero-Jackpot.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/hero-Jackpot.jpg', 'image/jpeg', 'hero-Jackpot.jpg', 57445, NULL, NULL, NULL, 1, 1355326185, 1, 1355326185, '420 420'),
(22, 1, 'lightly-salted.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/lightly-salted.jpg', 'image/jpeg', 'lightly-salted.jpg', 51608, NULL, NULL, NULL, 1, 1355326382, 1, 1355326382, '420 420'),
(23, 1, 'sea-salt.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/sea-salt.jpg', 'image/jpeg', 'sea-salt.jpg', 49134, NULL, NULL, NULL, 1, 1355326532, 1, 1355326532, '420 420'),
(24, 1, 'chedar.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/chedar.jpg', 'image/jpeg', 'chedar.jpg', 54908, NULL, NULL, NULL, 1, 1355326643, 1, 1355326643, '420 420'),
(25, 1, 'sweet-chilli.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/sweet-chilli.jpg', 'image/jpeg', 'sweet-chilli.jpg', 52723, NULL, NULL, NULL, 1, 1355326771, 1, 1355326771, '420 420'),
(26, 1, 'roast-beef.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/roast-beef.jpg', 'image/jpeg', 'roast-beef.jpg', 45399, NULL, NULL, NULL, 1, 1355326870, 1, 1355326870, '420 420'),
(27, 1, 'Shimmer-Stack-hero.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Shimmer-Stack-hero.jpg', 'image/jpeg', 'Shimmer-Stack-hero.jpg', 35003, NULL, NULL, NULL, 1, 1355329067, 1, 1355329067, '420 420'),
(28, 1, 'Bronze-eyes.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Bronze-eyes.jpg', 'image/jpeg', 'Bronze-eyes.jpg', 21703, NULL, NULL, NULL, 1, 1355329397, 1, 1355329397, '420 420'),
(29, 1, 'Smokey-eyes.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Smokey-eyes.jpg', 'image/jpeg', 'Smokey-eyes.jpg', 20958, NULL, NULL, NULL, 1, 1355329566, 1, 1355329566, '420 420'),
(30, 1, 'Little-angels.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Little-angels.jpg', 'image/jpeg', 'Little-angels.jpg', 21711, NULL, NULL, NULL, 1, 1355329637, 1, 1355329637, '420 420'),
(31, 1, 'Nail-Collection.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Nail-Collection.jpg', 'image/jpeg', 'Nail-Collection.jpg', 19395, NULL, NULL, NULL, 1, 1355329687, 1, 1355329687, '420 420'),
(32, 1, 'Shimmer-Stack.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Shimmer-Stack.jpg', 'image/jpeg', 'Shimmer-Stack.jpg', 18931, NULL, NULL, NULL, 1, 1355329709, 1, 1355329709, '420 420'),
(33, 1, 'Black.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Black.jpg', 'image/jpeg', 'Black.jpg', 22072, NULL, NULL, NULL, 1, 1355329972, 1, 1355329972, '420 420'),
(34, 1, 'Black.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Black.jpg', 'image/jpeg', 'Black.jpg', 22072, NULL, NULL, NULL, 1, 1355330045, 1, 1355330045, '420 420'),
(35, 1, 'Bronze.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Bronze.jpg', 'image/jpeg', 'Bronze.jpg', 19208, NULL, NULL, NULL, 1, 1355330060, 1, 1355330060, '420 420'),
(36, 1, 'Gold.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Gold.jpg', 'image/jpeg', 'Gold.jpg', 21875, NULL, NULL, NULL, 1, 1355330082, 1, 1355330082, '420 420'),
(37, 1, 'Leopard.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Leopard.jpg', 'image/jpeg', 'Leopard.jpg', 34500, NULL, NULL, NULL, 1, 1355330100, 1, 1355330100, '420 420'),
(38, 1, 'Hero-Tipsy-Feet.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Hero-Tipsy-Feet.jpg', 'image/jpeg', 'Hero-Tipsy-Feet.jpg', 38577, NULL, NULL, NULL, 1, 1355330317, 1, 1355330317, '420 420'),
(39, 1, 'Hero-Ice-Diamonds.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Hero-Ice-Diamonds.jpg', 'image/jpeg', 'Hero-Ice-Diamonds.jpg', 41284, NULL, NULL, NULL, 1, 1355330512, 1, 1355330512, '420 420'),
(40, 1, 'Black-mimi.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Black-mimi.jpg', 'image/jpeg', 'Black-mimi.jpg', 13043, NULL, NULL, NULL, 1, 1355330588, 1, 1355330588, '420 420'),
(41, 1, 'Blue-mini.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Blue-mini.jpg', 'image/jpeg', 'Blue-mini.jpg', 18079, NULL, NULL, NULL, 1, 1355330641, 1, 1355330641, '420 420'),
(42, 1, 'Pink-mini.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Pink-mini.jpg', 'image/jpeg', 'Pink-mini.jpg', 15216, NULL, NULL, NULL, 1, 1355330657, 1, 1355330657, '420 420'),
(43, 1, 'Purple-mini.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/Purple-mini.jpg', 'image/jpeg', 'Purple-mini.jpg', 17107, NULL, NULL, NULL, 1, 1355330684, 1, 1355330684, '420 420'),
(44, 1, 'White-mini.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/White-mini.jpg', 'image/jpeg', 'White-mini.jpg', 12651, NULL, NULL, NULL, 1, 1355330732, 1, 1355330732, '420 420'),
(45, 1, 'Hero-Nuba.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Hero-Nuba.jpg', 'image/jpeg', 'Hero-Nuba.jpg', 150925, NULL, NULL, NULL, 1, 1355330976, 1, 1355330976, '420 420'),
(46, 1, 'mojito.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/mojito.jpg', 'image/jpeg', 'mojito.jpg', 115505, NULL, NULL, NULL, 1, 1355331012, 1, 1355331012, '420 420'),
(47, 1, 'mai-tai.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/mai-tai.jpg', 'image/jpeg', 'mai-tai.jpg', 107149, NULL, NULL, NULL, 1, 1355331039, 1, 1355331039, '420 420'),
(48, 1, 'cosmo.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/cosmo.jpg', 'image/jpeg', 'cosmo.jpg', 111423, NULL, NULL, NULL, 1, 1355331056, 1, 1355331056, '420 420'),
(49, 1, 'cosmo-420-x-420.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/cosmo-420-x-420.jpg', 'image/jpeg', 'cosmo-420-x-420.jpg', 42434, NULL, NULL, NULL, 1, 1355331678, 1, 1355331678, '420 420'),
(50, 1, 'mojito-420-x-420.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/mojito-420-x-420.jpg', 'image/jpeg', 'mojito-420-x-420.jpg', 45774, NULL, NULL, NULL, 1, 1355331691, 1, 1355331691, '420 420'),
(51, 1, 'on-the-beach420-x-420.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/on-the-beach420-x-420.jpg', 'image/jpeg', 'on-the-beach420-x-420.jpg', 49078, NULL, NULL, NULL, 1, 1355331713, 1, 1355331713, '420 420'),
(52, 1, 'pina-420-x-420.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/pina-420-x-420.jpg', 'image/jpeg', 'pina-420-x-420.jpg', 45168, NULL, NULL, NULL, 1, 1355331757, 1, 1355331757, '420 420'),
(53, 1, 'Glamourous-bra-strap-420x-420.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Glamourous-bra-strap-420x-420.jpg', 'image/jpeg', 'Glamourous-bra-strap-420x-420.jpg', 70136, NULL, NULL, NULL, 1, 1355332498, 1, 1355332498, '420 420'),
(54, 1, 'bra-strap-black-stone-SG-row.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/bra-strap-black-stone-SG-row.jpg', 'image/jpeg', 'bra-strap-black-stone-SG-row.jpg', 23075, NULL, NULL, NULL, 1, 1355332524, 1, 1355332524, '420 420'),
(55, 1, 'bra-strap-multi-best.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/bra-strap-multi-best.jpg', 'image/jpeg', 'bra-strap-multi-best.jpg', 27442, NULL, NULL, NULL, 1, 1355332562, 1, 1355332562, '420 420'),
(56, 1, 'bra-strap-pearl.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/bra-strap-pearl.jpg', 'image/jpeg', 'bra-strap-pearl.jpg', 26818, NULL, NULL, NULL, 1, 1355332662, 1, 1355332662, '420 420'),
(57, 1, 'bra-strap-silver-clear-stone.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/bra-strap-silver-clear-stone.jpg', 'image/jpeg', 'bra-strap-silver-clear-stone.jpg', 17326, NULL, NULL, NULL, 1, 1355332681, 1, 1355332681, '420 420'),
(58, 1, 'choc-brownie.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/choc-brownie.jpg', 'image/jpeg', 'choc-brownie.jpg', 57682, NULL, NULL, NULL, 1, 1355333214, 1, 1355333214, '420 420'),
(59, 1, 'choc-brownie.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/choc-brownie.jpg', 'image/jpeg', 'choc-brownie.jpg', 57682, NULL, NULL, NULL, 1, 1355333247, 1, 1355333247, '420 420'),
(60, 1, 'Authentic-hero.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Authentic-hero.jpg', 'image/jpeg', 'Authentic-hero.jpg', 111366, NULL, NULL, NULL, 1, 1355398222, 1, 1355398222, '420 420'),
(61, 1, 'bra-strap-black-stone-with-bars.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/bra-strap-black-stone-with-bars.jpg', 'image/jpeg', 'bra-strap-black-stone-with-bars.jpg', 18707, NULL, NULL, NULL, 1, 1355499740, 1, 1355499740, '420 420'),
(62, 1, 'silver-strap-x-2.jpg', 1, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/silver-strap-x-2.jpg', 'image/jpeg', 'silver-strap-x-2.jpg', 22862, NULL, NULL, NULL, 1, 1355502787, 1, 1355502787, '420 420'),
(63, 1, 'Hewbys.jpg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Hewbys.jpg', 'image/jpeg', 'Hewbys.jpg', 395372, NULL, NULL, NULL, 2, 1356090971, 2, 1356090971, '916 916'),
(64, 1, 'ispyEye.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/ispyEye.jpeg', 'image/jpeg', 'ispyEye.jpeg', 93794, NULL, NULL, NULL, 1, 1357552163, 1, 1357552163, '606 606'),
(65, 1, 'nuba1-567x606.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/nuba1-567x606.jpeg', 'image/jpeg', 'nuba1-567x606.jpeg', 89409, NULL, NULL, NULL, 1, 1357552732, 1, 1357552732, '606 567'),
(66, 1, 'Bra-Straps-Large1.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Bra-Straps-Large1.jpeg', 'image/jpeg', 'Bra-Straps-Large1.jpeg', 35371, NULL, NULL, NULL, 1, 1357552884, 1, 1357552884, '400 427'),
(67, 1, 'iceDiamond.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/iceDiamond.jpeg', 'image/jpeg', 'iceDiamond.jpeg', 53658, NULL, NULL, NULL, 1, 1357553374, 1, 1357553374, '606 606'),
(68, 1, 'Janice.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/Janice.jpeg', 'image/jpeg', 'Janice.jpeg', 8398, NULL, NULL, NULL, 1, 1357553399, 1, 1357553399, '200 200'),
(69, 1, 'janice-smith.jpeg', 3, '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/janice-smith.jpeg', 'image/jpeg', 'janice-smith.jpeg', 119711, NULL, NULL, NULL, 1, 1357557909, 1, 1357557909, '449 420'),
(70, 1, 'Tipsy-Feet-Ad.jpg', 1, '/var/www/vhosts/stmbrands.co.uk/httpdocs/images/stmbrands/products/Tipsy-Feet-Ad.jpg', 'image/jpeg', 'Tipsy-Feet-Ad.jpg', 50883, NULL, NULL, NULL, 1, 1360159812, 1, 1360159812, '420 420'),
(71, 1, 'Tipsy-feet.jpg', 1, '/var/www/vhosts/stmbrands.co.uk/httpdocs/images/stmbrands/products/Tipsy-feet.jpg', 'image/jpeg', 'Tipsy-feet.jpg', 61225, NULL, NULL, NULL, 1, 1360159824, 1, 1360159824, '420 420');

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_categories`
--

CREATE TABLE IF NOT EXISTS `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_dimensions`
--

CREATE TABLE IF NOT EXISTS `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_watermarks`
--

CREATE TABLE IF NOT EXISTS `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pref_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `server_path` varchar(150) NOT NULL DEFAULT '',
  `filename` varchar(150) NOT NULL DEFAULT '',
  `extension` varchar(7) NOT NULL DEFAULT '',
  `filesize` int(10) NOT NULL DEFAULT '1',
  `emailed` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`attachment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `pref_id` (`pref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_entries`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_entries` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `weblog_id` int(4) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `form_name` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(150) NOT NULL DEFAULT '',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `status` char(10) NOT NULL DEFAULT 'open',
  `forename` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(150) NOT NULL DEFAULT '',
  `surname` varchar(150) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`entry_id`),
  KEY `author_id` (`author_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `exp_freeform_entries`
--

INSERT INTO `exp_freeform_entries` (`entry_id`, `group_id`, `weblog_id`, `author_id`, `ip_address`, `form_name`, `template`, `entry_date`, `edit_date`, `status`, `forename`, `email`, `surname`, `message`) VALUES
(1, 1, 0, 1, '127.0.0.1', 'Contact Form', '', 1355323827, 1355323827, 'open', 'Jonny', 'jonny@hicontrast.co.uk', 'Frodsham', 'kpkop kop p'),
(2, 3, 0, 0, '127.0.0.1', 'Contact Form', 'contact_us', 1355324136, 1355324136, 'open', 'Jonny', 'jonny@hicontrast.co.uk', 'Frodsham', 'ioijiojio'),
(3, 1, 0, 1, '109.111.197.225', 'Contact Form', 'contact_us', 1355399140, 1355399140, 'open', 'Martin', 'martin@shoot-the-moon.co.uk', 'Smith', 'This is just a test from the STM Brands website'),
(4, 1, 0, 1, '109.111.197.225', 'Newsletter', '', 1355417067, 1355417067, 'open', '', 'martin@shoot-the-moon.co.uk', '', ''),
(5, 1, 0, 1, '109.111.197.225', 'Newsletter', '', 1355417114, 1355417114, 'open', '', 'kathryn@shoot-the-moon.co.uk', '', ''),
(6, 1, 0, 1, '109.111.197.225', 'Newsletter', '', 1357294384, 1357294384, 'open', '', 'jamie@shoot-the-moon.co.uk', '', ''),
(7, 3, 0, 0, '109.111.197.225', 'Newsletter', '', 1357320792, 1357320792, 'open', '', 'kathryn@shoot-the-moon.co.uk', '', ''),
(8, 1, 0, 1, '109.111.197.225', 'Contact Form', 'contact_us', 1357552351, 1357552351, 'open', 'test', 'kathryn@shoot-the-moon.co.uk', 'test', 'test'),
(9, 1, 0, 1, '109.111.197.225', 'Contact Form', 'contact_us', 1357558184, 1357558184, 'open', 'sdfsdf', 'sdfsdf@fhdfdshj.com', 'sdfsdf', 'sdfsdfsfsfd'),
(10, 3, 0, 0, '86.133.224.47', 'Contact Form', 'contact_us', 1358067749, 1358067749, 'open', 'Phil', 'shootthemoon@me.com', 'Marshall', 'test 2 - can you confirm this one too (enquiry form)\n\nThanks\n\nPhil'),
(11, 3, 0, 0, '2.102.13.90', 'Contact Form', 'contact_us', 1358083780, 1358083780, 'open', 'Martin', 'martin@shoot-the-moon.co.uk', 'Smith', 'This is a test'),
(12, 3, 0, 0, '109.111.197.225', 'Contact Form', 'contact_us', 1358170212, 1358170212, 'open', 'oliver', 'oliver@shoot-the-moon.co.uk', 'zebedee-howard', 'test'),
(13, 3, 0, 0, '178.32.218.212', 'Contact Form', 'contact_us', 1361962234, 1361962234, 'open', 'iekeair', 'kpzwrj@ffzira.com', 'iekeair', 'vfOWgl  <a href="http://opphueymuoca.com/">opphueymuoca</a>, [url=http://ytfjnidabtds.com/]ytfjnidabtds[/url], [link=http://fggtitvqbjgo.com/]fggtitvqbjgo[/link], http://qaktykyherrq.com/'),
(14, 3, 0, 0, '188.143.232.211', 'Contact Form', 'contact_us', 1363076639, 1363076639, 'open', 'cucqarws', 'jkqipw@sebgbf.com', 'cucqarws', 'SP1WOr  <a href="http://bzdbozvamfgm.com/">bzdbozvamfgm</a>, [url=http://pesoqtmvfuut.com/]pesoqtmvfuut[/url], [link=http://cidulmimmsfd.com/]cidulmimmsfd[/link], http://xtlenmvzhwmi.com/'),
(15, 3, 0, 0, '80.235.138.122', 'Contact Form', 'contact_us', 1363689434, 1363689434, 'open', 'Julia ', 'juliat57@hotmail.co.uk', 'Thomas', 'Is it possible to buy a chain that is 12.5 inches long. I need two chains to fit to a bra where the chains are currently broken. These particular chains fit across the back and not over the shoulders !   I look forward to hearing from you . \n\nThank you \n\nJulia '),
(16, 3, 0, 0, '188.143.232.211', 'Contact Form', 'contact_us', 1364110340, 1364110340, 'open', 'xbdpyf', 'tnfwlo@pvnyys.com', 'xbdpyf', 'wnEAI9  <a href="http://yjjuojwvzwug.com/">yjjuojwvzwug</a>, [url=http://zbohexgsbehk.com/]zbohexgsbehk[/url], [link=http://uxybzzxywkak.com/]uxybzzxywkak[/link], http://wqazxfsrkmfy.com/'),
(17, 3, 0, 0, '37.221.174.111', 'Contact Form', 'contact_us', 1364806041, 1364806041, 'open', 'uunjegdz', 'inmqqv@xfgvzd.com', 'uunjegdz', 'scCcZQ  <a href="http://bhaarupxlhjp.com/">bhaarupxlhjp</a>, [url=http://rhnwloobbojs.com/]rhnwloobbojs[/url], [link=http://jgfmaooykdpl.com/]jgfmaooykdpl[/link], http://bthbfnhcmeeh.com/'),
(18, 3, 0, 0, '37.221.174.111', 'Contact Form', 'contact_us', 1364831126, 1364831126, 'open', 'mqkybja', 'klwtbo@ahoqxi.com', 'mqkybja', 'LkNver  <a href="http://vcfunxuwuwij.com/">vcfunxuwuwij</a>, [url=http://kqxcdyzuzsee.com/]kqxcdyzuzsee[/url], [link=http://iqwnfcixrsdh.com/]iqwnfcixrsdh[/link], http://anwpdjzowjdl.com/'),
(19, 3, 0, 0, '91.232.96.20', 'Contact Form', 'contact_us', 1364978646, 1364978646, 'open', 'kkkenk', 'ttvuyk@iytoew.com', 'kkkenk', 'SC9DLf  <a href="http://qhzwqyyrtlxa.com/">qhzwqyyrtlxa</a>, [url=http://kbljbbfgognh.com/]kbljbbfgognh[/url], [link=http://ifvrlhpwplxj.com/]ifvrlhpwplxj[/link], http://tekajzjlimbz.com/'),
(20, 3, 0, 0, '188.143.232.211', 'Contact Form', 'contact_us', 1365344872, 1365344872, 'open', 'gfoojjia', 'wvctlv@adzftq.com', 'gfoojjia', 'QHO55B  <a href="http://vhsgaiacpvpa.com/">vhsgaiacpvpa</a>, [url=http://aznrgyuebqru.com/]aznrgyuebqru[/url], [link=http://rpuqlisjagoh.com/]rpuqlisjagoh[/link], http://jpwumwifaaip.com/'),
(21, 3, 0, 0, '188.143.234.127', 'Contact Form', 'contact_us', 1365376710, 1365376710, 'open', 'horny', 'dondy228@hotmail.com', 'horny', '8ChN3v http://www.ggiodpc.com/www.6shpFpANPwYnffbs9P5rsRN67oJWDZuQ.com.php'),
(22, 3, 0, 0, '91.232.96.20', 'Contact Form', 'contact_us', 1365536108, 1365536108, 'open', 'yibptmb', 'jhrese@wsmruv.com', 'yibptmb', 'jCg96L  <a href="http://jpfadyjraymt.com/">jpfadyjraymt</a>, [url=http://slucsloexktn.com/]slucsloexktn[/url], [link=http://wchsndzupcbr.com/]wchsndzupcbr[/link], http://wyzrubdzwqmy.com/'),
(23, 3, 0, 0, '91.232.96.15', 'Contact Form', 'contact_us', 1366070624, 1366070624, 'open', 'cagtzbuacb', 'nkijzq@ttgaqq.com', 'cagtzbuacb', 'a80QQ0  <a href="http://pjfmjtffvrbz.com/">pjfmjtffvrbz</a>, [url=http://jtgjimevktrm.com/]jtgjimevktrm[/url], [link=http://nqjruqrdrvsh.com/]nqjruqrdrvsh[/link], http://ejmfxsmjbttw.com/'),
(24, 3, 0, 0, '37.221.174.111', 'Contact Form', 'contact_us', 1366436726, 1366436726, 'open', 'gvoeoiz', 'iaqfdj@tkoydj.com', 'gvoeoiz', 'uoZPym  <a href="http://vllreusdfxqj.com/">vllreusdfxqj</a>, [url=http://iljyfxdvafje.com/]iljyfxdvafje[/url], [link=http://tjjzotzyqict.com/]tjjzotzyqict[/link], http://rthsdovaadkx.com/'),
(25, 3, 0, 0, '37.221.174.111', 'Contact Form', 'contact_us', 1366494393, 1366494393, 'open', 'taqjipmwvmh', 'nqdnkq@wqmump.com', 'taqjipmwvmh', 'YQstVU  <a href="http://zdltodzcrzvl.com/">zdltodzcrzvl</a>, [url=http://yupicvgkkntx.com/]yupicvgkkntx[/url], [link=http://mmmzupeuelsl.com/]mmmzupeuelsl[/link], http://xbgjgndircrx.com/'),
(26, 3, 0, 0, '91.232.96.7', 'Contact Form', 'contact_us', 1366859788, 1366859788, 'open', 'qjdsqxxnp', 'soiqvs@krveyx.com', 'qjdsqxxnp', 'qdSfdb  <a href="http://oosffbfmtngt.com/">oosffbfmtngt</a>, [url=http://kucozytslgsw.com/]kucozytslgsw[/url], [link=http://tpglphtyrkuf.com/]tpglphtyrkuf[/link], http://robbfhzbxtdi.com/'),
(27, 3, 0, 0, '91.232.96.20', 'Contact Form', 'contact_us', 1367219340, 1367219340, 'open', 'jbkunhwtd', 'nhfgsb@kohaio.com', 'jbkunhwtd', 'in4nuV  <a href="http://ivlmfjagkatu.com/">ivlmfjagkatu</a>, [url=http://eitcajopblws.com/]eitcajopblws[/url], [link=http://wqvupdpcscvx.com/]wqvupdpcscvx[/link], http://tbpnzthdlhms.com/'),
(28, 3, 0, 0, '188.143.234.127', 'Contact Form', 'contact_us', 1367268249, 1367268249, 'open', 'john', 'sbdh47tf@hotmail.com', 'john', 'KbM6xz http://www.6shpFpANPwYnffbs9P5rsRN67oJWDZuQ.com'),
(29, 3, 0, 0, '91.121.170.197', 'Contact Form', 'contact_us', 1367703007, 1367703007, 'open', 'Mike', 'normy273@hotmail.com', 'Mike', 'm98Ntk http://www.6shpFpANPwYnffbs9P5rsRN67oJWDZuQ.com'),
(30, 3, 0, 0, '80.93.217.46', 'Contact Form', 'contact_us', 1367725709, 1367725709, 'open', 'zkosvbtbiz', 'mdfoau@dkslhk.com', 'zkosvbtbiz', 'viPvGQ  <a href="http://qextnhfaudfc.com/">qextnhfaudfc</a>, [url=http://rsurwdgkamru.com/]rsurwdgkamru[/url], [link=http://zijmbrnfximd.com/]zijmbrnfximd[/link], http://kqstfxdhoock.com/'),
(31, 3, 0, 0, '188.143.234.127', 'Contact Form', 'contact_us', 1367772522, 1367772522, 'open', 'ronny', 'dondy228@hotmail.com', 'ronny', 'ZpHG05 http://www.78NLRvzfIwzacsSvHH4hdZWSkQdHROAk.com'),
(32, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1367968541, 1367968541, 'open', 'Christian', 'heyjew@msn.com', 'Christian', 'This is the job description <a href=" http://buycialisrc.com/ ">buy cialis online</a>  clearly defined and distinguished in the syllabus]\n '),
(33, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1368357703, 1368357703, 'open', 'Kaylee', 'freelove@msn.com', 'Kaylee', 'Can I call you back? <a href=" http://www.newenergyresearch.net ">cheap zithromax</a>  professional professional development. professional professional\n '),
(34, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1369416142, 1369416142, 'open', 'Alexa', 'fifa55@yahoo.com', 'Alexa', 'I live here <a href=" ï»¿http://www.newenergyresearch.net ">order zithromax overnight</a>  Treat others kindly and with compassion, whether in the classroom setting, in meetings, or on rotation.\n '),
(35, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1369987100, 1369987100, 'open', 'Arianna', 'freelife@yahoo.com', 'Arianna', 'I''d like to open an account <a href=" http://www.info-kod.com ">oral cytotec</a>  dosage regimen, potential side effects, missed dose, instructions, refills\n '),
(36, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1370713283, 1370713283, 'open', 'lifestile', 'john@hotmail.com', 'lifestile', 'Would you like a receipt? <a href=" ï»¿http://dominosnm.com ">200 mg of clomid</a>  4. Patients Family- SHiumsmtoarrize y relevant or contributory social factors.\n '),
(37, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1371135286, 1371135286, 'open', 'Adam', 'nogood87@yahoo.com', 'Adam', 'An accountancy practice <a href=" http://www.colorsofbangladesh.com ">topamax xr</a>  claim is greater than the difference\n '),
(38, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1371642370, 1371642370, 'open', 'Luke', 'pitfighter@hotmail.com', 'Luke', 'I''m on holiday <a href=" ï»¿http://www.credit911online.com ">metronidazole (flagyl) cost</a>  vii. Prescription items that are not subsidised\n '),
(39, 3, 0, 0, '188.143.234.127', 'Contact Form', 'contact_us', 1371676742, 1371676742, 'open', 'john', 'barny182@hotmail.com', 'john', 'RkB1z1 http://www.c1dOvW6eef5JOp8ApWjKQy5RO5mLafkc.com'),
(40, 3, 0, 0, '188.143.234.127', 'Contact Form', 'contact_us', 1371869670, 1371869670, 'open', 'matt', 'barny182@hotmail.com', 'matt', 'r97P5l http://www.c1dOvW6eef5JOp8ApWjKQy5RO5mLafkc.com'),
(41, 3, 0, 0, '93.115.83.252', 'Contact Form', 'contact_us', 1371972731, 1371972731, 'open', 'Mike', 'normy273@hotmail.com', 'Mike', 'qOYNCt http://www.c1dOvW6eef5JOp8ApWjKQy5RO5mLafkc.com'),
(42, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1372037043, 1372037043, 'open', 'Michelle', 'thebest@hotmail.com', 'Michelle', 'I read a lot <a href=" http://www.jcgb.org ">does effexor xr make you gain weight</a>  preceptor(s) or seminar group. · Participate in therapeutic\n '),
(43, 3, 0, 0, '188.143.234.127', 'Contact Form', 'contact_us', 1372210782, 1372210782, 'open', 'horny', 'barny182@hotmail.com', 'horny', 'I6u3AE http://www.c1dOvW6eef5JOp8ApWjKQy5RO5mLafkc.com'),
(44, 3, 0, 0, '188.143.232.211', 'Contact Form', 'contact_us', 1372543533, 1372543533, 'open', 'alkyyrdcr', 'wgtjox@fogwgw.com', 'alkyyrdcr', 'b3VvmT  <a href="http://pxhaxhhkowlv.com/">pxhaxhhkowlv</a>, [url=http://cfaolyqtkfjt.com/]cfaolyqtkfjt[/url], [link=http://zqkolwlnqgmo.com/]zqkolwlnqgmo[/link], http://ntczadigczxa.com/'),
(45, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1372560236, 1372560236, 'open', 'Alexandra', 'quaker@yahoo.com', 'Alexandra', 'Looking for a job <a href=" ï»¿http://ubikhead.com ">fluconazole 150mg capsules</a>  all editing was passed and the\n '),
(46, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1373261554, 1373261554, 'open', 'Amia', 'flyman@gmail.com', 'Amia', 'I can''t get a signal <a href=" http://www.peepshowstories.com ">orlistat generico precio chile</a>  E. Optional Site Unable to questioning to complete complex tasks. Independently Independently\n '),
(47, 3, 0, 0, '188.143.232.31', 'Contact Form', 'contact_us', 1373878725, 1373878725, 'open', 'Brooke', 'fifa55@yahoo.com', 'Brooke', 'Do you have any exams coming up? <a href=" http://waterrow.org ">600 mg gabapentin generic neurontin</a>  52 Non-Matched Cardholder ID 065\n ');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fields`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_order` int(10) NOT NULL DEFAULT '0',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_length` int(3) NOT NULL DEFAULT '150',
  `form_name` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `name_old` varchar(50) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `weblog_id` int(4) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `editable` char(1) NOT NULL DEFAULT 'y',
  `status` char(10) NOT NULL DEFAULT 'open',
  PRIMARY KEY (`field_id`),
  KEY `name` (`name`),
  KEY `author_id` (`author_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `exp_freeform_fields`
--

INSERT INTO `exp_freeform_fields` (`field_id`, `field_order`, `field_type`, `field_length`, `form_name`, `name`, `name_old`, `label`, `weblog_id`, `author_id`, `entry_date`, `edit_date`, `editable`, `status`) VALUES
(1, 1, 'text', 150, '', 'forename', '', 'First Name', 0, 0, 0, 0, 'n', 'open'),
(2, 3, 'text', 150, '', 'email', '', 'Email', 0, 0, 0, 0, 'n', 'open'),
(15, 4, 'textarea', 1000, '', 'message', '', 'Message', 0, 0, 0, 0, 'y', 'open'),
(14, 2, 'text', 150, '', 'surname', '', 'Surname', 0, 0, 0, 0, 'y', 'open');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_params`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`params_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19604 ;

--
-- Dumping data for table `exp_freeform_params`
--

INSERT INTO `exp_freeform_params` (`params_id`, `entry_date`, `data`) VALUES
(19603, 1374071937, 'a:25:{s:15:"require_captcha";s:2:"no";s:9:"form_name";s:12:"Contact Form";s:10:"require_ip";s:0:"";s:11:"ee_required";s:30:"forename|surname|email|message";s:9:"ee_notify";s:0:"";s:18:"allowed_file_types";s:0:"";s:8:"reply_to";b:0;s:20:"reply_to_email_field";s:0:"";s:19:"reply_to_name_field";s:0:"";s:11:"output_json";s:1:"n";s:12:"ajax_request";s:1:"y";s:10:"recipients";s:1:"y";s:15:"recipient_limit";s:1:"1";s:17:"static_recipients";b:0;s:22:"static_recipients_list";a:0:{}s:18:"recipient_template";s:10:"contact_us";s:13:"discard_field";s:0:"";s:15:"send_attachment";s:0:"";s:15:"send_user_email";s:0:"";s:20:"send_user_attachment";s:0:"";s:18:"attachment_profile";s:0:"";s:19:"user_email_template";s:16:"default_template";s:8:"template";s:16:"default_template";s:20:"prevent_duplicate_on";s:0:"";s:11:"file_upload";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_preferences`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) NOT NULL DEFAULT '',
  `preference_value` text NOT NULL,
  PRIMARY KEY (`preference_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `html` char(1) NOT NULL DEFAULT 'n',
  `template_name` varchar(150) NOT NULL DEFAULT '',
  `template_label` varchar(150) NOT NULL DEFAULT '',
  `data_from_name` varchar(150) NOT NULL DEFAULT '',
  `data_from_email` varchar(200) NOT NULL DEFAULT '',
  `data_title` varchar(80) NOT NULL DEFAULT '',
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_freeform_templates`
--

INSERT INTO `exp_freeform_templates` (`template_id`, `enable_template`, `wordwrap`, `html`, `template_name`, `template_label`, `data_from_name`, `data_from_email`, `data_title`, `template_data`) VALUES
(1, 'y', 'y', 'n', 'default_template', 'Default Template', '', '', 'Someone has posted to Freeform', 'Someone has posted to Freeform. Here are the details:  \n			 		\nEntry Date: {entry_date}\n{all_custom_fields}'),
(2, 'y', 'n', 'y', 'contact_us', 'Contact Form', 'Enquiry from the STM Brands website', 'contact@stmbrands.co.uk', 'STM Brands Contact Form Submission', '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">\n<html>\n	<head>\n		<title></title>\n	</head>\n	<body>\n<h2>Personal Details:</h2>\n<p><b>Name:</b> {forename} {surname}</p>\n<p><b>Email:</b> {email}</p>\n<p><b>Contact Number:</b> {contact_number}</p>\n<h2>Message:</h2>\n<p>{message}</p>\n	</body>\n</html>');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_user_email`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `exp_freeform_user_email`
--

INSERT INTO `exp_freeform_user_email` (`email_id`, `entry_id`, `email_count`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 8, 1),
(5, 9, 1),
(6, 10, 1),
(7, 11, 1),
(8, 12, 1),
(9, 13, 1),
(10, 14, 1),
(11, 15, 1),
(12, 16, 1),
(13, 17, 1),
(14, 18, 1),
(15, 19, 1),
(16, 20, 1),
(17, 21, 1),
(18, 22, 1),
(19, 23, 1),
(20, 24, 1),
(21, 25, 1),
(22, 26, 1),
(23, 27, 1),
(24, 28, 1),
(25, 29, 1),
(26, 30, 1),
(27, 31, 1),
(28, 32, 1),
(29, 33, 1),
(30, 34, 1),
(31, 35, 1),
(32, 36, 1),
(33, 37, 1),
(34, 38, 1),
(35, 39, 1),
(36, 40, 1),
(37, 41, 1),
(38, 42, 1),
(39, 43, 1),
(40, 44, 1),
(41, 45, 1),
(42, 46, 1),
(43, 47, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_global_variables`
--

CREATE TABLE IF NOT EXISTS `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_global_variables`
--

INSERT INTO `exp_global_variables` (`variable_id`, `site_id`, `variable_name`, `variable_data`) VALUES
(1, 1, 'test', '{exp:channel:entries channel="product-brand" dynamic="no" orderby="random" limit="4"}\ntest\n	<li><a href="{page_uri}">{brand}{exp:ce_img:single src="{brand_image}" width="80" height="80"}{/brand}</li>\n	{/exp:channel:entries}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_html_buttons`
--

CREATE TABLE IF NOT EXISTS `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_html_buttons`
--

INSERT INTO `exp_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`) VALUES
(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(4, 1, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(5, 1, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img');

-- --------------------------------------------------------

--
-- Table structure for table `exp_layout_publish`
--

CREATE TABLE IF NOT EXISTS `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_layout_publish`
--

INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(1, 1, 1, 2, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(2, 1, 6, 2, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:8;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(3, 1, 1, 3, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(4, 1, 6, 3, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:5;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:6;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(5, 1, 1, 6, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(6, 1, 6, 6, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:7;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:2;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(7, 1, 1, 8, 'a:6:{s:7:"publish";a:6:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(8, 1, 6, 8, 'a:6:{s:7:"publish";a:6:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:3:{s:10:"_tab_label";s:9:"Structure";s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(9, 1, 1, 10, 'a:6:{s:7:"publish";a:4:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:20;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}'),
(10, 1, 6, 10, 'a:6:{s:7:"publish";a:4:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:20;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:9:"structure";a:6:{s:10:"_tab_label";s:9:"Structure";s:20:"structure__parent_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:14:"structure__uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:22:"structure__template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:17:"structure__hidden";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:26:"structure__listing_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:3:"seo";a:1:{s:10:"_tab_label";s:3:"SEO";}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_reorder_orders`
--

CREATE TABLE IF NOT EXISTS `exp_low_reorder_orders` (
  `set_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  `sort_order` text,
  PRIMARY KEY (`set_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_reorder_sets`
--

CREATE TABLE IF NOT EXISTS `exp_low_reorder_sets` (
  `set_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `set_label` varchar(100) NOT NULL,
  `set_notes` text NOT NULL,
  `new_entries` enum('append','prepend') NOT NULL DEFAULT 'append',
  `clear_cache` enum('y','n') NOT NULL DEFAULT 'y',
  `channels` varchar(255) NOT NULL,
  `cat_option` enum('all','some','one') NOT NULL DEFAULT 'all',
  `cat_groups` varchar(255) NOT NULL,
  `parameters` text NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`set_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_variables`
--

CREATE TABLE IF NOT EXISTS `exp_low_variables` (
  `variable_id` int(6) unsigned NOT NULL,
  `group_id` int(6) unsigned NOT NULL,
  `variable_label` varchar(100) NOT NULL,
  `variable_notes` text NOT NULL,
  `variable_type` varchar(50) NOT NULL,
  `variable_settings` text NOT NULL,
  `variable_order` int(4) unsigned NOT NULL,
  `early_parsing` char(1) NOT NULL DEFAULT 'n',
  `is_hidden` char(1) NOT NULL DEFAULT 'n',
  `save_as_file` char(1) NOT NULL DEFAULT 'n',
  `edit_date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`variable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_low_variables`
--

INSERT INTO `exp_low_variables` (`variable_id`, `group_id`, `variable_label`, `variable_notes`, `variable_type`, `variable_settings`, `variable_order`, `early_parsing`, `is_hidden`, `save_as_file`, `edit_date`) VALUES
(1, 0, '', '', 'low_textarea', '', 0, 'y', 'n', 'n', 1355490296);

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_variable_groups`
--

CREATE TABLE IF NOT EXISTS `exp_low_variable_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(6) unsigned NOT NULL,
  `group_label` varchar(100) NOT NULL,
  `group_notes` text NOT NULL,
  `group_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_cols`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `exp_matrix_cols`
--

INSERT INTO `exp_matrix_cols` (`col_id`, `site_id`, `field_id`, `var_id`, `col_name`, `col_label`, `col_instructions`, `col_type`, `col_required`, `col_search`, `col_order`, `col_width`, `col_settings`) VALUES
(5, 1, 2, NULL, 'product_title', 'Product Title', '', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(6, 1, 2, NULL, 'product_description', 'Product Description', '', 'wygwam', 'n', 'n', 2, '60%', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30='),
(7, 1, 2, NULL, 'product_image', 'Product Image', '', 'assets', 'n', 'n', 1, '15%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjEiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0='),
(8, 1, 6, NULL, 'main_image', 'Main Image', '', 'assets', 'n', 'n', 0, '50%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0='),
(9, 1, 6, NULL, 'home_image', 'Home Image', '', 'assets', 'n', 'n', 1, '50%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0='),
(10, 1, 9, NULL, 'value_copy', 'Value Copy', '', 'text', 'n', 'n', 0, '33%', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(11, 1, 9, NULL, 'authentic_copy', 'Authentic Copy', '', 'text', 'n', 'n', 1, '33%', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(12, 1, 9, NULL, 'service_copy', 'Service Copy', '', 'text', 'n', 'n', 2, '34%', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),
(13, 1, 7, NULL, 'brand_image', 'Brand Image', '', 'assets', 'n', 'n', 0, '15%', 'YTo0OntzOjg6ImZpbGVkaXJzIjthOjE6e2k6MDtzOjE6IjMiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fX0='),
(14, 1, 7, NULL, 'brand_description', 'Brand Description', '', 'text', 'n', 'n', 1, '85%', 'YTo0OntzOjQ6Im1heGwiO3M6NDoiMTAwMCI7czo5OiJtdWx0aWxpbmUiO3M6MToieSI7czozOiJmbXQiO3M6NDoibm9uZSI7czozOiJkaXIiO3M6MzoibHRyIjt9');

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_data`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_5` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` text,
  `col_id_13` text,
  `col_id_14` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `exp_matrix_data`
--

INSERT INTO `exp_matrix_data` (`row_id`, `site_id`, `entry_id`, `field_id`, `var_id`, `is_draft`, `row_order`, `col_id_5`, `col_id_6`, `col_id_7`, `col_id_8`, `col_id_9`, `col_id_10`, `col_id_11`, `col_id_12`, `col_id_13`, `col_id_14`) VALUES
(14, 1, 18, 2, NULL, 0, 1, 'Bronze Goddess Eye Collection', '<p>\n	The Bronze Goddess Eye Collection comes with an Eye Inspire Baked Eyeshadow Quad, Fat Cat mascara (black), Eye Sweep liquid eyeliner (black), Eye Line pencil eyeliner (in Coal) and an application full size eyeshadow brush -&nbsp; plus a little &#39;get the look&#39; flyer.</p>', '{filedir_1}Bronze-eyes.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 1, 18, 2, NULL, 0, 2, 'Smokey Eye Collection', '<p>\n	The Ultimate Smokey Eye Kit &ndash; make up set featuring 3 smokey eye colours in one easy to use pallette, an eyeshadow brush, coal eye pencil, Fat Cat mascara in Ultra Black and Eye Sweep liquid eyeliner in Hot Black - plus a little &#39;get the look&#39; flyer.</p>', '{filedir_1}Smokey-eyes.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 1, 18, 2, NULL, 0, 3, 'Little Angels Collection', '<p>\n	This fantastic Collection includes four of the best tints and illuminators on the market! Enhance your natural radiance with these iridescent skin illuminators in Gold and Pink. Whether it&rsquo;s a golden glow or a pearly pink sheen you desire, simply dot along brow and cheek bones to highlight and accentuate or mix with foundation for an all over shimmer. A soft petal pink liquid tint to instantly brighten lips and add a playful rosy glow to cheeks. Long-lasting wear to make you feel pretty in pink all day long. Introduce a suggestion of flirtation to cheek bones and lips with this sexy rouge tint. Long-lasting wear to create glamour no matter what the occasion.</p>', '{filedir_1}Little-angels.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 1, 18, 2, NULL, 0, 4, 'Summer Nail Collection', '<p>\n	Our Summer Collection contains 4 quick dry, high gloss nail polishes that protect and condition. Colours are: Red (Confident) Hot Pink (Vivacious) Lilac (Generous) and Pale Coral (Spirited). All bottles are 12ml and have a large brush&hellip;.</p>', '{filedir_1}Nail-Collection.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 1, 18, 2, NULL, 0, 5, 'Shimmer Stack and Brush', '<p>\n	Let your inner goddess shine with this ultimate fusion of 5 shimmer powders. Sweep the large brush&nbsp;&nbsp; (included) over the palette, to create shimmering illumination and definition on cheek and brow bones, or use each line of colour as eyeshadow. Perfect handbag accessory!</p>', '{filedir_1}Shimmer-Stack.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 1, 19, 2, NULL, 0, 1, 'Black', '<p>\n	Each pair of foldable tipsyfeet shoes come in our unique colour matched pouch which is made of the same material as the shoe upper.</p>', '{filedir_1}Black.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 1, 19, 2, NULL, 0, 2, 'Bronze', '<p>\n	Each pair of foldable tipsyfeet shoes come in our unique colour matched pouch which is made of the same material as the shoe upper.</p>', '{filedir_1}Bronze.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 1, 19, 2, NULL, 0, 3, 'Gold', '<p>\n	Each pair of foldable tipsyfeet shoes come in our unique colour matched pouch which is made of the same material as the shoe upper.</p>', '{filedir_1}Gold.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 1, 19, 2, NULL, 0, 4, 'Leopard', '<p>\n	Each pair of foldable tipsyfeet shoes come in our unique colour matched pouch which is made of the same material as the shoe upper.</p>', '{filedir_1}Leopard.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 1, 20, 2, NULL, 0, 1, 'Ice Diamond Ice Mini Black Styler', '<p>\n	Our black design styler comes in a velvet drawstring pouch. Tourmaline Plates heat to 200 degrees in 90 seconds. 2 metre, 360 degree,&nbsp; tangle-free, swivel cord. Dual voltage and gloss finish with beautiful crystals.</p>', '{filedir_1}Black-mimi.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 1, 20, 2, NULL, 0, 2, 'Ice Diamond Ice Mini Blue Styler', '<p>\n	Our blue design styler comes in a velvet drawstring pouch. Tourmaline Plates heat to 200 degrees in 90 seconds. 2 metre, 360 degree,&nbsp; tangle-free, swivel cord. Dual voltage and gloss finish with beautiful crystals.</p>', '{filedir_1}Blue-mini.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 1, 20, 2, NULL, 0, 3, 'Ice Diamond Ice Mini Pink Styler', '<p>\n	Our pink design styler comes in a velvet drawstring pouch. Tourmaline Plates heat to 200 degrees in 90 seconds. 2 metre, 360 degree,&nbsp; tangle-free, swivel cord. Dual voltage and gloss finish with beautiful crystals.</p>', '{filedir_1}Pink-mini.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 1, 20, 2, NULL, 0, 4, 'Ice Diamond Ice Mini Purple Styler', '<p>\n	Our purple design styler comes in a velvet drawstring pouch. Tourmaline Plates heat to 200 degrees in 90 seconds. 2 metre, 360 degree,&nbsp; tangle-free, swivel cord. Dual voltage and gloss finish with beautiful crystals.</p>', '{filedir_1}Purple-mini.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 1, 20, 2, NULL, 0, 5, 'Ice Diamond White with Pink Plates Stylers', '<p>\n	Our latest design styler comes in a velvet drawstring pouch. Tourmaline Plates heat to 200 degrees in 90 seconds. 2 metre, 360 degree,&nbsp; tangle-free, swivel cord. Dual voltage and gloss finish with beautiful crystals.</p>', '{filedir_1}White-mini.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 1, 21, 2, NULL, 0, 1, 'Mojito', '<p>\n	Many believe it is possibly the world&rsquo;s first cocktail, with its origins dating back to 16th century Cuba. Then it was called &ldquo;El Draque&rdquo; in honour of explorer and sailor Sir Francis Drake. Legend has it the drink was first concocted by the pirates of old as a means of covering up the harsh taste of their primitive form of rum.</p>\n<p>\n	<em><strong>Serving Suggestion:</strong> Shake with ice and pour into a highball glass before garnishing with fresh mint leaves. If desired, top with soda to taste.</em></p>', '{filedir_1}mojito.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 1, 21, 2, NULL, 0, 2, 'Mai Tai', '<p>\n	This is a cocktail with a controversial past, however the favoured history is that it was first created by &ldquo;Trader Vic&rdquo; in the 1940s in his Polynesian themed restaurant. Trader Vic concocted the drink in honour of his visitors from Tahiti, who cried out &ldquo;Maitai Roa&rdquo; when they tasted it, translating as &ldquo;very good!&rdquo; Hence the name &ldquo;Mai Tai&rdquo; was born!</p>\n<p>\n	<em><strong>Serving Suggestion:</strong> Shake with ice and pour into a double rocks glass. Garnish with fresh fruits and/or colourful exotic flowers.</em></p>', '{filedir_1}mai-tai.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 1, 21, 2, NULL, 0, 3, 'Cosmo', '<p>\n	Informally, it is referred to as a Cosmo, but whilst it may be pink, this is a cocktail to be taken seriously! Not overly sweet or fruity, but a slightly tart and a well-balanced cocktail. First created in America during the 1970s and rose to fame in the 1990s through the popularity of Sex and the City.</p>\n<p>\n	<em><strong>Serving Suggestion:</strong> Shake with ice before straining into a chilled martini glass and garnish with a twist of lime. Traditionally a coin sized piece of orange peel should be &ldquo;flamed&rdquo; across the top to coat the drink with the aroma of citrus oil.</em></p>', '{filedir_1}cosmo.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 1, 22, 2, NULL, 0, 1, 'Cosmopolitan', '<p>\n	A 250ml can, our Cosmopolitan contains a blend of cranberry juice, lime juice, orange liqueur and premium vodka.</p>', '{filedir_1}cosmo-420-x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 1, 22, 2, NULL, 0, 2, 'Mojito', '<p>\n	A 250ml can, our Mojito contains a blend of lime, fresh mint, soda water, sugar cane and premium white rum.</p>', '{filedir_1}mojito-420-x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 1, 22, 2, NULL, 0, 3, 'On the Beach', '<p>\n	A 250ml can, our On the Beach contains a blend of orange juice, cranberry juice, peach schnapps and premium vodka.</p>', '{filedir_1}on-the-beach420-x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 1, 22, 2, NULL, 0, 4, 'Pina Colada', '<p>\n	A 250ml can, our Pina Colada contains a blend of coconut, pineapple juice and premium white rum.</p>', '{filedir_1}pina-420-x-420.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 1, 23, 2, NULL, 0, 1, 'SINGLE ROW BLACK DIAMANTE', '<p>\n	A subtle and delicate touch of &ldquo;bling.&rdquo; A pair of Single Row Black Diamante bra straps is an ideal accessory. Not only for a romantic date, or dinner out, but also at any time, when an extra touch of glamour is always appreciated. Encrusted with black glass diamante jointed with metal links. Single Row Black Diamante&nbsp;straps (check spacing here) are sold in pairs, with adjustable fittings for maximum support and comfort. (Decorative chain length is 34cm, with 12cm adjustable chain length) This product is hypoallergenic / nickel free.These straps are packed in gift boxes as standard.</p>', '{filedir_1}bra-strap-black-stone-SG-row.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 1, 23, 2, NULL, 0, 2, 'ALTERNATE TOPAZ, BLACK AND CLEAR', '<p>\n	A subtle and delicate touch of &ldquo;bling.&rdquo; A pair of Single Row Diamante bra straps with alternate topaz, black and clear stones is an ideal accessory. Not only for a romantic date, or dinner out, but also at any time, when an extra touch of glamour is always appreciated. Encrusted with&nbsp;(check spacing here) glass diamantes and jointed with metal links, the straps are sold in pairs, with adjustable fittings for maximum support and comfort. (Decorative chain length is 34cm, with 12cm adjustable chain length) This product is hypoallergenic / nickel free.These straps are packed in gift boxes as standard.</p>', '{filedir_1}bra-strap-multi-best.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 1, 23, 2, NULL, 0, 3, 'Glamorous Bra Straps Pearl-effect', '<p>\n	A subtle and delicate pair of Single Row Pearl-effect bra straps is an ideal accessory. Not only for a romantic date, or dinner out, but also at any time, when an extra touch of glamour is always appreciated. These straps are sold in pairs, with adjustable fittings for maximum support and comfort. (Decorative chain length is 34cm, with 12cm adjustable chain length) This product is hypoallergenic / nickel free.These straps are packed in gift boxes as standard.</p>', '{filedir_1}bra-strap-pearl.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 1, 23, 2, NULL, 0, 4, 'SINGLE ROW CLEAR DIAMANTE', '<p>\n	A subtle and delicate touch of &ldquo;bling.&rdquo;&nbsp; A pair of Single Row Diamante bra straps is an ideal accessory. Not only for a romantic date, or dinner out, but also at any time, where an extra touch of glamour is always appreciated. Encrusted with&nbsp;(check spacing here) glass diamantes and jointed with metal links, the straps are sold in pairs, with adjustable fittings for maximum support and comfort. (Decorative chain length is 34cm, with 12cm adjustable chain length) This product is hypoallergenic / nickel free.These straps are packed in gift boxes as standard.</p>', '{filedir_1}bra-strap-silver-clear-stone.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 1, 24, 2, NULL, 0, 1, 'Chocolate Brownie', '<p>\n	Chewy, Chocolatey and Carefully made to give you the best tasting Brownie with an ambient shelf life of 6 months! What more could you ask for? Well how about a Flapjack, Rich fruit cake and Caramel Shortbread, all available now&hellip; Go on give Hewby&#39;s a try... You wont be disapointed...</p>', '{filedir_1}choc-brownie.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 1, 18, 7, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Shimmer-Stack-hero.jpg', 'The creation of gorgeous, distinctive and spirited cosmetics is their passion. MeMeMe are a female team who are submersed in all the must-have fashion magazines, samples, concepts and mood boards and enjoy nothing more than chatting about cosmetics - which is helpful when it comes to innovation, as they know what women want and indeed what they can afford to pay! They pride themselves on offering affordable luxury within an eclectic range of handbag essentials, on-trend editions, classic lines and gift collections as individual and versatile as their customers, and genuinely believe in going that extra mile in everything they do.\n'),
(42, 1, 19, 7, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Hero-Tipsy-Feet.jpg', 'Whether you''ve spent a night partying, or have traipsed around airport or train terminals, one thing’s for sure; if you''ve been wearing high heels for any length of time, then your feet will be aching - tipsyfeet fold-up shoes are the answer. A pair of foldable shoes that tuck away into a neat little pouch that you can pop into your handbag or keep in the glove box of your car. Tipsyfeet has become a very popular, many would say iconic, brand over the years since their early featuring on the BBC''s Dragon’s Den.'),
(43, 1, 20, 7, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Hero-Ice-Diamonds.jpg', 'At just 150mm long, this versatile styling iron is the perfect travel companion.. Compact, yet powerful, they heat to a salon 200 degrees in 90 seconds!!'),
(44, 1, 21, 7, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Hero-Nuba.jpg', 'We are passionate about cocktails. In fact, we love cocktails so much we want to enjoy them even when a cocktail bar is out of reach! If you are at 30,000 feet or zooming through the countryside onboard a fast speed train, you can still enjoy a professionally mixed cocktail - with our pre-mixed Nuba Cocktail Range. 187ml PET bottles.'),
(45, 1, 24, 7, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Hewbys.jpg', 'A generous mix of scrummy ingredients, traditional recipes and a large dollop of TLC. Individually wrapped to eat on the go, or enjoy over coffee with friends.'),
(46, 1, 23, 7, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Glamourous-bra-strap-420x-420.jpg', 'Our high-flying, Glamorous Bra Straps are the perfect impulse purchase for travellers, with proven sales onboard Virgin and Thomson Airways! They are not just Bra Straps; they are jewels for your shoulders!'),
(48, 1, 8, 6, NULL, 0, 1, NULL, NULL, NULL, '{filedir_3}COSMETICS_HERO.jpg', '{filedir_3}COSMETICS.jpg', NULL, NULL, NULL, NULL, NULL),
(49, 1, 9, 6, NULL, 0, 1, NULL, NULL, NULL, '{filedir_3}ACCESSORIES_HERO.jpg', '{filedir_3}ACCESSORIES.jpg', NULL, NULL, NULL, NULL, NULL),
(50, 1, 22, 7, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}Authentic-hero.jpg', 'Our fantastic, Bar-friendly, 250ml can design is perfect for adding that impulse purchase with an amazing 2 for £5 onboard offer! '),
(57, 1, 17, 7, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{filedir_3}hero-Jackpot.jpg', 'Jackpots aim to bring together classic taste combinations, with a modern twist. Produced using hand-selected spuds, all grown, picked, cooked and packed on a farm just outside Colchester, Essex; a bag of Jackpots prides itself on being filled with provenance. Available in Lightly Salted, Roast Beef and Horseradish, Mature English Cheddar and Spring Onion, Sweet Thai Chilli and Sea Salt and Cyder Vinegar.'),
(58, 1, 17, 2, NULL, 0, 1, 'Lightly Salted', '', '{filedir_1}lightly-salted.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 1, 17, 2, NULL, 0, 2, 'Sea Salt & Cyder Vinegar', '', '{filedir_1}sea-salt.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 1, 17, 2, NULL, 0, 3, 'Mature English Cheddar & Spring Onion', '', '{filedir_1}chedar.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 1, 17, 2, NULL, 0, 4, 'Sweet Chilli', '', '{filedir_1}sweet-chilli.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 1, 17, 2, NULL, 0, 5, 'Roast Beef & Horseradish ', '', '{filedir_1}roast-beef.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 1, 23, 2, NULL, 0, 5, 'ALTERNATE CLEAR DIAMANTES WITH SILVER BARS', '<p>\n	A stunning and elegant style of diamante bra straps. A simple silver bar separates each diamante. One of our most popular designs, these bra straps are sold in pairs, with adjustable fittings for maximum support and comfort. (Decorative chain length is 34cm, with 12cm adjustable chain length) This product is hypoallergenic / nickel free.&nbsp;These straps are packed in gift boxes as standard.</p>', '{filedir_1}silver-strap-x-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 1, 23, 2, NULL, 0, 6, 'ALTERNATE BLACK DIAMANTES WITH SILVER BARS', '<p>\n	A stunning and elegant style of diamante bra straps. A simple silver bar separates each black diamante. One of our most popular designs, these straps are available in two colours. Alternate Black with Silver Bars are sold in pairs, with adjustable fittings for maximum support and comfort. (Decorative chain length is 34cm, with 12cm adjustable chain length) This product is hypoallergenic / nickel free. These straps are packed in gift boxes as standard.</p>', '{filedir_1}bra-strap-black-stone-with-bars.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 1, 7, 6, NULL, 0, 1, NULL, NULL, NULL, '{filedir_3}CONSUMABLES_HERO.jpg', '{filedir_3}CONSUMABLES.jpg', NULL, NULL, NULL, NULL, NULL),
(68, 1, 1, 9, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, 'A consultative approach to the market, NPD and working with brands in their infancy.', 'An experienced team of brand managers, creatives, sales, marketing and procurement specialists with industry specific experience, insight and the enthusiasm to drive brands forward.', 'A flexible service bringing brands to market and working with the industry to generate incremental revenue through effective category planning.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_members`
--

CREATE TABLE IF NOT EXISTS `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_members`
--

INSERT INTO `exp_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `daylight_savings`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`, `rte_enabled`, `rte_toolset_id`) VALUES
(1, 1, 'Admin', 'Janice', 'f06de93566e8e36cf995926a4675d4ef84647398a8730c0e4db31d37d670699b7db9087eed59d80c79fb964668b69bee2db8c4a447aa17f9174436c4460ab540', 'Gi>6R{mR)65Q23kr2&RER!V+COWo[pyvQ<Rof#9wJWq4<}>oGhI`J9!mKHa)GiY,]8<<g>t.${_gwy\\FyFfmL&cmg''UQqOa2?F*w4<f``I5qn]/&7ob|@;T<=Y2Zc<OU', '937e8e7d550406d1b53f1befc0fe8aa051d8585b', '4e1c974366a35a0e267ada1f8c9c7b3079854f59', NULL, 'martin@shoot-the-moon.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '127.0.0.1', 1354898139, 1374077129, 1374765952, 27, 0, 0, 0, 1357813995, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UTC', 'n', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', '', 'Structure|C=addons_modules&M=show_module_cp&module=structure|1\nEdit Group|C=addons_modules&M=show_module_cp&module=field_editor&method=edit&group_id=2&field_id=|2\nManage Entries|C=addons_modules&M=show_module_cp&module=freeform|3', 'n', 0, 'y', 0),
(2, 6, 'stmstaff', 'Staff', '0106a08ad14cfad789bf1a3f6919bcc72be261324d4b78dc847d733792fd7dd30d1609c693d3f1ad1420e7049327f10b976ff081ca09faf7d95564b18ca6cf7d', 's(!!k\\N0fn+}H<{K5s}17o6>b|B&Qg$5mtKM[|}s?WQFT]z&.}?'',p]@093f|FS6l~B4oSh$>;U~#M;gXv7KlT!I7_Wj%J9Fu$)W*;K@Z''%Fx''jl)ro&6Ndj+_c)-bl5', 'ab1fc09c39bc25deb255d771a24d796e1a9f300d', 'a3f3ec468aa2abd090e268cf3a5ca0968ff2d406', NULL, 'kathryn@shoot-the-moon.co.uk', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '109.111.197.225', 1355393449, 1355751240, 1356090950, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UTC', 'n', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_bulletin_board`
--

CREATE TABLE IF NOT EXISTS `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_data`
--

CREATE TABLE IF NOT EXISTS `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_data`
--

INSERT INTO `exp_member_data` (`member_id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_fields`
--

CREATE TABLE IF NOT EXISTS `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_groups`
--

INSERT INTO `exp_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_admin_accessories`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`) VALUES
(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(6, 1, 'STM Staff', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_homepage`
--

CREATE TABLE IF NOT EXISTS `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_homepage`
--

INSERT INTO `exp_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`) VALUES
(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0),
(2, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_search`
--

CREATE TABLE IF NOT EXISTS `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_copies`
--

CREATE TABLE IF NOT EXISTS `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_data`
--

CREATE TABLE IF NOT EXISTS `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_folders`
--

CREATE TABLE IF NOT EXISTS `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_message_folders`
--

INSERT INTO `exp_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`) VALUES
(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_listed`
--

CREATE TABLE IF NOT EXISTS `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_modules`
--

CREATE TABLE IF NOT EXISTS `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `exp_modules`
--

INSERT INTO `exp_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`) VALUES
(1, 'Comment', '2.3', 'y', 'n'),
(2, 'Email', '2.0', 'n', 'n'),
(3, 'Emoticon', '2.0', 'n', 'n'),
(4, 'Jquery', '1.0', 'n', 'n'),
(5, 'Rss', '2.0', 'n', 'n'),
(6, 'Safecracker', '2.1', 'y', 'n'),
(7, 'Search', '2.2', 'n', 'n'),
(8, 'Channel', '2.0.1', 'n', 'n'),
(9, 'Member', '2.1', 'n', 'n'),
(10, 'Stats', '2.0', 'n', 'n'),
(11, 'Rte', '1.0', 'y', 'n'),
(13, 'Assets', '1.2.2', 'y', 'n'),
(16, 'Structure', '3.3.4', 'y', 'y'),
(17, 'Wygwam', '2.6.3', 'y', 'n'),
(18, 'Freeform', '3.1.4', 'y', 'n'),
(19, 'Deeploy_helper', '2.0.3', 'y', 'n'),
(20, 'Zoo_flexible_admin', '1.62', 'y', 'n'),
(21, 'Backup_proish', '1.0.5', 'y', 'n'),
(22, 'Field_editor', '1.0.3', 'y', 'n'),
(23, 'Playa', '4.3.3', 'n', 'n'),
(24, 'Low_variables', '2.3.2', 'y', 'n'),
(25, 'Low_reorder', '2.0.4', 'y', 'n'),
(26, 'Query', '2.0', 'n', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_module_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_module_member_groups`
--

INSERT INTO `exp_module_member_groups` (`group_id`, `module_id`) VALUES
(6, 1),
(6, 6),
(6, 11),
(6, 13),
(6, 16),
(6, 17),
(6, 18),
(6, 19);

-- --------------------------------------------------------

--
-- Table structure for table `exp_online_users`
--

CREATE TABLE IF NOT EXISTS `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8957 ;

--
-- Dumping data for table `exp_online_users`
--

INSERT INTO `exp_online_users` (`online_id`, `site_id`, `member_id`, `in_forum`, `name`, `ip_address`, `date`, `anon`) VALUES
(8954, 1, 0, 'n', '', '109.111.197.225', 1374765945, ''),
(8955, 1, 0, 'n', '', '109.111.197.225', 1374765945, ''),
(8956, 1, 1, 'n', 'Janice', '109.111.197.225', 1374765997, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_password_lockout`
--

CREATE TABLE IF NOT EXISTS `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `exp_password_lockout`
--

INSERT INTO `exp_password_lockout` (`lockout_id`, `login_date`, `ip_address`, `user_agent`, `username`) VALUES
(2, 1355406708, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(3, 1355743672, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11', 'admin'),
(4, 1357229493, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(5, 1357306170, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17', 'admin'),
(6, 1357306178, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17', 'admin'),
(7, 1357320860, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(8, 1357551714, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(9, 1357558970, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(10, 1357651013, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 'admin'),
(11, 1357653373, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', 'admin'),
(12, 1357809635, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17', 'admin'),
(13, 1357809644, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17', 'admin'),
(14, 1360154365, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(15, 1360154460, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(16, 1360154461, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(17, 1360158676, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(18, 1360158677, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(19, 1360158687, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(20, 1360158699, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'admin'),
(21, 1361193103, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'admin'),
(22, 1363368408, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17', 'admin'),
(23, 1363368422, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17', 'admin'),
(24, 1363368431, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17', 'admin'),
(25, 1363369786, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11', 'admin'),
(26, 1373969606, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `exp_ping_servers`
--

CREATE TABLE IF NOT EXISTS `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_playa_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_remember_me`
--

CREATE TABLE IF NOT EXISTS `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_reset_password`
--

CREATE TABLE IF NOT EXISTS `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_revision_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=96 ;

--
-- Dumping data for table `exp_revision_tracker`
--

INSERT INTO `exp_revision_tracker` (`tracker_id`, `item_id`, `item_table`, `item_field`, `item_date`, `item_author_id`, `item_data`) VALUES
(2, 2, 'exp_templates', 'template_data', 1354898828, 1, '{sn_doc_head}        \n    </head>\n    <body class="home">\n        {sn_chrome_frame}\n\n{sn_header}\n		<div id="wrap" class="clearfix">\n            <section id="categories">\n            <p class="scroll left"><a href="" class="ir">Scroll left</a></p>\n            <ul class="clearfix">\n            	<li><h2>Consumables</h2><p><a href="category-page.html"><img src="/img/dummies/dummy-5.jpeg" /></a></p></li>\n            	<li><h2>Cosmetics</h2><p><a href="category-page.html"><img src="/img/dummies/dummy-6.jpeg" /></a></p></li>\n            	<li><h2>Accessories</h2><p><a href="category-page.html"><img src="/img/dummies/dummy-7.jpeg" /></a></p></li>\n            </ul>\n            <p class="scroll right"><a href="" class="ir">Scroll right</a></p>\n            </section>\n            <section id="content" class="clearfix">\n            <div id="editorial">\n            	<h1>Is there a gap in your range or do you just need a fresh shot of innovation? - We''re here to assist and realise your brand potential.</h1>\n            	<ul>\n            	<li>\n            	<h2>Value</h2>\n            	<p>Wisi ornare dicta quae nihil delectus veniam duis corrupti sociosqu bibendum repellat harum beatae etiam volutpat, culpa torquent, maiores nemo cupidatat/p>\n            	</li>\n            	<li>\n            	<h2>Authentic</h2>\n            	<p>Modi assumenda? Eu malesuada per quasi! Eos inceptos porta perspiciatis quas tenetur, officiis, incidunt pariatur? Cubilia faucibus doloremque, facere pretium.</p>\n				</li>\n				<li>\n            	<h2>Service</h2>\n            	<p>Molestias phasellus rem eos hendrerit, adipiscing autem ullamcorper varius consequat pede! Praesent, congue cupidatat, amet, assumenda do dui voluptates possimus.</p>\n            	</li>\n            	</ul>\n            </div>\n            <div id="sidePanel">\n            	<img src="/img/janice.jpg" alt="" width="240px" />\n            	<p><a href="">More about the STM Brands Team</a></p>\n            </div>\n            </section>\n        </div>\n{sn_footer}\n{sn_base_scripts}\n	</body>\n</html>\n'),
(3, 3, 'exp_templates', 'template_data', 1354898971, 1, '{sn_doc_head}        \n    </head>\n    <body class="home">\n        {sn_chrome_frame}\n\n{sn_header}\n		<div id="wrap" class="clearfix">\n           <section id="content" class="clearfix">\n				<div id="catOverview">\n					<h1>Consumables</h1>\n					<img src="/img/dummies/dummy-3.jpg" alt="" width="420px" height="210px" />\n					<p class="catText">Aute labore habitant nibh vulputate, malesuada justo commodo, lorem proin eu iusto itaque reprehenderit fugit! Libero? Vel quisquam cupidatat, exercitation saepe animi! Odit possimus ultricies quasi rutrum ultricies, do sapien.</p>\n					<div id="cta">\n						<h3>Do you have a product we can help you distribute?</h3>\n						<p>Call Janice direct on 07800 897672 or 0161 205 3311</p>\n						<p><a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\n					</div>\n					\n				</div>\n				<ul id="catBrands">\n					<li><h4>Brand A</h4><a href="single-product.html"></a></li>\n					<li><h4>Brand A</h4><a href="single-product.html"></a></li>\n					<li><h4>Brand A</h4><a href="single-product.html"></a></li>\n					<li><h4>Brand A</h4><a href="single-product.html"></a></li>\n					<li><h4>Brand A</h4><a href="single-product.html"></a></li>\n				</ul>\n            </section>\n        </div>\n{sn_footer}\n{sn_base_scripts}\n	</body>\n</html>\n'),
(4, 4, 'exp_templates', 'template_data', 1355306140, 1, ''),
(6, 6, 'exp_templates', 'template_data', 1355314860, 1, ''),
(7, 7, 'exp_templates', 'template_data', 1355324008, 1, ''),
(8, 7, 'exp_templates', 'template_data', 1355324026, 1, ''),
(9, 7, 'exp_templates', 'template_data', 1355324123, 1, ''),
(10, 8, 'exp_templates', 'template_data', 1355332890, 1, ''),
(12, 6, 'exp_templates', 'template_data', 1355398380, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="home no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				<h1>{title}</h1>\n				\n				{exp:freeform:form collection="Contact Form" form:class="contact-form" required="forename|surname|email|message" return="/thank-you" recipients="yes" recipient_limit="1" recipient_template="contact_us"}\n					\n					<input type="hidden" name="recipient_email" value="janice@shoot-the-moon.co.uk">\n\n					<div class="row">\n						<label>First Name*</label>\n						<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n					</div>\n				\n					<div class="row">\n						<label>Surname*</label>\n						<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n					</div>\n				\n					<div class="row">\n						<label>Email Address*</label>\n						<input type="text" name="email" value="" class="required" placeholder="Your email address">\n					</div>\n				\n					<div class="row"><label>Message*</label>\n						<textarea name="message" class="required" placeholder="Please leave a small message so that one our staff can respond accordingly"></textarea>\n					</div>\n				\n					<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n				\n				{/exp:freeform:form}\n				\n				\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n{/exp:channel:entries}\n'),
(15, 2, 'exp_templates', 'template_data', 1355417827, 1, '{exp:channel:entries}\n\n{sn_doc_head}\n</head>\n\n<body>\n	{sn_chrome_frame}\n\n	{sn_header}\n\n	<div id="wrap" class="clearfix">\n	<section id="content" class="clearfix">\n		<div id="catOverview">\n			<h1>{title}</h1>\n			{images}\n			{exp:ce_img:single src="{main_image}" width="420" height="420"}\n			{/images}\n			<p class="catText">{description}</p>\n			\n{/exp:channel:entries}\n\n			<div id="cta">\n				<h3>Do you have a product we can help you distribute?</h3>\n				<p>Call Janice direct on 07800 897672 or 0161 205 3311</p>\n				<p><a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\n			</div>\n		</div>\n		<ul id="catBrands">\n			{exp:channel:entries entry_id="{structure:child_ids}" fixed_order="{structure:child_ids}"}\n			<li><h4>{title}</h4><a href="{page_uri}">{brand}{exp:ce_img:single src="{brand_image}" width="195" height="195"}{/brand}</a></li>\n			{/exp:channel:entries}\n		</ul>\n	</section>\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n'),
(31, 7, 'exp_templates', 'template_data', 1357558115, 1, '{sn_doc_head}        \n</head>\n\n<body class="home no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix forceHeight">\n				\n				<h1>Form submission successful</h1>\n				<p>A member of our team will in touch soon.</p>\n				\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n'),
(58, 1, 'exp_templates', 'template_data', 1357731412, 1, '{exp:channel:entries url_title="stm-brands"}\n\n{sn_doc_head}        \n</head>\n\n<body class="home">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		{embed="site/category_carousel"}\n		\n		<section id="content" class="clearfix">\n			<div id="editorial">\n				<h1>{homepage_copy}</h1>\n				<ul>\n					<li>\n						<h2>What we do</h2>\n							<p>{snippet_copy}{value_copy}{/snippet_copy}</p>\n					</li>\n					<li>\n						<h2>Who we are</h2>\n						<p>{snippet_copy}{authentic_copy}{/snippet_copy}</p>\n					</li>\n					<li class="last-item">\n						<h2>How we do it</h2>\n						<p>{snippet_copy}{service_copy}{/snippet_copy}</p>\n					</li>\n				</ul>\n			</div>\n			<ul id="sidePanel">\n				<li>\n					<img src="/images/stmbrands/general/logistics.jpg" />\n					<h3>Our Distribution</h3>\n					<a href=""></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/authentic_small.jpg" />\n					<h3>New for 2013</h3>\n					<p>Cocktails in a can...</p>\n					<a href=""></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/glamorous_small.jpg" />\n					<h3>Onboard Nowand</h3>\n					<p>Sales forecast doubled for Winter 2012</p>\n					<a href=""></a>\n				</li>\n			</ul>\n		</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n\n{/exp:channel:entries}\n'),
(59, 1, 'exp_templates', 'template_data', 1357731573, 1, '{exp:channel:entries url_title="stm-brands"}\n\n{sn_doc_head}        \n</head>\n\n<body class="home">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		{embed="site/category_carousel"}\n		\n		<section id="content" class="clearfix">\n			<div id="editorial">\n				<h1>{homepage_copy}</h1>\n				<ul>\n					<li>\n						<h2>What we do</h2>\n							<p>{snippet_copy}{value_copy}{/snippet_copy}</p>\n					</li>\n					<li>\n						<h2>Who we are</h2>\n						<p>{snippet_copy}{authentic_copy}{/snippet_copy}</p>\n					</li>\n					<li class="last-item">\n						<h2>How we do it</h2>\n						<p>{snippet_copy}{service_copy}{/snippet_copy}</p>\n					</li>\n				</ul>\n			</div>\n			<ul id="sidePanel">\n				<li>\n					<img src="/images/stmbrands/general/logistics.jpg" />\n					<h3>Our Distribution</h3>\n					<a href="/our-distribution-and-procurement"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/authentic_small.jpg" />\n					<h3>New for 2013</h3>\n					<p>Cocktails in a can...</p>\n					<a href="/food-and-drink/the-authentic-cocktail/"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/glamorous_small.jpg" />\n					<h3>Onboard Nowand</h3>\n					<p>Sales forecast doubled for Winter 2012</p>\n					<a href="/accessories/glamorous-bra-straps/"></a>\n				</li>\n			</ul>\n		</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n\n{/exp:channel:entries}'),
(61, 9, 'exp_templates', 'template_data', 1357731870, 1, '{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		<section id="content" class="clearfix">					\n			<div id="pageLeft">\n				<h1>Procurement &amp; Logistics</h1>\n				<img src="/img/procurement.jpg" width="420" height="290" alt="">\n				\n				<p style="font-weight: bold;">An end-to-end procurement & logistics solution, aimed at driving your range development through sourcing true innovation at competitive prices, supported by an established hassle-free logistics solution.\n</p>\n \n<p>Sourcing products for the inflight & travel sector can be time consuming, requiring specialist category knowledge in both food & non food categories. STM Brands can, not only, provide brands from our existing portfolio partners, but can also source products on your behalf from the UK, Europe & Worldwide. STM aim to facilitate a consumer-focused range within the sector margin parameters, ensuring your retail proposition has a recognised point of difference.</p>\n			</div>\n			<div id="pageRight">\n				<ul>\n					<li>Established industry wide distribution network</li>\n	<li>Effective & efficient logistics, ensuring hassle-free sourcing</li>\n	<li>Competitive, delivered to depot pricing</li>\n	<li>Real innovation through unique sourcing approach</li>\n	<li>Bespoke sourcing capability in support of your "Own Brands"</li>\n	<li>With the added creative support of our sister agency Shoot the Moon</li>\n				</ul>\n			</div>\n		</section>\n	\n	</div>\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(62, 1, 'exp_templates', 'template_data', 1357733474, 1, '{exp:channel:entries url_title="stm-brands"}\n\n{sn_doc_head}        \n</head>\n\n<body class="home">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		{embed="site/category_carousel"}\n		\n		<section id="content" class="clearfix">\n			<div id="editorial">\n				<h1>{homepage_copy}</h1>\n				<ul>\n					<li>\n						<h2>What we do</h2>\n							<p>{snippet_copy}{value_copy}{/snippet_copy}</p>\n					</li>\n					<li>\n						<h2>Who we are</h2>\n						<p>{snippet_copy}{authentic_copy}{/snippet_copy}</p>\n					</li>\n					<li class="last-item">\n						<h2>How we do it</h2>\n						<p>{snippet_copy}{service_copy}{/snippet_copy}</p>\n					</li>\n				</ul>\n			</div>\n			<ul id="sidePanel">\n				<li class="banner">\n					<a href="/our-distribution-and-procurement"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/authentic_small.jpg" />\n					<h3>New for 2013</h3>\n					<p>Cocktails in a can...</p>\n					<a href="/food-and-drink/the-authentic-cocktail/"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/glamorous_small.jpg" />\n					<h3>Onboard Nowand</h3>\n					<p>Sales forecast doubled for Winter 2012</p>\n					<a href="/accessories/glamorous-bra-straps/"></a>\n				</li>\n			</ul>\n		</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n\n{/exp:channel:entries}'),
(63, 9, 'exp_templates', 'template_data', 1357744736, 1, '{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		<section id="content" class="clearfix">					\n			<div id="pageLeft">\n				<h1>Our Distribution and Procurement</h1>\n				<img src="/img/procurement.jpg" width="420" height="290" alt="">\n				\n				<p style="font-weight: bold;">An end-to-end procurement & logistics solution, aimed at driving your range development through sourcing true innovation at competitive prices, supported by an established hassle-free logistics solution.\n</p>\n \n<p>Sourcing products for the inflight & travel sector can be time consuming, requiring specialist category knowledge in both food & non food categories. STM Brands can, not only, provide brands from our existing portfolio partners, but can also source products on your behalf from the UK, Europe & Worldwide. STM aim to facilitate a consumer-focused range within the sector margin parameters, ensuring your retail proposition has a recognised point of difference.</p>\n			</div>\n			<div id="pageRight">\n				<ul>\n					<li>Established industry wide distribution network</li>\n	<li>Effective & efficient logistics, ensuring hassle-free sourcing</li>\n	<li>Competitive, delivered to depot pricing</li>\n	<li>Real innovation through unique sourcing approach</li>\n	<li>Bespoke sourcing capability in support of your "Own Brands"</li>\n	<li>With the added creative support of our sister agency Shoot the Moon</li>\n				</ul>\n			</div>\n		</section>\n	\n	</div>\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(64, 6, 'exp_templates', 'template_data', 1357811739, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="home no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				<h1>{title}</h1>\n				\n				{exp:freeform:form collection="Contact Form" form:class="contact-form" required="forename|surname|email|message" return="/thank-you" recipients="yes" recipient_limit="1" recipient_template="contact_us"}\n					\n					<input type="hidden" name="recipient_email" value="janice@shoot-the-moon.co.uk">\n\n					<div class="row">\n						<label>First Name*</label>\n						<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n					</div>\n				\n					<div class="row">\n						<label>Surname*</label>\n						<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n					</div>\n				\n					<div class="row">\n						<label>Email Address*</label>\n						<input type="text" name="email" value="" class="required" placeholder="Your email address">\n					</div>\n				\n					<div class="row"><label>Message*</label>\n						<textarea name="message" class="required" placeholder="Please leave a small message so that one of our staff can respond accordingly"></textarea>\n					</div>\n				\n					<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n				\n				{/exp:freeform:form}\n				\n				\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n{/exp:channel:entries}\n'),
(68, 9, 'exp_templates', 'template_data', 1357812347, 1, '{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		<section id="content" class="clearfix">					\n			<div id="pageLeft">\n				<h1>Our Distribution and Procurement</h1>\n				<img src="/img/procurement.jpg" width="420" height="290" alt="">\n				\n				<p style="font-weight: bold;">An end-to-end procurement & logistics solution, aimed at driving your range development through sourcing true innovation at competitive prices, supported by an established hassle-free logistics solution.\n</p>\n \n<p>Sourcing products for the inflight & travel sector can be time consuming, requiring specialist category knowledge in both food & non food categories. STM Brands not only provides brands from our existing portfolio partners, but can also source products on your behalf from the UK, Europe & Worldwide.</p>\n\n<p>STM Brands aim to facilitate a consumer-focused range within the sector margin parameters, ensuring your retail proposition has a recognised point of difference.</p>\n			</div>\n			<div id="pageRight">\n				<ul>\n					<li>Established, industry-wide distribution network</li>\n	<li>Effective & efficient logistics, ensuring hassle-free sourcing</li>\n	<li>Competitive, delivered to depot pricing</li>\n	<li>Real innovation through unique sourcing approach</li>\n	<li>Bespoke sourcing capability in support of your Own Brands</li>\n	<li>With the added creative support of our sister agency, Shoot the Moon</li>\n				</ul>\n			</div>\n		</section>\n	\n	</div>\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(72, 6, 'exp_templates', 'template_data', 1357813134, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="home no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				<h1>{title}</h1>\n				\n				{exp:freeform:form collection="Contact Form" form:class="contact-form" required="forename|surname|email|message" return="/thank-you" recipients="yes" recipient_limit="1" recipient_template="contact_us"}\n					\n					<input type="hidden" name="recipient_email" value="janice@shoot-the-moon.co.uk">\n\n					<div class="row">\n						<label>First Name*</label>\n						<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n					</div>\n				\n					<div class="row">\n						<label>Surname*</label>\n						<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n					</div>\n				\n					<div class="row">\n						<label>Email Address*</label>\n						<input type="text" name="email" value="" class="required" placeholder="Your email address">\n					</div>\n				\n					<div class="row"><label>Message*</label>\n						<textarea name="message" class="required" placeholder="Please leave a short message and we''ll be in-touch"></textarea>\n					</div>\n				\n					<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n				\n				{/exp:freeform:form}\n				\n				\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n{/exp:channel:entries}\n'),
(74, 5, 'exp_templates', 'template_data', 1357814646, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				\n					\n				\n					\n					\n					{if segment_2}\n					<div id="newsLeftSingle">\n						<h1>News &amp; Updates</h1>\n						<div class="news-snippet">\n						<h2>{title}</h2>\n						<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n						{exp:ce_img:single src="{news_image}" width="420"}\n						{news_story}\n						{if segment_2}<p class="back"><a href="/news">Back to all news</a></p>{/if}\n						</div>\n						<span class="share">Share this story via:</span>{exp:hj_social_bookmarks sites="Facebook|Twitter|Email" entry_id="8" show_hyperlink_text="right" class="social"}\n					</div>\n					<div id="newsRightSingle">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n						\n<div id="divRss"></div>\n\n\n\n\n					</div>\n					{/if}\n					\n					\n					\n{/exp:channel:entries}\n\n				\n				\n				\n					\n					{if segment_2 == ""}\n					<div id="newsLeftList">\n						<h1>News &amp; Updates</h1>\n					{exp:channel:entries channel="news" dynamic="off"}\n					\n						<div class="news-snippet">\n							\n							{exp:ce_img:single src="{news_image}" width="195" class="news-thumb"}\n							{if news_image==""}\n								<div class="news-excerpt noimg">\n								{if:else}\n							<div class="news-excerpt">\n							{/if}\n								<h2><a href="{page_url}">{title}</a></h2>\n								<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n								<p>{news_snippet}</p>\n								<p class="more"><a href="{page_url}">Read more</a></p>\n							</div>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					<div id="newsRightList">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n<div id="divRss"></div>\n\n\n\n\n\n\n\n\n\n\n					</div>\n					{/if}\n			\n				\n					\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script>\n<script type="text/javascript" src="/resource/js/vendor/FeedEk.js"></script>\n \n<script type="text/javascript" >\n$(document).ready(function(){\n  \n  $(''#txtUrl'').val(''http://rss.cnn.com/rss/edition.rss'');\n  $(''#txtCount'').val(''5'');\n  $(''#chkDate'').attr(''checked'',''checked'');\n  $(''#chkDesc'').attr(''checked'',''checked'');\n  \n  \n   $(''#divRss'').FeedEk({\n   FeedUrl : ''http://www.moodiereport.com/rss.php'',\n   MaxCount : 5,\n   ShowDesc : true,\n   ShowPubDate: true\n  });\n  \n});\n\nfunction OpenBox()\n{\n$(''#divSrc'').toggle();\n}\nfunction changeFeedUrl()\n{\nvar cnt= 5;\nvar showDate=new Boolean();\nshowDate=true;\n\nvar showDescription=new Boolean();\nshowDescription=true;\n\nif($(''#txtCount'').val()!="") cnt=parseInt($(''#txtCount'').val());\nif (! $(''#chkDate'').attr(''checked'')) showDate=false;\nif (! $(''#chkDesc'').attr(''checked'')) showDescription=false;\n\n $(''#divRss'').FeedEk({\n   FeedUrl : $(''#txtUrl'').val(),\n   MaxCount : cnt,\n   ShowDesc : showDescription,\n   ShowPubDate: showDate\n  });\n}\n</script>\n\n\n\n	\n</body>\n</html>'),
(75, 5, 'exp_templates', 'template_data', 1357814712, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				\n					\n				\n					\n					\n					{if segment_2}\n					<div id="newsLeftSingle">\n						<h1>News &amp; Updates</h1>\n						<div class="news-snippet">\n						<h2>{title}</h2>\n						<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n						{exp:ce_img:single src="{news_image}" width="420"}\n						{news_story}\n						{if segment_2}<p class="back"><a href="/news">Back to all news</a></p>{/if}\n						</div>\n						<span class="share">Share this story via:</span>{exp:hj_social_bookmarks sites="Facebook|Twitter|Email" entry_id="8" show_hyperlink_text="right" class="social"}\n					</div>\n					<div id="newsRightSingle">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n						\n<div id="divRss"></div>\n\n\n\n\n					</div>\n					{/if}\n					\n					\n					\n{/exp:channel:entries}\n\n				\n				\n				\n					\n					{if segment_2 == ""}\n					<div id="newsLeftList">\n						<h1>News &amp; Updates</h1>\n					{exp:channel:entries channel="news" dynamic="off"}\n					\n						<div class="news-snippet">\n							\n							{exp:ce_img:single src="{news_image}" width="195" class="news-thumb"}\n							{if news_image==""}\n								<div class="news-excerpt noimg">\n								{if:else}\n							<div class="news-excerpt">\n							{/if}\n								<h2><a href="{page_url}">{title}</a></h2>\n								<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n								<p>{news_snippet}</p>\n								<p class="more"><a href="{page_url}">Read more</a></p>\n							</div>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					<div id="newsRightList">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n<div id="divRss"></div>\n\n\n\n\n\n\n\n\n\n\n					</div>\n					{/if}\n			\n				\n					\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<script type="text/javascript" src="/resource/js/vendor/FeedEk.js"></script>\n \n<script type="text/javascript" >\n$(document).ready(function(){\n  \n  $(''#txtUrl'').val(''http://rss.cnn.com/rss/edition.rss'');\n  $(''#txtCount'').val(''5'');\n  $(''#chkDate'').attr(''checked'',''checked'');\n  $(''#chkDesc'').attr(''checked'',''checked'');\n  \n  \n   $(''#divRss'').FeedEk({\n   FeedUrl : ''http://www.moodiereport.com/rss.php'',\n   MaxCount : 5,\n   ShowDesc : true,\n   ShowPubDate: true\n  });\n  \n});\n\nfunction OpenBox()\n{\n$(''#divSrc'').toggle();\n}\nfunction changeFeedUrl()\n{\nvar cnt= 5;\nvar showDate=new Boolean();\nshowDate=true;\n\nvar showDescription=new Boolean();\nshowDescription=true;\n\nif($(''#txtCount'').val()!="") cnt=parseInt($(''#txtCount'').val());\nif (! $(''#chkDate'').attr(''checked'')) showDate=false;\nif (! $(''#chkDesc'').attr(''checked'')) showDescription=false;\n\n $(''#divRss'').FeedEk({\n   FeedUrl : $(''#txtUrl'').val(),\n   MaxCount : cnt,\n   ShowDesc : showDescription,\n   ShowPubDate: showDate\n  });\n}\n</script>\n\n\n\n	\n</body>\n</html>'),
(76, 5, 'exp_templates', 'template_data', 1357814756, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				\n					\n				\n					\n					\n					{if segment_2}\n					<div id="newsLeftSingle">\n						<h1>News &amp; Updates</h1>\n						<div class="news-snippet">\n						<h2>{title}</h2>\n						<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n						{exp:ce_img:single src="{news_image}" width="420"}\n						{news_story}\n						{if segment_2}<p class="back"><a href="/news">Back to all news</a></p>{/if}\n						</div>\n						<span class="share">Share this story via:</span>{exp:hj_social_bookmarks sites="Facebook|Twitter|Email" entry_id="8" show_hyperlink_text="right" class="social"}\n					</div>\n					<div id="newsRightSingle">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n						\n<div id="divRss"></div>\n\n\n\n\n					</div>\n					{/if}\n					\n					\n					\n{/exp:channel:entries}\n\n				\n				\n				\n					\n					{if segment_2 == ""}\n					<div id="newsLeftList">\n						<h1>News &amp; Updates</h1>\n					{exp:channel:entries channel="news" dynamic="off"}\n					\n						<div class="news-snippet">\n							\n							{exp:ce_img:single src="{news_image}" width="195" class="news-thumb"}\n							{if news_image==""}\n								<div class="news-excerpt noimg">\n								{if:else}\n							<div class="news-excerpt">\n							{/if}\n								<h2><a href="{page_url}">{title}</a></h2>\n								<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n								<p>{news_snippet}</p>\n								<p class="more"><a href="{page_url}">Read more</a></p>\n							</div>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					<div id="newsRightList">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n<div id="divRss"></div>\n\n\n\n\n\n\n\n\n\n\n					</div>\n					{/if}\n			\n				\n					\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script>\n<script type="text/javascript" src="/resource/js/vendor/FeedEk.js"></script>\n \n<script type="text/javascript" >\n$(document).ready(function(){\n  \n  $(''#txtUrl'').val(''http://rss.cnn.com/rss/edition.rss'');\n  $(''#txtCount'').val(''5'');\n  $(''#chkDate'').attr(''checked'',''checked'');\n  $(''#chkDesc'').attr(''checked'',''checked'');\n  \n  \n   $(''#divRss'').FeedEk({\n   FeedUrl : ''http://www.moodiereport.com/rss.php'',\n   MaxCount : 5,\n   ShowDesc : true,\n   ShowPubDate: true\n  });\n  \n});\n\nfunction OpenBox()\n{\n$(''#divSrc'').toggle();\n}\nfunction changeFeedUrl()\n{\nvar cnt= 5;\nvar showDate=new Boolean();\nshowDate=true;\n\nvar showDescription=new Boolean();\nshowDescription=true;\n\nif($(''#txtCount'').val()!="") cnt=parseInt($(''#txtCount'').val());\nif (! $(''#chkDate'').attr(''checked'')) showDate=false;\nif (! $(''#chkDesc'').attr(''checked'')) showDescription=false;\n\n $(''#divRss'').FeedEk({\n   FeedUrl : $(''#txtUrl'').val(),\n   MaxCount : cnt,\n   ShowDesc : showDescription,\n   ShowPubDate: showDate\n  });\n}\n</script>\n\n\n\n	\n</body>\n</html>'),
(77, 5, 'exp_templates', 'template_data', 1357814788, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				\n					\n				\n					\n					\n					{if segment_2}\n					<div id="newsLeftSingle">\n						<h1>News &amp; Updates</h1>\n						<div class="news-snippet">\n						<h2>{title}</h2>\n						<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n						{exp:ce_img:single src="{news_image}" width="420"}\n						{news_story}\n						{if segment_2}<p class="back"><a href="/news">Back to all news</a></p>{/if}\n						</div>\n						<span class="share">Share this story via:</span>{exp:hj_social_bookmarks sites="Facebook|Twitter|Email" entry_id="8" show_hyperlink_text="right" class="social"}\n					</div>\n					<div id="newsRightSingle">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n						\n<div id="divRss"></div>\n\n\n\n\n					</div>\n					{/if}\n					\n					\n					\n{/exp:channel:entries}\n\n				\n				\n				\n					\n					{if segment_2 == ""}\n					<div id="newsLeftList">\n						<h1>News &amp; Updates</h1>\n					{exp:channel:entries channel="news" dynamic="off"}\n					\n						<div class="news-snippet">\n							\n							{exp:ce_img:single src="{news_image}" width="195" class="news-thumb"}\n							{if news_image==""}\n								<div class="news-excerpt noimg">\n								{if:else}\n							<div class="news-excerpt">\n							{/if}\n								<h2><a href="{page_url}">{title}</a></h2>\n								<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n								<p>{news_snippet}</p>\n								<p class="more"><a href="{page_url}">Read more</a></p>\n							</div>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					<div id="newsRightList">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n<div id="divRss"></div>\n\n\n\n\n\n\n\n\n\n\n					</div>\n					{/if}\n			\n				\n					\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<!-- <script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script> -->\n<script type="text/javascript" src="/resource/js/vendor/FeedEk.js"></script>\n \n<script type="text/javascript" >\n$(document).ready(function(){\n  \n  $(''#txtUrl'').val(''http://rss.cnn.com/rss/edition.rss'');\n  $(''#txtCount'').val(''5'');\n  $(''#chkDate'').attr(''checked'',''checked'');\n  $(''#chkDesc'').attr(''checked'',''checked'');\n  \n  \n   $(''#divRss'').FeedEk({\n   FeedUrl : ''http://www.moodiereport.com/rss.php'',\n   MaxCount : 5,\n   ShowDesc : true,\n   ShowPubDate: true\n  });\n  \n});\n\nfunction OpenBox()\n{\n$(''#divSrc'').toggle();\n}\nfunction changeFeedUrl()\n{\nvar cnt= 5;\nvar showDate=new Boolean();\nshowDate=true;\n\nvar showDescription=new Boolean();\nshowDescription=true;\n\nif($(''#txtCount'').val()!="") cnt=parseInt($(''#txtCount'').val());\nif (! $(''#chkDate'').attr(''checked'')) showDate=false;\nif (! $(''#chkDesc'').attr(''checked'')) showDescription=false;\n\n $(''#divRss'').FeedEk({\n   FeedUrl : $(''#txtUrl'').val(),\n   MaxCount : cnt,\n   ShowDesc : showDescription,\n   ShowPubDate: showDate\n  });\n}\n</script>\n\n\n\n	\n</body>\n</html>'),
(78, 9, 'exp_templates', 'template_data', 1357817597, 1, '{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		<section id="content" class="clearfix">					\n			<div id="pageLeft">\n				<h1>Our Distribution and Procurement</h1>\n				<img src="/img/procurement.jpg" width="420" height="290" alt="">\n				\n				<p style="font-weight: bold;">An end-to-end procurement & logistics solution, aimed at driving your range development through sourcing true innovation at competitive prices, supported by an established hassle-free logistics solution.\n</p>\n \n<p>Sourcing products for the inflight & travel sector can be time consuming, requiring specialist category knowledge in both food & non food categories. STM Brands not only provides brands from our existing portfolio partners, but can also source products on your behalf from the UK, Europe & Worldwide.</p>\n\n<p>STM Brands aim to facilitate a consumer-focused range within the sector margin parameters, ensuring your retail proposition has a recognised point of difference.</p>\n			</div>\n			<div id="pageRight">\n				<ul>\n					<li>Established, industry-wide distribution network</li>\n	<li>Effective & efficient logistics, ensuring hassle-free sourcing</li>\n	<li>Competitive, delivered to depot pricing</li>\n	<li>Real innovation through unique sourcing approach</li>\n	<li>Bespoke sourcing capability in support of your Own Brands</li>\n	<li>With the added creative support of our sister agency, <a href="http://www.shoot-the-moon.co.uk">Shoot the Moon</a></li>\n				</ul>\n			</div>\n		</section>\n	\n	</div>\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(79, 10, 'exp_templates', 'template_data', 1357817815, 1, '{sn_doc_head}\n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n\n	<div id="wrap" class="clearfix">\n	<section id="content" class="clearfix">\n		<div id="catOverview">\n\n{exp:channel:entries channel="generic" limit="1" entry_id="29"}\n\n			<h1>{title}</h1>\n			{body_text}\n{/exp:channel:entries}\n			<div id="cta">\n				<h3>Looking for brand exposure or need some new ideas?</h3>\n				<p>Give us a call on 0161 205 3311</p>\n				<p>Email: <a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\n			</div>\n		</div>\n		<ul id="catBrands aboutUs">\n			<li>\n				<h4>Janice Hockenhull</h4>\n				<a href="#"><img src="/img/staff/janice.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Phil Marshall</h4>\n				<a href="#"><img src="/img/staff/phil-marshall.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li>\n				<h4>Oliver Zebedee-Howard</h4>\n				<a href="#"><img src="/img/staff/oliver.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Alistair Stirling</h4>\n				<a href="#"><img src="/img/staff/alistairstirling.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n		</ul>\n	</section>\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(80, 10, 'exp_templates', 'template_data', 1357817887, 1, '{sn_doc_head}\n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n\n	<div id="wrap" class="clearfix">\n	<section id="content" class="clearfix">\n		<div id="catOverview">\n\n{exp:channel:entries channel="generic" limit="1" entry_id="29"}\n\n			<h1>{title}</h1>\n			{body_text}\n{/exp:channel:entries}\n			<div id="cta">\n				<h3>Looking for brand exposure or need some new ideas?</h3>\n				<p>Give us a call on 0161 205 3311</p>\n				<p>Email: <a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\n			</div>\n		</div>\n		<ul id="catBrands" class="aboutUs">\n			<li>\n				<h4>Janice Hockenhull</h4>\n				<a href="#"><img src="/img/staff/janice.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Phil Marshall</h4>\n				<a href="#"><img src="/img/staff/phil-marshall.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li>\n				<h4>Oliver Zebedee-Howard</h4>\n				<a href="#"><img src="/img/staff/oliver.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Alistair Stirling</h4>\n				<a href="#"><img src="/img/staff/alistairstirling.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n		</ul>\n	</section>\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(81, 10, 'exp_templates', 'template_data', 1357817991, 1, '{sn_doc_head}\n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n\n	<div id="wrap" class="clearfix">\n	<section id="content" class="clearfix">\n		<div id="catOverview">\n\n{exp:channel:entries channel="generic" limit="1" entry_id="29"}\n\n			<h1>{title}</h1>\n			{body_text}\n{/exp:channel:entries}\n			<div id="cta">\n				<h3>Looking for brand exposure or need some new ideas?</h3>\n				<p>Give us a call on 0161 205 3311</p>\n				<p>Email: <a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\n			</div>\n		</div>\n		<ul id="catBrands" class="aboutUs">\n			<li>\n				<h4>Janice Hockenhull</h4>\n				<a href="#"><img src="/img/staff/janice.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Phil Marshall</h4>\n				<a href="#"><img src="/img/staff/phil-marshall.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Alistair Stirling</h4>\n				<a href="#"><img src="/img/staff/alistairstirling.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li>\n				<h4>Oliver Zebedee-Howard</h4>\n				<a href="#"><img src="/img/staff/oliver.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n		</ul>\n	</section>\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(82, 10, 'exp_templates', 'template_data', 1357818013, 1, '{sn_doc_head}\n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n\n	<div id="wrap" class="clearfix">\n	<section id="content" class="clearfix">\n		<div id="catOverview">\n\n{exp:channel:entries channel="generic" limit="1" entry_id="29"}\n\n			<h1>{title}</h1>\n			{body_text}\n{/exp:channel:entries}\n			<div id="cta">\n				<h3>Looking for brand exposure or need some new ideas?</h3>\n				<p>Give us a call on 0161 205 3311</p>\n				<p>Email: <a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\n			</div>\n		</div>\n		<ul id="catBrands" class="aboutUs">\n			<li>\n				<h4>Janice Hockenhull</h4>\n				<a href="#"><img src="/img/staff/janice.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Phil Marshall</h4>\n				<a href="#"><img src="/img/staff/phil-marshall.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li>\n				<h4>Alistair Stirling</h4>\n				<a href="#"><img src="/img/staff/alistairstirling.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Oliver Zebedee-Howard</h4>\n				<a href="#"><img src="/img/staff/oliver.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n		</ul>\n	</section>\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(83, 7, 'exp_templates', 'template_data', 1358172734, 1, '{sn_doc_head}        \n</head>\n\n<body class="home no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix forceHeight">\n				\n				<h1>Form submission successful</h1>\n				<p>A member of our team will be in touch soon.</p>\n				\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n'),
(84, 9, 'exp_templates', 'template_data', 1358186452, 1, '{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		<section id="content" class="clearfix">					\n			<div id="pageLeft">\n				<h1>Our Distribution and Procurement</h1>\n				<img src="/img/procurement.jpg" width="420" height="290" alt="">\n				\n				<p style="font-weight: bold;">An end-to-end procurement & logistics solution, aimed at driving your range development through sourcing true innovation at competitive prices, supported by an established hassle-free logistics solution.\n</p>\n \n<p>Sourcing products for the inflight & travel sector can be time consuming, requiring specialist category knowledge in both food & non food categories. STM Brands not only provides brands from our existing portfolio partners, but can also source products on your behalf from the UK, Europe & Worldwide.</p>\n\n<p>STM Brands aim to facilitate a consumer-focused range within the sector margin parameters, ensuring your retail proposition has a recognised point of difference.</p>\n			</div>\n			<div id="pageRight">\n				<ul>\n					<li>Established, industry-wide distribution network</li>\n	<li>Effective & efficient logistics, ensuring hassle-free sourcing</li>\n	<li>Competitive, delivered to depot pricing</li>\n	<li>Real innovation through unique sourcing approach</li>\n	<li>Bespoke sourcing capability in support of your Own Brands</li>\n	<li>With the added creative support of our sister agency, <a href="http://www.shoot-the-moon.co.uk" target="_blank">Shoot the Moon</a></li>\n				</ul>\n			</div>\n		</section>\n	\n	</div>\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(85, 1, 'exp_templates', 'template_data', 1360160457, 1, '{exp:channel:entries url_title="stm-brands"}\n\n{sn_doc_head}        \n</head>\n\n<body class="home">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		{embed="site/category_carousel"}\n		\n		<section id="content" class="clearfix">\n			<div id="editorial">\n				<h1>{homepage_copy}</h1>\n				<ul>\n					<li>\n						<h2>What we do</h2>\n							<p>{snippet_copy}{value_copy}{/snippet_copy}</p>\n					</li>\n					<li>\n						<h2>Who we are</h2>\n						<p>{snippet_copy}{authentic_copy}{/snippet_copy}</p>\n					</li>\n					<li class="last-item">\n						<h2>How we do it</h2>\n						<p>{snippet_copy}{service_copy}{/snippet_copy}</p>\n					</li>\n				</ul>\n			</div>\n			<ul id="sidePanel">\n				<li class="banner">\n					<a href="/our-distribution-and-procurement"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/authentic_small.jpg" />\n					<h3>New for 2013</h3>\n					<p>Cocktails in a can...</p>\n					<a href="/food-and-drink/the-authentic-cocktail/"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/glamorous_small.jpg" />\n					<h3>Onboard Now and</h3>\n					<p>Sales forecast doubled for Winter 2012</p>\n					<a href="/accessories/glamorous-bra-straps/"></a>\n				</li>\n			</ul>\n		</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n\n{/exp:channel:entries}');
INSERT INTO `exp_revision_tracker` (`tracker_id`, `item_id`, `item_table`, `item_field`, `item_date`, `item_author_id`, `item_data`) VALUES
(86, 1, 'exp_templates', 'template_data', 1360160492, 1, '{exp:channel:entries url_title="stm-brands"}\n\n{sn_doc_head}        \n</head>\n\n<body class="home">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		{embed="site/category_carousel"}\n		\n		<section id="content" class="clearfix">\n			<div id="editorial">\n				<h1>{homepage_copy}</h1>\n				<ul>\n					<li>\n						<h2>What we do</h2>\n							<p>{snippet_copy}{value_copy}{/snippet_copy}</p>\n					</li>\n					<li>\n						<h2>Who we are</h2>\n						<p>{snippet_copy}{authentic_copy}{/snippet_copy}</p>\n					</li>\n					<li class="last-item">\n						<h2>How we do it</h2>\n						<p>{snippet_copy}{service_copy}{/snippet_copy}</p>\n					</li>\n				</ul>\n			</div>\n			<ul id="sidePanel">\n				<li class="banner">\n					<a href="/our-distribution-and-procurement"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/authentic_small.jpg" />\n					<h3>New for 2013</h3>\n					<p>Cocktails in a can...</p>\n					<a href="/food-and-drink/the-authentic-cocktail/"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/glamorous_small.jpg" />\n					<h3>Onboard Now...</h3>\n					<p>...and Sales forecast doubled for Winter 2012</p>\n					<a href="/accessories/glamorous-bra-straps/"></a>\n				</li>\n			</ul>\n		</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n\n{/exp:channel:entries}'),
(87, 2, 'exp_templates', 'template_data', 1363369290, 1, '{exp:channel:entries}\r\n\r\n{sn_doc_head}\r\n</head>\r\n\r\n<body>\r\n	{sn_chrome_frame}\r\n\r\n	{sn_header}\r\n\r\n	<div id="wrap" class="clearfix">\r\n	<section id="content" class="clearfix">\r\n		<div id="catOverview">\r\n			<h1>{title}</h1>\r\n			{images}\r\n			{exp:ce_img:single src="{main_image}" width="420" height="420"}\r\n			{/images}\r\n			<p class="catText">{description}</p>\r\n{/exp:channel:entries}\r\n\r\n			<div id="cta">\r\n				<h3>Looking for brand exposure or need some new ideas?</h3>\r\n				<p>Give us a call on 0161 205 3311</p>\r\n				<p>Email: <a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\r\n			</div>\r\n		</div>\r\n		<ul id="catBrands">\r\n			<?php $i = 1; ?>\r\n			{exp:channel:entries entry_id="{structure:child_ids}" fixed_order="{structure:child_ids}"}\r\n			<li <?php if ($i == 2) echo ''class="last-item"''; ?>><h4>{title}</h4><a href="{page_uri}">{brand}{exp:ce_img:single src="{brand_image}" width="195" height="195"}{/brand}</a></li>\r\n			<?php \r\n				if ($i == 2) {\r\n					$i = 1;\r\n				} else {\r\n					$i++;\r\n				}\r\n			?>\r\n			{/exp:channel:entries}\r\n		</ul>\r\n	</section>\r\n	</div>\r\n	\r\n	{sn_footer}\r\n	{sn_base_scripts}\r\n	\r\n</body>\r\n</html>\r\n'),
(88, 12, 'exp_templates', 'template_data', 1363369290, 1, '<html xmlns="http://www.w3.org/1999/xhtml" >\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n<title>FeedEk Jquery RSS/ATOM Feed Plugin Demo</title>\n\n<script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script>\n<script type="text/javascript" src="/resource/js/vendor/FeedEk.js"></script>\n \n<script type="text/javascript" >\n$(document).ready(function(){\n  \n  $(''#txtUrl'').val(''http://rss.cnn.com/rss/edition.rss'');\n  $(''#txtCount'').val(''5'');\n  $(''#chkDate'').attr(''checked'',''checked'');\n  $(''#chkDesc'').attr(''checked'',''checked'');\n  \n  \n   $(''#divRss'').FeedEk({\n   FeedUrl : ''http://www.moodiereport.com/rss.php'',\n   MaxCount : 5,\n   ShowDesc : true,\n   ShowPubDate: true\n  });\n  \n});\n\nfunction OpenBox()\n{\n$(''#divSrc'').toggle();\n}\nfunction changeFeedUrl()\n{\nvar cnt= 5;\nvar showDate=new Boolean();\nshowDate=true;\n\nvar showDescription=new Boolean();\nshowDescription=true;\n\nif($(''#txtCount'').val()!="") cnt=parseInt($(''#txtCount'').val());\nif (! $(''#chkDate'').attr(''checked'')) showDate=false;\nif (! $(''#chkDesc'').attr(''checked'')) showDescription=false;\n\n $(''#divRss'').FeedEk({\n   FeedUrl : $(''#txtUrl'').val(),\n   MaxCount : cnt,\n   ShowDesc : showDescription,\n   ShowPubDate: showDate\n  });\n}\n</script>\n\n\n</head>\n<body>\nTEST\n<div id="divRss"></div>\n</body>\n\n</html>\n'),
(89, 5, 'exp_templates', 'template_data', 1363369290, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				\n					\n				\n					\n					\n					{if segment_2}\n					<div id="newsLeftSingle">\n						<h1>News &amp; Updates</h1>\n						<div class="news-snippet">\n						<h2>{title}</h2>\n						<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n						{exp:ce_img:single src="{news_image}" width="420"}\n						{news_story}\n						{if segment_2}<p class="back"><a href="/news">Back to all news</a></p>{/if}\n						</div>\n						<span class="share">Share this story via:</span>{exp:hj_social_bookmarks sites="Facebook|Twitter|Email" entry_id="13" show_hyperlink_text="right" class="social"}\n					</div>\n					<div id="newsRightSingle">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n						\n<div id="divRss"></div>\n\n\n\n\n					</div>\n					{/if}\n					\n					\n					\n{/exp:channel:entries}\n\n				\n				\n				\n					\n					{if segment_2 == ""}\n					<div id="newsLeftList">\n						<h1>News &amp; Updates</h1>\n					{exp:channel:entries channel="news" dynamic="off"}\n					\n						<div class="news-snippet">\n							\n							{exp:ce_img:single src="{news_image}" width="195" class="news-thumb"}\n							{if news_image==""}\n								<div class="news-excerpt noimg">\n								{if:else}\n							<div class="news-excerpt">\n							{/if}\n								<h2><a href="{page_url}">{title}</a></h2>\n								<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n								<p>{news_snippet}</p>\n								<p class="more"><a href="{page_url}">Read more</a></p>\n							</div>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					<div id="newsRightList">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n<div id="divRss"></div>\n\n\n\n\n\n\n\n\n\n\n					</div>\n					{/if}\n			\n				\n					\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<!-- <script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script> -->\n<script type="text/javascript" src="/resource/js/vendor/FeedEk.js"></script>\n \n<script type="text/javascript" >\n$(document).ready(function(){\n  \n  $(''#txtUrl'').val(''http://rss.cnn.com/rss/edition.rss'');\n  $(''#txtCount'').val(''5'');\n  $(''#chkDate'').attr(''checked'',''checked'');\n  $(''#chkDesc'').attr(''checked'',''checked'');\n  \n  \n   $(''#divRss'').FeedEk({\n   FeedUrl : ''http://www.moodiereport.com/rss.php'',\n   MaxCount : 5,\n   ShowDesc : true,\n   ShowPubDate: true\n  });\n  \n});\n\nfunction OpenBox()\n{\n$(''#divSrc'').toggle();\n}\nfunction changeFeedUrl()\n{\nvar cnt= 5;\nvar showDate=new Boolean();\nshowDate=true;\n\nvar showDescription=new Boolean();\nshowDescription=true;\n\nif($(''#txtCount'').val()!="") cnt=parseInt($(''#txtCount'').val());\nif (! $(''#chkDate'').attr(''checked'')) showDate=false;\nif (! $(''#chkDesc'').attr(''checked'')) showDescription=false;\n\n $(''#divRss'').FeedEk({\n   FeedUrl : $(''#txtUrl'').val(),\n   MaxCount : cnt,\n   ShowDesc : showDescription,\n   ShowPubDate: showDate\n  });\n}\n</script>\n\n\n\n	\n</body>\n</html>'),
(90, 10, 'exp_templates', 'template_data', 1363369290, 1, '{sn_doc_head}\n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n\n	<div id="wrap" class="clearfix">\n	<section id="content" class="clearfix">\n		<div id="catOverview">\n\n{exp:channel:entries channel="generic" limit="1" entry_id="29"}\n\n			<h1>{title}</h1>\n			{body_text}\n{/exp:channel:entries}\n			<div id="cta">\n				<h3>Looking for brand exposure or need some new ideas?</h3>\n				<p>Give us a call on 0161 205 3311</p>\n				<p>Email: <a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\n			</div>\n		</div>\n		<ul id="catBrands" class="aboutUs">\n			<li>\n				<h4>Janice Hockenhull</h4>\n				<a href="#"><img src="/img/staff/janice.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Phil Marshall</h4>\n				<a href="#"><img src="/img/staff/phil-marshall.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li>\n				<h4>Alistair Stirling</h4>\n				<a href="#"><img src="/img/staff/alistairstirling.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Oliver Zebedee-Howard</h4>\n				<a href="#"><img src="/img/staff/oliver.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li>\n				<h4>Vicky Carson</h4>\n				<a href="#"><img src="/img/staff/vickycarson.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n		</ul>\n	</section>\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>'),
(91, 6, 'exp_templates', 'template_data', 1363369290, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="home no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				<h1>{title}</h1>\n				\n				{exp:freeform:form collection="Contact Form" form:class="contact-form" required="forename|surname|email|message" return="/thank-you" recipients="yes" recipient_limit="1" recipient_template="contact_us"}\n					\n					<input type="hidden" name="recipient_email" value="janice@shoot-the-moon.co.uk">\n\n					<div class="row">\n						<label>First Name*</label>\n						<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n					</div>\n				\n					<div class="row">\n						<label>Surname*</label>\n						<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n					</div>\n				\n					<div class="row">\n						<label>Email Address*</label>\n						<input type="text" name="email" value="" class="required" placeholder="Your email address">\n					</div>\n				\n					<div class="row"><label>Message*</label>\n						<textarea name="message" class="required" placeholder="Please leave a short message and we''ll be in-touch"></textarea>\n					</div>\n				\n					<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n				\n				{/exp:freeform:form}\n				\n				\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n{/exp:channel:entries}\n'),
(92, 4, 'exp_templates', 'template_data', 1363369290, 1, '<section id="categories">\n	<!--p class="scroll left"><a href="" class="ir">Scroll left</a></p-->\n	<ul class="clearfix">\n		\n		{exp:channel:entries channel="product-category" orderby="entry_date" sort="asc"}\n		<li>\n			<h2>{title}</h2>\n			<p><a href="{page_uri}">{images}{exp:ce_img:single src="{home_image}" width="280" height="227"}{/images}</a></p>\n		</li>\n		{/exp:channel:entries}\n	</ul>\n	<!--p class="scroll right"><a href="" class="ir">Scroll right</a></p-->\n</section>\n'),
(93, 3, 'exp_templates', 'template_data', 1363369290, 1, '{exp:channel:entries}\r\n\r\n{sn_doc_head}        \r\n</head>\r\n\r\n<body class="home">\r\n	{if segment_2 == "tipsy-feet"}\r\n	<div id="video-box" style="display:none;">\r\n		<video id="my_video_1" class="video-js vjs-default-skin" controls\r\n		  preload="auto" width="480" height="270" poster=""\r\n		  data-setup="{}">\r\n		  <source src="/video/Tipsyfeet_Foldable_Shoes_And_Matching_Pouch.mp4" type=''video/mp4''>\r\n		  <source src="/video/Tipsyfeet_Foldable_Shoes_And_Matching_Pouch.webm" type=''video/webm''>\r\n		</video>\r\n	</div>\r\n	{/if}\r\n	\r\n	{sn_chrome_frame}\r\n\r\n	{sn_header}\r\n	<div id="wrap" class="clearfix">\r\n		<section id="content" class="clearfix">\r\n			\r\n			<div id="catOverview">\r\n				<h1>{title}</h1>\r\n				<div id="main-image">\r\n					{if brand}{brand}<img src="{brand_image}" width="420" height="420" class="brand-shot">{/brand}{/if}\r\n					{products}\r\n					{exp:ce_img:single src="{product_image}" width="420" height="420" class="product{row_count}"}\r\n					{/products}\r\n				</div>\r\n			</div>\r\n			\r\n			<div id="prodDetails">\r\n				<ul class="clearfix" id="product-nav">\r\n					<?php $i = 1; ?>\r\n					{products}\r\n					<li <?php if ($i == 4) echo ''class="last-item"''; ?>><a href="" class="product{row_count}">{exp:ce_img:single src="{product_image}" width="80" height="80"}</a></li>\r\n					<?php \r\n						if ($i == 4) {\r\n							$i = 1;\r\n						} else {\r\n							$i++;\r\n						}\r\n					?>\r\n					{/products}\r\n					<li class="selected"><a href="" class="brand-shot">{brand}{exp:ce_img:single src="{brand_image}" width="80" height="80"}{/brand}</a></li>\r\n				</ul>\r\n				\r\n				<div id="product-content">\r\n					{brand}\r\n					<div class="brand-shot">\r\n						<h2>{title}</h2>\r\n						<p>{brand_description}</p>\r\n						{if segment_2 == "tipsy-feet"}\r\n						<p><a href="#video-box" class="fancybox">Play Tipsy Feet Video</a></p>\r\n						{/if}\r\n					</div>\r\n					{/brand}\r\n					{products}\r\n					<div class="product{row_count}">\r\n						<h2>{product_title}</h2>\r\n						<p>{product_description}</p>\r\n					</div>\r\n					{/products}\r\n				</div>\r\n\r\n				\r\n				{embed="site/other-brands" entry_ids="{exp:stm_helpers:get_siblings entry=''{structure:page:entry_id}'' parent=''{structure:parent:entry_id}''}" entry_id="{structure:page:entry_id} parent="{structure:parent:entry_id}"}\r\n				\r\n{/exp:channel:entries}\r\n				\r\n			</div>\r\n			\r\n		</section>\r\n	</div>\r\n	\r\n	\r\n	{sn_footer}\r\n	{sn_base_scripts}\r\n</body>\r\n</html>\r\n'),
(94, 11, 'exp_templates', 'template_data', 1363369290, 1, '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				\n					\n				\n					\n					\n					{if segment_2}\n					<div id="newsLeftSingle">\n						<h1>News &amp; updates</h1>\n						<div class="news-snippet">\n						<h2>{title}</h2>\n						<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n						{exp:ce_img:single src="{news_image}" width="420"}\n						{news_story}\n						{if segment_2}<p class="back"><a href="/news">Back to all news</a></p>{/if}\n						</div>\n						<ul id="social">\n							<li><span><img src="/img/facebook.jpg"></span><a href="#">Share story on Facebook</a></li>\n							<li><span><img src="/img/twitter.jpg"></span><a href="#">Share story on Twitter</a></li>\n							<li><span><img src="/img/email.jpg"></span><a href="#">Email story to a friend</a></li>\n						</ul>\n					</div>\n					<div id="newsRightSingle">\n						<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news-events/archives/"}\n							<!--<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>-->\n						</ul>\n						\n						<h1>Moodie Report</h1>\n					</div>\n					{/if}\n					\n					\n					\n{/exp:channel:entries}\n\n				\n				\n				\n					\n					{if segment_2 == ""}\n					<div id="newsLeftList">\n						<h1>News &amp; updates</h1>\n					{exp:channel:entries channel="news" dynamic="off"}\n					\n						<div class="news-snippet">\n							<div class="news-thumb">\n								{exp:ce_img:single src="{news_image}" width="195"}\n							</div>\n							<div class="news-excerpt">\n								<h2><a href="{page_url}">{title}</a></h2>\n								<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n								<p>{news_snippet}</p>\n								<p class="more"><a href="{page_url}">Read more</a></p>\n							</div>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					<div id="newsRightList">\n						<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news-events/archives/"}\n							<!--<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>-->\n						</ul>\n						\n						<h1>Moodie Report</h1>\n					{/if}\n			\n				\n					\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n'),
(95, 8, 'exp_templates', 'template_data', 1363369290, 1, '<h3>Also available</h3>\r\n<ul class="clearfix">\r\n	{if embed:entry_ids != ""}\r\n	{exp:channel:entries entry_id="{embed:entry_ids}" channel="product-brand" dynamic="no" orderby="random" limit="4"}\r\n	<li{if {count} == 4} class="last-item"{/if}><a href="{page_uri}">{brand}{exp:ce_img:single src="{brand_image}" width="80" height="80"}{/brand}</a></li>\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	{if embed:entry_ids == ""}\r\n	{exp:channel:entries entry_id="not {embed:entry_id}" channel="product-brand" dynamic="no" orderby="random" limit="4"}\r\n	<li{if {count} == 4} class="last-item"{/if}><a href="{page_uri}">{brand}{exp:ce_img:single src="{brand_image}" width="80" height="80"}{/brand}</a></li>\r\n	{/exp:channel:entries}\r\n	{/if}\r\n</ul>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_tools`
--

CREATE TABLE IF NOT EXISTS `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_rte_tools`
--

INSERT INTO `exp_rte_tools` (`tool_id`, `name`, `class`, `enabled`) VALUES
(1, 'Blockquote', 'Blockquote_rte', 'y'),
(2, 'Bold', 'Bold_rte', 'y'),
(3, 'Headings', 'Headings_rte', 'y'),
(4, 'Image', 'Image_rte', 'y'),
(5, 'Italic', 'Italic_rte', 'y'),
(6, 'Link', 'Link_rte', 'y'),
(7, 'Ordered List', 'Ordered_list_rte', 'y'),
(8, 'Underline', 'Underline_rte', 'y'),
(9, 'Unordered List', 'Unordered_list_rte', 'y'),
(10, 'View Source', 'View_source_rte', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_toolsets`
--

CREATE TABLE IF NOT EXISTS `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_rte_toolsets`
--

INSERT INTO `exp_rte_toolsets` (`toolset_id`, `member_id`, `name`, `tools`, `enabled`) VALUES
(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_search`
--

CREATE TABLE IF NOT EXISTS `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_search_log`
--

CREATE TABLE IF NOT EXISTS `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_security_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24830 ;

--
-- Dumping data for table `exp_security_hashes`
--

INSERT INTO `exp_security_hashes` (`hash_id`, `date`, `ip_address`, `hash`) VALUES
(24821, 1374765944, '109.111.197.225', '5aa28c0f34cbc04f7e6668c79c360dbaeb55e273'),
(24822, 1374765952, '109.111.197.225', 'c5f5f9484d09711e988eb6fd02b596a6c88f551a'),
(24823, 1374765952, '109.111.197.225', 'ed61ab5d47e293a975c529cd4f058e6acba23e73'),
(24824, 1374765978, '109.111.197.225', '6f78e5f9efbbceb9e9b3dffff50fad9f306d81cf'),
(24825, 1374765983, '109.111.197.225', 'c8b6ef786e618bf8a2803e23e256dfca18e1a3b6'),
(24826, 1374765985, '109.111.197.225', '819e2cd0fe793a04ff2f185756e8c1feeacc9b25'),
(24827, 1374765988, '109.111.197.225', '133c5584ab27d0be8cbc5f7c33665579a170f9e3'),
(24828, 1374765995, '109.111.197.225', 'a7ad792e3afef2e9d4cb3bbb84d44fd3fe8f3e1f'),
(24829, 1374765995, '109.111.197.225', '50ceab51779bb94563db86d004bb4314c510c22c');

-- --------------------------------------------------------

--
-- Table structure for table `exp_sessions`
--

CREATE TABLE IF NOT EXISTS `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_sessions`
--

INSERT INTO `exp_sessions` (`session_id`, `member_id`, `admin_sess`, `ip_address`, `user_agent`, `last_activity`) VALUES
('edc3514265a70ee41b91eba0a0f6529087ac027a', 1, 1, '109.111.197.225', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36', 1374765997);

-- --------------------------------------------------------

--
-- Table structure for table `exp_sites`
--

CREATE TABLE IF NOT EXISTS `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` longtext,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_sites`
--

INSERT INTO `exp_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`, `site_pages`) VALUES
(1, 'STM Brands', 'default_site', NULL, 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MzQ6Imh0dHA6Ly9zdG1icmFuZHMuc3RtcHJldmlldy5jby51ay8iO3M6MTY6InRoZW1lX2ZvbGRlcl91cmwiO3M6NDE6Imh0dHA6Ly9zdG1icmFuZHMuc3RtcHJldmlldy5jby51ay90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6Mjc6Im1hcnRpbkBzaG9vdC10aGUtbW9vbi5jby51ayI7czoxNDoid2VibWFzdGVyX25hbWUiO3M6MDoiIjtzOjIwOiJjaGFubmVsX25vbWVuY2xhdHVyZSI7czo3OiJjaGFubmVsIjtzOjEwOiJtYXhfY2FjaGVzIjtzOjM6IjE1MCI7czoxMToiY2FwdGNoYV91cmwiO3M6NTA6Imh0dHA6Ly9zdG1icmFuZHMuc3RtcHJldmlldy5jby51ay9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NzA6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMvc3RtYnJhbmRzL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czozOiJVVEMiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6Im4iO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czozOiJVVEMiO3M6MTY6ImRlZmF1bHRfc2l0ZV9kc3QiO3M6MToibiI7czoxNToiaG9ub3JfZW50cnlfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6NDI6Imh0dHA6Ly9zdG0tYnJhbmRzLWFsdDo4ODg4L2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo2MToiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG1icmFuZHMvdGhlbWVzLyI7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6NDk6Imh0dHA6Ly9zdG1icmFuZHMuc3RtcHJldmlldy5jby51ay9pbWFnZXMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjY5OiIvdmFyL3d3dy92aG9zdHMvc3RtcHJldmlldy5jby51ay9zdWJkb21haW5zL3N0bWJyYW5kcy9pbWFnZXMvYXZhdGFycy8iO3M6MTY6ImF2YXRhcl9tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE3OiJhdmF0YXJfbWF4X2hlaWdodCI7czozOiIxMDAiO3M6MTM6ImF2YXRhcl9tYXhfa2IiO3M6MjoiNTAiO3M6MTM6ImVuYWJsZV9waG90b3MiO3M6MToibiI7czo5OiJwaG90b191cmwiO3M6NTU6Imh0dHA6Ly9zdG1icmFuZHMuc3RtcHJldmlldy5jby51ay9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NzU6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMvc3RtYnJhbmRzL2ltYWdlcy9tZW1iZXJfcGhvdG9zLyI7czoxNToicGhvdG9fbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNjoicGhvdG9fbWF4X2hlaWdodCI7czozOiIxMDAiO3M6MTI6InBob3RvX21heF9rYiI7czoyOiI1MCI7czoxNjoiYWxsb3dfc2lnbmF0dXJlcyI7czoxOiJ5IjtzOjEzOiJzaWdfbWF4bGVuZ3RoIjtzOjM6IjUwMCI7czoyMToic2lnX2FsbG93X2ltZ19ob3RsaW5rIjtzOjE6Im4iO3M6MjA6InNpZ19hbGxvd19pbWdfdXBsb2FkIjtzOjE6Im4iO3M6MTE6InNpZ19pbWdfdXJsIjtzOjYzOiJodHRwOi8vc3RtYnJhbmRzLnN0bXByZXZpZXcuY28udWsvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTI6InNpZ19pbWdfcGF0aCI7czo4MzoiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG1icmFuZHMvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo3NjoiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG1icmFuZHMvaW1hZ2VzL3BtX2F0dGFjaG1lbnRzLyI7czoyMzoicHJ2X21zZ19tYXhfYXR0YWNobWVudHMiO3M6MToiMyI7czoyMjoicHJ2X21zZ19hdHRhY2hfbWF4c2l6ZSI7czozOiIyNTAiO3M6MjA6InBydl9tc2dfYXR0YWNoX3RvdGFsIjtzOjM6IjEwMCI7czoxOToicHJ2X21zZ19odG1sX2Zvcm1hdCI7czo0OiJzYWZlIjtzOjE4OiJwcnZfbXNnX2F1dG9fbGlua3MiO3M6MToieSI7czoxNzoicHJ2X21zZ19tYXhfY2hhcnMiO3M6NDoiNjAwMCI7czoxOToibWVtYmVybGlzdF9vcmRlcl9ieSI7czoxMToidG90YWxfcG9zdHMiO3M6MjE6Im1lbWJlcmxpc3Rfc29ydF9vcmRlciI7czo0OiJkZXNjIjtzOjIwOiJtZW1iZXJsaXN0X3Jvd19saW1pdCI7czoyOiIyMCI7fQ==', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJ5IjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo2NDoiL3Zhci93d3cvdmhvc3RzL3N0bXByZXZpZXcuY28udWsvc3ViZG9tYWlucy9zdG1icmFuZHMvdGVtcGxhdGVzLyI7fQ==', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YTozOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NzU6Ii9Vc2Vycy9Xb3JrL0RvY3VtZW50cy9Qcm9qZWN0cy9naXRfdmVyc2lvbl9jb250cm9sL3N0bS1icmFuZHMvd3d3L2luZGV4LnBocCI7czozMjoiYTJlYmE4YWQwMjc4NzAwZGQ0ZTljNjQ2MTNlMjJlMmYiO3M6NjM6Ii92YXIvd3d3L3Zob3N0cy9zdG1wcmV2aWV3LmNvLnVrL3N1YmRvbWFpbnMvc3RtYnJhbmRzL2luZGV4LnBocCI7czozMjoiYTJlYmE4YWQwMjc4NzAwZGQ0ZTljNjQ2MTNlMjJlMmYiO30=', 'YToxOntpOjE7YTozOntzOjM6InVybCI7czozNDoiaHR0cDovL3N0bWJyYW5kcy5zdG1wcmV2aWV3LmNvLnVrLyI7czo0OiJ1cmlzIjthOjIwOntpOjE7czoxMToiL3N0bS1icmFuZHMiO2k6NztzOjE2OiIvZm9vZC1hbmQtZHJpbmsvIjtpOjE3O3M6MjU6Ii9mb29kLWFuZC1kcmluay9qYWNrcG90cy8iO2k6MjE7czozMToiL2Zvb2QtYW5kLWRyaW5rL251YmEtY29ja3RhaWxzLyI7aToyMjtzOjM5OiIvZm9vZC1hbmQtZHJpbmsvdGhlLWF1dGhlbnRpYy1jb2NrdGFpbC8iO2k6MjQ7czo0MToiL2Zvb2QtYW5kLWRyaW5rL2hld2J5cy1jaG9jb2xhdGUtYnJvd25pZS8iO2k6ODtzOjExOiIvY29zbWV0aWNzLyI7aToxODtzOjIwOiIvY29zbWV0aWNzL21lLW1lLW1lLyI7aTo5O3M6MTM6Ii9hY2Nlc3Nvcmllcy8iO2k6MTk7czoyNDoiL2FjY2Vzc29yaWVzL3RpcHN5LWZlZXQvIjtpOjIwO3M6MjY6Ii9hY2Nlc3Nvcmllcy9pY2UtZGlhbW9uZHMvIjtpOjIzO3M6MzQ6Ii9hY2Nlc3Nvcmllcy9nbGFtb3JvdXMtYnJhLXN0cmFwcy8iO2k6Mjc7czozNDoiL291ci1kaXN0cmlidXRpb24tYW5kLXByb2N1cmVtZW50LyI7aToxMztzOjY6Ii9uZXdzLyI7aToxNjtzOjE0OiIvZ2V0LWluLXRvdWNoLyI7aToxNDtzOjY2OiIvbmV3cy9zdG0tYnJhbmRzLWFuZC1zaG9vdC10aGUtbW9vbi1jb25maXJtLXN1cHBvcnQtZm9yLWlzcHktMjAxMy8iO2k6Mjg7czoyOToiL25ld3MvYWRkLWEtdG91Y2gtb2YtZ2xhbW91ci8iO2k6Mjk7czoxMDoiL291ci10ZWFtLyI7aTozMDtzOjI2OiIvbmV3cy9wZXJmZWN0LWZvci1wYWNraW5nLyI7aTozMztzOjI2OiIvbmV3cy9hbGwtcGFja2VkLWZvci1pc3B5LyI7fXM6OToidGVtcGxhdGVzIjthOjIwOntpOjE7czoxOiIxIjtpOjc7czoxOiIyIjtpOjg7czoxOiIyIjtpOjk7czoxOiIyIjtpOjEzO3M6MToiNSI7aToxNDtzOjE6IjUiO2k6MTY7czoxOiI2IjtpOjE3O3M6MToiMyI7aToxODtzOjE6IjMiO2k6MTk7czoxOiIzIjtpOjIwO3M6MToiMyI7aToyMTtzOjE6IjMiO2k6MjI7czoxOiIzIjtpOjIzO3M6MToiMyI7aToyNDtzOjE6IjMiO2k6Mjc7czoxOiI5IjtpOjI4O3M6MToiNSI7aToyOTtzOjI6IjEwIjtpOjMwO3M6MToiNSI7aTozMztzOjE6IjUiO319fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `exp_snippets`
--

CREATE TABLE IF NOT EXISTS `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_snippets`
--

INSERT INTO `exp_snippets` (`snippet_id`, `site_id`, `snippet_name`, `snippet_contents`) VALUES
(1, 1, 'sn_doc_head', '<!DOCTYPE html>\n<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->\n<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->\n<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->\n    <head>\n       <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">\n\n  <meta name="author" content="{site_name}">\n  <title>{site_name} | {segment_1} - {title}</title>\n  <meta name="description" content="Delivering innovation to the inflight &amp; travel retail sector &dash; a refreshingly integrated approach to brand development, creative &amp; marketing support, purchasing, sales &amp; distribution...">\n <meta name="copyright" content="Copyright {current_time format=''%Y''} {site_name} All rights reserved.">\n        <meta name="viewport" content="width=device-width">\n\n\n        <link rel="stylesheet" href="/resource/css/normalize.css">\n        <link rel="stylesheet" href="/resource/css/main.css">\n        \n        <!--[if !IE 7]>\n        	<style type="text/css">\n        		#wrap {display:table;height:100%}\n        	</style>\n        <![endif]-->\n        \n        <!--[if gte IE 9]>\n          <style type="text/css">\n            .gradient {\n               filter: none;\n            }\n          </style>\n        <![endif]-->\n        \n        <script src="/resource/js/vendor/modernizr-2.6.2.min.js"></script>\n{if segment_1 == "accessories" && segment_2 == "tipsy-feet"}\n\n        <link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">\n<script src="http://vjs.zencdn.net/c/video.js"></script>\n{/if}\n        \n        <!--[if (gte IE 6)&(lte IE 9)]>\n		<script type="text/javascript" src="/resource/js/vendor/nwmatcher-1.2.5.js"></script>\n		<script type="text/javascript" src="/resource/js/vendor/selectivizr.js"></script>\n        <![endif]-->\n\n<script \n src="http://twitterjs.googlecode.com/svn/trunk/src/twitter.min.js" \n type="text/javascript">\n</script>\n<script type="text/javascript" charset="utf-8">\ngetTwitters(''tweet'', { \n  id: ''STMbrands'', \n  count: 1, \n  enableLinks: true, \n  ignoreReplies: true, \n  clearContents: true,\n  template: ''"%text%" <a href="http://twitter.com/%user_screen_name%/statuses/%id_str%/">%time%</a>''\n});\n</script>\n        \n        '),
(2, 1, 'sn_header', '		<header>\n			<div id="centered" class="clearfix">\n			<p id="brand"><a href="/"><img src="/resource/stat/STM-Brands-Logo-white.png" alt="STM Brands" /></a></p>\n			<nav>\n				<ul id="nav">\n<li><a href="/food-and-drink" id="our-products" class="show-drop" style="cursor:default;">Our Portfolio</a>\n<ul class="dropdown">\n<li class="first-item"><a href="/food-and-drink">Food and Drink</a></li>\n<li class="second-item"><a href="/cosmetics">Cosmetics</a></li>\n<li class="last-item"><a href="/accessories">Accessories</a></li>\n</ul>\n</li>\n<li><a href="/our-distribution-and-procurement">Our Distribution</a></li>\n<li><a href="/our-team">Our Team</a></li>\n<li><a href="/news">News</a></li>\n<li><a href="/get-in-touch">Get in Touch</a></li>\n					{!--exp:structure:nav include_ul="no"--}\n				</ul>\n			</nav>\n			<ul id="connected">\n				<li>Call us~07800 897672</li>\n					<li><a href="mailto:janice@shoot-the-moon.co.uk">Email us</a></li>\n		  		<li class="fb"><a href="http://www.facebook.com/pages/STMBRANDS/196944163690017" target="_blank" class="ir">Like us on Facebook</a></li>\n				<li class="tw"><a href="http://twitter.com/STMbrands" target="_blank" class="ir">Follow us on Twitter</a></li>\n			</ul>\n			</div>\n		</header>'),
(3, 1, 'sn_footer', '<footer class="clearfix">\n            	<section>\n <!-- <div id="tweet">\n <p><img src="/resource/stat/ajax-loader.gif">&nbsp;&nbsp;Please wait while our tweets load </p>\n <p><a href="http://twitter.com/STMbrands">If you can''t wait - check out what we''ve been twittering</a></p>\n</div>  -->\n<div class="twitter">\n            <p><a href="">@STMBrands</a> {exp:twitter_recent_tweets:get_tweets\n	    oauth_access_token="621719873-U3xvL64HdW2mmjL5susDnh9mr8zcI1obxtm0atn9" <!--required-->\n	    oauth_access_token_secret="MxXyb9W6sYbgeV5YNasD9p2VV3JWq2N9mffZ07ct8pE" <!--required-->\n	    consumer_key="6FEniahEAoWvYxq822qU8w" <!--required-->\n	    consumer_secret="bp5POAbPi3oy7glN5kF2t5yEG2T12Xaod0xIfZZbMY" <!--required-->\n	    screen_name="STMbrands" <!--optional-->\n	    count="1" <!--optional-->\n	    trim_user="1" <!--optional-->\n	    exclude_replies="1" <!--optional-->\n	    contributor_details="0" <!--optional-->\n	    include_rts="0" <!--optional-->\n	}\n	        {text}\n	        <span>{time_ago}</span>\n	{/exp:twitter_recent_tweets:get_tweets}</p>\n            	</div>\n<form id="newsLetter" method="post" action="http://www.stmbrands.co.uk/">\n<div class="hiddenFields">\n<input type="hidden" name="ACT" value="41">\n<input type="hidden" name="URI" value="index">\n<input type="hidden" name="XID" value="9fd707f18a0f783fcf68bfacb41bfc6c2451a0a9">\n<input type="hidden" name="status" value="open">\n<input type="hidden" name="return" value="">\n<input type="hidden" name="redirect_on_duplicate" value="">\n<input type="hidden" name="RET" value="http://www.stmbrands.co.uk">\n<input type="hidden" name="form_name" value="Newsletter">\n<input type="hidden" name="ajax_request" value="y">\n<input type="hidden" name="params_id" value="19646">\n<input type="hidden" name="site_id" value="1">\n</div>\n\n\n            		<fieldset>\n            			<legend>News in your inbox</legend>\n            			<label for="">Email:</label><input id="email" name="email" type="email" placeholder="example@domain.com" required="">\n            		</fieldset>\n            		<fieldset>\n            			<button type="submit">GO</button>\n            		</fieldset>\n            	</form>\n<p class="stmLink"><a href="http://www.shoot-the-moon.co.uk" target="_blank">Our creative side</a></p>\n            	</section>\n            </footer>'),
(4, 1, 'sn_base_scripts', '       <script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script>\n        <script src="/resource/js/plugins.js"></script>\n        <script src="/resource/js/main.js"></script>\n<script src="/resource/js/vendor/nwmatcher-1.2.5.js"></script>\n{if segment_1 == "accessories" && segment_2 == "tipsy-feet"}\n<script src="/resource/js/fancybox/jquery.fancybox.pack.js"></script>\n<link rel="stylesheet" type="text/css" href="/resource/js/fancybox/jquery.fancybox.css" media="screen" />\n{/if}\n\n        <!-- Google Analytics: change UA-XXXXX-X to be your site''s ID. -->\n        <script>\n            var _gaq=[[''_setAccount'',''UA-17786774-10''],[''_trackPageview'']];\n            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n            g.src=(''https:''==location.protocol?''//ssl'':''//www'')+''.google-analytics.com/ga.js'';\n            s.parentNode.insertBefore(g,s)}(document,''script''));\n        </script>\n{if segment_1=="news"}\n<script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script>\n{/if}'),
(5, 1, 'sn_chrome_frame', '        <!--[if lt IE 7]>\n            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>\n        <![endif]-->');

-- --------------------------------------------------------

--
-- Table structure for table `exp_specialty_templates`
--

CREATE TABLE IF NOT EXISTS `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `exp_specialty_templates`
--

INSERT INTO `exp_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`) VALUES
(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),
(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),
(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),
(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),
(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),
(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),
(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),
(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),
(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),
(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),
(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),
(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),
(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),
(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),
(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),
(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_stats`
--

CREATE TABLE IF NOT EXISTS `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_stats`
--

INSERT INTO `exp_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`) VALUES
(1, 1, 2, 2, 'Staff', 20, 0, 0, 0, 1357814295, 0, 0, 1374765997, 24, 1356091015, 1375370702);

-- --------------------------------------------------------

--
-- Table structure for table `exp_statuses`
--

CREATE TABLE IF NOT EXISTS `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_statuses`
--

INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`) VALUES
(1, 1, 1, 'open', 1, '009933'),
(2, 1, 1, 'closed', 2, '990000'),
(3, 1, 2, 'closed', 1, '990000'),
(4, 1, 2, 'draft', 2, 'B59A42'),
(5, 1, 2, 'submitted', 3, '3E6C89'),
(6, 1, 2, 'open', 4, '009933');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_groups`
--

CREATE TABLE IF NOT EXISTS `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_status_groups`
--

INSERT INTO `exp_status_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Statuses'),
(2, 1, 'Better Workflow');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure`
--

CREATE TABLE IF NOT EXISTS `exp_structure` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `listing_cid` int(6) unsigned NOT NULL DEFAULT '0',
  `lft` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rgt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dead` varchar(100) NOT NULL,
  `hidden` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure`
--

INSERT INTO `exp_structure` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `listing_cid`, `lft`, `rgt`, `dead`, `hidden`) VALUES
(0, 0, 0, 0, 0, 1, 34, 'root', 'n'),
(1, 1, 0, 2, 0, 2, 3, '', 'n'),
(1, 7, 0, 3, 0, 4, 13, '', 'n'),
(1, 8, 0, 3, 0, 14, 17, '', 'n'),
(1, 9, 0, 3, 0, 18, 25, '', 'n'),
(1, 20, 9, 6, 0, 21, 22, '', 'n'),
(1, 19, 9, 6, 0, 19, 20, '', 'n'),
(1, 18, 8, 6, 0, 15, 16, '', 'n'),
(1, 13, 0, 7, 8, 28, 29, '', 'n'),
(1, 16, 0, 7, 0, 30, 31, '', 'n'),
(1, 17, 7, 6, 0, 5, 6, '', 'n'),
(1, 21, 7, 6, 0, 7, 8, '', 'n'),
(1, 22, 7, 6, 0, 9, 10, '', 'n'),
(1, 23, 9, 6, 0, 23, 24, '', 'n'),
(1, 24, 7, 6, 0, 11, 12, '', 'n'),
(1, 27, 0, 7, 0, 26, 27, '', 'n'),
(1, 29, 0, 7, 0, 32, 33, '', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_channels`
--

CREATE TABLE IF NOT EXISTS `exp_structure_channels` (
  `site_id` smallint(5) unsigned NOT NULL,
  `channel_id` mediumint(8) unsigned NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `type` enum('page','listing','asset','unmanaged') NOT NULL DEFAULT 'unmanaged',
  `split_assets` enum('y','n') NOT NULL DEFAULT 'n',
  `show_in_page_selector` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`site_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_channels`
--

INSERT INTO `exp_structure_channels` (`site_id`, `channel_id`, `template_id`, `type`, `split_assets`, `show_in_page_selector`) VALUES
(1, 2, 1, 'page', 'n', 'y'),
(1, 4, 0, 'unmanaged', 'n', 'y'),
(1, 5, 0, 'unmanaged', 'n', 'y'),
(1, 3, 2, 'page', 'n', 'y'),
(1, 1, 3, 'listing', 'n', 'y'),
(1, 6, 3, 'page', 'n', 'y'),
(1, 7, 0, 'page', 'n', 'y'),
(1, 8, 5, 'listing', 'n', 'y'),
(1, 9, 0, 'unmanaged', 'n', 'y'),
(1, 10, 1, 'listing', 'n', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_listings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_listings` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `template_id` int(6) unsigned NOT NULL DEFAULT '0',
  `uri` varchar(75) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_listings`
--

INSERT INTO `exp_structure_listings` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `template_id`, `uri`) VALUES
(1, 14, 13, 8, 5, 'stm-brands-and-shoot-the-moon-confirm-support-for-ispy-2013'),
(1, 28, 13, 8, 5, 'add-a-touch-of-glamour'),
(1, 30, 13, 8, 5, 'perfect-for-packing'),
(1, 33, 13, 8, 5, 'all-packed-for-ispy');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_members`
--

CREATE TABLE IF NOT EXISTS `exp_structure_members` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `nav_state` text,
  PRIMARY KEY (`site_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_structure_members`
--

INSERT INTO `exp_structure_members` (`member_id`, `site_id`, `nav_state`) VALUES
(1, 1, '["7","8"]');

-- --------------------------------------------------------

--
-- Table structure for table `exp_structure_settings`
--

CREATE TABLE IF NOT EXISTS `exp_structure_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `var` varchar(60) NOT NULL,
  `var_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `exp_structure_settings`
--

INSERT INTO `exp_structure_settings` (`id`, `site_id`, `var`, `var_value`) VALUES
(1, 0, 'action_ajax_move', '40'),
(2, 0, 'module_id', '16'),
(19, 1, 'add_trailing_slash', 'y'),
(18, 1, 'hide_hidden_templates', 'y'),
(17, 1, 'redirect_on_publish', 'structure_only'),
(16, 1, 'redirect_on_login', 'y'),
(20, 1, 'perm_delete_6', 'none'),
(21, 1, 'perm_reorder_6', 'all');

-- --------------------------------------------------------

--
-- Table structure for table `exp_templates`
--

CREATE TABLE IF NOT EXISTS `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `exp_templates`
--

INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(1, 1, 1, 'index', 'y', 'webpage', '{exp:channel:entries url_title="stm-brands"}\n\n{sn_doc_head}        \n</head>\n\n<body class="home">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		{embed="site/category_carousel"}\n		\n		<section id="content" class="clearfix">\n			<div id="editorial">\n				<h1>{homepage_copy}</h1>\n				<ul>\n					<li>\n						<h2>What we do</h2>\n							<p>{snippet_copy}{value_copy}{/snippet_copy}</p>\n					</li>\n					<li>\n						<h2>Who we are</h2>\n						<p>{snippet_copy}{authentic_copy}{/snippet_copy}</p>\n					</li>\n					<li class="last-item">\n						<h2>How we do it</h2>\n						<p>{snippet_copy}{service_copy}{/snippet_copy}</p>\n					</li>\n				</ul>\n			</div>\n			<ul id="sidePanel">\n				<li class="banner">\n					<a href="/our-distribution-and-procurement"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/authentic_small.jpg" />\n					<h3>New for 2013</h3>\n					<p>Cocktails in a can...</p>\n					<a href="/food-and-drink/the-authentic-cocktail/"></a>\n				</li>\n				<li>\n					<img src="/images/stmbrands/products/glamorous_small.jpg" />\n					<h3>Onboard Now...</h3>\n					<p>...and Sales forecast doubled for Winter 2012</p>\n					<a href="/accessories/glamorous-bra-straps/"></a>\n				</li>\n			</ul>\n		</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n\n{/exp:channel:entries}', '', 1360160492, 1, 'n', 0, '', 'n', 'n', 'o', 12504),
(2, 1, 1, 'category_landing', 'y', 'webpage', '{exp:channel:entries}\r\n\r\n{sn_doc_head}\r\n</head>\r\n\r\n<body>\r\n	{sn_chrome_frame}\r\n\r\n	{sn_header}\r\n\r\n	<div id="wrap" class="clearfix">\r\n	<section id="content" class="clearfix">\r\n		<div id="catOverview">\r\n			<h1>{title}</h1>\r\n			{images}\r\n			{exp:ce_img:single src="{main_image}" width="420" height="420"}\r\n			{/images}\r\n			<p class="catText">{description}</p>\r\n{/exp:channel:entries}\r\n\r\n			<div id="cta">\r\n				<h3>Looking for brand exposure or need some new ideas?</h3>\r\n				<p>Give us a call on 0161 205 3311</p>\r\n				<p>Email: <a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\r\n			</div>\r\n		</div>\r\n		<ul id="catBrands">\r\n			<?php $i = 1; ?>\r\n			{exp:channel:entries entry_id="{structure:child_ids}" fixed_order="{structure:child_ids}"}\r\n			<li <?php if ($i == 2) echo ''class="last-item"''; ?>><h4>{title}</h4><a href="{page_uri}">{brand}{exp:ce_img:single src="{brand_image}" width="195" height="195"}{/brand}</a></li>\r\n			<?php \r\n				if ($i == 2) {\r\n					$i = 1;\r\n				} else {\r\n					$i++;\r\n				}\r\n			?>\r\n			{/exp:channel:entries}\r\n		</ul>\r\n	</section>\r\n	</div>\r\n	\r\n	{sn_footer}\r\n	{sn_base_scripts}\r\n	\r\n</body>\r\n</html>\r\n', '', 1363369290, 1, 'n', 0, '', 'n', 'y', 'o', 2281),
(3, 1, 1, 'single_product', 'y', 'webpage', '{exp:channel:entries}\r\n\r\n{sn_doc_head}        \r\n</head>\r\n\r\n<body class="home">\r\n	{if segment_2 == "tipsy-feet"}\r\n	<div id="video-box" style="display:none;">\r\n		<video id="my_video_1" class="video-js vjs-default-skin" controls\r\n		  preload="auto" width="480" height="270" poster=""\r\n		  data-setup="{}">\r\n		  <source src="/video/Tipsyfeet_Foldable_Shoes_And_Matching_Pouch.mp4" type=''video/mp4''>\r\n		  <source src="/video/Tipsyfeet_Foldable_Shoes_And_Matching_Pouch.webm" type=''video/webm''>\r\n		</video>\r\n	</div>\r\n	{/if}\r\n	\r\n	{sn_chrome_frame}\r\n\r\n	{sn_header}\r\n	<div id="wrap" class="clearfix">\r\n		<section id="content" class="clearfix">\r\n			\r\n			<div id="catOverview">\r\n				<h1>{title}</h1>\r\n				<div id="main-image">\r\n					{if brand}{brand}<img src="{brand_image}" width="420" height="420" class="brand-shot">{/brand}{/if}\r\n					{products}\r\n					{exp:ce_img:single src="{product_image}" width="420" height="420" class="product{row_count}"}\r\n					{/products}\r\n				</div>\r\n			</div>\r\n			\r\n			<div id="prodDetails">\r\n				<ul class="clearfix" id="product-nav">\r\n					<?php $i = 1; ?>\r\n					{products}\r\n					<li <?php if ($i == 4) echo ''class="last-item"''; ?>><a href="" class="product{row_count}">{exp:ce_img:single src="{product_image}" width="80" height="80"}</a></li>\r\n					<?php \r\n						if ($i == 4) {\r\n							$i = 1;\r\n						} else {\r\n							$i++;\r\n						}\r\n					?>\r\n					{/products}\r\n					<li class="selected"><a href="" class="brand-shot">{brand}{exp:ce_img:single src="{brand_image}" width="80" height="80"}{/brand}</a></li>\r\n				</ul>\r\n				\r\n				<div id="product-content">\r\n					{brand}\r\n					<div class="brand-shot">\r\n						<h2>{title}</h2>\r\n						<p>{brand_description}</p>\r\n						{if segment_2 == "tipsy-feet"}\r\n						<p><a href="#video-box" class="fancybox">Play Tipsy Feet Video</a></p>\r\n						{/if}\r\n					</div>\r\n					{/brand}\r\n					{products}\r\n					<div class="product{row_count}">\r\n						<h2>{product_title}</h2>\r\n						<p>{product_description}</p>\r\n					</div>\r\n					{/products}\r\n				</div>\r\n\r\n				\r\n				{embed="site/other-brands" entry_ids="{exp:stm_helpers:get_siblings entry=''{structure:page:entry_id}'' parent=''{structure:parent:entry_id}''}" entry_id="{structure:page:entry_id} parent="{structure:parent:entry_id}"}\r\n				\r\n{/exp:channel:entries}\r\n				\r\n			</div>\r\n			\r\n		</section>\r\n	</div>\r\n	\r\n	\r\n	{sn_footer}\r\n	{sn_base_scripts}\r\n</body>\r\n</html>\r\n', '', 1363369290, 1, 'n', 0, '', 'n', 'y', 'o', 2801),
(4, 1, 1, 'category_carousel', 'y', 'webpage', '<section id="categories">\n	<!--p class="scroll left"><a href="" class="ir">Scroll left</a></p-->\n	<ul class="clearfix">\n		\n		{exp:channel:entries channel="product-category" orderby="entry_date" sort="asc"}\n		<li>\n			<h2>{title}</h2>\n			<p><a href="{page_uri}">{images}{exp:ce_img:single src="{home_image}" width="280" height="227"}{/images}</a></p>\n		</li>\n		{/exp:channel:entries}\n	</ul>\n	<!--p class="scroll right"><a href="" class="ir">Scroll right</a></p-->\n</section>\n', '', 1363369290, 1, 'n', 0, '', 'n', 'n', 'o', 8),
(5, 1, 1, 'news', 'y', 'webpage', '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				\n					\n				\n					\n					\n					{if segment_2}\n					<div id="newsLeftSingle">\n						<h1>News &amp; Updates</h1>\n						<div class="news-snippet">\n						<h2>{title}</h2>\n						<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n						{exp:ce_img:single src="{news_image}" width="420"}\n						{news_story}\n						{if segment_2}<p class="back"><a href="/news">Back to all news</a></p>{/if}\n						</div>\n						<span class="share">Share this story via:</span>{exp:hj_social_bookmarks sites="Facebook|Twitter|Email" entry_id="13" show_hyperlink_text="right" class="social"}\n					</div>\n					<div id="newsRightSingle">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n						\n<div id="divRss"></div>\n\n\n\n\n					</div>\n					{/if}\n					\n					\n					\n{/exp:channel:entries}\n\n				\n				\n				\n					\n					{if segment_2 == ""}\n					<div id="newsLeftList">\n						<h1>News &amp; Updates</h1>\n					{exp:channel:entries channel="news" dynamic="off"}\n					\n						<div class="news-snippet">\n							\n							{exp:ce_img:single src="{news_image}" width="195" class="news-thumb"}\n							{if news_image==""}\n								<div class="news-excerpt noimg">\n								{if:else}\n							<div class="news-excerpt">\n							{/if}\n								<h2><a href="{page_url}">{title}</a></h2>\n								<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n								<p>{news_snippet}</p>\n								<p class="more"><a href="{page_url}">Read more</a></p>\n							</div>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					<div id="newsRightList">\n						<!--<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news/news-archive/"}\n							<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>\n						</ul>-->\n						\n						<h1>Moodie Report</h1>\n<div id="divRss"></div>\n\n\n\n\n\n\n\n\n\n\n					</div>\n					{/if}\n			\n				\n					\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<!-- <script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script> -->\n<script type="text/javascript" src="/resource/js/vendor/FeedEk.js"></script>\n \n<script type="text/javascript" >\n$(document).ready(function(){\n  \n  $(''#txtUrl'').val(''http://rss.cnn.com/rss/edition.rss'');\n  $(''#txtCount'').val(''5'');\n  $(''#chkDate'').attr(''checked'',''checked'');\n  $(''#chkDesc'').attr(''checked'',''checked'');\n  \n  \n   $(''#divRss'').FeedEk({\n   FeedUrl : ''http://www.moodiereport.com/rss.php'',\n   MaxCount : 5,\n   ShowDesc : true,\n   ShowPubDate: true\n  });\n  \n});\n\nfunction OpenBox()\n{\n$(''#divSrc'').toggle();\n}\nfunction changeFeedUrl()\n{\nvar cnt= 5;\nvar showDate=new Boolean();\nshowDate=true;\n\nvar showDescription=new Boolean();\nshowDescription=true;\n\nif($(''#txtCount'').val()!="") cnt=parseInt($(''#txtCount'').val());\nif (! $(''#chkDate'').attr(''checked'')) showDate=false;\nif (! $(''#chkDesc'').attr(''checked'')) showDescription=false;\n\n $(''#divRss'').FeedEk({\n   FeedUrl : $(''#txtUrl'').val(),\n   MaxCount : cnt,\n   ShowDesc : showDescription,\n   ShowPubDate: showDate\n  });\n}\n</script>\n\n\n\n	\n</body>\n</html>', '', 1363369290, 1, 'n', 0, '', 'n', 'n', 'o', 2597),
(6, 1, 1, 'get_in_touch', 'y', 'webpage', '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="home no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				<h1>{title}</h1>\n				\n				{exp:freeform:form collection="Contact Form" form:class="contact-form" required="forename|surname|email|message" return="/thank-you" recipients="yes" recipient_limit="1" recipient_template="contact_us"}\n					\n					<input type="hidden" name="recipient_email" value="janice@shoot-the-moon.co.uk">\n\n					<div class="row">\n						<label>First Name*</label>\n						<input type="text" name="forename" value="" class="required" placeholder="First name only please">\n					</div>\n				\n					<div class="row">\n						<label>Surname*</label>\n						<input type="text" name="surname" value="" class="required" placeholder="Your surname">\n					</div>\n				\n					<div class="row">\n						<label>Email Address*</label>\n						<input type="text" name="email" value="" class="required" placeholder="Your email address">\n					</div>\n				\n					<div class="row"><label>Message*</label>\n						<textarea name="message" class="required" placeholder="Please leave a short message and we''ll be in-touch"></textarea>\n					</div>\n				\n					<div class="row"><input type="submit" name="submit" value="Submit your enquiry" class="submit"></div>\n				\n				{/exp:freeform:form}\n				\n				\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n{/exp:channel:entries}\n', '', 1363369290, 1, 'n', 0, '', 'n', 'n', 'o', 446),
(7, 1, 1, 'thank-you', 'y', 'webpage', '{sn_doc_head}        \n</head>\n\n<body class="home no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix forceHeight">\n				\n				<h1>Form submission successful</h1>\n				<p>A member of our team will be in touch soon.</p>\n				\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n', '', 1358172734, 1, 'n', 0, '', 'n', 'n', 'o', 73),
(8, 1, 1, 'other-brands', 'y', 'webpage', '<h3>Also available</h3>\r\n<ul class="clearfix">\r\n	{if embed:entry_ids != ""}\r\n	{exp:channel:entries entry_id="{embed:entry_ids}" channel="product-brand" dynamic="no" orderby="random" limit="4"}\r\n	<li{if {count} == 4} class="last-item"{/if}><a href="{page_uri}">{brand}{exp:ce_img:single src="{brand_image}" width="80" height="80"}{/brand}</a></li>\r\n	{/exp:channel:entries}\r\n	{/if}\r\n	{if embed:entry_ids == ""}\r\n	{exp:channel:entries entry_id="not {embed:entry_id}" channel="product-brand" dynamic="no" orderby="random" limit="4"}\r\n	<li{if {count} == 4} class="last-item"{/if}><a href="{page_uri}">{brand}{exp:ce_img:single src="{brand_image}" width="80" height="80"}{/brand}</a></li>\r\n	{/exp:channel:entries}\r\n	{/if}\r\n</ul>\r\n', '', 1363369290, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(9, 1, 1, 'procurement', 'y', 'webpage', '{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n		<section id="content" class="clearfix">					\n			<div id="pageLeft">\n				<h1>Our Distribution and Procurement</h1>\n				<img src="/img/procurement.jpg" width="420" height="290" alt="">\n				\n				<p style="font-weight: bold;">An end-to-end procurement & logistics solution, aimed at driving your range development through sourcing true innovation at competitive prices, supported by an established hassle-free logistics solution.\n</p>\n \n<p>Sourcing products for the inflight & travel sector can be time consuming, requiring specialist category knowledge in both food & non food categories. STM Brands not only provides brands from our existing portfolio partners, but can also source products on your behalf from the UK, Europe & Worldwide.</p>\n\n<p>STM Brands aim to facilitate a consumer-focused range within the sector margin parameters, ensuring your retail proposition has a recognised point of difference.</p>\n			</div>\n			<div id="pageRight">\n				<ul>\n					<li>Established, industry-wide distribution network</li>\n	<li>Effective & efficient logistics, ensuring hassle-free sourcing</li>\n	<li>Competitive, delivered to depot pricing</li>\n	<li>Real innovation through unique sourcing approach</li>\n	<li>Bespoke sourcing capability in support of your Own Brands</li>\n	<li>With the added creative support of our sister agency, <a href="http://www.shoot-the-moon.co.uk" target="_blank">Shoot the Moon</a></li>\n				</ul>\n			</div>\n		</section>\n	\n	</div>\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>', '', 1358186452, 1, 'n', 0, '', 'n', 'n', 'o', 563),
(10, 1, 1, 'aboutus', 'y', 'webpage', '{sn_doc_head}\n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n\n	<div id="wrap" class="clearfix">\n	<section id="content" class="clearfix">\n		<div id="catOverview">\n\n{exp:channel:entries channel="generic" limit="1" entry_id="29"}\n\n			<h1>{title}</h1>\n			{body_text}\n{/exp:channel:entries}\n			<div id="cta">\n				<h3>Looking for brand exposure or need some new ideas?</h3>\n				<p>Give us a call on 0161 205 3311</p>\n				<p>Email: <a href="mailto:stmbrands@shoot-the-moon.co.uk">stmbrands@shoot-the-moon.co.uk</a></p>\n			</div>\n		</div>\n		<ul id="catBrands" class="aboutUs">\n			<li>\n				<h4>Janice Hockenhull</h4>\n				<a href="#"><img src="/img/staff/janice.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Phil Marshall</h4>\n				<a href="#"><img src="/img/staff/phil-marshall.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li>\n				<h4>Alistair Stirling</h4>\n				<a href="#"><img src="/img/staff/alistairstirling.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li class="last-item">\n				<h4>Oliver Zebedee-Howard</h4>\n				<a href="#"><img src="/img/staff/oliver.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n			<li>\n				<h4>Vicky Carson</h4>\n				<a href="#"><img src="/img/staff/vickycarson.jpg" width="195" height="195" alt="" style="cursor:default;"></a>\n			</li>\n		</ul>\n	</section>\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>', '', 1363369290, 1, 'n', 0, '', 'n', 'n', 'o', 620),
(11, 1, 1, 'news-archive', 'y', 'webpage', '{exp:channel:entries}\n\n{sn_doc_head}        \n</head>\n\n<body class="no-bg">\n	{sn_chrome_frame}\n\n	{sn_header}\n	\n	<div id="wrap" class="clearfix">\n		\n			<section id="content" class="clearfix">\n				\n				\n					\n				\n					\n					\n					{if segment_2}\n					<div id="newsLeftSingle">\n						<h1>News &amp; updates</h1>\n						<div class="news-snippet">\n						<h2>{title}</h2>\n						<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n						{exp:ce_img:single src="{news_image}" width="420"}\n						{news_story}\n						{if segment_2}<p class="back"><a href="/news">Back to all news</a></p>{/if}\n						</div>\n						<ul id="social">\n							<li><span><img src="/img/facebook.jpg"></span><a href="#">Share story on Facebook</a></li>\n							<li><span><img src="/img/twitter.jpg"></span><a href="#">Share story on Twitter</a></li>\n							<li><span><img src="/img/email.jpg"></span><a href="#">Email story to a friend</a></li>\n						</ul>\n					</div>\n					<div id="newsRightSingle">\n						<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news-events/archives/"}\n							<!--<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>-->\n						</ul>\n						\n						<h1>Moodie Report</h1>\n					</div>\n					{/if}\n					\n					\n					\n{/exp:channel:entries}\n\n				\n				\n				\n					\n					{if segment_2 == ""}\n					<div id="newsLeftList">\n						<h1>News &amp; updates</h1>\n					{exp:channel:entries channel="news" dynamic="off"}\n					\n						<div class="news-snippet">\n							<div class="news-thumb">\n								{exp:ce_img:single src="{news_image}" width="195"}\n							</div>\n							<div class="news-excerpt">\n								<h2><a href="{page_url}">{title}</a></h2>\n								<div class="details"><span>Posted by: {author}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>Posted: {entry_date format=''%d/%m/%y''}</span></div>\n								<p>{news_snippet}</p>\n								<p class="more"><a href="{page_url}">Read more</a></p>\n							</div>\n						</div>\n						{/exp:channel:entries}\n					</div>\n					<div id="newsRightList">\n						<h1>Archives</h1>\n						<ul id="archives">\n							{exp:structure_monthly_archives:show parent_entry_id="8" url_path="/news-events/archives/"}\n							<!--<li><a href="#">December 2012</a></li>\n							<li><a href="#">November 2012</a></li>\n							<li><a href="#">October 2012</a></li>\n							<li><a href="#">September 2012</a></li>\n							<li><a href="#">August 2012</a></li>\n							<li><a href="#">July 2012</a></li>\n							<li><a href="#">June 2012</a></li>\n							<li><a href="#">May 2012</a></li>-->\n						</ul>\n						\n						<h1>Moodie Report</h1>\n					{/if}\n			\n				\n					\n			</section>\n		\n	</div>\n	\n	{sn_footer}\n	{sn_base_scripts}\n	\n</body>\n</html>\n', NULL, 1363369290, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(12, 1, 1, 'test', 'y', 'webpage', '<html xmlns="http://www.w3.org/1999/xhtml" >\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n<title>FeedEk Jquery RSS/ATOM Feed Plugin Demo</title>\n\n<script type="text/javascript" src="/resource/js/vendor/jquery-1.8.3.min.js"></script>\n<script type="text/javascript" src="/resource/js/vendor/FeedEk.js"></script>\n \n<script type="text/javascript" >\n$(document).ready(function(){\n  \n  $(''#txtUrl'').val(''http://rss.cnn.com/rss/edition.rss'');\n  $(''#txtCount'').val(''5'');\n  $(''#chkDate'').attr(''checked'',''checked'');\n  $(''#chkDesc'').attr(''checked'',''checked'');\n  \n  \n   $(''#divRss'').FeedEk({\n   FeedUrl : ''http://www.moodiereport.com/rss.php'',\n   MaxCount : 5,\n   ShowDesc : true,\n   ShowPubDate: true\n  });\n  \n});\n\nfunction OpenBox()\n{\n$(''#divSrc'').toggle();\n}\nfunction changeFeedUrl()\n{\nvar cnt= 5;\nvar showDate=new Boolean();\nshowDate=true;\n\nvar showDescription=new Boolean();\nshowDescription=true;\n\nif($(''#txtCount'').val()!="") cnt=parseInt($(''#txtCount'').val());\nif (! $(''#chkDate'').attr(''checked'')) showDate=false;\nif (! $(''#chkDesc'').attr(''checked'')) showDescription=false;\n\n $(''#divRss'').FeedEk({\n   FeedUrl : $(''#txtUrl'').val(),\n   MaxCount : cnt,\n   ShowDesc : showDescription,\n   ShowPubDate: showDate\n  });\n}\n</script>\n\n\n</head>\n<body>\nTEST\n<div id="divRss"></div>\n</body>\n\n</html>\n', NULL, 1363369290, 1, 'n', 0, '', 'n', 'n', 'o', 15);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_template_groups`
--

INSERT INTO `exp_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`) VALUES
(1, 1, 'site', 1, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_template_member_groups`
--

INSERT INTO `exp_template_member_groups` (`group_id`, `template_group_id`) VALUES
(6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_throttle`
--

CREATE TABLE IF NOT EXISTS `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_prefs`
--

CREATE TABLE IF NOT EXISTS `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_upload_prefs`
--

INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`) VALUES
(1, 1, 'Products', '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/products/', 'http://stmbrands.stmpreview.co.uk/images/stmbrands/products/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(2, 1, 'Content Images', '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/content-images/', 'http://stmbrands.stmpreview.co.uk/images/stmbrands/content-images/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(3, 1, 'General', '/var/www/vhosts/stmpreview.co.uk/subdomains/stmbrands/images/stmbrands/general/', 'http://stmbrands.stmpreview.co.uk/images/stmbrands/general/', 'img', '', '', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_wygwam_configs`
--

CREATE TABLE IF NOT EXISTS `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_wygwam_configs`
--

INSERT INTO `exp_wygwam_configs` (`config_id`, `config_name`, `settings`) VALUES
(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9'),
(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ=='),
(3, 'Basic Clone', 'YTo1OntzOjc6InRvb2xiYXIiO2E6ODp7aTowO3M6NDoiQm9sZCI7aToxO3M6NjoiSXRhbGljIjtpOjI7czo5OiJVbmRlcmxpbmUiO2k6MztzOjk6IlRleHRDb2xvciI7aTo0O3M6MTI6Ik51bWJlcmVkTGlzdCI7aTo1O3M6MTI6IkJ1bGxldGVkTGlzdCI7aTo2O3M6NDoiTGluayI7aTo3O3M6NjoiVW5saW5rIjt9czo2OiJoZWlnaHQiO3M6MzoiMTIwIjtzOjE0OiJyZXNpemVfZW5hYmxlZCI7czoxOiJuIjtzOjExOiJjb250ZW50c0NzcyI7YTowOnt9czoxMDoidXBsb2FkX2RpciI7czowOiIiO30='),
(4, 'News Intro', 'YTo1OntzOjc6InRvb2xiYXIiO2E6NTp7aTowO3M6NDoiQm9sZCI7aToxO3M6NjoiSXRhbGljIjtpOjI7czo5OiJVbmRlcmxpbmUiO2k6MztzOjQ6IkxpbmsiO2k6NDtzOjY6IlVubGluayI7fXM6NjoiaGVpZ2h0IjtzOjM6IjEwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToibiI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9'),
(5, 'News Body', 'YTo1OntzOjc6InRvb2xiYXIiO2E6OTp7aTowO3M6NjoiRm9ybWF0IjtpOjE7czo0OiJCb2xkIjtpOjI7czo2OiJJdGFsaWMiO2k6MztzOjk6IlVuZGVybGluZSI7aTo0O3M6MTI6Ik51bWJlcmVkTGlzdCI7aTo1O3M6MTI6IkJ1bGxldGVkTGlzdCI7aTo2O3M6NDoiTGluayI7aTo3O3M6NjoiVW5saW5rIjtpOjg7czo5OiJUZXh0Q29sb3IiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6Im4iO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `exp_zoo_flexible_admin_menus`
--

CREATE TABLE IF NOT EXISTS `exp_zoo_flexible_admin_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT NULL,
  `group_id` int(4) DEFAULT NULL,
  `nav` text,
  `autopopulate` tinyint(1) DEFAULT NULL,
  `hide_sidebar` tinyint(1) DEFAULT NULL,
  `startpage` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_zoo_flexible_admin_menus`
--

INSERT INTO `exp_zoo_flexible_admin_menus` (`id`, `site_id`, `group_id`, `nav`, `autopopulate`, `hide_sidebar`, `startpage`) VALUES
(1, 1, 6, '<li class="home"  id="" ><a href="index.php?D=cp" class="first_level"><img src=/themes/cp_themes/default/images/home_icon.png /></a></li><li class=""  id="" ><a href="index.php?D=cp&C=addons_modules&M=show_module_cp&module=structure" class="first_level">Structure</a></li>', 1, 1, 'index.php?D=cp&C=addons_modules&M=show_module_cp&module=structure');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
